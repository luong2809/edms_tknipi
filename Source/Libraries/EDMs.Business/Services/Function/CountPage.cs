﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace EDMs.Business.Services.Function
{
    public class CountPage
    {
        public List<string> CountPageUpload(string path)
        {
            //read page size pdf
            //var CountPage = new List<int>() { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; //Letter, Legal, Ledger, A0, A1, A2, A3 ,A4, A5, A6 ,A7, A8
            var CountPage = new List<int>() { 0, 0, 0, 0, 0, 0 };
            PdfReader reader = new iTextSharp.text.pdf.PdfReader(path);

            for (int i = 1; i <= reader.NumberOfPages; i++)
            {
                Rectangle dim = reader.GetPageSize(i);
                var width = dim.Width;
                var height = dim.Height;
                if (width > height)
                {
                    width = dim.Height;
                    height = dim.Width;
                }
                var sizeInch = ConvertPointToMillimeters(width) + " x " + ConvertPointToMillimeters(height);
                switch (GetPagesizeFormat(width, height))
                {
                    //case "Letter":
                    //    CountPage[0] += 1;
                    //    break;
                    //case "Legal":
                    //    CountPage[1] += 1;
                    //    break;
                    //case "Ledger":
                    //    CountPage[2] += 1;
                    //    break;
                    case "A0":
                        CountPage[0] += 1;
                        break;
                    case "A1":
                        CountPage[1] += 1;
                        break;
                    case "A2":
                        CountPage[2] += 1;
                        break;
                    case "A3":
                        CountPage[3] += 1;
                        break;
                    case "A4":
                        CountPage[4] += 1;
                        break;
                    case "Other":
                        CountPage[5] += 1;
                        break;
                        //case "A5":
                        //    CountPage[8] += 1;
                        //    break;
                        //case "A6":
                        //    CountPage[9] += 1;
                        //    break;
                        //case "A7":
                        //    CountPage[10] += 1;
                        //    break;
                        //case "A8":
                        //    CountPage[11] += 1;
                        //    break;
                }
            }
            return Print(CountPage);
        }

        static List<string> Print(List<int> countPage)
        {
            List<string> page = new List<string>();
            for (var count = 0; count < countPage.Count; count++)
            {
                switch (count)
                {
                    //case 0:
                    //    page.Add("Letter :" + countPage[count]);
                    //    break;
                    //case 1:
                    //    page.Add("Legal :" + countPage[count]);
                    //    break;
                    //case 2:
                    //    page.Add("Ledger :" + countPage[count]);
                    //    break;
                    case 0:
                        page.Add("A0 :" + countPage[count]);
                        break;
                    case 1:
                        page.Add("A1 :" + countPage[count]);
                        break;
                    case 2:
                        page.Add("A2 :" + countPage[count]);
                        break;
                    case 3:
                        page.Add("A3 :" + countPage[count]);
                        break;
                    case 4:
                        page.Add("A4 :" + countPage[count]);
                        break;
                    case 5:
                        page.Add("Other :" + countPage[count]);
                        break;
                        //case 8:
                        //    page.Add("A5 :" + countPage[count]);
                        //    break;
                        //case 9:
                        //    page.Add("A6 :" + countPage[count]);
                        //    break;
                        //case 10:
                        //    page.Add("A7 :" + countPage[count]);
                        //    break;
                        //case 11:
                        //    page.Add("A8 :" + countPage[count]);
                        //    break;
                }
            }
            return page;
        }
        static float ConvertPointToMillimeters(float value)
        {
            return (float)Math.Round((value / 2.834646), 0);// point to mm
            //2.834646 point = 1 mm
            //1 point =72 inchess
        }
        static string GetPagesizeFormat(float width, float height)
        {
            /*Table: International Standard Paper Sizes
            Size        millimeters     inches
            Letter (US) 215.9 x 279.4   8.5 x 11
            Legal (US)  215.9 x 355.6   8.5 x 14
            Ledger (US) 279.4 x 431.8   11 X 17
            A0          841 x 1189      33.125 x 46.75
            A1          594 x 841       23.375 x 33.125
            A2          420 x 594       16.5 x 23.375
            A3          297 x 420       11.75 x 16.5
            A4          210 x 297       8.25 x11.75
            A5          148 x 210 
            A6          105 x 148
            A7          74 x 105
            A8          52 x 74
             */
            var sizeInch = ConvertPointToMillimeters(width) + " x " + ConvertPointToMillimeters(height);
            var Size = "";
            switch (sizeInch)
            {
                //case "215.9 x 279.4":
                //    Size = "Letter";
                //    break;
                //case "215.9 x 355.6":
                //    Size = "Legal";
                //    break;
                //case "279.4 x 431.8":
                //    Size = "Ledger";
                //    break;
                case "841 x 1189":
                    Size = "A0";
                    break;
                case "594 x 841":
                    Size = "A1";
                    break;
                case "420 x 594":
                    Size = "A2";
                    break;
                case "297 x 420":
                    Size = "A3";
                    break;
                case "210 x 297":
                    Size = "A4";
                    break;
                //case "148 x 210":
                //    Size = "A5";
                //    break;
                //case "105 x 148":
                //    Size = "A6";
                //    break;
                //case "74 x 105":
                //    Size = "A7";
                //    break;
                //case "52 x 74":
                //    Size = "A8";
                //    break;
                default:
                    Size = "Other";
                    break;

            }
            return Size;
        }
    }
}

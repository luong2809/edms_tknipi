﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttachDocToICATransmittalService.cs" company="">
//   
// </copyright>
// <summary>
//   The category service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class AttachDocToICATransmittalService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly AttachDocToICATransmittalDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttachDocToICATransmittalService"/> class.
        /// </summary>
        public AttachDocToICATransmittalService()
        {
            this.repo = new AttachDocToICATransmittalDAO();
        }

        #region Get (Advances)

        public bool IsExist(int transId, int docId)
        {
            return this.repo.IsExist(transId, docId);
        }

        public AttachDocToICATransmittal GetByDoc(int transId, int docId)
        {
            return this.repo.GetByDoc(transId,docId);
        }

        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<AttachDocToICATransmittal> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// The get all by doc id.
        /// </summary>
        /// <param name="docId">
        /// The doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AttachDocToICATransmittal> GetAllByDocId(int docId)
        {
            return this.repo.GetAllByDocId(docId).ToList();
        }

        /// <summary>
        /// The get all by doc id.
        /// </summary>
        /// <param name="docId">
        /// The doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AttachDocToICATransmittal> GetAllByTransId(int transId)
        {
            return this.repo.GetAllByTransId(transId);
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public AttachDocToICATransmittal GetById(int id)
        {
            return this.repo.GetById(id);
        }

        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(AttachDocToICATransmittal bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(AttachDocToICATransmittal bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(AttachDocToICATransmittal bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}

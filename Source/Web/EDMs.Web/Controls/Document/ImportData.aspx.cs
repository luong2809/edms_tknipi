﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using EDMs.Business.Services.Scope;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.Hosting;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    public class MonthWeekProgress
    {
        private int month;
        private string week;

        public MonthWeekProgress()
        {

        }

        public MonthWeekProgress(int month, string week)
        {
            this.month = month;
            this.week = week;
        }

        public int Month
        {
            get
            {
                return month;
            }

            set
            {
                month = value;
            }
        }

        public string Week
        {
            get
            {
                return week;
            }

            set
            {
                week = value;
            }
        }
    }

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportData : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly TransmittalService transmittalService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly WorkGroupService workGroupService;

        private readonly ProcessPlanedService processPlanedService;
        private readonly ProcessActualService processActualService;
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportData()
        {
            this.documentService = new DocumentService();
            this.transmittalService = new TransmittalService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.processPlanedService = new ProcessPlanedService();
            this.processActualService = new ProcessActualService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {

                    }
                }
            }
        }

        public DateTime GetMondayOfWeek(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        public DateTime GetFridayOfWeek(DateTime date)
        {
            return GetMondayOfWeek(date).AddDays(4);
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            //try
            //{
            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var transID = Convert.ToInt32(this.Request.QueryString["tranId"]);
                var transObj = this.transmittalService.GetById(transID);

                if (transObj != null)
                {
                    foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                    {
                        var filename = DateTime.Now.ToBinary() + "_" + docFile.FileName;
                        var oldfilePath = Server.MapPath(transObj.GeneratePath);

                        var newServerPath = "../../Transmittals/Generated/" + filename;
                        var newFilePath = Server.MapPath("../../Transmittals/Generated/") + filename;

                        docFile.SaveAs(newFilePath);

                        transObj.GeneratePath = newServerPath;
                        transObj.LastUpdatedBy = UserSession.Current.User.Id;
                        transObj.LastUpdatedDate = DateTime.Now;
                        transObj.IsGenerate = true;
                        if (File.Exists(oldfilePath))
                        {
                            File.Delete(oldfilePath);
                        }

                        this.transmittalService.Update(transObj);
                    }
                }
            }
            else if (!string.IsNullOrEmpty(this.Request.QueryString["type"]))
            {
                var type = this.Request.QueryString["type"];
                switch (type)
                {
                    case "progress":
                        foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                        {
                            var extension = docFile.GetExtension();
                            if (extension == ".xls" || extension == ".xlsx")
                            {
                                var importPath = Server.MapPath("../../Import") + "/" +
                                                 DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                                 "_" + docFile.FileName;
                                docFile.SaveAs(importPath);

                                var workbook = new Workbook();
                                workbook.Open(importPath);

                                var wsProgress = workbook.Worksheets[0];
                                var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(wsProgress.Cells["A1"].Value));
                                if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null)
                                {
                                    var startDate = projectObj.StartDate.GetValueOrDefault();
                                    //while (startDate.DayOfWeek != DayOfWeek.Monday)
                                    //{
                                    //    startDate = startDate.AddDays(1);
                                    //}
                                    //for (var j = GetFridayOfWeek(startDate);
                                    //    j < projectObj.Deadline.GetValueOrDefault();
                                    //    j = j.AddDays(7))
                                    //{
                                    //    count += 1;
                                    //}

                                    var progressActualWeekList = new List<string>();
                                    var progressActualMonthList = new List<string>();
                                    var progressPlanedWeekList = new List<string>();
                                    var progressPlanedMonthList = new List<string>();
                                    var numberofWeek = 0;
                                    var numberofMonth = 0;
                                    for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                        j < projectObj.Deadline.GetValueOrDefault();
                                        j = j.AddDays(7))
                                    {
                                        numberofWeek += 1;
                                    }
                                    var currentMonth = 0;
                                    for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                                j < projectObj.Deadline.GetValueOrDefault();
                                                j = j.AddDays(7))
                                    {
                                        if (currentMonth != j.Month)
                                        {
                                            currentMonth = j.Month;
                                            numberofMonth += 1;
                                        }

                                    }
                                    //                for (var j = projectObj.StartDate.GetValueOrDefault();
                                    //j < projectObj.Deadline.GetValueOrDefault();
                                    //j = j.AddMonths(1))
                                    //                {
                                    //                    numberofMonth += 1;
                                    //                }

                                    var progressType = wsProgress.Cells["A7"].Value.ToString();
                                    if (progressType == "Planed" || progressType == "Actual")
                                    {
                                        // Create a datatable
                                        var dataTable = new DataTable();
                                        if (projectObj.WeeklyProgress.GetValueOrDefault())
                                        {
                                            // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                                            dataTable = wsProgress.Cells.ExportDataTable(7, 0,
                                                wsProgress.Cells.MaxRow - 7, numberofWeek + 3);

                                            foreach (DataRow dataRow in dataTable.Rows)
                                            {
                                                var workgroupId = dataRow["Column1"].ToString();
                                                if (!string.IsNullOrEmpty(workgroupId))
                                                {
                                                    var wpObj =
                                                        this.workGroupService.GetById(Convert.ToInt32(workgroupId));
                                                    if (wpObj != null)
                                                    {
                                                        if (progressType == "Planed")
                                                        {
                                                            progressPlanedWeekList = new List<string>();
                                                            progressPlanedMonthList = new List<string>();
                                                            for (int i = 0; i < numberofWeek; i++)
                                                            {
                                                                var value =
                                                                    !string.IsNullOrEmpty(
                                                                        dataRow["Column" + (i + 4)].ToString())
                                                                        ? Math.Round(Convert.ToDouble(
                                                                            dataRow["Column" + (i + 4)].ToString()), 4) * 100
                                                                        : 0;
                                                                progressPlanedWeekList.Add(value.ToString());
                                                            }
                                                            //var flag = 0;
                                                            //var tempValue = 0.0;
                                                            //for (int i = 0; i < progressPlanedWeekList.Count; i++)
                                                            //{
                                                            //    if (flag < 4)
                                                            //    {
                                                            //        tempValue += Double.Parse(progressPlanedWeekList[i]);
                                                            //    }
                                                            //    flag++;
                                                            //    if (flag == 4)
                                                            //    {
                                                            //        progressPlanedMonthList.Add((tempValue / flag).ToString());
                                                            //        flag = 0;
                                                            //        tempValue = 0.0;
                                                            //    }
                                                            //}
                                                            currentMonth = 0;
                                                            var weekinMonth = 0;
                                                            var countWeek = 0;
                                                            var countMonth = 0;
                                                            var tempValue = 0.0;
                                                            var newMonth = false;
                                                            for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                                                        j < projectObj.Deadline.GetValueOrDefault();
                                                                        j = j.AddDays(7))
                                                            {
                                                                if (progressPlanedWeekList.Count > countWeek)
                                                                {
                                                                    if (currentMonth != j.Month)
                                                                    {
                                                                        //if (countMonth != 0)
                                                                        //{
                                                                        //    planMonthList.Add(tempValue / weekinMonth);
                                                                        //}
                                                                        double temp;
                                                                        tempValue = Double.TryParse(progressPlanedWeekList[countWeek], out temp) ? Double.Parse(progressPlanedWeekList[countWeek]) : 0;
                                                                        weekinMonth = 1;
                                                                        countMonth++;
                                                                        currentMonth = j.Month;
                                                                        newMonth = true;
                                                                    }
                                                                    else
                                                                    {
                                                                        newMonth = false;
                                                                        weekinMonth++;
                                                                        double temp;
                                                                        tempValue += Double.TryParse(progressPlanedWeekList[countWeek], out temp) ? Double.Parse(progressPlanedWeekList[countWeek]) : 0;
                                                                    }
                                                                    if (newMonth)
                                                                    {
                                                                        progressPlanedMonthList.Add((tempValue / weekinMonth).ToString());
                                                                    }
                                                                    else
                                                                    {
                                                                        progressPlanedMonthList[countMonth - 1] = (tempValue / weekinMonth).ToString();
                                                                    }
                                                                    countWeek++;
                                                                }
                                                            }
                                                            var existProgressPlaned =
                                                                this.processPlanedService.GetByProjectAndWorkgroup(
                                                                    projectObj.ID, wpObj.ID);

                                                            if (existProgressPlaned == null)
                                                            {
                                                                var progressPlaned = new ProcessPlaned();
                                                                progressPlaned.ProjectId = wpObj.ProjectId;
                                                                progressPlaned.WorkgroupId = wpObj.ID;
                                                                progressPlaned.Planed = string.Join("$", progressPlanedWeekList);
                                                                progressPlaned.PlanedMonth = string.Join("$", progressPlanedMonthList);

                                                                this.processPlanedService.Insert(progressPlaned);
                                                            }
                                                            else
                                                            {
                                                                existProgressPlaned.Planed = string.Join("$", progressPlanedWeekList);
                                                                existProgressPlaned.PlanedMonth = string.Join("$", progressPlanedMonthList);
                                                                this.processPlanedService.Update(existProgressPlaned);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            progressActualWeekList = new List<string>();
                                                            progressActualMonthList = new List<string>();
                                                            for (int i = 0; i < numberofWeek; i++)
                                                            {
                                                                var value =
                                                                    !string.IsNullOrEmpty(
                                                                        dataRow["Column" + (i + 4)].ToString())
                                                                        ? Math.Round(Convert.ToDouble(
                                                                            dataRow["Column" + (i + 4)].ToString()), 4) * 100
                                                                        : 0;
                                                                progressActualWeekList.Add(value.ToString());
                                                            }
                                                            //var flag = 0;
                                                            //var tempValue = 0.0;
                                                            //for (int i = 0; i < progressActualWeekList.Count; i++)
                                                            //{
                                                            //    if (flag < 4)
                                                            //    {
                                                            //        tempValue += Double.Parse(progressActualWeekList[i]);
                                                            //    }
                                                            //    flag++;
                                                            //    if (flag == 4)
                                                            //    {
                                                            //        progressActualMonthList.Add((tempValue / flag).ToString());
                                                            //        flag = 0;
                                                            //        tempValue = 0.0;
                                                            //    }
                                                            //}
                                                            currentMonth = 0;
                                                            var weekinMonth = 0;
                                                            var countWeek = 0;
                                                            var countMonth = 0;
                                                            var tempValue = 0.0;
                                                            var newMonth = false;
                                                            for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                                                        j < projectObj.Deadline.GetValueOrDefault();
                                                                        j = j.AddDays(7))
                                                            {
                                                                if (progressActualWeekList.Count > countWeek)
                                                                {
                                                                    var months = j.Month;
                                                                    if (currentMonth != months)
                                                                    {
                                                                        //if (countMonth != 0)
                                                                        //{
                                                                        //    actualMonthList.Add(tempValue / weekinMonth);
                                                                        //}
                                                                        double temp;
                                                                        tempValue = Double.TryParse(progressActualWeekList[countWeek], out temp) ? Double.Parse(progressActualWeekList[countWeek]) : 0;
                                                                        weekinMonth = 1;
                                                                        countMonth++;
                                                                        currentMonth = j.Month;
                                                                        newMonth = true;
                                                                    }
                                                                    else
                                                                    {
                                                                        newMonth = false;
                                                                        weekinMonth++;
                                                                        double temp;
                                                                        tempValue += Double.TryParse(progressActualWeekList[countWeek], out temp) ? Double.Parse(progressActualWeekList[countWeek]) : 0;
                                                                    }
                                                                    if (newMonth)
                                                                    {
                                                                        progressActualMonthList.Add((tempValue / weekinMonth).ToString());
                                                                    }
                                                                    else
                                                                    {
                                                                        progressActualMonthList[countMonth - 1] = (tempValue / weekinMonth).ToString();
                                                                    }
                                                                    countWeek++;
                                                                }
                                                            }
                                                            var existProgressActual =
                                                                this.processActualService.GetByProjectAndWorkgroup(
                                                                    projectObj.ID, wpObj.ID);

                                                            if (existProgressActual == null)
                                                            {
                                                                var progressActual = new ProcessActual();
                                                                progressActual.ProjectId = wpObj.ProjectId;
                                                                progressActual.WorkgroupId = wpObj.ID;
                                                                progressActual.Actual = string.Join("$", progressActualWeekList);
                                                                progressActual.ActualMonth = string.Join("$", progressActualMonthList);

                                                                this.processActualService.Insert(progressActual);
                                                            }
                                                            else
                                                            {
                                                                existProgressActual.Actual = string.Join("$", progressActualWeekList);
                                                                existProgressActual.ActualMonth = string.Join("$", progressActualMonthList);

                                                                this.processActualService.Update(existProgressActual);
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            dataTable = wsProgress.Cells.ExportDataTable(7, 0,
                                                wsProgress.Cells.MaxRow - 7, numberofMonth + 3);

                                            foreach (DataRow dataRow in dataTable.Rows)
                                            {
                                                var workgroupId = dataRow["Column1"].ToString();
                                                if (!string.IsNullOrEmpty(workgroupId))
                                                {
                                                    var wpObj =
                                                        this.workGroupService.GetById(Convert.ToInt32(workgroupId));
                                                    if (wpObj != null)
                                                    {
                                                        if (progressType == "Planed")
                                                        {
                                                            progressPlanedMonthList = new List<string>();
                                                            for (int i = 0; i < numberofMonth; i++)
                                                            {
                                                                var value =
                                                                    !string.IsNullOrEmpty(
                                                                        dataRow["Column" + (i + 4)].ToString())
                                                                        ? Math.Round(Convert.ToDouble(
                                                                            dataRow["Column" + (i + 4)].ToString()), 4) * 100
                                                                        : 0;
                                                                progressPlanedMonthList.Add(value.ToString());
                                                            }

                                                            #region Plan Month
                                                            currentMonth = 0;
                                                            var countWeek = 0;
                                                            var countMonth = 0;
                                                            List<int> weekinMonth = new List<int>();
                                                            List<MonthWeekProgress> monthweekList = new List<MonthWeekProgress>();
                                                            progressPlanedWeekList = new List<string>();
                                                            for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                                                        j < projectObj.Deadline.GetValueOrDefault();
                                                                        j = j.AddDays(7))
                                                            {
                                                                progressPlanedWeekList.Add("0");
                                                                int monthTemp = j.Month;
                                                                if (currentMonth != monthTemp)
                                                                {
                                                                    MonthWeekProgress progress = new MonthWeekProgress();
                                                                    progress.Month = countMonth;
                                                                    monthweekList.Add(progress);
                                                                    countMonth++;
                                                                    currentMonth = j.Month;
                                                                    weekinMonth.Clear();
                                                                    weekinMonth.Add(countWeek);
                                                                    if (currentMonth != j.AddDays(7).Month || j.AddDays(7) > projectObj.Deadline.GetValueOrDefault())
                                                                    {
                                                                        monthweekList[countMonth - 1].Week = string.Join("-", weekinMonth);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    weekinMonth.Add(countWeek);
                                                                    monthweekList[countMonth - 1].Week = string.Join("-", weekinMonth);
                                                                }
                                                                countWeek++;
                                                            }
                                                            double temp;
                                                            var progressPlanedMonthListDouble = progressPlanedMonthList.Select(t => double.TryParse(t, out temp) ? double.Parse(t) : 0).ToList();
                                                            foreach (var item in monthweekList)
                                                            {
                                                                var monthValue = 0.0;
                                                                if (item.Month == 0)
                                                                {
                                                                    monthValue = progressPlanedMonthListDouble[item.Month];
                                                                }
                                                                else
                                                                {
                                                                    monthValue = progressPlanedMonthListDouble[item.Month] - progressPlanedMonthListDouble[item.Month - 1];
                                                                }
                                                                weekinMonth = new List<int>();
                                                                weekinMonth = item.Week.Split('-').Select(int.Parse).ToList();
                                                                foreach (var week in weekinMonth)
                                                                {
                                                                    if (week != 0)
                                                                    {
                                                                        progressPlanedWeekList[week] = Math.Round((double.Parse(progressPlanedWeekList[week - 1]) + monthValue / weekinMonth.Count()), 2).ToString();
                                                                    }
                                                                    else
                                                                    {
                                                                        progressPlanedWeekList[week] = Math.Round((monthValue / weekinMonth.Count()), 2).ToString();
                                                                    }
                                                                }
                                                            }
                                                            var existProgressPlaned =
                                                                this.processPlanedService.GetByProjectAndWorkgroup(
                                                                    projectObj.ID, wpObj.ID);

                                                            if (existProgressPlaned == null)
                                                            {
                                                                var progressPlaned = new ProcessPlaned();
                                                                progressPlaned.ProjectId = wpObj.ProjectId;
                                                                progressPlaned.WorkgroupId = wpObj.ID;
                                                                progressPlaned.Planed = string.Join("$", progressPlanedWeekList);
                                                                progressPlaned.PlanedMonth = string.Join("$", progressPlanedMonthList);

                                                                this.processPlanedService.Insert(progressPlaned);
                                                            }
                                                            else
                                                            {
                                                                existProgressPlaned.Planed = string.Join("$", progressPlanedWeekList);
                                                                existProgressPlaned.PlanedMonth = string.Join("$", progressPlanedMonthList);
                                                                this.processPlanedService.Update(existProgressPlaned);
                                                            }

                                                            int countMonthPlan = 0;
                                                            foreach (var progressItem in progressPlanedMonthList)
                                                            {
                                                                var date = projectObj.StartDate.GetValueOrDefault();
                                                                var month = date.AddMonths(countMonthPlan).Month;
                                                                var year = date.AddMonths(countMonthPlan).Year;
                                                                int prevMonth = date.AddMonths(-1).Month;
                                                                int prevYear = date.AddMonths(-1).Year;
                                                                var wmmObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), month, year);
                                                                var prevWMMObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), prevMonth, prevYear);
                                                                if (wmmObj != null)
                                                                {
                                                                    wmmObj.Month = month;
                                                                    wmmObj.Year = year;
                                                                    wmmObj.WorkgroupID = wpObj.ID;
                                                                    wmmObj.WorkgroupName = wpObj.Name;
                                                                    wmmObj.ProjectID = projectObj.ID;
                                                                    wmmObj.DeparmentID = wpObj.DepartmentId;
                                                                    wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                                                                    wmmObj.CompletePlanTotal = Convert.ToDouble(progressItem);
                                                                    if (prevWMMObj != null)
                                                                    {
                                                                        wmmObj.CompletePlanMonth = wmmObj.CompletePlanTotal.GetValueOrDefault() - prevWMMObj.CompletePlanTotal.GetValueOrDefault();
                                                                    }
                                                                    else
                                                                    {
                                                                        wmmObj.CompletePlanMonth = wmmObj.CompletePlanTotal.GetValueOrDefault();
                                                                    }
                                                                    wmmObj.UpdateBy = UserSession.Current.User.Id;
                                                                    wmmObj.UpdateDate = DateTime.Now;
                                                                    this.workgroupManhourMonthService.Update(wmmObj);
                                                                }
                                                                else
                                                                {
                                                                    WorkgroupManhourMonth newWMMObj = new WorkgroupManhourMonth();
                                                                    newWMMObj.Month = month;
                                                                    newWMMObj.Year = year;
                                                                    newWMMObj.WorkgroupID = wpObj.ID;
                                                                    newWMMObj.WorkgroupName = wpObj.Name;
                                                                    newWMMObj.DeparmentID = wpObj.DepartmentId;
                                                                    newWMMObj.ProjectID = wpObj.ProjectId.GetValueOrDefault();
                                                                    newWMMObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                                                                    newWMMObj.CompletePlanTotal = Convert.ToDouble(progressItem);
                                                                    if (prevWMMObj != null)
                                                                    {
                                                                        newWMMObj.CompletePlanMonth = newWMMObj.CompletePlanTotal.GetValueOrDefault() - prevWMMObj.CompletePlanTotal.GetValueOrDefault();
                                                                    }
                                                                    else
                                                                    {
                                                                        newWMMObj.CompletePlanMonth = newWMMObj.CompletePlanTotal.GetValueOrDefault();
                                                                    }
                                                                    newWMMObj.CreateBy = UserSession.Current.User.Id;
                                                                    newWMMObj.CreateDate = DateTime.Now;
                                                                    this.workgroupManhourMonthService.Insert(newWMMObj);
                                                                }
                                                                countMonthPlan++;
                                                            }
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            #region Actual Month
                                                            progressActualMonthList = new List<string>();
                                                            for (int i = 0; i < numberofMonth; i++)
                                                            {
                                                                var value =
                                                                    !string.IsNullOrEmpty(
                                                                        dataRow["Column" + (i + 4)].ToString())
                                                                        ? Math.Round(Convert.ToDouble(
                                                                            dataRow["Column" + (i + 4)].ToString()), 4) * 100
                                                                        : 0;
                                                                progressActualMonthList.Add(value.ToString());
                                                            }
                                                            currentMonth = 0;
                                                            var countWeek = 0;
                                                            var countMonth = 0;
                                                            List<int> weekinMonth = new List<int>();
                                                            List<MonthWeekProgress> monthweekList = new List<MonthWeekProgress>();
                                                            progressActualWeekList = new List<string>();
                                                            for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                                                        j < projectObj.Deadline.GetValueOrDefault();
                                                                        j = j.AddDays(7))
                                                            {
                                                                progressActualWeekList.Add("0");
                                                                if (currentMonth != j.Month)
                                                                {
                                                                    MonthWeekProgress progress = new MonthWeekProgress();
                                                                    progress.Month = countMonth;
                                                                    monthweekList.Add(progress);
                                                                    countMonth++;
                                                                    currentMonth = j.Month;
                                                                    weekinMonth.Clear();
                                                                    weekinMonth.Add(countWeek);
                                                                    if (currentMonth != j.AddDays(7).Month || j.AddDays(7) > projectObj.Deadline.GetValueOrDefault())
                                                                    {
                                                                        monthweekList[countMonth - 1].Week = string.Join("-", weekinMonth);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    weekinMonth.Add(countWeek);
                                                                    monthweekList[countMonth - 1].Week = string.Join("-", weekinMonth);
                                                                }
                                                                countWeek++;
                                                            }
                                                            double temp;
                                                            var progressActualMonthListDouble = progressActualMonthList.Select(t => double.TryParse(t, out temp) ? double.Parse(t) : 0).ToList();
                                                            foreach (var item in monthweekList)
                                                            {
                                                                var monthValue = 0.0;
                                                                if (item.Month == 0)
                                                                {
                                                                    monthValue = progressActualMonthListDouble[item.Month];
                                                                }
                                                                else
                                                                {
                                                                    monthValue = progressActualMonthListDouble[item.Month] - progressActualMonthListDouble[item.Month - 1];
                                                                }
                                                                weekinMonth = new List<int>();
                                                                weekinMonth = item.Week.Split('-').Select(int.Parse).ToList();
                                                                foreach (var week in weekinMonth)
                                                                {
                                                                    if (week != 0)
                                                                    {
                                                                        progressActualWeekList[week] = Math.Round((double.Parse(progressActualWeekList[week - 1]) + monthValue / weekinMonth.Count()), 2).ToString();
                                                                    }
                                                                    else
                                                                    {
                                                                        progressActualWeekList[week] = Math.Round((monthValue / weekinMonth.Count()), 2).ToString();
                                                                    }
                                                                }
                                                            }
                                                            var existProgressActual =
                                                                this.processActualService.GetByProjectAndWorkgroup(
                                                                    projectObj.ID, wpObj.ID);

                                                            if (existProgressActual == null)
                                                            {
                                                                var progressActual = new ProcessActual();
                                                                progressActual.ProjectId = wpObj.ProjectId;
                                                                progressActual.WorkgroupId = wpObj.ID;
                                                                progressActual.Actual = string.Join("$", progressActualWeekList);
                                                                progressActual.ActualMonth = string.Join("$", progressActualMonthList);

                                                                this.processActualService.Insert(progressActual);
                                                            }
                                                            else
                                                            {
                                                                existProgressActual.Actual = string.Join("$", progressActualWeekList);
                                                                existProgressActual.ActualMonth = string.Join("$", progressActualMonthList);

                                                                this.processActualService.Update(existProgressActual);
                                                            }
                                                        }

                                                        var countMonthActual = 0;
                                                        foreach (var progressItem in progressActualMonthList)
                                                        {
                                                            var date = projectObj.StartDate.GetValueOrDefault();
                                                            var month = date.AddMonths(countMonthActual).Month;
                                                            var year = date.AddMonths(countMonthActual).Year;
                                                            int prevMonth = date.AddMonths(-1).Month;
                                                            int prevYear = date.AddMonths(-1).Year;
                                                            var wmmObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), month, year);
                                                            var prevWMMObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), prevMonth, prevYear);
                                                            if (wmmObj != null)
                                                            {
                                                                wmmObj.Month = month;
                                                                wmmObj.Year = year;
                                                                wmmObj.WorkgroupID = wpObj.ID;
                                                                wmmObj.WorkgroupName = wpObj.Name;
                                                                wmmObj.ProjectID = projectObj.ID;
                                                                wmmObj.DeparmentID = wpObj.DepartmentId;
                                                                wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                                                                wmmObj.CompleteActualTotal = Convert.ToDouble(progressItem);
                                                                if (prevWMMObj != null)
                                                                {
                                                                    wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompletePlanTotal.GetValueOrDefault();
                                                                }
                                                                else
                                                                {
                                                                    wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                                                                }
                                                                wmmObj.UpdateBy = UserSession.Current.User.Id;
                                                                wmmObj.UpdateDate = DateTime.Now;
                                                                this.workgroupManhourMonthService.Update(wmmObj);
                                                            }
                                                            else
                                                            {
                                                                WorkgroupManhourMonth newWMMObj = new WorkgroupManhourMonth();
                                                                newWMMObj.Month = month;
                                                                newWMMObj.Year = year;
                                                                newWMMObj.WorkgroupID = wpObj.ID;
                                                                newWMMObj.WorkgroupName = wpObj.Name;
                                                                newWMMObj.DeparmentID = wpObj.DepartmentId;
                                                                newWMMObj.ProjectID = wpObj.ProjectId.GetValueOrDefault();
                                                                newWMMObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                                                                newWMMObj.CompleteActualTotal = Convert.ToDouble(progressItem);
                                                                if (prevWMMObj != null)
                                                                {
                                                                    newWMMObj.CompleteActualMonth = newWMMObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompletePlanTotal.GetValueOrDefault();
                                                                }
                                                                else
                                                                {
                                                                    newWMMObj.CompleteActualMonth = newWMMObj.CompleteActualTotal.GetValueOrDefault();
                                                                }
                                                                newWMMObj.CreateBy = UserSession.Current.User.Id;
                                                                newWMMObj.CreateDate = DateTime.Now;
                                                                this.workgroupManhourMonthService.Insert(newWMMObj);
                                                            }
                                                            countMonthActual++;
                                                        }
                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        this.blockError.Visible = true;
                                        this.lblError.Text = "This file is invalid";
                                    }


                                }
                            }
                        }

                        break;
                }
            }
            //}
            //catch (Exception ex)
            //{
            //    this.blockError.Visible = true;
            //    this.lblError.Text = "Have error when import Progress: <br/>'" + ex.Message + "'";
            //}

            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc)
        {
            var listUpload = uploadDocControl.UploadedFiles;
            if (listUpload.Count > 0)
            {
                foreach (UploadedFile docFile in listUpload)
                {
                    var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    docFile.SaveAs(revisionFilePath, true);
                }
            }
        }
    }
}
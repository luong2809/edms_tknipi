﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Customers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Business.Services;
    using EDMs.Web.Utilities.Sessions;
    using Telerik.Web.UI;
    using EDMs.Data.Entities;
    
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class Detail_UsedService_Edit : Page
    {
        /// <summary>
        /// The patient status service
        /// </summary>
        private Patient_UsedServiceService _patientUsedServiceService;
        private ServiceTypeService _serviceTypeService;

        /// <summary>
        /// The patient service
        /// </summary>
        private PatientService _patientService;
        private ResourceService _resourceService;

        public Detail_UsedService_Edit()
        {
            _serviceTypeService = new ServiceTypeService();
            _patientUsedServiceService = new Patient_UsedServiceService();
            _patientService =new PatientService();
            _resourceService = new ResourceService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindResourceType();
                BindData();
            }
        }

        private void BindResourceType()
        {
            var resources = _resourceService.FindByIsResource(true);
            cboResource.DataSource = resources;
            cboResource.DataTextField = "FullName";
            cboResource.DataValueField = "ID";

            cboResource.DataBind();

            var services = _serviceTypeService.GetAll();
            cboService.DataSource = services;
            cboService.DataTextField = "Name";
            cboService.DataValueField = "ID";

            cboService.DataBind();
        }
        private void BindData()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["cID"]))
            {
                var cID = Request.QueryString["cID"];
                if (cID != null)
                {
                    int tempcID;
                    if (int.TryParse(cID, out tempcID))
                    {
                       var customer = _patientService.GetByID(tempcID);
                       lblTenKhachHang.Text = customer.FullName;
                    }
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                var ID = Request.QueryString["ID"];
                if (ID != null)
                {
                    int tempID;
                    if (int.TryParse(ID, out tempID))
                    {
                        UsedService UsedService = _patientUsedServiceService.GetByID(tempID);
                        txtContent.Text = UsedService.Content;
                        txtDateTime.SelectedDate = UsedService.UpdatedDate;

                        cboService.SelectedValue = UsedService.ServiceTypeID.ToString();
                        cboResource.SelectedValue = UsedService.ResourceID.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                var ID = Request.QueryString["ID"];
                if (ID != null)
                {
                    int tempID;
                    if (int.TryParse(ID, out tempID))
                    {
                        //Update
                        UpdateUsedService(tempID);
                        ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
                    }
                }
            }
            else
            {
                CreateUsedService();
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }
        private void UpdateUsedService(int ID)
        {
            //Create
            UsedService cs = _patientUsedServiceService.GetByID(ID);
            cs.UsedDate = txtDateTime.SelectedDate;
            cs.UpdatedBy = UserSession.Current.User.Id;
            cs.UpdatedDate = DateTime.Now;
            cs.UserID = UserSession.Current.User.Id;
            cs.Content = txtContent.Text.Trim();
            cs.ServiceTypeID = int.Parse(cboService.SelectedValue);
            cs.ResourceID = int.Parse(cboResource.SelectedValue);

            _patientUsedServiceService.Update(cs);
        }
        private void CreateUsedService()
        {
            //Create
            UsedService cs = new UsedService();
            cs.UpdatedBy = UserSession.Current.User.Id;
            cs.UpdatedDate = DateTime.Now;
            cs.UsedDate = txtDateTime.SelectedDate;
            cs.UserID = UserSession.Current.User.Id;
            cs.CustomerID = Request.QueryString["cID"] == null ? 0 : int.Parse(Request.QueryString["cID"].ToString());
            cs.Content = txtContent.Text.Trim();
            cs.ServiceTypeID = int.Parse(cboService.SelectedValue);
            cs.ResourceID = int.Parse(cboResource.SelectedValue);

            _patientUsedServiceService.Insert(cs);
        }
        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }
        
        private static void ShowCheckedItems(RadListBox listBox, Label label)
        {
            StringBuilder sb = new StringBuilder();
            IList<RadListBoxItem> collection = listBox.CheckedItems;
            foreach (RadListBoxItem item in collection)
            {
                sb.Append(item.Value + "<br />");
            }

            label.Text = sb.ToString();

        }
    }
}
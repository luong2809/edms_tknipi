﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Detail_Member.aspx.cs" Inherits="EDMs.Web.Controls.Customers.Detail_Member" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Liên hệ</title>
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <script src="../../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        function RowDblClick(sender, eventArgs) {
            window.parent.radopen("Controls/Customers/Details/Detail_Member_Edit.aspx?ID=" + eventArgs.getDataKeyValue("ID"), "CustomerDialog");
        }
        $(document).ready(function () {
            $(".content").fadeIn("slow");
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content" style="display: none;">
            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
            <telerik:RadGrid ID="grdMemberCard" runat="server"
                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                Width="100%" Skin="Windows7" ShowFooter="true"
                OnItemCreated="grdMemberCard_ItemCreated"
                OnNeedDataSource="grdMemberCard_NeedDataSource"
                OnDeleteCommand="grdMemberCard_DeleteCommand">
                <MasterTableView ClientDataKeyNames="ID,CustomerID" DataKeyNames="ID">
                    <NoRecordsTemplate>
                        Chưa có dữ liệu.
                    </NoRecordsTemplate>
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="Trang đầu" LastPageToolTip="Trang cuối" NextPagesToolTip="Trang sau" NextPageToolTip="Trang sau" PagerTextFormat="Đổi trang: {4} &amp;nbsp;Trang &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Tổng:  &lt;strong&gt;{5}&lt;/strong&gt; dòng." PageSizeLabelText="Số dòng trên mỗi trang: " PrevPagesToolTip="Trang trước" PrevPageToolTip="Trang trước" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="STT">
                            <HeaderStyle HorizontalAlign="Center" Width="30" />
                            <ItemStyle Font-Bold="True" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblSTT" runat="server" align="center" Enabled="True" Text="<%# grdMemberCard.CurrentPageIndex*grdMemberCard.PageSize + grdMemberCard.Items.Count+1 %>" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="ID" HeaderText="ID" UniqueName="ID" Visible="False" />
                        <telerik:GridBoundColumn DataField="Name" HeaderText="Tên thẻ" UniqueName="Name" Aggregate="Count" FooterText="Tổng số thẻ: ">
                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                        </telerik:GridBoundColumn>
                        <%--<telerik:GridBoundColumn DataField="Description" HeaderText="Ghi chú" UniqueName="Description">
                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                    </telerik:GridBoundColumn>--%>
                        <telerik:GridBoundColumn DataField="UserFullName" HeaderText="Nhân viên cấp" UniqueName="UserFullName">
                            <HeaderStyle HorizontalAlign="Center" Width="120" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <%--<telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Ngày cấp thẻ" UniqueName="CreatedDate">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridTemplateColumn HeaderText="Ngày cấp thẻ" UniqueName="CreatedDate">
                            <HeaderStyle Width="80" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedDate" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}", DataBinder.Eval(Container.DataItem, "CreatedDate"))  %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="DiscountRate" HeaderText="Chiết khấu" UniqueName="DiscountRate">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <%--<telerik:GridBoundColumn DataField="EndDate" HeaderText="Sử dụng đến" UniqueName="EndDate">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridTemplateColumn HeaderText="Sử dụng đến" UniqueName="EndDate">
                            <HeaderStyle Width="80" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblEndDate" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}", DataBinder.Eval(Container.DataItem, "EndDate"))  %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Sửa">
                            <HeaderStyle Width="30" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer;" AlternateText="Chỉnh sửa" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn HeaderText="Xóa" CommandName="Delete" Text="Xóa" ConfirmText="Bạn có muốn xóa thẻ thành viên này?">
                            <HeaderStyle Width="30px" />
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <ClientEvents OnRowDblClick="RowDblClick" />
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="200" UseStaticHeaders="True" />
                </ClientSettings>
            </telerik:RadGrid>
        </div>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
                function RowDblClick(sender, eventArgs) {
                    var ID = eventArgs.getDataKeyValue("ID");
                    var cID = eventArgs.getDataKeyValue("CustomerID");
                    window.parent.radopen("Controls/Customers/Details/Detail_Member_Edit.aspx?ID=" + ID + "&cID=" + cID, "CustomerDialog");
                }

                function ShowEditForm(id, rowIndex) {
                    var grid = $find('<%= grdMemberCard.ClientID %>');

                    var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                    var ID = grid.get_masterTableView().get_dataItems()[rowIndex].getDataKeyValue("ID");
                    var cID = grid.get_masterTableView().get_dataItems()[rowIndex].getDataKeyValue("CustomerID");
                    grid.get_masterTableView().selectItem(rowControl, true);

                    window.parent.radopen("Controls/Customers/Details/Detail_Member_Edit.aspx?ID=" + ID + "&cID=" + cID, "CustomerDialog");
                    return false;
                }
            </script>
        </telerik:RadCodeBlock>
    </form>
</body>
</html>

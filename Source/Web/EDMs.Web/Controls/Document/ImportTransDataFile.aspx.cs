﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web.Hosting;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Web.UI;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;
    using EDMs.Business.Services.Scope;
    using EDMs.Web.Utilities;
    using Telerik.Web.UI;
    using Aspose.Cells;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportTransDataFile : Page
    {
        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly ToListService toListService;

        private readonly TransmittalService transmittalService;
        private readonly ContractorService contractorService;
        private readonly RoleService rolesevice;
        private readonly UserService userService;
        private readonly AttachDocToTransmittalService attachDocToTransmittalService;
        private readonly DocumentPackageService documentPackageService;
        private readonly ScopeProjectService scopeProjectService;
       // private readonly UserDataPermissionService userDataPermissionService;
       // private readonly FolderService folderService;
        private readonly AttachFilesPackageService attachFilesPackageService;
        private readonly AttachCommentFileService attachcommentFileService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportTransDataFile()
        {
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.toListService = new ToListService();
            this.transmittalService = new TransmittalService();
            this.userService = new UserService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.scopeProjectService = new ScopeProjectService();
           // this.folderService = new FolderService();
           // this.userDataPermissionService = new UserDataPermissionService();
            this.documentPackageService = new DocumentPackageService();
            this.documentService = new DocumentService();
            this.rolesevice = new RoleService();
            this.contractorService = new ContractorService();
            this.attachcommentFileService = new AttachCommentFileService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.LoadComboData();
               // var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));

            }
        }

        private void LoadComboData()
        {


            var projectList = this.scopeProjectService.GetAll();
            this.ddlProject.DataSource = projectList;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();
            var grouplist = this.rolesevice.GetAll(false).OrderBy(t => t.FullName).ToList();

            this.ddlGroupFrom.DataSource = grouplist;
            this.ddlGroupFrom.DataTextField = "Name";
            this.ddlGroupFrom.DataValueField = "ID";
            this.ddlGroupFrom.DataBind();
            this.ddlGroupFrom.SelectedIndex = 0;

            this.ddlGroupTo.DataSource = grouplist;
            this.ddlGroupTo.DataTextField = "Name";
            this.ddlGroupTo.DataValueField = "ID";
            this.ddlGroupTo.DataBind();
            this.ddlGroupTo.SelectedIndex = 0;



            var userListTo = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroupTo.SelectedValue)).OrderBy(t => t.FullName).ToList();
            this.ddlToList.DataSource = userListTo;
            this.ddlToList.DataTextField = "FullName";
            this.ddlToList.DataValueField = "Id";
            this.ddlToList.DataBind();


            var userListFrom = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroupFrom.SelectedValue)).OrderBy(t => t.FullName).ToList();
            this.ddlFromList.DataSource = userListFrom;
            this.ddlFromList.DataTextField = "FullName";
            this.ddlFromList.DataValueField = "Id";
            this.ddlFromList.DataBind();

            var contractorList = this.contractorService.GetAll();
            this.ddlContractor.DataSource = contractorList;
            this.ddlContractor.DataTextField = "Name";
            this.ddlContractor.DataValueField = "ID";
            this.ddlContractor.DataBind();
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var docNonExistList = new List<string>();
                var splitAttachFileNameCharacter =
                                       ConfigurationManager.AppSettings.Get("SplitAttachFileNameCharacter");
                var fileIcon = new Dictionary<string, string>()
                {
                    {"doc", "~/images/wordfile.png"},
                    {"docx", "~/images/wordfile.png"},
                    {"dotx", "~/images/wordfile.png"},
                    {"xls", "~/images/excelfile.png"},
                    {"xlsx", "~/images/excelfile.png"},
                    {"pdf", "~/images/pdffile.png"},
                    {"7z", "~/images/7z.png"},
                    {"dwg", "~/images/dwg.png"},
                    {"dxf", "~/images/dxf.png"},
                    {"rar", "~/images/rar.png"},
                    {"zip", "~/images/zip.png"},
                    {"txt", "~/images/txt.png"},
                    {"xml", "~/images/xml.png"},
                    {"xlsm", "~/images/excelfile.png"},
                    {"bmp", "~/images/bmp.png"},
                };

                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var transFileName = docFile.FileName;
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                      //  var owner = this.toListService.GetAll().FirstOrDefault(t => t.Name == "XNXL");

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var tranSheet = workbook.Worksheets[0];
                        var projectID = this.ddlProject.SelectedItem != null
                                ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                : 0;
                        var projectObj = this.scopeProjectService.GetById(projectID);
                        var listSelectedDocId = new List<int>();
                        var listAttachFilePackage = new List<AttachFilesPackage>();
                        var doclist = new List<DocumentPackage>();
                        if (projectObj != null)
                        {

                            // Create incoming Trans Info
                          
                            //Get the second textbox object.
                           // Aspose.Cells.Drawing.TextBox textbox1 = tranSheet.TextBoxes[1];

                            //Obtain the text in the second textbox.
                            string text1 = tranSheet.Cells["J11"].Value.ToString();

                            var date = new DateTime();
                            var datestring = tranSheet.Cells["J3"].Value.ToString().Trim();
                            Utility.ConvertStringToDateTime(datestring, ref date);

                            var toId = Convert.ToInt32(this.ddlToList.SelectedValue);
                            var fromId = Convert.ToInt32(this.ddlFromList.SelectedValue);

                            var toObj = this.userService.GetByID(toId);
                            var fromObj = this.userService.GetByID(fromId);
                            var objTran = new Transmittal()
                            {
                                TransmittalNumber = text1,
                                ProjectName = this.ddlProject.SelectedItem.Text,
                                ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                                ToId = toObj != null ? toObj.Id : 0,
                                ToList = toObj != null
                                    ? "<span style='color: blue; font-weight: bold'>" + toObj.FullName + "</span>" + "<br/>" +
                                      toObj.Position
                                    : string.Empty,
                                FromId = fromObj != null ? fromObj.Id : 0,
                                FromList = fromObj != null ? "<span style='color: blue; font-weight: bold'>" + fromObj.FullName + "</span>" + "<br/>" + fromObj.Position : string.Empty,
                                ReceivedDate = date==null? DateTime.Now: date,
                                Type = 1,
                                ContractorId = this.ddlContractor.SelectedItem != null
                                                ? Convert.ToInt32(this.ddlContractor.SelectedValue)
                                                : 0,
                                ContractorName = this.ddlContractor.SelectedItem != null
                                                ? this.ddlContractor.SelectedItem.Text
                                                : string.Empty,

                                CreatedBy = UserSession.Current.User.Id,
                                CreatedDate = DateTime.Now,
                            };

                            var transId = this.transmittalService.Insert(objTran);
                            if (transId != null)
                            {

                                var filename = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                                //var oldfilePath = Server.MapPath(objTran.GeneratePath);
                                File.Copy(importPath, Server.MapPath("../../Transmittals/Generated/" + transFileName), true);
                                var newServerPath = "../../Transmittals/Generated/" + transFileName;
                                // var newServerPath = "../../Transmittals/Generated/" + filename;
                                //   var newFilePath = Server.MapPath("../../Transmittals/Generated/") + filename;

                                //   docFile.SaveAs(newFilePath);
                                objTran.IsGenerate = true;
                                objTran.GeneratePath = newServerPath;

                                this.transmittalService.Update(objTran);

                            }


                            // Create attach doc to trans info
                            if (objTran != null)
                            {
                                //var dataTable = tranSheet.Cells.ExportDataTable(13, 1, tranSheet.Cells.MaxDataRow, 4);

                               // foreach (DataRow dataRow in dataTable.Rows)
                              //  {
                                var i=14; 
                                for(i=14;i<tranSheet.Cells.MaxDataRow;i++){
                                    if (tranSheet.Cells["B" + i].Value != null && !string.IsNullOrEmpty(tranSheet.Cells["B" + i].Value.ToString())  )
                                    {
                                    var revName = tranSheet.Cells["G"+i].Value.ToString();
                                    var docNo = tranSheet.Cells["B"+i].Value.ToString();
                                    var Code = tranSheet.Cells["N"+i].Value.ToString();
                                        var docObj = this.documentPackageService.GetOneByDocNo(docNo, revName, projectObj.ID);

                                        if (docObj != null)
                                        {
                                            listSelectedDocId.Add(docObj.ID);
                                            doclist.Add(docObj);
                                            var attachDoc = new AttachDocToTransmittal()
                                            {
                                                TransmittalId = transId,
                                                DocumentId = docObj.ID
                                            };
                                            if (!this.attachDocToTransmittalService.IsExist(transId.GetValueOrDefault(), docObj.ID))
                                            {
                                                this.attachDocToTransmittalService.Insert(attachDoc);
                                            }
                                            docObj.IncomingTransNo = objTran.TransmittalNumber;
                                            docObj.IncomingTransDate = objTran.ReceivedDate;
                                            docObj.FinalCodeName = Code;
                                            this.documentPackageService.Update(docObj);

                                        }
                                        else
                                        {
                                            docNonExistList.Add(docNo + "_" + revName);
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }


                                const string TargetFolder = "../../DocumentLibrary";
                                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary";
                                foreach (UploadedFile commentfile in this.Uploadfilecomment.UploadedFiles)
                                {
                                    var docFileName = commentfile.FileName;
                                    var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;

                                    // Path file to save on server disc
                                    var saveFilePath = Path.Combine(Server.MapPath(TargetFolder), serverDocFileName);

                                    // Path file to download from server
                                    var serverFilePath = serverFolder + "/" + serverDocFileName;
                                    var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                                    commentfile.SaveAs(saveFilePath, true);

                                    var docInfo = docFileName.Split('_').ToList();
                                    var docNo = docInfo[0];
                                    var docRev = docInfo[1];
                                    var docObj = this.documentPackageService.GetOneByDocNo(docNo, docRev, projectObj.ID);
                                    if (docObj != null)
                                    {

                                     

                                        var attachFile = new AttachCommentFile()
                                        {
                                            DocumentId = docObj.ID,
                                            Filename = docFileName,
                                            Extension = fileExt,
                                            FilePath = serverFilePath,
                                            ExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                                            FileSize = (double)docFile.ContentLength / 1024,
                                            CreatedBy = UserSession.Current.User.Id,
                                            CreatedByName = UserSession.Current.User.FullName,
                                            CreatedDate = DateTime.Now
                                        };

                                        this.attachcommentFileService.Insert(attachFile);
                                    }
                                }
                                #region hiden
                                //}
                                // Create Trans Folder on document Library
                                //var dirName = "../../DocumentLibrary/SharedDoc/" +
                                //              Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/" + RemoveAllSpecialCharacter("02. Transmittals In");
                                //if (this.rbtnVendor.Checked)
                                //{
                                //    dirName = "../../DocumentLibrary/SharedDoc/" +
                                //              Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/Vendor/" + RemoveAllSpecialCharacter("02. Transmittals In");
                                //}
                                //else if (this.rbtnreview1.Checked || this.rbtnreview2.Checked || this.rbtnreview3.Checked)
                                //{
                                //    dirName = "";
                                //}
                                //var mainTransFolder = this.folderService.GetByDirName(dirName);
                                //if (mainTransFolder != null)
                                //{
                                //    var transFolder = new Folder()
                                //    {
                                //        Name = Utility.RemoveSpecialCharacterForFolder(incomingTransObj.TransNumber),
                                //        Description = Utility.RemoveSpecialCharacterForFolder(incomingTransObj.TransNumber),
                                //        ParentID = mainTransFolder.ID,
                                //        DirName = mainTransFolder.DirName + "/" + Utility.RemoveSpecialCharacterForFolder(incomingTransObj.TransNumber),
                                //        CreatedBy = UserSession.Current.UserId,
                                //        CreatedDate = DateTime.Now,
                                //        ProjectId = mainTransFolder.ProjectId,
                                //        ProjectName = mainTransFolder.ProjectName
                                //    };

                                //    Directory.CreateDirectory(Server.MapPath(transFolder.DirName));
                                //    var transFolderId = this.folderService.Insert(transFolder);
                                //    var usersInPermissionOfParent = this.userDataPermissionService.GetAllByFolder(mainTransFolder.ID);
                                //    foreach (var parentPermission in usersInPermissionOfParent)
                                //    {
                                //        var childPermission = new UserDataPermission()
                                //        {
                                //            CategoryId = parentPermission.CategoryId,
                                //            RoleId = parentPermission.RoleId,
                                //            FolderId = transFolderId,
                                //            UserId = parentPermission.UserId,
                                //            IsFullPermission = parentPermission.IsFullPermission,
                                //            CreatedDate = DateTime.Now,
                                //            CreatedBy = UserSession.Current.User.Id
                                //        };

                                //        this.userDataPermissionService.Insert(childPermission);
                                //    }

                                //    // Update trans folder id for outgoing transmittal
                                //    incomingTransObj.FolderId = transFolderId;
                                //    this.incomingTransmittalService.Update(incomingTransObj);

                                //    foreach (var docId in listSelectedDocId)
                                //    {
                                //        listAttachFilePackage.AddRange(
                                //            this.attachFilesPackageService.GetAllDocumentFileByDocId(docId));
                                //    }

                                //    foreach (var attachFile in listAttachFilePackage)
                                //    {
                                //        if (transFolder != null && !string.IsNullOrEmpty(transFolder.DirName))
                                //        {
                                //            // Path file to save on server disc
                                //            var saveFilePath = Path.Combine(Server.MapPath(transFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                //            // Path file to download from server
                                //            var serverFilePath = transFolder.DirName + "/" + attachFile.FileName;
                                //            var fileExt = attachFile.Extension;

                                //            if (!File.Exists(saveFilePath))
                                //            {
                                //                var document = new Data.Entities.Document()
                                //                {
                                //                    Name = attachFile.FileName,
                                //                    FileExtension = fileExt,
                                //                    FileExtensionIcon = attachFile.ExtensionIcon,
                                //                    FilePath = serverFilePath,
                                //                    FolderID = transFolder.ID,
                                //                    IsLeaf = true,
                                //                    IsDelete = false,
                                //                    CreatedBy = UserSession.Current.User.Id,
                                //                    CreatedDate = DateTime.Now
                                //                };
                                //                this.documentService.Insert(document);
                                //            }

                                //            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                //            {
                                //                File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                //            }
                                //        }
                                //    }
                                //    this.NotifiListDocument(doclist, projectObj.ID, incomingTransObj, transFolder.DirName);
                                //}
#endregion
                            }
                        }
                    }
                }

                if (docNonExistList.Count > 0)
                {
                    this.blockError.Visible = true;
                    this.lblError.Text = "Transmittal object created Successfull.</br>" +
                                         "But can't find documents:</br>";
                    foreach (var docNo in docNonExistList)
                    {
                        this.lblError.Text += docNo + "</br>";
                    }

                    this.lblError.Text += "to attach to Transmittal. Please create Document first and Attach to transmittal again.";
                }
                else
                {
                    this.blockError.Visible = true;
                    this.lblError.Text = "Import transmittal file Successfull.";
                }

            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error at Transmittal Form: '" + ex.Message + "'";
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }


        private string RemoveAllSpecialCharacter(string input)
        {
            return Regex.Replace(input, @"[^0-9a-zA-Z]+", string.Empty);
        }
        private void NotifiListDocument(List<DocumentPackage> Listdoc, int projectid, IncomingTransmittal transmittal, string dername)
        {
            try
            {
                if (transmittal != null)
                {

                    // var userListid;// = this.autoDistributionMatriceService.GetAllByProject(projectid).Select(t=>(int)t.UserId).Distinct().ToList();

                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };
                    int count = 0;
                    var containtable = string.Empty;

                    var subject = "New Transmittal (#Trans#) has been created on SFP System";

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject.Replace("#Trans#", transmittal.TransNumber);
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;

                    var bodyContent = @"<div style=‘text-align: center;’> 
                                    <span class=‘Apple-tab-span’>Dear All,&nbsp;</span><br />
                                   
                                    <p style=‘text-align: center;’><strong><span style=‘font-size: 18px;’>please be informed that the following document of transmittal(#Trans#)</span></strong></p><br/><br/>

                                       <table border='1' cellspacing='0'>
                                       <tr>
                                       <th style='text-align:center; width:40px'>No.</th>
                                       <th style='text-align:center; width:330px'>Document Number</th>
                                       <th style='text-align:center; width:60px'>Revision</th>
                                       <th style='text-align:center; width:330px'>Document Title</th>
                                       <th style='text-align:center; width:330px'>Deadline Comment</th>
                                       </tr>";
                    foreach (var document in Listdoc)
                    {
                        var deadline = string.Empty;
                        count += 1;
                        deadline = document.PlanedDate != null ? document.PlanedDate.Value.ToString("dd/MM/yyyy") : "";
                        bodyContent += @"<tr>
                               <td>" + count + @"</td>
                               <td>" + document.DocNo + @"</td>
                               <td>"
                                       + document.RevisionName + @"</td>
                               <td>"
                                       + document.DocTitle + @"</td>
                               <td>"
                                       + deadline + @"</td>";

                    }
                    var st = @"\\spf.vietsov.com.vn\XNXL_Transmittal Folder" + dername.Replace("../../DocumentLibrary/SharedDoc", string.Empty);
                    bodyContent += @"</table>
                                       <br/>
                                       <span><br />
                                    &nbsp;This link to access&nbsp;:&nbsp; <a href='" + st + "'>" + st + "</a>" +
                                  @" <br/> Thanks and regards, <br/> System Admin.";
                    message.Body = bodyContent.Replace("#Trans#", transmittal.TransNumber); ;

                    //var Userlist = this.userService.GetSpecialUser(userListid).Where(t=> !string.IsNullOrEmpty(t.Email));
                    //foreach (var user in Userlist)
                    //{
                    //    try
                    //    {
                    //        message.To.Add(new MailAddress(user.Email));
                    //    }
                    //    catch { }

                    //}
                    message.To.Add(new MailAddress("duwongvanhoc@truetech.com.vn"));
                    smtpClient.Send(message);


                }
            }
            catch { }
        }

        protected void ddlGroupTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var userListTo = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroupTo.SelectedValue)).OrderBy(t => t.FullName).ToList();
            this.ddlToList.DataSource = userListTo;
            this.ddlToList.DataTextField = "FullName";
            this.ddlToList.DataValueField = "Id";
            this.ddlToList.DataBind();
        }

        protected void ddlGroupFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            var userListFrom = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroupFrom.SelectedValue)).OrderBy(t => t.FullName).ToList();

            this.ddlFromList.DataSource = userListFrom;
            this.ddlFromList.DataTextField = "FullName";
            this.ddlFromList.DataValueField = "Id";
            this.ddlFromList.DataBind();
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using Aspose.Words;
using EDMs.Business.Services.Scope;
using OfficeHelper.Utilities.Data;
using Telerik.Web.UI.Calendar;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.ServiceProcess;
    using System.Text;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using CheckBox = System.Web.UI.WebControls.CheckBox;
    using Label = System.Web.UI.WebControls.Label;
    using TextBox = System.Web.UI.WebControls.TextBox;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class MilestoneReport : Page
    {
        private readonly OptionalTypeService optionalTypeService = new OptionalTypeService();

        /// <summary>
        /// The permission service.
        /// </summary>
        private readonly PermissionService permissionService = new PermissionService();

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService = new RevisionService();

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService = new StatusService();

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService = new DisciplineService();

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService = new ReceivedFromService();

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService = new LanguageService();

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService = new FolderService();

        private readonly DocumentService documentService = new DocumentService();

        private readonly DocumentNewService documentNewService = new DocumentNewService();

        private readonly NotificationRuleService notificationRuleService = new NotificationRuleService();

        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly UserService userService = new UserService();

        private readonly AttachFileService attachFileService = new AttachFileService();

        private readonly AttachFilesPackageService attachFilesPackageService = new AttachFilesPackageService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly PackageService packageService = new PackageService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly WorkGroupService workGroupService = new WorkGroupService();

        private readonly RoleService roleService = new RoleService();

        private readonly TemplateManagementService templateManagementService = new TemplateManagementService();

        private readonly PermissionWorkgroupService permissionWorkgroupService = new PermissionWorkgroupService();

        private readonly MilestoneService milestoneService = new MilestoneService();

        protected const string ServiceName = "EDMSFolderWatcher";

        public static RadTreeNode editedNode = null;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

        /// <summary>
        /// The list folder id.
        /// </summary>
        private List<int> listFolderId = new List<int>();

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {
                this.txtFromMonthYear.SelectedDate = DateTime.Now;
                this.txtToMonthYear.SelectedDate = DateTime.Now;
                this.LoadObjectTree();
                Session.Add("IsListAll", false);
                if (UserSession.Current.IsEngineer)
                {
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
                    this.grdDocument.AllowMultiRowSelection = false;
                }

                if (UserSession.Current.User.Role.IsAdmin.GetValueOrDefault())
                {
                    this.IsFullPermission.Value = "true";
                }
            }
        }

        /// <summary>
        /// The rad tree view 1_ node click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void radTreeFolder_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            var folder = this.folderService.GetById(Convert.ToInt32(e.Node.Value));
            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(true, isListAll);
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(bool isbind = false, bool isListAll = false)
        {
            var milestoneList = new List<Milestone>();

            if (this.ddlDepartment.SelectedItem != null)
            {
                var fromMonth = this.txtFromMonthYear.SelectedDate != null
                    ? this.txtFromMonthYear.SelectedDate.Value.Month
                    : DateTime.Now.Month;
                var fromYear = this.txtFromMonthYear.SelectedDate != null
                   ? this.txtFromMonthYear.SelectedDate.Value.Year
                   : DateTime.Now.Year;
                var fromDate = new DateTime(fromYear, fromMonth, 1);

                var toMonth = this.txtToMonthYear.SelectedDate != null
                    ? this.txtToMonthYear.SelectedDate.Value.AddMonths(1).Month
                    : DateTime.Now.Month;
                var toYear = this.txtToMonthYear.SelectedDate != null
                   ? this.txtToMonthYear.SelectedDate.Value.AddMonths(1).Year
                   : DateTime.Now.Year;
                var toDate = new DateTime(toYear, toMonth, 1);


                if (this.ddlDepartment.SelectedValue == "0")
                {
                    var departmentIds = this.ddlDepartment.Items.Select(t => Convert.ToInt32(t.Value)).ToList();
                    var wpIds = this.workGroupService.GetAllByDepartment(departmentIds).Select(t => t.ID).ToList();

                    milestoneList = this.milestoneService.GetAllByWorkpackage(wpIds, fromDate, toDate);
                }
                else
                {
                    var wpIds = this.workGroupService.GetAllByDepartment(Convert.ToInt32(this.ddlDepartment.SelectedValue)).Select(t => t.ID).ToList();

                    milestoneList = this.milestoneService.GetAllByWorkpackage(wpIds, fromDate, toDate);
                }
            }

            this.grdDocument.DataSource = milestoneList;
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            var month = this.txtFromMonthYear.SelectedDate != null
                                ? this.txtFromMonthYear.SelectedDate.Value.Month
                                : DateTime.Now.Month;
            var year = this.txtFromMonthYear.SelectedDate != null
               ? this.txtFromMonthYear.SelectedDate.Value.Year
               : DateTime.Now.Year;

            var nextMonth = this.txtFromMonthYear.SelectedDate != null
               ? this.txtFromMonthYear.SelectedDate.Value.AddMonths(1).Month
               : DateTime.Now.AddMonths(1).Month;

            var nextYear = this.txtFromMonthYear.SelectedDate != null
               ? this.txtFromMonthYear.SelectedDate.Value.AddMonths(1).Year
               : DateTime.Now.AddMonths(1).Year;

            if (e.Argument == "Rebind")
            {
                this.grdDocument.Rebind();
            }
            /*

            else if (e.Argument == "MonthReport")
            {
                var milestoneListOfMonth = new List<Milestone>();
                if (this.ddlDepartment.SelectedItem != null )
                {
                    milestoneListOfMonth = this.ddlDepartment.SelectedValue == "0" 
                        ? this.milestoneService.GetAllByMonth(month, year)
                        : this.milestoneService.GetAllByMonth(month, year, Convert.ToInt32(this.ddlDepartment.SelectedValue));
                }

                if (milestoneListOfMonth != null && milestoneListOfMonth.Count > 0)
                {
                    var workpackageList = milestoneListOfMonth
                                        .Select(t => t.WorkpackageId)
                                        .Distinct()
                                        .Select(t => this.workGroupService.GetById(t.GetValueOrDefault()));

                    var wpPlannedList = workpackageList.Where(t => t.TypeId == 1).ToList();
                    var wpUnPlannedList = workpackageList.Where(t => t.TypeId == 2).ToList();
                    var wpOtherList = workpackageList.Where(t => t.TypeId == 0).ToList();
                    var wpCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("Text1", Type.GetType("System.String")),
                                new DataColumn("Plan", Type.GetType("System.String")),
                                new DataColumn("PlanTotal", Type.GetType("System.String")),
                                new DataColumn("EndDate", Type.GetType("System.String")),
                                new DataColumn("OutgoingNumber", Type.GetType("System.String")),
                                new DataColumn("Real", Type.GetType("System.String")),
                                new DataColumn("RealTotal", Type.GetType("System.String")),
                                new DataColumn("RealPerPlan", Type.GetType("System.String")),
                                new DataColumn("TotalRealPerTotalPlan", Type.GetType("System.String")),
                                new DataColumn("MilestoneDate", Type.GetType("System.String")),
                                new DataColumn("TotalManHours", Type.GetType("System.String")),
                                new DataColumn("UsedManHours", Type.GetType("System.String")),
                                new DataColumn("PerformingUser", Type.GetType("System.String")),
                                new DataColumn("Note", Type.GetType("System.String")),
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("DepartmentR", Type.GetType("System.String")),
                                new DataColumn("MonthR", Type.GetType("System.String")),
                                new DataColumn("Year", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var deparment = this.roleService.GetByID(UserSession.Current.RoleId);

                    var infoItem = reportInfo.NewRow();
                    infoItem["DepartmentR"] = deparment != null ? deparment.RussiaName : string.Empty;
                    infoItem["MonthR"] = Utility.MonthR[DateTime.Now.Month];
                    infoItem["Year"] = DateTime.Now.Year.ToString();
                    reportInfo.Rows.Add(infoItem);


                    var dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "1";
                    dataworkpackageItem["WorkPackageContent"] = "Плановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    foreach (var workpackage in wpPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData(workpackage, milestoneObj, ref dataTable, wpCount, "1.");
                        }
                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "2";
                    dataworkpackageItem["WorkPackageContent"] = "Внеплановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpUnPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData(workpackage, milestoneObj, ref dataTable, wpCount, "2.");
                        }

                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "3";
                    dataworkpackageItem["WorkPackageContent"] = "Другие работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpOtherList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData(workpackage, milestoneObj, ref dataTable, wpCount, "3.");
                        }

                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = "MonthReportTemplate.doc";
                    var strOutputFileName = "MonthReport_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);

                    // Apply style for rows have RealTotal == 100%
                    var doc = new Aspose.Words.Document(rootPath + WordPathExport + strOutputFileName);
                    var table = (Aspose.Words.Tables.Table)doc.GetChild(NodeType.Table, 2, true);

                    for (var i = 2; i < table.Rows.Count; i++)
                    {
                        if (table.Rows[i].Cells.Count > 12)
                        {
                            var temp = table.Rows[i].Cells[8].GetText();
                            if (temp.Contains("100%"))
                            {
                                foreach (Aspose.Words.Tables.Cell cell in table.Rows[i].Cells)
                                {
                                    cell.CellFormat.Shading.BackgroundPatternColor = Color.DarkGray;
                                    
                                }
                            }
                        }
                    }

                    for (var i = 2; i < table.Rows.Count; i++)
                    {
                        if (table.Rows[i].Cells.Count > 12)
                        {
                            var temp = table.Rows[i].Cells[0].GetText();
                            if (temp == "1\a" || temp == "2\a" || temp == "3\a")
                            {
                                foreach (Aspose.Words.Tables.Cell cell in table.Rows[i].Cells)
                                {
                                    // Get Runs in this cell.
                                    NodeCollection runs = cell.GetChildNodes(NodeType.Run, true);

                                    // Loop through all runs and make them bold.
                                    foreach (Run run in runs)
                                    {
                                        run.Font.Bold = true;
                                    }
                                }
                            }
                        }
                    }

                    doc.Save(rootPath + WordPathExport + strOutputFileName);

                    if (isSuccess)
                    {
                        var departmentName = this.ddlDepartment.SelectedValue != "0"
                            ? this.ddlDepartment.SelectedItem.Text.Split('-')[0] + "_"
                            : string.Empty;

                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        departmentName + "MonthReport_" + DateTime.Now.ToString("MM-yyyy") + ".doc", true);
                    }

                }
            }
            else if (e.Argument == "NextMonthReport")
            {
                var milestoneListOfMonth = new List<Milestone>();
                if (this.ddlDepartment.SelectedItem != null)
                {
                    milestoneListOfMonth = this.ddlDepartment.SelectedValue == "0"
                        ? this.milestoneService.GetAllByMonth(nextMonth, nextYear)
                        : this.milestoneService.GetAllByMonth(nextMonth, nextYear, Convert.ToInt32(this.ddlDepartment.SelectedValue));
                }
                if (milestoneListOfMonth != null && milestoneListOfMonth.Count > 0)
                {
                    var workpackageList = milestoneListOfMonth
                                        .Select(t => t.WorkpackageId)
                                        .Distinct()
                                        .Select(t => this.workGroupService.GetById(t.GetValueOrDefault()));

                    var wpPlannedList = workpackageList.Where(t => t.TypeId == 1).ToList();
                    var wpUnPlannedList = workpackageList.Where(t => t.TypeId == 2).ToList();
                    var wpOtherList = workpackageList.Where(t => t.TypeId == 0).ToList();
                    var wpCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("Text1", Type.GetType("System.String")),
                                new DataColumn("Plan", Type.GetType("System.String")),
                                new DataColumn("PlanTotal", Type.GetType("System.String")),
                                new DataColumn("MaxPlan", Type.GetType("System.String")),
                                new DataColumn("MilestoneDate", Type.GetType("System.String")),
                                new DataColumn("PerformingUser", Type.GetType("System.String")),
                                new DataColumn("Note", Type.GetType("System.String")),
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("DepartmentR", Type.GetType("System.String")),
                                new DataColumn("MonthR", Type.GetType("System.String")),
                                new DataColumn("Year", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var deparment = this.roleService.GetByID(UserSession.Current.RoleId);

                    var infoItem = reportInfo.NewRow();
                    infoItem["DepartmentR"] = deparment != null ? deparment.RussiaName : string.Empty;
                    infoItem["MonthR"] = Utility.MonthR[nextMonth];
                    infoItem["Year"] = nextYear;
                    reportInfo.Rows.Add(infoItem);


                    var dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "1";
                    dataworkpackageItem["WorkPackageContent"] = "Плановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    foreach (var workpackage in wpPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData1(workpackage, milestoneObj, ref dataTable, wpCount, "1.");
                        }
                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "2";
                    dataworkpackageItem["WorkPackageContent"] = "Внеплановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpUnPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData1(workpackage, milestoneObj, ref dataTable, wpCount, "2.");
                        }

                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "3";
                    dataworkpackageItem["WorkPackageContent"] = "Другие работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpOtherList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData1(workpackage, milestoneObj, ref dataTable, wpCount, "3.");
                        }

                    }



                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = "NextMonthReportTemplate.doc";
                    var strOutputFileName = "NextMonthReport_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);

                    // Apply style for rows have RealTotal == 100%
                    var doc = new Aspose.Words.Document(rootPath + WordPathExport + strOutputFileName);
                    var table = (Aspose.Words.Tables.Table)doc.GetChild(NodeType.Table, 2, true);

                    for (var i = 2; i < table.Rows.Count; i++)
                    {
                        if (table.Rows[i].Cells.Count > 7)
                        {
                            var temp = table.Rows[i].Cells[0].GetText();
                            if (temp == "1\a" || temp == "2\a" || temp == "3\a")
                            {
                                foreach (Aspose.Words.Tables.Cell cell in table.Rows[i].Cells)
                                {
                                    // Get Runs in this cell.
                                    NodeCollection runs = cell.GetChildNodes(NodeType.Run, true);

                                    // Loop through all runs and make them bold.
                                    foreach (Run run in runs)
                                    {
                                        run.Font.Bold = true;
                                    }
                                }
                            }
                        }
                    }

                    doc.Save(rootPath + WordPathExport + strOutputFileName);


                    if (isSuccess)
                    {
                        var departmentName = this.ddlDepartment.SelectedValue != "0"
                            ? this.ddlDepartment.SelectedItem.Text.Split('-')[0] + "_"
                            : string.Empty;
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        departmentName.Trim() + "PlanNextMonthReport_" + nextMonth + "-" + nextYear + ".doc", true);
                    }
                }
            }*/

        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(false, isListAll);
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var milestoneId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.milestoneService.Delete(milestoneId);
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid")
            {

            }
            if (e.CommandName == RadGrid.RebindGridCommandName)
            {
                this.grdDocument.Rebind();
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {

            }
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {

                ////Populate Filters by binding the combo to datasource
                //var filteringItem = (GridFilteringItem)e.Item;
                //var myRadComboBox = (RadComboBox)filteringItem.FindControl("RadComboBoxCustomerProgramDescription");

                //myRadComboBox.DataSource = myDataSet;
                //myRadComboBox.DataTextField = "CustomerProgramDescription";
                //myRadComboBox.DataValueField = "CustomerProgramDescription";
                //myRadComboBox.ClearSelection();
                //myRadComboBox.DataBind();
            }
            if (e.Item is GridDataItem)
            {
                double temp = 0;
                var item = e.Item as GridDataItem;
                if (!string.IsNullOrEmpty(item["PlanPercent"].Text))
                {
                    item["PlanPercent"].Text = Math.Round(double.TryParse(item["PlanPercent"].Text, out temp) ? temp : 0, 2).ToString() + "%";
                }
                if (!string.IsNullOrEmpty(item["PlanTotal"].Text))
                {
                    item["PlanTotal"].Text = Math.Round(double.TryParse(item["PlanTotal"].Text, out temp) ? temp : 0, 2).ToString() + "%";
                }
                if (!string.IsNullOrEmpty(item["Real"].Text))
                {
                    item["Real"].Text = Math.Round(double.TryParse(item["Real"].Text, out temp) ? temp : 0, 2).ToString() + "%";
                }
                if (!string.IsNullOrEmpty(item["RealTotal"].Text))
                {
                    item["RealTotal"].Text = Math.Round(double.TryParse(item["RealTotal"].Text, out temp) ? temp : 0, 2).ToString() + "%";
                }
            }

            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var lblWorkgroupName = item.FindControl("lblWorkgroupName") as Label;
                var lbldocNo = item.FindControl("lbldocNo") as Label;
                var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var txtManHours = item.FindControl("txtManHours") as RadNumericTextBox;
                var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
                var txtDeadline = item.FindControl("txtDeadline") as RadDatePicker;
                var txtEndDate = item.FindControl("txtEndDate") as RadDatePicker;
                var txtIsoReviseDate = item.FindControl("txtIsoReviseDate") as RadDatePicker;
                var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
                var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;
                var listRevision = this.revisionService.GetAll();
                listRevision.Insert(0, new Revision() { ID = 0 });
                if (ddlRevision != null)
                {
                    ddlRevision.DataSource = listRevision;
                    ddlRevision.DataTextField = "Name";
                    ddlRevision.DataValueField = "ID";
                    ddlRevision.DataBind();
                }

                if (txtStartDate != null)
                {
                    txtStartDate.DatePopupButton.Visible = false;
                }

                if (txtDeadline != null)
                {
                    txtDeadline.DatePopupButton.Visible = false;
                }

                if (txtEndDate != null)
                {
                    txtEndDate.DatePopupButton.Visible = false;
                }

                if (txtIsoReviseDate != null)
                {
                    txtIsoReviseDate.DatePopupButton.Visible = false;
                }

                var WorkgroupName = (item.FindControl("WorkgroupName") as HiddenField).Value;
                var DocNo = (item.FindControl("DocNo") as HiddenField).Value;
                var DocTitle = (item.FindControl("DocTitle") as HiddenField).Value;
                var RevisionId = (item.FindControl("RevisionId") as HiddenField).Value;
                var ManHours = (item.FindControl("ManHours") as HiddenField).Value;
                var StartDate = (item.FindControl("StartDate") as HiddenField).Value;
                var Deadline = (item.FindControl("Deadline") as HiddenField).Value;
                var EndDate = (item.FindControl("EndDate") as HiddenField).Value;
                var IsoReviseDate = (item.FindControl("IsoReviseDate") as HiddenField).Value;
                var complete = (item.FindControl("Complete") as HiddenField).Value;
                var weight = (item.FindControl("Weight") as HiddenField).Value;

                if (!string.IsNullOrEmpty(StartDate))
                {
                    txtStartDate.SelectedDate = Convert.ToDateTime(StartDate);
                }

                if (!string.IsNullOrEmpty(Deadline))
                {
                    txtDeadline.SelectedDate = Convert.ToDateTime(Deadline);
                }

                if (!string.IsNullOrEmpty(EndDate))
                {
                    txtEndDate.SelectedDate = Convert.ToDateTime(EndDate);
                }

                if (!string.IsNullOrEmpty(IsoReviseDate))
                {
                    txtIsoReviseDate.SelectedDate = Convert.ToDateTime(IsoReviseDate);
                }

                lblWorkgroupName.Text = WorkgroupName;
                lbldocNo.Text = DocNo;
                txtDocTitle.Text = DocTitle;


                ddlRevision.SelectedValue = RevisionId;
                txtComplete.Value = Convert.ToDouble(complete);
                txtWeight.Value = Convert.ToDouble(weight);
                txtManHours.Value = Convert.ToDouble(ManHours);
            }
        }

        protected void radTreeFolder_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            PopulateNodeOnDemand(e, TreeNodeExpandMode.ServerSideCallBack);
        }

        protected void grdDocument_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var txtManHours = item.FindControl("txtManHours") as RadNumericTextBox;
                var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
                var txtDeadline = item.FindControl("txtDeadline") as RadDatePicker;
                var txtEndDate = item.FindControl("txtEndDate") as RadDatePicker;
                var txtIsoReviseDate = item.FindControl("txtIsoReviseDate") as RadDatePicker;
                var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
                var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;

                var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                var objDoc = this.documentPackageService.GetById(docId);

                var currentRevision = objDoc.RevisionId;
                var newRevision = Convert.ToInt32(ddlRevision.SelectedValue);
                if (currentRevision != 0 && newRevision != currentRevision)
                {
                    var docObjNew = new DocumentPackage
                    {
                        ProjectId = Convert.ToInt32(this.ddlDepartment.SelectedValue),
                        ProjectName = this.ddlDepartment.SelectedItem.Text,
                        WorkgroupId = objDoc.WorkgroupId,
                        WorkgroupName = objDoc.WorkgroupName,
                        DocNo = objDoc.DocNo,
                        DocTitle = txtDocTitle.Text.Trim(),
                        StartDate = txtStartDate.SelectedDate,
                        Deadline = txtDeadline.SelectedDate,
                        EndDate = txtEndDate.SelectedDate,
                        IsoReviseDate = txtIsoReviseDate.SelectedDate,
                        RevisionId = newRevision,
                        RevisionName = ddlRevision.SelectedItem.Text,
                        Complete = txtComplete.Value,
                        Weight = txtWeight.Value,
                        ManHours = txtManHours.Value,
                        DocumentTypeId = objDoc.DocumentTypeId,
                        DocumentTypeName = objDoc.DocumentTypeName,
                        IsLeaf = true,
                        IsEMDR = true,
                        ParentId = objDoc.ParentId ?? objDoc.ID,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };

                    this.documentPackageService.Insert(docObjNew);

                    objDoc.IsLeaf = false;
                }
                else
                {
                    objDoc.DocTitle = txtDocTitle.Text.Trim();
                    objDoc.StartDate = txtStartDate.SelectedDate;
                    objDoc.Deadline = txtDeadline.SelectedDate;
                    objDoc.EndDate = txtEndDate.SelectedDate;
                    objDoc.IsoReviseDate = txtIsoReviseDate.SelectedDate;
                    objDoc.ManHours = txtManHours.Value;
                    objDoc.RevisionId = newRevision;
                    objDoc.RevisionName = ddlRevision.SelectedItem.Text;
                    objDoc.Complete = txtComplete.Value.GetValueOrDefault();
                    objDoc.Weight = txtWeight.Value.GetValueOrDefault();
                }

                objDoc.UpdatedBy = UserSession.Current.User.Id;
                objDoc.UpdatedDate = DateTime.Now;

                this.documentPackageService.Update(objDoc);
            }
        }

        protected void ckbEnableFilter_OnCheckedChanged(object sender, EventArgs e)
        {
            this.grdDocument.AllowFilteringByColumn = ((CheckBox)sender).Checked;
            this.grdDocument.Rebind();
        }

        protected void radTreeFolder_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "Images/folderdir16.png";
        }

        private void PopulateNodeOnDemand(RadTreeNodeEventArgs e, TreeNodeExpandMode expandMode)
        {
            var categoryId = this.lblCategoryId.Value;
            var folderPermission =
                this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Where(
                    t => t.CategoryIdList == categoryId).Select(t => Convert.ToInt32(t.FolderIdList)).ToList();

            var listFolChild = this.folderService.GetAllByParentId(Convert.ToInt32(e.Node.Value), folderPermission);
            foreach (var folderChild in listFolChild)
            {
                var nodeFolder = new RadTreeNode();
                nodeFolder.Text = folderChild.Name;
                nodeFolder.Value = folderChild.ID.ToString();
                nodeFolder.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                nodeFolder.ImageUrl = "Images/folderdir16.png";
                e.Node.Nodes.Add(nodeFolder);
            }

            e.Node.Expanded = true;
        }

        //private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
        //{
        //    // Create a new validation to the validations list.
        //    Validation validation = objValidations[objValidations.Add()];

        //    // Set the validation type.
        //    validation.Type = Aspose.Cells.ValidationType.List;

        //    // Set the operator.
        //    validation.Operator = OperatorType.None;

        //    // Set the in cell drop down.
        //    validation.InCellDropDown = true;

        //    // Set the formula1.
        //    validation.Formula1 = "=" + formular;

        //    // Enable it to show error.
        //    validation.ShowError = true;
        //    validation.ShowInput = true;

        //    // Set the alert type severity level.
        //    validation.AlertStyle = ValidationAlertType.Stop;

        //    // Set the error title.
        //    validation.ErrorTitle = "Error";
        //    validation.InputTitle = "Warning";

        //    // Set the error message.
        //    validation.ErrorMessage = "Please select item from the list";
        //    validation.InputMessage = "Please select item from the list";

        //    // Specify the validation area.
        //    CellArea area;
        //    area.StartRow = startRow;
        //    area.EndRow = endRow;
        //    area.StartColumn = startColumn;
        //    area.EndColumn = endColumn;

        //    // Add the validation area.
        //    validation.AreaList.Add(area);

        //    ////return validation;
        //}

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void LoadObjectTree()
        {
            var departmentList = this.roleService.GetAll(false);

            if (!UserSession.Current.IsAdmin && !UserSession.Current.IsDC && !UserSession.Current.IsSuperViewer)
            {
                departmentList = departmentList.Where(t => t.Id == UserSession.Current.RoleId).ToList();
            }
            else
            {
                departmentList.Insert(0, new Role { Id = 0, Name = "All" });
            }

            this.ddlDepartment.DataSource = departmentList;
            this.ddlDepartment.DataTextField = "FullName";
            this.ddlDepartment.DataValueField = "Id";
            this.ddlDepartment.DataBind();

            if (!UserSession.Current.IsAdmin && !UserSession.Current.IsDC)
            {
                this.ddlDepartment.Enabled = false;
            }
        }

        /// <summary>
        /// The repair list.
        /// </summary>
        /// <param name="listOptionalTypeDetail">
        /// The list optional type detail.
        /// </param>
        private void RepairList(ref List<OptionalTypeDetail> listOptionalTypeDetail)
        {
            var temp = listOptionalTypeDetail.Where(t => t.ParentId != null).Select(t => t.ParentId).Distinct().ToList();
            var temp2 = listOptionalTypeDetail.Select(t => t.ID).ToList();
            var tempList = new List<OptionalTypeDetail>();
            foreach (var x in temp)
            {
                if (!temp2.Contains(x.Value))
                {
                    tempList.AddRange(listOptionalTypeDetail.Where(t => t.ParentId == x.Value).ToList());
                }
            }

            var listOptionalType = tempList.Where(t => t.OptionalTypeId != null).Select(t => t.OptionalTypeId).Distinct().ToList();

            foreach (var optionalTypeId in listOptionalType)
            {
                var optionalType = this.optionalTypeService.GetById(optionalTypeId.Value);
                var tempOptTypeDetail = new OptionalTypeDetail() { ID = optionalType.ID * 9898, Name = optionalType.Name + "s" };
                listOptionalTypeDetail.Add(tempOptTypeDetail);
                ////tempList.Add(tempOptTypeDetail);
                OptionalType type = optionalType;
                foreach (var optionalTypeDetail in tempList.Where(t => t.OptionalTypeId == type.ID).ToList())
                {
                    optionalTypeDetail.ParentId = tempOptTypeDetail.ID;
                }
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            this.grdDocument.Rebind();
        }

        protected void grdDocument_Init(object sender, EventArgs e)
        {
        }

        protected void grdDocument_DataBound(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// The btn download_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnDownload_Click(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).Parent.Parent as GridDataItem;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentNewService.GetById(docId);
            var docPackName = string.Empty;
            if (docObj != null)
            {
                //docPackName = docObj.Name;
                //var serverDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_" + docObj.Name + "_Pack.rar");

                //var attachFiles = this.attachFileService.GetAllByDocId(docId);

                //var temp = ZipPackage.CreateFile(serverDocPackPath);

                //foreach (var attachFile in attachFiles)
                //{
                //    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                //    {
                //        temp.Add(Server.MapPath(attachFile.FilePath));
                //    }
                //}

                //this.DownloadByWriteByte(serverDocPackPath, docPackName + ".rar", true);
            }
        }

        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {
                var filterItem = (GridFilteringItem)e.Item;
                var selectedProperty = new List<string>();

                var ddlFilterRev = (RadComboBox)filterItem.FindControl("ddlFilterRev");
            }
        }

        protected DateTime? SetPublishDate(GridItem item)
        {
            if (item.OwnerTableView.GetColumn("Index27").CurrentFilterValue == string.Empty)
            {
                return new DateTime?();
            }
            else
            {
                return DateTime.Parse(item.OwnerTableView.GetColumn("Index27").CurrentFilterValue);
            }
        }
        protected void rtvWorkgroup_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/package.png";
        }

        protected void ddlDepartment_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"Images/group.png";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        protected void txtFromMonthYear_OnSelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            this.grdDocument.Rebind();
        }

        private void CollectPlanData(WorkGroup workpackage, Milestone milestone, ref DataTable dt, int wpCount, string index, List<Milestone> previousMilestone, int month)
        {
            var dataitem = dt.NewRow();
            dataitem["Index"] = index + wpCount;

            dataitem["WorkPackageContent"] = workpackage.Name + (!string.IsNullOrEmpty(workpackage.RevisionName) ? ", " + workpackage.RevisionName : string.Empty) + "\n" + workpackage.Description;

            dataitem["WPIncommingNo"] = (!string.IsNullOrEmpty(workpackage.IncomingNo) ? workpackage.IncomingNo + ",\n" : string.Empty)
                                        + (workpackage.StartDate != null ? workpackage.StartDate.Value.ToString("dd.MM.yyyy") : string.Empty);

            ////dataitem["Plan"] = milestone.MilestoneDate <= workpackage.Deadline
            ////                                ? "(+)" + milestone.PlanPercent + "%"
            ////                                : "(-)" + milestone.PlanPercent + "%";
            ////dataitem["PlanTotal"] = milestone.MilestoneDate <= workpackage.Deadline
            ////                                ? "(+)" + milestone.PlanTotal + "%"
            ////                                : "(-)" + milestone.PlanTotal + "%";

            dataitem["Plan"] = milestone.PlanPercent;
            dataitem["PlanTotal"] = milestone.PlanTotal;
            //if (month > 1)
            //{
            //    var sumReal = 0.0;
            //    sumReal = previousMilestone.Aggregate(sumReal, (current, t) => current + t.Real.GetValueOrDefault());
            //    dataitem["SumReal"] = (object)sumReal ?? DBNull.Value;
            //}
            //else
            //{
            //    dataitem["SumReal"] = milestone.PlanPercent;
            //}

            dataitem["SumReal"] = milestone.PlanOfYear;

            dataitem["Deadline"] = workpackage.Deadline != null
                                            ? workpackage.Deadline.Value.ToString("dd.MM.yy")
                                            : string.Empty;

            dataitem["PerformingUser"] = milestone.PerformingUser;
            dataitem["Note"] = milestone.Note;
            // dataitem["WPWeight"] = workpackage.Weight != null && workpackage.Weight != 0                ? workpackage.Weight.Value.ToString()                : string.Empty;

            dt.Rows.Add(dataitem);
        }

        private void CollectRealData(WorkGroup workpackage, Milestone milestone, ref DataTable dt, int wpCount, string index, List<Milestone> previousMilestone)
        {
            var dataitem = dt.NewRow();
            dataitem["Index"] = index + wpCount;

            dataitem["WorkPackageContent"] = workpackage.Name + (!string.IsNullOrEmpty(workpackage.RevisionName) ? ", " + workpackage.RevisionName : string.Empty) + "\n" + workpackage.Description;

            dataitem["WPIncommingNo"] = (!string.IsNullOrEmpty(workpackage.IncomingNo) ? workpackage.IncomingNo + ",\n" : string.Empty)
                                        + (workpackage.StartDate != null ? workpackage.StartDate.Value.ToString("dd.MM.yyyy") : string.Empty);

            dataitem["Real"] = Math.Round(milestone.Real.GetValueOrDefault(), 2);
            dataitem["RealTotal"] = Math.Round(milestone.RealTotal.GetValueOrDefault(), 2);

            var sumReal = 0.0;
            sumReal = previousMilestone.Aggregate(sumReal, (current, t) => current + t.Real.GetValueOrDefault());
            if (sumReal < 0)
            {
                sumReal = 0;
            }
            else if (sumReal > 100)
            {
                sumReal = 100;
            }

            dataitem["SumReal"] = Math.Round(sumReal, 2);
            double complete = workpackage.Complete != null ? workpackage.Complete.GetValueOrDefault() : 0;
            dataitem["Deadline"] = complete < 100 ? (workpackage.Deadline != null
                                            ? workpackage.Deadline.Value.ToString("dd.MM.yy")
                                            : string.Empty) : (workpackage.EndDate != null ? workpackage.EndDate.Value.ToString("dd.MM.yy") : string.Empty);

            dataitem["PerformingUser"] = milestone.PerformingUser;
            dataitem["Note"] = milestone.Note;
            //dataitem["WPWeight"] = workpackage.Weight != null && workpackage.Weight != 0
            //    ? workpackage.Weight.Value.ToString()
            //    : string.Empty;

            dataitem["TotalManHours"] = milestone.Real / 100 * workpackage.TotalManHours;
            dataitem["DescriptionOfDuty"] = milestone.DescriptionOfDuty;

            dt.Rows.Add(dataitem);
        }
        private void CollectRealDataWPUnpland(DocumentPackage documenObj, ref DataTable dt, int wpCount, string index)
        {
            var dataitem = dt.NewRow();
            dataitem["Index"] = index + wpCount;

            dataitem["WorkPackageContent"] = documenObj.DocNo + "\n" + documenObj.DocTitle;

            dataitem["WPIncommingNo"] = documenObj.StartDate != null ? documenObj.StartDate.Value.ToString("dd.MM.yyyy") : string.Empty;


            // dataitem["RealTotal"] = milestone.RealTotal;

            //var sumReal = 0.0;
            //sumReal = previousMilestone.Aggregate(sumReal, (current, t) => current + t.Real.GetValueOrDefault());
            // dataitem["SumReal"] = DBNull.Value;
            double complete = documenObj.Complete != null ? documenObj.Complete.GetValueOrDefault() : 0;
            dataitem["Real"] = Math.Round(complete, 2);
            dataitem["Deadline"] = documenObj.Deadline != null ? documenObj.Deadline.Value.ToString("dd.MM.yyyy") : string.Empty;

            dataitem["PerformingUser"] = documenObj.EngineerName + " \n" + documenObj.PersonsInvolved;
            dataitem["Note"] = documenObj.Notes;
            //dataitem["WPWeight"] = workpackage.Weight != null && workpackage.Weight != 0
            //    ? workpackage.Weight.Value.ToString()
            //    : string.Empty;

            dataitem["TotalManHours"] = 0;// milestone.Real / 100 * workpackage.TotalManHours;
            dataitem["DescriptionOfDuty"] = "";// milestone.DescriptionOfDuty;

            dt.Rows.Add(dataitem);
        }
        protected void btnExportMonthReport_Click(object sender, EventArgs e)
        {
            var fromMonth = this.txtFromMonthYear.SelectedDate != null
                    ? this.txtFromMonthYear.SelectedDate.Value.Month
                    : DateTime.Now.Month;
            var fromYear = this.txtFromMonthYear.SelectedDate != null
               ? this.txtFromMonthYear.SelectedDate.Value.Year
               : DateTime.Now.Year;
            var fromDate = new DateTime(fromYear, fromMonth, 1);

            var toMonth = this.txtToMonthYear.SelectedDate != null
                ? this.txtToMonthYear.SelectedDate.Value.AddMonths(1).Month
                : DateTime.Now.Month;
            var toYear = this.txtToMonthYear.SelectedDate != null
               ? this.txtToMonthYear.SelectedDate.Value.AddMonths(1).Year
               : DateTime.Now.Year;
            var toDate = new DateTime(toYear, toMonth, 1);

            var department = this.roleService.GetByID(UserSession.Current.RoleId);

            var differenceMonth = ((toDate.Year - fromDate.Year) * 12) + toDate.Month - fromDate.Month;

            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\MonthReportTemplate.xls");
            if (differenceMonth == 0)
            {
                var dataSheet = workbook.Worksheets[0];
                var planDataTable = PlanMonthReport(fromMonth, fromYear);
                var realDataTable = RealMonthReport(fromMonth, fromYear);
                var discriptiondepartment = department.Description.Split('-').ToList();

                dataSheet.Cells["A10"].PutValue(realDataTable.Rows.Count);
                dataSheet.Cells["E2"].PutValue(department.Code);
                dataSheet.Cells["F2"].PutValue(discriptiondepartment.Count > 1 ? discriptiondepartment[1] : "");
                dataSheet.Cells["I2"].PutValue(discriptiondepartment[0]);
                dataSheet.Cells["E3"].PutValue(Utility.MonthR2[fromMonth]);
                dataSheet.Cells["F3"].PutValue(fromYear);

                dataSheet.Cells.ImportDataTable(planDataTable, false, 10, 0, planDataTable.Rows.Count, 12, false);
                dataSheet.Cells.ImportDataTable(realDataTable, false, 10, 13, realDataTable.Rows.Count, 14, false);

                dataSheet.Cells["B" + (planDataTable.Rows.Count + 13)].PutValue("Trưởng phòng");
                dataSheet.Cells["B" + (planDataTable.Rows.Count + 14)].PutValue("Начальник Отдела");

                dataSheet.Cells["N" + (realDataTable.Rows.Count + 13)].PutValue("Trưởng phòng");
                dataSheet.Cells["N" + (realDataTable.Rows.Count + 14)].PutValue("Начальник Отдела");

                dataSheet.AutoFitRows();
                dataSheet.Name = fromDate.ToString("MM-yyyy");
                var filename = "MonthReport_" + DateTime.Now.ToString("MM-yyyy") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, filename, true);
            }
            else
            {
                for (int i = 0; i < differenceMonth - 1; i++)
                {
                    workbook.Worksheets.AddCopy(1);
                }

                for (int i = 0; i < differenceMonth; i++)
                {
                    var dataSheet = workbook.Worksheets[i + 1];
                    var planDataTable = PlanMonthReport(fromDate.AddMonths(i).Month, fromDate.AddMonths(i).Year);
                    var realDataTable = RealMonthReport(fromDate.AddMonths(i).Month, fromDate.AddMonths(i).Year);

                    dataSheet.Cells["A10"].PutValue(realDataTable.Rows.Count);
                    dataSheet.Cells["E2"].PutValue(department.Code);
                    dataSheet.Cells["F2"].PutValue(Utility.DepartmentDescriptionR[department.Code]);
                    dataSheet.Cells["I2"].PutValue(Utility.DepartmentDescription[department.Code]);
                    dataSheet.Cells["E3"].PutValue(Utility.MonthR2[fromDate.AddMonths(i).Month]);
                    dataSheet.Cells["F3"].PutValue(fromDate.AddMonths(i).Year);

                    dataSheet.Cells.ImportDataTable(planDataTable, false, 10, 0, planDataTable.Rows.Count, 12, false);
                    dataSheet.Cells.ImportDataTable(realDataTable, false, 10, 13, realDataTable.Rows.Count, 14, false);

                    dataSheet.Cells["B" + (planDataTable.Rows.Count + 13)].PutValue("Trưởng phòng");
                    dataSheet.Cells["B" + (planDataTable.Rows.Count + 14)].PutValue("Начальник Отдела");

                    dataSheet.Cells["N" + (realDataTable.Rows.Count + 13)].PutValue("Trưởng phòng");
                    dataSheet.Cells["N" + (realDataTable.Rows.Count + 14)].PutValue("Начальник Отдела");

                    dataSheet.AutoFitRows();
                    dataSheet.Name = fromDate.AddMonths(i).ToString("MM-yyyy");
                }
                //workbook.Worksheets.RemoveAt(0);
                var filename = "MonthReport_" + DateTime.Now.ToString("MM-yyyy") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, filename, true);
            }
        }

        private DataTable PlanMonthReport(int month, int year)
        {
            var dataTable = new DataTable();
            var milestoneListOfMonth = new List<Milestone>();

            if (this.ddlDepartment.SelectedItem != null)
            {
                milestoneListOfMonth = this.ddlDepartment.SelectedValue == "0"
                    ? this.milestoneService.GetAllByMonth(month, year)
                    : this.milestoneService.GetAllByMonth(month, year, Convert.ToInt32(this.ddlDepartment.SelectedValue));
            }

            if (milestoneListOfMonth != null && milestoneListOfMonth.Count > 0)
            {
                var workpackageList = milestoneListOfMonth
                    .Select(t => t.WorkpackageId)
                    .Distinct()
                    .Select(t => this.workGroupService.GetById(t.GetValueOrDefault()))
                    .Where(t => t != null && t.IsStop == false)
                    .ToList();

                var wpPlannedList = workpackageList.Where(t => t.TypeId.GetValueOrDefault() == 1).ToList();
                var wpUnPlannedList = workpackageList.Where(t => t.TypeId.GetValueOrDefault() == 2).ToList();
                var wpOtherList = workpackageList.Where(t => t.TypeId.GetValueOrDefault() == 0).ToList();
                var wpCount = 0;

                var reportInfo = new DataTable();

                var listColumn = new[]
                {
                    new DataColumn("Type", Type.GetType("System.String")),
                    new DataColumn("Index", Type.GetType("System.String")),
                    new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                    new DataColumn("Text1", Type.GetType("System.String")),
                    new DataColumn("WPIncommingNo", Type.GetType("System.String")),
                    new DataColumn("Plan", Type.GetType("System.String")),
                    new DataColumn("PlanTotal", Type.GetType("System.String")),
                    new DataColumn("SumReal", Type.GetType("System.String")),
                    new DataColumn("Deadline", Type.GetType("System.String")),
                    new DataColumn("PerformingUser", Type.GetType("System.String")),
                    new DataColumn("Note", Type.GetType("System.String")),
                    new DataColumn("WPWeight", Type.GetType("System.String")),
                };
                dataTable.Columns.AddRange(listColumn);

                var dataworkpackageItem = dataTable.NewRow();
                dataworkpackageItem["Type"] = "1";
                dataworkpackageItem["Index"] = "I";
                dataworkpackageItem["WorkPackageContent"] = "Плановые работы";
                dataworkpackageItem["Text1"] = "Công việc trong kế hoạch";

                dataTable.Rows.Add(dataworkpackageItem);
                foreach (var workpackage in wpPlannedList)
                {
                    var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                    if (milestoneObj != null)
                    {
                        wpCount += 1;
                        var previousMilestone = new List<Milestone>();
                        if (month > 1)
                        {
                            var fromDate = new DateTime(year, 1, 1);
                            var toDate = new DateTime(year, month, 1);
                            previousMilestone = this.milestoneService.GetAllByWorkpackage(fromDate, toDate, workpackage.ID);
                        }
                        this.CollectPlanData(workpackage, milestoneObj, ref dataTable, wpCount, "I.", previousMilestone, month);
                    }
                }

                dataworkpackageItem = dataTable.NewRow();
                dataworkpackageItem["Type"] = "1";
                dataworkpackageItem["Index"] = "II";
                dataworkpackageItem["WorkPackageContent"] = "Внеплановые работы";
                dataworkpackageItem["Text1"] = "Công việc ngoài kế hoạch";

                dataTable.Rows.Add(dataworkpackageItem);
                wpCount = 0;
                foreach (var workpackage in wpUnPlannedList)
                {
                    var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                    if (milestoneObj != null)
                    {
                        wpCount += 1;
                        var previousMilestone = new List<Milestone>();
                        if (month > 1)
                        {
                            var fromDate = new DateTime(year, 1, 1);
                            var toDate = new DateTime(year, month, 1);
                            previousMilestone = this.milestoneService.GetAllByWorkpackage(fromDate, toDate, workpackage.ID);
                        }
                        this.CollectPlanData(workpackage, milestoneObj, ref dataTable, wpCount, "II.", previousMilestone, month);
                    }
                }

                dataworkpackageItem = dataTable.NewRow();
                dataworkpackageItem["Type"] = "1";
                dataworkpackageItem["Index"] = "III";
                dataworkpackageItem["WorkPackageContent"] = "Услуги сторонним организациям";
                dataworkpackageItem["Text1"] = "Dịch vụ ngoài";

                dataTable.Rows.Add(dataworkpackageItem);
                wpCount = 0;
                foreach (var workpackage in wpOtherList)
                {
                    var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                    if (milestoneObj != null)
                    {
                        wpCount += 1;
                        var previousMilestone = new List<Milestone>();
                        if (month > 1)
                        {
                            var fromDate = new DateTime(year, 1, 1);
                            var toDate = new DateTime(year, month, 1);
                            previousMilestone = this.milestoneService.GetAllByWorkpackage(fromDate, toDate, workpackage.ID);
                        }

                        this.CollectPlanData(workpackage, milestoneObj, ref dataTable, wpCount, "III.", previousMilestone, month);
                    }
                }
                dataworkpackageItem = dataTable.NewRow();
                dataworkpackageItem["Type"] = "1";
                dataTable.Rows.Add(dataworkpackageItem);
            }
            return dataTable;
        }

        private List<WorkGroup> GetUserOfWorkgroup(int month, int year)
        {
            var milestoneListOfMonth = new List<Milestone>();
            var workpackageList = new List<WorkGroup>();
            if (this.ddlDepartment.SelectedItem != null)
            {
                milestoneListOfMonth = this.ddlDepartment.SelectedValue == "0"
                    ? this.milestoneService.GetAllByMonth(month, year)
                    : this.milestoneService.GetAllByMonth(month, year, Convert.ToInt32(this.ddlDepartment.SelectedValue));
            }
            if (milestoneListOfMonth != null && milestoneListOfMonth.Count > 0)
            {
                workpackageList = milestoneListOfMonth
               .Select(t => t.WorkpackageId)
               .Distinct()
               .Select(t => this.workGroupService.GetById(t.GetValueOrDefault()))
               .Where(t => t != null)
               .ToList();
            }
            return workpackageList;
        }
        private DataTable RealMonthReport(int month, int year)
        {
            var dataTable = new DataTable();
            var milestoneListOfMonth = new List<Milestone>();

            var documentunplaned = new List<DocumentPackage>();
            var projectunplaned = "UP-" + DateTime.Now.Year;
            var projectobj = this.scopeProjectService.GetByName(projectunplaned);

            if (this.ddlDepartment.SelectedItem != null)
            {
                milestoneListOfMonth = this.ddlDepartment.SelectedValue == "0"
                    ? this.milestoneService.GetAllByMonth(month, year)
                    : this.milestoneService.GetAllByMonth(month, year, Convert.ToInt32(this.ddlDepartment.SelectedValue));
            }
            if (milestoneListOfMonth != null && milestoneListOfMonth.Count > 0)
            {
                var workpackageList = milestoneListOfMonth
                    .Select(t => t.WorkpackageId)
                    .Distinct()
                    .Select(t => this.workGroupService.GetById(t.GetValueOrDefault()))
                    .Where(t => t != null && t.IsStop == false)
                    .ToList();

                var wpPlannedList = workpackageList.Where(t => t.TypeId.GetValueOrDefault() == 1).ToList();
                var wpUnPlannedList = workpackageList.Where(t => t.TypeId.GetValueOrDefault() == 2).ToList();
                var wpOtherList = workpackageList.Where(t => t.TypeId.GetValueOrDefault() == 0).ToList();
                var wpCount = 0;

                var listColumn = new[]
                {
                    new DataColumn("Index", Type.GetType("System.String")),
                    new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                    new DataColumn("Text1", Type.GetType("System.String")),
                    new DataColumn("WPIncommingNo", Type.GetType("System.String")),
                    new DataColumn("Real", Type.GetType("System.String")),
                    new DataColumn("RealTotal", Type.GetType("System.String")),
                    new DataColumn("SumReal", Type.GetType("System.String")),
                    new DataColumn("Deadline", Type.GetType("System.String")),
                    new DataColumn("PerformingUser", Type.GetType("System.String")),
                    new DataColumn("Note", Type.GetType("System.String")),
                    new DataColumn("WPWeight", Type.GetType("System.String")),
                    new DataColumn("TotalManHours", Type.GetType("System.String")),
                    new DataColumn("Text2", Type.GetType("System.String")),
                    new DataColumn("DescriptionOfDuty", Type.GetType("System.String")),
                };
                dataTable.Columns.AddRange(listColumn);



                var deparment = this.roleService.GetByID(UserSession.Current.RoleId);

                var dataworkpackageItem = dataTable.NewRow();
                dataworkpackageItem["Index"] = "I";
                dataworkpackageItem["WorkPackageContent"] = "Плановые работы";
                dataworkpackageItem["Text1"] = "Công việc trong kế hoạch";
                dataTable.Rows.Add(dataworkpackageItem);
                foreach (var workpackage in wpPlannedList)
                {
                    var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                    if (milestoneObj != null)
                    {
                        wpCount += 1;
                        var fromDate = new DateTime(year, 1, 1);
                        var toDate = new DateTime(month == 12 ? year + 1 : year, month == 12 ? 1 : month + 1, 1);
                        var previousMilestone = this.milestoneService.GetAllByWorkpackage(fromDate, toDate, workpackage.ID);

                        this.CollectRealData(workpackage, milestoneObj, ref dataTable, wpCount, "I.", previousMilestone);
                    }
                }

                dataworkpackageItem = dataTable.NewRow();
                dataworkpackageItem["Index"] = "II";
                dataworkpackageItem["WorkPackageContent"] = "Внеплановые работы";
                dataworkpackageItem["Text1"] = "Công việc ngoài kế hoạch";
                dataTable.Rows.Add(dataworkpackageItem);
                wpCount = 0;
                foreach (var workpackage in wpUnPlannedList)
                {
                    var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                    if (milestoneObj != null)
                    {
                        wpCount += 1;
                        var fromDate = new DateTime(year, 1, 1);
                        var toDate = new DateTime(month == 12 ? year + 1 : year, month == 12 ? 1 : month + 1, 1);
                        var previousMilestone = this.milestoneService.GetAllByWorkpackage(fromDate, toDate, workpackage.ID);

                        this.CollectRealData(workpackage, milestoneObj, ref dataTable, wpCount, "II.", previousMilestone);
                    }

                }


                dataworkpackageItem = dataTable.NewRow();
                dataworkpackageItem["Index"] = "III";
                dataworkpackageItem["WorkPackageContent"] = "Услуги сторонним организациям";
                dataworkpackageItem["Text1"] = "Dịch vụ ngoài";
                dataTable.Rows.Add(dataworkpackageItem);
                wpCount = 0;
                foreach (var workpackage in wpOtherList)
                {
                    var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                    if (milestoneObj != null)
                    {
                        wpCount += 1;
                        var fromDate = new DateTime(year, 1, 1);
                        var toDate = new DateTime(month == 12 ? year + 1 : year, month == 12 ? 1 : month + 1, 1);
                        var previousMilestone = this.milestoneService.GetAllByWorkpackage(fromDate, toDate, workpackage.ID);

                        this.CollectRealData(workpackage, milestoneObj, ref dataTable, wpCount, "III.", previousMilestone);
                    }

                }


                dataworkpackageItem = dataTable.NewRow();
                dataworkpackageItem["Index"] = "IV";
                dataworkpackageItem["WorkPackageContent"] = "Другие работы";
                dataworkpackageItem["Text1"] = "Công việc khác của phòng";
                dataTable.Rows.Add(dataworkpackageItem);
                wpCount = 0;
                if (projectobj != null)
                {
                    var wplist = this.ddlDepartment.SelectedItem != null && this.ddlDepartment.SelectedValue == "0" ? this.workGroupService.GetAllWorkGroupOfProject(projectobj.ID).ToList() : this.workGroupService.GetAllWorkGroupOfProject(projectobj.ID).Where(t => t.DepartmentId == Convert.ToInt32(this.ddlDepartment.SelectedValue)).ToList();
                    if (wplist.Count > 0)
                    {
                        documentunplaned = wplist.Count > 1 ? documentPackageService.GetAllDocProject(projectobj.ID) : documentPackageService.GetAllEMDRByWorkgroup(wplist[0].ID);
                        var fromD = new DateTime(year, month, 1);
                        var toD = DateTime.Now.Date.Month;
                        var docreal = documentunplaned.Where(t => t.StartDate != null && t.Deadline != null && t.StartDate.Value.Month <= toD && t.Deadline.Value.Month >= toD);
                        foreach (var item in docreal)
                        {
                            wpCount += 1;
                            CollectRealDataWPUnpland(item, ref dataTable, wpCount, "IV");
                        }
                    }
                }

            }



            return dataTable;
        }
    }
}
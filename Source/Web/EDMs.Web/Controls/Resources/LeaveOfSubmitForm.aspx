﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeaveOfSubmitForm.aspx.cs" Inherits="EDMs.Web.Controls.Resources.LeaveOfSubmitForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }
        
        var isTimeSelected = false;
        function DateSelected(sender, args) {
            if (!isTimeSelected)
                sender.get_timeView().setTime(7, 0, 0, 0);
            isTimeSelected = true;
        }

        function ClientTimeSelected(sender, args) {

            isTimeSelected = true;
        }

        var isTimeSelected1 = false;
        function DateSelected1(sender, args) {
            if (!isTimeSelected)
                sender.get_timeView().setTime(7, 0, 0, 0);
            isTimeSelected = true;
        }

        function ClientTimeSelected1(sender, args) {

            isTimeSelected = true;
        }
    </script>

    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>

        <div style="width: 100%">
            <ul style="list-style-type: none">
                <li style="width: 500px; padding-bottom: 3px;">
                    <div style="border-style: solid; border-color: #E44B25;"></div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <span style="color: #E44B25; font-size: 16px;">THÔNG TIN CÁ NHÂN</span>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Họ và tên
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:DropDownList ID="ddlResource" runat="server" Width="200px" AutoPostBack="True" 
                                onselectedindexchanged="ddlResource_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">E-mail
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="txtEmail" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Số điện thoại
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="txtSoDienThoai" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Địa chỉ
                            </span>
                        </label>
                        <div style="float: left;">
                             <asp:Label ID="txtDiaChi" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px; padding-top: 10px; padding-bottom: 3px;">
                    <div style="border-style: solid; border-color: #E44B25;"></div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <span style="color: #E44B25; font-size: 16px;">THÔNG TIN NGHỈ PHÉP</span>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Lý do
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:TextBox ID="txtLydo" runat="server" Style="width: 350px;" Rows="3" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Số ngày nghỉ còn lại:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblRemainDayLeaveOf" runat="server" Text=""></asp:Label>

                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Từ ngày
                            </span>
                        </label>
                        <div style="float: left;">
                            <telerik:RadDateTimePicker ID="txtTuNgay" runat="server">
                                <ClientEvents OnDateSelected="DateSelected" />
                                <Calendar ID="CalendarTuNgay" runat="server" EnableKeyboardNavigation="true" CultureInfo="vi-VN" RangeMinDate="2011-01-01">
                                </Calendar>
                                <DateInput DisplayDateFormat="dd/MM/yyyy HH:mm"></DateInput>
                                <TimeView ID="TimeView1" OnClientTimeSelecting="ClientTimeSelected" runat="server" Columns="3" ShowHeader="false" StartTime="07:00"
									EndTime="20:00" Interval="00:30" TimeFormat="HH:mm"/>
                                <DateInput ToolTip="Date input"></DateInput>
                            </telerik:RadDateTimePicker>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" 
                                ControlToValidate="txtTuNgay" 
                                ErrorMessage="Không được để trống" 
                                ForeColor="Red"
						      CssClass="rsValidatorMsg" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Đến ngày
                            </span>
                        </label>
                        <div style="float: left;">
                            <telerik:RadDateTimePicker ID="txtDenNgay" runat="server" >
                                <ClientEvents OnDateSelected="DateSelected1" />
                                <Calendar ID="CalendarDenNgay" runat="server" EnableKeyboardNavigation="true" CultureInfo="vi-VN" RangeMinDate="2011-01-01" DayStyle="dd/MM/yyyy">
                                </Calendar>
                                <DateInput DisplayDateFormat="dd/MM/yyyy HH:mm"></DateInput>
                                <TimeView ID="TimeView2" OnClientTimeSelecting="ClientTimeSelected1" runat="server" Columns="3" ShowHeader="false" StartTime="07:00"
									EndTime="20:00" Interval="00:30" TimeFormat="HH:mm"/>
                                <DateInput ToolTip="Date input"></DateInput>
                            </telerik:RadDateTimePicker>
                            <asp:RequiredFieldValidator runat="server" ID="NameValidator" 
                                ControlToValidate="txtDenNgay" 
                                ErrorMessage="Không được để trống" 
                                ForeColor="Red"
						      CssClass="rsValidatorMsg" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:CustomValidator runat="server" ID="DurationValidator" ControlToValidate="txtTuNgay"
				                EnableClientScript="false" Display="Dynamic" CssClass="rsValidatorMsg rsInvalid" ErrorMessage="Khoảng thời gian không hợp lệ. Vui lòng nhập lại!"
				                OnServerValidate="CheckValidDuration" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px; padding-top: 10px; padding-bottom: 3px; text-align: center">
                    <telerik:RadButton ID="btnCapNhat" runat="server" Text="Cập nhật" OnClick="btnCapNhat_Click">
                        <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btncancel" runat="server" Text="Bỏ qua"
                        OnClick="btncancel_Click">
                        <Icon PrimaryIconUrl="../../Images/Cancel.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>

                </li>
            </ul>
        </div>


    </form>
</body>
</html>

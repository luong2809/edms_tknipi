﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Scope;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ScopeProjectAttachFiles : Page
    {
        private readonly AttachFilesProjectService attachFilesProjectService;

        private readonly ScopeProjectService scopeProjectService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ScopeProjectAttachFiles()
        {
            this.attachFilesProjectService = new AttachFilesProjectService();
            this.scopeProjectService = new ScopeProjectService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (UserSession.Current.IsEngineer)
                {
                    this.UploadControl.Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                    this.grdDocument.Height = 447;
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.Session.Remove("IsFillData");
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var projectId = Convert.ToInt32(this.Request.QueryString["docId"]);
                const string targetFolder = "../../DocumentLibrary/ProjectFiles";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/ProjectFiles";
                var listUpload = docuploader.UploadedFiles;

                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var docFileName = docFile.FileName;

                        var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;

                        // Path file to save on server disc
                        var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);

                        // Path file to download from server
                        var serverFilePath = serverFolder + "/" + serverDocFileName;
                        var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                        
                        docFile.SaveAs(saveFilePath, true);

                        var attachFile = new AttachFilesProject()
                        {
                            ProjectId = projectId,
                            FileName = docFileName,
                            Extension = fileExt,
                            FilePath = serverFilePath,
                            ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                            FileSize = (double)docFile.ContentLength / 1024,
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedDate = DateTime.Now,
                            IsDefault = true
                        };

                        var defaultAttachFile = this.attachFilesProjectService.GetAllByProject(projectId).FirstOrDefault(t => t.IsDefault.GetValueOrDefault());
                        if (defaultAttachFile != null)
                        {
                            defaultAttachFile.IsDefault = false;
                            this.attachFilesProjectService.Update(defaultAttachFile);
                        }

                        var projectObj = this.scopeProjectService.GetById(attachFile.ProjectId.GetValueOrDefault());
                        if (projectObj != null)
                        {
                            projectObj.DefaultFilePath = attachFile.FilePath;
                            projectObj.FileExtentionIcon = attachFile.ExtensionIcon;
                            this.scopeProjectService.Update(projectObj);
                        }

                        this.attachFilesProjectService.Insert(attachFile);
                    }
                }
            }

            this.docuploader.UploadedFiles.Clear();

            this.grdDocument.Rebind();
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var attachFileObj = this.attachFilesProjectService.GetById(docId);

            if (attachFileObj != null && attachFileObj.IsDefault.GetValueOrDefault())
            {
                var attachFiles = this.attachFilesProjectService.GetAllByProject(attachFileObj.ProjectId.GetValueOrDefault()).Where(t => !t.IsDefault.GetValueOrDefault()).ToList();

                var projectObj = this.scopeProjectService.GetById(attachFileObj.ProjectId.GetValueOrDefault());

                if (attachFiles.Count > 0)
                {
                    attachFiles[0].IsDefault = true;
                    this.attachFilesProjectService.Update(attachFiles[0]);


                    if (projectObj != null)
                    {
                        projectObj.DefaultFilePath = attachFiles[0].FilePath;
                        projectObj.FileExtentionIcon = attachFiles[0].ExtensionIcon;
                        this.scopeProjectService.Update(projectObj);
                    }

                }
                else
                {
                    if (projectObj != null)
                    {
                        projectObj.DefaultFilePath = string.Empty;
                        projectObj.FileExtentionIcon = string.Empty;
                        this.scopeProjectService.Update(projectObj);
                    }
                }
            }

            this.attachFilesProjectService.Delete(docId);
            this.grdDocument.Rebind();
        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var projId = Convert.ToInt32(Request.QueryString["docId"]);
                this.grdDocument.DataSource = this.attachFilesProjectService.GetAllByProject(projId).OrderByDescending(t => t.CreatedDate).ThenByDescending(t => t.FileName);
            }
            else
            {
                this.grdDocument.DataSource = new List<AttachFilesProject>();
            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            throw new NotImplementedException();
        }

        protected void rbtnDefaultDoc_CheckedChanged(object sender, EventArgs e)
        {
            ((GridItem)((RadioButton)sender).Parent.Parent).Selected = ((RadioButton)sender).Checked;

            var item = ((RadioButton)sender).Parent.Parent as GridDataItem;
            var attachFileId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var attachFileObj = this.attachFilesProjectService.GetById(attachFileId);
            if (attachFileObj != null)
            {
                var attachFiles = this.attachFilesProjectService.GetAllByProject(attachFileObj.ProjectId.GetValueOrDefault());
                foreach (var attachFile in attachFiles)
                {
                    attachFile.IsDefault = attachFile.ID == attachFileId;

                    if (attachFile.IsDefault.GetValueOrDefault())
                    {
                        var projectObj = this.scopeProjectService.GetById(attachFile.ProjectId.GetValueOrDefault());
                        if (projectObj != null)
                        {
                            projectObj.DefaultFilePath = attachFile.FilePath;
                            projectObj.FileExtentionIcon = attachFile.ExtensionIcon;
                            this.scopeProjectService.Update(projectObj);
                        }
                    }

                    this.attachFilesProjectService.Update(attachFile);
                }
            }
        }
    }
}
﻿

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
    public class UserManhourMonthDAO : BaseDAO
    {

        public UserManhourMonthDAO() : base() { }

        public IQueryable<UserManhourMonth> GetIQueryable()
        {
            return this.EDMsDataContext.UserManhourMonths;
        }
        public List<UserManhourMonth> GetAll()
        {
            return this.EDMsDataContext.UserManhourMonths.OrderByDescending(t => t.ID).ToList();
        }

        public List<UserManhourMonth> GetByUserID(int userID, int month, int year)
        {
            return this.EDMsDataContext.UserManhourMonths.Where(ob => ob.UserID == userID && ob.Month == month && ob.Year == year).OrderByDescending(t => t.ID).ToList();
        }

        public List<UserManhourMonth> GetByUserID(List<int> userID, int month, int year)
        {
            return this.EDMsDataContext.UserManhourMonths.Where(ob => userID.Contains(ob.UserID.Value) && ob.Month == month && ob.Year == year).OrderByDescending(t => t.ID).ToList();
        }

        public List<UserManhourMonth> GetByDocIDAndUserID(int documentID, int userID)
        {
            return this.EDMsDataContext.UserManhourMonths.Where(ob => ob.DocumentID == documentID && ob.UserID == userID).OrderByDescending(t => t.ID).ToList();
        }

        public UserManhourMonth GetByDocIDAndUserID(int documentID, int userID, int month, int year)
        {
            return this.EDMsDataContext.UserManhourMonths.FirstOrDefault(ob => ob.DocumentID == documentID && ob.UserID == userID && ob.Month == month && ob.Year == year);
        }

        public UserManhourMonth GetLastedByDocIDAndUserID(int documentID, int userID, int month, int year)
        {
            return this.EDMsDataContext.UserManhourMonths.OrderByDescending(ob => ob.ID).FirstOrDefault(ob => ob.DocumentID == documentID && ob.UserID == userID && ob.Month < month && ob.Year <= year);
        }

        public List<UserManhourMonth> GetByDocID(int documentID, int month, int year)
        {
            return this.EDMsDataContext.UserManhourMonths.Where(ob => ob.DocumentID == documentID && ob.Month == month && ob.Year == year).ToList();
        }

        public UserManhourMonth GetById(int id)
        {
            return this.EDMsDataContext.UserManhourMonths.FirstOrDefault(ob => ob.ID == id);
        }

        public int? Insert(UserManhourMonth ob)
        {
            try
            {
                this.EDMsDataContext.AddToUserManhourMonths(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }


        public bool Update(UserManhourMonth src)
        {
            try
            {
                UserManhourMonth des = (from rs in this.EDMsDataContext.UserManhourMonths
                                        where rs.ID == src.ID
                                        select rs).First();

                des.UserID = src.UserID;
                des.UserName = src.UserName;
                des.DeparmentID = src.DeparmentID;
                des.DocumentID = src.DocumentID;
                des.DocumentName = src.DocumentName;
                des.Month = src.Month;
                des.Year = src.Year;
                des.ManhourActualMonth = src.ManhourActualMonth;
                des.CompleteMonth = src.CompleteMonth;
                des.ManhourActualTotal = src.ManhourActualTotal;
                des.CompleteTotal = src.CompleteTotal;
                des.UpdateBy = src.UpdateBy;
                des.UpdateDate = src.UpdateDate;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }



        public bool Delete(UserManhourMonth src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }


        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

    }
}

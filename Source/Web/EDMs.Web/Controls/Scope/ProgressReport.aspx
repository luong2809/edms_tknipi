﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProgressReport.aspx.cs" Inherits="EDMs.Web.Controls.Scope.ProgressReport" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        
        html, body, form {
            overflow-y: auto !important;
        }

        div.RadGrid .rgPager .rgAdvPart     
        {     
            display:none;
        }
        div.rgDataDiv {
            overflow: auto !important;
        }

        .DropZone1
        {
            width: 300px;
            height: 250px;
            padding-left: 230px;
            background: #fff url(../../Images/placeholder-add.png) no-repeat center center;
            background-color: #357A2B;
            border-color: #CCCCCC;
            color: #767676;
            float: left;
            text-align: center;
            font-size: 16px;
            color: white;
            position: relative;
        }

           
    </style>

    <script src="~/Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }


            </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <div style="width: 100%" >
            <telerik:RadHtmlChart runat="server" ID="LineChart" Width="900" Height="500" Transitions="true" >
                    <Appearance>
                            <FillStyle BackgroundColor="White"></FillStyle>
                    </Appearance>
                    <ChartTitle >
                            <Appearance Align="Center" BackgroundColor="White" Position="Top">
                            </Appearance>
                    </ChartTitle>
                    <Legend>
                            <Appearance BackgroundColor="White" Position="Bottom">
                            </Appearance>
                    </Legend>
                    <PlotArea>
                            <Appearance>
                                <FillStyle BackgroundColor="White"></FillStyle>
                            </Appearance>
                            <XAxis  Color="#b3b3b3" DataLabelsField="STRWeekDate" AxisCrossingValue="0" MajorTickType="Outside" MinorTickType="Outside"
                    Reversed="false">
                                <LabelsAppearance DataFormatString="{0}" RotationAngle="0" Skip="0" Step="1" />
                                <MajorGridLines Color="#EFEFEF" Width="1"/>
                                <MinorGridLines Color="#F7F7F7" Width="1"/>
                                <TitleAppearance Text="MONTH"/>
                             
                            </XAxis>
                            <YAxis AxisCrossingValue="0" Color="#b3b3b3" MajorTickSize="1" MajorTickType="Outside"
                                MaxValue="100" MinorTickSize="1" MinorTickType="Outside" MinValue="0" Reversed="false"
                                Step="10">
                                <LabelsAppearance DataFormatString="{0}%" RotationAngle="0" Skip="0" Step="1"/>
                              
                                <MajorGridLines Color="#EFEFEF" Width="1"/>
                                <MinorGridLines Color="#F7F7F7" Width="1"/>
                                <TitleAppearance Position="Center" RotationAngle="0" Text=""/>
                              
                            </YAxis>
                            <Series>
                                <telerik:LineSeries Name="Planed" DataFieldY="Planed" >
                                    <Appearance>
                                        <FillStyle BackgroundColor="#5ab7de"></FillStyle>
                                    </Appearance>
                                    <LabelsAppearance DataFormatString="{0}" Position="Below" Visible="false"/>
                                   
                                    <LineAppearance Width="2" />
                                    <MarkersAppearance MarkersType="Circle" BackgroundColor="White" Size="6" BorderColor="#5ab7de" BorderWidth="2"/>
                                    <TooltipsAppearance >
                                        <ClientTemplate>
                                            Planed of  #= kendo.format(\'{0:d}\', category) #: #= value #%
                                        </ClientTemplate>

                                    </TooltipsAppearance>
                                </telerik:LineSeries>
                                <telerik:LineSeries Name="Actual" MissingValues="Interpolate">
                                    <Appearance>
                                        <FillStyle BackgroundColor="#FF3300"></FillStyle>
                                    </Appearance>
                                    <LabelsAppearance DataFormatString="{0}" Position="Above" Visible="false">
                                    </LabelsAppearance>
                                    <LineAppearance Width="2" />
                                    <MarkersAppearance MarkersType="Square" BackgroundColor="#FF3300" Size="6" BorderColor="#FF3300"
                                        BorderWidth="2"></MarkersAppearance>
                                    <TooltipsAppearance>
                                        <ClientTemplate>
                                            Actual of  #= kendo.format(\'{0:d}\', category) #: #= value #%
                                        </ClientTemplate>
                                    </TooltipsAppearance>
                                </telerik:LineSeries>
                            </Series>
                    </PlotArea>
                </telerik:RadHtmlChart>
        </div>
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings> 
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="LineChart" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;

          </script>

        </telerik:RadScriptBlock>
    </form>
</body>
</html>

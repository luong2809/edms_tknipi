﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScopeProjectEditForm.aspx.cs" Inherits="EDMs.Web.Controls.Scope.ScopeProjectEditForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, form {
            overflow: visible;
        }

        .RadComboBoxDropDown_Windows7 {
            width: 288px !important;
            height: 200px !important;
        }

        .RadComboBoxDropDown .rcbScroll {
            height: 199px !important;
        }

        .RadComboBoxDropDown_Default .rcbHovered {
            background-color: #46A3D3;
            color: #fff;
        }

        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
            margin: 0 0px;
        }

        .RadComboBox .rcbInputCell .rcbInput {
            border-left-color: #46A3D3 !important;
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 283px;
        }

        .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
            padding-left: 0px !important;
        }

        div.rgEditForm label {
            float: right;
            text-align: right;
            width: 72px;
        }

        .rgEditForm {
            text-align: right;
        }

        .RadComboBox {
            width: 115px !important;
            border-bottom: none !important;
        }

        .RadUpload .ruFileWrap {
            overflow: visible !important;
        }

        .styleIcon {
            margin-left: -23px;
            margin-top: 3px;
        }

        .accordion dt a {
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .accordion dt span {
            color: #085B8F;
            border-bottom: 1px solid #46A3D3;
            font-size: 10pt;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }
    </style>

    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }


    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <div style="width: 100%">
            <ul style="list-style-type: none">
                <div class="qlcbFormItem">
                    <div class="dnnFormMessage dnnFormInfo">
                        <div class="dnnFormItem dnnFormHelp dnnClear">
                            <p class="dnnFormRequired" style="float: left;">
                                <span style="text-decoration: underline;">Notes</span>: All fields marked with a red are required.
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
            </ul>
        </div>

        <div style="width: 100%" runat="server" id="divContent">
            <ul style="list-style-type: none">

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Block
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlBlock" runat="server" CssClass="min25Percent" Style="width: 315px; max-width: 315px" AutoPostBack="True" OnSelectedIndexChanged="ddlBlock_SelectedIndexChanged" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Field
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlField" runat="server" CssClass="min25Percent" Style="width: 315px; max-width: 315px" AutoPostBack="True" OnSelectedIndexChanged="ddlField_SelectedIndexChange" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Platform
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlPlatform" runat="server" CssClass="min25Percent" Style="width: 315px; max-width: 315px" AutoPostBack="True" OnSelectedIndexChanged="ddlPlatform_SelectedIndexChange" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Project Number
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divFileName">
                            <asp:TextBox ID="txtName" runat="server" Style="width: 283px; padding-right: 21px !important;" CssClass="min25Percent qlcbFormRequired" />
                            <asp:ImageButton ID="btnAutoGenerate" runat="server" ImageUrl="~/Images/autogenerate.png" ToolTip="Re-generate Project Number" OnClick="btnAutoGenerate_Click" ImageAlign="AbsMiddle" CssClass="styleIcon" />

                            <asp:CustomValidator runat="server" ID="fileNameValidator" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="ServerValidationFileNameIsExist" Display="Dynamic" ControlToValidate="txtName" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Project Name
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divDescription">
                            <asp:TextBox ID="txtDescription" runat="server" Style="width: 300px;" CssClass="min25Percent qlcbFormRequired" TextMode="MultiLine" Rows="3" />
                            <asp:CustomValidator runat="server" ID="descriptionValidate" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="descriptionValidate_ServerValidate" Display="Dynamic" ControlToValidate="txtDescription" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <dl class="accordion">
                    <dt style="width: 100%;">
                        <span>Order Infomation</span>
                    </dt>
                </dl>
                <%--<li style="width: 500px; display:none">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Order type
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divOrderType">
                            <asp:DropDownList ID="ddlOderType" AutoPostBack="true" OnSelectedIndexChanged="ddlOderType_SelectedIndexChanged" runat="server" CssClass="min25Percent" Style="width: 315px; max-width: 315px">
                                <asp:ListItem Text="" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Hạng mục theo kế hoạch của hội đồng hàng năm(Projects in Council Annual Plan)" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Hạng mục chuẩn bị kế hoạch của hội đồng hàng năm( Projects Prepare for Annual Plan of Council)" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Đề tài nghiên cứu khoa học( Research)" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Hạng mục ngoài kế hoạch(Unplanned Projects)" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:CustomValidator runat="server" ID="validateOrderType" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="validateOrderType_ServerValidate" Display="Dynamic" ControlToValidate="ddlOderType" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;" runat="server" visible="false" id="IdexOrderCode">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Order Code
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divPlanName">
                            <asp:TextBox ID="txtPlanNane" runat="server" Style="width: 300px;" CssClass="min25Percent" TextMode="MultiLine" Rows="3" />

                            <asp:CustomValidator runat="server" ID="vldPlan" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="vldPlan_ServerValidate" Display="Dynamic" ControlToValidate="txtPlanNane" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;" runat="server" visible="false" id="IndexAppendix">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Appendix
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" id="divAppendix" runat="server">
                            <asp:DropDownList ID="ddlAppendix" runat="server" CssClass="min25Percent" Style="width: 315px; max-width: 315px">
                                <asp:ListItem Text="" Value="4"></asp:ListItem>
                                <asp:ListItem Text="10 - Khảo sát TK" Value="0"></asp:ListItem>
                                <asp:ListItem Text="10.1 - Khảo sát TK" Value="1"></asp:ListItem>
                                <asp:ListItem Text="11" Value="2"></asp:ListItem>
                                <asp:ListItem Text="12 - Sửa chữa" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Other" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:CustomValidator runat="server" ID="vldAppendix" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="vldAppendix_ServerValidate" Display="Dynamic" ControlToValidate="ddlAppendix" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;" runat="server" visible="false" id="IndexExpected">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Expected list
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlExpectedlist" runat="server" CssClass="min25Percent" Style="width: 315px; max-width: 315px">
                                <asp:ListItem Text="Danh mục dự kiến công trình biển để lập tài liệu TKDT cho năm(For New Offshore Platforms) " Value="0"></asp:ListItem>
                                <asp:ListItem Text="Danh mục dự kiến công việc khảo sát sửa chữa để lập tài liệu TKDT cho năm( For Inspection and Reparation) " Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;" runat="server" visible="false" id="IndexOrderYear">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Year
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="txtOrderYear" runat="server" CssClass="min25Percent" Width="150px" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>--%>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Year
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="DivYear">
                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="min25Percent qlcbFormRequired" Width="150px" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true" />
                            <asp:CustomValidator runat="server" ID="CustomValidatorYear" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="CustomValidatorYear_ServerValidate" Display="Dynamic" ControlToValidate="ddlYear" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;"></span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:RadioButtonList ID="rblTypeWork" runat="server" OnSelectedIndexChanged="rblTypeWork_SelectedIndexChanged" RepeatDirection="Horizontal" AutoPostBack="true">
                                <asp:ListItem Text="Planned" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Unplanned" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Type of Work
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="" runat="server" id="DivWork">
                            <telerik:RadComboBox ID="rcbWorkList" runat="server" Skin="Windows7" InputCssClass=""
                                OnClientDropDownOpened="ddlWorkHandler" AllowCustomText="true">
                                <ItemTemplate>
                                    <div id="div4">
                                        <telerik:RadTreeView runat="server" ID="rtvWorkList" Skin="Windows7"
                                            OnClientNodeClicking="rtvWorkNodeClicking" OnNodeClick="rtvWorkList_NodeClick" ShowLineImages="false">
                                            <DataBindings>
                                                <telerik:RadTreeNodeBinding Expanded="true"></telerik:RadTreeNodeBinding>
                                            </DataBindings>
                                        </telerik:RadTreeView>
                                    </div>
                                </ItemTemplate>
                                <Items>
                                    <telerik:RadComboBoxItem Text="" />
                                </Items>
                            </telerik:RadComboBox>
                            <asp:CustomValidator runat="server" ID="CustomValidatorWork" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="CustomValidatorWork_ServerValidate" Display="Dynamic" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Appendix List
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" runat="server" class="qlcbFormItem" id="divAppendixList">
                            <telerik:RadComboBox ID="rcbAppendixList" runat="server" Skin="Windows7" InputCssClass="min25Percent qlcbFormRequired"
                                OnClientDropDownOpened="ddlAppendixHandler" AllowCustomText="true">
                                <ItemTemplate>
                                    <div id="div4">
                                        <telerik:RadTreeView runat="server" ID="rtvAppendixList" Skin="Windows7"
                                            OnClientNodeClicking="rtvAppendixNodeClicking" ShowLineImages="false">
                                            <DataBindings>
                                                <telerik:RadTreeNodeBinding Expanded="true"></telerik:RadTreeNodeBinding>
                                            </DataBindings>
                                        </telerik:RadTreeView>
                                    </div>
                                </ItemTemplate>
                                <Items>
                                    <telerik:RadComboBoxItem Text="" />
                                </Items>
                            </telerik:RadComboBox>
                            <asp:CustomValidator runat="server" ID="vldAppendixList" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="vldAppendixList_ServerValidate" Display="Dynamic" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Basis of Project
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divBasis">
                            <asp:TextBox ID="txtBasis" runat="server" Style="width: 300px;" CssClass="min25Percent qlcbFormRequired" />
                            <asp:CustomValidator runat="server" ID="vldBasis" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="vldBasis_ServerValidate" Display="Dynamic" ControlToValidate="txtBasis" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;" runat="server" visible="false" id="IndexOrderRevision">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Order Revision
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="div1">
                            <asp:TextBox ID="txtOrderRevision" runat="server" Style="width: 300px;" CssClass="min25Percent" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <dl class="accordion">
                    <dt style="width: 100%;">
                        <span></span>
                    </dt>
                </dl>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Transfer Document of Project
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtTransferDoc" runat="server" Style="width: 300px;" CssClass="min25Percent"
                                TextMode="MultiLine" Rows="5" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Scope of work
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtScopeOfWork" runat="server" Style="width: 300px;" CssClass="min25Percent"
                                TextMode="MultiLine" Rows="5" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px; display: none">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Year
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadNumericTextBox ID="txtYear" CssClass="min25Percent" IncrementSettings-InterceptArrowKeys="true" IncrementSettings-InterceptMouseWheel="true" runat="server" Width="70px" MinValue="0">
                                <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="0" GroupSeparator="" />
                                <EnabledStyle HorizontalAlign="Right" />
                            </telerik:RadNumericTextBox>

                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <%--<li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Project type
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlProjectType" runat="server" CssClass="min25Percent" Style="width: 315px; max-width: 315px">
                                <asp:ListItem Text="" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Planned" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Unplanned" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>--%>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Engineering Manager (GIP)
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlProjectManager" runat="server" CssClass="min25Percent" Style="width: 315px; max-width: 315px" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Project Manager
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" runat="server" id="divPM" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlSupervisor" runat="server" CssClass="min25Percent qlcbFormRequired" Style="width: 315px; max-width: 315px" />
                            <asp:CustomValidator runat="server" ID="validatePM" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="validatePM_ServerValidate" Display="Dynamic" ControlToValidate="ddlSupervisor" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Document Controller (DC)
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddldocumentcontroller" runat="server" CssClass="min25Percent" Style="width: 315px; max-width: 315px" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Start Date
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divStatdate">
                            <telerik:RadDatePicker ID="txtStartDate" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormRequired" />
                            </telerik:RadDatePicker>
                            <asp:CustomValidator runat="server" ID="vldStartDate" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="vldStartDate_ServerValidate" Display="Dynamic" ControlToValidate="txtStartDate" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Deadline
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divDeadline">
                            <telerik:RadDatePicker ID="txtDeadline" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormRequired" />
                            </telerik:RadDatePicker>

                            <asp:CustomValidator runat="server" ID="vldDeadline" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="vldDeadline_ServerValidate" Display="Dynamic" ControlToValidate="txtDeadline" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Finish Date
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtEndDate" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Notes
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtNotes" runat="server" Style="width: 300px;" CssClass="min25Percent"
                                TextMode="MultiLine" Rows="3" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Percent complete
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadNumericTextBox ID="txtPercentComplete" CssClass="min25Percent" IncrementSettings-InterceptArrowKeys="true" IncrementSettings-InterceptMouseWheel="true" runat="server" Width="70px" MinValue="0" MaxValue="100">
                                <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSeparator="" PositivePattern="n %" />
                                <EnabledStyle HorizontalAlign="Right" />
                            </telerik:RadNumericTextBox>
                            <asp:CheckBox ID="cbAutoCalculate" runat="server" Text="Auto calculate" Checked="True"
                                OnCheckedChanged="cbAutoCalculate_OnCheckedChanged" AutoPostBack="True" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px; display: none;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Plan Name
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtPlanNaness" runat="server" Style="width: 300px;" CssClass="min25Percent"
                                TextMode="MultiLine" Rows="3" />

                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;"></span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:CheckBox ID="cbWeeklyProgress" runat="server" Text="Weekly Progress" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;"></span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:CheckBox ID="cbAutoCalculateWeight" runat="server" Text="Auto calculate weight" Checked="True" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;"></span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:CheckBox ID="cbIDC" runat="server" Text="IDC Check" Checked="False" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 100%;" runat="server" id="UploadControl">
                    <div>
                        <label style="width: 150px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Upload files
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divFileUpload">
                            <telerik:RadAsyncUpload runat="server" ID="docuploader"
                                MultipleFileSelection="Disabled" TemporaryFileExpiration="05:00:00"
                                EnableInlineProgress="true" Width="350px"
                                Localization-Cancel="Cancel" CssClass="min25Percent qlcbFormRequired"
                                Localization-Remove="Remove" Localization-Select="Select" Skin="Windows7">
                            </telerik:RadAsyncUpload>
                            <asp:CustomValidator runat="server" ID="fileUploadValidator" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="fileUploadValidator_ServerValidate" Display="Dynamic" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <div class="qlcbFormItem" runat="server" id="CreatedInfo" visible="False">
                    <div class="dnnFormMessage dnnFormInfo">
                        <div class="dnnFormItem dnnFormHelp dnnClear">
                            <p class="dnnFormRequired" style="float: left;">
                                <asp:Label ID="lblCreated" runat="server"></asp:Label>
                                <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
            </ul>
        </div>

        <div id="divExecuted" runat="server" style="width: 100%;">
            <ul style="list-style-type: none">
                <li style="width: 500px; padding-top: 10px; padding-bottom: 3px; text-align: center">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" Width="70px" Style="text-align: center">
                        <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                    <%--<telerik:RadButton ID="btncancel" runat="server" Text="Cancel" Width="70px" style="text-align: center"
                        OnClick="btncancel_Click">
                        <Icon PrimaryIconUrl="../../Images/Cancel.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>--%>

                </li>
            </ul>
        </div>
        <asp:HiddenField runat="server" ID="docUploadedIsExist" />
        <asp:HiddenField runat="server" ID="docIdUpdateUnIsLeaf" />
        <asp:HiddenField runat="server" ID="hidPlanName" />
        <asp:HiddenField runat="server" ID="treeViewAppendixStatus" />
        <asp:HiddenField runat="server" ID="treeViewWorkStatus" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Width="100%" Height="100px">
            <asp:Image ID="Image1" runat="server" Width="160px" Height="20px" ImageUrl="~/Images/loading1.gif"></asp:Image>
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" />
                        <telerik:AjaxUpdatedControl ControlID="divExecuted" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlField">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtName" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlYear">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rcbWorkList" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="rcbAppendixList" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="rblTypeWork">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rcbWorkList" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="rcbAppendixList" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="rcbWorkList">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rcbAppendixList" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlOderType">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="IdexOrderCode" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="IndexAppendix" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="IndexExpected" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="IndexOrderYear" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="IndexOrderRevision" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnAutoGenerate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtName" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="cbAutoCalculate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtPercentComplete" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;
                function OnClientFilesUploaded(sender, args) {
                    var name = args.get_fileName();
                    document.getElementById("txtName").value = name;
                    $find('<%=ajaxDocument.ClientID %>').ajaxRequest();
                }

                function pageLoad() {
                    ajaxManager = $find("<%=ajaxDocument.ClientID %>");
                    var appendixStatus = document.getElementById("<%= treeViewAppendixStatus.ClientID%>").value;
                    var workStatus = document.getElementById("<%= treeViewWorkStatus.ClientID%>").value;
                    let params = (new URL(document.location)).searchParams;
                    let name = params.get("disId");
                    if (name != null) {
                        if (appendixStatus == "1") {
                            var comboBox = $find("<%= rcbAppendixList.ClientID %>");
                            //var tree = comboBox.get_items().getItem(0).findControl("rtvSystem");
                            var tree = $telerik.findControl(comboBox.get_element().parentNode, "rtvAppendixList");
                            if (tree != null) {
                                var node = tree.get_selectedNode();
                                if (node != null) {
                                    comboBox.set_text(node.get_text());
                                    comboBox.trackChanges();
                                    comboBox.get_items().getItem(0).set_text(node.get_text());
                                    comboBox.get_items().getItem(0).set_value(node.get_value());
                                    comboBox.commitChanges();
                                }
                            }
                        }
                        if (workStatus == "1") {
                            var comboBox = $find("<%= rcbWorkList.ClientID %>");
                            //var tree = comboBox.get_items().getItem(0).findControl("rtvSystem");
                            var tree = $telerik.findControl(comboBox.get_element().parentNode, "rtvWorkList");
                            if (tree != null) {
                                var node = tree.get_selectedNode();
                                if (node != null) {
                                    comboBox.set_text(node.get_text());
                                    comboBox.trackChanges();
                                    comboBox.get_items().getItem(0).set_text(node.get_text());
                                    comboBox.get_items().getItem(0).set_value(node.get_value());
                                    comboBox.commitChanges();
                                }
                            }
                        }
                    }

                }

                function ValidateForm() {
                    var name = document.getElementById("<%= txtName.ClientID%>");
                    if (name.value.trim() == "") {
                        alert("Please enter file name.");
                        name.focus();
                        return false;
                    }
                }

                function ddlAppendixHandler(sender, eventArgs) {
                    var tree = sender.get_items().getItem(0).findControl("rtvAppendixList");
                    var selectedNode = tree.get_selectedNode();
                    if (selectedNode) {
                        selectedNode.scrollIntoView();
                    }
                }

                function ddlWorkHandler(sender, eventArgs) {
                    var tree = sender.get_items().getItem(0).findControl("rtvWorkList");
                    var selectedNode = tree.get_selectedNode();
                    if (selectedNode) {
                        selectedNode.scrollIntoView();
                    }
                }

                function rtvAppendixNodeClicking(sender, args) {
                    var comboBox = $find("<%= rcbAppendixList.ClientID %>");

                    var node = args.get_node();
                    comboBox.set_text(node.get_text());
                    comboBox.trackChanges();
                    comboBox.get_items().getItem(0).set_text(node.get_text());
                    comboBox.get_items().getItem(0).set_value(node.get_value());
                    comboBox.commitChanges();
                    comboBox.hideDropDown();
                }

                function rtvWorkNodeClicking(sender, args) {
                    var comboBox = $find("<%= rcbWorkList.ClientID %>");

                    var node = args.get_node();
                    comboBox.set_text(node.get_text());
                    comboBox.trackChanges();
                    comboBox.get_items().getItem(0).set_text(node.get_text());
                    comboBox.get_items().getItem(0).set_value(node.get_value());
                    comboBox.commitChanges();
                    comboBox.hideDropDown();
                }

               <%-- function loadNodePostBack()
                {
                    var comboBox = $find("<%= ddlSystem.ClientID %>");
                    var tree = sender.get_items().getItem(0).findControl("rtvSystem");
                    var node = tree.get_selectedNode();
                    comboBox.set_text(node.get_text());
                    comboBox.trackChanges();
                    comboBox.get_items().getItem(0).set_text(node.get_text());
                    comboBox.get_items().getItem(0).set_value(node.get_value());
                    comboBox.commitChanges();
                }--%>

                function fileUploading(sender, args) {
                    var name = args.get_fileName();
                    document.getElementById("txtName").value = name;

                    ajaxManager.ajaxRequest("CheckFileName$" + name);
                }

            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>

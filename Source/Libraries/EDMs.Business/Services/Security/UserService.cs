﻿namespace EDMs.Business.Services.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
    using EDMs.Data.DAO.Security;

    public class UserService
    {      
        private readonly UserDAO repo;

        public UserService()
        {
            this.repo = new UserDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All User
        /// </summary>
        /// <returns></returns>
        public List<User> GetAll(bool isHost)
        {
            return isHost
                       ? this.repo.GetAll()
                       : this.repo.GetAll().Where(t => t.Id != 1).ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get User By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public User GetByID(string ID)
        {
            return this.repo.GetByID(Convert.ToInt32(ID));
            //return patientDAO.GetByID(ID);
        }

        public User GetByID(int ID)
        {
            return this.repo.GetByID(ID);
            //return patientDAO.GetByID(ID);
        }

        public User GetByUserName(string userName)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.Username == userName);
        }

        public User GetByUserFullName(string userFullName)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.UserNameWithFullName == userFullName);
        }
        #endregion

        #region Get (Advances)

        public List<User>  GetAllByRoleId(int roleId)
        {
            return this.repo.GetAllByRoleId(roleId);
        }

        public List<User> GetAllByRoleId(List<int> roleIds)
        {
            return this.repo.GetAll().Where(t => roleIds.Contains(t.RoleId.GetValueOrDefault())).ToList();
        }

        public List<User> GetByListID(List<int> listID)
        {
            return this.repo.GetByListID(listID);
        }

        public User GetUserByUsername(string username)
        {
            return this.repo.GetUserByUsername(username);
        }

        public List<User> GetSpecialListUser(List<int> roleIds)
        {
            return this.repo.GetSpecialListUser(roleIds);
        }

        public User GetByResourceId(int resourceId)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.ResourceId == resourceId);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        public bool ChangePassword(int userId, string newPassword)
        {
            return this.repo.ChangePassword(userId, newPassword);
        }

        /// <summary>
        /// Insert User
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(User bo)
        {
            try
            {
                return this.repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(User bo)
        {
            try
            {
                return this.repo.Update(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(User bo)
        {
            try
            {
                return this.repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete User By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return this.repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Checks the exists.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        public bool CheckExists(int? userId, string userName)
        {
            return this.repo.CheckExists(userId, userName);
        }
    }
}

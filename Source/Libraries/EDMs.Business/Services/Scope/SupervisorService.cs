﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Business.Services.Library;
using EDMs.Data.DAO.Scope;
using EDMs.Data.Entities;

namespace EDMs.Business.Services.Scope
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class SupervisorService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly SupervisorDAO repo;

        private readonly PermissionWorkgroupService PermissionWorkgroupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SupervisorService"/> class.
        /// </summary>
        public SupervisorService()
        {
            this.repo = new SupervisorDAO();
            this.PermissionWorkgroupService = new PermissionWorkgroupService();
        }

        #region Get (Advances)

        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Supervisor> GetAll()
        {
            return this.repo.GetAll().ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Supervisor GetById(int id)
        {
            return this.repo.GetById(id);
        }

        public Supervisor GetByName(string name)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.Name == name);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Supervisor bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Supervisor bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Supervisor bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web.Controls.Scope
{
    using System;
    using System.Web.UI;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class WorkpackageMilestoneEditForm : Page
    {
        private readonly MilestoneService milestoneService;
        private readonly WorkGroupService workGroupService;
        private readonly UserService userService;
        private Milestone milestoneObj
        {
            get
            {
                return this.milestoneService.GetById(Convert.ToInt32(Request.QueryString["milestoneId"]));
            }
        }
        private WorkGroup wpObj
        {
            get
            {
                return this.workGroupService.GetById(this.milestoneObj.WorkpackageId.GetValueOrDefault());
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public WorkpackageMilestoneEditForm()
        {
            this.userService = new UserService();
            this.milestoneService = new MilestoneService();
            this.workGroupService = new WorkGroupService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["milestoneId"]))
                {
                    if (this.milestoneObj != null)
                    {
                        this.txtWorkpackageName.Text = this.wpObj != null ? wpObj.Name : string.Empty;
                        this.txtDescription.Text = this.wpObj != null ? wpObj.Description : string.Empty;

                        this.txtMilestone.SelectedDate = this.milestoneObj.MilestoneDate;
                        this.txtPlan.Value = this.milestoneObj.PlanPercent;
                        this.txtPlanTotal.Value = this.milestoneObj.PlanTotal;
                        this.txtPlanOfYear.Value = this.milestoneObj.PlanOfYear;
                        this.txtReal.Value = this.milestoneObj.Real;
                        this.txtRealTotal.Value = this.milestoneObj.RealTotal;
                        this.txtPerformingUser.Text = this.milestoneObj.PerformingUser;
                        this.txtNote.Text = this.milestoneObj.Note;
                        this.txtDescriptionOfDuty.Text = this.milestoneObj.DescriptionOfDuty;

                        var createdUser = this.userService.GetByID(this.milestoneObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + this.milestoneObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (this.milestoneObj.UpdatedBy != null && this.milestoneObj.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(this.milestoneObj.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + this.milestoneObj.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["milestoneId"]))
                {
                    var obj = this.milestoneService.GetById(Convert.ToInt32(this.Request.QueryString["milestoneId"]));
                    if (obj != null)
                    {
                        this.CollectData(ref obj);

                        obj.UpdatedBy = UserSession.Current.User.Id;
                        obj.UpdatedDate = DateTime.Now;

                        this.milestoneService.Update(obj);

                        if (obj.MilestoneDate.GetValueOrDefault().Month == DateTime.Now.Month
                            && obj.MilestoneDate.GetValueOrDefault().Year == DateTime.Now.Year
                            && !this.wpObj.IsAutoCalculate.GetValueOrDefault())
                        {
                            this.wpObj.Complete = obj.RealTotal;
                            this.workGroupService.Update(this.wpObj);
                        }

                        // Auto add next plan
                        var milestoneList = this.milestoneService.GetAllByWorkpackage(obj.WorkpackageId.GetValueOrDefault()).OrderByDescending(t => t.MilestoneDate).ToList();
                        if (this.txtPlannextMonth.Value != null && this.txtPlannextMonth.Value != 0 &&
                            milestoneList.Any()
                            && obj.PlanTotal < 100 && obj.RealTotal < 100
                            && milestoneList[0].MilestoneDate != null
                            && DateTime.ParseExact("01/" + milestoneList[0].MilestoneDate.Value.ToString("MM/yyyy"), "dd/MM/yyyy", null) < DateTime.ParseExact("01/" + DateTime.Now.AddMonths(1).ToString("MM/yyyy"), "dd/MM/yyyy", null)
                            && Utility.DifferentMonth(DateTime.ParseExact("01/" + milestoneList[0].MilestoneDate.Value.ToString("MM/yyyy"), "dd/MM/yyyy", null), DateTime.ParseExact("01/" + DateTime.Now.AddMonths(1).ToString("MM/yyyy"), "dd/MM/yyyy", null)) < 2)
                        {
                            var nextPlan = new Milestone();
                            nextPlan.MilestoneDate = this.txtMilestone.SelectedDate!= null
                                ? this.txtMilestone.SelectedDate.GetValueOrDefault().AddMonths(1)
                                :(DateTime?) null;
                            nextPlan.PlanPercent = this.txtPlannextMonth.Value;
                            nextPlan.PlanTotal = this.txtPlannextMonth.Value.GetValueOrDefault() + this.wpObj.Complete.GetValueOrDefault();
                            nextPlan.RealTotal = 0;
                            nextPlan.Real = 0;
                            nextPlan.PerformingUser = this.txtPerformingUser.Text.Trim();
                            nextPlan.Note = this.txtNote.Text.Trim();
                            nextPlan.WorkpackageId = obj.WorkpackageId;
                            nextPlan.WorkpackageName = obj.WorkpackageName;

                            nextPlan.CreatedBy = UserSession.Current.User.Id;
                            nextPlan.CreatedDate = DateTime.Now;

                            this.milestoneService.Insert(nextPlan);
                        }
                    }
                }
                ////else
                ////{
                ////    var obj = new Milestone();
                ////    this.CollectData(ref obj);
                ////    this.milestoneService.Insert(obj);
                ////}

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }


        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
        
        private void CollectData(ref Milestone obj)
        {
            obj.MilestoneDate = this.txtMilestone.SelectedDate;
            obj.PlanPercent = this.txtPlan.Value;
            obj.PlanTotal = this.txtPlanTotal.Value;

            obj.PlanOfYear = this.txtPlanOfYear.Value;

            obj.RealTotal = this.txtRealTotal.Value;
            obj.Real = this.txtReal.Value;
            obj.PerformingUser = this.txtPerformingUser.Text.Trim();
            obj.Note = this.txtNote.Text.Trim();
            obj.DescriptionOfDuty = this.txtDescriptionOfDuty.Text.Trim();
        }

        protected void txtReal_OnTextChanged(object sender, EventArgs e)
        {
            if (wpObj != null)
            {
                this.txtRealTotal.Value = this.txtReal.Value + wpObj.Complete.GetValueOrDefault();
            }
        }

        protected void txtPlan_OnTextChanged(object sender, EventArgs e)
        {
            if (wpObj != null)
            {
                this.txtPlanTotal.Value = this.txtPlan.Value + wpObj.Complete.GetValueOrDefault();
            }
        }
    }
}
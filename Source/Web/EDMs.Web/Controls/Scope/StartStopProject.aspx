﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StartStopProject.aspx.cs" Inherits="EDMs.Web.Controls.Scope.StartStopProject" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, form {
            overflow-y: auto !important;
        }

        div.RadGrid .rgPager .rgAdvPart {
            display: none;
        }

        div.rgDataDiv {
            overflow: auto !important;
        }

        .DropZone1 {
            width: 300px;
            height: 250px;
            padding-left: 230px;
            background: #fff url(../../Images/placeholder-add.png) no-repeat center center;
            background-color: #357A2B;
            border-color: #CCCCCC;
            color: #767676;
            float: left;
            text-align: center;
            font-size: 16px;
            color: white;
            position: relative;
        }
    </style>

    <script src="~/Scripts/jquery-1.7.1.js" type="text/javascript"></script>

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }


    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <asp:Panel ID="Panel1" runat="server">
            <ul style="list-style-type: none">
                <li style="width: 100%;">
                    <div>
                        <label style="width: 110px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Upload file
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadAsyncUpload runat="server" ID="docuploader"
                                MultipleFileSelection="Automatic" TemporaryFileExpiration="05:00:00"
                                EnableInlineProgress="true" Width="350px"
                                Localization-Cancel="Cancel" CssClass="min25Percent qlcbFormRequired"
                                Localization-Remove="Remove" Localization-Select="Select" Skin="Windows7">
                            </telerik:RadAsyncUpload>

                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 100%;">
                    <div>
                        <label style="width: 110px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">
                                <asp:Label ID="lblReason" runat="server" Text=""></asp:Label>
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtReason" runat="server" Style="width: 300px;" CssClass="min25Percent"
                                TextMode="MultiLine" Rows="3" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 100%;">
                    <div>
                        <label style="width: 110px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;"><asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtDate" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>

                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <asp:Panel runat="server" ID="pnDeadline">
                    <li style="width: 100%;">
                        <div>
                            <label style="width: 110px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">New Deadline
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <telerik:RadDatePicker ID="txtDeadline" runat="server"
                                    ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                    <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                </telerik:RadDatePicker>

                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>
                </asp:Panel>
                <li style="width: 100%; text-align: center; margin-top: 20px">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" Width="70px" Style="text-align: center">
                    </telerik:RadButton>
                </li>
                <li style="width: 400px;" runat="server" id="blockError" visible="False">
                        <div>
                            <label style="width: 60px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: red; text-align: right;">Notification:
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:Label runat="server" ID="lblError" Width="300px"></asp:Label>
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>
            </ul>
            <div style="width: 100%">
            <telerik:RadGrid ID="grdProject" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellSpacing="0" CellPadding="0" PageSize="5" Height="200" GridLines="None"
                OnNeedDataSource="grdProject_NeedDataSource">
                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                <MasterTableView DataKeyNames="ID" ClientDataKeyNames="ID" Width="100%">
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; records." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Columns>
                        <telerik:GridBoundColumn DataField="TypeId" UniqueName="TypeId" Display="False" />
                        <telerik:GridBoundColumn DataField="TypeName" HeaderText="Type Name" UniqueName="TypeName">
                            <HeaderStyle Width="50%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="50%" HorizontalAlign="Left"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Reason" HeaderText="Reason" UniqueName="Reason">
                            <HeaderStyle Width="50%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="50%" HorizontalAlign="Left"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Date" HeaderText="Date" UniqueName="Date" DataFormatString="dd/MM/yyyy">
                            <HeaderStyle Width="50%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="50%" HorizontalAlign="Left"></ItemStyle>
                        </telerik:GridBoundColumn>
                    </Columns>
                    <CommandItemStyle Height="25px"></CommandItemStyle>
                </MasterTableView>
                <ClientSettings>
                    <Selecting AllowRowSelect="true"></Selecting>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" ScrollHeight="500"></Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
        </div>
        </asp:Panel>

        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument" OnAjaxRequest="ajaxDocument_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="btnSave"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="lblReason"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="lblDate"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="pnDeadline"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;
            </script>

        </telerik:RadScriptBlock>
    </form>
</body>
</html>

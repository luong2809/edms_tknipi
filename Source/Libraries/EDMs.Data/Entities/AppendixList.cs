﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Library;

    public partial class Appendix
    {
        public string FullName
        {
            get
            {
                return !string.IsNullOrEmpty(this.Name)
                        ? this.Code + " - " + this.Name
                        : this.Name;
            }
        }

        public string ParentCode
        {
            get
            {
                AppendixDAO dao = new AppendixDAO();
                var obj = dao.GetById(this.ParentID.GetValueOrDefault());
                if(obj!=null)
                {
                    return obj.Code;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}

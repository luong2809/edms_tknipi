﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Hosting;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using System.IO;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportTemp : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly ScopeProjectService scopeProjectService;



        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportTemp()
        {
            this.scopeProjectService = new ScopeProjectService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {

                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var dataSheet = workbook.Worksheets[0];

                        // Create a datatable
                        var dataTable = new DataTable();
                        // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                        dataTable = dataSheet.Cells.ExportDataTable(1, 0, dataSheet.Cells.MaxRow, 4);
                        int index = 2;
                        foreach (DataRow dataRow in dataTable.Rows)
                        {
                            if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                            {
                                var projectObj = this.scopeProjectService.GetByName(dataRow["Column1"].ToString());
                                if (projectObj != null)
                                {
                                    double tempTotalCostEstimate;
                                    if (double.TryParse(dataRow["Column2"].ToString().Trim(), out tempTotalCostEstimate))
                                    {
                                        projectObj.TotalCostEstimate = tempTotalCostEstimate;
                                    }
                                    double tempDesignCost;
                                    if (double.TryParse(dataRow["Column4"].ToString().Trim(), out tempDesignCost))
                                    {
                                        projectObj.DesignCost = tempDesignCost;
                                    }
                                    double tempProjectPlanningCost;
                                    if (double.TryParse(dataRow["Column3"].ToString().Trim(), out tempProjectPlanningCost))
                                    {
                                        projectObj.ProjectPlanningCost = tempProjectPlanningCost;
                                    }
                                    this.scopeProjectService.Update(projectObj);
                                }
                                else
                                {
                                    dataSheet.Cells["F" + index].PutValue("NULL");
                                }
                            }
                            index++;
                        }
                        var filename = "PL " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                        workbook.Save(filePath + filename);
                        this.DownloadByWriteByte(filePath + filename, filename, true);
                        //this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                var fs = new FileStream(strFileName, FileMode.Open);
                var streamLength = Convert.ToInt32(fs.Length);
                var data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Library
{
    using System;
    using System.Configuration;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;
    using EDMs.Web.Utilities.Sessions;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ProjectShareEditForm : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly AreaService AreaService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;
        private readonly RoleService roleService = new RoleService();
        private readonly WorkService workService = new WorkService();
        private readonly ProjectShareService projectShareService = new ProjectShareService();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ProjectShareEditForm()
        {
            this.userService = new UserService();
            this.AreaService = new AreaService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.categoryService = new CategoryService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.LoadComboData();
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = this.projectShareService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (obj != null)
                    {
                        var workObj = this.workService.GetById(obj.WorkID.GetValueOrDefault());
                        if (workObj != null)
                        {
                            this.ddlYear.SelectedValue = workObj.Year.ToString();
                            this.ddlWork.SelectedValue = obj.WorkID.ToString();
                        }
                        this.ddlGroup.SelectedValue = obj.GroupID.ToString();

                        this.cbActive.Checked = obj.Active.GetValueOrDefault();
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        private void LoadComboData()
        {
            for (var i = DateTime.Now.Year + 5; i >= 2018; i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.SelectedValue = DateTime.Now.Year.ToString();
            var workList = this.workService.GetByYear(Convert.ToInt32(ddlYear.SelectedValue));
            ddlWork.DataSource = workList;
            ddlWork.DataTextField = "Name";
            ddlWork.DataValueField = "Id";
            ddlWork.DataBind();

            var groupList = this.roleService.GetAllNgoaiKhoiThietKe();
            ddlGroup.DataSource = groupList;
            ddlGroup.DataTextField = "Name";
            ddlGroup.DataValueField = "Id";
            ddlGroup.DataBind();
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var projectShareID = Convert.ToInt32(this.Request.QueryString["disId"]);
                    var obj = this.projectShareService.GetById(projectShareID);
                    if (obj != null)
                    {
                        obj.GroupID = Convert.ToInt32(this.ddlGroup.SelectedValue);
                        obj.GroupName = this.ddlGroup.SelectedItem.Text;
                        obj.WorkID = Convert.ToInt32(this.ddlWork.SelectedValue);
                        obj.WorkName = this.ddlWork.SelectedItem.Text;
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;
                        obj.Active = this.cbActive.Checked;

                        this.projectShareService.Update(obj);
                    }
                }
                else
                {
                    if (!this.projectShareService.IsExist(Convert.ToInt32(this.ddlWork.SelectedValue),(Convert.ToInt32(this.ddlGroup.SelectedValue))))
                    {
                        var obj = new ProjectShare();
                        obj.GroupID = Convert.ToInt32(this.ddlGroup.SelectedValue);
                        obj.GroupName = this.ddlGroup.SelectedItem.Text;
                        obj.WorkID = Convert.ToInt32(this.ddlWork.SelectedValue);
                        obj.WorkName = this.ddlWork.SelectedItem.Text;
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;
                        obj.Active = this.cbActive.Checked;
                        this.projectShareService.Insert(obj);
                    }
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            var workList = this.workService.GetByYear(Convert.ToInt32(ddlYear.SelectedValue));
            ddlWork.DataSource = workList;
            ddlWork.DataTextField = "Name";
            ddlWork.DataValueField = "Id";
            ddlWork.DataBind();
        }
    }
}
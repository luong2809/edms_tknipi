﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewEMDR.aspx.cs" Inherits="EDMs.Web.ViewEMDR" EnableViewState="true" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    
    <style type="text/css">
      #ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important;}
        #ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important;}
        /*End*/
        @-moz-document url-prefix() {
            #ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important;}
            #ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important;}
        }
        
        #RAD_SPLITTER_PANE_CONTENT_ctl00_rightPane {
            overflow: hidden !important;
        }

        .rgExpandCol {
            width: 1% !important;
        }

        .rgGroupCol {
            width: 1% !important;
        }

        #ContentPlaceHolder2_grdDocument_ctl00_ctl02_ctl03_txtDate_popupButton {
            display: none;
        }

        .rgExpandCol {
            width: 1% !important;
        }

        .rgGroupCol {
            width: 1% !important;
        }

        .RadGrid .rgSelectedRow
        {
            background-image : none !important;
            background-color: coral !important;
        }

        .rgMasterTable .rgClipCells .rgClipCells {
            table-layout: auto !important;
        }

        .rgGroupCol
        {
            padding-left: 0 !important;
            padding-right: 0 !important;
            font-size:1px !important;
        }
 
        div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none; 
        }
        /*#RAD_SPLITTER_PANE_CONTENT_ctl00_leftPane {
            width: 250px !important;
        }
        #RAD_SPLITTER_PANE_CONTENT_ctl00_topLeftPane {
            width: 250px !important;
        }*/

        .RadAjaxPanel {
            height: 100% !important;
        }

        .rpExpandHandle {
            display: none !important;
        }

        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 0px !important;
            padding-right: 0px !important;
        }

        /*Hide change page size control*/
        /*div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none;        sssssss
        }*/    

        a.tooltip
        {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong
            {
                line-height: 30px;
            }

            a.tooltip:hover
            {
                text-decoration: none;
            }

            a.tooltip span
            {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span
            {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout
        {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span
        {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }

        
        #ctl00_ContentPlaceHolder2_radTreeFolder {
            overflow: visible !important;
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel
        {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5
        {
            height: 100% !important;
        }

        #divContainerLeft
        {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight
        {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted
        {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper
        {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip
        {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage
        {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow
        {
            z-index: 8000 !important;
        }

        .TemplateMenu
        {
            z-index: 10;
        }

        .RadGrid_Windows7 .rgGroupHeader {
            font-size: 14px !important;
            line-height: 19px !important;
        }

        .RadComboBoxDropDown_Windows7 .rcbHovered {
            background: coral !important;
            }
         
            #overlayDiv1 {
            position: fixed;
            top: 0;
            left: 0;
            z-index:100;
        }
        .accordion dt a
        {
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .accordion dt span {
            color: #085B8F;
            border-bottom: 1px solid #46A3D3;
            font-size: 1.0em;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
            margin-left: 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
      <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
        
        <telerik:RadSplitter ID="Radsplitter1" runat="server" Orientation="Vertical" Width="100%" Height="100%">
            <telerik:RadPane ID="Radpane1" runat="server"  Scrollable="false" Scrolling="None" Width="20%">
                <%-- <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
                       <telerik:RadPane ID="RadPane3" runat="server"  Scrollable="false"  Scrolling="None">--%>
                        <dl class="accordion">
                            <dt style="width: 100%;">
                                <span>Workpackages:</span>
                            </dt>
                        </dl>
                        <div style="width: 98%; padding-top: 10px; padding-left: 5px">

        
                            <telerik:RadTreeView ID="rtvWorkgroup" runat="server" 
                                Width="100%" Height="100%" ShowLineImages="False"
                                OnNodeClick="rtvWorkgroup_NodeClick" 
                                OnNodeDataBound="rtvWorkgroup_NodeDataBound"
                                OnClientNodeClicking="rtvWorkgroup_ClientNodeClicking" Skin="Windows7">
                                <DataBindings>
                                    <telerik:RadTreeNodeBinding Expanded="false"></telerik:RadTreeNodeBinding>
                                </DataBindings>
                            </telerik:RadTreeView>
                        </div>
               

          <%--  </telerik:RadPane>
            </telerik:RadSplitter>--%>
           </telerik:RadPane>
         <telerik:RadPane ID="Radpane2" runat="server" Scrollable="false" Scrolling="None" Width="80%" >
          
                    <telerik:RadSplitter ID="Radsplitter3" runat="server" Orientation="Horizontal">
                <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None" > 
                    <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical">
                        <telerik:RadPane ID="Radpane6" runat="server" Scrolling="None">
                            <telerik:RadGrid AllowCustomPaging="False" AllowPaging="True" AllowSorting="True" 
                                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" GridLines="None" 
                                Height="100%" ID="grdDocument"  AllowFilteringByColumn="True" AllowMultiRowSelection="True"
                                OnDeleteCommand="grdDocument_DeleteCommand" 
                                OnItemCommand="grdDocument_ItemCommand" 
                                OnNeedDataSource="grdDocument_OnNeedDataSource" 
                                OnItemDataBound="grdDocument_ItemDataBound" Skin="Windows7"
                                PageSize="100" runat="server" Style="outline: none" Width="100%">
                                <SortingSettings SortedBackColor="#FFF6D6"></SortingSettings>
                                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                                <MasterTableView AllowMultiColumnSorting="false"
                                    ClientDataKeyNames="ID" DataKeyNames="ID" CommandItemDisplay="Top" 
                                    EditMode="InPlace" Font-Size="9pt">
                                    <GroupByExpressions>
                                            <telerik:GridGroupByExpression>
                                                <SelectFields>
                                                    <telerik:GridGroupByField FieldAlias="_" FieldName="WorkgroupFullName" FormatString="{0:D}"
                                                        HeaderValueSeparator=""></telerik:GridGroupByField>
                                                </SelectFields>
                                                <GroupByFields>
                                                    <telerik:GridGroupByField FieldName="WorkgroupFullName" SortOrder="Ascending" ></telerik:GridGroupByField>
                                                </GroupByFields>
                                            </telerik:GridGroupByExpression>
                                        </GroupByExpressions>
                                    <CommandItemSettings  ShowAddNewRecordButton="false" RefreshText="Refresh Data" ShowExportToExcelButton="false"/>
                                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ColumnGroups>
                                        <telerik:GridColumnGroup HeaderText="REVISION DETAILS" Name="RevisionDetails"
                                             HeaderStyle-HorizontalAlign="Center"/>
                                        <telerik:GridColumnGroup HeaderText="OUTGOING TRANSMITTAL" Name="OutgoingTrans"
                                             HeaderStyle-HorizontalAlign="Center"/>
                                        <telerik:GridColumnGroup HeaderText="TRANSMITTAL HRAD COPY" Name="OutgoingTransXDCB"
                                             HeaderStyle-HorizontalAlign="Center"/>
                                        <telerik:GridColumnGroup HeaderText="INCOMING TRANSMITTAL" Name="IncomingTrans"
                                             HeaderStyle-HorizontalAlign="Center"/>
                                        <telerik:GridColumnGroup HeaderText="ICA REVIEW DETAILS" Name="ICAReviews"
                                             HeaderStyle-HorizontalAlign="Center"/>
                                    </ColumnGroups>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                        <telerik:GridBoundColumn DataField="HasAttachFile" UniqueName="HasAttachFile" Display="False" />
                                        
                                        <telerik:GridClientSelectColumn UniqueName="IsSelected" Visible="false">
                                                <HeaderStyle Width="25"  />
                                                <ItemStyle HorizontalAlign="Center" Width="25"/>
                                            </telerik:GridClientSelectColumn>

                                        <telerik:GridTemplateColumn HeaderText="No." Groupable="False" AllowFiltering="false">
                                            <HeaderStyle HorizontalAlign="Center" Width="30" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Width="30"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSoTT" runat="server" Text='<%# grdDocument.CurrentPageIndex * grdDocument.PageSize + grdDocument.Items.Count+1 %>'>
                                                </asp:Label>
                                      
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="EditColumn">
                                                <HeaderStyle Width="30"  />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <a href='javascript:ShowEditForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color:blue">
                                                    <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer;" AlternateText="Edit properties" />
                                                        <a/>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                    <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" HeaderTooltip="Delete document" Visible="False"
                                            ConfirmText="Do you want to delete document?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                                            <HeaderStyle Width="25" />
                                            <ItemStyle HorizontalAlign="Center" Width="25"  />
                                        </telerik:GridButtonColumn>
                                    
                                        <telerik:GridBoundColumn HeaderText="Workpackage" UniqueName="WorkgroupRevName" 
                                        ShowFilterIcon="False" FilterControlWidth="97%"  DataField="WorkgroupRevName"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="200"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                            
                                        <telerik:GridBoundColumn HeaderText="DOC. No." UniqueName="DocNo"
                                        DataField="DocNo" ShowFilterIcon="False" FilterControlWidth="97%" 
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="150"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn HeaderText="DOC. Title" UniqueName="DocTitle"
                                        DataField="DocTitle" ShowFilterIcon="False" FilterControlWidth="97%" 
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="220"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn HeaderText="Rev." DataField="RevisionName" UniqueName="Rev" ShowFilterIcon="false" FilterControlWidth="97%"  AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" >
                                            <HeaderStyle HorizontalAlign="Center" Width="50"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>

                                          <telerik:GridTemplateColumn HeaderText="Man-Hour Planed" UniqueName="ManHourPlan" AllowFiltering="false">
                                            <HeaderStyle HorizontalAlign="Center" Width="65" />
                                            <ItemStyle HorizontalAlign="Center" Width="65" />
                                            <ItemTemplate>
                                                <%# Eval("ManHourPlan") != null && Eval("ManHourPlan").ToString() != "0" ? Eval("ManHourPlan") : string.Empty%>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn HeaderText="Total Real Man-Hour" UniqueName="ManHours" AllowFiltering="false">
                                            <HeaderStyle HorizontalAlign="Center" Width="65" />
                                            <ItemStyle HorizontalAlign="Center" Width="65" />
                                            <ItemTemplate>
                                                <%# Eval("ManHours") != null && Eval("ManHours").ToString() != "0" ? Eval("ManHours") : string.Empty%>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        

                                        <telerik:GridBoundColumn DataField="StartDate" HeaderText="Start" UniqueName="StartDate" AllowFiltering="false" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                            
                                        <telerik:GridDateTimeColumn DataField="Deadline" HeaderText="Deadline" UniqueName="Deadline" DataFormatString="{0:dd/MM/yyyy}" SortExpression="StartDate" PickerType="DatePicker" AutoPostBackOnFilter="true" EnableRangeFiltering="true" FilterControlWidth="95" ShowFilterIcon="true" CurrentFilterFunction="Between">
                                            <HeaderStyle HorizontalAlign="Center" Width="280" />
                                        <ItemStyle HorizontalAlign="Center" Width="280"/>
                                        </telerik:GridDateTimeColumn>
                                            
                                        <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="Finish" UniqueName="EndDate" DataFormatString="{0:dd/MM/yyyy}" SortExpression="EndDate" PickerType="DatePicker" AutoPostBackOnFilter="true" EnableRangeFiltering="true" FilterControlWidth="95" ShowFilterIcon="true" CurrentFilterFunction="Between">
                                            <HeaderStyle HorizontalAlign="Center" Width="280" />
                                            <ItemStyle HorizontalAlign="Center" Width="280"/>
                                        </telerik:GridDateTimeColumn>
                                        
                                        <telerik:GridBoundColumn DataField="IsoReviseDate" HeaderText="Iso Revise Date" UniqueName="IsoReviseDate" AllowFiltering="false" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                        
                                    <telerik:GridTemplateColumn HeaderText="Weight" UniqueName="Weight" DataField="Weight" ShowFilterIcon="true" FilterControlWidth="50" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="LessThanOrEqualTo" SortExpression="Weight">
                                        <HeaderStyle HorizontalAlign="Center" Width="90" />
                                        <ItemStyle HorizontalAlign="Center"/>
                                        <ItemTemplate>
                                             <%# Eval("Weight") != null ? Eval("Weight") + "%" : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                    <telerik:GridTemplateColumn HeaderText="Complete" UniqueName="Complete" DataField="Complete" ShowFilterIcon="True" FilterControlWidth="50" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="LessThanOrEqualTo"
                                                SortExpression="Complete">
                                        <HeaderStyle HorizontalAlign="Center" Width="90" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                             <%# Eval("Complete") != null ? Eval("Complete") + "%" : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridBoundColumn DataField="OutgoingTransNo" HeaderText="No." UniqueName="OutgoingTransNo"  ShowFilterIcon="false" ColumnGroupName="OutgoingTrans" Display="True"  FilterControlWidth="97%" 
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" >
                                            <HeaderStyle HorizontalAlign="Center" Width="100"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>

                                       <telerik:GridBoundColumn DataField="OutgoingTransDate" HeaderText="Date" UniqueName="OutgoingTransDate"
                                        ColumnGroupName="OutgoingTrans" AllowFiltering="false" Display="True" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                              <telerik:GridBoundColumn DataField="DealineComment" HeaderText="Deadline Comment" UniqueName="DealineComment"
                                        ColumnGroupName="OutgoingTrans" AllowFiltering="false" Display="True" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                        
                                        <telerik:GridBoundColumn DataField="OutgoingLeterNo" HeaderText="No." UniqueName="OutgoingLeterNo"  ShowFilterIcon="false" ColumnGroupName="OutgoingTransXDCB" Display="True" FilterControlWidth="97%" 
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="100"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                            
                                        <telerik:GridBoundColumn DataField="OutgoingLeterDate" UniqueName="OutgoingLeterDate"
                                        ColumnGroupName="OutgoingTransXDCB" AllowFiltering="false" Display="True" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                            
                                        <telerik:GridBoundColumn DataField="IncomingTransNo" HeaderText="No." UniqueName="IncomingTransNo"  ShowFilterIcon="false" AutoPostBackOnFilter="true" FilterControlWidth="97%" CurrentFilterFunction="Contains" ColumnGroupName="IncomingTrans">
                                            <HeaderStyle HorizontalAlign="Center" Width="100"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                        
                                        <telerik:GridBoundColumn DataField="IncomingTransDate" HeaderText="Date" UniqueName="IncomingTransDate"
                                        ColumnGroupName="IncomingTrans" AllowFiltering="false" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn DataField="FinalCodeName" HeaderText="Code" UniqueName="FinalCodeName" ShowFilterIcon="false" AutoPostBackOnFilter="true" FilterControlWidth="97%" CurrentFilterFunction="Contains" ColumnGroupName="IncomingTrans">
                                            <HeaderStyle HorizontalAlign="Center" Width="40"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                              
                                        <telerik:GridBoundColumn DataField="ICAReviewOutTransNo" HeaderText="Trans No." UniqueName="ICAReviewOutTransNo" AllowFiltering="false" ColumnGroupName="ICAReviews">
                                            <HeaderStyle HorizontalAlign="Center" Width="100"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                        
                                        <telerik:GridBoundColumn DataField="ICAReviewOutDate" HeaderText="Trans Date" UniqueName="ICAReviewOutDate"
                                        ColumnGroupName="ICAReviews" AllowFiltering="false" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                                
                                        <telerik:GridBoundColumn DataField="ICAReviewCode" HeaderText="ReviewCode" UniqueName="ICAReviewCode" AllowFiltering="false" ColumnGroupName="ICAReviews">
                                            <HeaderStyle HorizontalAlign="Center" Width="100"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>

                                          <telerik:GridBoundColumn HeaderText="Markup" UniqueName="Markup" 
                                        ShowFilterIcon="False" FilterControlWidth="97%"  DataField="Markup" ColumnGroupName="ICAReviews"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="100"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Status" UniqueName="StatusName" 
                                        ShowFilterIcon="False" FilterControlWidth="97%"  DataField="StatusName" 
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="120"/>
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                            <%--<ClientEvents OnRowContextMenu="RowContextMenu" OnRowClick="RowClick"></ClientEvents>--%>
                            <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                            </ClientSettings>
                        </telerik:RadGrid>


                    </telerik:RadPane>

                </telerik:RadSplitter>       

            </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>
             <div> 
           
        </div>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
    <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest"  >
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="rtvWorkgroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="IsFullPermission" />
                      
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="rtvWorkgroup" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
               
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContainer" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="rtvWorkgroup" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="CustomerMenu" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>
    <%--OnClientClose="refreshGrid"--%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" >
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Document Information" 
                VisibleStatusbar="false" Height="600" Width="650" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

  
    <asp:HiddenField runat="server" ID="FolderContextMenuAction"/>
    <asp:HiddenField runat="server" ID="lblWorkgroupId"/>
    <asp:HiddenField runat="server" ID="wpidmove" />
    <asp:HiddenField runat="server" ID="lblFolderId"/>
    <asp:HiddenField runat="server" ID="lblDocId"/>
    <asp:HiddenField runat="server" ID="lblCategoryId"/>
    <asp:HiddenField runat="server" ID="IsFullPermission"/>
    <asp:HiddenField runat="server" ID="ProjectFolderPath"/>

    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex"/>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">

           
            function OnClientLoad(sender) {
                var input = sender.get_inputDomElement();
                var selectedItem = sender.get_selectedItem();
                input.style.background = "url(" + selectedItem.get_imageUrl() + ") no-repeat";
            }

            var radDocuments;

            function ShowFilter(obj) {
                if (obj.checked) {
                    $find('<%=grdDocument.ClientID %>').get_masterTableView().showFilterItem();
                } else {
                    $find('<%=grdDocument.ClientID %>').get_masterTableView().hideFilterItem();
                }
            }

            function refreshGrid() {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }


            function ExportGrid() {
                var masterTable = $find("<%=grdDocument.ClientID %>").get_masterTableView();
                masterTable.exportToExcel('PIN_list.xls');
                return false;
            }

            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

            function onRequestStart(sender, args) {
                ////alert(args.get_eventTarget());
                if (args.get_eventTarget().indexOf("ExportTo") >= 0 || args.get_eventTarget().indexOf("btnDownloadPackage") >= 0 ||
                    args.get_eventTarget().indexOf("ajaxCustomer") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            
          

            function RowClick(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;
            }

           
            function rtvExplore_OnNodeExpandedCollapsed(sender, eventArgs) {
                var allNodes = eventArgs._node.get_treeView().get_allNodes();

                var i;
                var selectedNodes = "";

                for (i = 0; i < allNodes.length; i++) {
                    if (allNodes[i].get_expanded())
                        selectedNodes += allNodes[i].get_value() + "*";
                }

                Set_Cookie("expandedNodesObjTree", selectedNodes, 30);
            }

            function Set_Cookie(name, value, expires, path, domain, secure) {
                // set time, it's in milliseconds
                var today = new Date();
                today.setTime(today.getTime());

                /*
                if the expires variable is set, make the correct 
                expires time, the current script below will set 
                it for x number of days, to make it for hours, 
                delete * 24, for minutes, delete * 60 * 24
                */
                if (expires) {
                    expires = expires * 1000 * 60 * 60 * 24;
                }
                var expires_date = new Date(today.getTime() + (expires));

                document.cookie = name + "=" + escape(value) +
                    ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
                    ((path) ? ";path=" + path : "") +
                    ((domain) ? ";domain=" + domain : "") +
                    ((secure) ? ";secure" : "");
            }

          
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents());
                });

             <%--  // toolbar = $find("<%= CustomerMenu.ClientID %>");--%>
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function ShowEditForm(id) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;

              

                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/DocumentPackageInfoEditForm.aspx?docId=" + id +"&folId=" + selectedFolder + "&categoryId=" + categoryId, "CustomerDialog");

                // window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + id, "CustomerDialog");
                //  return false;
            }

          

            function refreshGrid(arg) {
                if (!arg) {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                    masterTable.rebind();
                }
                else {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                    masterTable.rebind();
                }
            }

            function refreshTab(arg) {
                $('.EDMsRadPageView' + arg + ' iframe').attr('src', $('.EDMsRadPageView' + arg + ' iframe').attr('src'));
            }

            function RowDblClick(sender, eventArgs) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/ViewCustomerDetails.aspx?docId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
                // window.radopen("Controls/Customers/ViewCustomerDetails.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
            }

            function rtvWorkgroup_ClientNodeClicking(sender, args) {
                var workgroupValue = args.get_node().get_value();
                document.getElementById("<%= lblWorkgroupId.ClientID %>").value = workgroupValue;
            }

           

            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("images/clear.gif");
                    searchButton.set_value("clear");
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value());
            }
            function onTabSelecting(sender, args) {
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }
            }


        </script>
    </telerik:RadCodeBlock>
 </form>
</body>
</html>
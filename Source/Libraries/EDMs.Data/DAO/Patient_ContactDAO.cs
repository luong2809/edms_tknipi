﻿using EDMs.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EDMs.Data.DAO
{
    public class Patient_ContactDAO:BaseDAO
    {
        public Patient_ContactDAO() : base() { }

        #region GET (Basic)
        public IQueryable<Contact> GetIQueryable()
        {
            return EDMsDataContext.Contacts;
        }
        
        public List<Contact> GetAll()
        {
            return EDMsDataContext.Contacts.ToList<Contact>();
        }

        public Contact GetByID(int ID)
        {
            return EDMsDataContext.Contacts.Where(ob => ob.ID == ID).FirstOrDefault<Contact>();
        }

       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// Find By Any expression
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<Contact> Find(Expression<Func<Contact, bool>> where)
        {
            return EDMsDataContext.Contacts.Where(where).ToList<Contact>();
        }
        #endregion

        #region Insert, Update, Delete
        public bool Insert(Contact ob)
        {
            try
            {
                EDMsDataContext.AddToContacts(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Contact ob)
        {
            try
            {
                Contact _ob;

                _ob = (from rs in EDMsDataContext.Contacts
                       where rs.ID == ob.ID
                       select rs).First();

                _ob.UserID = ob.UserID;
                _ob.CustomerID = ob.CustomerID;
                _ob.Content = ob.Content;
                _ob.Description = ob.Description;
                _ob.UpdatedDate = ob.UpdatedDate;
                _ob.UpdatedBy = ob.UpdatedBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Contact ob)
        {
            try
            {
                Contact _ob = GetByID(ob.ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                Contact _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Data.Entities;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Customers
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Business.Services;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ViewCustomerDetails : Page
    {
        /// <summary>
        /// The patient status service
        /// </summary>
        private PatientStatusService _patientStatusService;


        /// <summary>
        /// The patient service
        /// </summary>
        private PatientService _patientService;

        public ViewCustomerDetails()
        {
            _patientStatusService = new PatientStatusService();
            _patientService = new PatientService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["patientId"]))
                {
                    var customerID = Convert.ToInt32(Request.QueryString["patientId"]);
                    var patient = this._patientService.GetByID(customerID);
                    if (patient != null)
                    {
                        lblMaKH.Text = patient.SSN;

                        //Birthday
                        if (patient.DateOfBirth == null)
                        {
                            lblNgaySinh.Text = "N/A";
                        }
                        else
                        {
                            lblNgaySinh.Text = String.Format("{0:dd/MM/yyyy}", patient.DateOfBirth);

                            imgBirthDay.Visible = false;

                            if (patient.DateOfBirth.Value.Month == DateTime.Now.Month)
                            {
                                imgBirthDay.Visible = true;
                                lblBirthday.Text = "Sinh nhật trong tháng " + DateTime.Now.Month.ToString() + " này";
                            }
                            if (patient.DateOfBirth.Value.Day == DateTime.Now.Day && patient.DateOfBirth.Value.Month == DateTime.Now.Month)
                            {
                                imgBirthDay.Visible = true;
                                lblBirthday.Text = "Hôm nay là ngày sinh nhật";
                            }
                        }
                        //End Birthday

                        lblNgheNghiep.Text = patient.Occupation;
                        lblDiaChi.Text = patient.Address1;
                        lblDienThoai.Text = patient.CellPhone;
                        lblEmail.Text = patient.Email;
                        lblCMND.Text = patient.IdentityCard;
                        lblNgayCap.Text = "N/A";
                        lblNoiCap.Text = "N/A";
                        lblTinhTrang.Text = patient.PatientStatus.Name;
                        //Ghi chu cuoi cung cua Customer
                        Patient_DescriptionService descriptionService = new Patient_DescriptionService();
                        Description ds = descriptionService.Find(x => x.CustomerID.Value == customerID).OrderByDescending(x => x.ID).FirstOrDefault();
                        lblGhiChu.Text = ds == null ? "N/A" : ds.Content;
                        if (lblGhiChu.Text != "N/A")
                        {
                            lblGhiChu.ForeColor = System.Drawing.Color.Red;
                            lblGhiChu.Font.Bold = true;
                        }
                        else
                        {
                            lblGhiChu.ForeColor = System.Drawing.Color.Black;
                            lblGhiChu.Font.Bold = false;
                        }

                        RadPageView1.ContentUrl = "Details/Detail_Contact.aspx?ID=" + customerID.ToString();
                        RadPageView2.ContentUrl = "Details/Detail_Consulting.aspx?ID=" + customerID.ToString();
                        RadPageView3.ContentUrl = "Details/Detail_UsedService.aspx?ID=" + customerID.ToString();
                        RadPageView4.ContentUrl = "Details/Detail_Member.aspx?ID=" + customerID.ToString();
                        RadPageView5.ContentUrl = "Details/Detail_Description.aspx?ID=" + customerID.ToString();
                    }
                }
            }
        }

        protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {
            //AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }
    }
}
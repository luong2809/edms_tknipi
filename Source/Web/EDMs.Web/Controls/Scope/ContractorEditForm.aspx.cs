﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Scope
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Scope;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ContractorEditForm : Page
    {
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly ContractorService ContractorService;

        private readonly ScopeProjectService scopeProjectService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ContractorEditForm()
        {
            this.ContractorService = new ContractorService();
            this.scopeProjectService = new ScopeProjectService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                var projectInPermission = this.scopeProjectService.GetAll();
                this.ddlProject.DataSource = projectInPermission;
                this.ddlProject.DataTextField = "Name";
                this.ddlProject.DataValueField = "ID";
                this.ddlProject.DataBind();

                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var objContractor = this.ContractorService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objContractor != null)
                    {
                        this.txtName.Text = objContractor.Name;
                        this.txtDescription.Text = objContractor.Description;
                       // this.ddlProject.SelectedValue = objContractor.ProjectId.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var ContractorId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    var obj = this.ContractorService.GetById(ContractorId);
                    if (obj != null)
                    {
                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                      //  obj.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                      //  obj.ProjectName = this.ddlProject.SelectedItem.Text;

                        this.ContractorService.Update(obj);
                    }
                }
                else
                {
                    var obj = new Contractor()
                    {
                        Name = this.txtName.Text.Trim(),
                        Description = this.txtDescription.Text.Trim(),
                    //    ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                    //    ProjectName = this.ddlProject.SelectedItem.Text
                    };

                    this.ContractorService.Insert(obj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Discipline name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class WorkpackageListByProject : Page
    {
        private readonly DocumentNewService documentNewService;

        private readonly AttachFileService attachFileService;

        private readonly DocumentPackageService documentPackageService;

        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly WorkGroupService workGroupService;
        private readonly AttachFilesWorkpackageService attachFilesWorkpackageService;


        /// <summary>
        /// Initializes a new instance of the <see cref="RevisionHistory"/> class.
        /// </summary>
        public WorkpackageListByProject()
        {
            this.documentNewService = new DocumentNewService();
            this.attachFileService = new AttachFileService();
            this.documentPackageService = new DocumentPackageService();
            this.workGroupService = new WorkGroupService();
            this.attachFilesWorkpackageService = new AttachFilesWorkpackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if(Request.QueryString["docId"] != null)
            {
                var projectId = Convert.ToInt32(Request.QueryString["docId"]);
                var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
                var currentDepartment = UserSession.Current.User.RoleId;
                var workPackageList = isSeeFull
                    ? this.workGroupService.GetAll().Where(t => t.ProjectId == projectId)
                    : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()).Where(t => t.ProjectId == projectId);

                if (UserSession.Current.IsEngineer)
                {
                    var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                    workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
                }

                this.grdDocument.DataSource = workPackageList.OrderBy(t => t.ID);
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                grdDocument.Rebind();
            }
        }

        /// <summary>
        /// The btn download_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnDownload_Click(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).Parent.Parent as GridDataItem;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentNewService.GetById(docId);
            var docPackName = string.Empty;
            if (docObj != null)
            {
                //docPackName = docObj.Name;
                //var serverDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_" + docObj.Name + "_Pack.rar");

                //var attachFiles = this.attachFileService.GetAllByDocId(docId);

                //var temp = ZipPackage.CreateFile(serverDocPackPath);

                //foreach (var attachFile in attachFiles)
                //{
                //    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                //    {
                //        temp.Add(Server.MapPath(attachFile.FilePath));
                //    }
                //}

                //this.DownloadByWriteByte(serverDocPackPath, docPackName + ".rar", true);
            }
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        protected void grdDocument_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            switch (e.DetailTableView.Name)
            {
                case "DocDetail":
                    {
                        var wpId = Convert.ToInt32(dataItem.GetDataKeyValue("ID").ToString());
                        e.DetailTableView.DataSource = this.attachFilesWorkpackageService.GetAllByWorkpackage(wpId); ;
                        break;
                    }
            }
        }
    }
}
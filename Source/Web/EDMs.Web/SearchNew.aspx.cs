﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Drawing;
using System.Linq;
using EDMs.Business.Services.Scope;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;
    using CheckBox = System.Web.UI.WebControls.CheckBox;
    using Label = System.Web.UI.WebControls.Label;
    using TextBox = System.Web.UI.WebControls.TextBox;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class SearchNew : Page
    {
        private readonly OptionalTypeService optionalTypeService = new OptionalTypeService();

        private readonly RevisionService revisionService = new RevisionService();

        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        private readonly FolderService folderService = new FolderService();

        private readonly UserService userService = new UserService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly WorkGroupService workGroupService = new WorkGroupService();

        private readonly RoleService roleService = new RoleService();

        protected const string ServiceName = "EDMSFolderWatcher";

        public static RadTreeNode editedNode = null;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

        /// <summary>
        /// The list folder id.
        /// </summary>
        private List<int> listFolderId = new List<int>();

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            var temp = (RadPane)this.Master.FindControl("leftPane");
            temp.Collapsed = true;

            this.Form.DefaultButton = this.btnSearch.UniqueID;

            if (!Page.IsPostBack)
            {
                this.LoadComboData();
                if (UserSession.Current.IsEngineer)
                {
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
                    this.grdDocument.AllowMultiRowSelection = false;

                }


                if (UserSession.Current.User.Role.IsAdmin.GetValueOrDefault())
                {
                    this.IsFullPermission.Value = "true";
                }
            }
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(bool isbind = false, bool isListAll = false)
        {
            var docList = new List<DocumentPackage>();
            var searchAll = this.txtSearchAll.Text.Trim();
            var docNo = this.txtDocNo.Text.Trim();
            var title = this.txtTitle.Text.Trim();
            var docTypeId = Convert.ToInt32(this.ddlDocumentType.SelectedValue);
            var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
            var workpackageId = Convert.ToInt32(this.ddlWorkpackage.SelectedValue);
            var revisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
            var engineerId = Convert.ToInt32(this.ddlEngineer.SelectedValue);

            var wpInPermission =
                this.ddlWorkpackage.Items.Where(t => t.Value != "0").Select(t => Convert.ToInt32(t.Value)).ToList();

            docList = this.documentPackageService.SearchDocument(searchAll, docNo, title, docTypeId, projectId, workpackageId, revisionId, engineerId, wpInPermission);
            this.grdDocument.DataSource = docList;
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.Rebind();
            }
        }
        
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(false, isListAll);
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentPackageService.GetById(docId);
            if (docObj != null)
            {
                if (docObj.ParentId == null)
                {
                    docObj.IsDelete = true;
                    this.documentPackageService.Update(docObj);
                }
                else
                {
                    var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                    if (listRelateDoc != null)
                    {
                        foreach (var objDoc in listRelateDoc)
                        {
                            objDoc.IsDelete = true;
                            this.documentPackageService.Update(objDoc);
                        }
                    }
                }

                var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());
                if (wpObj != null)
                {
                    var docList = this.documentPackageService.GetAllByWorkgroup(wpObj.ID);
                    if (docList.Count == 0)
                    {
                        wpObj.CanDelete = true;
                        this.workGroupService.Update(wpObj);
                    }
                }
            }
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid")
            {
               
            }
            if (e.CommandName == RadGrid.RebindGridCommandName)
            {
                this.grdDocument.Rebind();
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                
            }
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {

                ////Populate Filters by binding the combo to datasource
                //var filteringItem = (GridFilteringItem)e.Item;
                //var myRadComboBox = (RadComboBox)filteringItem.FindControl("RadComboBoxCustomerProgramDescription");

                //myRadComboBox.DataSource = myDataSet;
                //myRadComboBox.DataTextField = "CustomerProgramDescription";
                //myRadComboBox.DataValueField = "CustomerProgramDescription";
                //myRadComboBox.ClearSelection();
                //myRadComboBox.DataBind();
            }
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["HasAttachFile"].Text == "True")
                {
                    item.BackColor = Color.Aqua;
                    item.BorderColor = Color.Aqua;
                }
            }

            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var lblWorkgroupName = item.FindControl("lblWorkgroupName") as Label;
                var lbldocNo = item.FindControl("lbldocNo") as Label;
                var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var txtManHours = item.FindControl("txtManHours") as RadNumericTextBox;
                var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
                var txtDeadline = item.FindControl("txtDeadline") as RadDatePicker;
                var txtEndDate = item.FindControl("txtEndDate") as RadDatePicker;
                var txtIsoReviseDate = item.FindControl("txtIsoReviseDate") as RadDatePicker;
                var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
                var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;
                var listRevision = this.revisionService.GetAll();
                listRevision.Insert(0, new Revision() {ID = 0});
                if (ddlRevision != null)
                {
                    ddlRevision.DataSource = listRevision;
                    ddlRevision.DataTextField = "Name";
                    ddlRevision.DataValueField = "ID";
                    ddlRevision.DataBind();
                }

                if (txtStartDate != null)
                {
                    txtStartDate.DatePopupButton.Visible = false;
                }

                if (txtDeadline != null)
                {
                    txtDeadline.DatePopupButton.Visible = false;
                }

                if (txtEndDate != null)
                {
                    txtEndDate.DatePopupButton.Visible = false;
                }

                if (txtIsoReviseDate != null)
                {
                    txtIsoReviseDate.DatePopupButton.Visible = false;
                }

                var WorkgroupName = (item.FindControl("WorkgroupName") as HiddenField).Value;
                var DocNo = (item.FindControl("DocNo") as HiddenField).Value;
                var DocTitle = (item.FindControl("DocTitle") as HiddenField).Value;
                var RevisionId = (item.FindControl("RevisionId") as HiddenField).Value;
                var ManHours = (item.FindControl("ManHours") as HiddenField).Value;
                var StartDate = (item.FindControl("StartDate") as HiddenField).Value;
                var Deadline = (item.FindControl("Deadline") as HiddenField).Value;
                var EndDate = (item.FindControl("EndDate") as HiddenField).Value;
                var IsoReviseDate = (item.FindControl("IsoReviseDate") as HiddenField).Value;
                var complete = (item.FindControl("Complete") as HiddenField).Value;
                var weight = (item.FindControl("Weight") as HiddenField).Value;

                if (!string.IsNullOrEmpty(StartDate))
                {
                    txtStartDate.SelectedDate = Convert.ToDateTime(StartDate);
                }

                if (!string.IsNullOrEmpty(Deadline))
                {
                    txtDeadline.SelectedDate = Convert.ToDateTime(Deadline);
                }

                if (!string.IsNullOrEmpty(EndDate))
                {
                    txtEndDate.SelectedDate = Convert.ToDateTime(EndDate);
                }

                if (!string.IsNullOrEmpty(IsoReviseDate))
                {
                    txtIsoReviseDate.SelectedDate = Convert.ToDateTime(IsoReviseDate);
                }

                lblWorkgroupName.Text = WorkgroupName;
                lbldocNo.Text = DocNo;
                txtDocTitle.Text = DocTitle;


                ddlRevision.SelectedValue = RevisionId;
                txtComplete.Value = Convert.ToDouble(complete);
                txtWeight.Value = Convert.ToDouble(weight);
                txtManHours.Value = Convert.ToDouble(ManHours);
            }
        }

        protected void grdDocument_DataBound(object sender, EventArgs e)
        {
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        private void LoadComboData()
        {
            var revisionList = this.revisionService.GetAll();
            revisionList.Insert(0, new Revision() { Name = string.Empty });
            this.ddlRevision.DataSource = revisionList;
            this.ddlRevision.DataValueField = "ID";
            this.ddlRevision.DataTextField = "Name";
            this.ddlRevision.DataBind();

            var documentTypeList = this.documentTypeService.GetAll().OrderBy(t => t.Name).ToList();
            documentTypeList.Insert(0, new DocumentType {ID = 0});
            this.ddlDocumentType.DataSource = documentTypeList;
            this.ddlDocumentType.DataValueField = "ID";
            this.ddlDocumentType.DataTextField = "FullName";
            this.ddlDocumentType.DataBind();

            var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
            var currentDepartment = UserSession.Current.User.RoleId;
            var workPackageList = isSeeFull
                ? this.workGroupService.GetAll()
                : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault());

            if (UserSession.Current.IsEngineer)
            {
                var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
            }

            var projectInPermission = isSeeFull
                ? this.scopeProjectService.GetAll()
                : this.scopeProjectService.GetAll(workPackageList.Select(t => t.ProjectId.GetValueOrDefault()).Distinct().ToList());

            projectInPermission.Insert(0, new ScopeProject { ID = 0 });
            this.ddlProject.DataSource = projectInPermission;
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataBind();

            var departmentIds = workPackageList.Where(t => t.DepartmentId != null).Select(t => t.DepartmentId.Value).Distinct().ToList();

            var userList = this.userService.GetAllByRoleId(departmentIds);
            userList.Insert(0, new User { Id = 0 });
            this.ddlEngineer.DataSource = userList;
            this.ddlEngineer.DataTextField = "UserNameWithFullName";
            this.ddlEngineer.DataValueField = "Id";
            this.ddlEngineer.DataBind();

            if (UserSession.Current.IsEngineer)
            {
                this.ddlEngineer.SelectedValue = UserSession.Current.User.Id.ToString();
                this.ddlEngineer.Enabled = false;
            }

            workPackageList.Insert(0, new WorkGroup {ID = 0});
            this.ddlWorkpackage.DataSource = workPackageList;
            this.ddlWorkpackage.DataTextField = "Name";
            this.ddlWorkpackage.DataValueField = "ID";
            this.ddlWorkpackage.DataBind();

        }

        protected void ddlProject_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var selectedProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
            var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip;
            var currentDepartment = UserSession.Current.User.RoleId;
            var workPackageList = isSeeFull
                ? (selectedProjectId != 0 
                    ? this.workGroupService.GetAll().Where(t => t.ProjectId == selectedProjectId).ToList()
                    : this.workGroupService.GetAll())
                : (selectedProjectId != 0
                    ? this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()).Where(t => t.ProjectId == selectedProjectId).ToList()
                    : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()));

            if (UserSession.Current.IsEngineer)
            {
                var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
            }

            var departmentIds = workPackageList.Where(t => t.DepartmentId != null).Select(t => t.DepartmentId.Value).Distinct().ToList();

            var userList = this.userService.GetAllByRoleId(departmentIds);
            userList.Insert(0, new User { Id = 0 });
            this.ddlEngineer.DataSource = userList;
            this.ddlEngineer.DataTextField = "UserNameWithFullName";
            this.ddlEngineer.DataValueField = "Id";
            this.ddlEngineer.DataBind();
            if (UserSession.Current.IsEngineer)
            {
                this.ddlEngineer.SelectedValue = UserSession.Current.User.Id.ToString();
                this.ddlEngineer.Enabled = false;
            }


            workPackageList.Insert(0, new WorkGroup { ID = 0 });
            this.ddlWorkpackage.DataSource = workPackageList;
            this.ddlWorkpackage.DataTextField = "Name";
            this.ddlWorkpackage.DataValueField = "ID";
            this.ddlWorkpackage.DataBind();
        }

        protected void ddlWorkpackage_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var selectedProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
            var selectedWPId = Convert.ToInt32(this.ddlWorkpackage.SelectedValue);
            var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip;
            var currentDepartment = UserSession.Current.User.RoleId;
            var workPackageList = isSeeFull
                ? (selectedProjectId != 0
                    ? this.workGroupService.GetAll().Where(t => t.ProjectId == selectedProjectId).ToList()
                    : this.workGroupService.GetAll())
                : (selectedProjectId != 0
                    ? this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()).Where(t => t.ProjectId == selectedProjectId).ToList()
                    : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()));

            if (UserSession.Current.IsEngineer)
            {
                var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
            }

            var departmentIds = workPackageList.Where(t =>
                    (selectedWPId == 0 || t.ID == selectedWPId) &&
                    t.DepartmentId != null).Select(t => t.DepartmentId.Value).Distinct().ToList();

            var userList = this.userService.GetAllByRoleId(departmentIds);
            userList.Insert(0, new User { Id = 0 });
            this.ddlEngineer.DataSource = userList;
            this.ddlEngineer.DataTextField = "UserNameWithFullName";
            this.ddlEngineer.DataValueField = "Id";
            this.ddlEngineer.DataBind();

            if (UserSession.Current.IsEngineer)
            {
                this.ddlEngineer.SelectedValue = UserSession.Current.User.Id.ToString();
                this.ddlEngineer.Enabled = false;
            }
        }
    }
}
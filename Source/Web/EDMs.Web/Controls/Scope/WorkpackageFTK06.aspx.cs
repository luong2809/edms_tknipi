﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;

using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;

using OfficeHelper.Utilities.Data;

using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class WorkpackageFTK06 : Page
    {
        private readonly WorkGroupService workGroupService;
        private readonly UserService userService;
        private readonly DocumentPackageService documentPackageService;
        private readonly RoleService roleService;
        private readonly ScopeProjectService scopeProjectService;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public WorkpackageFTK06()
        {
            this.workGroupService = new WorkGroupService();
            this.roleService = new RoleService();
            this.scopeProjectService = new ScopeProjectService();
            this.userService = new UserService();
            this.documentPackageService = new DocumentPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    var workpackageObj = this.workGroupService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
                    this.txtProject.Text = workpackageObj.ProjectName;
                    this.txtDepartment.Text = workpackageObj.DepartmentName;
                   // this.txtWorkpackage.Text = workpackageObj.Name;
                   this.txtdate.SelectedDate = DateTime.Now.Date;
                    this.txtSymbol.Focus();
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnS_Click(object sender, EventArgs e)
        {
            var selectedWP = this.workGroupService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
            if (selectedWP != null)
            {
                var dataTable = new DataTable();
                var reportInfo = new DataTable();

                var ds = new DataSet();
                var listColumn = new[]
                    {
                        new DataColumn("Index", Type.GetType("System.String")),
                        new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                        new DataColumn("DeadLine", Type.GetType("System.String")),
                        new DataColumn("Note", Type.GetType("System.String"))
                    };
                dataTable.Columns.AddRange(listColumn);

                var listColumn1 = new[]
                    {
                        new DataColumn("Department", Type.GetType("System.String")),
                        new DataColumn("Project", Type.GetType("System.String")),
                        new DataColumn("Symbol", Type.GetType("System.String")),
                        new DataColumn("WPStartDate", Type.GetType("System.String")),
                        new DataColumn("GIP", Type.GetType("System.String")),
                        new DataColumn("Change", Type.GetType("System.String")),
                        new DataColumn("Date", Type.GetType("System.String")),
                        new DataColumn("Content", Type.GetType("System.String")), 
                        new DataColumn("Unit", Type.GetType("System.String")), 
                        new DataColumn("Profile", Type.GetType("System.String")), 
                        new DataColumn("Deadline", Type.GetType("System.String")), 
                    };
                reportInfo.Columns.AddRange(listColumn1);

                var infoItem = reportInfo.NewRow();
                var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                var projectManager =
                    this.userService.GetByID(projectObj != null
                        ? projectObj.ProjectManagerId.GetValueOrDefault()
                        : 0);
                //var Departmentmanager = this.userService.GetAllByRoleId(selectedWP.DepartmentId.GetValueOrDefault()).Where(t => t.IsChief.GetValueOrDefault() && (t.Position != null && convertToUnSign(t.Position.ToLower()) == "truong phong")).ToList();
                var deparment = this.roleService.GetByID(selectedWP.DepartmentId.GetValueOrDefault());

                infoItem["Department"] = deparment != null
                    ? deparment.RussiaName
                    : string.Empty;

                // infoItem["ProjectDescription"] = projectObj != null ? projectObj.Description : string.Empty;
                infoItem["Project"] = projectObj != null ? projectObj.Name : string.Empty;
                infoItem["Symbol"] = this.txtSymbol.Text;
                infoItem["WPStartDate"] = "день " + DateTime.Now.Day + " месяц " +
                          DateTime.Now.Month + " год " +
                          DateTime.Now.Year;

                infoItem["GIP"] = projectManager != null ? projectManager.FullName : string.Empty;
                infoItem["Change"] = this.txtchange.Text;
                infoItem["Unit"] = this.txtUnit.Text;
                infoItem["Profile"] = this.txtprofile.Text;
                infoItem["Date"] = DateTime.Now.ToString("dd/MM/yyyy");
                infoItem["Deadline"] = this.txtdeadline.SelectedDate != null ? this.txtdeadline.SelectedDate.Value.ToString("dd/MM/yyyy"):string.Empty;
                infoItem["Content"] = this.txtContent.Text + "\r\n";
                //foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                //{
                //    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                //    var docObj = this.documentPackageService.GetById(docId);
                //    infoItem["Contain"] += "-" + docObj.DocNo + "_" + docObj.RevisionName + "_" + docObj.DocTitle + "\r\n";
                //}

                reportInfo.Rows.Add(infoItem);

                ds.Tables.Add(dataTable);
                ds.Tables[0].TableName = "Table";

                var rootPath = Server.MapPath("../../Exports") + @"\";
                const string WordPath = @"Template\";
                const string WordPathExport = @"Generated\";
                var StrTemplateFileName = "F-TK-06 Rev2 Russ.doc";
                var strOutputFileName = "F-TK-06_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                var isSuccess = OfficeCommon.ExportToWordWithRegion(
                    rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                if (isSuccess)
                {
                    this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "F-TK-06_" + Utility.RemoveSpecialCharacter(selectedWP.Name, "-") + ".doc", true);
                }
            }
            else
            {
                var st = "Content is empty! Please enter contents of Workpackage.";
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + st + "');</script>");
            }

            this.grdDocument.Rebind();
        }

        protected void btnV_Click(object sender, EventArgs e)
        {

            var selectedWP = this.workGroupService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
            if (selectedWP != null)
            {
                var dataTable = new DataTable();
                var reportInfo = new DataTable();

                var ds = new DataSet();
                var listColumn = new[]
                    {
                        new DataColumn("Index", Type.GetType("System.String")),
                        new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                        new DataColumn("DeadLine", Type.GetType("System.String")),
                        new DataColumn("Note", Type.GetType("System.String"))
                    };
                dataTable.Columns.AddRange(listColumn);

                var listColumn1 = new[]
                    {
                        new DataColumn("Department", Type.GetType("System.String")),
                        new DataColumn("Project", Type.GetType("System.String")),
                        new DataColumn("Symbol", Type.GetType("System.String")),
                        new DataColumn("WPStartDate", Type.GetType("System.String")),
                        new DataColumn("GIP", Type.GetType("System.String")),
                        new DataColumn("Change", Type.GetType("System.String")),
                        new DataColumn("Date", Type.GetType("System.String")),
                        new DataColumn("Content", Type.GetType("System.String")), 
                        new DataColumn("Unit", Type.GetType("System.String")), 
                        new DataColumn("Profile", Type.GetType("System.String")), 
                        new DataColumn("Deadline", Type.GetType("System.String")), 
                    };
                reportInfo.Columns.AddRange(listColumn1);

                var infoItem = reportInfo.NewRow();
                var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                var projectManager =
                    this.userService.GetByID(projectObj != null
                        ? projectObj.ProjectManagerId.GetValueOrDefault()
                        : 0);
                //var Departmentmanager = this.userService.GetAllByRoleId(selectedWP.DepartmentId.GetValueOrDefault()).Where(t => t.IsChief.GetValueOrDefault() && (t.Position != null && convertToUnSign(t.Position.ToLower()) == "truong phong")).ToList();
                var deparment = this.roleService.GetByID(selectedWP.DepartmentId.GetValueOrDefault());

                infoItem["Department"] = deparment != null
                    ? deparment.RussiaName
                    : string.Empty;

                // infoItem["ProjectDescription"] = projectObj != null ? projectObj.Description : string.Empty;
                infoItem["Project"] = projectObj != null ? projectObj.Name : string.Empty;
                infoItem["Symbol"] = this.txtSymbol.Text;
                infoItem["WPStartDate"] = "Ngày " + DateTime.Now.Day + " Tháng " +
                          DateTime.Now.Month + " Năm " +
                          DateTime.Now.Year;

                infoItem["GIP"] = projectManager != null ? projectManager.FullName : string.Empty;
                infoItem["Change"] = this.txtchange.Text;
                infoItem["Unit"] = this.txtUnit.Text;
                infoItem["Profile"] = this.txtprofile.Text;
                infoItem["Date"] = DateTime.Now.ToString("dd/MM/yyyy");
                infoItem["Deadline"] = this.txtdeadline.SelectedDate != null ? this.txtdeadline.SelectedDate.Value.ToString("dd/MM/yyyy") : string.Empty;
                infoItem["Content"] = this.txtContent.Text + "\r\n";
                //foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                //{
                //    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                //    var docObj = this.documentPackageService.GetById(docId);
                //    infoItem["Contain"] += "-" + docObj.DocNo + "_" + docObj.RevisionName + "_" + docObj.DocTitle + "\r\n";
                //}

                reportInfo.Rows.Add(infoItem);

                ds.Tables.Add(dataTable);
                ds.Tables[0].TableName = "Table";

                var rootPath = Server.MapPath("../../Exports") + @"\";
                const string WordPath = @"Template\";
                const string WordPathExport = @"Generated\";
                var StrTemplateFileName = "F-TK-06 Viet Rev2.doc";
                var strOutputFileName = "F-TK-06_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                var isSuccess = OfficeCommon.ExportToWordWithRegion(
                    rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                if (isSuccess)
                {
                    this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "F-TK-06_" + Utility.RemoveSpecialCharacter(selectedWP.Name, "-") + ".doc", true);
                }
            }
            else
            {
                var st = "Content is empty! Please enter contents of Workpackage.";
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + st + "');</script>");
            }

            this.grdDocument.Rebind();
        }
        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                var fs = new FileStream(strFileName, FileMode.Open);
                var streamLength = Convert.ToInt32(fs.Length);
                var data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public static string convertToUnSign(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }  
       

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            //{
            //    var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
            //    var listcontent = this.documentPackageService.GetAllByWorkgroup(workpackageId)
            //           .OrderBy(t => t.DocNo)
            //           .ToList();

            //    this.grdDocument.DataSource = listcontent;
            //}
            //else
            //{
                this.grdDocument.DataSource = new List<DocumentPackage>();
           // }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            throw new NotImplementedException();
        }

        

    }
}
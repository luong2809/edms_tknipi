﻿using EDMs.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EDMs.Data.DAO
{
    public class Patient_ConsultingDAO:BaseDAO
    {
        public Patient_ConsultingDAO() : base() { }

        #region GET (Basic)
        public IQueryable<Consulting> GetIQueryable()
        {
            return EDMsDataContext.Consultings;
        }
        
        public List<Consulting> GetAll()
        {
            return EDMsDataContext.Consultings.ToList<Consulting>();
        }

        public Consulting GetByID(int ID)
        {
            return EDMsDataContext.Consultings.Where(ob => ob.ID == ID).FirstOrDefault<Consulting>();
        }

       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// Find By Any expression
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<Consulting> Find(Expression<Func<Consulting, bool>> where)
        {
            return EDMsDataContext.Consultings.Where(where).ToList<Consulting>();
        }
        #endregion

        #region Insert, Update, Delete
        public bool Insert(Consulting ob)
        {
            try
            {
                EDMsDataContext.AddToConsultings(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Consulting ob)
        {
            try
            {
                Consulting _ob;

                _ob = (from rs in EDMsDataContext.Consultings
                       where rs.ID == ob.ID
                       select rs).First();

                _ob.UserID = ob.UserID;
                _ob.CustomerID = ob.CustomerID;
                _ob.Content = ob.Content;
                _ob.Description = ob.Description;
                _ob.UpdatedDate = ob.UpdatedDate;
                _ob.UpdatedBy = ob.UpdatedBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Consulting ob)
        {
            try
            {
                Consulting _ob = GetByID(ob.ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                Consulting _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

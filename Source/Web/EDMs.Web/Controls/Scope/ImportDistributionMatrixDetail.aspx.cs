﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportDistributionMatrixDetail : Page
    {
        private readonly DocumentPackageService documentPackageService;

        private readonly WorkGroupService workGroupService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly MilestoneService milestoneService;

        private readonly DistributionMatrixDetailService distributionMatrixDetailService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportDistributionMatrixDetail()
        {
            this.workGroupService = new WorkGroupService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.milestoneService = new MilestoneService();
            this.distributionMatrixDetailService = new DistributionMatrixDetailService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
            {
                var extension = docFile.GetExtension();
                if (extension == ".xls" || extension == ".xlsx")
                {
                    var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                     "_" + docFile.FileName;
                    docFile.SaveAs(importPath);

                    var workbook = new Workbook();
                    workbook.Open(importPath);
                    for (int i = 1; i < workbook.Worksheets.Count; i++)
                    {
                        var worksheet = workbook.Worksheets[i];
                        var flag = worksheet.Cells["L4"].Value.ToString();
                        if (flag == "1")
                        {
                            var projectId = worksheet.Cells["A1"].Value.ToString();
                            var projectObj = this.scopeProjectService.GetById(!string.IsNullOrEmpty(projectId)
                                            ? Convert.ToInt32(projectId)
                                            : 0);
                            var workgroupId = worksheet.Cells["A2"].Value;
                            var countDiscipline = Convert.ToInt32(worksheet.Cells["A3"].Value);

                            this.distributionMatrixDetailService.DeleteByWorkpackage(Convert.ToInt32(workgroupId));

                            var wpObj = this.workGroupService.GetById(Convert.ToInt32(workgroupId));
                            if (wpObj != null && projectObj != null)
                            {
                                // Create a datatable
                                var dtMain = new DataTable();
                                var dtDiscipline = new DataTable();

                                // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                                dtMain = worksheet.Cells.ExportDataTable(29, 1, worksheet.Cells.MaxRow, countDiscipline + 13);
                                dtDiscipline = worksheet.Cells.ExportDataTableAsString(4, 13, 2, countDiscipline);
                                foreach (DataRow dataRow in dtMain.Rows)
                                {
                                    if (!string.IsNullOrEmpty(dataRow[0].ToString()))
                                    {
                                        for (int j = 0; j < countDiscipline; j++)
                                        {
                                            if (!string.IsNullOrEmpty(dataRow[j + 12].ToString()) && dataRow[j + 12].ToString().ToLower() == "x")
                                            {
                                                var matrixObj = new DistributionMatrixDetail();
                                                matrixObj.WorkgroupID = Convert.ToInt32(workgroupId);
                                                matrixObj.DisciplineID = Convert.ToInt32(dtDiscipline.Rows[0][j].ToString());
                                                matrixObj.DisciplineName = dtDiscipline.Rows[1][j].ToString();
                                                matrixObj.ProjectID = Convert.ToInt32(projectId);
                                                matrixObj.DocumentID = Convert.ToInt32(dataRow[0].ToString());
                                                matrixObj.DocumentName = dataRow[2].ToString();
                                                matrixObj.CreateBy = UserSession.Current.User.Id;
                                                matrixObj.CreateDate = DateTime.Now;
                                                this.distributionMatrixDetailService.Insert(matrixObj);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    blockError.Visible = true;
                    lblError.Text = "Import Success!";
                }
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}
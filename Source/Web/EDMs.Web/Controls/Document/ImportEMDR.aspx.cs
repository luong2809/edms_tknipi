﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportEMDR : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly PackageService packageService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly WorkGroupService workGroupService;

        private readonly EmailNotificationTemplateService emailNotificationTemplateService;
        private readonly UserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportEMDR()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.documentService = new DocumentService();
            this.packageService = new PackageService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.emailNotificationTemplateService = new EmailNotificationTemplateService();
            this.userService = new UserService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentFileName = string.Empty;
            var currentDocumentNo = string.Empty;

            var documentPackageList = new List<DocumentPackage>();
            var newdocumentPackageList = new List<DocumentPackage>();

            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    currentFileName = docFile.FileName;
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                         "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var importStartRow = Convert.ToInt32(ConfigurationManager.AppSettings.Get("ImportStartRow"));
                        var isAutoDetectDocNo =
                            Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AutoDetectDocNo"));
                        // Instantiate a new workbook
                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var wsData = workbook.Worksheets[0];
                        var wsNewData = workbook.Worksheets["Add New Documents"];


                        var projectId = string.Empty;
                        var workgroupId = string.Empty;

                        // Create a datatable
                        var totalDataTable = new DataTable();
                        var dataTable = new DataTable();
                        var insertNewDoc = new DataTable();

                        // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class	

                        totalDataTable = wsData.Cells.ExportDataTable(6, 0, wsData.Cells.MaxRow, 23);

                        var haveEdit = totalDataTable.AsEnumerable().Any(t => t["Column1"].ToString().ToLower() == "1" || t["Column1"].ToString().ToLower() == "true");

                        dataTable = haveEdit
                            ? totalDataTable.AsEnumerable()
                                .Where(t => t["Column1"].ToString().ToLower() == "1" || t["Column1"].ToString().ToLower() == "true")
                                .CopyToDataTable()
                            : new DataTable();

                        insertNewDoc = wsNewData.Cells.ExportDataTable(6, 0, wsNewData.Cells.MaxRow, 23);

                        if (totalDataTable.Rows.Count > 0)
                        {
                            projectId = totalDataTable.Rows[0][1].ToString().Split(',')[0];
                            workgroupId = totalDataTable.Rows[0][1].ToString().Split(',')[1];
                        }

                        var project = this.scopeProjectService.GetById(Convert.ToInt32(projectId));
                        var workgroup = this.workGroupService.GetById(Convert.ToInt32(workgroupId));

                        foreach (DataRow dataRow in dataTable.Rows)
                        {
                            if (!string.IsNullOrEmpty(dataRow["Column2"].ToString()) && dataRow["Column2"].ToString() != "-1")
                            {
                                currentDocumentNo = dataRow["Column4"].ToString();

                                var documentId = Convert.ToInt32(dataRow["Column2"].ToString());
                                var documentPackageObj = this.documentPackageService.GetById(documentId);
                                if (documentPackageObj != null)
                                {
                                    var listDocRelated = this.documentPackageService.GetAllRelatedDocument(documentPackageObj.ParentId != null ? documentPackageObj.ParentId.GetValueOrDefault() : documentPackageObj.ID);
                                    foreach (var documentPackage in listDocRelated)
                                    {
                                        var revisionName = dataRow["Column7"].ToString();
                                        if (documentPackage.RevisionName == revisionName)
                                        {
                                            var strRevPlanedDate = dataRow["Column8"].ToString();
                                            var RevPlanedDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(strRevPlanedDate, ref RevPlanedDate))
                                            {
                                                documentPackage.RevisionPlanedDate = RevPlanedDate;
                                            }

                                            var strRevActualDate = dataRow["Column9"].ToString();
                                            var RevActualDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(strRevActualDate, ref RevActualDate))
                                            {
                                                documentPackage.RevisionActualDate = RevActualDate;
                                            }
                                        }

                                        documentPackage.DocNo = dataRow["Column4"].ToString();
                                        documentPackage.DocTitle = dataRow["Column5"].ToString();

                                        var strstartDate = dataRow["Column6"].ToString();
                                        var StartDate = new DateTime();
                                        if (Utility.ConvertStringToDateTime(strstartDate, ref StartDate))
                                        {
                                            documentPackage.StartDate = StartDate;
                                        }

                                        documentPackage.Complete = !string.IsNullOrEmpty(dataRow["Column10"].ToString())
                                            ? Math.Round(Convert.ToDouble(dataRow["Column10"]) * 100, 2)
                                            : 0;
                                        documentPackage.Weight = !string.IsNullOrEmpty(dataRow["Column11"].ToString())
                                            ? Math.Round(Convert.ToDouble(dataRow["Column11"]) * 100, 2)
                                            : 0;

                                        var department = this.roleService.GetByName(dataRow["Column12"].ToString());
                                        documentPackage.DeparmentId = department != null ? department.Id : 0;
                                        documentPackage.DeparmentName = department != null ? department.Name : string.Empty;

                                        documentPackage.Notes = dataRow["Column13"].ToString();
                                        documentPackage.IsEMDR = dataRow["Column14"].ToString().ToLower() == "x";
                                        documentPackageList.Add(documentPackage);

                                        if (!this.cbCheckValidFile.Checked)
                                        {
                                            this.documentPackageService.Update(documentPackage);
                                            if (UserSession.Current.User.Role.Notify.GetValueOrDefault())
                                            {
                                                this.NotificationUpdateDoc(documentPackage);
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }

                        foreach (DataRow dataRow in insertNewDoc.Rows)
                        {
                            if (!string.IsNullOrEmpty(dataRow["Column4"].ToString()))
                            {
                                currentDocumentNo = dataRow["Column4"].ToString();
                                if (!this.documentPackageService.IsExistByDocNo(currentDocumentNo))
                                {
                                    var package = new Package();
                                    var discipline = new Discipline();
                                    var documentType = new DocumentType();

                                    if (isAutoDetectDocNo)
                                    {
                                        var docNo = dataRow["Column4"].ToString().Replace(" ", string.Empty);
                                        var detectData = docNo.Split('-').Select(t => t.Trim()).ToList();

                                        package = this.packageService.GetByName(detectData[2], project.ID);
                                        discipline =
                                            this.disciplineService.GetByName(detectData[3], project.ID);
                                        documentType =
                                            this.documentTypeService.GetByName(detectData[4], project.ID);
                                    }


                                    var department = this.roleService.GetByName(dataRow["Column12"].ToString());
                                    var revision = this.revisionService.GetByName(dataRow["Column7"].ToString());

                                    var docObj = new DocumentPackage();

                                    docObj.WorkgroupId = workgroup.ID;
                                    docObj.WorkgroupName = workgroup.Name;

                                    docObj.DocNo = dataRow["Column4"].ToString().Replace(" ", string.Empty);
                                    docObj.DocTitle = dataRow["Column5"].ToString();

                                    docObj.PackageId = package != null ? package.ID : 0;
                                    docObj.PackageName = package != null ? package.Name : string.Empty;

                                    docObj.DisciplineId = discipline != null ? discipline.ID : 0;
                                    docObj.DisciplineName = discipline != null ? discipline.Name : string.Empty;

                                    docObj.DocumentTypeId = documentType != null ? documentType.ID : 0;
                                    docObj.DocumentTypeName = documentType != null
                                        ? documentType.FullName
                                        : string.Empty;

                                    docObj.DeparmentId = department != null ? department.Id : 0;
                                    docObj.DeparmentName = department != null ? department.Name : string.Empty;

                                    docObj.ProjectId = project.ID;
                                    docObj.ProjectName = project.Name;

                                    var strstartDate = dataRow["Column6"].ToString();
                                    var StartDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(strstartDate, ref StartDate))
                                    {
                                        docObj.StartDate = StartDate;
                                    }

                                    docObj.RevisionId = revision != null ? revision.ID : 0;
                                    docObj.RevisionName = revision != null ? revision.Name : string.Empty;

                                    var strRevPlanedDate = dataRow["Column8"].ToString();
                                    var RevPlanedDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(strRevPlanedDate, ref RevPlanedDate))
                                    {
                                        docObj.RevisionPlanedDate = RevPlanedDate;
                                    }

                                    var strRevActualDate = dataRow["Column9"].ToString();
                                    var RevActualDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(strRevActualDate, ref RevActualDate))
                                    {
                                        docObj.RevisionActualDate = RevActualDate;
                                    }

                                    docObj.Complete = !string.IsNullOrEmpty(dataRow["Column10"].ToString())
                                        ? Math.Round(Convert.ToDouble(dataRow["Column10"]) * 100, 2)
                                        : 0;
                                    docObj.Weight = !string.IsNullOrEmpty(dataRow["Column11"].ToString())
                                        ? Math.Round(Convert.ToDouble(dataRow["Column11"]) * 100, 2)
                                        : 0;

                                    docObj.OutgoingTransNo = string.Empty;
                                    docObj.IncomingTransNo = string.Empty;
                                    docObj.ICAReviewCode = string.Empty;
                                    docObj.RevisionCommentCode = string.Empty;
                                    docObj.ICAReviewOutTransNo = string.Empty;

                                    docObj.Notes = dataRow["Column13"].ToString();
                                    docObj.IsEMDR = dataRow["Column14"].ToString().ToLower() == "x";
                                    docObj.IsLeaf = true;
                                    docObj.IsDelete = false;
                                    docObj.CreatedBy = UserSession.Current.User.Id;
                                    docObj.CreatedDate = DateTime.Now;

                                    newdocumentPackageList.Add(docObj);

                                    if (!this.cbCheckValidFile.Checked)
                                    {
                                        this.documentPackageService.Insert(docObj);
                                        if (UserSession.Current.User.Role.Notify.GetValueOrDefault())
                                        {
                                            this.NotificationAddNewDoc(docObj);
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }

                if (this.cbCheckValidFile.Checked)
                {
                    foreach (var documentPackage in documentPackageList)
                    {
                        this.documentPackageService.Update(documentPackage);
                        if (UserSession.Current.User.Role.Notify.GetValueOrDefault())
                        {
                            this.NotificationUpdateDoc(documentPackage);
                        }
                    }

                    foreach (var documentPackage in newdocumentPackageList)
                    {
                        this.documentPackageService.Insert(documentPackage);
                        if (UserSession.Current.User.Role.Notify.GetValueOrDefault())
                        {
                            this.NotificationAddNewDoc(documentPackage);
                        }
                    }

                    this.blockError.Visible = true;
                    this.lblError.Text = "All EMDR report file is valid.";
                }
                else
                {
                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);    
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error at EMDR file: '" + currentFileName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        private void NotificationDeleteDoc(DocumentPackage docObj)
        {
            if (docObj.EngineerId != "0")
            {
                var engineer = this.userService.GetByID(docObj.EngineerId);
                var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());

                var emailList = this.userService.GetAllByRoleId(wpObj != null ? wpObj.DepartmentId.GetValueOrDefault() : 0)
                    .Where(t => t.IsChief.GetValueOrDefault())
                    .Select(t => t.Email)
                    .Where(t => !string.IsNullOrEmpty(t)).ToList();

                if (engineer != null)
                {
                    if (!string.IsNullOrEmpty(engineer.Email))
                    {
                        emailList.Add(engineer.Email);
                    }
                    var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.DeleteDoc);
                    if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                    {
                        var smtpClient = new SmtpClient
                        {
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                            EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                            Host = ConfigurationManager.AppSettings["Host"],
                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                            Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                        };
                        var deletedUser = this.userService.GetByID(UserSession.Current.User.Id);
                        var subject = notificationTemplate.Subject.Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#DeletedUser#", deletedUser != null ? deletedUser.FullName : string.Empty);

                        var message = new MailMessage();
                        message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                        message.Subject = subject;
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = notificationTemplate.Contents
                            .Replace("#DeletedUser#", deletedUser != null ? deletedUser.FullName : string.Empty)
                            .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                            .Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#DocTitle#", docObj.DocTitle)
                            .Replace("#DocType#", docObj.DocumentTypeName)
                            .Replace("#DocRev#", docObj.RevisionName)
                            .Replace("#DocEng#", engineer.FullName)
                            .Replace("#DocStartDate#", docObj.StartDate != null ? docObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocDeadline#", docObj.Deadline != null ? docObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocFinishDate#", docObj.EndDate != null ? docObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocISODate#", docObj.IsoReviseDate != null ? docObj.IsoReviseDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocWeight#", docObj.Weight != null ? docObj.Weight.Value.ToString() : string.Empty)
                            .Replace("#DocComplete#", docObj.Complete != null ? docObj.Complete.Value.ToString() : string.Empty);

                        //if (ConfigurationManager.AppSettings["SendOnlySupport"] == "True")
                        //{
                        //    message.To.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                        //}
                        //else
                        //{
                            foreach (var to in emailList)
                            {
                                message.To.Add(new MailAddress(to));
                            }
                       // }
                       

                        smtpClient.Send(message);
                    }
                }
            }
        }

        private void NotificationAddNewDoc(DocumentPackage docObj)
        {
            if (docObj.EngineerId != "0")
            {
                var engineer = this.userService.GetByID(docObj.EngineerId);
                if (engineer != null && !string.IsNullOrEmpty(engineer.Email))
                {
                    var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.CreateNewDoc);
                    if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                    {
                        var smtpClient = new SmtpClient
                        {
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                            EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                            Host = ConfigurationManager.AppSettings["Host"],
                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                            Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                        };

                        var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());
                        var subject = notificationTemplate.Subject.Replace("#DocNumber#", docObj.DocNo).Replace("#AssignedUser#", engineer.FullName);

                        var message = new MailMessage();
                        message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                        message.Subject = subject;
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = notificationTemplate.Contents
                            .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                            .Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#AssignedUser#", engineer.FullName)
                            .Replace("#DocTitle#", docObj.DocTitle)
                            .Replace("#DocType#", docObj.DocumentTypeName)
                            .Replace("#DocRev#", docObj.RevisionName)
                            .Replace("#DocEng#", engineer.FullName)
                            .Replace("#DocStartDate#", docObj.StartDate != null ? docObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocDeadline#", docObj.Deadline != null ? docObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocFinishDate#", docObj.EndDate != null ? docObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocISODate#", docObj.IsoReviseDate != null ? docObj.IsoReviseDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocWeight#", docObj.Weight != null ? docObj.Weight.Value.ToString() : string.Empty)
                            .Replace("#DocComplete#", docObj.Complete != null ? docObj.Complete.Value.ToString() : string.Empty);

                        
                        //if (ConfigurationManager.AppSettings["SendOnlySupport"] == "True")
                        //{
                        //    message.To.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                        //}
                        //else
                        //{
                            message.To.Add(new MailAddress(engineer.Email));
                      //  }
                       
                        smtpClient.Send(message);
                    }
                }
            }
        }

        private void NotificationUpdateDoc(DocumentPackage docObj)
        {
            if (docObj.EngineerId != "0")
            {
                var engineer = this.userService.GetByID(docObj.EngineerId);
                var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());

                var emailList = this.userService.GetAllByRoleId(wpObj != null ? wpObj.DepartmentId.GetValueOrDefault() : 0)
                    .Where(t => t.IsChief.GetValueOrDefault())
                    .Select(t => t.Email)
                    .Where(t => !string.IsNullOrEmpty(t)).ToList();

                if (engineer != null)
                {
                    if (!string.IsNullOrEmpty(engineer.Email))
                    {
                        emailList.Add(engineer.Email);
                    }
                    var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.UpdateWP);
                    if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                    {
                        var smtpClient = new SmtpClient
                        {
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                            EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                            Host = ConfigurationManager.AppSettings["Host"],
                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                            Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                        };
                        var updatedUser = this.userService.GetByID(UserSession.Current.User.Id);
                        var subject = notificationTemplate.Subject.Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#UpdatedUser#", updatedUser != null ? updatedUser.FullName : string.Empty);

                        var message = new MailMessage();
                        message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                        message.Subject = subject;
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = notificationTemplate.Contents
                            .Replace("#UpdatedUser#", updatedUser != null ? updatedUser.FullName : string.Empty)
                            .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                            .Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#DocTitle#", docObj.DocTitle)
                            .Replace("#DocType#", docObj.DocumentTypeName)
                            .Replace("#DocRev#", docObj.RevisionName)
                            .Replace("#DocEng#", engineer.FullName)
                            .Replace("#DocStartDate#", docObj.StartDate != null ? docObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocDeadline#", docObj.Deadline != null ? docObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocFinishDate#", docObj.EndDate != null ? docObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocISODate#", docObj.IsoReviseDate != null ? docObj.IsoReviseDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocWeight#", docObj.Weight != null ? docObj.Weight.Value.ToString() : string.Empty)
                            .Replace("#DocComplete#", docObj.Complete != null ? docObj.Complete.Value.ToString() : string.Empty);

                        //if (ConfigurationManager.AppSettings["SendOnlySupport"] == "True")
                        //{
                        //    message.To.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                        //}
                        //else
                        //{
                            foreach (var to in emailList)
                            {
                                message.To.Add(new MailAddress(to));
                            }
                       // }

                        smtpClient.Send(message);
                    }
                }
            }
        }
    }
}
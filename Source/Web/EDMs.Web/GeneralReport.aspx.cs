﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using System.Data.SqlClient;
using Aspose.Cells;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;
using Telerik.Web.UI.GridExcelBuilder;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Web.UI;
    using System.Drawing;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;
    using System.Web.Hosting;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class GeneralReport : Page
    {
        private readonly OptionalTypeService optionalTypeService = new OptionalTypeService();

        /// <summary>
        /// The permission service.
        /// </summary>
        private readonly PermissionService permissionService = new PermissionService();

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService = new RevisionService();

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService = new StatusService();

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService = new DisciplineService();

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService = new ReceivedFromService();

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService = new LanguageService();

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService = new FolderService();

        private readonly DocumentService documentService = new DocumentService();

        private readonly DocumentNewService documentNewService = new DocumentNewService();

        private readonly NotificationRuleService notificationRuleService = new NotificationRuleService();

        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly UserService userService = new UserService();

        private readonly AttachFileService attachFileService = new AttachFileService();

        private readonly AttachFilesPackageService attachFilesPackageService = new AttachFilesPackageService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly PackageService packageService = new PackageService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly WorkGroupService workGroupService = new WorkGroupService();

        private readonly RoleService roleService = new RoleService();

        private readonly TemplateManagementService templateManagementService = new TemplateManagementService();

        private readonly PermissionWorkgroupService permissionWorkgroupService = new PermissionWorkgroupService();

        private readonly MilestoneService milestoneService = new MilestoneService();

        private readonly FieldService fieldService = new FieldService();

        private readonly PlatformService platformService = new PlatformService();

        private readonly MonthlyReportService monthlyReportService = new MonthlyReportService();

        private readonly AppendixService appendixListService = new AppendixService();

        private readonly WorkService workListService = new WorkService();

        private readonly ProjectAppendixService projectAppendixService = new ProjectAppendixService();
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();

        private readonly UserManhourMonthService userManhourMonthService = new UserManhourMonthService();
        protected const string ServiceName = "EDMSFolderWatcher";

        public static RadTreeNode editedNode = null;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

        /// <summary>
        /// The list folder id.
        /// </summary>
        private List<int> listFolderId = new List<int>();

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {

                //list user role gip and chief
                List<int> userSpecial = ConfigurationManager.AppSettings.Get("ChiefandGIP").Split(',').Select(Int32.Parse).ToList();

                if (userSpecial.Contains(UserSession.Current.User.Id))
                {
                    this.GIPReport.Visible = true;
                    this.PMReport.Visible = false;
                    this.MasterReport.Visible = false;
                    this.ChiefReport.Visible = true;
                }

                else if (UserSession.Current.IsGip)
                {
                    this.GIPReport.Visible = true;
                    this.PMReport.Visible = false;
                    this.MasterReport.Visible = false;
                    this.ChiefReport.Visible = false;

                }
                else if (UserSession.Current.IsSuperViewer)
                {
                    this.GIPReport.Visible = false;
                    this.PMReport.Visible = true;
                    this.MasterReport.Visible = true;
                    this.ChiefReport.Visible = true;
                }
                else if (UserSession.Current.IsAdmin || UserSession.Current.IsDC)
                {
                    this.GIPReport.Visible = true;
                    this.PMReport.Visible = true;
                    this.MasterReport.Visible = true;
                    this.ChiefReport.Visible = true;
                }
                else if (UserSession.Current.IsCheif || UserSession.Current.IsLeader)
                {
                    this.GIPReport.Visible = false;
                    this.PMReport.Visible = false;
                    this.MasterReport.Visible = false;
                    this.ChiefReport.Visible = true;
                }

                else
                {
                    this.GIPReport.Visible = false;
                    this.PMReport.Visible = false;
                    this.MasterReport.Visible = false;
                    this.ChiefReport.Visible = false;
                }
                if (this.ChiefReport.Visible)
                {
                    for (var i = DateTime.Now.Year + 5; i >= 2019; i--)
                    {
                        this.ddlYearManhourChiefReport.Items.Add(new System.Web.UI.WebControls.ListItem(i.ToString(), i.ToString()));
                    }
                    this.ddlYearManhourChiefReport.SelectedValue = DateTime.Now.Year.ToString();
                    List<int> specialUserExportChief = new List<int>() { 210, 257, 81, 78, 1, 87, 342, 191 };

                    if (specialUserExportChief.Contains(UserSession.Current.User.Id))
                    {
                        this.ChiefReport.Visible = true;
                        ddlDepartmentManhourChiefReport.Visible = true;
                        var roleList = this.roleService.GetAllKhoiThietKe().ToList();
                        ddlDepartmentManhourChiefReport.DataSource = roleList;
                        ddlDepartmentManhourChiefReport.DataTextField = "Name";
                        ddlDepartmentManhourChiefReport.DataValueField = "Id";
                        ddlDepartmentManhourChiefReport.DataBind();
                    }
                }
                if (this.GIPReport.Visible)
                {
                    var projectlis = this.scopeProjectService.GetAll().ToList();
                    this.ddlProject.DataSource = projectlis;
                    this.ddlProject.DataTextField = "FullName";
                    this.ddlProject.DataValueField = "ID";
                    this.ddlProject.DataBind();
                }
                if (this.PMReport.Visible)
                {
                    var projectlis = this.scopeProjectService.GetAll().ToList();
                    this.ddlPMProject.DataSource = projectlis;
                    this.ddlPMProject.DataTextField = "FullName";
                    this.ddlPMProject.DataValueField = "ID";
                    this.ddlPMProject.DataBind();
                    for (var i = DateTime.Now.Year + 5; i >= 2019; i--)
                    {
                        this.ddlYearProgressReport.Items.Add(new System.Web.UI.WebControls.ListItem(i.ToString(), i.ToString()));
                        this.ddlYearManhourReport.Items.Add(new System.Web.UI.WebControls.ListItem(i.ToString(), i.ToString()));
                    }
                    this.ddlYearProgressReport.SelectedValue = DateTime.Now.Year.ToString();
                    this.ddlYearManhourReport.SelectedValue = DateTime.Now.Year.ToString();
                    var roleList = this.roleService.GetAllKhoiThietKe().ToList();
                    ddlDepartmentReport.DataSource = roleList;
                    ddlDepartmentReport.DataTextField = "Name";
                    ddlDepartmentReport.DataValueField = "Id";
                    ddlDepartmentReport.DataBind();
                    LoadComboData();
                }

                this.txtGIPToDate.SelectedDate = DateTime.Now;
                this.txtGIPFromDate.SelectedDate = DateTime.Now.AddDays(-30);

                this.txtPMToDate.SelectedDate = DateTime.Now;
                this.txtPMFromDate.SelectedDate = DateTime.Now.AddDays(-30);

                this.txtMasterToDate.SelectedDate = DateTime.Now;
                this.txtMasterFromDate.SelectedDate = DateTime.Now.AddDays(-30);

                //this.txtChiefToDate.SelectedDate = DateTime.Now;
                //this.txtChiefFromDate.SelectedDate = DateTime.Now.AddDays(-30);

                //this.txtManhourFromDate.SelectedDate = DateTime.Now.AddDays(-30);
                //this.txtManhourToDate.SelectedDate = DateTime.Now;

                this.txtmhgipfromdate.SelectedDate = DateTime.Now.AddDays(-30);
                this.txtmhgipTodate.SelectedDate = DateTime.Now;

                this.txtUnplannedProjectFromDate.SelectedDate = DateTime.Now.AddDays(-30);
                this.txtUnplannedProjectToDate.SelectedDate = DateTime.Now;

                this.txtFromDateProject.SelectedDate = new DateTime(2017, 1, 1);
                this.txtToDateProject.SelectedDate = DateTime.Now;
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
            }
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        protected void btnExportPMReport_OnClick(object sender, EventArgs e)
        {
            var projectList = new List<ScopeProject>();
            var projectOfPM = new List<ScopeProject>();
            if (this.txtMasterFromDate.SelectedDate != null && this.txtMasterToDate.SelectedDate == null)
            {
                projectList = scopeProjectService.GetAll().Where(t => (t.EndDate != null && t.EndDate.Value >= this.txtMasterFromDate.SelectedDate.Value)
                                                                    || t.Complete < 100).ToList();
            }
            else if (this.txtMasterFromDate.SelectedDate == null && this.txtMasterToDate.SelectedDate != null)
            {
                projectList = scopeProjectService.GetAll().Where(t => (t.EndDate != null && t.EndDate.Value <= this.txtMasterToDate.SelectedDate.Value)
                                                                    || t.Complete < 100).ToList();
            }
            else if (this.txtMasterFromDate.SelectedDate != null && this.txtMasterToDate.SelectedDate != null)
            {
                projectList = scopeProjectService.GetAll()
                    .Where(t => (t.EndDate != null
                                && t.EndDate.Value.Date >= this.txtMasterFromDate.SelectedDate.Value.Date
                                && t.EndDate.Value.Date <= this.txtMasterToDate.SelectedDate.Value.Date)
                        || t.Complete < 100).ToList();
            }
            else
            {
                projectList = scopeProjectService.GetAll().Where(t => t.Complete < 100).ToList();
            }

            if (!UserSession.Current.IsAdmin && !UserSession.Current.IsDC)
            {
                projectOfPM = projectList.Where(t => t.SupervisorId == UserSession.Current.User.Id).ToList();
            }
            else
            {
                projectOfPM = projectList;
            }

            if (projectOfPM.Count > 0)
            {
                var countProject = 0;
                var dataTable = new DataTable();
                var listColumn = new[]
                            {
                                new DataColumn("Type", Type.GetType("System.String")),
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("Field", Type.GetType("System.String")),
                                new DataColumn("ProjNumber", Type.GetType("System.String")),
                                new DataColumn("ProjName", Type.GetType("System.String")),
                                new DataColumn("WPNumber", Type.GetType("System.String")),
                                new DataColumn("Department", Type.GetType("System.String")),
                                new DataColumn("StartDate", Type.GetType("System.String")),
                                new DataColumn("Deadline", Type.GetType("System.String")),
                                new DataColumn("Area", Type.GetType("System.String")),
                                new DataColumn("Discipline", Type.GetType("System.String")),
                                new DataColumn("Weight", Type.GetType("System.String")),
                                new DataColumn("Complete", Type.GetType("System.String")),
                                new DataColumn("EndDate", Type.GetType("System.String")),
                                new DataColumn("TotalManHours", Type.GetType("System.String")),
                                new DataColumn("UsedManHours", Type.GetType("System.String")),
                                new DataColumn("GIP", Type.GetType("System.String")),
                                new DataColumn("PM", Type.GetType("System.String")),
                            };
                dataTable.Columns.AddRange(listColumn);

                foreach (var scopeProject in projectOfPM)
                {
                    countProject += 1;
                    var dataRowProject = dataTable.NewRow();
                    dataRowProject["Type"] = 1;
                    dataRowProject["Index"] = countProject;
                    dataRowProject["Field"] = scopeProject.FieldName;
                    dataRowProject["ProjNumber"] = scopeProject.Name;
                    dataRowProject["ProjName"] = scopeProject.Description;

                    dataTable.Rows.Add(dataRowProject);

                    var countWP = 0;
                    var wpList = this.workGroupService.GetAllWorkGroupOfProject(scopeProject.ID);
                    foreach (var workGroup in wpList)
                    {
                        countWP += 1;
                        var dataRowWP = dataTable.NewRow();
                        dataRowWP["Type"] = 2;
                        dataRowWP["Index"] = countProject + "." + countWP;
                        dataRowWP["Field"] = scopeProject.FieldName;
                        dataRowWP["ProjNumber"] = scopeProject.Name;
                        dataRowWP["ProjName"] = scopeProject.Description;
                        dataRowWP["WPNumber"] = workGroup.Name;
                        dataRowWP["Department"] = workGroup.DepartmentName;
                        dataRowWP["StartDate"] = workGroup.StartDate != null
                            ? workGroup.StartDate.Value.ToString("dd.MM.yyyy")
                            : string.Empty;
                        dataRowWP["Deadline"] = workGroup.Deadline != null
                            ? workGroup.Deadline.Value.ToString("dd.MM.yyyy")
                            : string.Empty;
                        dataRowWP["Area"] = workGroup.AreaName;
                        dataRowWP["Discipline"] = workGroup.DisciplineName;
                        dataRowWP["Weight"] = workGroup.Weight != null && workGroup.Weight != 0
                            ? workGroup.Weight.Value + "%"
                            : string.Empty;
                        dataRowWP["Complete"] = workGroup.Complete != null && workGroup.Complete != 0
                            ? workGroup.Complete.Value + "%"
                            : string.Empty;
                        dataRowWP["EndDate"] = workGroup.EndDate != null
                            ? workGroup.EndDate.Value.ToString("dd.MM.yyyy")
                            : string.Empty;

                        dataRowWP["TotalManHours"] = workGroup.TotalManHours;
                        dataRowWP["UsedManHours"] = workGroup.UsedManHours;
                        dataRowWP["GIP"] = scopeProject.ProjectManagerFullName;
                        dataRowWP["PM"] = scopeProject.SupervisorFullName;

                        dataTable.Rows.Add(dataRowWP);
                    }
                }

                // Write report file
                var filePath = Server.MapPath("Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\Report_PM.xls");
                var worksheet1 = workbook.Worksheets[0];

                worksheet1.Cells["E3"].PutValue("FROM " + (this.txtPMFromDate.SelectedDate != null ? this.txtPMFromDate.SelectedDate.Value.ToString("dd.MM.yyyy") : string.Empty)
                    + " TO " + (this.txtPMToDate.SelectedDate != null ? this.txtPMToDate.SelectedDate.Value.ToString("dd.MM.yyyy") : string.Empty));

                worksheet1.Cells.ImportDataTable(dataTable, false, 5, 0, dataTable.Rows.Count, 18, true);
                worksheet1.AutoFitRows();
                var filename = "Report_PM_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, "PM Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls", false);
            }
        }

        protected void btnExportGipReport_OnClick(object sender, EventArgs e)
        {
            var strCondition = string.Empty;

            if (this.txtGIPFromDate.SelectedDate != null && this.txtGIPToDate.SelectedDate == null)
            {
                strCondition += "AND (StartDate >= CONVERT(date, '" + this.txtGIPFromDate.SelectedDate.Value.ToString("dd/MM/yyyy") + "', 103) OR Deadline >= CONVERT(date, '" + this.txtGIPFromDate.SelectedDate.Value.ToString("dd/MM/yyyy") + "', 103)) ";

            }
            else if (this.txtGIPFromDate.SelectedDate == null && this.txtGIPToDate.SelectedDate != null)
            {
                strCondition += "AND (StartDate <= CONVERT(date, '" + this.txtGIPToDate.SelectedDate.Value.ToString("dd/MM/yyyy") + "', 103) OR StartDate <= CONVERT(date, '" + this.txtGIPToDate.SelectedDate.Value.ToString("dd/MM/yyyy") + "', 103) ) ";// "OR P.Complete <= 100) ";
            }
            else if (this.txtGIPFromDate.SelectedDate != null && this.txtGIPToDate.SelectedDate != null)
            {
                strCondition += "AND ((StartDate >= CONVERT(date, '" + this.txtGIPFromDate.SelectedDate.Value.ToString("dd/MM/yyyy") + "', 103) " +
                                " AND StartDate <= CONVERT(date, '" + this.txtGIPToDate.SelectedDate.Value.ToString("dd/MM/yyyy") + "', 103))  " +
                                "OR (Deadline >= CONVERT(date, '" + this.txtGIPFromDate.SelectedDate.Value.ToString("dd/MM/yyyy") + "', 103)" +
                                "AND Deadline <= CONVERT(date, '" + this.txtGIPToDate.SelectedDate.Value.ToString("dd/MM/yyyy") + "', 103))" +
                                "OR P.Complete <100)";

            }
            else
            {
                strCondition += " AND P.Complete <=100 ";
            }

            if (!UserSession.Current.IsAdmin && !UserSession.Current.IsDC)
            {
                strCondition += " AND P.ProjectManagerId =" + UserSession.Current.User.Id;
            }

            var dataset = new DataSet();
            using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["SiteSqlServer"]))
            {
                using (var cmd = new SqlCommand("GIPReport", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@p_Condition", strCondition);
                    conn.Open();
                    using (var da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(dataset);
                    }

                    conn.Close();
                }
            }

            if (dataset.Tables.Count > 0)
            {
                var filePath = Server.MapPath("Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\Report_GIP.xls");

                // Get the first worksheet.
                var worksheet1 = workbook.Worksheets[0];

                worksheet1.Cells["E4"].PutValue("From " + this.txtGIPFromDate.SelectedDate.Value.ToString("dd/MM/yyyy") + " To " + this.txtGIPToDate.SelectedDate.Value.ToString("dd/MM/yyyy"));
                worksheet1.Cells["H4"].PutValue(UserSession.Current.User.UserNameWithFullName);
                worksheet1.Cells.ImportDataTable(dataset.Tables[0], false, 6, 0, dataset.Tables[0].Rows.Count, 12, true);

                var filename = "Report_GIP_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, "GIP Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls", false);
            }
        }

        protected void btnExportMasterReport_OnClick(object sender, EventArgs e)
        {

            var projectList = new List<ScopeProject>();
            if (this.txtMasterFromDate.SelectedDate != null && this.txtMasterToDate.SelectedDate == null)
            {
                projectList = scopeProjectService.GetAll().Where(t => (t.EndDate != null && t.EndDate.Value >= this.txtMasterFromDate.SelectedDate.Value)
                                                                    || t.Complete < 100).ToList();
            }
            else if (this.txtMasterFromDate.SelectedDate == null && this.txtMasterToDate.SelectedDate != null)
            {
                projectList = scopeProjectService.GetAll().Where(t => (t.EndDate != null && t.EndDate.Value <= this.txtMasterToDate.SelectedDate.Value)
                                                                    || t.Complete < 100).ToList();
            }
            else if (this.txtMasterFromDate.SelectedDate != null && this.txtMasterToDate.SelectedDate != null)
            {
                projectList = scopeProjectService.GetAll()
                    .Where(t => (t.EndDate != null
                                && t.EndDate.Value.Date >= this.txtMasterFromDate.SelectedDate.Value.Date
                                && t.EndDate.Value.Date <= this.txtMasterToDate.SelectedDate.Value.Date)
                        || t.Complete < 100).ToList();
            }
            else
            {
                projectList = scopeProjectService.GetAll().Where(t => t.Complete < 100).ToList();
            }

            var countField = 64;
            var dataTable = new DataTable();
            var listColumn = new[]
                            {
                                new DataColumn("Type", Type.GetType("System.String")),
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("Name", Type.GetType("System.String")),
                                new DataColumn("Department", Type.GetType("System.String")),
                                new DataColumn("StartDate", Type.GetType("System.String")),
                                new DataColumn("ActualDate", Type.GetType("System.String")),
                                new DataColumn("Deadline", Type.GetType("System.String")),
                                new DataColumn("EndDate", Type.GetType("System.String")),
                                new DataColumn("Complete", Type.GetType("System.String")),
                                new DataColumn("GIP", Type.GetType("System.String")),
                                new DataColumn("PM", Type.GetType("System.String")),
                            };
            dataTable.Columns.AddRange(listColumn);
            // Type: 1-Field, 2-Platform, 3-Project, 4-Workpackage
            foreach (var scopeProjectGroupByField in projectList.Where(t => t.FieldId != 0).OrderBy(t => t.FieldName).GroupBy(t => t.FieldId))
            {
                var countPlatForm = 0;

                var fieldId = scopeProjectGroupByField.Key;
                var fieldObj = this.fieldService.GetById(fieldId.GetValueOrDefault());

                if (fieldObj != null)
                {
                    countField += 1;

                    this.CollectFieldData(ref dataTable, fieldObj, countField);
                }

                foreach (var scopeProjectGroupByPlatform in scopeProjectGroupByField.Where(t => t.PlatformId != 0).OrderBy(t => t.PlatformName).GroupBy(t => t.PlatformId))
                {
                    countPlatForm += 1;

                    var countProject = 0;
                    var platformId = scopeProjectGroupByPlatform.Key;
                    var platformObj = this.platformService.GetById(platformId.GetValueOrDefault());

                    if (platformObj != null)
                    {
                        this.CollectPlatformData(ref dataTable, platformObj, countPlatForm);

                        foreach (var scopeProject in scopeProjectGroupByPlatform)
                        {
                            countProject += 1;

                            var countWP = 0;
                            this.CollectProjectData(ref dataTable, scopeProject, countProject);

                            var wpList = this.workGroupService.GetAllWorkGroupOfProject(scopeProject.ID);
                            foreach (var workGroup in wpList)
                            {
                                countWP += 1;
                                this.CollectWorkpackageData(ref dataTable, workGroup, countProject, countWP);
                            }
                        }
                    }


                }

                // Get infor for Projects that dont have Platform infor
                var projectWithoutPlatform = scopeProjectGroupByField.Where(t => t.PlatformId == 0).ToList();
                if (projectWithoutPlatform.Any())
                {
                    countPlatForm += 1;
                    var countProjectWithoutPlatform = 0;
                    var emptyPlatform = new Platform() { Name = "Other" };

                    this.CollectPlatformData(ref dataTable, emptyPlatform, countPlatForm);

                    foreach (var scopeProject in projectWithoutPlatform)
                    {
                        countProjectWithoutPlatform += 1;

                        var countWP = 0;
                        this.CollectProjectData(ref dataTable, scopeProject, countProjectWithoutPlatform);

                        var wpList = this.workGroupService.GetAllWorkGroupOfProject(scopeProject.ID);
                        foreach (var workGroup in wpList)
                        {
                            countWP += 1;
                            this.CollectWorkpackageData(ref dataTable, workGroup, countProjectWithoutPlatform, countWP);
                        }
                    }
                }

            }

            // Get infor for Projects that dont have Field infor
            var projectWithoutField = projectList.Where(t => t.FieldId == 0).ToList();
            if (projectWithoutField.Any())
            {
                countField += 1;
                var countProjectWithoutField = 0;
                var emptyField = new Field() { Description = "Other" };
                this.CollectFieldData(ref dataTable, emptyField, countField);

                foreach (var scopeProject in projectWithoutField)
                {
                    countProjectWithoutField += 1;

                    var countWP = 0;
                    this.CollectProjectData(ref dataTable, scopeProject, countProjectWithoutField);

                    var wpList = this.workGroupService.GetAllWorkGroupOfProject(scopeProject.ID);
                    foreach (var workGroup in wpList)
                    {
                        countWP += 1;
                        this.CollectWorkpackageData(ref dataTable, workGroup, countProjectWithoutField, countWP);
                    }
                }
            }

            // Write report file
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\Report_Master.xls");
            var worksheet1 = workbook.Worksheets[0];

            worksheet1.Cells["C1"].PutValue("Текущее состояние проектных работ НИПИморнефтегаз на " + DateTime.Now.ToString("dd.MM.yyyy") + "г.");
            worksheet1.Cells["C1"].PutValue("From  " + this.txtMasterFromDate.SelectedDate.Value.ToString("dd.MM.yyyy") + " To " + this.txtMasterToDate.SelectedDate.Value.ToString("dd.MM.yyyy"));
            worksheet1.Cells.ImportDataTable(dataTable, false, 6, 0, dataTable.Rows.Count, 11, true);

            var filename = "Report_Master_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, "Master Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls", false);
        }

        private void CollectFieldData(ref DataTable dataTable, Field fieldObj, int countField)
        {
            var dataRow = dataTable.NewRow();
            dataRow["Type"] = 1;
            dataRow["Index"] = Convert.ToChar(countField);
            dataRow["Name"] = fieldObj.Description;

            dataTable.Rows.Add(dataRow);
        }

        private void CollectPlatformData(ref DataTable dataTable, Platform platformObj, int countPlatForm)
        {
            var dataRow = dataTable.NewRow();
            dataRow["Type"] = 2;
            dataRow["Index"] = Utility.ToRoman(countPlatForm);
            dataRow["Name"] = platformObj.Name;

            dataTable.Rows.Add(dataRow);
        }

        private void CollectProjectData(ref DataTable dataTable, ScopeProject scopeProject, int countProject)
        {
            var dataRowProject = dataTable.NewRow();
            dataRowProject["Type"] = 3;
            dataRowProject["Index"] = countProject;
            dataRowProject["Name"] = scopeProject.Name + "\n" + scopeProject.Description;

            dataRowProject["StartDate"] = scopeProject.StartDate != null
                ? scopeProject.StartDate.Value.ToString("dd.MM.yyyy")
                : string.Empty;
            dataRowProject["Deadline"] = scopeProject.Deadline != null
                ? scopeProject.Deadline.Value.ToString("dd.MM.yyyy")
                : string.Empty;
            dataRowProject["EndDate"] = scopeProject.EndDate != null
                ? scopeProject.EndDate.Value.ToString("dd.MM.yyyy")
                : string.Empty;
            dataRowProject["Complete"] = scopeProject.Complete != null && scopeProject.Complete != 0
                ? scopeProject.Complete.Value + "%"
                : string.Empty;

            dataRowProject["GIP"] = scopeProject.ProjectManagerFullName;
            dataRowProject["PM"] = scopeProject.SupervisorFullName;

            dataTable.Rows.Add(dataRowProject);
        }

        private void CollectWorkpackageData(ref DataTable dataTable, WorkGroup workGroup, int countProject, int countWP)
        {
            var department = this.roleService.GetByID(workGroup.DepartmentId.GetValueOrDefault());
            var dataRowWP = dataTable.NewRow();
            dataRowWP["Type"] = 4;
            dataRowWP["Index"] = countProject + "." + countWP;
            dataRowWP["Name"] = workGroup.Name;
            dataRowWP["Department"] = department != null ? department.Name : string.Empty;
            dataRowWP["StartDate"] = workGroup.StartDate != null
                ? workGroup.StartDate.Value.ToString("dd.MM.yyyy")
                : string.Empty;
            dataRowWP["Deadline"] = workGroup.Deadline != null
                ? workGroup.Deadline.Value.ToString("dd.MM.yyyy")
                : string.Empty;
            dataRowWP["EndDate"] = workGroup.EndDate != null
                ? workGroup.EndDate.Value.ToString("dd.MM.yyyy")
                : string.Empty;
            dataRowWP["Complete"] = workGroup.Complete != null && workGroup.Complete != 0
                ? workGroup.Complete.Value + "%"
                : string.Empty;

            dataTable.Rows.Add(dataRowWP);
        }

        protected void btnExportChiefReport_Click(object sender, EventArgs e)
        {
            List<int> specialUser = new List<int>() { 210, 257, 81, 78, 1, 87, 342, 191 };
            var role = new Role();
            var listUserofDepartment = new List<User>();
            if (specialUser.Contains(UserSession.Current.User.Id))
            {
                role = this.roleService.GetByID(Convert.ToInt32(this.ddlDepartmentManhourChiefReport.SelectedValue));
                listUserofDepartment = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlDepartmentManhourChiefReport.SelectedValue)).ToList();
            }
            else
            {
                role = this.roleService.GetByID(UserSession.Current.User.RoleId.GetValueOrDefault());
                listUserofDepartment = this.userService.GetAllByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).ToList();
            }

            var dtFull = new DataTable();
            var ListColumn = new[]
            {
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("Rev", typeof(String)),
                        new DataColumn("StartDate", typeof(String)),
                        new DataColumn("Deadline", typeof(String)),
                        new DataColumn("EndDate", typeof(String)),
                        new DataColumn("Weight", typeof(String)),
                        new DataColumn("Complete", typeof(String)),
                        new DataColumn("ManHourPlan", typeof(String)),
                        new DataColumn("ManHourActual", typeof(String)),
                        new DataColumn("ManHourActualAsPercent", typeof(String)),
                        new DataColumn("OverDue", typeof(String)),
                        new DataColumn("Workpackage", typeof(String)),
                        new DataColumn("Note", typeof(String)),
            };
            dtFull.Columns.AddRange(ListColumn);
            var dataSum = new DataTable();
            var List = new[]
            {
                        new DataColumn("Index", typeof(String)),
                        new DataColumn("User", typeof(String)),
                        new DataColumn("TotalDocuments", typeof(String)),
                        new DataColumn("CompleteDocuments",typeof(String)),
                        new DataColumn("UncompleteDocuments",typeof(String)),
                        new DataColumn("Overdue",typeof(String)),
                        new DataColumn("ManHourPlan", typeof(String)),
                        new DataColumn("ManHourActual", typeof(String)),
                        new DataColumn("ManHourActualAsPercent", typeof(String)),

            };
            dataSum.Columns.AddRange(List);
            int month = Convert.ToInt32(ddlMonthManhourChiefReport.SelectedValue);
            int year = Convert.ToInt32(ddlYearManhourChiefReport.SelectedValue);
            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\Report_Manhour_For_Chief.xls");
            var sheets = workbook.Worksheets;
            var sheetsummary = sheets[1];
            sheetsummary.Cells["D1"].PutValue(role.Name + " DEPARTMENT - SUMMARY REPORT "
                + month + "/" + year);
            sheetsummary.Cells["K1"].PutValue(role.Name);
            sheetsummary.Cells["K3"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
            sheetsummary.Cells["A4"].PutValue(listUserofDepartment.Count);

            //var us = userService.GetByID(8);
            //listUserofDepartment = new List<User>();
            //listUserofDepartment.Add(us);

            var listIdUserofDepartment = listUserofDepartment.Select(t => t.Id).ToList();
            var r = listIdUserofDepartment.ConvertAll<string>(x => x.ToString());
            var documentAllUserList = this.documentPackageService.GetAllByEngineer(r);
            var documentEndNullList = documentAllUserList.Where(t => t.EndDate == null).ToList();
            var documentEndList = documentAllUserList.Where(t => t.EndDate != null).ToList();
            var stt = 1;
            var counttotal = 0;
            var sumchuahoanthanh = 0;
            var sumhoanthanhtrongthang = 0;
            var sumtoihan = 0;
            var sumtrehan = 0;
            var documentEngList = new List<DocumentPackage>();
            var documentLeaderList = new List<DocumentPackage>();
            string[] temlist = new string[listUserofDepartment.Count];
            var sumrow = dataSum.NewRow();
            int trehan = 0;
            int toihan = 0;
            int chuahoanthanh = 0;
            int hoanthanhtrongthang = 0;
            double manhourActualTotal = 0;
            double manhourActualAsPercentTotal = 0;
            double manhourPlanTotal = 0;
            double manhourActual = 0;
            double manhourActualAsPercent = 0;
            double manhourPlan = 0;
            double sumManhourPlanTotal = 0;
            double sumManhourActualTotal = 0;
            double sumManhourActualAsPercentTotal = 0;
            for (int i = 0; i < listUserofDepartment.Count; i++)
            {
                sumrow = dataSum.NewRow();
                trehan = 0;
                toihan = 0;
                hoanthanhtrongthang = 0;
                chuahoanthanh = 0;
                manhourActualTotal = 0.0;
                manhourActualAsPercentTotal = 0;
                manhourPlanTotal = 0.0;
                manhourActual = 0.0;
                manhourActualAsPercent = 0;
                manhourPlan = 0.0;

                //documentList = this.documentPackageService.GetAllByEngineer(listUserofDepartment[i].Id)
                //    .Where(t => (t.StartDate != null
                //                && t.StartDate.Value >= this.txtChiefFromDate.SelectedDate.Value
                //                && t.StartDate.Value <= this.txtChiefToDate.SelectedDate.Value)
                //                || (t.StartDate != null && t.EndDate != null
                //                && t.StartDate.Value < this.txtChiefFromDate.SelectedDate.Value
                //                && t.EndDate.Value > this.txtChiefToDate.SelectedDate.Value)
                //                || (t.EndDate != null
                //                && t.EndDate.Value >= this.txtChiefFromDate.SelectedDate.Value
                //                && t.EndDate.Value <= this.txtChiefToDate.SelectedDate.Value)
                //                || (t.StartDate != null && t.Deadline != null
                //                && t.StartDate.Value < this.txtChiefFromDate.SelectedDate.Value
                //                && t.Deadline.Value > this.txtChiefToDate.SelectedDate.Value)
                //                || (t.Deadline != null
                //                && t.Deadline.Value >= this.txtChiefFromDate.SelectedDate.Value
                //                && t.Deadline.Value <= this.txtChiefToDate.SelectedDate.Value)
                //        ).OrderByDescending(t => t.CreatedDate).ToList();

                var documentList1 = documentEndNullList.Where(t => (t.EngineerId.Split(',').ToList()).Any(listUserofDepartment[i].Id.ToString().Equals)
                                && (t.StartDate != null && t.Deadline != null)
                                && (t.Deadline.Value.Year >= year && t.Deadline.Value.Month >= month)
                                && ((t.StartDate.Value.Year < year) || (t.StartDate.Value.Year == year && t.StartDate.Value.Month <= month))).ToList();
                var documentList2 = documentEndList.Where(t => (t.EngineerId.Split(',').ToList()).Any(listUserofDepartment[i].Id.ToString().Equals)
                                && (t.StartDate != null && t.Deadline != null)
                                && (t.EndDate.Value.Year >= year && t.EndDate.Value.Month >= month)
                                && ((t.StartDate.Value.Year < year) || (t.StartDate.Value.Year == year && t.StartDate.Value.Month <= month))).ToList();
                documentEngList = (documentList1.Concat(documentList2)).OrderByDescending(t => t.CreatedDate).ToList();

                var documentLeaderList1 = documentEndNullList.Where(t => t.LeaderId == listUserofDepartment[i].Id
                                && (t.StartDate != null && t.Deadline != null)
                                && (t.Deadline.Value.Year >= year && t.Deadline.Value.Month >= month)
                                && ((t.StartDate.Value.Year < year) || (t.StartDate.Value.Year == year && t.StartDate.Value.Month <= month))).ToList();
                var documentLeaderList2 = documentEndList.Where(t => t.LeaderId == listUserofDepartment[i].Id
                                && (t.StartDate != null && t.Deadline != null)
                                && (t.EndDate.Value.Year >= year && t.EndDate.Value.Month >= month)
                                && ((t.StartDate.Value.Year < year) || (t.StartDate.Value.Year == year && t.StartDate.Value.Month <= month))).ToList();
                documentLeaderList = (documentLeaderList1.Concat(documentLeaderList2)).OrderByDescending(t => t.CreatedDate).ToList();

                //var documentList1 = documentEndNullList.Where(t => (t.EngineerId.Split(',').ToList()).Any(listUserofDepartment[i].Id.ToString().Equals)
                //                && ((t.StartDate != null
                //                && t.StartDate.Value >= this.txtChiefFromDate.SelectedDate.Value
                //               && t.StartDate.Value <= this.txtChiefToDate.SelectedDate.Value)
                //               || (t.StartDate != null && t.Deadline != null
                //                && t.StartDate.Value < this.txtChiefFromDate.SelectedDate.Value
                //                && t.Deadline.Value > this.txtChiefToDate.SelectedDate.Value)
                //                || (t.Deadline != null
                //                && t.Deadline.Value >= this.txtChiefFromDate.SelectedDate.Value
                //                && t.Deadline.Value <= this.txtChiefToDate.SelectedDate.Value))).ToList();
                //var documentList2 = documentEndList.Where(t => (t.EngineerId.Split(',').ToList()).Any(listUserofDepartment[i].Id.ToString().Equals)
                //                && ((t.StartDate != null
                //                && t.StartDate.Value >= this.txtChiefFromDate.SelectedDate.Value
                //                && t.StartDate.Value <= this.txtChiefToDate.SelectedDate.Value)
                //                || (t.StartDate != null && t.EndDate != null
                //                && t.StartDate.Value < this.txtChiefFromDate.SelectedDate.Value
                //                && t.EndDate.Value > this.txtChiefToDate.SelectedDate.Value)
                //                || (t.EndDate != null
                //                && t.EndDate.Value >= this.txtChiefFromDate.SelectedDate.Value
                //                && t.EndDate.Value <= this.txtChiefToDate.SelectedDate.Value))).ToList();
                //documentEngList = (documentList1.Concat(documentList2)).OrderByDescending(t => t.CreatedDate).ToList();
                //var documentLeaderList1 = documentEndNullList.Where(t => t.LeaderId == listUserofDepartment[i].Id
                //                && ((t.StartDate != null
                //                && t.StartDate.Value >= this.txtChiefFromDate.SelectedDate.Value
                //               && t.StartDate.Value <= this.txtChiefToDate.SelectedDate.Value)
                //               || (t.StartDate != null && t.Deadline != null
                //                && t.StartDate.Value < this.txtChiefFromDate.SelectedDate.Value
                //                && t.Deadline.Value > this.txtChiefToDate.SelectedDate.Value)
                //                || (t.Deadline != null
                //                && t.Deadline.Value >= this.txtChiefFromDate.SelectedDate.Value
                //                && t.Deadline.Value <= this.txtChiefToDate.SelectedDate.Value))).ToList();
                //var documentLeaderList2 = documentEndList.Where(t => t.LeaderId == listUserofDepartment[i].Id
                //                && ((t.StartDate != null
                //                && t.StartDate.Value >= this.txtChiefFromDate.SelectedDate.Value
                //                && t.StartDate.Value <= this.txtChiefToDate.SelectedDate.Value)
                //                || (t.StartDate != null && t.EndDate != null
                //                && t.StartDate.Value < this.txtChiefFromDate.SelectedDate.Value
                //                && t.EndDate.Value > this.txtChiefToDate.SelectedDate.Value)
                //                || (t.EndDate != null
                //                && t.EndDate.Value >= this.txtChiefFromDate.SelectedDate.Value
                //                && t.EndDate.Value <= this.txtChiefToDate.SelectedDate.Value))).ToList();
                //documentLeaderList = (documentLeaderList1.Concat(documentLeaderList2)).OrderByDescending(t => t.CreatedDate).ToList();

                dtFull.Rows.Clear();
                sheets.AddCopy(2);
                sheets[i + 3].Name = listUserofDepartment[i].Username + "_" + i.ToString();
                sheets[i + 3].Cells["Q4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                sheets[i + 3].Cells["R6"].PutValue("Summary");
                sheets[i + 3].Hyperlinks.Add("R6", 1, 1, "Summary!D1");
                sheets[i + 3].Cells["D1"].PutValue("Document List Of Engineer " + listUserofDepartment[i].Username + " "
                    + month + "/" + year);
                var tempCount = documentEngList.Count + documentLeaderList.Count + 2;
                sheets[i + 3].Cells["A4"].PutValue(tempCount);
                //sheets[i + 3].Cells["A4"].PutValue(documentEngList.Count);

                //Engineer
                var count = 1;
                var drEng = dtFull.NewRow();
                drEng["NoIndex"] = "Engineer";
                dtFull.Rows.Add(drEng);
                foreach (var document in documentEngList)
                {
                    manhourPlan = 0;
                    manhourActual = 0;
                    manhourActualAsPercent = 0;
                    var planTotal = (document.ManHourPlan.GetValueOrDefault() * 95) / 100;
                    var diffMonth = ((document.Deadline.GetValueOrDefault().Year - document.StartDate.GetValueOrDefault().Year) * 12) + document.Deadline.GetValueOrDefault().Month - document.StartDate.GetValueOrDefault().Month;
                    var dif = document.Deadline.GetValueOrDefault() - document.StartDate.GetValueOrDefault();
                    var countEng = document.EngineerId.Split(',').Length;
                    manhourPlan = countEng > 0 ? planTotal / countEng : planTotal;
                    manhourPlan = diffMonth > 0 ? manhourPlan / diffMonth : manhourPlan;

                    var userManhourMonthObj = this.userManhourMonthService.GetByDocIDAndUserID(document.ID, listUserofDepartment[i].Id, month, year);
                    if (userManhourMonthObj != null)
                    {
                        var complete = userManhourMonthObj.CompleteMonth.GetValueOrDefault();
                        var actual = userManhourMonthObj.ManhourActualMonth.GetValueOrDefault();

                        var doc = this.documentPackageService.GetById(document.ID);
                        if (doc.ParentId != null)
                        {
                            var docPrevRev = this.documentPackageService.GetAllRevDoc(doc.ParentId.GetValueOrDefault()).OrderByDescending(t => t.ID).First(t => t.ID != doc.ID);
                            var actualPrevRev = docPrevRev.ManHours.GetValueOrDefault();
                            actual = actual - actualPrevRev;
                        }
                        manhourActual = actual > 0 ? (actual * 95) / 100 : 0;
                        manhourActualAsPercent = (complete * planTotal) / 100;
                    }

                    manhourActualTotal += manhourActual;
                    manhourActualAsPercentTotal += manhourActualAsPercent;
                    manhourPlanTotal += manhourPlan;
                    var dataRow = dtFull.NewRow();
                    dataRow = dtFull.NewRow();
                    dataRow["NoIndex"] = count;
                    dataRow["DocNo"] = document.DocNo;
                    dataRow["DocTitle"] = document.DocTitle;
                    dataRow["Rev"] = document.RevisionName;
                    dataRow["StartDate"] = document.StartDate != null
                      ? document.StartDate.Value.ToString("MM/dd/yyyy")
                      : string.Empty;
                    dataRow["DeadLine"] = document.Deadline != null
                      ? document.Deadline.Value.ToString("MM/dd/yyyy")
                      : string.Empty;
                    dataRow["EndDate"] = document.EndDate != null
                      ? document.EndDate.Value.ToString("MM/dd/yyyy")
                      : string.Empty;
                    dataRow["Complete"] = document.Complete != null && document.Complete != 0
                      ? Math.Round(document.Complete.Value, 2) + "%" : "0%";
                    dataRow["Weight"] = document.Weight != null && document.Weight != 0
                      ? Math.Round(document.Weight.Value, 2) + "%" : "0%";
                    dataRow["ManHourPlan"] = Math.Round(manhourPlan, 2);
                    dataRow["ManHourActual"] = Math.Round(manhourActual, 2);
                    dataRow["ManHourActualAsPercent"] = Math.Round(manhourActualAsPercent, 2);
                    dataRow["OverDue"] = document.Deadline != null &&
                        (DateTime.Now.Date > document.Deadline.Value.Date) &&
                        (document.Complete < 100) ? "X" : string.Empty;
                    trehan += document.Deadline != null &&
                         (DateTime.Now.Date > document.Deadline.Value.Date) &&
                         (document.Complete < 100) ? 1 : 0;
                    toihan += document.Deadline != null &&
                         (DateTime.Now.Date < document.Deadline.Value.Date && document.Deadline.Value.Date < DateTime.Now.AddDays(3).Date) &&
                         (document.Complete < 100) ? 1 : 0;
                    chuahoanthanh += (document.Complete < 100 || document.Complete == null) ? 1 : 0;
                    hoanthanhtrongthang += (document.EndDate.GetValueOrDefault().Month == month) ? 1 : 0;
                    dataRow["Workpackage"] = document.WorkgroupName;
                    dataRow["Note"] = document.Notes;
                    count++;
                    dtFull.Rows.Add(dataRow);
                }
                //Leader
                count = 1;
                var drLead = dtFull.NewRow();
                drLead["NoIndex"] = "Leader";
                dtFull.Rows.Add(drLead);
                foreach (var document in documentLeaderList)
                {
                    manhourPlan = 0;
                    manhourActual = 0;
                    manhourActualAsPercent = 0;
                    var diffMonth = ((document.Deadline.GetValueOrDefault().Year - document.StartDate.GetValueOrDefault().Year) * 12) + document.Deadline.GetValueOrDefault().Month - document.StartDate.GetValueOrDefault().Month;
                    var planTotal = (document.ManHourPlan.GetValueOrDefault() * 5) / 100;
                    manhourPlan = diffMonth > 0 ? planTotal / diffMonth : planTotal;

                    var userManhourMonthList = this.userManhourMonthService.GetByDocID(document.ID, month, year);
                    if (userManhourMonthList.Count > 0)
                    {
                        var complete = userManhourMonthList.Sum(t => t.CompleteMonth.GetValueOrDefault());
                        manhourActual = userManhourMonthList.Sum(t => t.ManhourActualMonth.GetValueOrDefault());

                        var doc = this.documentPackageService.GetById(document.ID);
                        if (doc.ParentId != null)
                        {
                            var docPrevRev = this.documentPackageService.GetAllRevDoc(doc.ParentId.GetValueOrDefault()).OrderByDescending(t => t.ID).First(t => t.ID != doc.ID);
                            var actualPrevRev = docPrevRev.ManHours.GetValueOrDefault();
                            manhourActual = manhourActual - actualPrevRev;
                        }

                        manhourActual = (manhourActual * 5) / 100;
                        manhourActualAsPercent = (complete * planTotal) / 100;
                    }

                    manhourActualAsPercentTotal += manhourActualAsPercent;
                    manhourActualTotal += manhourActual;
                    manhourPlanTotal += manhourPlan;

                    var dataRow = dtFull.NewRow();
                    dataRow = dtFull.NewRow();
                    dataRow["NoIndex"] = count;
                    dataRow["DocNo"] = document.DocNo;
                    dataRow["DocTitle"] = document.DocTitle;
                    dataRow["Rev"] = document.RevisionName;
                    dataRow["StartDate"] = document.StartDate != null
                      ? document.StartDate.Value.ToString("MM/dd/yyyy")
                      : string.Empty;
                    dataRow["DeadLine"] = document.Deadline != null
                      ? document.Deadline.Value.ToString("MM/dd/yyyy")
                      : string.Empty;
                    dataRow["EndDate"] = document.EndDate != null
                      ? document.EndDate.Value.ToString("MM/dd/yyyy")
                      : string.Empty;
                    dataRow["Complete"] = document.Complete != null && document.Complete != 0
                      ? Math.Round(document.Complete.Value, 2) + "%" : "0%";
                    dataRow["Weight"] = document.Weight != null && document.Weight != 0
                      ? Math.Round(document.Weight.Value, 2) + "%" : "0%";
                    dataRow["ManHourPlan"] = Math.Round(manhourPlan, 2);
                    dataRow["ManHourActual"] = Math.Round(manhourActual, 2);
                    dataRow["ManHourActualAsPercent"] = Math.Round(manhourActualAsPercent, 2);
                    dataRow["OverDue"] = document.Deadline != null &&
                        (DateTime.Now.Date > document.Deadline.Value.Date) &&
                        (document.Complete < 100) ? "X" : string.Empty;

                    trehan += document.Deadline != null &&
                         (DateTime.Now.Date > document.Deadline.Value.Date) &&
                         (document.Complete < 100) ? 1 : 0;

                    toihan += document.Deadline != null &&
                         (DateTime.Now.Date < document.Deadline.Value.Date && document.Deadline.Value.Date < DateTime.Now.AddDays(3).Date) &&
                         (document.Complete < 100) ? 1 : 0;
                    //chuahoanthanh += (document.Complete < 100 || document.Complete == null) ? 1 : 0;
                    dataRow["Workpackage"] = document.WorkgroupName;
                    dataRow["Note"] = document.Notes;
                    dtFull.Rows.Add(dataRow);
                    count++;
                }

                sheets[i + 3].Cells["A5"].PutValue("FALSE");
                sheets[i + 3].Cells.ImportDataTable(dtFull, false, 6, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                sumrow["Index"] = stt;
                sumrow["User"] = listUserofDepartment[i].FullName;
                sumrow["TotalDocuments"] = documentEngList.Count;
                sumrow["CompleteDocuments"] = hoanthanhtrongthang;
                sumrow["UncompleteDocuments"] = chuahoanthanh;
                sumrow["Overdue"] = trehan;
                sumrow["ManHourPlan"] = Math.Round(manhourPlanTotal, 2);
                sumrow["ManHourActual"] = Math.Round(manhourActualTotal, 2);
                sumrow["ManHourActualAsPercent"] = Math.Round(manhourActualAsPercentTotal, 2);
                //sumrow["ManHourPlan"] = documentEngList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                //sumrow["ManHourReal"] = documentEngList.Sum(t => t.ManHours.GetValueOrDefault());

                //sumrow["Due"] = toihan;
                dataSum.Rows.Add(sumrow);
                counttotal += documentEngList.Count;
                sumchuahoanthanh += chuahoanthanh;
                sumhoanthanhtrongthang += hoanthanhtrongthang;
                sumtoihan += toihan;
                sumtrehan += trehan;
                sumManhourPlanTotal += manhourPlanTotal;
                sumManhourActualTotal += manhourActualTotal;
                sumManhourActualAsPercentTotal += manhourActualAsPercentTotal;
                sheets[i + 3].Cells["K" + (tempCount + 7)].PutValue(Math.Round(manhourPlanTotal, 2));
                sheets[i + 3].Cells["L" + (tempCount + 7)].PutValue(Math.Round(manhourActualTotal, 2));
                sheets[i + 3].Cells["M" + (tempCount + 7)].PutValue(Math.Round(manhourActualAsPercentTotal, 2));
                temlist[stt - 1] = "C" + (stt + 6) + ";" + listUserofDepartment[i].Username + "_" + i.ToString() + "!A1";
                stt++;
            }
            sheetsummary.Cells.ImportDataTable(dataSum, false, 6, 1, dataSum.Rows.Count, dataSum.Columns.Count, false);
            sheetsummary.Cells["A5"].PutValue("FALSE");
            sheetsummary.Cells["C" + (stt + 6)].PutValue("Sum");
            sheetsummary.Cells["D" + (stt + 6)].PutValue(counttotal);
            sheetsummary.Cells["E" + (stt + 6)].PutValue(sumhoanthanhtrongthang);
            sheetsummary.Cells["F" + (stt + 6)].PutValue(sumchuahoanthanh);
            sheetsummary.Cells["G" + (stt + 6)].PutValue(sumtrehan);
            sheetsummary.Cells["H" + (stt + 6)].PutValue(Math.Round(sumManhourPlanTotal, 2));
            sheetsummary.Cells["I" + (stt + 6)].PutValue(Math.Round(sumManhourActualTotal, 2));
            sheetsummary.Cells["J" + (stt + 6)].PutValue(Math.Round(sumManhourActualAsPercentTotal, 2));

            //sheetsummary.Cells["J" + (stt + 6)].PutValue(sumtoihan);
            foreach (var row in temlist)
            {
                var rage = row.Substring(0, row.IndexOf(';'));
                var link = row.Substring(rage.Length + 1, (row.Length - rage.Length) - 1);

                int ind = sheetsummary.Hyperlinks.Add(rage, 1, 1, link);
            }

            sheets[2].IsVisible = false;

            var filename = "Report_Chief_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, "Report Chief_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls", false);
        }

        //protected void btnReportManhourForCheif_Click(object sender, EventArgs e)
        //{
        //    var role = this.roleService.GetByID(UserSession.Current.User.RoleId.GetValueOrDefault());
        //    var listuser = this.userService.GetAllByRoleId(role.Id).Where(t => t.IsEngineer.GetValueOrDefault()).ToList();

        //    var filePath = Server.MapPath("~/Exports") + @"\";
        //    var workbook = new Workbook();
        //    workbook.Open(filePath + @"Template\ReportManHour_Chief.xls");
        //    var sheets = workbook.Worksheets;
        //    var sheet = sheets[0];
        //    List<DocumentPackage> doclist = new List<DocumentPackage>();

        //    sheet.Cells["D1"].PutValue("Man hour report for department " + role.Name + " (Cut-off: From  " + this.txtManhourFromDate.SelectedDate.Value.ToString("dd/MM/yyyy") + " To " + this.txtManhourToDate.SelectedDate.Value.ToString("dd/MM/yyyy") + " )");
        //    sheet.Cells["A5"].PutValue("FALSE");

        //    if (this.txtManhourFromDate.SelectedDate != null && this.txtManhourToDate.SelectedDate != null)
        //    {
        //        var wplist = this.workGroupService.GetAllByDepartment(role.Id).ToList();

        //        var projectlist = wplist.Select(t => new { t.ProjectId, t.ProjectName }).Distinct().ToList();

        //        int i = 3;
        //        foreach (var users in listuser)
        //        {
        //            sheet.Cells[4, i].PutValue(users.Username);
        //            sheet.Cells[5, i].PutValue("Planning");
        //            sheet.Cells[5, i + 1].PutValue("Total Actural");
        //            i += 2;
        //        }
        //        sheet.Cells[4, i].PutValue("Sum UsedManHours Of Project");
        //        sheet.Cells["A1"].PutValue(ColumnIndexToColumnLetter(i + 1));
        //        sheet.Cells["A3"].PutValue(i + 1);
        //        var j = 0;
        //        foreach (var pj in projectlist)
        //        {
        //            List<int> wplistid = wplist.Where(t => t.ProjectId == pj.ProjectId).Select(t => t.ID).Distinct().ToList();
        //            doclist = this.documentPackageService.GetAllByWorkgroupInPermission(wplistid)
        //          .Where(t => (t.StartDate != null
        //                          && t.StartDate.Value.Date >= this.txtManhourFromDate.SelectedDate.Value.Date
        //                          && t.StartDate.Value.Date <= this.txtManhourToDate.SelectedDate.Value.Date)
        //                  || (t.Deadline != null
        //                      && t.Deadline.Value.Date >= this.txtManhourFromDate.SelectedDate.Value.Date
        //                      && t.Deadline.Value.Date <= this.txtManhourToDate.SelectedDate.Value.Date)).ToList();

        //            j += 1;
        //            sheet.Cells["B" + (6 + j)].PutValue(j);
        //            sheet.Cells["C" + (6 + j)].PutValue(pj.ProjectName);
        //            i = 3;
        //            var sum = 0.0;
        //            foreach (var us in listuser)
        //            {

        //                var plan = 0.0;
        //                var tatol = 0.0;
        //                var doclistofeng = doclist.Where(t => t.EngineerId == us.Id.ToString()).ToList();
        //                plan = doclistofeng.Aggregate(plan, (current, t) => current + t.ManHourPlan.GetValueOrDefault());
        //                tatol = doclistofeng.Aggregate(tatol, (current, t) => current + t.ManHours.GetValueOrDefault());
        //                sheet.Cells[j + 5, i].PutValue(plan);
        //                i++;
        //                sheet.Cells[j + 5, i].PutValue(tatol);
        //                sum += tatol;
        //                i++;
        //            }

        //            sheet.Cells[j + 5, i].PutValue(sum);

        //            var project = this.scopeProjectService.GetById(pj.ProjectId.GetValueOrDefault());
        //            if (project != null)
        //            {
        //                if (!string.IsNullOrEmpty(project.Notes))
        //                {
        //                    sheet.Cells[j + 5, i + 1].PutValue(project.Notes);
        //                    var range = sheet.Cells.CreateRange(j + 5, 2, j + 5, i + 1);
        //                    Style stl0 = workbook.Styles[workbook.Styles.Add()];
        //                    stl0.BackgroundColor = Color.Yellow;
        //                    //Set the solid background fillment.
        //                    stl0.Pattern = BackgroundType.Solid;
        //                    //Set a font.
        //                    StyleFlag flag = new StyleFlag();
        //                    //Apply cell shading.
        //                    flag.CellShading = true;
        //                    //Apply font.
        //                    flag.FontName = true;
        //                    //Apply font size.
        //                    flag.FontSize = true;
        //                    //Apply font color.
        //                    flag.FontColor = true;
        //                    //Apply bold font.
        //                    flag.FontBold = true;
        //                    //Apply italic attribute.
        //                    flag.FontItalic = true;

        //                    range.ApplyStyle(stl0, flag);
        //                }
        //            }
        //        }
        //        sheet.Cells["A2"].PutValue(6 + j);
        //        // sheet.AutoFitColumns();
        //        var filename = "Report_Manhours_Department_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
        //        workbook.Save(filePath + filename);
        //        this.DownloadByWriteByte(filePath + filename, "Report_Manhours_Department_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls", false);
        //    }

        //}

        static string ColumnIndexToColumnLetter(int colIndex)
        {
            int div = colIndex;
            string colLetter = String.Empty;
            int mod = 0;

            while (div > 0)
            {
                mod = (div - 1) % 26;
                colLetter = (char)(65 + mod) + colLetter;
                div = (int)((div - mod) / 26);
            }
            return colLetter;
        }

        protected void btnExportManhoursGip_Click(object sender, EventArgs e)
        {
            if (this.ddlProject.SelectedValue != null)
            {
                var wplist = this.workGroupService.GetAllWorkGroupOfProject(Int32.Parse(this.ddlProject.SelectedValue));
                List<int> departmentIds = wplist.Select(t => t.DepartmentId.GetValueOrDefault()).Distinct().ToList();
                var rolelist = this.roleService.GetAll(false).Where(t => departmentIds.Contains(t.Id)).ToList();
                var project = this.scopeProjectService.GetById(Int32.Parse(this.ddlProject.SelectedValue));


                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\ReportManHour_Gip.xls");
                var sheets = workbook.Worksheets;
                var sheet = sheets[0];

                int i = 0;
                List<DocumentPackage> doclist = new List<DocumentPackage>();

                sheet.Cells["D1"].PutValue(this.ddlProject.SelectedItem.Text + " Cut-off: " + DateTime.Now.ToString("dd/MM/yyyy"));
                sheet.Cells["A5"].PutValue("FALSE");

                if (!string.IsNullOrEmpty(project.Notes))
                {
                    sheet.Cells["I1"].PutValue("Notes");
                    sheet.Cells["J1"].PutValue(project.Notes);
                    // Aspose.Cells.GridCell cell = sheet.Cells["A1"];
                    //Style style = cell.GetStyle();
                    // sheet.Cells["I1"].Style.BackgroundColor = Color.Yellow;
                    // sheet.Cells["I1"].Style.Font.IsBold = true;
                    //  sheet.Cells["J1"].Style.BackgroundColor = Color.Yellow; 


                }

                foreach (var role in rolelist)
                {
                    i++;
                    sheet.Cells["B" + (i + 7)].PutValue(i);
                    sheet.Cells["C" + (i + 7)].PutValue(role.Name + "(" + role.Description + ")");
                    var wpofdep = wplist.Where(t => t.DepartmentId == role.Id).Select(t => t.ID).Distinct().ToList();
                    doclist = this.documentPackageService.GetAllByWorkgroupInPermission(wpofdep).ToList();


                    var plan = 0.0;
                    var tatol = 0.0;
                    plan = doclist.Aggregate(plan, (current, t) => current + t.ManHourPlan.GetValueOrDefault());
                    sheet.Cells["D" + (i + 7)].PutValue(plan);
                    tatol = doclist.Aggregate(tatol, (current, t) => current + t.ManHours.GetValueOrDefault());
                    sheet.Cells["E" + (i + 7)].PutValue(tatol);

                    sheet.Cells["F" + (i + 7)].PutValue(doclist.Count);

                    var incomplete = doclist.Where(t => t.Complete == null || t.Complete < 100).ToList();
                    sheet.Cells["G" + (i + 7)].PutValue(incomplete.Count);

                    var Ovedur = doclist.Where(t => t.Deadline != null && t.Complete < 100 && t.Deadline.Value.Date < DateTime.Now.Date).ToList();
                    sheet.Cells["H" + (i + 7)].PutValue(Ovedur.Count);
                }

                sheet.Cells["A2"].PutValue(i);


                var filename = "Report_Manhours_Gip_" + project.Name.Replace("/", " ").Replace(@"\", " ") + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, "Report_Manhours_Gip_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls", false);
            }
        }
        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"~/Images/project.png";
        }

        protected void btnexportmhgip_Click(object sender, EventArgs e)
        {
            var projectlist = this.scopeProjectService.GetAll()
                                               .Where(t => (t.ProjectManagerId == UserSession.Current.User.Id) && ((t.StartDate != null
                                               && t.StartDate.Value.Date >= this.txtmhgipfromdate.SelectedDate.Value.Date
                                               && t.StartDate.Value.Date <= this.txtmhgipTodate.SelectedDate.Value.Date)
                                               || (t.Deadline != null
                                               && t.Deadline.Value.Date >= this.txtmhgipfromdate.SelectedDate.Value.Date
                                               && t.Deadline.Value.Date <= this.txtmhgipTodate.SelectedDate.Value.Date))).ToList();
            if (projectlist != null)
            {

                List<DocumentPackage> doclist = new List<DocumentPackage>();

                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\ReportManHour_PM.xls");
                var sheets = workbook.Worksheets;
                var sheet = sheets[0];
                int i = 0;
                var russianame = sheet.Cells["A6"].Value.ToString().Split(',').ToList();
                sheet.Cells["P4"].PutValue("From " + this.txtmhgipfromdate.SelectedDate.Value.ToString("dd/MM/yyyy") + " To " + this.txtmhgipTodate.SelectedDate.Value.ToString("dd/MM/yyyy"));
                sheet.Cells["A5"].PutValue("FALSE");
                foreach (var pj in projectlist)
                {
                    i++;
                    var wplist = this.workGroupService.GetAllWorkGroupOfProject(pj.ID).ToList();
                    doclist = this.documentPackageService.GetAllByWorkgroupInPermission(wplist.Select(t => t.ID).ToList()).ToList();
                    sheet.Cells["B" + (i + 6)].PutValue(i);
                    sheet.Cells["C" + (i + 6)].PutValue(pj.Name);
                    sheet.Cells["D" + (i + 6)].PutValue(pj.ProjectManagerFullName);
                    int j = 4;
                    var sum = 0.0;
                    foreach (var rolename in russianame)
                    {

                        Role department = this.roleService.GetAll(false).FirstOrDefault(t => t.RussiaName == rolename);
                        var wp = wplist.Where(t => t.DepartmentId == department.Id).Select(t => t.ID).ToList();
                        var doc = doclist.Where(t => wp.Contains(t.WorkgroupId.GetValueOrDefault())).ToList();
                        var manhour = 0.0;
                        manhour = doc.Aggregate(manhour, (current, t) => current + t.ManHours.GetValueOrDefault());
                        sheet.Cells[i + 5, j].PutValue(manhour);
                        sum += manhour;
                        j++;
                    }
                    sheet.Cells[i + 5, j].PutValue(sum);
                    if (!string.IsNullOrEmpty(pj.Notes))
                    {
                        sheet.Cells[i + 5, j + 1].PutValue(pj.Notes);
                        var range = sheet.Cells.CreateRange(i + 5, 2, i + 5, j + 1);
                        Style stl0 = workbook.Styles[workbook.Styles.Add()];
                        stl0.BackgroundColor = Color.Yellow;
                        //Set the solid background fillment.
                        stl0.Pattern = BackgroundType.Solid;
                        //Set a font.
                        StyleFlag flag = new StyleFlag();
                        //Apply cell shading.
                        flag.CellShading = true;
                        //Apply font.
                        flag.FontName = true;
                        //Apply font size.
                        flag.FontSize = true;
                        //Apply font color.
                        flag.FontColor = true;
                        //Apply bold font.
                        flag.FontBold = true;
                        //Apply italic attribute.
                        flag.FontItalic = true;


                        range.ApplyStyle(stl0, flag);

                    }


                }
                sheet.Cells["A2"].PutValue(i + 6);

                sheet.Cells.HideColumn((byte)3);

                var filename = "Report_Manhours_Gip_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, "Report_Manhours_Gip_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls", false);
            }
        }

        protected void btnincompletedocument_Click(object sender, EventArgs e)
        {
            if (this.txtdateto.SelectedDate == null || this.txtdatefrom.SelectedDate == null)
            {
                return;
            }

            var doclistincomplete = this.documentPackageService.GetAllDocListIncomplete().Where(t => (t.StartDate != null
                                               && t.StartDate.Value.Date >= this.txtdatefrom.SelectedDate.Value.Date
                                               && t.StartDate.Value.Date <= this.txtdateto.SelectedDate.Value.Date)
                                               || (t.Deadline != null
                                               && t.Deadline.Value.Date >= this.txtdatefrom.SelectedDate.Value.Date
                                               && t.Deadline.Value.Date <= this.txtdateto.SelectedDate.Value.Date));

            var wp = doclistincomplete.Select(t => t.WorkgroupId.GetValueOrDefault()).Distinct();
            var wplistobj = this.workGroupService.GetAll().Where(t => wp.Contains(t.ID)).ToList();

            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            var docList = new List<DocumentPackage>();

            var dtFull = new DataTable();

            dtFull.Columns.AddRange(new[]
                {
                        //new DataColumn("Flag", typeof(String)),
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("RevName", typeof(String)),
                        new DataColumn("Start", typeof(String)),
                        new DataColumn("DeadLine", typeof(String)),
                        new DataColumn("End", typeof(String)),
                        new DataColumn("Complete", typeof(Double)),
                        new DataColumn("Weight",typeof(Double)),
                        new DataColumn("DocumentTypeName", typeof(String)),
                        new DataColumn("ManhourPlan", typeof(String)),
                        new DataColumn("ManhourActual", typeof(String)),
                        new DataColumn("Department", typeof(String)),
                        new DataColumn("Engineer", typeof(String)),
                        new DataColumn("PersonsInvolved", typeof(String)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("OutgoingXDCBNo", typeof(String)),
                        new DataColumn("OutgoingXDCBDate", typeof(String)),
                        new DataColumn("IncomingNo", typeof(String)),
                        new DataColumn("IncomingDate", typeof(String)),
                        new DataColumn("ICANo", typeof(String)),
                        new DataColumn("ICADate", typeof(String)),
                        new DataColumn("ICAReviewCode", typeof(String)),
                        new DataColumn("Markup",typeof(String)),
                        new DataColumn("hardcopy", typeof(String)),
                        new DataColumn("Notes", typeof(String)),
                        //new DataColumn("LastestRevision", typeof(String)),
                       

                    });

            if (wplistobj.Count > 0)
            {

                //workbook.Open(Server.MapPath("~/"+templateManagement.FilePath));
                workbook.Open(filePath + @"Template/CMDRReport.xls");


                var sheets = workbook.Worksheets;
                var wsSummary = sheets[0];
                wsSummary.Cells.InsertRows(7, wplistobj.Count - 1);
                for (int i = 0; i < wplistobj.Count; i++)
                {
                    docList = doclistincomplete.Where(t => t.WorkgroupId == wplistobj[i].ID).OrderBy(t => t.DocNo).ToList();

                    var totalDoc = 0;
                    var totalDocIssues = 0;
                    var totalDocRev5Issues = 0;
                    var totalDocRevIssues = 0;

                    var totalDocDontIssues = 0;

                    dtFull.Rows.Clear();

                    sheets.AddCopy(1);
                    sheets[i + 2].Name = wplistobj[i].Name.Replace("/", " ").Replace(@"\", " ") + "_" + wplistobj[i].RevisionName;
                    sheets[i + 2].Cells["Z4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                    sheets[i + 2].Cells["C7"].PutValue(wplistobj[i].Name);
                    sheets[i + 2].Cells["E1"].PutValue("DETAIL ENGINEERING SERVICE FOR PROJECT " + wplistobj[i].ProjectFullName);

                    // add hyperlink
                    var linkName = wplistobj[i].Name;
                    wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                    wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1, "'" + wplistobj[i].Name.Replace("/", " ").Replace(@"\", " ") + "_" + wplistobj[i].RevisionName + "'" + "!D7");
                    if (wplistobj[i].DisciplineId != null)
                    {
                        var disciplineObj = this.disciplineService.GetById(wplistobj[i].DisciplineId.GetValueOrDefault());
                        wsSummary.Cells["C" + (7 + i)].PutValue(disciplineObj != null ? disciplineObj.Description : string.Empty);
                    }
                    wsSummary.Cells["C1"].PutValue("DETAIL  ENGINEERING SERVICE FOR All PROJECT");
                    wsSummary.Cells["C3"].PutValue(" PROJECT - SUMMARY REPORT");
                    wsSummary.Cells["O4"].PutValue("From: " + this.txtdatefrom.SelectedDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " To :" + this.txtdateto.SelectedDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                    wsSummary.Cells["O3"].PutValue(wplistobj.Count.ToString());


                    var docListHasAttachFile = docList.Where(t => t.HasAttachFile.GetValueOrDefault());
                    var wgDoc = docList.Count;
                    var wgDocIssues = docListHasAttachFile.Count();

                    var wgDocHardCopy = docListHasAttachFile.Count(t => !string.IsNullOrEmpty(t.OutgoingTransNo));
                    var wgTotalDocRev = wgDocIssues;
                    var wgDocDontIssues = wgDoc - wgDocIssues;

                    var total = docListHasAttachFile.Count(t => !string.IsNullOrEmpty(t.OutgoingLeterNo));

                    totalDoc += wgDoc;
                    totalDocIssues += wgDocIssues;

                    totalDocRev5Issues += wgDocHardCopy;
                    totalDocRevIssues += total;
                    totalDocDontIssues = totalDoc - totalDocIssues;

                    wsSummary.Cells["D" + (7 + i)].PutValue(wgDoc);
                    wsSummary.Cells["E" + (7 + i)].PutValue(wgDocDontIssues);
                    wsSummary.Cells["K" + (7 + i)].PutValue(wgTotalDocRev);
                    wsSummary.Cells["L" + (7 + i)].PutValue(totalDocRev5Issues);
                    wsSummary.Cells["M" + (7 + i)].PutValue(totalDocRevIssues);

                    var count = 1;

                    var listDocumentTypeId =
                        docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                    double complete = 0;
                    double weight = 0;

                    foreach (var documentTypeId in listDocumentTypeId)
                    {
                        var documentType = this.documentTypeService.GetById((int)documentTypeId);

                        var dataRow = dtFull.NewRow();
                        dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                        dtFull.Rows.Add(dataRow);

                        var listDocByDocType =
                            docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                        foreach (var document in listDocByDocType)
                        {
                            dataRow = dtFull.NewRow();
                            dataRow["DocId"] = document.ID;
                            dataRow["NoIndex"] = count;
                            dataRow["DocNo"] = document.DocNo;
                            dataRow["DocTitle"] = document.DocTitle;

                            dataRow["RevName"] = document.RevisionName;
                            dataRow["Start"] = document.StartDate != null
                              ? document.StartDate.Value.ToString("dd/MM/yyyy")
                              : string.Empty;
                            dataRow["DeadLine"] = document.Deadline != null
                              ? document.Deadline.Value.ToString("dd/MM/yyyy")
                              : string.Empty;
                            dataRow["End"] = document.EndDate != null
                              ? document.EndDate.Value.ToString("dd/MM/yyyy")
                              : string.Empty;
                            dataRow["Complete"] = document.Complete != null ? document.Complete / 100 : 0;
                            dataRow["Weight"] = document.Weight != null ? document.Weight / 100 : 0;

                            dataRow["DocumentTypeName"] = document.DocumentTypeName;
                            dataRow["ManhourPlan"] = document.ManHourPlan != null ? document.ManHourPlan.GetValueOrDefault() : 0;
                            dataRow["ManhourActual"] = document.ManHours != null ? document.ManHours.GetValueOrDefault() : 0;
                            dataRow["Department"] = document.DeparmentName;
                            dataRow["Engineer"] = document.EngineerName;
                            dataRow["PersonsInvolved"] = document.PersonsInvolved;
                            dataRow["OutgoingNo"] = document.OutgoingTransNo;
                            dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["OutgoingXDCBNo"] = document.OutgoingLeterNo;
                            dataRow["OutgoingXDCBDate"] = document.OutgoingLeterDate != null
                                ? document.OutgoingLeterDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["IncomingNo"] = document.IncomingTransNo;
                            dataRow["IncomingDate"] = document.IncomingTransDate != null
                                ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["ICANo"] = document.ICAReviewOutTransNo;
                            dataRow["ICADate"] = document.ICAReviewOutDate != null
                                ? document.ICAReviewOutDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["ICAReviewCode"] = document.ICAReviewCode;
                            dataRow["Markup"] = document.Markup;
                            dataRow["hardcopy"] = string.Empty;
                            dataRow["Notes"] = document.Notes;


                            count += 1;
                            dtFull.Rows.Add(dataRow);

                            complete += (document.Complete != null && document.Weight != null) ? (double)((document.Complete / 100) * (document.Weight / 100)) : 0;
                            weight += document.Weight != null ? (double)document.Weight / 100 : 0;
                        }
                    }

                    sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                    sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);

                    sheets[i + 2].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");

                    sheets[i + 2].Cells[7 + dtFull.Rows.Count, 9].PutValue(complete);
                    sheets[i + 2].Cells[7 + dtFull.Rows.Count, 10].PutValue(weight);

                }



                sheets[1].IsVisible = false;

                var filename = "All EMDR Report " +
                               DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                filename = filename.Replace("/", " ").Replace(@"\", " ");
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, filename, true);

            }
        }

        protected void rdbExportProjectNull_Click(object sender, EventArgs e)
        {

            var ProjectOfDocument = documentPackageService.GetAll().Select(t => t.ProjectId).Distinct().ToList();
            var projectlist = this.scopeProjectService.GetAll().Where(t => t.CreatdDate != null
                && this.txtToDateProject.SelectedDate != null
                && this.txtFromDateProject.SelectedDate != null
                && (this.txtFromDateProject.SelectedDate.Value.Date <= t.CreatdDate.Value && t.CreatdDate.Value.Date <= this.txtToDateProject.SelectedDate.Value)
                && !ProjectOfDocument.Contains(t.ID));


            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\Report_ProjectNullEMDR.xls");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var dtFull = new DataTable();

            dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("Field", typeof(String)),
                        new DataColumn("Platform", typeof(String)),
                        new DataColumn("Name", typeof(String)),
                        new DataColumn("Title", typeof(String)),
                        new DataColumn("Type", typeof(String)),
                        new DataColumn("Year", typeof(String)),
                        new DataColumn("ProjectManager", typeof(String)),
                        new DataColumn("Supervisor", typeof(String)),
                        new DataColumn("StartDate", typeof(String)),
                        new DataColumn("EndDate", typeof(String)),
                    });

            dataSheet.Cells["M1"].PutValue(this.txtFromDateProject.SelectedDate != null ? this.txtFromDateProject.SelectedDate.Value.ToString("dd/MM/yyyy") : "");
            dataSheet.Cells["M2"].PutValue(this.txtToDateProject.SelectedDate != null ? this.txtToDateProject.SelectedDate.Value.ToString("dd/MM/yyyy") : "");
            var count = 1;
            foreach (var obj in projectlist)
            {
                var dataRow = dtFull.NewRow();
                dataRow["NoIndex"] = count;
                dataRow["Field"] = obj.FieldName;
                dataRow["Platform"] = obj.PlatformName;
                dataRow["Name"] = obj.Name;
                dataRow["Title"] = obj.Description;
                dataRow["Type"] = obj.ProjectTypeName;
                dataRow["Year"] = obj.Year;
                dataRow["ProjectManager"] = obj.ProjectManagerFullName;
                dataRow["Supervisor"] = obj.SupervisorFullName;
                dataRow["StartDate"] = obj.StartDate != null ? obj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty;
                dataRow["EndDate"] = obj.EndDate != null ? obj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty;

                dtFull.Rows.Add(dataRow);
            }
            dataSheet.Cells.ImportDataTable(dtFull, false, 2, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
            dataSheet.Cells["A1"].PutValue(dtFull.Rows.Count);
            var filename = "Project_List_Null_EMDR_Report " +
                                         DateTime.Now.ToString("ddMMyyyy") + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, filename, true);
        }

        protected void btnExportUnplannedProject_Click(object sender, EventArgs e)
        {
            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\exportUnplannedProject.xls");
            var dataSheet = workbook.Worksheets[0];
            int count = 0;
            int stt = 0;

            //var projectUnplannedList = this.scopeProjectService.GetAll().Where(t => t.TypePlanID == 3 && ((t.StartDate != null
            //                        && t.StartDate.Value >= this.txtUnplannedProjectFromDate.SelectedDate.Value
            //                        && t.StartDate.Value <= this.txtUnplannedProjectToDate.SelectedDate.Value)
            //                        || (t.StartDate != null && t.EndDate != null
            //                        && t.StartDate.Value < this.txtUnplannedProjectFromDate.SelectedDate.Value
            //                        && t.EndDate.Value > this.txtUnplannedProjectToDate.SelectedDate.Value)
            //                        || (t.EndDate != null
            //                        && t.EndDate.Value >= this.txtUnplannedProjectFromDate.SelectedDate.Value
            //                        && t.EndDate.Value <= this.txtUnplannedProjectToDate.SelectedDate.Value)
            //                        || (t.StartDate != null && t.Deadline != null
            //                        && t.StartDate.Value < this.txtUnplannedProjectFromDate.SelectedDate.Value
            //                        && t.Deadline.Value > this.txtUnplannedProjectToDate.SelectedDate.Value)
            //                        || (t.Deadline != null
            //                        && t.Deadline.Value >= this.txtUnplannedProjectFromDate.SelectedDate.Value
            //                        && t.Deadline.Value <= this.txtUnplannedProjectToDate.SelectedDate.Value))).ToList();
            var projectAllList = this.scopeProjectService.GetAll().Where(t => t.ProjectTypeId == 2).ToList();
            var projectUnplannedEndNullList = projectAllList.Where(t => t.EndDate == null).ToList();
            var projectUnplannedEndList = projectAllList.Where(t => t.EndDate != null).ToList();


            var projectUnplannedList1 = projectUnplannedEndNullList.Where(t => (t.StartDate != null
                                    && t.StartDate.Value >= this.txtUnplannedProjectFromDate.SelectedDate.Value
                                    && t.StartDate.Value <= this.txtUnplannedProjectToDate.SelectedDate.Value)
                                    || (t.StartDate != null && t.Deadline != null
                                    && t.StartDate.Value < this.txtUnplannedProjectFromDate.SelectedDate.Value
                                    && t.Deadline.Value > this.txtUnplannedProjectToDate.SelectedDate.Value)
                                    || (t.Deadline != null
                                    && t.Deadline.Value >= this.txtUnplannedProjectFromDate.SelectedDate.Value
                                    && t.Deadline.Value <= this.txtUnplannedProjectToDate.SelectedDate.Value)).ToList();
            var projectUnplannedList2 = projectUnplannedEndList.Where(t => (t.StartDate != null
                                   && t.StartDate.Value >= this.txtUnplannedProjectFromDate.SelectedDate.Value
                                   && t.StartDate.Value <= this.txtUnplannedProjectToDate.SelectedDate.Value)
                                   || (t.StartDate != null && t.EndDate != null
                                   && t.StartDate.Value < this.txtUnplannedProjectFromDate.SelectedDate.Value
                                   && t.EndDate.Value > this.txtUnplannedProjectToDate.SelectedDate.Value)
                                   || (t.EndDate != null
                                   && t.EndDate.Value >= this.txtUnplannedProjectFromDate.SelectedDate.Value
                                   && t.EndDate.Value <= this.txtUnplannedProjectToDate.SelectedDate.Value)).ToList();
            var projectUnplannedList = projectUnplannedList1.Concat(projectUnplannedList2).ToList();

            for (int i = 0; i < projectUnplannedList.Count; i++)
            {
                stt++;
                count = stt + 7;
                dataSheet.Cells.InsertRow(count);
                dataSheet.Cells["A" + count].PutValue(stt);
                dataSheet.Cells["B" + count].PutValue(projectUnplannedList[i].planName);
                dataSheet.Cells["C" + count].PutValue(projectUnplannedList[i].Description);
                dataSheet.Cells["D" + count].PutValue(projectUnplannedList[i].Name);
                dataSheet.Cells["E" + count].PutValue(projectUnplannedList[i].StartDate.HasValue ? projectUnplannedList[i].StartDate.Value.ToString("dd/MM/yyyy") : null);
                dataSheet.Cells["F" + count].PutValue(projectUnplannedList[i].Deadline.HasValue ? projectUnplannedList[i].Deadline.Value.ToString("dd/MM/yyyy") : null);
                dataSheet.Cells["G" + count].PutValue(projectUnplannedList[i].Complete);
                dataSheet.Cells["I" + count].PutValue(projectUnplannedList[i].ProjectManagerFullName);
            }
            var filename = "Report_Permission " + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, filename, true);
        }

        protected void btnExportManhourProject_Click(object sender, EventArgs e)
        {
            var documentList = new List<DocumentPackage>();

            //string[] temlist = new string[listUserofDepartment.Count];

            var dtFull = new DataTable();
            var ListColumn = new[]
            {
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("Rev", typeof(String)),
                        new DataColumn("ManHourPlan", typeof(String)),
                        new DataColumn("Workpackage", typeof(String)),
                        new DataColumn("Note", typeof(String)),

            };
            dtFull.Columns.AddRange(ListColumn);

            var dataSum = new DataTable();
            var List = new[]
            {
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("Workpackage", typeof(String)),
                        new DataColumn("DocumentSum", typeof(String)),
                        new DataColumn("ManhourSum",typeof(String)),
                        new DataColumn("Note", typeof(String)),
            };
            dataSum.Columns.AddRange(List);

            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\ReportManhourProject.xls");
            var sheets = workbook.Worksheets;
            var sheetsummary = sheets[0];

            var sttSum = 1;
            var sttDoc = 1;
            var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(ddlPMProject.SelectedValue));
            if (projectObj != null)
            {
                sheetsummary.Cells["D1"].PutValue("BÁO CÁO CHI PHÍ NHÂN CÔNG \n DỰ ÁN: " + projectObj.Name);
                sheetsummary.Cells["G1"].PutValue(projectObj.Name);
                sheetsummary.Cells["G3"].PutValue(DateTime.Now.ToShortDateString());
                var listWPProject = this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(ddlPMProject.SelectedValue));
                var listDocProject = this.documentPackageService.GetAllDocProject(Convert.ToInt32(ddlPMProject.SelectedValue));
                foreach (var wpItem in listWPProject)
                {
                    var listDocWP = this.documentPackageService.GetAllByWorkgroup(wpItem.ID);
                    var dataRow = dataSum.NewRow();
                    dataRow["NoIndex"] = sttSum;
                    dataRow["Workpackage"] = wpItem.Name;
                    dataRow["DocumentSum"] = listDocWP.Count;
                    dataRow["ManhourSum"] = listDocWP.Sum(t => t.ManHourPlan);
                    dataSum.Rows.Add(dataRow);
                    sheets.AddCopy(1);
                    sheets[sttSum + 1].Cells["D1"].PutValue("CHI PHÍ NHÂN CÔNG TRỰC TIẾP \n DỰ ÁN: " + projectObj.Name + "\n NHÓM THỰC HIỆN: " + wpItem.DepartmentName);
                    sheets[sttSum + 1].Cells["H4"].PutValue(DateTime.Now.ToShortDateString());
                    sheets[sttSum + 1].Name = Utilities.Utility.RemoveSpecialCharacterForFolder(wpItem.Name);
                    sheets[sttSum + 1].Cells["I6"].PutValue("Summary");
                    sheets[sttSum + 1].Hyperlinks.Add("I6", 1, 1, "Summary!D1");
                    dtFull.Clear();
                    sttDoc = 1;
                    foreach (var docItem in listDocWP)
                    {
                        var dtRow = dtFull.NewRow();
                        dtRow["NoIndex"] = sttDoc;
                        dtRow["DocNo"] = docItem.DocNo;
                        dtRow["DocTitle"] = docItem.DocTitle;
                        dtRow["Rev"] = docItem.RevisionName;
                        dtRow["ManHourPlan"] = docItem.ManHourPlan;
                        dtRow["Workpackage"] = docItem.WorkgroupName;
                        dtFull.Rows.Add(dtRow);
                        sttDoc++;
                    }
                    sheets[sttSum + 1].Cells.ImportDataTable(dtFull, false, 6, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                    sttSum++;
                }

                sheetsummary.Cells.ImportDataTable(dataSum, false, 7, 1, dataSum.Rows.Count, dataSum.Columns.Count, true);
                sttSum = 8;

                sheetsummary.Cells["D" + (8 + listWPProject.Count)].PutValue(listDocProject.Count);
                var sumManhourI = listDocProject.Sum(t => t.ManHourPlan);
                var sumManhourII = sumManhourI * 5 / 100;
                var sumManhourIII = sumManhourI * 5 / 100;
                sheetsummary.Cells["E" + (8 + listWPProject.Count)].PutValue(sumManhourI);
                sheetsummary.Cells["E" + (9 + listWPProject.Count)].PutValue(sumManhourII);
                sheetsummary.Cells["E" + (10 + listWPProject.Count)].PutValue(sumManhourIII);
                sheetsummary.Cells["E" + (11 + listWPProject.Count)].PutValue(sumManhourI + sumManhourII + sumManhourIII);
                foreach (var wpItem in listWPProject)
                {
                    sheetsummary.Hyperlinks.Add("C" + (sttSum), 1, 1, "'" + wpItem.Name + "'" + "!D1");
                    sttSum++;
                }
                sheets[1].IsVisible = false;

                var filename = "Report_Manhour_Project - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, "Report_Manhour_Project - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls", false);
            }
        }

        protected void LoadComboData()
        {
            var workList = this.workListService.GetByYear(DateTime.Now.Year).Where(t => t.ParentID == null).ToList();
            workList.Add(new Work() { ID = 20, Name = "All" });
            this.ddlWorkList.DataSource = workList;
            this.ddlWorkList.DataTextField = "Name";
            this.ddlWorkList.DataValueField = "ID";
            this.ddlWorkList.DataBind();
        }

        protected void btnProgressReport_Click(object sender, EventArgs e)
        {
            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            var filename = "";
            int typetemp = 0;
            switch (ddlWorkList.SelectedValue)
            {
                case "1":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2019_Format_PL17.1.xls");
                        filename = "Monthly_Report_Planned_2019_PL17.1";
                        typetemp = 5;
                        break;
                    }
                case "2":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2019_Format_PL17_2.xls");
                        filename = "Monthly_Report_Planned_2019_PL17.2";
                        typetemp = 1;
                        break;
                    }
                case "3":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2019_Format_PL7.xls");
                        filename = "Monthly report_Planned_2019_Format_PL7";
                        typetemp = 1;
                        break;
                    }
                case "4":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2019_Format_PL16.xls");
                        filename = "Monthly report_Planned_2019_Format_PL16";
                        typetemp = 1;
                        break;
                    }
                case "5":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2019_Format_PL18.xls");
                        filename = "Monthly report_Planned_2019_Format_PL18";
                        typetemp = 1;
                        break;
                    }
                case "32":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2019_Other_Blocks.xls");
                        filename = "Monthly report_Planned_2019_Other_Blocks";
                        typetemp = 2;
                        break;
                    }
                case "12":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2019_Other_Works.xls");
                        filename = "Monthly report_Planned_2019_Other_Works";
                        typetemp = 3;
                        break;
                    }
                case "28":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Unplanned_2019.xls");
                        filename = "Monthly report_Unplanned_2019";
                        typetemp = 4;
                        break;
                    }
                case "30":
                    {
                        workbook.Open(filePath + @"Template\Monthly_Report_Planned_2019_DMSB_XDCB.xls");
                        filename = "Monthly_Report_Planned_2019_DMSB_XDCB";
                        typetemp = 6;
                        break;
                    }
                case "33":
                    {
                        workbook.Open(filePath + @"Template\Monthly_Report_Planned_2020_PL17.1.xls");
                        filename = "Monthly_Report_Planned_2020_PL17.1";
                        typetemp = 5;
                        break;
                    }
                case "34":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2020_PL17.2.xls");
                        filename = "Monthly report_Planned_2020_PL17.2";
                        typetemp = 1;
                        break;
                    }
                case "35":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2020_Other_Blocks.xls");
                        filename = "Monthly report_Planned_2020_Other_Blocks";
                        typetemp = 2;
                        break;
                    }
                case "45":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2020_Other_Works.xls");
                        filename = "Monthly report_Planned_2020_Other_Works";
                        typetemp = 3;
                        break;
                    }
                case "46":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Service_2020.xls");
                        filename = "Monthly report_Service_2020";
                        typetemp = 1;
                        break;
                    }
                case "49":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2020_PL16.xls");
                        filename = "Monthly report_Planned_2020_PL16";
                        typetemp = 1;
                        break;
                    }
                case "50":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2020_PL7.xls");
                        filename = "Monthly report_Planned_2020_PL7";
                        typetemp = 1;
                        break;
                    }
                case "51":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Unplanned_2020.xls");
                        filename = "Monthly report_Unplanned_2020";
                        typetemp = 4;
                        break;
                    }
                default:
                    break;
            }
            var sheets = workbook.Worksheets;
            var sheet = sheets[0];
            int count = 0;
            int firstrow = 15;
            int rowindex = 15;
            if (typetemp == 1)
            {
                // Create a datatable
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(14, 1, sheet.Cells.MaxDataRow, 2);
                sheet.Cells["J10"].PutValue(DateTime.Now.ToShortDateString());
                var sttMulti = 0;
                var workList = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (!string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        var appendixObj = appendixListService.GetByCodeType(item["Column1"].ToString(), workList);
                        if (appendixObj != null)
                        {
                            if (!appendixObj.MultipleProject.GetValueOrDefault())
                            {
                                var projectAppendixObj = projectAppendixService.GetByAppendix(appendixObj.ID, workList, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                if (projectAppendixObj != null)
                                {
                                    var projectObj = scopeProjectService.GetById(projectAppendixObj.ProjectID.GetValueOrDefault());
                                    if (projectObj != null)
                                    {
                                        var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                        sheet.Cells["I" + rowindex].PutValue(projectAppendixObj.Basis);
                                        sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                        sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                        sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                        sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["O" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                        sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                        sheet.Cells["Q" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                        sheet.Cells["R" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                        sheet.Cells["S" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                        sheet.Cells["T" + rowindex].PutValue(projectObj.DesignCost);
                                        sheet.Cells["U" + rowindex].PutValue(projectObj.Notes);
                                    }
                                }
                            }
                            else
                            {
                                sttMulti = 0;
                                var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workList, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                if (projectAppendixObj.Count != 0)
                                {
                                    foreach (var projectAppendixItem in projectAppendixObj)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            sheet.Cells.InsertRow(rowindex);
                                            count++;
                                            rowindex = firstrow + count;
                                            sttMulti++;
                                            var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                            sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                            sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["O" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                            sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                            sheet.Cells["Q" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["R" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                            sheet.Cells["S" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.DesignCost);
                                            sheet.Cells["U" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    count++;
                }
            }
            else if (typetemp == 2)
            {
                int index = 15;
                int prefix = 0;
                int sttMulti = 0;
                int flag = 0;
                int countTemp = 0;
                // Create a datatable
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(14, 0, sheet.Cells.MaxDataRow, 2);
                sheet.Cells["J10"].PutValue(DateTime.Now.ToShortDateString());
                var workListPlan = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                var workListUnplan = workListService.GetByTypeAndYear(2, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).Select(t => t.ID).ToList();
                var projectAppendixList = projectAppendixService.GetByWork(workListUnplan, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).OrderBy(t => t.WorkID).ToList();
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        if (!string.IsNullOrEmpty(item["Column2"].ToString()))
                        {
                            var appendixObj = appendixListService.GetByCodeType(item["Column2"].ToString(), workListPlan);
                            if (appendixObj != null)
                            {
                                if (!appendixObj.MultipleProject.GetValueOrDefault())
                                {
                                    var projectAppendixObj = projectAppendixService.GetByAppendix(appendixObj.ID, workListPlan, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                    if (projectAppendixObj != null)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixObj.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixObj.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["O" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                            sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                            sheet.Cells["Q" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["R" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                            sheet.Cells["S" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.DesignCost);
                                            sheet.Cells["U" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                                else
                                {
                                    sttMulti = 0;
                                    var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workListPlan, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                    if (projectAppendixObj.Count != 0)
                                    {
                                        foreach (var projectAppendixItem in projectAppendixObj)
                                        {
                                            var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                            if (projectObj != null)
                                            {
                                                sheet.Cells.InsertRow(rowindex);
                                                count++;
                                                rowindex = firstrow + count;
                                                sttMulti++;
                                                var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                                sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                                sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                                sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                                sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                                sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                                sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                                sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                                sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                                sheet.Cells["O" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                                sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                                sheet.Cells["Q" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                                sheet.Cells["R" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                                sheet.Cells["S" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                                sheet.Cells["T" + rowindex].PutValue(projectObj.DesignCost);
                                                sheet.Cells["U" + rowindex].PutValue(projectObj.Notes);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //var projectAppendixList = projectAppendixService.GetByWork(workListUnplan, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).OrderBy(t => t.WorkID).ToList();
                        if (projectAppendixList.Count != 0)
                        {
                            if (!string.IsNullOrEmpty(item["Column1"].ToString()))
                            {
                                var projectAppendixByWork = projectAppendixList.Where(t => t.WorkID == Convert.ToInt32(item["Column1"].ToString())).ToList();

                                prefix++;
                                countTemp = count;
                                foreach (var projectAppendixItem in projectAppendixByWork)
                                {
                                    rowindex = index + 1 + count;
                                    if (prefix != flag)
                                    {
                                        flag = prefix;
                                        sttMulti = 1;
                                    }
                                    else
                                    {
                                        sttMulti++;
                                    }
                                    var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                    if (projectObj != null)
                                    {
                                        var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                        sheet.Cells.InsertRow(rowindex);
                                        count++;
                                        sheet.Cells["B" + rowindex].PutValue(prefix + "." + sttMulti);
                                        sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                        sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                        sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                        sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                        sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                        sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["O" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                        sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                        sheet.Cells["Q" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                        sheet.Cells["R" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                        sheet.Cells["S" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                        sheet.Cells["T" + rowindex].PutValue(projectObj.DesignCost);
                                        sheet.Cells["U" + rowindex].PutValue(projectObj.Notes);
                                    }
                                }
                            }
                            //index++;
                        }
                    }
                    count++;
                }
            }
            else if (typetemp == 3)
            {
                rowindex = 16;
                firstrow = 16;
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(14, 1, sheet.Cells.MaxDataRow, 2);
                sheet.Cells["J10"].PutValue(DateTime.Now.ToShortDateString());
                var workList = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                var sttMulti = 0;
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (!string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        var appendixObj = appendixListService.GetByCodeType(item["Column1"].ToString(), workList);
                        if (appendixObj != null)
                        {
                            if (!appendixObj.MultipleProject.GetValueOrDefault())
                            {
                                var projectAppendixObj = projectAppendixService.GetByWork(Convert.ToInt32(ddlWorkList.SelectedValue), Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                if (projectAppendixObj.Count != 0)
                                {
                                    foreach (var projectAppendixItem in projectAppendixObj)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["O" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                            sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                            sheet.Cells["Q" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["R" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                            sheet.Cells["S" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.DesignCost);
                                            sheet.Cells["U" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                sttMulti = 0;
                                var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workList, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                if (projectAppendixObj.Count != 0)
                                {
                                    foreach (var projectAppendixItem in projectAppendixObj)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            sheet.Cells.InsertRow(rowindex);
                                            rowindex = firstrow + count;
                                            sttMulti++;
                                            var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                            sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                            sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["O" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                            sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                            sheet.Cells["Q" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["R" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                            sheet.Cells["S" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.DesignCost);
                                            sheet.Cells["U" + rowindex].PutValue(projectObj.Notes);
                                            count++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    count++;
                }
            }
            else if (typetemp == 4)
            {
                int prefix = 0;
                int sttMulti = 0;
                int flag = 0;
                sheet.Cells["G10"].PutValue(DateTime.Now.ToShortDateString());
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(14, 0, sheet.Cells.MaxDataRow, 2);

                var workList = workListService.GetByTypeAndYear(2, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).Select(t => t.ID).ToList();
                //var projectAppendixList = projectAppendixService.GetByWork(workList, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).OrderBy(t => t.WorkID).ToList();

                var workListUnplan = workListService.GetByTypeAndYear(2, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).Select(t => t.ID).ToList();
                var projectAppendixList = projectAppendixService.GetByWork(workListUnplan, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).OrderBy(t => t.WorkID).ToList();
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        if (!string.IsNullOrEmpty(item["Column2"].ToString()))
                        {
                            var appendixObj = appendixListService.GetByCodeType(item["Column2"].ToString(), workListUnplan);
                            if (appendixObj != null)
                            {
                                if (!appendixObj.MultipleProject.GetValueOrDefault())
                                {
                                    var projectAppendixObj = projectAppendixService.GetByAppendix(appendixObj.ID, workListUnplan, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                    if (projectAppendixObj != null)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixObj.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixObj.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["O" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                            sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                            sheet.Cells["Q" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["R" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                            sheet.Cells["S" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.DesignCost);
                                            sheet.Cells["U" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                                else
                                {
                                    sttMulti = 0;
                                    var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workListUnplan, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                    if (projectAppendixObj.Count != 0)
                                    {
                                        foreach (var projectAppendixItem in projectAppendixObj)
                                        {
                                            
                                            var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                            if (projectObj != null)
                                            {
                                                sheet.Cells.InsertRow(rowindex);
                                                count++;
                                                rowindex = firstrow + count;
                                                sttMulti++;
                                                var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                                sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                                sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                                sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                                sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                                sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                                sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                                sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                                sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                                sheet.Cells["O" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                                sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                                sheet.Cells["Q" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                                sheet.Cells["R" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                                sheet.Cells["S" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                                sheet.Cells["T" + rowindex].PutValue(projectObj.DesignCost);
                                                sheet.Cells["U" + rowindex].PutValue(projectObj.Notes);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //var projectAppendixList = projectAppendixService.GetByWork(workListUnplan, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).OrderBy(t => t.WorkID).ToList();
                        if (projectAppendixList.Count != 0)
                        {
                            if (!string.IsNullOrEmpty(item["Column1"].ToString()))
                            {
                                var projectAppendixByWork = projectAppendixList.Where(t => t.WorkID == Convert.ToInt32(item["Column1"].ToString())).ToList();

                                prefix++;
                                foreach (var projectAppendixItem in projectAppendixByWork)
                                {
                                    rowindex = firstrow + count + 1;
                                    if (prefix != flag)
                                    {
                                        flag = prefix;
                                        sttMulti = 1;
                                    }
                                    else
                                    {
                                        sttMulti++;
                                    }
                                    var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                    if (projectObj != null)
                                    {
                                        var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                        sheet.Cells.InsertRow(rowindex);
                                        count++;
                                        sheet.Cells["B" + rowindex].PutValue(prefix + "." + sttMulti);
                                        sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                        sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                        sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                        sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                        sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                        sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["O" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                        sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                        sheet.Cells["Q" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                        sheet.Cells["R" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                        sheet.Cells["S" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                        sheet.Cells["T" + rowindex].PutValue(projectObj.DesignCost);
                                        sheet.Cells["U" + rowindex].PutValue(projectObj.Notes);
                                    }
                                }
                            }
                        }
                        //sheet.Cells.DeleteRow(rowindex);
                    }
                    count++;
                }
            }
            else if (typetemp == 5)
            {
                // Create a datatable
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(14, 1, sheet.Cells.MaxDataRow, 2);
                sheet.Cells["J10"].PutValue(DateTime.Now.ToShortDateString());
                var sttMulti = 0;
                var workList = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (!string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        var appendixObj = appendixListService.GetByCodeType(item["Column1"].ToString(), workList);
                        if (appendixObj != null)
                        {
                            if (!appendixObj.MultipleProject.GetValueOrDefault())
                            {
                                var projectAppendixObj = projectAppendixService.GetByAppendix(appendixObj.ID, workList, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                if (projectAppendixObj != null)
                                {
                                    var projectObj = scopeProjectService.GetById(projectAppendixObj.ProjectID.GetValueOrDefault());
                                    if (projectObj != null)
                                    {
                                        double totalManhour = 0;
                                        double completeRQSM = 0;
                                        var docRQSMList = this.documentPackageService.GetAllByRQSM(projectObj.ID);
                                        if (docRQSMList.Count > 0)
                                        {
                                            var deadlineRQSM = docRQSMList.Max(t => t.Deadline.GetValueOrDefault());
                                            totalManhour = docRQSMList.Sum(t => t.ManHourPlan.GetValueOrDefault());

                                            foreach (var item1 in docRQSMList)
                                            {
                                                completeRQSM += (item1.Complete.GetValueOrDefault() * (item1.ManHourPlan.GetValueOrDefault() / totalManhour));
                                            }
                                            sheet.Cells["O" + rowindex].PutValue(deadlineRQSM.ToString("dd/MM/yyyy"));
                                            sheet.Cells["S" + rowindex].PutValue(Math.Round(completeRQSM, 2).ToString());
                                        }
                                        double completeWithoutPOC = 0;
                                        var docWithoutPOC = this.documentPackageService.GetAllByWithoutPOC(projectObj.ID);
                                        if (docWithoutPOC.Count > 0)
                                        {
                                            var deadlineWithoutPOC = docWithoutPOC.Max(t => t.Deadline.GetValueOrDefault());
                                            totalManhour = docWithoutPOC.Sum(t => t.ManHourPlan.GetValueOrDefault());

                                            foreach (var item1 in docWithoutPOC)
                                            {
                                                completeWithoutPOC += (item1.Complete.GetValueOrDefault() * (item1.ManHourPlan.GetValueOrDefault() / totalManhour));
                                            }
                                            sheet.Cells["P" + rowindex].PutValue(deadlineWithoutPOC.ToString("dd/MM/yyyy"));
                                            sheet.Cells["T" + rowindex].PutValue(Math.Round(completeWithoutPOC, 2).ToString());
                                        }
                                        double completeWithPOC = 0;
                                        var docWithPOC = this.documentPackageService.GetAllByWithPOC(projectObj.ID);
                                        if (docWithPOC.Count > 0)
                                        {
                                            var deadlineWithPOC = docWithPOC.Max(t => t.Deadline.GetValueOrDefault());
                                            totalManhour = docWithPOC.Sum(t => t.ManHourPlan.GetValueOrDefault());

                                            foreach (var item1 in docWithPOC)
                                            {
                                                completeWithPOC += (item1.Complete.GetValueOrDefault() * (item1.ManHourPlan.GetValueOrDefault() / totalManhour));
                                            }
                                            sheet.Cells["Q" + rowindex].PutValue(deadlineWithPOC.ToString("dd/MM/yyyy"));
                                            sheet.Cells["U" + rowindex].PutValue(Math.Round(completeWithPOC, 2).ToString());
                                        }
                                        //var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                        sheet.Cells["I" + rowindex].PutValue(projectAppendixObj.Basis);
                                        sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                        sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                        sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                        sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["R" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                        //sheet.Cells["U" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                        sheet.Cells["V" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                        sheet.Cells["W" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                        sheet.Cells["X" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                        sheet.Cells["Y" + rowindex].PutValue(projectObj.DesignCost);
                                        sheet.Cells["Z" + rowindex].PutValue(projectObj.Notes);
                                    }
                                }
                            }
                            else
                            {
                                sttMulti = 0;
                                var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workList, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                if (projectAppendixObj.Count != 0)
                                {
                                    foreach (var projectAppendixItem in projectAppendixObj)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            sheet.Cells.InsertRow(rowindex);
                                            count++;
                                            rowindex = firstrow + count;
                                            sttMulti++;
                                            double totalManhour = 0;
                                            double completeRQSM = 0;
                                            var docRQSMList = this.documentPackageService.GetAllByRQSM(projectObj.ID);
                                            if (docRQSMList.Count > 0)
                                            {
                                                var deadlineRQSM = docRQSMList.Max(t => t.Deadline.GetValueOrDefault());
                                                totalManhour = docRQSMList.Sum(t => t.ManHourPlan.GetValueOrDefault());

                                                foreach (var item1 in docRQSMList)
                                                {
                                                    completeRQSM += (item1.Complete.GetValueOrDefault() * (item1.ManHourPlan.GetValueOrDefault() / totalManhour));
                                                }
                                                sheet.Cells["O" + rowindex].PutValue(deadlineRQSM.ToString("dd/MM/yyyy"));
                                                sheet.Cells["S" + rowindex].PutValue(Math.Round(completeRQSM, 2).ToString());
                                            }
                                            double completeWithoutPOC = 0;
                                            var docWithoutPOC = this.documentPackageService.GetAllByWithoutPOC(projectObj.ID);
                                            if (docWithoutPOC.Count > 0)
                                            {
                                                var deadlineWithoutPOC = docWithoutPOC.Max(t => t.Deadline.GetValueOrDefault());
                                                totalManhour = docWithoutPOC.Sum(t => t.ManHourPlan.GetValueOrDefault());

                                                foreach (var item1 in docWithoutPOC)
                                                {
                                                    completeWithoutPOC += (item1.Complete.GetValueOrDefault() * (item1.ManHourPlan.GetValueOrDefault() / totalManhour));
                                                }
                                                sheet.Cells["P" + rowindex].PutValue(deadlineWithoutPOC.ToString("dd/MM/yyyy"));
                                                sheet.Cells["T" + rowindex].PutValue(Math.Round(completeWithoutPOC, 2).ToString());
                                            }
                                            double completeWithPOC = 0;
                                            var docWithPOC = this.documentPackageService.GetAllByWithPOC(projectObj.ID);
                                            if (docWithPOC.Count > 0)
                                            {
                                                var deadlineWithPOC = docWithPOC.Max(t => t.Deadline.GetValueOrDefault());
                                                totalManhour = docWithPOC.Sum(t => t.ManHourPlan.GetValueOrDefault());

                                                foreach (var item1 in docWithPOC)
                                                {
                                                    completeWithPOC += (item1.Complete.GetValueOrDefault() * (item1.ManHourPlan.GetValueOrDefault() / totalManhour));
                                                }
                                                sheet.Cells["Q" + rowindex].PutValue(deadlineWithPOC.ToString("dd/MM/yyyy"));
                                                sheet.Cells["U" + rowindex].PutValue(Math.Round(completeWithPOC, 2).ToString());
                                            }
                                            //var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                            sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                            sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["R" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                            //sheet.Cells["U" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                            sheet.Cells["V" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["W" + rowindex].PutValue(projectObj.TotalCostEstimate);
                                            sheet.Cells["X" + rowindex].PutValue(projectObj.ProjectPlanningCost);
                                            sheet.Cells["Y" + rowindex].PutValue(projectObj.DesignCost);
                                            sheet.Cells["Z" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    count++;
                }
            }
            else if (typetemp == 6)
            {
                firstrow = 7;
                rowindex = 7;
                // Create a datatable
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(8, 1, sheet.Cells.MaxDataRow, 2);
                sheet.Cells["G4"].PutValue(DateTime.Now.ToShortDateString());
                var sttMulti = 0;
                var workList = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (!string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        var appendixObj = appendixListService.GetByCodeType(item["Column1"].ToString(), workList);
                        if (appendixObj != null)
                        {
                            if (!appendixObj.MultipleProject.GetValueOrDefault())
                            {
                                var projectAppendixObj = projectAppendixService.GetByAppendix(appendixObj.ID, workList, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                if (projectAppendixObj != null)
                                {
                                    var projectObj = scopeProjectService.GetById(projectAppendixObj.ProjectID.GetValueOrDefault());
                                    if (projectObj != null)
                                    {
                                        //var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                        sheet.Cells["L" + rowindex].PutValue(projectAppendixObj.Basis);
                                        sheet.Cells["M" + rowindex].PutValue(projectObj.SupervisorFullName);
                                        sheet.Cells["N" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                        sheet.Cells["O" + rowindex].PutValue(projectObj.Name);
                                        sheet.Cells["P" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["Q" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["R" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                        //sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                        sheet.Cells["S" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                        sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                    }
                                }
                            }
                            else
                            {
                                sttMulti = 0;
                                var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workList, Convert.ToInt32(ddlYearProgressReport.SelectedValue));
                                if (projectAppendixObj.Count != 0)
                                {
                                    foreach (var projectAppendixItem in projectAppendixObj)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            sheet.Cells.InsertRow(rowindex);
                                            count++;
                                            rowindex = firstrow + count;
                                            sttMulti++;
                                            //var WPCost = this.workGroupService.GetWorkGroupCostOfProject(projectObj.ID);
                                            sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                            sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                            sheet.Cells["L" + rowindex].PutValue(projectAppendixItem.Basis);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["O" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["P" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["Q" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["R" + rowindex].PutValue(Math.Round(projectObj.Complete.GetValueOrDefault(), 2).ToString());
                                            //sheet.Cells["P" + rowindex].PutValue(WPCost != null ? WPCost.Complete : 0);
                                            sheet.Cells["S" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    count++;
                }
            }

            filename = filename + "_" + DateTime.Now.Year + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, filename, false);
        }

        private void UpdateWorkgroupManhourMonthBeforeReport(int month, int year)
        {
            DateTime date = new DateTime(year, month, 1);
            int prevMonth = date.AddMonths(-1).Month;
            int prevYear = date.AddMonths(-1).Year;
            var wmmListInMonth = this.workgroupManhourMonthService.GetByMonthAndYear(month, year);
            foreach (var wmmObj in wmmListInMonth)
            {
                if (DateTime.Now.Month == month)
                {
                    var wpObj = this.workGroupService.GetById(wmmObj.WorkgroupID.GetValueOrDefault());
                    if (wpObj != null)
                    {
                        var prevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpObj.ID, month, year);
                        var docList = this.documentPackageService.GetAllByWorkgroup(wpObj.ID);
                        var manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                        wmmObj.WorkgroupWeight = wpObj.Weight;
                        wmmObj.ManhourPlanTotal = manhourPlanTotal;
                        wmmObj.ManhourPlanMonth = (wmmObj.ManhourPlanTotal.GetValueOrDefault() * wmmObj.CompletePlanMonth.GetValueOrDefault()) / 100;
                        wmmObj.CompleteActualTotal = wpObj.Complete.GetValueOrDefault();
                        wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                        if (prevWMMObj != null)
                        {
                            wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                            wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                        }
                        else
                        {
                            wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                            wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                        }

                        wmmObj.UpdateBy = UserSession.Current.User.Id;
                        wmmObj.UpdateDate = DateTime.Now;
                        this.workgroupManhourMonthService.Update(wmmObj);
                    }
                }
            }
        }

        /// <summary>
        /// Xuat report manhour monthly report cho lanh dao vien
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnMonthlyManhourReport_Click(object sender, EventArgs e)
        {
            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            var filename = "";
            int typetemp = 0;
            switch (ddlWorkList.SelectedValue)
            {
                case "1":
                    {
                        workbook.Open(filePath + @"Template\Manhour_Monthly report_Planned_2019_Format_PL17.1.xls");
                        filename = "Manhour_Monthly_Report_Planned_2019_PL17.1";
                        typetemp = 1;
                        break;
                    }
                case "2":
                    {
                        workbook.Open(filePath + @"Template\Manhour_Monthly report_Planned_2019_Format_PL17_2.xls");
                        filename = "Manhour_Monthly_Report_Planned_2019_PL17.2";
                        typetemp = 1;
                        break;
                    }
                case "3":
                    {
                        workbook.Open(filePath + @"Template\Manhour_Monthly report_Planned_2019_Format_PL7.xls");
                        filename = "Manhour_Monthly report_Planned_2019_Format_PL7";
                        typetemp = 1;
                        break;
                    }
                case "4":
                    {
                        workbook.Open(filePath + @"Template\Manhour_Monthly report_Planned_2019_Format_PL16.xls");
                        filename = "Manhour_Monthly report_Planned_2019_Format_PL16";
                        typetemp = 1;
                        break;
                    }
                case "5":
                    {
                        workbook.Open(filePath + @"Template\Manhour_Monthly report_Planned_2019_Format_PL18.xls");
                        filename = "Manhour_Monthly report_Planned_2019_Format_PL18";
                        typetemp = 1;
                        break;
                    }
                case "32":
                    {
                        workbook.Open(filePath + @"Template\Manhour_Monthly report_Planned_2019_Other_Blocks.xls");
                        filename = "Manhour_Monthly report_Planned_2019_Other_Blocks";
                        typetemp = 2;
                        break;
                    }
                case "12":
                    {
                        workbook.Open(filePath + @"Template\Manhour_Monthly report_Planned_2019_Other_Works.xls");
                        filename = "Manhour_Monthly report_Planned_2019_Other_Works";
                        typetemp = 3;
                        break;
                    }
                case "28":
                    {
                        workbook.Open(filePath + @"Template\Manhour_Monthly report_Unplanned_2019.xls");
                        filename = "Manhour_Monthly report_Unplanned_2019";
                        typetemp = 4;
                        break;
                    }
                case "30":
                    {
                        workbook.Open(filePath + @"Template\Manhour_Monthly_Report_Planned_2019_DMSB_XDCB.xls");
                        filename = "Manhour_Monthly_Report_Planned_2019_DMSB_XDCB";
                        typetemp = 6;
                        break;
                    }
                case "33":
                    {
                        workbook.Open(filePath + @"Template\Manhour report_Planned_2020_PL17.1.xls");
                        filename = "Manhour report_Planned_2020_PL17.1";
                        typetemp = 1;
                        break;
                    }
                case "34":
                    {
                        workbook.Open(filePath + @"Template\Manhour report_Planned_2020_PL17.2.xls");
                        filename = "Manhour report_Planned_2020_PL17.2";
                        typetemp = 1;
                        break;
                    }
                case "35":
                    {
                        workbook.Open(filePath + @"Template\Manhour report_Planned_2020_Other_Blocks.xls");
                        filename = "Manhour report_Planned_2020_Other_Blocks";
                        typetemp = 2;
                        break;
                    }
                case "45":
                    {
                        workbook.Open(filePath + @"Template\Monthly report_Planned_2020_Other_Works.xls");
                        filename = "Manhour report_Planned_2020_Other_Works";
                        typetemp = 3;
                        break;
                    }
                case "46":
                    {
                        workbook.Open(filePath + @"Template\Manhour report_Service_2020.xls");
                        filename = "Manhour report_Service_2020";
                        typetemp = 1;
                        break;
                    }
                case "49":
                    {
                        workbook.Open(filePath + @"Template\Manhour report_Planned_2020_PL16.xls");
                        filename = "Manhour report_Planned_2020_PL16";
                        typetemp = 1;
                        break;
                    }
                case "50":
                    {
                        workbook.Open(filePath + @"Template\Manhour report_Planned_2020_PL7.xls");
                        filename = "Manhour report_Planned_2020_PL7";
                        typetemp = 1;
                        break;
                    }
                case "51":
                    {
                        workbook.Open(filePath + @"Template\Manhour report_Unplanned_2020.xls");
                        filename = "Manhour report_Unplanned_2020";
                        typetemp = 4;
                        break;
                    }
                default:
                    break;
            }
            int month = Convert.ToInt32(ddlMonthManhourReport.SelectedValue);
            int year = Convert.ToInt32(ddlYearManhourReport.SelectedValue);
            DateTime date = new DateTime(year, month, 1);
            var sheets = workbook.Worksheets;
            var sheet = sheets[0];
            sheet.Name = date.ToString("MM-yyyy");
            int count = 0;
            int firstrow = 15;
            int rowindex = 15;

            //update
            UpdateWorkgroupManhourMonthBeforeReport(month, year);
            if (typetemp == 1)
            {
                // Create a datatable
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(14, 1, sheet.Cells.MaxDataRow, 2);
                sheet.Cells["J10"].PutValue(ddlMonthManhourReport.SelectedValue + "-" + ddlYearManhourReport.SelectedValue);
                var sttMulti = 0;
                var workList = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (!string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        var appendixObj = appendixListService.GetByCodeType(item["Column1"].ToString(), workList);
                        if (appendixObj != null)
                        {
                            if (!appendixObj.MultipleProject.GetValueOrDefault())
                            {
                                var projectAppendixObj = projectAppendixService.GetByAppendix(appendixObj.ID, workList, year);
                                if (projectAppendixObj != null)
                                {
                                    var projectObj = scopeProjectService.GetById(projectAppendixObj.ProjectID.GetValueOrDefault());
                                    if (projectObj != null)
                                    {
                                        var wmmList = new List<WorkgroupManhourMonth>();
                                        if (date > projectObj.Deadline)
                                        {
                                            wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                        }
                                        else
                                        {
                                            wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                        }

                                        double complete = 0.0;
                                        foreach (var wmmItem in wmmList)
                                        {
                                            var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                            complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                        }
                                        //manhour cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                        var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                        sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                        var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                        double sumManhourActualMonth = 0;
                                        sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);

                                        sheet.Cells["I" + rowindex].PutValue(projectAppendixObj.Basis);
                                        sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                        sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                        sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                        sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                        sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                        sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                        sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                        sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                        sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                    }
                                }
                            }
                            else
                            {
                                sttMulti = 0;
                                var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workList, year);
                                if (projectAppendixObj.Count != 0)
                                {
                                    foreach (var projectAppendixItem in projectAppendixObj)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            sheet.Cells.InsertRow(rowindex);
                                            count++;
                                            rowindex = firstrow + count;
                                            sttMulti++;
                                            var wmmList = new List<WorkgroupManhourMonth>();
                                            if (date > projectObj.Deadline)
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                            }
                                            else
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                            }
                                            double complete = 0.0;
                                            foreach (var wmmItem in wmmList)
                                            {
                                                var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                                complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                            }
                                            var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                            //manhour plan cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                            sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                            var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                            double sumManhourActualMonth = 0;
                                            sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);

                                            sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                            sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                            sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                            sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                            sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    count++;
                }
            }
            else if (typetemp == 2)
            {
                firstrow = 16;
                int index = 16;
                int prefix = 0;
                int sttMulti = 0;
                int flag = 0;
                int countTemp = 0;
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(15, 0, sheet.Cells.MaxDataRow, 2);
                sheet.Cells["J10"].PutValue(ddlMonthManhourReport.SelectedValue + "-" + ddlYearManhourReport.SelectedValue);
                var workListPlan = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                var workListUnplan = workListService.GetByTypeAndYear(2, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).Select(t => t.ID).ToList();
                var projectAppendixList = projectAppendixService.GetByWork(workListUnplan, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).OrderBy(t => t.WorkID).ToList();
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        if (!string.IsNullOrEmpty(item["Column2"].ToString()))
                        {
                            var appendixObj = appendixListService.GetByCodeType(item["Column2"].ToString(), workListPlan);
                            if (appendixObj != null)
                            {
                                if (!appendixObj.MultipleProject.GetValueOrDefault())
                                {
                                    var projectAppendixObj = projectAppendixService.GetByAppendix(appendixObj.ID, workListPlan, year);
                                    if (projectAppendixObj != null)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixObj.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            var wmmList = new List<WorkgroupManhourMonth>();
                                            if (date > projectObj.Deadline)
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                            }
                                            else
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                            }

                                            double complete = 0.0;
                                            foreach (var wmmItem in wmmList)
                                            {
                                                var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                                complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                            }
                                            //manhour cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                            var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                            sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                            var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                            double sumManhourActualMonth = 0;
                                            sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);

                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixObj.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                            sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                            sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                            sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                                else
                                {
                                    sttMulti = 0;
                                    var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workListPlan, year);
                                    if (projectAppendixObj.Count != 0)
                                    {
                                        foreach (var projectAppendixItem in projectAppendixObj)
                                        {
                                            var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                            if (projectObj != null)
                                            {
                                                sheet.Cells.InsertRow(rowindex);
                                                count++;
                                                rowindex = firstrow + count;
                                                sttMulti++;
                                                var wmmList = new List<WorkgroupManhourMonth>();
                                                if (date > projectObj.Deadline)
                                                {
                                                    wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                                }
                                                else
                                                {
                                                    wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                                }
                                                double complete = 0.0;
                                                foreach (var wmmItem in wmmList)
                                                {
                                                    var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                                    complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                                }
                                                var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                                //manhour plan cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                                sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                                var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                                double sumManhourActualMonth = 0;
                                                sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);

                                                sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                                sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                                sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                                sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                                sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                                sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                                sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                                sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                                sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                                sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                                sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                                sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                                sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                                sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //var projectAppendixList = projectAppendixService.GetByWork(workListUnplan, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).OrderBy(t => t.WorkID).ToList();
                        if (projectAppendixList.Count != 0)
                        {
                            var projectAppendixByWork = projectAppendixList.Where(t => t.WorkID == Convert.ToInt32(item["Column1"].ToString())).ToList();

                            flag++;
                            countTemp = count;
                            foreach (var projectAppendixItem in projectAppendixByWork)
                            {
                                rowindex = index + 1 + count;
                                if (prefix != flag)
                                {
                                    prefix = flag;
                                    sttMulti = 1;
                                }
                                else
                                {
                                    sttMulti++;
                                }
                                var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                if (projectObj != null)
                                {
                                    var wmmList = new List<WorkgroupManhourMonth>();
                                    if (date > projectObj.Deadline)
                                    {
                                        wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                    }
                                    else
                                    {
                                        wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                    }

                                    double complete = 0.0;
                                    foreach (var wmmItem in wmmList)
                                    {
                                        var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                        complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                    }
                                    //manhour cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                    var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                    sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                    var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                    double sumManhourActualMonth = 0;
                                    sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);
                                    sheet.Cells.InsertRow(rowindex);
                                    count++;
                                    sheet.Cells["B" + rowindex].PutValue(prefix + "." + sttMulti);
                                    sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                    sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                    sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                    sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                    sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                    sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                    sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                    sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                    sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                    sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                    sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                    sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                    sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                }
                            }
                        }
                    }
                    count++;
                }
            }
            else if (typetemp == 3)
            {
                firstrow = 16;
                rowindex = 16;
                sheet.Cells["J10"].PutValue(ddlMonthManhourReport.SelectedValue + "-" + ddlYearManhourReport.SelectedValue);
                var sttMulti = 0;
                var workList = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(14, 1, sheet.Cells.MaxDataRow, 2);
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (!string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        var appendixObj = appendixListService.GetByCodeType(item["Column1"].ToString(), workList);
                        if (appendixObj != null)
                        {
                            if (!appendixObj.MultipleProject.GetValueOrDefault())
                            {
                                var projectAppendixObj = projectAppendixService.GetByWork(Convert.ToInt32(ddlWorkList.SelectedValue), year);
                                if (projectAppendixObj.Count != 0)
                                {
                                    foreach (var projectAppendixItem in projectAppendixObj)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            var wmmList = new List<WorkgroupManhourMonth>();
                                            if (date > projectObj.Deadline)
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                            }
                                            else
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, Convert.ToInt32(ddlYearManhourReport.SelectedValue));
                                            }
                                            double complete = 0.0;
                                            foreach (var wmmItem in wmmList)
                                            {
                                                var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                                complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                            }
                                            var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                            //manhour plan cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                            sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                            var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                            double sumManhourActualMonth = 0;
                                            sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);

                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                            sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                            sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                            sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                sttMulti = 0;
                                var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workList, year);
                                if (projectAppendixObj.Count != 0)
                                {
                                    foreach (var projectAppendixItem in projectAppendixObj)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            sheet.Cells.InsertRow(rowindex);

                                            rowindex = firstrow + count;
                                            sttMulti++;
                                            var wmmList = new List<WorkgroupManhourMonth>();
                                            if (date > projectObj.Deadline)
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                            }
                                            else
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                            }
                                            double complete = 0.0;
                                            foreach (var wmmItem in wmmList)
                                            {
                                                var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                                complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                            }
                                            var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                            //manhour plan cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                            sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                            var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                            double sumManhourActualMonth = 0;
                                            sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);

                                            sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                            sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                            sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                            sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                            sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                            count++;
                                        }
                                    }

                                }
                            }
                        }
                    }
                    count++;
                }
            }
            else if (typetemp == 4)
            {
                sheet.Cells["G10"].PutValue(ddlMonthManhourReport.SelectedValue + "-" + ddlYearManhourReport.SelectedValue);
                int index = 15;
                int prefix = 0;
                int sttMulti = 0;
                int flag = 0;
                int countTemp = 0;
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(14, 0, sheet.Cells.MaxDataRow, 2);
                //var workList = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                var workList = workListService.GetByTypeAndYear(2, year).Select(t => t.ID).ToList();
                //var projectAppendixList = projectAppendixService.GetByWork(workList, Convert.ToInt32(ddlYearManhourReport.SelectedValue)).OrderBy(t => t.WorkID).ToList();
                var workListPlan = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                var workListUnplan = workListService.GetByTypeAndYear(2, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).Select(t => t.ID).ToList();
                var projectAppendixList = projectAppendixService.GetByWork(workListUnplan, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).OrderBy(t => t.WorkID).ToList();
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        if (!string.IsNullOrEmpty(item["Column2"].ToString()))
                        {
                            var appendixObj = appendixListService.GetByCodeType(item["Column2"].ToString(), workListPlan);
                            if (appendixObj != null)
                            {
                                if (!appendixObj.MultipleProject.GetValueOrDefault())
                                {
                                    var projectAppendixObj = projectAppendixService.GetByAppendix(appendixObj.ID, workListPlan, year);
                                    if (projectAppendixObj != null)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixObj.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            var wmmList = new List<WorkgroupManhourMonth>();
                                            if (date > projectObj.Deadline)
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                            }
                                            else
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                            }

                                            double complete = 0.0;
                                            foreach (var wmmItem in wmmList)
                                            {
                                                var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                                complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                            }
                                            //manhour cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                            var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                            sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                            var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                            double sumManhourActualMonth = 0;
                                            sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);

                                            sheet.Cells["I" + rowindex].PutValue(projectAppendixObj.Basis);
                                            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                            sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                            sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                            sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                            sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                                else
                                {
                                    sttMulti = 0;
                                    var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workListPlan, year);
                                    if (projectAppendixObj.Count != 0)
                                    {
                                        foreach (var projectAppendixItem in projectAppendixObj)
                                        {
                                            var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                            if (projectObj != null)
                                            {
                                                sheet.Cells.InsertRow(rowindex);
                                                count++;
                                                rowindex = firstrow + count;
                                                sttMulti++;
                                                var wmmList = new List<WorkgroupManhourMonth>();
                                                if (date > projectObj.Deadline)
                                                {
                                                    wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                                }
                                                else
                                                {
                                                    wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                                }
                                                double complete = 0.0;
                                                foreach (var wmmItem in wmmList)
                                                {
                                                    var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                                    complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                                }
                                                var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                                //manhour plan cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                                sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                                var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                                double sumManhourActualMonth = 0;
                                                sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);

                                                sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                                sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                                sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                                sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                                sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                                sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                                sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                                sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                                sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                                sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                                sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                                sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                                sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                                sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //var projectAppendixList = projectAppendixService.GetByWork(workListUnplan, Convert.ToInt32(ddlYearProgressReport.SelectedValue)).OrderBy(t => t.WorkID).ToList();
                        if (projectAppendixList.Count != 0)
                        {
                            var projectAppendixByWork = projectAppendixList.Where(t => t.WorkID == Convert.ToInt32(item["Column1"].ToString())).ToList();

                            flag++;
                            countTemp = count;
                            foreach (var projectAppendixItem in projectAppendixByWork)
                            {
                                rowindex = index + 1 + count;
                                if (prefix != flag)
                                {
                                    prefix = flag;
                                    sttMulti = 1;
                                }
                                else
                                {
                                    sttMulti++;
                                }
                                var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                if (projectObj != null)
                                {
                                    var wmmList = new List<WorkgroupManhourMonth>();
                                    if (date > projectObj.Deadline)
                                    {
                                        wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                    }
                                    else
                                    {
                                        wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                    }

                                    double complete = 0.0;
                                    foreach (var wmmItem in wmmList)
                                    {
                                        var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                        complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                    }
                                    //manhour cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                    var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                    sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                    var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                    double sumManhourActualMonth = 0;
                                    sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);
                                    sheet.Cells.InsertRow(rowindex);
                                    count++;
                                    sheet.Cells["B" + rowindex].PutValue(prefix + "." + sttMulti);
                                    sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                    sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                                    sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                                    sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                    sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                                    sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                    sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                    sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                    sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                    sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                    sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                    sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                    sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                                }
                            }
                        }
                        count++;

                        //if (!string.IsNullOrEmpty(item["Column1"].ToString()))
                        //{
                        //    var projectAppendixByWork = projectAppendixList.Where(t => t.WorkID == Convert.ToInt32(item["Column1"].ToString())).ToList();
                        //    prefix = Convert.ToInt32(item["Column2"].ToString());
                        //    foreach (var projectAppendixItem in projectAppendixByWork)
                        //    {
                        //        firstrow = index + 1 + count;
                        //        if (prefix != flag)
                        //        {
                        //            flag = prefix;
                        //            sttMulti = 1;
                        //        }
                        //        else
                        //        {
                        //            sttMulti++;
                        //        }
                        //        var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                        //        if (projectObj != null)
                        //        {
                        //            sheet.Cells.InsertRow(firstrow);
                        //            count++;
                        //            rowindex = firstrow;
                        //            var wmmList = new List<WorkgroupManhourMonth>();
                        //            if (date > projectObj.Deadline)
                        //            {
                        //                wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                        //            }
                        //            else
                        //            {
                        //                wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                        //            }
                        //            double complete = 0.0;
                        //            foreach (var wmmItem in wmmList)
                        //            {
                        //                var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                        //                complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                        //            }
                        //            var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                        //            //manhour plan cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                        //            sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                        //            var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                        //            double sumManhourActualMonth = 0;
                        //            sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);
                        //            sheet.Cells["B" + rowindex].PutValue(prefix + "." + sttMulti);
                        //            sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                        //            sheet.Cells["I" + rowindex].PutValue(projectAppendixItem.Basis);
                        //            sheet.Cells["J" + rowindex].PutValue(projectObj.SupervisorFullName);
                        //            sheet.Cells["K" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                        //            sheet.Cells["L" + rowindex].PutValue(projectObj.Name);
                        //            sheet.Cells["M" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                        //            sheet.Cells["N" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                        //            sheet.Cells["O" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                        //            sheet.Cells["P" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                        //            sheet.Cells["Q" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                        //            sheet.Cells["R" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                        //            sheet.Cells["S" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                        //            sheet.Cells["T" + rowindex].PutValue(projectObj.Notes);
                        //        }
                        //    }
                        //}
                        //index++;
                    }
                }
            }
            else if (typetemp == 6)
            {
                firstrow = 7;
                rowindex = 7;
                // Create a datatable
                var totalDataTable = new DataTable();
                totalDataTable = sheet.Cells.ExportDataTableAsString(8, 1, sheet.Cells.MaxDataRow, 2);
                sheet.Cells["F4"].PutValue(ddlMonthManhourReport.SelectedValue + "-" + ddlYearManhourReport.SelectedValue);
                var sttMulti = 0;
                var workList = workListService.GetByParent(Convert.ToInt32(ddlWorkList.SelectedValue)).Select(t => t.ID).ToList();
                foreach (DataRow item in totalDataTable.Rows)
                {
                    rowindex = firstrow + count;
                    if (!string.IsNullOrEmpty(item["Column1"].ToString()))
                    {
                        var appendixObj = appendixListService.GetByCodeType(item["Column1"].ToString(), workList);
                        if (appendixObj != null)
                        {
                            if (!appendixObj.MultipleProject.GetValueOrDefault())
                            {
                                var projectAppendixObj = projectAppendixService.GetByAppendix(appendixObj.ID, workList, year);
                                if (projectAppendixObj != null)
                                {
                                    var projectObj = scopeProjectService.GetById(projectAppendixObj.ProjectID.GetValueOrDefault());
                                    if (projectObj != null)
                                    {
                                        var wmmList = new List<WorkgroupManhourMonth>();
                                        if (date > projectObj.Deadline)
                                        {
                                            wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                        }
                                        else
                                        {
                                            wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                        }

                                        double complete = 0.0;
                                        foreach (var wmmItem in wmmList)
                                        {
                                            var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                            complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                        }
                                        //manhour cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                        var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                        sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                        var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                        double sumManhourActualMonth = 0;
                                        sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);


                                        sheet.Cells["K" + rowindex].PutValue(projectAppendixObj.Basis);
                                        sheet.Cells["L" + rowindex].PutValue(projectObj.SupervisorFullName);
                                        sheet.Cells["M" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                        sheet.Cells["N" + rowindex].PutValue(projectObj.Name);
                                        sheet.Cells["O" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["P" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                        sheet.Cells["Q" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                        sheet.Cells["R" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                        sheet.Cells["S" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                        sheet.Cells["T" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                        sheet.Cells["U" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                        sheet.Cells["V" + rowindex].PutValue(projectObj.Notes);
                                    }
                                }
                            }
                            else
                            {
                                sttMulti = 0;
                                var projectAppendixObj = projectAppendixService.GetAllByAppendix(appendixObj.ID, workList, year);
                                if (projectAppendixObj.Count != 0)
                                {
                                    foreach (var projectAppendixItem in projectAppendixObj)
                                    {
                                        var projectObj = scopeProjectService.GetById(projectAppendixItem.ProjectID.GetValueOrDefault());
                                        if (projectObj != null)
                                        {
                                            sheet.Cells.InsertRow(rowindex);
                                            count++;
                                            rowindex = firstrow + count;
                                            sttMulti++;
                                            var wmmList = new List<WorkgroupManhourMonth>();
                                            if (date > projectObj.Deadline)
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetLastedByPJ(projectObj.ID);
                                            }
                                            else
                                            {
                                                wmmList = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, month, year);
                                            }
                                            double complete = 0.0;
                                            foreach (var wmmItem in wmmList)
                                            {
                                                var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                                                complete += (wmmItem.WorkgroupWeight.GetValueOrDefault() * wmmItem.CompleteActualTotal.GetValueOrDefault()) / 100;
                                            }
                                            var sumManhourPlanTotal = wmmList.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                                            //manhour plan cho Vien: sumManhour*1.1 (5% cho cv chung, 5% cho cv quan ly)
                                            sumManhourPlanTotal = sumManhourPlanTotal * 1.1;
                                            var sumManhourActualTotal = wmmList.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.1);
                                            double sumManhourActualMonth = 0;
                                            sumManhourActualMonth = wmmList.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.1);

                                            sheet.Cells["B" + rowindex].PutValue(sttMulti.ToString());
                                            sheet.Cells["C" + rowindex].PutValue(projectObj.Description);
                                            sheet.Cells["K" + rowindex].PutValue(projectAppendixItem.Basis);
                                            sheet.Cells["L" + rowindex].PutValue(projectObj.SupervisorFullName);
                                            sheet.Cells["M" + rowindex].PutValue(projectObj.ProjectManagerFullName);
                                            sheet.Cells["N" + rowindex].PutValue(projectObj.Name);
                                            sheet.Cells["O" + rowindex].PutValue(projectObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["P" + rowindex].PutValue(projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                            sheet.Cells["Q" + rowindex].PutValue(Math.Round(complete, 2).ToString());
                                            sheet.Cells["R" + rowindex].PutValue(projectObj.EndDate.HasValue ? projectObj.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : "");
                                            sheet.Cells["S" + rowindex].PutValue(sumManhourPlanTotal.ToString());
                                            sheet.Cells["T" + rowindex].PutValue(Math.Round(sumManhourActualTotal, 2).ToString());
                                            sheet.Cells["U" + rowindex].PutValue(Math.Round(sumManhourActualMonth, 2).ToString());
                                            sheet.Cells["V" + rowindex].PutValue(projectObj.Notes);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    count++;
                }
            }

            filename = filename + "_" + ddlMonthManhourReport.SelectedValue + "-" + ddlYearManhourReport.SelectedValue + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, filename, false);
        }

        protected void btnChartLeaderVien_Click(object sender, EventArgs e)
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\ProgressManhourTemplate_PL.xls");
            var wsProgress = workbook.Worksheets[0];
            var allworkList = new List<Work>();
            var year = Convert.ToInt32(ddlYearManhourReport.SelectedValue);
            if (ddlWorkList.SelectedValue == "20")
            {
                allworkList = this.workListService.GetAllParent(year);
            }
            else
            {
                var work = this.workListService.GetById(Convert.ToInt32(ddlWorkList.SelectedValue));
                allworkList.Add(work);
            }

            var wgCount = allworkList.Count;
            var flag = 0;
            var processReportList = new List<ProcessReport>();
            wsProgress.Cells["A1"].PutValue(0);
            wsProgress.Cells["A2"].PutValue(wgCount);
            wsProgress.Cells["A3"].PutValue(12);
            wsProgress.Cells.InsertRows(8, wgCount * 3);
            foreach (var item in allworkList)
            {
                var workList = workListService.GetByParent(item.ID).Select(t => t.ID).ToList();
                var projectInWork = this.projectAppendixService.GetByWork(workList, Convert.ToInt32(ddlYearManhourReport.SelectedValue)).Select(t => t.ProjectID.GetValueOrDefault()).Distinct().ToList();
                var wmmList = this.workgroupManhourMonthService.GetByPJ(projectInWork, Convert.ToInt32(ddlYearManhourReport.SelectedValue));
                var count = 3;
                wsProgress.Cells["B" + (8 + (flag * 3))].PutValue(item.Name);
                wsProgress.Cells["C" + (8 + (flag * 3))].PutValue("Planed");
                wsProgress.Cells["C" + (9 + (flag * 3))].PutValue("Actual");
                wsProgress.Cells["C" + (10 + (flag * 3))].PutValue("+/-");

                wsProgress.Cells.Merge(7 + (flag * 3), 1, 3, 1);

                for (int i = 1; i <= 12; i++)
                {
                    var processReport = new ProcessReport();
                    processReportList.Add(processReport);
                    DateTime d = new DateTime(year, i, 1);
                    wsProgress.Cells[2, count].PutValue(d);

                    wsProgress.Cells[3, count].PutValue(count - 2);
                    wsProgress.Cells[4, count].PutValue(d);

                    count += 1;

                    var wmmMonth = wmmList.Where(t => t.Month == i).ToList();
                    var manhourPlan = Math.Round(wmmMonth.Sum(t => t.ManhourPlanMonth.GetValueOrDefault()));
                    var manhourReal = Math.Round(wmmMonth.Sum(t => t.ManhourActualMonth.GetValueOrDefault()));
                    wsProgress.Cells[7 + (flag * 3), i + 2].PutValue(manhourPlan);
                    wsProgress.Cells[8 + (flag * 3), i + 2].PutValue(manhourReal);
                    wsProgress.Cells[9 + (flag * 3), i + 2].PutValue(manhourReal - manhourPlan);
                    processReportList[i - 1].Planed += manhourPlan;
                    processReportList[i - 1].Actual += manhourReal;
                }

                flag++;
            }
            for (int i = 1; i <= 12; i++)
            {
                wsProgress.Cells["B" + (8 + (wgCount * 3))].PutValue("Overall Project");
                wsProgress.Cells["C" + (8 + (wgCount * 3))].PutValue("Planed");
                wsProgress.Cells["C" + (8 + (wgCount * 3) + 1)].PutValue("Actual");
                wsProgress.Cells["C" + (8 + (wgCount * 3) + 2)].PutValue("+/-");
                wsProgress.Cells[7 + (wgCount * 3), i + 2].PutValue(processReportList[i - 1].Planed);
                wsProgress.Cells[8 + (wgCount * 3), i + 2].PutValue(processReportList[i - 1].Actual);
                wsProgress.Cells[9 + (wgCount * 3), i + 2].PutValue(processReportList[i - 1].Actual - processReportList[i - 1].Planed);
            }
            var filename = "Manhour Report of " + Utilities.Utility.RemoveAllSpecialCharacter(ddlWorkList.SelectedItem.Text) + " - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, filename, true);
        }

        protected void btnDepartmentManhourReport_Click(object sender, EventArgs e)
        {
            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            var filename = "";
            workbook.Open(filePath + @"Template\ReportDepartmentMonthTemp.xls");
            var sheets = workbook.Worksheets;
            var sheet = sheets[0];

            // wmm theo department
            int year = Convert.ToInt32(ddlYearManhourReport.SelectedValue);
            var wmmListThisYear = this.workgroupManhourMonthService.GetByDP(Convert.ToInt32(ddlDepartmentReport.SelectedValue), year);
            var wmmList = wmmListThisYear.Where(t => t.Month == Convert.ToInt32(ddlMonthManhourReport.SelectedValue));
            var projectAppendixList = this.projectAppendixService.GetByYear(year).Select(t => t.ProjectID.GetValueOrDefault()).ToList();
            var pjList = this.scopeProjectService.GetAll(wmmList.Select(t => t.ProjectID.GetValueOrDefault()).Distinct().ToList());
            pjList = pjList.Where(t => projectAppendixList.Contains(t.ID)).ToList();
            var count = 7;
            int stt = 1;
            double sumTotalManhourPlan = 0.0;
            double sumTotalAccumulationActual = 0.0;
            double sumTotalAccumulationPlan = 0;
            double sumAccumulationPlanThisYear = 0;
            double sumManhourPlanThisMonth = 0;
            double sumAccumulationActualThisYear = 0;
            double sumManhourActualThisMonth = 0.0;

            foreach (var item in pjList)
            {
                double totalManhourPlan = 0.0;
                double totalAccumulationActual = 0.0;
                double totalAccumulationPlan = 0;
                double accumulationPlanThisYear = 0;
                double manhourPlanThisMonth = 0;
                double accumulationActualThisYear = 0;
                double manhourActualThisMonth = 0.0;

                var wmmPJ = wmmList.Where(t => t.ProjectID == item.ID).ToList();
                //var wmmPJThisYear = this.workgroupManhourMonthService.GetByPJ(item.ID, Convert.ToInt32(ddlDepartmentReport.SelectedValue), 12, (year - 1));
                var wmmPJThisYear = wmmListThisYear.Where(t => t.ProjectID == item.ID);
                //manhour plan cho department: sumManhour*1.05 (5% cho cv chung)

                totalManhourPlan = wmmPJ.Sum(t => t.ManhourPlanTotal.GetValueOrDefault());
                totalManhourPlan = Math.Round(totalManhourPlan * 1.05, 2);
                totalAccumulationPlan = wmmPJ.Sum(t => (t.ManhourPlanTotal.GetValueOrDefault() * t.CompletePlanTotal.GetValueOrDefault()) / 100);
                totalAccumulationPlan = Math.Round(totalAccumulationPlan * 1.05, 2);
                var tempAccumulationPlanThisYear = wmmPJThisYear.Sum(t => (t.ManhourPlanMonth.GetValueOrDefault()));
                tempAccumulationPlanThisYear = tempAccumulationPlanThisYear * 1.05;
                accumulationPlanThisYear = Math.Round(tempAccumulationPlanThisYear, 2);
                //accumulationPlanThisYear = Math.Round(totalAccumulationPlan - tempAccumulationPlanThisYear, 2);
                manhourPlanThisMonth = wmmPJ.Sum(t => t.ManhourPlanMonth.GetValueOrDefault());
                manhourPlanThisMonth = Math.Round(manhourPlanThisMonth * 1.05, 2);

                totalAccumulationActual = wmmPJ.Sum(t => t.ManhourActualAccumulation.GetValueOrDefault() * 1.05);
                totalAccumulationActual = Math.Round(totalAccumulationActual, 2);
                manhourActualThisMonth = wmmPJ.Sum(t => t.ManhourActualMonth.GetValueOrDefault() * 1.05);
                manhourActualThisMonth = Math.Round(manhourActualThisMonth, 2);
                var tempAccumulationActualThisYear = wmmPJThisYear.Sum(t => t.ManhourActualMonth.GetValueOrDefault());
                tempAccumulationActualThisYear = tempAccumulationActualThisYear * 1.05;
                accumulationActualThisYear = Math.Round(tempAccumulationActualThisYear, 2);
                //accumulationActualThisYear = Math.Round(totalAccumulationActual - tempAccumulationActualThisYear, 2);
                sheet.Cells.InsertRow(count);
                sheet.Cells["B" + count].PutValue(stt);
                sheet.Cells["C" + count].PutValue(item.Name);
                sheet.Cells["D" + count].PutValue(item.Description);

                sheet.Cells["E" + count].PutValue(totalManhourPlan);
                sheet.Cells["F" + count].PutValue(totalAccumulationPlan);
                sheet.Cells["G" + count].PutValue(accumulationPlanThisYear);
                sheet.Cells["H" + count].PutValue(manhourPlanThisMonth);
                sheet.Cells["I" + count].PutValue(totalAccumulationActual);
                sheet.Cells["J" + count].PutValue(accumulationActualThisYear);
                sheet.Cells["K" + count].PutValue(manhourActualThisMonth);

                count++;
                stt++;
                sumTotalManhourPlan += totalManhourPlan;
                sumTotalAccumulationPlan += totalAccumulationPlan;
                sumAccumulationPlanThisYear += accumulationPlanThisYear;
                sumManhourPlanThisMonth += manhourPlanThisMonth;
                sumTotalAccumulationActual += totalAccumulationActual;
                sumAccumulationActualThisYear += accumulationActualThisYear;
                sumManhourActualThisMonth += manhourActualThisMonth;
            }
            sheet.Cells["E" + (count + 1)].PutValue(sumTotalManhourPlan);
            sheet.Cells["F" + (count + 1)].PutValue(sumTotalAccumulationPlan);
            sheet.Cells["G" + (count + 1)].PutValue(sumAccumulationPlanThisYear);
            sheet.Cells["H" + (count + 1)].PutValue(sumManhourPlanThisMonth);
            sheet.Cells["I" + (count + 1)].PutValue(sumTotalAccumulationActual);
            sheet.Cells["J" + (count + 1)].PutValue(sumAccumulationActualThisYear);
            sheet.Cells["K" + (count + 1)].PutValue(sumManhourActualThisMonth);

            sheet.Cells["D1"].PutValue(ddlDepartmentReport.SelectedItem.Text + " DEPARTMENT - MANHOUR REPORT");
            sheet.Cells["D3"].PutValue(ddlMonthManhourReport.SelectedValue + "-" + ddlYearManhourReport.SelectedValue);
            sheet.Cells["K1"].PutValue(ddlDepartmentReport.SelectedItem.Text);
            sheet.Cells["K2"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
            filename = ddlDepartmentReport.SelectedItem.Text + " - " + "Manhour Report - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, filename, true);
        }

        protected void btnChartDepartment_Click(object sender, EventArgs e)
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\ProgressManhourTemplate_PL.xls");
            var wsProgress = workbook.Worksheets[0];

            var wmmList = this.workgroupManhourMonthService.GetByDP(Convert.ToInt32(ddlDepartmentReport.SelectedValue), Convert.ToInt32(ddlYearManhourReport.SelectedValue));
            var processReportList = new List<ProcessReport>();
            var count = 3;
            var year = Convert.ToInt32(ddlYearManhourReport.SelectedValue);
            var wgCount = 1;
            wsProgress.Cells["B" + 8].PutValue(ddlDepartmentReport.SelectedItem.Text);
            wsProgress.Cells["C" + 8].PutValue("Planed");
            wsProgress.Cells["C" + 9].PutValue("Actual");
            wsProgress.Cells["C" + 10].PutValue("+/-");
            wsProgress.Cells["A1"].PutValue(0);
            wsProgress.Cells["A2"].PutValue(1);
            wsProgress.Cells["A3"].PutValue(12);
            wsProgress.Cells.InsertRows(8, wgCount * 3);
            wsProgress.Cells.Merge(7, 1, 3, 1);

            for (int i = 1; i <= 12; i++)
            {
                DateTime d = new DateTime(year, i, 1);
                wsProgress.Cells[2, count].PutValue(d);
                wsProgress.Cells[3, count].PutValue(count - 2);
                wsProgress.Cells[4, count].PutValue(d);
                count += 1;
                var wmmMonth = wmmList.Where(t => t.Month == i).ToList();
                var manhourPlan = Math.Round(wmmMonth.Sum(t => t.ManhourPlanMonth.GetValueOrDefault()));
                var manhourReal = Math.Round(wmmMonth.Sum(t => t.ManhourActualMonth.GetValueOrDefault()));
                wsProgress.Cells[7, i + 2].PutValue(manhourPlan);
                wsProgress.Cells[8, i + 2].PutValue(manhourReal);
                wsProgress.Cells[9, i + 2].PutValue(manhourReal - manhourPlan);
                wsProgress.Cells["B" + (8 + (wgCount * 3))].PutValue("Overall Project");
                wsProgress.Cells["C" + (8 + (wgCount * 3))].PutValue("Planed");
                wsProgress.Cells["C" + (8 + (wgCount * 3) + 1)].PutValue("Actual");
                wsProgress.Cells["C" + (8 + (wgCount * 3) + 2)].PutValue("+/-");
                wsProgress.Cells[7 + (wgCount * 3), i + 2].PutValue(manhourPlan);
                wsProgress.Cells[8 + (wgCount * 3), i + 2].PutValue(manhourReal);
                wsProgress.Cells[9 + (wgCount * 3), i + 2].PutValue(manhourReal - manhourPlan);
            }

            var filename = "Manhour Progress report of " + ddlDepartmentReport.SelectedItem.Text + "-" + DateTime.Now.ToString("dd - MM - yyyy") + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, filename, true);
        }

        protected void ddlYearProgressReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            var workList = this.workListService.GetByYear(Convert.ToInt32(ddlYearProgressReport.SelectedValue)).Where(t => t.ParentID == null).ToList();
            var workListPlan = workList.Where(t => t.Type == 1).ToList();
            if ((workList.Where(t => t.Type == 2 && t.Name == "Unplanned").Any()))
            {
                workListPlan.Add((workList.First(t => t.Type == 2 && t.Name == "Unplanned")));
            }
            this.ddlWorkList.DataSource = workListPlan;
            this.ddlWorkList.DataTextField = "Name";
            this.ddlWorkList.DataValueField = "ID";
            this.ddlWorkList.DataBind();
        }
    }
}
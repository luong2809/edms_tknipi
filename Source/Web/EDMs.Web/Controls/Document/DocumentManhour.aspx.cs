﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace EDMs.Web.Controls.Document
{

     using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;
    using System.Globalization;


    using Telerik.Web.UI;
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentManhour : Page
    {
        private readonly DocumentPackageService documentpackageService;
        private readonly ManhourService manhourService;

        private DocumentPackage DocNo
        {
            get
            {
                return this.documentpackageService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DocumentManhour()
        {
            this.manhourService = new ManhourService();
            this.documentpackageService = new DocumentPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.btnSave.Enabled = this.txtMilestone.SelectedDate != null;

                //if (UserSession.Current.IsEngineer)
                //{
                //   // this.EditContent.Visible = false;
                //    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                //    this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                //    //this.grdDocument.Height = 547;
                //}

               // check auto calculate
             
             

                this.ClearData();
               
            }
            this.txtRealTotal.Enabled = false;
            this.txtPlan.Enabled = false;
               
            this.txtdocument.Text = DocNo.DocNo;
            this.txtPerformingUser.Text = DocNo.EngineerName;
            this.txtPlan.Value = DocNo.ManHourPlan;
            this.txtRealTotal.Value = DocNo.ManHours;
          

        }

      


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            this.Session.Remove("IsFillData");
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var Document = this.documentpackageService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
                 CultureInfo myCI = CultureInfo.CurrentCulture;
                 System.Globalization.Calendar myCal = myCI.Calendar;
                 CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                 DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
 
                //return myCal.GetWeekOfYear(time, myCWR, myFirstDOW);
                
                
                if (Session["EditingId"] != null)
                {
                   
                    var milestoneId = Convert.ToInt32(Session["EditingId"]);
                    var manhourObj = this.manhourService.GetById(milestoneId);

                     Document.ManHours+=(this.txtReal.Value-manhourObj.Total.GetValueOrDefault());
                    if (manhourObj != null)
                    {
                       
                        manhourObj.ManhourDate = this.txtMilestone.SelectedDate;
                        
                        var oldmanhour = manhourObj.Total;
                        manhourObj.Total = this.txtReal.Value;
                        var newmanhour = this.txtReal.Value;

                        manhourObj.Engineer = Convert.ToInt32(Document.EngineerId);
                        manhourObj.Note = this.txtNote.Text;
                        manhourObj.EngineerName = Document.EngineerName;
                        manhourObj.DocumentID = Document.ID;
                        manhourObj.DocumentName = Document.DocNo;
                        manhourObj.UpdateDate = DateTime.Now;
                        manhourObj.UpdateBy = UserSession.Current.User.Id;

                        var list = this.manhourService.GetAllByDocumentPackage(Document.ID).ToList();
                        var check = list.FirstOrDefault(t => t.ID != manhourObj.ID &&
                            myCal.GetWeekOfYear(DateTime.Now.Date, myCWR, myFirstDOW) == myCal.GetWeekOfYear(t.ManhourDate.GetValueOrDefault(), myCWR, myFirstDOW));
                        if(check==null){
                        this.manhourService.Update(manhourObj);
                        
                        //auto update % difference total
                       // var totalmanhour = this.manhourService.GetAllByDocumentPackage(Document.ID);
                       // var value = 0.0;
                       // value = totalmanhour.Aggregate(value, (current, t) => current + t.Total.GetValueOrDefault());
                       
                        this.documentpackageService.Update(Document);
                        }
                       
                    }

                    Session.Remove("EditingId");
                }
                else
                {
                   
                    var list = this.manhourService.GetAllByDocumentPackage(Document.ID, DateTime.Now.Date.Month, DateTime.Now.Date.Year).ToList();
                    var temp = list.FirstOrDefault(t =>
                        myCal.GetWeekOfYear(DateTime.Now.Date, myCWR, myFirstDOW) == myCal.GetWeekOfYear(t.ManhourDate.GetValueOrDefault(), myCWR, myFirstDOW));

                    if (temp == null)
                    {
                        var documentmanhour = new Manhour()
                        {
                            ManhourDate = this.txtMilestone.SelectedDate,
                            Total = this.txtReal.Value,
                            DocumentID = Document.ID,
                            DocumentName = Document.DocNo,
                            Engineer = Convert.ToInt32(Document.EngineerId),
                            EngineerName = Document.EngineerName,
                            Note = this.txtNote.Text,
                            CreateBy = UserSession.Current.User.Id,
                            CreateDate = DateTime.Now
                        };
                        this.manhourService.Insert(documentmanhour);

                            //var totalmanhour =this.manhourService.GetAllByDocumentPackage(Document.ID) ;
                            //var value = 0.0;
                            //value = totalmanhour.Aggregate(value, (current, t) => current + t.Total.GetValueOrDefault());
                            Document.ManHours +=this.txtReal.Value;
                            this.documentpackageService.Update(Document);

                    }
                    else if(string.IsNullOrEmpty(temp.Total.ToString()))
	                {  
                        temp.Total = this.txtReal.Value;
                        temp.ManhourDate = this.txtMilestone.SelectedDate;
                        temp.Note = this.txtNote.Text;
                        this.manhourService.Update(temp);

                        //var totalmanhour =this.manhourService.GetAllByDocumentPackage(Document.ID) ;
                        //var value = 0.0;
                        //value = totalmanhour.Aggregate(value, (current, t) => current + t.Total.GetValueOrDefault());
                        Document.ManHours +=(this.txtReal.Value-temp.Total
                            .GetValueOrDefault()) ;


                        this.documentpackageService.Update(Document);
	                }
                    else
                    {
                         ScriptManager.RegisterStartupScript(
                                      this,
                                      typeof(Page),
                                      "Alert",
                                      "<script>alert('ManHours updated for this week. Please select another week');</script>",
                                      false);
                    }
                  
                }

                this.txtdocument.Text = Document.DocNo;
                this.txtPerformingUser.Text = Document.EngineerName;
                this.txtPlan.Value = Document.ManHourPlan;
                this.txtRealTotal.Value = Document.ManHours;
            }

            this.ClearData();
            this.grdDocument.Rebind();
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var DocumentID = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
             
                var manhour=this.manhourService.GetById(DocumentID);
                var hour = manhour.Total;
                this.manhourService.Delete(manhour);
                var document = this.documentpackageService.GetById(Int32.Parse(Request.QueryString["docId"]));

                document.ManHours -= hour;

                this.documentpackageService.Update(document);
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(
                    //                  this,
                    //                  typeof(Page),
                    //                  "Alert",
                    //                  "<script>alert('Only allowed to delete the latest milestone.');</script>",
                    //                  false);

                    //}



                this.ClearData();

        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = Convert.ToInt32(Request.QueryString["docId"]);
                this.grdDocument.DataSource = this.manhourService.GetAllByDocumentPackage(docId).Where(t=>!string.IsNullOrEmpty(t.Total.ToString()));
       
            }
            else
            {
                this.grdDocument.DataSource = new List<Manhour>();
            }
            
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
           // throw new NotImplementedException();
        }

        protected void rbtnDefaultDoc_CheckedChanged(object sender, EventArgs e)
        {
            //((GridItem)((RadioButton)sender).Parent.Parent).Selected = ((RadioButton)sender).Checked;

            //var item = ((RadioButton)sender).Parent.Parent as GridDataItem;
            //var attachFileId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            //var attachFileObj = this.attachFileService.GetById(attachFileId);
            //if (attachFileObj != null)
            //{
            //    var attachFiles = this.attachFileService.GetAllByDocId(attachFileObj.DocumentId.GetValueOrDefault());
            //    foreach (var attachFile in attachFiles)
            //    {
            //        attachFile.IsDefault = attachFile.ID == attachFileId;
            //        this.attachFileService.Update(attachFile);
            //    }
            //}
        }

        protected void grdDocument_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            if (e.CommandName == "EditCmd")
            {
                var milestoneId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
                var manhourObj = this.manhourService.GetById(milestoneId);

                var document = this.documentpackageService.GetById(Int32.Parse(Request.QueryString["docId"]));

                if (manhourObj != null)
                {
                    Session.Add("EditingId", manhourObj.ID);
                    this.txtMilestone.SelectedDate = manhourObj.ManhourDate;
                    this.txtPlan.Value = document.ManHourPlan;
                    this.txtReal.Value = manhourObj.Total;
                    this.txtRealTotal.Value = document.ManHours;
                    this.txtdocument.Text = manhourObj.DocumentName;
                    this.txtPerformingUser.Text = manhourObj.EngineerName;
                    this.txtNote.Text = manhourObj.Note;
                }

                this.btnSave.Enabled = this.txtMilestone.SelectedDate != null;

                //kiem tra thang co phai moi nhat ko neu ko an txtplannextmonth
                //var date = txtMilestone.SelectedDate;
                //if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                //{
                //    var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                //    var wp = this.milestoneService.GetAllByWorkpackage(workpackageId).ToList();
                //    if (wp.Count > 1)
                //    {
                //        DateTime dateofmileston = wp[0].MilestoneDate.GetValueOrDefault();
                //        this.txtPlannextMonth.Enabled = (date.GetValueOrDefault().Month < dateofmileston.Month) ? false : true;
                //    }
                //}
                Session.Add("LastId",FindLastIdMileston( manhourObj.ID));

            }
        }

        private void ClearData()
        {
            this.txtMilestone.SelectedDate = null;
           // this.txtPerformingUser.Text = string.Empty;
            this.txtNote.Text = string.Empty;
            //this.txtPlan.Value = null;
            
           this.txtReal.Value = null;
           // this.txtRealTotal.Value = null;
           // this.txtdocument.Text = null;    
            Session.Remove("EditingId");
            Session.Remove("LastId");
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            this.ClearData();
            this.btnSave.Enabled = this.txtMilestone.SelectedDate != null;
        }

        protected void txtReal_OnTextChanged(object sender, EventArgs e)
        {
            if (DocNo != null)
            {
                var document = this.documentpackageService.GetById(Int32.Parse(Request.QueryString["docId"]));
                if (Session["LastId"] != null)
                {
                var milestoneId = Convert.ToInt32(Session["LastId"]);
                var manhourObj = this.manhourService.GetById(milestoneId);
                if (manhourObj != null)
                {

                    this.txtRealTotal.Value = document.ManHours + (this.txtReal.Value - manhourObj.Total);
                }
                else
                {
                    milestoneId = Convert.ToInt32(Session["EditingId"]);
                    manhourObj = this.manhourService.GetById(milestoneId);
                    if (manhourObj != null)
                    {
                        this.txtRealTotal.Value = document.ManHours + (this.txtReal.Value - manhourObj.Total);
                    }
                }
                }
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                    var wp = this.manhourService.GetAllByDocumentPackage(workpackageId).ToList();
                    if (wp.Count != 0)
                    {
                        this.txtRealTotal.Value = document.ManHours + this.txtReal.Value;
                    }
                    else
                    {
                        this.txtRealTotal.Value = this.txtReal.Value + (document.ManHours != null ? document.ManHours:0);
                    }
                }
            }
            }
        }


        protected void ServerValidationMilestone(object source, ServerValidateEventArgs args)
        {
            if (this.txtMilestone.SelectedDate == null)
            {
                this.MilestoneValidator.ErrorMessage = "Please enter Date.";
                this.divMilestone.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }
        protected bool checkDateOfMileston()
        {
            //kiem tra thang co phai moi nhat ko neu ko an txtplannextmonth
            var date = txtMilestone.SelectedDate;
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]) && !string.IsNullOrEmpty(date.ToString()))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                var wp = this.manhourService.GetAllByDocumentPackage(workpackageId).ToList();
                if (wp.Count > 1)
                {
                    DateTime dateofmileston = wp[0].ManhourDate.GetValueOrDefault();
                    if (date.GetValueOrDefault().Month < dateofmileston.Month) return true;

                }
            }
            return false;
        }
    

        protected int FindLastIdMileston(int milestonId){
            var Id = -1;
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                var wp = this.manhourService.GetAllByDocumentPackage(workpackageId).OrderBy(t=>t.ManhourDate).ToList();
                foreach (var miles in wp) {
                    if (miles.ID > Id && miles.ID < milestonId) Id = miles.ID;
                }
            }
            
            return Id;
        }

        protected void txtMilestone_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            this.btnSave.Enabled = this.txtMilestone.SelectedDate != null;
            //kiem tra thang co phai moi nhat ko neu ko an txtplannextmonth
            var date = txtMilestone.SelectedDate;
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                var wp = this.manhourService.GetAllByDocumentPackage(workpackageId).ToList();
                if (wp.Count > 1)
                {
                    DateTime dateofmileston = wp[0].ManhourDate.GetValueOrDefault();
                    //this.txtPlannextMonth.Enabled = (date.GetValueOrDefault().Month < dateofmileston.Month) ? false : true;
                }
            }
        }

        

    }
}
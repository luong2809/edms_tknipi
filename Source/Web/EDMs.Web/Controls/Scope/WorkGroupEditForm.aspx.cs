﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Hosting;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using System.Text.RegularExpressions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Business.Services.Document;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;
    using System.Collections.Generic;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class WorkGroupEditForm : Page
    {
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly WorkGroupService workGroupService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;
        private readonly RoleService roleservice;
        private readonly ScopeProjectService scopeProjectService;
        private readonly DocumentService documentService;
        private readonly AreaService areaService;
        private readonly RoleService roleService;
        private readonly DisciplineService disciplineService;
        private readonly AttachFilesWorkpackageService attachFilesWorkpackageService;
        private readonly WPRevisionService wpRevisionService;
        private readonly EmailNotificationTemplateService emailNotificationTemplateService;
        private readonly DocumentPackageService documentPackageService;
        private readonly FolderService folderService = new FolderService();
        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();
        private readonly ProcessPlanedService processPlanedService = new ProcessPlanedService();
        private readonly ProcessActualService processActualService = new ProcessActualService();
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();
        private readonly DocumentPackageService documentpackageservice;
        protected const string UnreadPattern = @"\(\d+\)";

        private const string RegexValidateEmail =
            @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public WorkGroupEditForm()
        {
            this.userService = new UserService();
            this.workGroupService = new WorkGroupService();
            this.scopeProjectService = new ScopeProjectService();
            this.documentService = new DocumentService();
            this.areaService = new AreaService();
            this.roleService = new RoleService();
            this.disciplineService = new DisciplineService();
            this.attachFilesWorkpackageService = new AttachFilesWorkpackageService();
            this.wpRevisionService = new WPRevisionService();
            this.emailNotificationTemplateService = new EmailNotificationTemplateService();
            this.documentPackageService = new DocumentPackageService();
            this.roleservice = new RoleService();
            this.documentpackageservice = new DocumentPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var objWorkGroup = this.workGroupService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objWorkGroup != null)
                    {

                        this.ddlProject.SelectedValue = objWorkGroup.ProjectId.ToString();
                        this.ddlArea.SelectedValue = objWorkGroup.AreaId.ToString();
                        this.ddlDiscipline.SelectedValue = objWorkGroup.DisciplineId.ToString();
                        this.ddlDepartment.SelectedValue = objWorkGroup.DepartmentId.ToString();

                        this.txtName.Text = objWorkGroup.Name;
                        this.txtDescription.Text = objWorkGroup.Description;
                        this.ddlWorkpackageType.SelectedValue = objWorkGroup.TypeId.ToString();
                        this.cbAutoCalculate.Checked = objWorkGroup.IsAutoCalculate.GetValueOrDefault() ? true : false;
                        this.ddlRevision.SelectedValue = (objWorkGroup.RevisionId.GetValueOrDefault() + 1).ToString();

                        if (string.IsNullOrEmpty(this.Request.QueryString["type"]))
                        {
                            this.CreatedInfo.Visible = true;

                            this.txtComplete.Value = objWorkGroup.Complete;
                            this.txtWeight.Value = objWorkGroup.Weight;
                            this.cbAutoCalculate.Checked = objWorkGroup.IsAutoCalculate.GetValueOrDefault();
                            this.txtStartDate.SelectedDate = objWorkGroup.StartDate;
                            this.txtDeadline.SelectedDate = objWorkGroup.Deadline;
                            this.txtEndDate.SelectedDate = objWorkGroup.EndDate;

                            this.txtInputDataSupplier.Text = objWorkGroup.InputDataSupplier;
                            this.txtInputDocument.Text = objWorkGroup.InputDocument;
                            this.txtInfoExchange.Text = objWorkGroup.InformationExchange;
                            this.txtReportMode.Text = objWorkGroup.ReportMode;
                            this.txtIncomingNo.Text = objWorkGroup.IncomingNo;
                            this.txtOutgoingNo.Text = objWorkGroup.OutgoingNo;
                            this.ddlRevision.SelectedValue = objWorkGroup.RevisionId.GetValueOrDefault().ToString();
                            this.txtTotalManHours.Value = objWorkGroup.TotalManHours;
                            this.txtUsedManHours.Value = objWorkGroup.UsedManHours;

                            var createdUser = this.userService.GetByID(objWorkGroup.CreatedBy.GetValueOrDefault());

                            this.lblCreated.Text = "Created at " + objWorkGroup.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                            if (objWorkGroup.UpdatedBy != null && objWorkGroup.UpdatedDate != null)
                            {
                                this.lblCreated.Text += "<br/>";
                                var lastUpdatedUser = this.userService.GetByID(objWorkGroup.UpdatedBy.GetValueOrDefault());
                                this.lblUpdated.Text = "Last modified at " + objWorkGroup.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                            }
                            else
                            {
                                this.lblUpdated.Visible = false;
                            }
                        }
                        var projectobj = this.scopeProjectService.GetById(objWorkGroup.ProjectId.GetValueOrDefault());
                        if (projectobj != null)
                        {
                            //tu tinh weight WP thi ko cho nhap
                            if (projectobj.AutoCalculateWeightWorkGroup == true)
                            {
                                this.txtWeight.Enabled = false;
                            }
                            if (this.ddlDepartment.SelectedValue == "84")
                            {
                                this.pnCost.Visible = true;
                                this.txtTotalCostEstimate.Text = projectobj.TotalCostEstimate.ToString();
                                this.txtDesignCost.Text = projectobj.DesignCost.ToString();
                                this.txtProjectPlanningCost.Text = projectobj.ProjectPlanningCost.ToString();
                            }
                            else
                            {
                                this.pnCost.Visible = false;
                            }
                            if (UserSession.Current.IsCheif || UserSession.Current.IsLeader)
                            {
                                PermissionWp(false);
                            }
                            if (UserSession.Current.IsAdmin || (UserSession.Current.User.Id == projectobj.ProjectManagerId) || UserSession.Current.IsDC || UserSession.Current.IsCheif || UserSession.Current.IsLeader)
                            {
                                this.btnSave.Visible = true;
                                if (UserSession.Current.IsCheif || UserSession.Current.IsLeader)
                                {
                                    if (objWorkGroup.DepartmentId == UserSession.Current.RoleId)
                                    {
                                        this.btnSave.Visible = true;
                                    }
                                    else
                                    {
                                        this.btnSave.Visible = false;
                                    }
                                }
                            }
                            else
                            {
                                this.btnSave.Visible = false;
                            }
                            if (projectobj.AutoCalculateWeightWorkGroup == true)
                            {
                                this.txtWeight.Enabled = false;
                            }
                        }
                        //tu tinh thi ko cho nhap
                        if (objWorkGroup.IsAutoCalculate.GetValueOrDefault())
                        {
                            this.txtComplete.Enabled = false;
                            this.txtTotalManHours.Enabled = false;
                            this.txtUsedManHours.Enabled = false;
                        }
                    }
                }
                else
                {
                    if (cbAutoCalculate.Checked)
                    {
                        this.txtComplete.Enabled = false;
                        this.txtTotalManHours.Enabled = false;
                        this.txtUsedManHours.Enabled = false;
                    }
                    this.AutoGenerateName();
                    this.CreatedInfo.Visible = false;
                }

            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                int? workpackageId;
                int tempID = 0;
                const string targetFolder = "../../DocumentLibrary/WorkPackageFiles";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/"
                    ? string.Empty
                    : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/WorkPackageFiles";
                var listUpload = docuploader.UploadedFiles;
                var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));

                //if (projectObj.Deadline != null && this.txtDeadline.SelectedDate != null && this.txtDeadline.SelectedDate > projectObj.Deadline.GetValueOrDefault())
                //{
                //    this.blockError.Visible = true;
                //    this.lblError.Text = "Deadline of Workpackage Greater than deadline of Project. please check it again.";
                //    return;
                //}
                var obj = new WorkGroup();
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]) && string.IsNullOrEmpty(this.Request.QueryString["type"]))
                {
                    workpackageId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    obj = this.workGroupService.GetById(workpackageId.GetValueOrDefault());
                    if (obj != null)
                    {
                        var isUpdatePrjectInfo = obj.ProjectId.GetValueOrDefault().ToString() != this.ddlProject.SelectedValue;
                        var oldValidWP = Utilities.Utility.RemoveSpecialCharacter(obj.Name);
                        var oldValidWPName = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name);

                        this.CollectData(ref obj, false);

                        obj.UpdatedBy = UserSession.Current.User.Id;
                        obj.UpdatedDate = DateTime.Now;
                        var validWP = Utilities.Utility.RemoveSpecialCharacter(this.txtName.Text.Trim());
                        var validWPName = Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim()).Replace('&', '-');

                        if (projectObj.PathLatestRevision != null && !string.IsNullOrEmpty(projectObj.PathLatestRevision) && projectObj.PathLatestRevision.Contains("DocumentLibrary/Latest Revision") && validWPName != oldValidWPName && oldValidWP != validWP)
                        {
                            ChangeNameWP(projectObj, obj, oldValidWP, oldValidWPName, validWP, validWPName);
                        }
                        this.workGroupService.Update(obj);
                        if (listUpload.Count > 0)
                        {
                            foreach (UploadedFile docFile in listUpload)
                            {
                                var docFileName = docFile.FileName;

                                var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;

                                // Path file to save on server disc
                                var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);

                                // Path file to download from server
                                var serverFilePath = serverFolder + "/" + serverDocFileName;
                                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1,
                                    docFileName.Length - docFileName.LastIndexOf(".") - 1);

                                docFile.SaveAs(saveFilePath, true);

                                var attachFile = new AttachFilesWorkpackage()
                                {
                                    WorkpackageId = workpackageId,
                                    FileName = docFileName,
                                    Extension = fileExt,
                                    FilePath = serverFilePath,
                                    ExtensionIcon =
                                        Utility.FileIcon.ContainsKey(fileExt.ToLower())
                                            ? Utility.FileIcon[fileExt.ToLower()]
                                            : "~/images/otherfile.png",
                                    FileSize = (double)docFile.ContentLength / 1024,
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedDate = DateTime.Now
                                };

                                this.attachFilesWorkpackageService.Insert(attachFile);
                                obj.DefaultFilePath = serverFilePath;
                                obj.FileExtentionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower())
                                        ? Utility.FileIcon[fileExt.ToLower()]
                                        : "~/images/otherfile.png";
                                this.workGroupService.Update(obj);
                            }
                        }

                        projectObj = this.scopeProjectService.GetById(obj.ProjectId.GetValueOrDefault());
                        if (projectObj != null)
                        {
                            if (isUpdatePrjectInfo)
                            {
                                // Update project info for document of workpackage
                                var docList = this.documentPackageService.GetAllByWorkgroup(obj.ID);
                                foreach (var documentPackage in docList)
                                {
                                    documentPackage.ProjectId = obj.ProjectId;
                                    documentPackage.ProjectName = obj.ProjectName;
                                    documentPackage.WorkgroupName = obj.Name;
                                    documentPackage.WorkgroupFullName = obj.FullName;
                                    documentPackage.WorkgroupRevName = obj.FullNameRev;

                                    this.documentPackageService.Update(documentPackage);
                                }
                            }

                            // Send notification
                            var Notify = this.roleService.GetByID(obj.DepartmentId.GetValueOrDefault());
                            if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                            {
                                if (UserSession.Current.User.IsGip.GetValueOrDefault())
                                {
                                    this.NotificationUpdateWp(obj);
                                }
                            }
                        }
                    }
                }
                else
                {
                    obj = new WorkGroup();
                    if (this.workGroupService.IsExist(this.txtName.Text.Trim(), 0))
                    {
                        this.fileNameValidator.ErrorMessage = "Workpackage is already exist.";
                        this.divFileName.Style["margin-bottom"] = "-26px;";
                        return;
                    }
                    this.CollectData(ref obj, true);
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    obj.CanDelete = true;
                    var projectFolder = projectObj.PathLatestRevision != null && !string.IsNullOrEmpty(projectObj.PathLatestRevision) ? Server.MapPath("../.." + projectObj.PathLatestRevision) : string.Empty;

                    if (projectObj.PathLatestRevision != null && !string.IsNullOrEmpty(projectObj.PathLatestRevision) && projectObj.PathLatestRevision.Contains("DocumentLibrary/Latest Revision") && Directory.Exists(projectFolder) && !string.IsNullOrEmpty(projectFolder))
                    {
                        var physicalPath = projectFolder + @"/" + Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name.Replace("&", "-"));
                        Directory.CreateDirectory(physicalPath);
                        obj.PathLatestRevision = projectObj.PathLatestRevision + "/" + Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim()).Replace('&', '-');
                    }
                    workpackageId = this.workGroupService.Insert(obj);
                    if (listUpload.Count > 0)
                    {
                        foreach (UploadedFile docFile in listUpload)
                        {
                            var docFileName = docFile.FileName;
                            var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;
                            // Path file to save on server disc
                            var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                            // Path file to download from server
                            var serverFilePath = serverFolder + "/" + serverDocFileName;
                            var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1,
                                docFileName.Length - docFileName.LastIndexOf(".") - 1);
                            docFile.SaveAs(saveFilePath, true);
                            var attachFile = new AttachFilesWorkpackage()
                            {
                                WorkpackageId = workpackageId,
                                FileName = docFileName,
                                Extension = fileExt,
                                FilePath = serverFilePath,
                                ExtensionIcon =
                                    Utility.FileIcon.ContainsKey(fileExt.ToLower())
                                        ? Utility.FileIcon[fileExt.ToLower()]
                                        : "~/images/otherfile.png",
                                FileSize = (double)docFile.ContentLength / 1024,
                                CreatedBy = UserSession.Current.User.Id,
                                CreatedDate = DateTime.Now
                            };

                            tempID = this.attachFilesWorkpackageService.Insert(attachFile);
                            obj.DefaultFilePath = serverFilePath;
                            obj.FileExtentionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower())
                                        ? Utility.FileIcon[fileExt.ToLower()]
                                        : "~/images/otherfile.png";
                            this.workGroupService.Update(obj);
                        }
                        // gui mail tao moi khi co file dinh kem
                        var Notify = this.roleService.GetByID(obj.DepartmentId.GetValueOrDefault());
                        if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                        {
                            if (tempID != 0 && UserSession.Current.User.IsGip.GetValueOrDefault())
                            {
                                this.NotificationAddNewWp(obj);
                            }
                        }
                    }
                    //tao folder library
                    if (workpackageId != null && obj.DepartmentId != null && obj.DepartmentId.GetValueOrDefault() != 0)
                    {
                        var folderparent = this.folderService.GetAllByName(obj.ProjectName);
                        if (folderparent != null)
                        {
                            //var listuserwp = this.userService.GetAllByRoleId(obj.DepartmentId.GetValueOrDefault());
                            //foreach (var user in listuserwp)
                            //{
                            //    var childPermission = new UserDataPermission()
                            //    {
                            //        RoleId = user.RoleId,
                            //        FolderId = folderparent.ID,
                            //        UserId = user.Id,
                            //        IsFullPermission = false,
                            //        CreatedDate = DateTime.Now,
                            //        CreatedBy = UserSession.Current.User.Id
                            //    };
                            //    this.userDataPermissionService.Insert(childPermission);
                            //}
                            var folderwp = creterfolder(obj.Name, folderparent, obj);
                            //PDF
                            var pdf = creterfolder("PDF", folderwp, obj);
                            //Native
                            var native = creterfolder("Native", folderwp, obj);
                        }
                    }
                    // Update weight for list workgroup have same name
                    // Get only previous WP, without current wp
                    var wpSameName = this.workGroupService.GetAllByName(obj.Name).Where(t => t.ID != workpackageId).ToList();
                    if (wpSameName.Count > 0)
                    {
                        var weight = 0.0;
                        weight = (wpSameName.Aggregate(weight, (current, t) => current + t.Weight.GetValueOrDefault())) / (wpSameName.Count + 1);

                        foreach (var workGroup in wpSameName)
                        {
                            workGroup.Weight = weight;
                            this.workGroupService.Update(workGroup);
                        }

                        obj.Weight = weight;
                        this.workGroupService.Update(obj);
                    }

                    // Update total WP count of department
                    var departmentObj = this.roleService.GetByID(Convert.ToInt32(this.ddlDepartment.SelectedItem != null
                                                                            ? this.ddlDepartment.SelectedValue
                                                                            : "0"));
                    if (departmentObj != null && workpackageId != null)
                    {
                        departmentObj.CurrentWorkpackageCount += 1;
                        this.roleService.Update(departmentObj);
                    }

                    //tu dong ve progressPlan
                    DrawProgressPlan(obj);
                }
                // Re-calculate weight value for Project
                projectObj = this.scopeProjectService.GetById(obj.ProjectId.GetValueOrDefault());
                var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                if (obj != null)
                {
                    if (obj.IsAutoCalculate.GetValueOrDefault())
                    {
                        var docList = this.documentPackageService.GetAllEMDRByWorkgroup(obj.ID);
                        if (docList.Count > 0)
                        {
                            var complete = docList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                            complete = Math.Round(complete, 5);
                            if (complete == 100 || !docList.Where(t => t.Complete < 100).Any())
                            {
                                obj.EndDate = DateTime.Now;
                                obj.Complete = 100;
                            }
                            else
                            {
                                obj.Complete = complete;
                            }
                            obj.TotalManHours = docList.Sum(t => t.ManHourPlan) != 0 ? docList.Sum(t => t.ManHourPlan) : 0;
                            obj.UsedManHours = docList.Sum(t => t.ManHours) != 0 ? docList.Sum(t => t.ManHours) : 0;
                        }
                    }
                    //Insert new Process actual if does not exist
                    var processActualObj = processActualService.GetByProjectAndWorkgroup(projectObj.ID, obj.ID);
                    if (processActualObj == null)
                    {
                        ProcessActual newProcessActual = new ProcessActual();
                        newProcessActual.ProjectId = projectObj.ID;
                        newProcessActual.WorkgroupId = obj.ID;
                        newProcessActual.Actual = "0";
                        newProcessActual.ActualMonth = "0";
                        processActualService.Insert(newProcessActual);
                    }
                    //Update name and weight workgroup manhour month
                    var wmmListInMonth = this.workgroupManhourMonthService.GetByPJ(projectObj.ID, DateTime.Now.Month, DateTime.Now.Year);
                    foreach (var wmmItem in wmmListInMonth)
                    {
                        var wpObj = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                        if (wpObj != null)
                        {
                            wmmItem.WorkgroupName = wpObj.Name;
                            wmmItem.WorkgroupWeight = wpObj.Weight;
                            this.workgroupManhourMonthService.Update(wmmItem);
                        }
                    }
                }
                if (projectObj != null)
                {
                    if (wpList.Count > 0)
                    {
                        projectObj.CanDelete = false;
                    }

                    if (projectObj.IsAutoCalculate.GetValueOrDefault())
                    {
                        var complete = 0.0;
                        complete = wpList.Aggregate(complete,
                            (current, t) => (current + ((t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100)));
                        projectObj.Complete = complete > 100 ? 100 : complete;
                    }

                    // Update total workpackage weight for Project
                    var totalWeight = 0.0;
                    totalWeight = wpList.Sum(t => t.Weight.GetValueOrDefault());
                    projectObj.TotalWorkpackageWeight = totalWeight > 100 ? 100 : totalWeight;

                    this.scopeProjectService.Update(projectObj);

                    //cal auto weight WP
                    if (projectObj.AutoCalculateWeightWorkGroup == true)
                    {
                        var sumTotalManHours = 0.0;
                        sumTotalManHours = wpList.Sum(t => t.TotalManHours.GetValueOrDefault());
                        foreach (var item in wpList)
                        {
                            var calWeight = ((item.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) > 0 ? ((item.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) : 0;
                            item.Weight = calWeight > 100 ? 100 : calWeight;
                            this.workGroupService.Update(item);
                        }
                    }
                }

                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }

        private void ChangeNameWP(ScopeProject projectObj, WorkGroup obj, string oldValidWP, string oldValidWPName, string validWP, string validWPName)
        {
            //var projectFolder = projectObj.PathLatestRevision != null && !string.IsNullOrEmpty(projectObj.PathLatestRevision) ? Server.MapPath("../.." + projectObj.PathLatestRevision) : string.Empty;
            //if (Directory.Exists(projectFolder + "\\" + oldValidWPName))
            //{
            //    Directory.Move(projectFolder + "\\" + oldValidWPName,
            //                     projectFolder + "\\" + validWPName);
            //}

            //obj.PathLatestRevision = projectObj.PathLatestRevision + "/" + Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim()).Replace('&', '-');

            var folderRoot = this.folderService.GetAllByName(oldValidWPName);
            if (folderRoot != null)
            {
                folderRoot.Name = validWPName;
                folderRoot.Description = validWPName;
                this.folderService.Update(folderRoot);
                //var oldDirName = folderRoot.DirName;
                //try
                //{
                //    var folderChildWP = this.folderService.GetByContainDirName(oldDirName);
                //    if (folderChildWP.Count > 0)
                //    {
                //        foreach (var folObj in folderChildWP)
                //        {
                //            var Place = folObj.DirName.IndexOf(oldValidWP);
                //            if (Place != -1)
                //            {
                //                folObj.DirName = folObj.DirName.Remove(Place, oldValidWP.Length).Insert(Place, validWP);
                //                this.folderService.Update(folObj);
                //            }
                //        }
                //    }

                //    var docWP = this.documentService.GetAllByFolder(folderChildWP.Select(t => Convert.ToInt32(t.ID)).ToList<int>());
                //    if (docWP.Count > 0)
                //    {
                //        foreach (var docObj in docWP)
                //        {
                //            var Place = docObj.FilePath.IndexOf(oldValidWP);
                //            if (Place != -1)
                //            {
                //                docObj.FilePath = docObj.FilePath.Remove(Place, oldValidWP.Length).Insert(Place, validWP);
                //                this.documentService.Update(docObj);
                //            }
                //        }
                //    }

                //    var docPKWP = this.documentPackageService.GetAllByWorkgroup(obj.ID);
                //    if (docPKWP.Count > 0)
                //    {
                //        foreach (var docPKObj in docPKWP)
                //        {
                //            docPKObj.WorkgroupName = obj.Name;
                //            docPKObj.WorkgroupFullName = obj.FullName;
                //            docPKObj.WorkgroupRevName = obj.FullNameRev;
                //            this.documentPackageService.Update(docPKObj);
                //        }
                //    }

                //    folderRoot.Name = validWPName;
                //    folderRoot.Description = validWPName;
                //    this.folderService.Update(folderRoot);
                //    var newpath = Server.MapPath(folderRoot.DirName.Replace(oldValidWP, validWP));
                //    Directory.Move(Server.MapPath(oldDirName), newpath);
                //}
                //catch (Exception ex)
                //{ }
            }
        }

        private void DrawProgressPlan(WorkGroup wpObj)
        {
            var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
            var numberofWeek = 0;
            var numberofMonth = 0;
            for (var j = GetSaturdayOfWeek(projectObj.StartDate.GetValueOrDefault());
                j < projectObj.Deadline.GetValueOrDefault();
                j = j.AddDays(7))
            {
                numberofWeek += 1;
            }
            var currentMonth = 0;
            for (var j = GetSaturdayOfWeek(projectObj.StartDate.GetValueOrDefault());
                        j < projectObj.Deadline.GetValueOrDefault();
                        j = j.AddDays(7))
            {
                if (currentMonth != j.Month)
                {
                    currentMonth = j.Month;
                    numberofMonth += 1;
                }
            }
            var avgWeek = Math.Round((decimal)(100.0 / numberofWeek), 2);
            var avgMonth = Math.Round((decimal)(100.0 / numberofMonth), 2);
            var progressPlanedWeekList = new List<string>();
            var progressPlanedMonthList = new List<string>();
            decimal tempValue = 0;
            for (int i = 0; i < numberofWeek; i++)
            {
                tempValue += avgWeek;
                if (i == (numberofWeek - 1))
                {
                    progressPlanedWeekList.Add("100");
                }
                else
                {
                    progressPlanedWeekList.Add(tempValue.ToString());
                }
            }
            tempValue = 0;
            for (int i = 0; i < numberofMonth; i++)
            {
                tempValue += avgMonth;
                if (i == (numberofMonth - 1))
                {
                    progressPlanedMonthList.Add("100");
                }
                else
                {
                    progressPlanedMonthList.Add(tempValue.ToString());
                }
            }
            var progressPlaned = new ProcessPlaned();
            progressPlaned.ProjectId = wpObj.ProjectId;
            progressPlaned.WorkgroupId = wpObj.ID;
            progressPlaned.Planed = string.Join("$", progressPlanedWeekList);
            progressPlaned.PlanedMonth = string.Join("$", progressPlanedMonthList);

            this.processPlanedService.Insert(progressPlaned);

            //Insert new workgroup manhour month
            int countMonthPlan = 0;
            foreach (var progressItem in progressPlanedMonthList)
            {
                var date = projectObj.StartDate.GetValueOrDefault();
                var currentDate = date.AddMonths(countMonthPlan);
                var month = currentDate.Month;
                var year = currentDate.Year;
                int prevMonth = currentDate.AddMonths(-1).Month;
                int prevYear = currentDate.AddMonths(-1).Year;
                var wmmObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), month, year);
                var prevWMMObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), prevMonth, prevYear);
                if (wmmObj != null)
                {
                    wmmObj.Month = month;
                    wmmObj.Year = year;
                    wmmObj.WorkgroupID = wpObj.ID;
                    wmmObj.WorkgroupName = wpObj.Name;
                    wmmObj.ProjectID = projectObj.ID;
                    wmmObj.DeparmentID = wpObj.DepartmentId;
                    wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                    wmmObj.CompletePlanTotal = Convert.ToDouble(progressItem);
                    if (prevWMMObj != null)
                    {
                        wmmObj.CompletePlanMonth = wmmObj.CompletePlanTotal.GetValueOrDefault() - prevWMMObj.CompletePlanTotal.GetValueOrDefault();
                    }
                    else
                    {
                        wmmObj.CompletePlanMonth = wmmObj.CompletePlanTotal.GetValueOrDefault();
                    }
                    wmmObj.UpdateBy = UserSession.Current.User.Id;
                    wmmObj.UpdateDate = DateTime.Now;
                    this.workgroupManhourMonthService.Update(wmmObj);
                }
                else
                {
                    WorkgroupManhourMonth newWMMObj = new WorkgroupManhourMonth();
                    newWMMObj.Month = month;
                    newWMMObj.Year = year;
                    newWMMObj.WorkgroupID = wpObj.ID;
                    newWMMObj.WorkgroupName = wpObj.Name;
                    newWMMObj.DeparmentID = wpObj.DepartmentId;
                    newWMMObj.ProjectID = wpObj.ProjectId.GetValueOrDefault();
                    newWMMObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                    newWMMObj.CompletePlanTotal = Convert.ToDouble(progressItem);
                    if (prevWMMObj != null)
                    {
                        newWMMObj.CompletePlanMonth = newWMMObj.CompletePlanTotal.GetValueOrDefault() - prevWMMObj.CompletePlanTotal.GetValueOrDefault();
                    }
                    else
                    {
                        newWMMObj.CompletePlanMonth = newWMMObj.CompletePlanTotal.GetValueOrDefault();
                    }
                    newWMMObj.CreateBy = UserSession.Current.User.Id;
                    newWMMObj.CreateDate = DateTime.Now;
                    this.workgroupManhourMonthService.Insert(newWMMObj);
                }
                countMonthPlan++;
            }
        }

        public DateTime GetMondayOfWeek(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        public DateTime GetSaturdayOfWeek(DateTime date)
        {
            return GetMondayOfWeek(date).AddDays(4);
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if (this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Workpackage number.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else
            {
                var workpackageId = !string.IsNullOrEmpty(this.Request.QueryString["disId"])
                   ? Convert.ToInt32(this.Request.QueryString["disId"])
                   : 0;

                if (this.workGroupService.IsExist(this.txtName.Text.Trim(), workpackageId))
                {
                    this.fileNameValidator.ErrorMessage = "Workpackage is already exist.";
                    this.divFileName.Style["margin-bottom"] = "-26px;";
                    args.IsValid = false;
                }
            }
        }

        protected void btnAutoGenerate_Click(object sender, ImageClickEventArgs e)
        {
            this.AutoGenerateName();
        }

        protected void ddlProject_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.AutoGenerateName();
        }

        protected void ddlArea_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.AutoGenerateName();
        }

        protected void ddlDiscipline_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var disObj = this.disciplineService.GetById(Convert.ToInt32(this.ddlDiscipline.SelectedValue));
            if (disObj != null && disObj.DefaultDepartmentId != null && disObj.DefaultDepartmentId != 0)
            {
                this.ddlDepartment.SelectedValue = disObj.DefaultDepartmentId.Value.ToString();
            }

            this.AutoGenerateName();
        }

        protected void ddlDepartment_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            // this.AutoGenerateName();
        }
        private void CollectData(ref WorkGroup obj, bool isinsert)
        {
            var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
            var areaObj = this.areaService.GetById(Convert.ToInt32(this.ddlArea.SelectedValue));
            var disciplineObj = this.disciplineService.GetById(Convert.ToInt32(this.ddlDiscipline.SelectedValue));
            var departmentObj = this.roleService.GetByID(Convert.ToInt32(this.ddlDepartment.SelectedItem != null
                                                                            ? this.ddlDepartment.SelectedValue
                                                                            : "0"));

            obj.ProjectId = projectObj != null ? projectObj.ID : 0;
            obj.ProjectName = projectObj != null ? projectObj.Name : string.Empty;
            obj.ProjectManagerID = projectObj != null ? projectObj.ProjectManagerId : 0;
            obj.ProjectManagerFullName = projectObj != null ? projectObj.ProjectManagerFullName : string.Empty;

            double tempTotalCostEstimate;
            if (double.TryParse(this.txtTotalCostEstimate.Text.Trim(), out tempTotalCostEstimate))
            {
                projectObj.TotalCostEstimate = tempTotalCostEstimate;
            }
            double tempDesignCost;
            if (double.TryParse(this.txtDesignCost.Text.Trim(), out tempDesignCost))
            {
                projectObj.DesignCost = tempDesignCost;
            }
            double tempProjectPlanningCost;
            if (double.TryParse(this.txtProjectPlanningCost.Text.Trim(), out tempProjectPlanningCost))
            {
                projectObj.ProjectPlanningCost = tempProjectPlanningCost;
            }
            this.scopeProjectService.Update(projectObj);

            obj.AreaId = areaObj != null ? areaObj.ID : 0;
            obj.AreaName = areaObj != null ? areaObj.Name : string.Empty;

            obj.DisciplineId = disciplineObj != null ? disciplineObj.ID : 0;
            obj.DisciplineName = disciplineObj != null ? disciplineObj.Name : string.Empty;

            obj.DepartmentId = departmentObj != null ? departmentObj.Id : 0;
            obj.DepartmentName = departmentObj != null ? departmentObj.Name : string.Empty;

            obj.Name = this.txtName.Text.Trim();
            obj.Description = this.txtDescription.Text.Trim();

            obj.TypeId = Convert.ToInt32(this.ddlWorkpackageType.SelectedValue);
            obj.TypeName = this.ddlWorkpackageType.SelectedItem.Text;

            if (!this.cbAutoCalculate.Checked)
            {
                obj.Complete = this.txtComplete.Value ?? 0;
                obj.TotalManHours = this.txtTotalManHours.Value ?? 0;
                obj.UsedManHours = this.txtUsedManHours.Value ?? 0;
            }
            obj.IsAutoCalculate = this.cbAutoCalculate.Checked;

            obj.StartDate = this.txtStartDate.SelectedDate;
            obj.Deadline = this.txtDeadline.SelectedDate;
            obj.EndDate = this.txtEndDate.SelectedDate;

            obj.InputDataSupplier = this.txtInputDataSupplier.Text.Trim();
            obj.InputDocument = this.txtInputDocument.Text.Trim();
            obj.InformationExchange = this.txtInfoExchange.Text.Trim();
            obj.ReportMode = this.txtReportMode.Text.Trim();
            obj.IncomingNo = this.txtIncomingNo.Text.Trim();
            obj.OutgoingNo = this.txtOutgoingNo.Text.Trim();

            obj.RevisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
            obj.RevisionName = this.ddlRevision.SelectedItem.Text;
            if (projectObj.AutoCalculateWeightWorkGroup != true)
            {
                obj.Weight = this.txtWeight.Value ?? 0;
            }
        }

        private void LoadComboData()
        {
            var projectList = this.scopeProjectService.GetAll();
            projectList.Insert(0, new ScopeProject() { ID = 0 });
            this.ddlProject.DataSource = projectList;
            this.ddlProject.DataTextField = "FullName";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();

            if (!string.IsNullOrEmpty(this.Request.QueryString["projectId"]))
            {
                this.ddlProject.SelectedValue = this.Request.QueryString["projectId"];
            }

            var areaList = this.areaService.GetAll().OrderBy(t => t.Name).ToList();
            areaList.Insert(0, new Area() { ID = 0 });
            this.ddlArea.DataSource = areaList;
            this.ddlArea.DataTextField = "FullName";
            this.ddlArea.DataValueField = "ID";
            this.ddlArea.DataBind();

            var disciplineList = new List<Discipline>();
            if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
            {
                disciplineList = this.disciplineService.GetAllView().OrderBy(t => t.Name).ToList();
            }
            else
            {
                disciplineList = this.disciplineService.GetAll().OrderBy(t => t.Name).ToList();
            }
            disciplineList.Insert(0, new Discipline() { ID = 0 });
            this.ddlDiscipline.DataSource = disciplineList;
            this.ddlDiscipline.DataTextField = "FullName";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataBind();

            var departmentList = this.roleService.GetAll(UserSession.Current.User.Id == 1).ToList();

            if (UserSession.Current.IsCheif)
            {
                departmentList = departmentList.Where(t => t.Id == UserSession.Current.RoleId).ToList();
            }

            departmentList.Insert(0, new Role { Id = 0 });
            this.ddlDepartment.DataSource = departmentList;
            this.ddlDepartment.DataTextField = "FullName";
            this.ddlDepartment.DataValueField = "Id";
            this.ddlDepartment.DataBind();

            this.ddlWorkpackageType.SelectedValue = "1";

            var listRevision = this.wpRevisionService.GetAll();//.OrderBy(t => t.Name).ToList();
            listRevision.Insert(0, new WPRevision { ID = 0 });
            this.ddlRevision.DataSource = listRevision;
            this.ddlRevision.DataTextField = "Name";
            this.ddlRevision.DataValueField = "ID";
            this.ddlRevision.DataBind();
        }

        private void AutoGenerateName()
        {
            var departmentObj = this.roleService.GetByID(Convert.ToInt32(this.ddlDepartment.SelectedItem != null
                                                                                ? this.ddlDepartment.SelectedValue
                                                                                : "0"));
            if (this.ddlProject.SelectedValue != "0")
            {
                var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
                if (projectObj != null)
                {
                    if (projectObj.AutoCalculateWeightWorkGroup == true)
                    {
                        this.txtWeight.Enabled = false;
                    }
                }
            }

            if (this.ddlProject.SelectedValue != "0" &&
            this.ddlArea.SelectedValue != "0" &&
            this.ddlDiscipline.SelectedValue != "0")
            {
                var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
                var areaObj = this.areaService.GetById(Convert.ToInt32(this.ddlArea.SelectedValue));
                var disciplineObj = this.disciplineService.GetById(Convert.ToInt32(this.ddlDiscipline.SelectedValue));
                this.txtName.Text = projectObj != null && areaObj != null && disciplineObj != null
                    ? projectObj.Name + "-" + areaObj.Name + "-" + disciplineObj.Name
                    : string.Empty;
            }
            else
            {
                this.txtName.Text = departmentObj != null
                        ? departmentObj.Name + "-" + departmentObj.CurrentWorkpackageCount
                        : string.Empty;
            }
        }
        protected void cbAutoCalculate_OnCheckedChanged(object sender, EventArgs e)
        {
            this.txtComplete.Enabled = !this.cbAutoCalculate.Checked;
            this.txtTotalManHours.Enabled = !this.cbAutoCalculate.Checked;
            this.txtUsedManHours.Enabled = !this.cbAutoCalculate.Checked;
        }

        private void NotificationAddNewWp(WorkGroup wpObj)
        {
            if (wpObj.DepartmentId != 0 && wpObj.Complete.GetValueOrDefault() < 100)
            {
                List<User> userDPList = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault());
                var emailList = userDPList.Where(t => t.IsChief.GetValueOrDefault() || t.IsLeader.GetValueOrDefault())
                                .Select(t => t.Email)
                                .Where(t => !string.IsNullOrEmpty(t)).ToList();

                //var leader = userDPList.Where(t => t.IsLeader.GetValueOrDefault() && wpObj.DisciplineId == t.DisciplineID)
                //             .Select(t => t.Email)
                //             .Where(t => !string.IsNullOrEmpty(t)).ToList();

                var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.CreateNewWP);
                if (emailList.Count > 0 && notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                {
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };

                    var wpCreatedUser = this.userService.GetByID(wpObj.CreatedBy.GetValueOrDefault());
                    var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                    var subject = notificationTemplate.Subject.Replace("#WPNumber#", wpObj.Name).Replace("#WPDepartment#", wpObj.DepartmentName).Replace("#WPCreatedUser#", wpCreatedUser != null ? wpCreatedUser.FullName : string.Empty);

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    message.Body = notificationTemplate.Contents
                        .Replace("#WPNumber#", wpObj.Name)
                        .Replace("#WPDepartment#", wpObj.DepartmentName)
                        .Replace("#WPProjectNumber#", projectObj != null ? projectObj.Name : string.Empty)
                        .Replace("#WPProjectName#", projectObj != null ? projectObj.Description : string.Empty)
                        .Replace("#WPName#", wpObj.Description)
                        .Replace("#WPStartDate#", wpObj.StartDate != null ? wpObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPDeadline#", wpObj.Deadline != null ? wpObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPFinishDate#", wpObj.EndDate != null ? wpObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPIncomingNo#", wpObj.IncomingNo)
                        .Replace("#WPOutgoingNo#", wpObj.OutgoingNo)
                        .Replace("#WPWeight#", wpObj.Weight != null ? wpObj.Weight.Value.ToString() : string.Empty)
                        .Replace("#WPComplete#", wpObj.Complete != null ? wpObj.Complete.Value.ToString() : string.Empty);

                    //5 user 2in1 role chief and GIP
                    List<int> userSpecial = ConfigurationManager.AppSettings.Get("ChiefandGIP").Split(',').Select(Int32.Parse).ToList();
                    var specialList = userDPList.Where(t => userSpecial.Contains(t.Id))
                        .Select(t => t.Email)
                        .Where(t => !string.IsNullOrEmpty(t)).ToList();
                    foreach (var to in specialList)
                    {
                        message.To.Add(new MailAddress(to));
                    }

                    foreach (var to in emailList)
                    {
                        message.To.Add(new MailAddress(to));
                    }

                    //foreach (var to in leader)
                    //{
                    //    message.CC.Add(new MailAddress(to));
                    //}
                    var userdc = this.userService.GetByID(projectObj.DocumentControlId.GetValueOrDefault());
                    if (userdc != null && !string.IsNullOrEmpty(userdc.Email))
                    {
                        message.CC.Add(new MailAddress(userdc.Email));
                    }
                    else
                    {
                        var userdclist = this.userService.GetAll(false).Where(t => t.IsDC.GetValueOrDefault() && !string.IsNullOrEmpty(t.Email)).ToList();
                        foreach (var item in userdclist)
                        {
                            message.CC.Add(new MailAddress(item.Email));
                        }
                    }
                    //message.To.Add(new MailAddress("edms.cd@vietsov.com.vn"));
                    smtpClient.Send(message);
                }
            }
        }

        private void NotificationUpdateWp(WorkGroup wpObj)
        {
            if (wpObj.DepartmentId != 0)
            {
                var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                var gipUser = this.userService.GetByID(projectObj != null ? projectObj.ProjectManagerId.GetValueOrDefault() : 0);
                List<User> userDPList = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault());
                var emailList = userDPList.Where(t => t.IsChief.GetValueOrDefault() || t.IsLeader.GetValueOrDefault())
                                .Select(t => t.Email)
                                .Where(t => !string.IsNullOrEmpty(t)).ToList();

                var leader = userDPList.Where(t => t.IsLeader.GetValueOrDefault() && wpObj.DisciplineId == t.DisciplineID)
                             .Select(t => t.Email)
                             .Where(t => !string.IsNullOrEmpty(t)).ToList();

                if (gipUser != null && !string.IsNullOrEmpty(gipUser.Email))
                {
                    emailList.Add(gipUser.Email);
                }

                var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.UpdateWP);
                if (emailList.Count > 0 && notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                {
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };

                    var wpUpdatedUser = this.userService.GetByID(wpObj.UpdatedBy.GetValueOrDefault());
                    var subject = notificationTemplate.Subject.Replace("#WPName#", wpObj.Name)
                        .Replace("#WPUpdatedUser#", wpUpdatedUser != null ? wpUpdatedUser.FullName : string.Empty);

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    message.Body = notificationTemplate.Contents
                        .Replace("#WPNumber#", wpObj.Name)
                        .Replace("#WPUpdatedUser#", wpUpdatedUser != null ? wpUpdatedUser.FullName : string.Empty)
                        .Replace("#WPProjectNumber#", projectObj != null ? projectObj.Name : string.Empty)
                        .Replace("#WPProjectName#", projectObj != null ? projectObj.Description : string.Empty)
                        .Replace("#WPName#", wpObj.Description)
                        .Replace("#WPStartDate#", wpObj.StartDate != null ? wpObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPDeadline#", wpObj.Deadline != null ? wpObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPFinishDate#", wpObj.EndDate != null ? wpObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPIncomingNo#", wpObj.IncomingNo)
                        .Replace("#WPOutgoingNo#", wpObj.OutgoingNo)
                        .Replace("#WPWeight#", wpObj.Weight != null ? wpObj.Weight.Value.ToString() : string.Empty)
                        .Replace("#WPComplete#", wpObj.Complete != null ? wpObj.Complete.Value.ToString() : string.Empty);

                    //5 user 2in1 role chief and GIP
                    List<int> userSpecial = ConfigurationManager.AppSettings.Get("ChiefandGIP").Split(',').Select(Int32.Parse).ToList();
                    var specialList = userDPList.Where(t => userSpecial.Contains(t.Id))
                        .Select(t => t.Email)
                        .Where(t => !string.IsNullOrEmpty(t)).ToList();
                    foreach (var to in specialList)
                    {
                        message.To.Add(new MailAddress(to));
                    }

                    foreach (var to in emailList)
                    {
                        message.To.Add(new MailAddress(to));
                    }

                    foreach (var to in leader)
                    {
                        message.CC.Add(new MailAddress(to));
                    }

                    var userdc = this.userService.GetByID(projectObj.DocumentControlId.GetValueOrDefault());
                    if (userdc != null && !string.IsNullOrEmpty(userdc.Email))
                    {
                        message.CC.Add(new MailAddress(userdc.Email));
                    }

                    message.Attachments.Add(new Attachment(Server.MapPath(wpObj.DefaultFilePath)));

                    //message.To.Add(new MailAddress("edms.cd@vietsov.com.vn"));

                    smtpClient.Send(message);
                }
            }
        }

        protected void txtTotalManHours_OnTextChanged(object sender, EventArgs e)
        {
            this.txtUsedManHours.Value = this.txtTotalManHours.Value.GetValueOrDefault() * this.txtComplete.Value.GetValueOrDefault() / 100;
        }

        protected void txtComplete_OnTextChanged(object sender, EventArgs e)
        {
            this.txtUsedManHours.Value = this.txtTotalManHours.Value.GetValueOrDefault() * this.txtComplete.Value.GetValueOrDefault() / 100;
        }

        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"~/Images/project.png";
        }

        protected void DescriptionValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {

            if (this.txtDescription.Text.Trim().Length == 0)
            {
                this.DescriptionValidator.ErrorMessage = "Please enter Workpackage name.";
                this.DescriptionValidator.Style["margin-bottom"] = "5px;";
                this.DescriptionValidator.Style["margin-top"] = "124px;";
                args.IsValid = false;
            }
        }


        public void PermissionWp(bool flag)
        {
            this.ddlProject.Enabled = flag;
            this.ddlArea.Enabled = flag;
            this.ddlDiscipline.Enabled = flag;
            this.ddlDepartment.Enabled = flag;
            this.txtName.Enabled = flag;
            this.txtDescription.Enabled = flag;
            this.txtComplete.Enabled = flag;
            this.txtWeight.Enabled = flag;
            this.ddlWorkpackageType.Enabled = flag;
            this.txtTotalManHours.Enabled = flag;
            this.txtUsedManHours.Enabled = flag;
            this.txtStartDate.Enabled = flag;
            this.txtDeadline.Enabled = flag;
            //this.txtEndDate.Enabled = flag;
            this.cbAutoCalculate.Enabled = flag;
        }

        private Folder creterfolder(string name, Folder folder, WorkGroup wp)
        {

            var trans = new Folder()
            {
                Name = name,
                Description = name,
                ParentID = folder.ID,
                DirName = folder.DirName + "/" + Regex.Replace(name, @"[^0-9a-zA-Z]+", string.Empty),
                ProjectId = wp.ProjectId,
                WorkgroupID = wp.ID,
                CreatedBy = UserSession.Current.User.Id,
                CreatedDate = DateTime.Now
            };
            Directory.CreateDirectory(Server.MapPath(trans.DirName));
            var folderId = this.folderService.Insert(trans);
            var AllUser = this.userService.GetAll(false).Where(t => !t.IsDC.GetValueOrDefault());
            var obj = this.scopeProjectService.GetById(wp.ProjectId.GetValueOrDefault());

            var addingPermission = AllUser.Where(t => t.RoleId == wp.DepartmentId.GetValueOrDefault() || t.Id == obj.ProjectManagerId || t.Id == obj.SupervisorId).Select(t => new UserDataPermission()
            {
                ProjectId= wp.ProjectId,
                RoleId = t.RoleId,
                FolderId = folderId,
                UserId = t.Id,
                IsFullPermission = true,
                CreatedDate = DateTime.Now,
                CreatedBy = UserSession.Current.User.Id,
                OnlyView = false
            });
            this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            if (name != "Native")
            {
                List<int> groupOut = ConfigurationManager.AppSettings.Get("GroupNotInVien").Split(',').Select(Int32.Parse).ToList();
                var listgroup = this.roleservice.GetAll(false).Where(t => t.IsWorking.GetValueOrDefault() && !groupOut.Contains(t.Id)).Select(t => t.Id).ToList();
                var listuserview = AllUser.Where(t => listgroup.Contains(t.RoleId.GetValueOrDefault()));

                addingPermission = listuserview.Where(t => t.RoleId != wp.DepartmentId.GetValueOrDefault() && t.Id != obj.ProjectManagerId && t.Id != obj.SupervisorId).Select(t => new UserDataPermission()
                {
                    ProjectId = wp.ProjectId,
                    RoleId = t.RoleId,
                    FolderId = folderId,
                    UserId = t.Id,
                    IsFullPermission = false,
                    CreatedDate = DateTime.Now,
                    CreatedBy = UserSession.Current.User.Id,
                    OnlyView = true
                });
                this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            }
            else if (name == "Native")
            {
                var listGipAndChief = AllUser.Where(t => t.RoleId != wp.DepartmentId.GetValueOrDefault() && (t.IsGip.GetValueOrDefault() || t.IsChief.GetValueOrDefault()));
                addingPermission = listGipAndChief.Select(t => new UserDataPermission()
                {
                    ProjectId = wp.ProjectId,
                    RoleId = t.RoleId,
                    FolderId = folderId,
                    UserId = t.Id,
                    IsFullPermission = false,
                    CreatedDate = DateTime.Now,
                    CreatedBy = UserSession.Current.User.Id,
                    OnlyView = false
                });
                this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            }
            return trans;
        }

        protected void ValidatorStartDate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtStartDate.SelectedDate == null)
            {
                this.ValidatorStartDate.ErrorMessage = "Please enter  StartDate.";
                this.divStartDate.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else
            {
                if (ddlProject.SelectedItem != null)
                {

                    var projectobj = this.scopeProjectService.GetById(Convert.ToInt32(ddlProject.SelectedValue));
                    if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                    {
                        var objWorkGroup = this.workGroupService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                        if (objWorkGroup != null)
                        {
                            if (this.txtStartDate.SelectedDate.GetValueOrDefault().Date > objWorkGroup.Deadline.GetValueOrDefault().Date)
                            {
                                this.ValidatorStartDate.ErrorMessage = "The Startdate must be less than the Deadline (" + objWorkGroup.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") + ")";
                                this.divStartDate.Style["margin-bottom"] = "-26px;";
                                args.IsValid = false;
                            }

                        }
                    }
                    else
                            if (this.txtStartDate.SelectedDate.GetValueOrDefault().Date < projectobj.StartDate.GetValueOrDefault().Date || this.txtStartDate.SelectedDate.GetValueOrDefault().Date > projectobj.Deadline.GetValueOrDefault().Date)
                    {
                        this.ValidatorStartDate.ErrorMessage = "StartDate must be between " + projectobj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + projectobj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy");
                        this.divStartDate.Style["margin-bottom"] = "-26px;";
                        args.IsValid = false;
                    }

                }

            }
        }

        protected void ValidatorDeadline_ServerValidate(object source, ServerValidateEventArgs args)
        {

            if (this.txtDeadline.SelectedDate == null)
            {
                this.ValidatorDeadline.ErrorMessage = "Please enter Deadline.";
                this.divDeadline.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else
            {

                if (ddlProject.SelectedItem != null)
                {

                    var projectobj = this.scopeProjectService.GetById(Convert.ToInt32(ddlProject.SelectedValue));
                    if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                    {
                        var objWorkGroup = this.workGroupService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                        if (objWorkGroup != null)
                        {
                            var maxdatedoc = this.documentpackageservice.GetMaxDatedLineDoc(objWorkGroup.ID);
                            if (maxdatedoc != null)
                            {
                                if (maxdatedoc.GetValueOrDefault().Date > this.txtDeadline.SelectedDate.GetValueOrDefault().Date || this.txtDeadline.SelectedDate.GetValueOrDefault().Date > projectobj.Deadline.GetValueOrDefault().Date)
                                {
                                    this.ValidatorDeadline.ErrorMessage = "Deadline must be between " + maxdatedoc.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + projectobj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy");
                                    this.divDeadline.Style["margin-bottom"] = "-26px;";
                                    args.IsValid = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        if ((this.txtStartDate.SelectedDate != null && this.txtStartDate.SelectedDate.GetValueOrDefault().Date > this.txtDeadline.SelectedDate.GetValueOrDefault().Date) || this.txtDeadline.SelectedDate.GetValueOrDefault().Date > projectobj.Deadline.GetValueOrDefault().Date)
                        {
                            this.ValidatorDeadline.ErrorMessage = "Deadline must be between StartDate and " + projectobj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy");
                            this.divDeadline.Style["margin-bottom"] = "-26px;";
                            args.IsValid = false;
                        }
                    }
                }
            }
        }

        protected void WeightValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (txtWeight.Enabled != false)
            {
                if (txtWeight.Text.Trim().Length == 0)
                {
                    this.WeightValidator.ErrorMessage = "Please enter Weight.";
                    this.divWeight.Style["margin-bottom"] = "-26px;";
                    args.IsValid = false;
                }
                else
                {
                    if (Convert.ToDouble(txtWeight.Text.Trim()) <= 0)
                    {
                        this.WeightValidator.ErrorMessage = "Please enter Weight greater 0.";
                        this.divWeight.Style["margin-bottom"] = "-26px;";
                        args.IsValid = false;
                    }
                }
            }
        }

        protected void fileUploadValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (docuploader.UploadedFiles.Count > 0)
            {
                if (docuploader.UploadedFiles[0].GetExtension().ToLower() != ".pdf")
                {
                    this.fileUploadValidator.ErrorMessage = "Please upload file .PDF";
                    this.divFileUpload.Style["margin-bottom"] = "-26px;";
                    args.IsValid = false;
                }
            }
        }

        protected void departmentValidate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.ddlDepartment.SelectedIndex == 0)
            {
                this.departmentValidate.ErrorMessage = "Please enter Department.";
                this.divDepartment.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        //protected void TotalManHoursValidator_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (txtTotalManHours.Text.Trim().Length == 0)
        //    {
        //        this.TotalManHoursValidator.ErrorMessage = "Please enter Manhours.";
        //        this.divTotalManHours.Style["margin-bottom"] = "-26px;";
        //        args.IsValid = false;
        //    }
        //    else
        //    {
        //        if (Convert.ToDouble(txtTotalManHours.Text.Trim()) <= 0)
        //        {
        //            this.TotalManHoursValidator.ErrorMessage = "Please enter Manhours greater 0.";
        //            this.divTotalManHours.Style["margin-bottom"] = "-26px;";
        //            args.IsValid = false;
        //        }
        //    }
        //}
    }
}
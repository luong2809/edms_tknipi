﻿
/////


//////
namespace EDMs.Business.Services.Document
{

    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;
    using System.Globalization;


    public  class ManhourService
    {
        private readonly ManhourDAO repo;

        public ManhourService()
        {
            this.repo = new ManhourDAO();
        }


        public List<Manhour> GetAllByDocumentPackage(int DocumentId)
        {
            return this.repo.GetAll().Where(t => t.DocumentID == DocumentId).ToList();
        }

        public List<Manhour> GetAllByDocumentPackage(int DocumentId, int month, int year)
        {
            return this.repo.GetAll().Where(t => t.DocumentID == DocumentId
                                       && (t.ManhourDate!= null && t.ManhourDate.Value.Month==month
                                       && t.ManhourDate.Value.Year==year)).ToList();
        }

        public List<Manhour> GetAllByWorkpackage(List<int> documentIds, DateTime fromDate, DateTime toDate)
        {
            return this.repo.GetAll().Where(t => documentIds.Contains(t.DocumentID.GetValueOrDefault())
                                        && t.ManhourDate !=null
                                        && t.ManhourDate.Value >= fromDate
                                        && t.ManhourDate < toDate).ToList();
        }

        public Manhour GetManhourOfWeek(int documentId, DateTime time, int engineer)
        {        CultureInfo myCI = CultureInfo.CurrentCulture;
                 System.Globalization.Calendar myCal = myCI.Calendar;
                 CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                 DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
                 return this.repo.GetAll().Find(t => documentId == t.DocumentID && 
                                        t.Engineer==engineer
                                        &&
                                        (myCal.GetWeekOfYear(time,myCWR, myFirstDOW)== (myCal.GetWeekOfYear(t.ManhourDate.GetValueOrDefault(),myCWR, myFirstDOW))));

        }

        public Manhour GetManhourOfWeek(int id,int documentId, DateTime time, int engineer)
        {
            CultureInfo myCI = CultureInfo.CurrentCulture;
            System.Globalization.Calendar myCal = myCI.Calendar;
            CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
            return this.repo.GetAll().Find(t => (documentId == t.DocumentID) &&
                                        (t.Engineer == engineer)
                                        && (t.ID!=id)
                                        &&
                                        (myCal.GetWeekOfYear(time, myCWR, myFirstDOW) == (myCal.GetWeekOfYear(t.ManhourDate.GetValueOrDefault(), myCWR, myFirstDOW))));

        }
        public List<Manhour> GetAllByDocument(List<int> DocumentIds)
        {
            return this.repo.GetAll().Where(t => DocumentIds.Contains(t.DocumentID.GetValueOrDefault())).ToList();
        }

        public List<Manhour> GetAllByDocumentCurrentMonth(List<int> DocumentIds, DateTime day)
        {
            return this.repo.GetAll().Where(t => DocumentIds.Contains(t.DocumentID.GetValueOrDefault())
                                        && t.ManhourDate != null
                                        && t.ManhourDate.Value.Month == day.Month
                                        && t.ManhourDate.Value.Year == day.Year).ToList().ToList();
        }
        public List<Manhour> GetAll()
        {
            return this.repo.GetAll().ToList();

        }

        public List<Manhour> GetAllByMonth(int month, int year){
            return this.repo.GetAll().Where(t => t.ManhourDate != null
                                        && t.ManhourDate.Value.Month == month
                                        && t.ManhourDate.Value.Year == year).ToList();
        }

        public List<Manhour> GetAllByMonth(int month, int year, int document)
        {
            return this.repo.GetAll().Where(t => t.ManhourDate != null
                                       && t.ManhourDate.Value.Month == month
                                       && t.ManhourDate.Value.Year == year
                                       && t.DocumentID==document).ToList();
        }

        //public List<Manhour> GetAllByDocument(DateTime fromdate, DateTime todate, int document)
        //{
        //    return this.repo.GetAll().Where(t => t.ManhourDate != null
        //                               && t.ManhourDate.Value >= fromdate.Date
        //                               && t.ManhourDate.Value < todate.Date
        //                               && t.DocumentID == document).ToList();
        //}

        public List<Manhour> GetAllByDocument(DateTime fromdate, DateTime todate, List<int> listuserId)
        {
            return this.repo.GetAll().Where(t => t.ManhourDate != null
                                       && t.ManhourDate.Value >= fromdate.Date
                                       && t.ManhourDate.Value < todate.Date
                                       && listuserId.Contains(t.Engineer.GetValueOrDefault())).ToList();
        }
        public List<Manhour> GetAllByEngineer(DateTime fromdate, DateTime todate, int userId)
        {
            return this.repo.GetAll().Where(t => t.ManhourDate != null
                                       && t.ManhourDate.Value >= fromdate.Date
                                       && t.ManhourDate.Value < todate.Date
                                       && userId==t.Engineer.GetValueOrDefault()).ToList();
        }

        public Manhour GetById(int id)
        {
            return this.repo.GetById(id);
        }

        public int? Insert(Manhour bo)
        {
            return this.repo.Insert(bo);
        }


        public bool Update(Manhour bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(Manhour bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }


        public object GetManhourOfWeek(int p1, int p2, DateTime? nullable1, int? nullable2)
        {
            throw new NotImplementedException();
        }

        public object GetManhourOfWeek(int p, DateTime? nullable1, int? nullable2)
        {
            throw new NotImplementedException();
        }
    }
}

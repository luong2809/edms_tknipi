﻿namespace EDMs.Business.Services.Library
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Library;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class ProjectAppendixService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly ProjectAppendixDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectAppendixService"/> class.
        /// </summary>
        public ProjectAppendixService()
        {
            this.repo = new ProjectAppendixDAO();
        }

        #region Get (Advances)
        public ProjectAppendix GetByProject(int projectID)
        {
            return this.repo.GetByProject(projectID);
        }
        public ProjectAppendix GetByProject(int projectID, int year)
        {
            return this.repo.GetByProject(projectID, year);
        }
        public ProjectAppendix GetByAppendix(int appendixID, List<int> workID, int year)
        {
            return this.repo.GetByAppendix(appendixID, workID, year);
        }
        public List<ProjectAppendix> GetByWork(int workID)
        {
            return this.repo.GetByWork(workID);
        }
        public List<ProjectAppendix> GetByWork(int workID, int year)
        {
            return this.repo.GetByWork(workID, year);
        }
        public List<ProjectAppendix> GetAllByAppendix(int appendixID, List<int> workID, int year)
        {
            return this.repo.GetAllByAppendix(appendixID, workID, year);
        }

        public List<ProjectAppendix> GetAllByAppendix(int appendixID)
        {
            return this.repo.GetAllByAppendix(appendixID);
        }

        public List<ProjectAppendix> GetByWork(List<int> workID, int year)
        {
            return this.repo.GetByWork(workID, year);
        }
        public List<ProjectAppendix> GetByYear(int year)
        {
            return this.repo.GetByYear(year);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<ProjectAppendix> GetAll()
        {
            return this.repo.GetAll().ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public ProjectAppendix GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(ProjectAppendix bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(ProjectAppendix bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(ProjectAppendix bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

﻿
namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;

  public  class ProcessManhourPlanedService
    {
       private readonly ProcessManhourPlanedDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessManhourPlanedService"/> class.
        /// </summary>
         public ProcessManhourPlanedService()
        {
            this.repo = new ProcessManhourPlanedDAO();
        }

        #region Get (Advances)
        public List<ProcessManhourPlaned> GetAllByProject(int projectId, int workgroupId)
        {
            return this.repo.GetAll().Where(t => t.ProjectId == projectId &&  t.WorkgroupId == workgroupId).ToList();
        }

        public ProcessManhourPlaned GetByProjectAndWorkgroup(int projectId, int workgroupid)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.ProjectId == projectId && t.WorkgroupId == workgroupid);
        }
        public List<ProcessManhourPlaned> GetAllProject(int projectID)
        {
            return this.repo.GetAll().Where(t=>t.ProjectId==projectID).ToList();
        }
        public List<ProcessManhourPlaned> GetAllCurrent(int DepartmentId, DateTime days)
        {
            return this.repo.GetAll().Where(t=> t.DepartmentId == DepartmentId 
                && (t.StartDate <= days && t.EndDate >=days)).ToList();
        }
        public List<ProcessManhourPlaned> GetAllCurrent(List<int> DepartmentId, DateTime days)
        {
            return this.repo.GetAll().Where(t =>DepartmentId.Contains(t.DepartmentId.GetValueOrDefault())
                && (t.StartDate <= days && t.EndDate >= days)).ToList();
        }

        public List<ProcessManhourPlaned> GetAllCurrentProject(int projectId, DateTime days)
        {
            return this.repo.GetAll().Where(t => t.ProjectId == projectId
                && (t.StartDate <= days && t.EndDate >= days)).ToList();
        }
        public List<ProcessManhourPlaned> GetAllCurrent( DateTime days)
        {
            return this.repo.GetAll().Where(t => t.StartDate <= days && t.EndDate >= days).ToList();
        }
        public ProcessManhourPlaned GetByDepartmentAndWorkgroup(int department, int workgroupid)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.DepartmentId == department && t.WorkgroupId == workgroupid);
        }
        public List<ProcessManhourPlaned> GetByDepartment(int departmentId)
        {
            return this.repo.GetAll().Where(t => t.DepartmentId == departmentId).ToList();
        }

        public bool ISExist(int projectId, int workgroupid)
        {
            return this.repo.GetAll().Any(t => t.ProjectId == projectId && t.WorkgroupId == workgroupid);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The ProcessManhourPlaned
        /// </returns>
        public List<ProcessManhourPlaned> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of ProcessManhourPlaned
        /// </param>
        /// <returns>
        /// The ProcessManhourPlaned</returns>
        public ProcessManhourPlaned GetById(int id)
        {
            return this.repo.GetById(id);
        }

        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(ProcessManhourPlaned bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(ProcessManhourPlaned bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(ProcessManhourPlaned bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Net;
using System.Net.Mail;
using System.Text;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;
    using System.Globalization;
    using Business.Services.Function;
    using Function;

    public class NotificationDoc
    {
        private string docNo;
        private string error;

        public NotificationDoc()
        { }

        public NotificationDoc(string docNo, string error)
        {
            DocNo = docNo;
            Error = error;
        }

        public string DocNo
        {
            get
            {
                return docNo;
            }

            set
            {
                docNo = value;
            }
        }

        public string Error
        {
            get
            {
                return error;
            }

            set
            {
                error = value;
            }
        }
    }

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportDocMasterList : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        private readonly UserService userService;
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly EmailNotificationTemplateService emailNotificationTemplateService;
        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly WorkGroupService workGroupService;
        private readonly ManhourService mahoursService;
        private readonly ProcessManhourPlanedService processmanhourplanedsevice;
        private readonly ProcessManhourActualService processmanhouractualservice;
        private readonly ProcessActualService processActualService;
        private readonly MilestoneService milestoneService;
        private readonly UserManhourService userManhourService = new UserManhourService();
        private readonly UserManhourMonthService userManhourMonthService = new UserManhourMonthService();
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();
        private readonly ProcessBussiness processBussiness = new ProcessBussiness();
        private readonly StatusService statusService = new StatusService();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportDocMasterList()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.documentService = new DocumentService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.userService = new UserService();
            this.emailNotificationTemplateService = new EmailNotificationTemplateService();
            this.milestoneService = new MilestoneService();
            this.mahoursService = new ManhourService();
            this.processmanhourplanedsevice = new ProcessManhourPlanedService();
            this.processmanhouractualservice = new ProcessManhourActualService();
            this.processActualService = new ProcessActualService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {

                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var weightfrommanhour = false;
            this.blockError.Visible = false;
            this.lblError.Visible = false;
            this.lblError2.Text = "";
            var documentPackageList = new List<DocumentPackage>();
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        //var project = this.scopeProjectService.GetByName(docFile.FileName.Split('$')[0]);

                        var listExistFile = new List<string>();
                        var listErrorFile = new List<NotificationDoc>();
                        var isAutoDetectDocNo =
                            Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AutoDetectDocNo"));
                        // Instantiate a new workbook
                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        for (int i = 2; i < workbook.Worksheets.Count; i++)
                        {
                            var worksheet = workbook.Worksheets[i];

                            var workgroupId = worksheet.Cells["A1"].Value;
                            var wpObj = this.workGroupService.GetById(Convert.ToInt32(workgroupId));
                            currentSheetName = worksheet.Name;
                            currentDocumentNo = string.Empty;
                            var error = "";
                            if (wpObj != null)
                            {
                                // var projectId = worksheet.Cells["A2"].Value.ToString();
                                var project = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());

                                // Create a datatable
                                var dataTable = new DataTable();

                                // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                                dataTable = worksheet.Cells.ExportDataTable(2, 1,
                                    worksheet.Cells.MaxRow, 18);

                                var day = new DateTime(2017, 01, 01);
                                //if (project.StartDate != null && project.StartDate.GetValueOrDefault() >= day.Date)
                                //{
                                //    var summanhour = CheckManhour(dataTable, ref error, wpObj.Deadline.GetValueOrDefault());
                                //    weightfrommanhour = true;
                                //}

                                //if (!string.IsNullOrEmpty(error))
                                //{
                                //    this.blockError.Visible = true;
                                //    this.lblError.Visible = true;
                                //    this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + error + "< br />. please check it again.";
                                //    return;
                                //}

                                int row = 0;
                                bool flag = true;
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    var notiDoc = new NotificationDoc();
                                    flag = true;
                                    if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) || !string.IsNullOrEmpty(dataRow["Column16"].ToString()))
                                    {
                                        currentDocumentNo = dataRow["Column1"].ToString().Trim();

                                        var documentType =
                                            this.documentTypeService.GetByName(dataRow["Column13"].ToString().Trim());
                                        //var role = this.roleService.GetByName(dataRow["Column11"].ToString().Trim());
                                        var role = this.roleService.GetByID(wpObj.DepartmentId.GetValueOrDefault());
                                        var revision = this.revisionService.GetByName(
                                            !string.IsNullOrEmpty(dataRow["Column3"].ToString()) ? dataRow["Column3"].ToString() : "0");
                                        if (!this.documentPackageService.IsExist(currentDocumentNo, revision.ID, wpObj.ID))
                                        {
                                            var docObj = new DocumentPackage();
                                            docObj.WorkgroupId = wpObj.ID;
                                            docObj.WorkgroupName = wpObj.Name;
                                            docObj.WorkgroupFullName = wpObj.FullName;
                                            docObj.WorkgroupRevName = wpObj.FullNameRev;
                                            docObj.ProjectId = project.ID;
                                            docObj.ProjectName = project.Name;
                                            if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                                            {
                                                docObj.DocNo = dataRow["Column1"].ToString().Trim();
                                            }
                                            else
                                            {
                                                flag = false;
                                                notiDoc.DocNo = currentDocumentNo;
                                                notiDoc.Error += "Doc No is null or empty<br/>";
                                            }
                                            docObj.DocTitle = dataRow["Column2"].ToString();

                                            if (revision != null)
                                            {
                                                docObj.RevisionId = revision.ID;
                                                docObj.RevisionName = revision.Name;
                                                docObj.StatusID = revision.StatusID;
                                                docObj.StatusName = revision.StatusName;
                                            }
                                            else
                                            {
                                                flag = false;
                                                notiDoc.DocNo = currentDocumentNo;
                                                notiDoc.Error += "Rev is null or empty<br/>";
                                            }

                                            if (!string.IsNullOrEmpty(dataRow["Column4"].ToString()))
                                            {
                                                var strstartDate = dataRow["Column4"].ToString();
                                                var startDate = new DateTime();
                                                if (Utility.ConvertStringToDateTimeddMMyyyy(strstartDate, ref startDate))
                                                {
                                                    if (wpObj.StartDate != null && wpObj.Deadline != null && startDate.Date <= wpObj.Deadline.GetValueOrDefault().Date && startDate.Date >= wpObj.StartDate.GetValueOrDefault().Date)
                                                    {

                                                        docObj.StartDate = startDate;
                                                    }
                                                    else
                                                    {
                                                        flag = false;
                                                        notiDoc.DocNo = currentDocumentNo;
                                                        notiDoc.Error += "Startdate must be between " + wpObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + wpObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") + " <br/>";
                                                    }

                                                }
                                                else
                                                {
                                                    flag = false;
                                                    notiDoc.DocNo = currentDocumentNo;
                                                    notiDoc.Error += "StartDate is invalid format<br/>";
                                                }
                                            }
                                            else
                                            {
                                                flag = false;
                                                notiDoc.DocNo = currentDocumentNo;
                                                notiDoc.Error += "StartDate is null or empty or invalid format<br/>";
                                            }

                                            if (!string.IsNullOrEmpty(dataRow["Column5"].ToString()))
                                            {
                                                var strdeadline = dataRow["Column5"].ToString();
                                                var deadline = new DateTime();
                                                if (Utility.ConvertStringToDateTimeddMMyyyy(strdeadline, ref deadline))
                                                {

                                                    if (docObj.StartDate != null && wpObj.Deadline != null && deadline.Date <= wpObj.Deadline.GetValueOrDefault().Date && deadline.Date >= docObj.StartDate.GetValueOrDefault().Date)
                                                    {

                                                        docObj.Deadline = deadline;
                                                    }
                                                    else
                                                    {
                                                        flag = false;
                                                        notiDoc.DocNo = currentDocumentNo;
                                                        notiDoc.Error += "Deadline must be between " + docObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + wpObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") + " <br/>";
                                                    }
                                                }
                                                else
                                                {
                                                    flag = false;
                                                    notiDoc.DocNo = currentDocumentNo;
                                                    notiDoc.Error += "Deadline is invalid format<br/>";
                                                }
                                            }
                                            else
                                            {
                                                flag = false;
                                                notiDoc.DocNo = currentDocumentNo;
                                                notiDoc.Error += "Deadline is null or empty or invalid format<br/>";
                                            }

                                            if (project.IsIDC.GetValueOrDefault())
                                            {
                                                if (!string.IsNullOrEmpty(dataRow["Column7"].ToString()))
                                                {
                                                    var stridcdeadline = dataRow["Column7"].ToString();
                                                    var idcdeadline = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeddMMyyyy(stridcdeadline, ref idcdeadline))
                                                    {

                                                        if (docObj.StartDate != null && wpObj.Deadline != null && idcdeadline.Date <= wpObj.Deadline.GetValueOrDefault().Date && idcdeadline.Date >= docObj.StartDate.GetValueOrDefault().Date)
                                                        {

                                                            docObj.IDCDeadline = idcdeadline;
                                                        }
                                                        else
                                                        {
                                                            flag = false;
                                                            notiDoc.DocNo = currentDocumentNo;
                                                            notiDoc.Error += "IDC Deadline must be between " + docObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + wpObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") + " <br/>";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        flag = false;
                                                        notiDoc.DocNo = currentDocumentNo;
                                                        notiDoc.Error += "IDC Deadline is invalid format<br/>";
                                                    }
                                                }
                                                else
                                                {
                                                    flag = false;
                                                    notiDoc.DocNo = currentDocumentNo;
                                                    notiDoc.Error += "IDC Deadline is null or empty or invalid format<br/>";
                                                }
                                                if (!string.IsNullOrEmpty(dataRow["Column9"].ToString()))
                                                {
                                                    var strifrdeadline = dataRow["Column9"].ToString();
                                                    var ifrdeadline = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeddMMyyyy(strifrdeadline, ref ifrdeadline))
                                                    {

                                                        if (docObj.StartDate != null && wpObj.Deadline != null && ifrdeadline.Date <= wpObj.Deadline.GetValueOrDefault().Date && ifrdeadline.Date >= docObj.StartDate.GetValueOrDefault().Date)
                                                        {

                                                            docObj.IFRDeadline = ifrdeadline;
                                                        }
                                                        else
                                                        {
                                                            flag = false;
                                                            notiDoc.DocNo = currentDocumentNo;
                                                            notiDoc.Error += "IFR Deadline must be between " + docObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + wpObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") + " <br/>";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        flag = false;
                                                        notiDoc.DocNo = currentDocumentNo;
                                                        notiDoc.Error += "IFR Deadline is invalid format<br/>";
                                                    }
                                                }
                                                //else
                                                //{
                                                //    flag = false;
                                                //    notiDoc.DocNo = currentDocumentNo;
                                                //    notiDoc.Error += "IFR Deadline is null or empty or invalid format<br/>";
                                                //}
                                            }

                                            docObj.Weight = !string.IsNullOrEmpty(dataRow["Column11"].ToString())
                                                ? Math.Round(Convert.ToDouble(dataRow["Column11"]) * 100, 2)
                                                : 0;
                                            var temp = !string.IsNullOrEmpty(dataRow["Column12"].ToString())
                                               ? Math.Round(Convert.ToDouble(dataRow["Column12"]) * 100, 2)
                                               : 0;
                                            if (project.IsIDC.GetValueOrDefault())
                                            {
                                                var statusObj = this.statusService.GetById(docObj.StatusID.GetValueOrDefault());
                                                if (statusObj != null)
                                                {
                                                    var idcComplete = this.statusService.GetById(6).Complete.GetValueOrDefault();
                                                    var ifrComplete = this.statusService.GetById(7).Complete.GetValueOrDefault();
                                                    var ifaComplete = this.statusService.GetById(8).Complete.GetValueOrDefault();
                                                    if (statusObj.ID == 6)
                                                    {
                                                        docObj.Complete = temp < idcComplete ? temp : 59;
                                                    }
                                                    else if (statusObj.ID == 7)
                                                    {
                                                        docObj.Complete = temp < ifrComplete ? (temp > idcComplete ? temp : 61) : 79;
                                                    }
                                                    else if (statusObj.ID == 8)
                                                    {
                                                        docObj.Complete = temp < ifaComplete ? (temp > ifrComplete ? temp : 81) : 99;
                                                    }
                                                }
                                                else
                                                {
                                                    docObj.Complete = 0;
                                                }
                                            }
                                            else
                                            {
                                                docObj.Complete = temp < 100 ? temp : 99;
                                            }

                                            //docObj.DeparmentId = role != null ? role.Id : 0;
                                            //docObj.DeparmentName = role != null ? role.Name : string.Empty;
                                            docObj.DeparmentId = role != null ? role.Id : 0;
                                            docObj.DeparmentName = role != null ? role.Name : string.Empty;
                                            docObj.DocumentTypeId = documentType != null ? documentType.ID : 0;
                                            docObj.DocumentTypeName = documentType != null
                                                ? documentType.FullName
                                                : string.Empty;
                                            if (!string.IsNullOrEmpty(dataRow["Column14"].ToString()))
                                            {
                                                docObj.ManHourPlan = Convert.ToDouble(dataRow["Column14"].ToString());
                                            }
                                            else
                                            {
                                                flag = false;
                                                notiDoc.DocNo = currentDocumentNo;
                                                notiDoc.Error += "Manhours is null or empty<br/>";
                                            }
                                            var leader = this.userService.GetByUserFullName(dataRow["Column15"].ToString().Trim());
                                            if (leader != null)
                                            {
                                                docObj.LeaderId = leader.Id;
                                                docObj.LeaderName = leader.UserNameWithFullName;
                                            }
                                            else
                                            {
                                                flag = false;
                                                notiDoc.DocNo = currentDocumentNo;
                                                notiDoc.Error += "Leader is null or empty<br/>";
                                            }
                                            var stringEng = dataRow["Column16"].ToString().Trim();
                                            List<string> listEngID = new List<string>();
                                            List<string> listEngName = new List<string>();
                                            if (stringEng.Contains(","))
                                            {
                                                foreach (var item in stringEng.Split(','))
                                                {
                                                    var engineerTemp = this.userService.GetByUserFullName(item.Trim());
                                                    if (engineerTemp != null)
                                                    {
                                                        listEngID.Add(engineerTemp.Id.ToString());
                                                        listEngName.Add(engineerTemp.UserNameWithFullName);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var engineer =
                                            this.userService.GetByUserFullName(stringEng);
                                                if (engineer != null)
                                                {
                                                    listEngID.Add(engineer.Id.ToString());
                                                    listEngName.Add(engineer.UserNameWithFullName);
                                                }
                                            }
                                            listEngID = listEngID.Distinct().ToList();
                                            if (listEngID.Count > 0)
                                            {
                                                docObj.EngineerId = string.Join(",", listEngID.ToArray());
                                                docObj.EngineerName = string.Join(",", listEngName.ToArray());
                                            }
                                            else
                                            {
                                                flag = false;
                                                notiDoc.DocNo = currentDocumentNo;
                                                notiDoc.Error += "Engineers is null or empty<br/>";
                                            }

                                            var value = dataRow["Column17"].ToString().Trim();
                                            switch (value.ToLower())
                                            {
                                                case "high":
                                                    docObj.LevelPriority = 3;
                                                    break;
                                                case "medium":
                                                    docObj.LevelPriority = 2;
                                                    break;
                                                case "low":
                                                    docObj.LevelPriority = 1;
                                                    break;
                                                default:
                                                    docObj.LevelPriority = 0;
                                                    break;
                                            }
                                            docObj.PersonsInvolved = dataRow["Column18"].ToString();
                                            docObj.PackageId = 0;
                                            docObj.PackageName = string.Empty;
                                            docObj.DisciplineId = 0;
                                            docObj.DisciplineName = string.Empty;
                                            docObj.OutgoingTransNo = string.Empty;
                                            docObj.IncomingTransNo = string.Empty;
                                            docObj.ICAReviewCode = string.Empty;
                                            docObj.RevisionCommentCode = string.Empty;
                                            docObj.ICAReviewOutTransNo = string.Empty;
                                            docObj.Notes = string.Empty;
                                            docObj.IsEMDR = true;
                                            docObj.IsLeaf = true;
                                            docObj.IsDelete = false;
                                            docObj.CreatedBy = UserSession.Current.User.Id;
                                            docObj.CreatedDate = DateTime.Now;
                                            docObj.HasAttachFile = false;

                                            if (!flag)
                                            {
                                                listErrorFile.Add(notiDoc);
                                            }

                                            if (!documentPackageList.Exists(t => t.DocNo == docObj.DocNo) && flag == true)
                                            { documentPackageList.Add(docObj); }

                                            if (!this.cbCheckValidFile.Checked && flag == true)
                                            {
                                                this.documentPackageService.Insert(docObj);
                                                var en = this.userService.GetByID(docObj.EngineerId);
                                                // Send Notification 
                                                if (en != null && en.Role.Notify.GetValueOrDefault() && Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSendNotification"]))
                                                {
                                                    this.NotificationAddNewDoc(docObj);
                                                }
                                            }
                                            row++;
                                        }
                                        else
                                        { listExistFile.Add(currentDocumentNo); }
                                    }
                                }
                            }
                            else
                            {
                                this.blockError.Visible = true;
                                this.lblError.Visible = true;
                                this.lblError.Text = "Document master list file is invalid. Please re-export new template file to input data.";
                            }

                            if (this.cbCheckValidFile.Checked && documentPackageList.Count > 0)
                            {
                                foreach (var documentObj in documentPackageList)
                                {
                                    this.documentPackageService.Insert(documentObj);
                                    if (wpObj != null)
                                    {
                                        var docList = this.documentPackageService.GetAllEMDRByWorkgroup(wpObj.ID);
                                        var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                                        double complete = 0;
                                        Milestone milestoneObj = new Milestone();
                                        ProcessActual processActualObj = new ProcessActual();
                                        List<WorkgroupManhourMonth> wmmList = new List<WorkgroupManhourMonth>();
                                        WorkgroupManhourMonth wmmObj = new WorkgroupManhourMonth();
                                        UserManhour userManhourObj = new UserManhour();
                                        UserManhourMonth userManhourMonthObj = new UserManhourMonth();

                                        //update weight cho document
                                        UpdateWeightDocument(projectObj, ref docList);
                                        var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                                        //update data workpackage
                                        UpdateDataWorkpackage(projectObj, docList, ref wpObj, ref wpList, ref complete);
                                        if (wpObj.IsAutoCalculate.GetValueOrDefault())
                                        {
                                            if ((complete < 100 && docList.Where(t => t.Complete != 100).Any()))
                                            {
                                                //update milestone
                                                UpdateMilestonActual(wpObj, UserSession.Current.User, milestoneObj);
                                            }
                                        }
                                        //Update actual progress
                                        UpdateProgressActual(wpObj, processActualObj);

                                        // update data Project
                                        UpdateDataProject(wpList, projectObj);

                                        //Update all wpWeight trong thang workgroup manhour month cua project
                                        UpdateWeightWorkgroupManhourMonth(wpObj, wmmList);

                                        UpdateDataWorkgroupManhourMonth(wpObj, UserSession.Current.User, wpList, wmmObj);

                                        //update manhour
                                        UpdateUserManhourWeek(documentObj, UserSession.Current.User, userManhourObj);
                                        UpdateUserManhourMonth(documentObj, UserSession.Current.User, userManhourMonthObj);
                                    }
                                }
                                //var engineering = documentPackageList.Select(t => t.EngineerId).Distinct().ToList();
                                //foreach (var tem in engineering)
                                //{
                                //    var engList = tem.Split(',').ToList();
                                //    foreach (var item in engList)
                                //    {
                                //        var en = this.userService.GetByID(item);
                                //        // Send Notification 
                                //        if (en != null && en.Role.Notify.GetValueOrDefault() && Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSendNotification"]))
                                //        {
                                //            this.NotifiListDocument(documentPackageList.Where(t => (t.EngineerId.Split(',').ToList()).Any(en.Id.ToString().Equals)).ToList(), en);
                                //        }
                                //    }
                                //}

                                documentPackageList = new List<DocumentPackage>();
                                this.blockError.Visible = true;
                                this.lblError.Visible = true;
                                this.lblError.Text =
                                    "Data of document master list file is valid. And data imported successful.";
                            }
                        }

                        if (listExistFile.Count > 0)
                        {
                            this.blockError.Visible = true;
                            this.lblError.Visible = true;
                            this.lblError.Text += "Document exists: <br/>";
                            foreach (var item in listExistFile)
                            {
                                this.lblError.Text += "<span style='color: blue; font-weight: bold'>" + item + "</span> <br/>";
                            }
                        }

                        if (listErrorFile.Count > 0)
                        {
                            this.blockError.Visible = true;
                            this.lblError2.Visible = true;
                            this.lblError2.Text += "Document error: <br/>";
                            foreach (var item in listErrorFile)
                            {
                                this.lblError2.Text += "<span style='color: blue; font-weight: bold'>" + item.DocNo + "<br>" + item.Error + "</span> <br/>";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Visible = true;
                this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        private void NotificationAddNewDoc(DocumentPackage docObj)
        {
            try
            {
                if (docObj.EngineerId != "0")
                {
                    var engineer = new List<User>();
                    if (docObj.EngineerId.Contains(","))
                    {
                        var listEng = docObj.EngineerId.Split(',');
                        foreach (var item in listEng)
                        {
                            engineer.Add(this.userService.GetByID(item));
                        }
                    }
                    else
                    {
                        engineer.Add(this.userService.GetByID(Convert.ToInt32(docObj.EngineerId)));
                    }
                    if (engineer != null && engineer.Count > 0)
                    {
                        string strEnginner = string.Join(" - ", engineer.Select(t => t.FullName));
                        var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.CreateNewDoc);
                        if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                        {
                            var smtpClient = new SmtpClient
                            {
                                DeliveryMethod = SmtpDeliveryMethod.Network,
                                UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                                EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                                Host = ConfigurationManager.AppSettings["Host"],
                                Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                            };

                            var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());
                            var subject = notificationTemplate.Subject.Replace("#DocNumber#", docObj.DocNo).Replace("#AssignedUser#", strEnginner);

                            var message = new MailMessage();
                            message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                            message.Subject = subject;
                            message.BodyEncoding = new UTF8Encoding();
                            message.IsBodyHtml = true;
                            message.Body = notificationTemplate.Contents
                                .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                                .Replace("#DocNumber#", docObj.DocNo)
                                .Replace("#AssignedUser#", strEnginner)
                                .Replace("#DocTitle#", docObj.DocTitle)
                                .Replace("#DocType#", docObj.DocumentTypeName)
                                .Replace("#DocRev#", docObj.RevisionName)
                                .Replace("#DocEng#", strEnginner)
                                .Replace("#DocStartDate#", docObj.StartDate != null ? docObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                                .Replace("#DocDeadline#", docObj.Deadline != null ? docObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                                .Replace("#DocFinishDate#", docObj.EndDate != null ? docObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                                .Replace("#DocISODate#", docObj.IsoReviseDate != null ? docObj.IsoReviseDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                                .Replace("#DocWeight#", docObj.Weight != null ? docObj.Weight.Value.ToString() : string.Empty)
                                .Replace("#DocComplete#", docObj.Complete != null ? docObj.Complete.Value.ToString() : string.Empty);

                            if (ConfigurationManager.AppSettings["SendOnlySupport"].ToLower() == "true")
                            {
                                message.To.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                            }
                            else
                            {
                                foreach (var item in engineer)
                                {
                                    if (!string.IsNullOrEmpty(item.Email))
                                    {
                                        message.To.Add(item.Email);
                                    }
                                }
                                var leader = this.userService.GetByID(docObj.LeaderId.ToString());
                                message.To.Add(leader.Email);
                            }

                            smtpClient.Send(message);
                        }
                    }
                }
            }
            catch { }
        }

        private double CheckManhour(DataTable dataTable, ref string error, DateTime date)
        {
            var summanhour = 0.0;
            var row = 0;
            foreach (DataRow dataRow in dataTable.Rows)
            {
                row++;
                var strdeadline = dataRow["Column5"].ToString();
                var deadline = new DateTime();
                var flag = Utility.ConvertStringToDateTimeddMMyyyy(strdeadline, ref deadline);
                ////// deadline = DateTime.ParseExact(strdeadline, "dd/mm/YYYY", null);

                if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                {
                    if (string.IsNullOrEmpty(dataRow["Column14"].ToString()))
                    {
                        error += dataRow["Column1"].ToString().Trim().Replace("  ", " ") + @" Manhour value is empty; <br\> ";
                    }
                    if (string.IsNullOrEmpty(dataRow["Column15"].ToString()))
                    {
                        error += dataRow["Column1"].ToString().Trim().Replace("  ", " ") + @" Leader is empty; <br\> ";
                    }
                    if (string.IsNullOrEmpty(dataRow["Column16"].ToString()))
                    {
                        error += dataRow["Column1"].ToString().Trim().Replace("  ", " ") + @" Employees is empty; <br\> ";
                    }
                    if (deadline.Date > date.Date)
                    {
                        error += dataRow["Column1"].ToString().Trim().Replace("  ", " ") + @" Deadline of Document Greater than deadline of Workpackage; <br\> ";
                    }
                    if (string.IsNullOrEmpty(dataRow["Column5"].ToString()))
                    {
                        error += dataRow["Column1"].ToString().Trim().Replace("  ", " ") + @" Deadline is empty;<br\> ";
                    }
                    if (string.IsNullOrEmpty(dataRow["Column4"].ToString()))
                    {
                        error += dataRow["Column1"].ToString().Trim().Replace("  ", " ") + @" StartDate is empty;<br\> ";
                    }
                    if (string.IsNullOrEmpty(dataRow["Column3"].ToString()))
                    {
                        error += dataRow["Column1"].ToString().Trim().Replace("  ", " ") + @" Revision is empty;<br\> ";
                    }

                }
            }
            return summanhour;
        }
        private List<DateTime> GetBusinessDays(DateTime start, DateTime end)
        {
            List<DateTime> listdate = new List<DateTime>();
            for (var i = start; i <= end; i = i.AddDays(1))
            {
                if (i.DayOfWeek != DayOfWeek.Saturday && i.DayOfWeek != DayOfWeek.Sunday) { listdate.Add(i); }
            }
            return listdate;
        }
        #region UpdateData
        private void UpdateWeightDocument(ScopeProject projectObj, ref List<DocumentPackage> docList)
        {
            try
            {
                var totalPlanManhour = 0.0;
                totalPlanManhour = docList.Aggregate(totalPlanManhour, (current, t) => current + t.ManHourPlan.GetValueOrDefault());

                foreach (var doc in docList)
                {
                    doc.Weight = (doc.ManHourPlan.GetValueOrDefault() / totalPlanManhour) * 100;
                    this.documentPackageService.Update(doc);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkpackage(ScopeProject projectObj, List<DocumentPackage> docList, ref WorkGroup wpObj, ref List<WorkGroup> wpList, ref double complete)
        {
            try
            {
                if (docList.Count > 0)
                {
                    //update complete workpackage
                    complete = 0.0;
                    complete = docList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                    complete = Math.Round(complete, 5);
                    if (complete == 100 || !docList.Where(t => t.Complete < 100).Any())
                    {
                        wpObj.EndDate = DateTime.Now;
                        wpObj.Complete = 100;
                    }
                    else
                    {
                        wpObj.Complete = complete;
                    }
                }

                // Update total document weight for workpackage
                double totalWeight = 0.0;
                totalWeight = docList.Sum(t => t.Weight.GetValueOrDefault());
                totalWeight = Math.Round(totalWeight, 2);
                totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                wpObj.TotalDocWeight = totalWeight;

                // Update total man hours plan & actual for workpackage
                double totalManhourPlan = 0.0;
                totalManhourPlan = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                wpObj.TotalManHours = totalManhourPlan;

                double totalManHourActual = 0.0;
                totalManHourActual = docList.Sum(t => t.ManHours.GetValueOrDefault());
                wpObj.UsedManHours = totalManHourActual;

                // Update can delete for workpackage
                wpObj.CanDelete = !docList.Any();
                this.workGroupService.Update(wpObj);

                //update weight all WP of PJ
                if (projectObj.AutoCalculateWeightWorkGroup == true)
                {
                    var sumTotalManHours = 0.0;
                    sumTotalManHours = wpList.Sum(t => t.TotalManHours.GetValueOrDefault());
                    foreach (var items in wpList)
                    {
                        var calWeight = ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) > 0 ? ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) : 0;
                        items.Weight = calWeight >= 100 ? 100 : calWeight;
                        this.workGroupService.Update(items);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateMilestonActual(WorkGroup wpObj, User userObj, Milestone milestoneObj)
        {
            try
            {
                var milestoneList = this.milestoneService.GetAllByWorkpackage(wpObj.ID).OrderByDescending(t => t.MilestoneDate).ToList();
                if (milestoneList.Count > 0)
                {
                    var presentMilestoneObj = milestoneList.FirstOrDefault(t => t.MilestoneDate != null
                                                                         && t.MilestoneDate.Value.Month == DateTime.Now.Month
                                                                         && t.MilestoneDate.Value.Year == DateTime.Now.Year);
                    if (presentMilestoneObj != null)
                    {
                        milestoneObj = presentMilestoneObj;
                        var prevIndex = milestoneList.IndexOf(milestoneObj) + 1;
                        var prevRealTotal = 0.0;
                        var prevPlanTotal = 0.0;
                        if (prevIndex < (milestoneList.Count - 1))
                        {
                            var prevMilestoneObj = milestoneList[prevIndex];
                            if (prevMilestoneObj != null)
                            {
                                prevRealTotal = prevMilestoneObj.RealTotal.GetValueOrDefault();
                                prevPlanTotal = prevMilestoneObj.PlanTotal.GetValueOrDefault();
                            }
                            else
                            {
                                prevRealTotal = 0;
                                prevPlanTotal = 0;
                            }
                        }
                        else
                        {
                            prevRealTotal = 0;
                            prevPlanTotal = 0;
                        }

                        // calculator real
                        double real = Math.Round(wpObj.Complete.GetValueOrDefault() - prevRealTotal, 2);
                        milestoneObj.Real = real;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PlanTotal = (prevPlanTotal + milestoneObj.PlanPercent) > 100 ? 100 : (prevPlanTotal + milestoneObj.PlanPercent);
                        milestoneObj.UpdatedBy = userObj.Id;
                        milestoneObj.UpdatedDate = DateTime.Now;
                        this.milestoneService.Update(milestoneObj);
                    }
                    else
                    {
                        var lastedMilestoneObj = milestoneList[0];
                        milestoneObj = new Milestone();
                        milestoneObj.MilestoneDate = DateTime.Now;
                        milestoneObj.PlanPercent = 0;
                        milestoneObj.PlanTotal = lastedMilestoneObj.PlanTotal;
                        double preReal = Math.Round(wpObj.Complete.GetValueOrDefault() - lastedMilestoneObj.RealTotal.GetValueOrDefault(), 2);
                        milestoneObj.Real = preReal;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PerformingUser = lastedMilestoneObj.PerformingUser;
                        milestoneObj.Note = lastedMilestoneObj.Note;
                        milestoneObj.WorkpackageId = wpObj.ID;
                        milestoneObj.WorkpackageName = wpObj.Name;
                        if (DateTime.Now.Year > lastedMilestoneObj.MilestoneDate.Value.Year && (DateTime.Now.Year - lastedMilestoneObj.MilestoneDate.Value.Year) > 0)
                        {
                            milestoneObj.PlanOfYear = 0;
                        }
                        else
                        {
                            milestoneObj.PlanOfYear = lastedMilestoneObj.PlanOfYear;
                        }
                        milestoneObj.CreatedBy = userObj.Id;
                        milestoneObj.CreatedDate = DateTime.Now;
                        this.milestoneService.Insert(milestoneObj);
                    }
                }
                else
                {
                    //milestoneObj = new Milestone();
                    milestoneObj.MilestoneDate = DateTime.Now;
                    milestoneObj.PlanPercent = 0;
                    milestoneObj.PlanTotal = 0;
                    milestoneObj.RealTotal = wpObj.Complete;
                    milestoneObj.Real = wpObj.Complete;
                    milestoneObj.PerformingUser = string.Empty;
                    milestoneObj.Note = string.Empty;
                    milestoneObj.WorkpackageId = wpObj.ID;
                    milestoneObj.WorkpackageName = wpObj.Name;
                    milestoneObj.PlanOfYear = 0;
                    milestoneObj.CreatedBy = userObj.Id;
                    milestoneObj.CreatedDate = DateTime.Now;
                    this.milestoneService.Insert(milestoneObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateProgressActual(WorkGroup wpObj, ProcessActual processActualObj)
        {
            try
            {
                var projectId = wpObj.ProjectId.GetValueOrDefault();
                var projectObj = this.scopeProjectService.GetById(projectId);
                var tempProcessActualObj = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, wpObj.ID);
                if (tempProcessActualObj != null)
                {
                    processActualObj = tempProcessActualObj;
                    if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null)
                    {
                        if (string.IsNullOrEmpty(processActualObj.Actual))
                        {
                            processActualObj.Actual = "0";
                        }
                        if (string.IsNullOrEmpty(processActualObj.ActualMonth))
                        {
                            processActualObj.ActualMonth = "0";
                        }
                        var progressActualWeekList = processActualObj.Actual.Split('$').ToList();
                        var progressActualMonthList = processActualObj.ActualMonth.Split('$').ToList();
                        var countWeek = 0;
                        var countMonth = 0;
                        for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualWeekList.Count() > countWeek)
                            {
                                if (DateTime.Now > j.AddDays(7))
                                {
                                    countWeek += 1;
                                }
                            }
                        }
                        var currentMonth = 0;
                        for (var j = projectObj.StartDate.GetValueOrDefault();
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualMonthList.Count() > countMonth)
                            {
                                if (DateTime.Now.Month > j.AddDays(7).Month && DateTime.Now > j.AddDays(7) && currentMonth != j.AddDays(7).Month)
                                {
                                    currentMonth = j.Month;
                                    countMonth++;
                                }
                            }
                        }
                        if (progressActualWeekList.Count() >= countWeek && progressActualWeekList.Count() > 0 && countWeek > 0)
                        {
                            progressActualWeekList = progressActualWeekList.Take(countWeek).ToList();
                            progressActualWeekList[countWeek - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.Actual = string.Join("$", progressActualWeekList);
                            this.processActualService.Update(processActualObj);
                        }
                        if (progressActualMonthList.Count() >= countMonth && progressActualMonthList.Count() > 0 && countMonth > 0)
                        {
                            progressActualMonthList = progressActualMonthList.Take(countMonth).ToList();
                            progressActualMonthList[countMonth - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.ActualMonth = string.Join("$", progressActualMonthList);
                            this.processActualService.Update(processActualObj);
                        }
                    }
                }
                else
                {
                    processActualObj = new ProcessActual();
                    processActualObj.ProjectId = wpObj.ProjectId;
                    processActualObj.WorkgroupId = wpObj.ID;
                    processActualObj.Actual = wpObj.Complete.ToString();
                    processActualObj.ActualMonth = wpObj.Complete.ToString();
                    this.processActualService.Insert(processActualObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private DateTime GetMondayOfWeek(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        private DateTime GetFridayOfWeek(DateTime date)
        {
            return GetMondayOfWeek(date).AddDays(4);
        }

        private void UpdateDataProject(List<WorkGroup> wpList, ScopeProject projectObj)
        {
            try
            {
                if (projectObj != null)
                {
                    projectObj.CanDelete = false;
                    if (projectObj.IsAutoCalculate.GetValueOrDefault())
                    {
                        double complete = 0.0;
                        complete = wpList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                        projectObj.Complete = Math.Round(complete, 5);
                        double totalWeight = 0.0;
                        totalWeight = wpList.Sum(t => t.Weight.GetValueOrDefault());
                        totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                        projectObj.TotalWorkpackageWeight = Math.Round(totalWeight, 2);
                    }
                    this.scopeProjectService.Update(projectObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateWeightWorkgroupManhourMonth(WorkGroup wpObj, List<WorkgroupManhourMonth> wmmList)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                wmmList = this.workgroupManhourMonthService.GetByPJ(wpObj.ProjectId.GetValueOrDefault(), month, year);
                foreach (var wmmItem in wmmList)
                {
                    var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                    if (wpItem != null)
                    {
                        wmmItem.WorkgroupName = wpItem.Name;
                        wmmItem.WorkgroupWeight = wpItem.Weight;
                        this.workgroupManhourMonthService.Update(wmmItem);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkgroupManhourMonth(WorkGroup wpObj, User userObj, List<WorkGroup> wpList, WorkgroupManhourMonth wmmObj)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                var docList = this.documentPackageService.GetAllByWorkgroup(wpObj.ID);
                var manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                var presentWMMObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), month, year);
                var prevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpObj.ID, month, year);
                if (presentWMMObj != null)
                {
                    wmmObj = presentWMMObj;
                    wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                    wmmObj.ManhourPlanTotal = manhourPlanTotal;
                    wmmObj.ManhourPlanMonth = (wmmObj.ManhourPlanTotal.GetValueOrDefault() * wmmObj.CompletePlanMonth.GetValueOrDefault()) / 100;
                    wmmObj.CompleteActualTotal = wpObj.Complete.GetValueOrDefault();
                    wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                    //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                    if (prevWMMObj != null)
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                    }
                    else
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                    }

                    wmmObj.UpdateBy = userObj.Id;
                    wmmObj.UpdateDate = DateTime.Now;
                    this.workgroupManhourMonthService.Update(wmmObj);
                }
                else
                {
                    foreach (var wpItem in wpList)
                    {
                        docList = this.documentPackageService.GetAllByWorkgroup(wpItem.ID);
                        manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                        var tempObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpItem.ID, wpItem.ProjectId.GetValueOrDefault(), month, year);
                        var tempPrevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpItem.ID, month, year);
                        if (tempObj == null)
                        {
                            wmmObj = new WorkgroupManhourMonth();
                            wmmObj.Month = month;
                            wmmObj.Year = year;
                            wmmObj.WorkgroupID = wpItem.ID;
                            wmmObj.WorkgroupName = wpItem.Name;
                            wmmObj.DeparmentID = wpItem.DepartmentId;
                            wmmObj.ProjectID = wpItem.ProjectId.GetValueOrDefault();
                            wmmObj.WorkgroupWeight = wpItem.Weight.GetValueOrDefault();
                            wmmObj.ManhourPlanTotal = manhourPlanTotal;
                            wmmObj.CompleteActualTotal = wpItem.Complete.GetValueOrDefault();
                            wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                            //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                            if (tempPrevWMMObj != null)
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - tempPrevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - tempPrevWMMObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            else
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            wmmObj.CreateBy = userObj.Id;
                            wmmObj.CreateDate = DateTime.Now;
                            this.workgroupManhourMonthService.Insert(wmmObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourWeek(DocumentPackage documentObj, User userObj, UserManhour userManhourObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;

                CultureInfo myCI = CultureInfo.CurrentCulture;
                System.Globalization.Calendar myCal = myCI.Calendar;
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var listUserManhour = this.userManhourService.GetByDocIDAndUserID(documentObj.ID, user.Id);
                            listUserManhour = listUserManhour.OrderByDescending(t => t.ID).ToList();

                            var currentUserManhourWeek = listUserManhour.FirstOrDefault(t => myCal.GetWeekOfYear(DateTime.Now.Date, myCWR, myFirstDOW) == myCal.GetWeekOfYear(t.Date.GetValueOrDefault(), myCWR, myFirstDOW));
                            int index = listUserManhour.IndexOf(currentUserManhourWeek);
                            var prevUserManhourWeek = new UserManhour();
                            if (index != -1 && (index + 1) < listUserManhour.Count)
                            {
                                prevUserManhourWeek = listUserManhour[(index + 1)];
                            }
                            if (currentUserManhourWeek != null)
                            {
                                userManhourObj = currentUserManhourWeek;
                                if (prevUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - prevUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UpdateBy = userObj.Id;
                                userManhourObj.UpdateDate = DateTime.Now;
                                this.userManhourService.Update(userManhourObj);
                            }
                            else
                            {
                                var lastedUserManhourWeek = new UserManhour();
                                if (listUserManhour.Count > 0)
                                {
                                    lastedUserManhourWeek = listUserManhour[0];
                                }
                                userManhourObj = new UserManhour();
                                userManhourObj.DocumentID = documentObj.ID;
                                userManhourObj.Date = DateTime.Now.Date;
                                userManhourObj.DeparmentID = user.RoleId;
                                userManhourObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourObj.ProjectID = documentObj.ProjectId;
                                if (lastedUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - lastedUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - lastedUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UserID = user.Id;
                                userManhourObj.UserName = user.FullName;
                                userManhourObj.CreateBy = userObj.Id;
                                userManhourObj.CreateDate = DateTime.Now;
                                this.userManhourService.Insert(userManhourObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourMonth(DocumentPackage documentObj, User userObj, UserManhourMonth userManhourMonthObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;
                var date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var currentUserManhourMonth = this.userManhourMonthService.GetByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            var prevUserManhourMonth = this.userManhourMonthService.GetLastedByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            if (currentUserManhourMonth != null)
                            {
                                userManhourMonthObj = currentUserManhourMonth;
                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UpdateBy = userObj.Id;
                                userManhourMonthObj.UpdateDate = DateTime.Now;
                                this.userManhourMonthService.Update(userManhourMonthObj);
                            }
                            else
                            {
                                userManhourMonthObj = new UserManhourMonth();
                                userManhourMonthObj.DocumentID = documentObj.ID;
                                userManhourMonthObj.Month = DateTime.Now.Month;
                                userManhourMonthObj.Year = DateTime.Now.Year;
                                userManhourMonthObj.DeparmentID = user.RoleId;
                                userManhourMonthObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourMonthObj.ProjectID = documentObj.ProjectId;

                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UserID = user.Id;
                                userManhourMonthObj.UserName = user.FullName;
                                userManhourMonthObj.CreateBy = userObj.Id;
                                userManhourMonthObj.CreateDate = DateTime.Now;
                                this.userManhourMonthService.Insert(userManhourMonthObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}
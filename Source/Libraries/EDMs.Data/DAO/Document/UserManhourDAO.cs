﻿

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
    public class UserManhourDAO : BaseDAO
    {

        public UserManhourDAO() : base() { }

        public IQueryable<UserManhour> GetIQueryable()
        {
            return this.EDMsDataContext.UserManhours;
        }
        public List<UserManhour> GetAll()
        {
            return this.EDMsDataContext.UserManhours.OrderByDescending(t => t.ID).ToList();
        }

        public List<UserManhour> GetByDocIDAndUserID(int documentID, int userID)
        {
            return this.EDMsDataContext.UserManhours.Where(ob => ob.DocumentID == documentID && ob.UserID == userID).ToList();
        }

        public UserManhour GetById(int id)
        {
            return this.EDMsDataContext.UserManhours.FirstOrDefault(ob => ob.ID == id);
        }



        public int? Insert(UserManhour ob)
        {
            try
            {
                this.EDMsDataContext.AddToUserManhours(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }


        public bool Update(UserManhour src)
        {
            try
            {
                UserManhour des = (from rs in this.EDMsDataContext.UserManhours
                                   where rs.ID == src.ID
                                   select rs).First();

                des.UserID = src.UserID;
                des.UserName = src.UserName;
                des.DeparmentID = src.DeparmentID;
                des.DocumentID = src.DocumentID;
                des.DocumentName = src.DocumentName;
                des.Date = src.Date;
                des.ManhourActualWeek = src.ManhourActualWeek;
                des.CompleteWeek = src.CompleteWeek;
                des.ManhourActualTotal = src.ManhourActualTotal;
                des.CompleteTotal = src.CompleteTotal;
                des.UpdateBy = src.UpdateBy;
                des.UpdateDate = src.UpdateDate;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }



        public bool Delete(UserManhour src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }


        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

    }
}

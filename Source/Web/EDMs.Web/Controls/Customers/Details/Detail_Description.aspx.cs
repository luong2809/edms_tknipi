﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Data.Entities;

namespace EDMs.Web.Controls.Customers
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Business.Services;
    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class Detail_Description : Page
    {
        /// <summary>
        /// The patient status service
        /// </summary>
        private Patient_DescriptionService _patientDescriptionService;


        public Detail_Description()
        {
            _patientDescriptionService = new Patient_DescriptionService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

            }
        }
        protected void grdDescriptionHistory_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                var patient = Request.QueryString["ID"];
                if (patient != null)
                {
                    int tempID;
                    if (int.TryParse(patient, out tempID))
                    {
                        var listDescriptionHistory = _patientDescriptionService.Find(x => x.CustomerID == tempID);
                        grdDescriptionHistory.DataSource = listDescriptionHistory.OrderByDescending(t => t.UpdatedDate);
                    }
                }
            }
        }
        protected void grdDescriptionHistory_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            int csID = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());

            _patientDescriptionService.Delete(csID);
        }
        protected void grdDescriptionHistory_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var editLink = (Image)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "#";
                editLink.Attributes["onclick"] = string.Format("return ShowEditForm('{0}','{1}');",
                                                               e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex][
                                                                   "Id"], e.Item.ItemIndex);
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using EDMs.Business.Services.Scope;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.ServiceProcess;
    using System.Text;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using CheckBox = System.Web.UI.WebControls.CheckBox;
    using Label = System.Web.UI.WebControls.Label;
    using TextBox = System.Web.UI.WebControls.TextBox;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class ViewEMDR : Page
    {
     
  

        private readonly DocumentNewService documentNewService = new DocumentNewService();


        private readonly UserService userService = new UserService();

        private readonly AttachFileService attachFileService = new AttachFileService();

        private readonly AttachFilesPackageService attachFilesPackageService = new AttachFilesPackageService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly PackageService packageService = new PackageService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly WorkGroupService workGroupService = new WorkGroupService();
        

        private readonly RoleService roleservice = new RoleService();

        protected const string ServiceName = "EDMSFolderWatcher";

        public static RadTreeNode editedNode = null;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

        /// <summary>
        /// The list folder id.
        /// </summary>
       // private List<int> listFolderId = new List<int>();

        private List<WorkGroup> WorkpackageList
        {
            get
            {
                var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
                var currentDepartment = UserSession.Current.User.RoleId;

                return isSeeFull
                    ? this.workGroupService.GetAll()
                    : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault());
            }
        }
        //List<DocumentPackage> docListFilter = new List<DocumentPackage>();
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
           
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {

                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var objScopeProject = this.scopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objScopeProject != null)
                    {
                        this.rtvWorkgroup.DataSource = this.workGroupService.GetAllWorkGroupOfProject(objScopeProject.ID);
                        this.rtvWorkgroup.DataTextField = "FullNameRev";
                        this.rtvWorkgroup.DataValueField = "ID";
                        this.rtvWorkgroup.DataFieldID = "ID";
                        this.rtvWorkgroup.DataBind();
                        this.LoadDocuments(false, false);
                    }
                }

                    
                
            }
        }

        /// <summary>
        /// The rad tree view 1_ node click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void radTreeFolder_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            

            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(true, isListAll);
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(bool isbind = false, bool isListAll = false)
        {
             
            var docList = new List<DocumentPackage>();
            try
            {
                if (this.rtvWorkgroup.SelectedNode != null )
                {
                    
                        docList = this.documentPackageService.GetAllByWorkgroup(Convert.ToInt32(this.rtvWorkgroup.SelectedNode.Value))
                        .OrderBy(t => t.DocNo)
                        .ToList();


                }
                else
                {
                 
                    docList = this.documentPackageService.GetAllDocProject(Convert.ToInt32(this.Request.QueryString["disId"]))
                        .OrderBy(t => t.DocNo)
                        .ToList();
                }

            }
            catch { }
            this.grdDocument.DataSource = docList;
          //  docListFilter = docList;
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.Rebind();
            }
           
            
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.Rebind();
            }
            
            
        }
        
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(false, isListAll);
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentPackageService.GetById(docId);
            if (docObj != null)
            {
                if (docObj.ParentId == null || docObj.ParentId == 0 )
                {
                    docObj.IsDelete = true;
                    docObj.UpdatedBy = UserSession.Current.User.Id;
                    docObj.UpdatedDate = DateTime.Now.Date;
                    this.documentPackageService.Update(docObj);
                }
                else
                {
                    var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                    if (listRelateDoc != null)
                    {
                        foreach (var objDoc in listRelateDoc)
                        {
                            objDoc.IsDelete = true;
                            objDoc.UpdatedBy = UserSession.Current.User.Id;
                            objDoc.UpdatedDate = DateTime.Now.Date;
                            this.documentPackageService.Update(objDoc);
                        }
                    }
                  
                }
                var docLists = this.documentPackageService.GetAllByWorkgroup(docObj.WorkgroupId.GetValueOrDefault());
                var totalPlanManhour = 0.0;
                totalPlanManhour = docLists.Aggregate(totalPlanManhour, (current, t) => current + t.ManHourPlan.GetValueOrDefault());
                foreach (var doc in docLists)
                {
                    doc.Weight = Math.Round((doc.ManHourPlan.GetValueOrDefault() / totalPlanManhour) * 100, 2);
                    this.documentPackageService.Update(doc);
                }
                var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());
                if (wpObj != null)
                {
                    var docList = this.documentPackageService.GetAllByWorkgroup(wpObj.ID);
                    if (docList.Count == 0)
                    {
                        wpObj.CanDelete = true;
                        this.workGroupService.Update(wpObj);
                    }
                }

              
            }
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid")
            {
               
            }
            if (e.CommandName == RadGrid.RebindGridCommandName)
            {
               
                this.rtvWorkgroup.UnselectAllNodes();
                this.grdDocument.Rebind();
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                
            }
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["HasAttachFile"].Text == "True")
                {
                    item.BackColor = Color.Aqua;
                    item.BorderColor = Color.Aqua;
                }
            }

            
        }

        protected void radTreeFolder_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            PopulateNodeOnDemand(e, TreeNodeExpandMode.ServerSideCallBack);
        }

        protected void grdDocument_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var txtManHours = item.FindControl("txtManHours") as RadNumericTextBox;
                var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
                var txtDeadline = item.FindControl("txtDeadline") as RadDatePicker;
                var txtEndDate = item.FindControl("txtEndDate") as RadDatePicker;
                var txtIsoReviseDate = item.FindControl("txtIsoReviseDate") as RadDatePicker;
                var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
                var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;

                var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                var objDoc = this.documentPackageService.GetById(docId);

                var currentRevision = objDoc.RevisionId;
                var newRevision = Convert.ToInt32(ddlRevision.SelectedValue);
                if (currentRevision != 0 && newRevision != currentRevision)
                {
                    var docObjNew = new DocumentPackage
                    {
                        //ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                       // ProjectName = this.ddlProject.SelectedItem.Text,
                        WorkgroupId = objDoc.WorkgroupId,
                        WorkgroupName = objDoc.WorkgroupName,
                        DocNo = objDoc.DocNo,
                        DocTitle = txtDocTitle.Text.Trim(),
                        StartDate = txtStartDate.SelectedDate,
                        Deadline = txtDeadline.SelectedDate,
                        EndDate = txtEndDate.SelectedDate,
                        IsoReviseDate = txtIsoReviseDate.SelectedDate,
                        RevisionId = newRevision,
                        RevisionName = ddlRevision.SelectedItem.Text,
                        Complete = txtComplete.Value,
                        Weight = txtWeight.Value,
                        ManHours = txtManHours.Value,
                        DocumentTypeId = objDoc.DocumentTypeId,
                        DocumentTypeName = objDoc.DocumentTypeName,
                        IsLeaf = true,
                        IsEMDR = true,
                        ParentId = objDoc.ParentId ?? objDoc.ID,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };

                    this.documentPackageService.Insert(docObjNew);

                    objDoc.IsLeaf = false;
                }
                else
                {
                    objDoc.DocTitle = txtDocTitle.Text.Trim();
                    objDoc.StartDate = txtStartDate.SelectedDate;
                    objDoc.Deadline = txtDeadline.SelectedDate;
                    objDoc.EndDate = txtEndDate.SelectedDate;
                    objDoc.IsoReviseDate = txtIsoReviseDate.SelectedDate;
                    objDoc.ManHours = txtManHours.Value;
                    objDoc.RevisionId = newRevision;
                    objDoc.RevisionName = ddlRevision.SelectedItem.Text;
                    objDoc.Complete = txtComplete.Value.GetValueOrDefault();
                    objDoc.Weight = txtWeight.Value.GetValueOrDefault();
                }

                objDoc.UpdatedBy = UserSession.Current.User.Id;
                objDoc.UpdatedDate = DateTime.Now;

                this.documentPackageService.Update(objDoc); 
            }
        }

        protected void ckbEnableFilter_OnCheckedChanged(object sender, EventArgs e)
        {
            this.grdDocument.AllowFilteringByColumn = ((CheckBox)sender).Checked;
            this.grdDocument.Rebind();
        }

        protected void radTreeFolder_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "Images/folderdir16.png";
        }

        private void PopulateNodeOnDemand(RadTreeNodeEventArgs e, TreeNodeExpandMode expandMode)
        {
            
        }


       private List<DateTime> GetBusinessDays(DateTime start, DateTime end)
        {
            List<DateTime> listdate = new List<DateTime>();  
            for (var i = start; i <= end;i=i.AddDays(1) )
            {
                if (i.DayOfWeek != DayOfWeek.Saturday && i.DayOfWeek != DayOfWeek.Sunday) { listdate.Add(i); }
            }
            return listdate;   
        }


        /// <summary>
        /// The get all child folder id.
        /// </summary>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        //private List<int> GetAllChildFolderId(int parentId, List<int> folderPermission)
        //{
        //    if (!this.listFolderId.Contains(parentId))
        //    {
        //        this.listFolderId.Add(parentId);
        //    }


        //    var listFolder = this.folderService.GetAllByParentId(parentId, folderPermission);
        //    foreach (var folder in listFolder)
        //    {
        //        this.listFolderId.Add(folder.ID);
        //        this.GetAllChildFolderId(folder.ID, folderPermission);
        //    }

        //    return this.listFolderId;
        //}

        /// <summary>
        /// The custom folder tree.
        /// </summary>
        /// <param name="radTreeView">
        /// The rad tree view.
        /// </param>
        private void CustomFolderTree(RadTreeNode radTreeView)
        {
            foreach (var node in radTreeView.Nodes)
            {
                var nodetemp = (RadTreeNode)node;
                if (nodetemp.Nodes.Count > 0)
                {
                    this.CustomFolderTree(nodetemp);
                }

                nodetemp.ImageUrl = "Images/folderdir16.png";
            }
        }

       

        private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = Aspose.Cells.ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;
            validation.ShowInput = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";
            validation.InputTitle = "Warning";

            // Set the error message.
            validation.ErrorMessage = "Please select item from the list";
            validation.InputMessage = "Please select item from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        
     

      

        protected void grdDocument_Init(object sender, EventArgs e)
        {
        }

        protected void grdDocument_DataBound(object sender, EventArgs e)
        {
        }

        protected void rtvWorkgroup_NodeClick(object sender, RadTreeNodeEventArgs e)
        {

            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The btn download_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnDownload_Click(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).Parent.Parent as GridDataItem;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentNewService.GetById(docId);
            var docPackName = string.Empty;
            if (docObj != null)
            {
                //docPackName = docObj.Name;
                //var serverDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_" + docObj.Name + "_Pack.rar");

                //var attachFiles = this.attachFileService.GetAllByDocId(docId);
                //var temp = ZipPackage.CreateFile(serverDocPackPath);

                //foreach (var attachFile in attachFiles)
                //{
                //    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                //    {
                //        temp.Add(Server.MapPath(attachFile.FilePath));
                //    }
                //}

                //this.DownloadByWriteByte(serverDocPackPath, docPackName + ".rar", true);
            }
        }

        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {
                var filterItem = (GridFilteringItem)e.Item;
                var selectedProperty = new List<string>();

                var ddlFilterRev = (RadComboBox)filterItem.FindControl("ddlFilterRev");
            }
        }

        protected DateTime? SetPublishDate(GridItem item)
        {
            if (item.OwnerTableView.GetColumn("Index27").CurrentFilterValue == string.Empty)
            {
                return new DateTime?();
            }
            else
            {
                return DateTime.Parse(item.OwnerTableView.GetColumn("Index27").CurrentFilterValue);
            }
        }

        
        protected void rtvWorkgroup_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/package.png";
        }

        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"Images/project.png";
        }

        protected void rtvmove_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/package.png";
        }


       
    }
}
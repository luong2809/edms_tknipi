﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppendixesDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Library
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class AppendixDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppendixDAO"/> class.
        /// </summary>
        public AppendixDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Appendix> GetIQueryable()
        {
            return this.EDMsDataContext.Appendixes;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Appendix> GetAll()
        {
            return this.EDMsDataContext.Appendixes.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public Appendix GetById(int id)
        {
            return this.EDMsDataContext.Appendixes.FirstOrDefault(ob => ob.ID == id);
        }

        #endregion

        #region GET ADVANCE
        public List<Appendix> GetByWorkWithoutActive(int workID)
        {
            return this.EDMsDataContext.Appendixes.Where(t => t.WorkID == workID).ToList();
        }
        public List<Appendix> GetByWork(int workID)
        {
            return this.EDMsDataContext.Appendixes.Where(t => t.WorkID == workID && t.Active.Value).ToList();
        }

        public List<Appendix> GetByParent(int parentID)
        {
            return this.EDMsDataContext.Appendixes.Where(t => t.ParentID == parentID && t.Active.Value).ToList();
        }

        public List<Appendix> GetByWorkWithoutID(int workID, int year, int id)
        {
            return this.EDMsDataContext.Appendixes.Where(t => t.WorkID == workID && t.Year == year && t.ID != id).ToList();
        }

        public Appendix GetByCodeType(string code, List<int> workID)
        {
            return this.EDMsDataContext.Appendixes.FirstOrDefault(ob => ob.Code == code && workID.Contains(ob.WorkID.Value));
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(Appendix ob)
        {
            try
            {
                this.EDMsDataContext.AddToAppendixes(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(Appendix src)
        {
            try
            {
                var des = (from rs in this.EDMsDataContext.Appendixes
                           where rs.ID == src.ID
                           select rs).First();
                des.WorkID = des.WorkID;
                des.Code = src.Code;
                des.AliasCode = src.AliasCode;
                des.Name = src.Name;
                des.Description = src.Description;
                des.ParentID = src.ParentID;
                des.Year = src.Year;

                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(Appendix src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

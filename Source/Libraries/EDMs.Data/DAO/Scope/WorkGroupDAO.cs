﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkGroupDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Scope
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
    using System;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class WorkGroupDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkGroupDAO"/> class.
        /// </summary>
        public WorkGroupDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<WorkGroup> GetIQueryable()
        {
            return this.EDMsDataContext.WorkGroups;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<WorkGroup> GetAll()
        {
            return this.EDMsDataContext.WorkGroups.OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public WorkGroup GetById(int id)
        {
            return this.EDMsDataContext.WorkGroups.FirstOrDefault(ob => ob.ID == id);
        }

        #endregion

        #region GET ADVANCE
        public List<WorkGroup> GetByProject(int projectId)
        {
            return this.EDMsDataContext.WorkGroups.Where(t => t.ProjectId == projectId).ToList();
        }
        public List<WorkGroup> GetAllByDepartment(int departmentId)
        {
            return this.EDMsDataContext.WorkGroups.Where(t => t.DepartmentId == departmentId).ToList();
        }
        public List<WorkGroup> GetAllByDepartment(int departmentId, int projectId)
        {
            return this.EDMsDataContext.WorkGroups.Where(t => t.DepartmentId == departmentId && t.ProjectId == projectId).ToList();
        }
        public List<WorkGroup> GetAllByDepartment(List<int> departmentId)
        {
            return this.EDMsDataContext.WorkGroups.Where(t => departmentId.Contains(t.DepartmentId.Value)).OrderBy(t => t.Name).ToList();
        }
        public List<WorkGroup> GetAllByDepartment(int deparmentId, List<int> prjectInPermission)
        {
            return this.EDMsDataContext.WorkGroups.Where(t => t.DepartmentId.Value == deparmentId || prjectInPermission.Contains(t.ProjectId.Value)).ToList();
        }
        public WorkGroup GetByName(string name)
        {
            return this.EDMsDataContext.WorkGroups.FirstOrDefault(t => t.Name.Trim() == name.Trim());
        }
        public List<WorkGroup> GetAllWorkGroupOfProject(int projectId)
        {
            return this.EDMsDataContext.WorkGroups.Where(t => t.ProjectId == projectId).ToList();
        }
        public List<WorkGroup> GetAllWorkGroupOfProject(List<int> projectId)
        {
            return this.EDMsDataContext.WorkGroups.Where(t => projectId.Contains(t.ProjectId.Value)).ToList();
        }
        public WorkGroup GetWorkGroupCostOfProject(int projectId, List<int> list)
        {
            return this.EDMsDataContext.WorkGroups.FirstOrDefault(t => t.ProjectId == projectId && list.Contains(t.DisciplineId.Value));
        }
        public WorkGroup GetWorkGroupByDiscipline(int projectId, int discipline)
        {
            return this.EDMsDataContext.WorkGroups.FirstOrDefault(t => t.ProjectId == projectId && t.DisciplineId == discipline);
        }
        public List<WorkGroup> GetAllByName(string name)
        {
            return this.EDMsDataContext.WorkGroups.Where(t => t.Name.Trim() == name.Trim()).ToList();
        }
        public List<WorkGroup> GetAllByTodolist(List<int> wpList)
        {
            return this.EDMsDataContext.WorkGroups.Where(t => wpList.Contains(t.ID)).ToList();
        }
        public List<WorkGroup> GetAllWorkGroupInPermission(int userId, int projectId, List<int> WorkGroupIdInPermission)
        {
            return this.EDMsDataContext.WorkGroups.Where(t => t.ProjectId == projectId && WorkGroupIdInPermission.Contains(t.ID)).ToList();
        }
        public bool IsExist(string workpackageName, int workpackageId)
        {
            return this.EDMsDataContext.WorkGroups.Any(t => t.Name == workpackageName && (workpackageId == 0 || t.ID != workpackageId));
        }

        public DateTime? GetMaxDeadline(int pjId)
        {
            return
               this.EDMsDataContext.WorkGroups.Where(t => t.ProjectId == pjId).Max(t => t.Deadline);
        }
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(WorkGroup ob)
        {
            try
            {
                this.EDMsDataContext.AddToWorkGroups(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(WorkGroup src)
        {
            try
            {
                WorkGroup des = (from rs in this.EDMsDataContext.WorkGroups
                                 where rs.ID == src.ID
                                 select rs).First();

                des.Name = src.Name;
                des.Description = src.Description;

                des.ProjectId = src.ProjectId;
                des.ProjectName = src.ProjectName;
                des.AreaId = src.AreaId;
                des.AreaName = src.AreaName;
                des.DisciplineId = src.DisciplineId;
                des.DisciplineName = src.DisciplineName;
                des.DepartmentId = src.DepartmentId;
                des.DepartmentName = src.DepartmentName;
                des.TypeId = src.TypeId;
                des.TypeName = src.TypeName;

                des.StartDate = src.StartDate;
                des.Deadline = src.Deadline;
                des.EndDate = src.EndDate;

                des.Complete = src.Complete;
                des.Weight = src.Weight;
                des.TotalManHours = src.TotalManHours;
                des.UsedManHours = src.UsedManHours;
                des.TotalDocWeight = src.TotalDocWeight;
                des.CanDelete = src.CanDelete;
                des.IsAutoCalculate = src.IsAutoCalculate;

                des.ProjectManagerID = src.ProjectManagerID;
                des.ProjectManagerFullName = src.ProjectManagerFullName;

                des.DefaultFilePath = src.DefaultFilePath;
                des.FileExtentionIcon = src.FileExtentionIcon;
                des.PathLatestRevision = src.PathLatestRevision;

                des.UpdatedBy = src.UpdatedBy;
                des.UpdatedDate = src.UpdatedDate;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(WorkGroup src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        #endregion
    }
}

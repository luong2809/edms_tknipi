﻿
namespace EDMs.Data.DAO.Security
{
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using EDMs.Data.Entities;
    public class CountUserAccessDAO: BaseDAO
    {
        public CountUserAccessDAO() : base() { }

        #region GET (Basic)
        public List<CountUserAccess> GetAll()
        {
            return this.EDMsDataContext.CountUserAccesses.ToList<CountUserAccess>();
        }

        public CountUserAccess GetByID(int ID)
        {
            return this.EDMsDataContext.CountUserAccesses.FirstOrDefault(ob => ob.Id == ID);
        }
        public CountUserAccess GetDate(DateTime date)
        {
            return this.EDMsDataContext.CountUserAccesses.FirstOrDefault(ob =>ob.DateAccess.Value==date.Date);
        }
       
        #endregion

        #region Get (Advances)



        /// <summary>
        /// Gets the by resource id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        //public CountUserAccess GetByResourceId(int id)
        //{
        //    return this.EDMsDataContext.CountUserAccesss.FirstOrDefault(x => x.ResourceId == id);
        //}

        #endregion

        #region Insert, Update, Delete
        public int? Insert(CountUserAccess ob)
        {
            try
            {
                this.EDMsDataContext.AddToCountUserAccesses(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.Id;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
       
        public bool Update(CountUserAccess src)
        {
            try
            {
                CountUserAccess des;

                des = (from rs in this.EDMsDataContext.CountUserAccesses
                       where rs.Id == src.Id
                       select rs).First();
                des.Number = src.Number;
                des.DateAccess = src.DateAccess;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(CountUserAccess ob)
        {
            try
            {
                CountUserAccess _ob = this.GetByID(ob.Id);
                if (_ob != null)
                {
                    this.EDMsDataContext.DeleteObject(_ob);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                CountUserAccess _ob = this.GetByID(ID);
                if (_ob != null)
                {
                    this.EDMsDataContext.DeleteObject(_ob);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

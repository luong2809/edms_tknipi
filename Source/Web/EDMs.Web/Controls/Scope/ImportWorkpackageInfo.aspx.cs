﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Hosting;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using System.IO;
using System.Text.RegularExpressions;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportWorkpackageInfo : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;
        private readonly ScopeProjectService scopeProjectService;

        private readonly WorkGroupService workGroupService;
        private readonly AreaService areaService;
        private readonly RoleService roleService;
        private readonly DisciplineService disciplineService;
        private readonly AttachFilesWorkpackageService attachFilesWorkpackageService;
        private readonly WPRevisionService wpRevisionService;
        private readonly EmailNotificationTemplateService emailNotificationTemplateService;
        private readonly UserService userService;
        private readonly FolderService folderService;
        private readonly UserDataPermissionService userDataPermissionService;
        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportWorkpackageInfo()
        {
            this.documentService = new DocumentService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.areaService = new AreaService();
            this.roleService = new RoleService();
            this.disciplineService = new DisciplineService();
            this.attachFilesWorkpackageService = new AttachFilesWorkpackageService();
            this.wpRevisionService = new WPRevisionService();
            this.emailNotificationTemplateService = new EmailNotificationTemplateService();
            this.userService = new UserService();
            this.folderService = new FolderService();
            this.userDataPermissionService = new UserDataPermissionService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {

                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var errorMessage = string.Empty;
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var dataSheet = workbook.Worksheets[1];

                        // Create a datatable
                        var dataTable = new DataTable();

                        if (!string.IsNullOrEmpty(Request.QueryString["type"]) && Request.QueryString["type"] == "addnew")
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["projId"]))
                            {
                                if (Request.QueryString["projId"] != dataSheet.Cells["A1"].StringValue)
                                {
                                    errorMessage =
                                        "Workpackage master list is incorrect for this project. Please import correct file.";
                                }
                                else
                                {
                                    var projectId = Convert.ToInt32(Request.QueryString["projId"]);
                                    var projectObj = this.scopeProjectService.GetById(projectId);
                                    if (projectObj != null)
                                    {
                                        dataTable = dataSheet.Cells.ExportDataTable(2, 1, dataSheet.Cells.MaxDataRow, 20);
                                        foreach (DataRow dataRow in dataTable.Rows)
                                        {
                                            if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) && !string.IsNullOrEmpty(dataRow["Column4"].ToString()) && !string.IsNullOrEmpty(dataRow["Column11"].ToString()) && !string.IsNullOrEmpty(dataRow["Column12"].ToString()))
                                            {
                                                var wpRevision = this.wpRevisionService.GetByName(dataRow["Column3"].ToString().Trim());

                                                if (this.workGroupService.IsExist(dataRow["Column1"].ToString().Trim(), 0))
                                                {
                                                    errorMessage += "Workpackage \"" + dataRow["Column1"] +
                                                                    "\" is already exist. <br/>";
                                                }
                                                else
                                                {
                                                    var workgroupObj = new WorkGroup();
                                                    var departmentObj = this.roleService.GetByName(dataRow["Column4"].ToString().Trim());
                                                    var areaObj = this.areaService.GetByName(dataRow["Column5"].ToString().Trim());
                                                    var disciplineObj = this.disciplineService.GetByName(dataRow["Column6"].ToString().Trim());

                                                    workgroupObj.RevisionId = wpRevision != null ? wpRevision.ID : 0;
                                                    workgroupObj.RevisionName = wpRevision != null ? wpRevision.Name : string.Empty;

                                                    workgroupObj.ProjectManagerID = projectObj != null
                                                        ? projectObj.ProjectManagerId
                                                        : 0;
                                                    workgroupObj.ProjectManagerFullName = projectObj != null
                                                        ? projectObj.ProjectManagerFullName
                                                        : string.Empty;

                                                    workgroupObj.ProjectId = projectObj != null ? projectObj.ID : 0;
                                                    workgroupObj.ProjectName = projectObj != null ? projectObj.Name : string.Empty;


                                                    workgroupObj.AreaId = areaObj != null ? areaObj.ID : 0;
                                                    workgroupObj.AreaName = areaObj != null ? areaObj.Name : string.Empty;

                                                    workgroupObj.DisciplineId = disciplineObj != null ? disciplineObj.ID : 0;
                                                    workgroupObj.DisciplineName = disciplineObj != null ? disciplineObj.Name : string.Empty;

                                                    workgroupObj.DepartmentId = departmentObj != null ? departmentObj.Id : 0;
                                                    workgroupObj.DepartmentName = departmentObj != null ? departmentObj.Name : string.Empty;

                                                    workgroupObj.Name = dataRow["Column1"].ToString().Trim();
                                                    workgroupObj.Description = dataRow["Column2"].ToString().Trim();

                                                    var wpType = dataRow["Column7"].ToString().Trim();
                                                    switch (wpType)
                                                    {
                                                        case "":
                                                            workgroupObj.TypeId = 0;
                                                            workgroupObj.TypeName = string.Empty;
                                                            break;
                                                        case "Planned":
                                                            workgroupObj.TypeId = 1;
                                                            workgroupObj.TypeName = "Planned";
                                                            break;
                                                        case "Unplanned":
                                                            workgroupObj.TypeId = 2;
                                                            workgroupObj.TypeName = "Unplanned";
                                                            break;
                                                    }

                                                    try
                                                    {
                                                        workgroupObj.Weight = Convert.ToDouble(dataRow["Column8"].ToString().Trim());
                                                    }
                                                    catch (Exception)
                                                    {
                                                        workgroupObj.Weight = 1;
                                                    }

                                                    try
                                                    {
                                                        workgroupObj.Complete = Convert.ToDouble(dataRow["Column9"].ToString().Trim());
                                                    }
                                                    catch (Exception)
                                                    {
                                                        workgroupObj.Complete = 0;
                                                    }

                                                    try
                                                    {
                                                        workgroupObj.TotalManHours = Convert.ToDouble(dataRow["Column10"].ToString().Trim());
                                                    }
                                                    catch (Exception)
                                                    {
                                                        workgroupObj.TotalManHours = 1;
                                                    }

                                                    var strStartDate = dataRow["Column11"].ToString().Trim();
                                                    var startDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strStartDate, ref startDate))
                                                    {
                                                        workgroupObj.StartDate = startDate;
                                                    }

                                                    var strDeadLine = dataRow["Column12"].ToString().Trim();
                                                    var deadline = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strDeadLine, ref deadline))
                                                    {
                                                        if ((startDate.Date > deadline.Date) || deadline.Date > projectObj.Deadline.GetValueOrDefault().Date)
                                                        {
                                                            errorMessage += "Deadline must be between StartDate and " + projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy");
                                                        }
                                                        else
                                                        {
                                                            workgroupObj.Deadline = deadline;
                                                        }
                                                    }

                                                    var strEndDate = dataRow["Column13"].ToString().Trim();
                                                    var endDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strEndDate, ref endDate))
                                                    {
                                                        workgroupObj.EndDate = endDate;
                                                    }

                                                    workgroupObj.InputDataSupplier = dataRow["Column14"].ToString().Trim();
                                                    workgroupObj.InputDocument = dataRow["Column15"].ToString().Trim();
                                                    workgroupObj.InformationExchange = dataRow["Column16"].ToString().Trim();
                                                    workgroupObj.ReportMode = dataRow["Column17"].ToString().Trim();
                                                    workgroupObj.IncomingNo = dataRow["Column18"].ToString().Trim();
                                                    workgroupObj.OutgoingNo = dataRow["Column19"].ToString().Trim();
                                                    workgroupObj.IsAutoCalculate = dataRow["Column20"].ToString().Trim().ToLower() == "x";

                                                    workgroupObj.CanDelete = true;

                                                    var projectFolder = projectObj.PathLatestRevision != null && !string.IsNullOrEmpty(projectObj.PathLatestRevision) ? Server.MapPath("../.." + projectObj.PathLatestRevision) : string.Empty;

                                                    if (projectObj.PathLatestRevision != null && !string.IsNullOrEmpty(projectObj.PathLatestRevision) && projectObj.PathLatestRevision.Contains("DocumentLibrary/Latest Revision") && Directory.Exists(projectFolder) && !string.IsNullOrEmpty(projectFolder))
                                                    {
                                                        var physicalPath = projectFolder + @"/" + Utilities.Utility.RemoveSpecialCharacterForFolder(workgroupObj.Name.Replace("&", "-"));

                                                        Directory.CreateDirectory(physicalPath);
                                                        workgroupObj.PathLatestRevision = projectObj.PathLatestRevision + "/" + Utilities.Utility.RemoveSpecialCharacterForFolder(dataRow["Column2"].ToString().Trim()).Replace('&', '-');
                                                    }

                                                    var workgroupId = this.workGroupService.Insert(workgroupObj);
                                                    //if (workgroupId != null && !string.IsNullOrEmpty(dataRow["Column21"].ToString().Trim()))
                                                    //{
                                                    //    var attachFiles = dataRow["Column21"].ToString().Trim();
                                                    //    var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/WorkPackageFiles";
                                                    //    foreach (var fileName in attachFiles.Split(';'))
                                                    //    {
                                                    //        var docFileName = fileName.Substring(fileName.LastIndexOf("/") + 1, fileName.Length - fileName.LastIndexOf("/") - 1);

                                                    //        // Path file to download from server
                                                    //        var serverFilePath = serverFolder + "/" + fileName;
                                                    //        var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);

                                                    //        var attachFile = new AttachFilesWorkpackage()
                                                    //        {
                                                    //            WorkpackageId = workgroupId,
                                                    //            FileName = docFileName,
                                                    //            Extension = fileExt,
                                                    //            FilePath = serverFilePath,
                                                    //            ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                                                    //            //FileSize = (double)docFile.ContentLength / 1024,
                                                    //            CreatedBy = UserSession.Current.User.Id,
                                                    //            CreatedDate = DateTime.Now
                                                    //        };

                                                    //        this.attachFilesWorkpackageService.Insert(attachFile);
                                                    //    }
                                                    //}

                                                    if (workgroupId != null && workgroupObj.DepartmentId != null && workgroupObj.DepartmentId.GetValueOrDefault() != 0)
                                                    {
                                                        var folderparent = this.folderService.GetAllByName(workgroupObj.ProjectName);
                                                        if (folderparent != null)
                                                        {
                                                            var folderwp = creterfolder(workgroupObj.Name, folderparent, workgroupObj);
                                                            //PDF
                                                            var pdf = creterfolder("PDF", folderwp, workgroupObj);
                                                            //Native
                                                            var native = creterfolder("Native", folderwp, workgroupObj);
                                                        }
                                                    }
                                                    // Update weight for list workgroup have same name
                                                    // Get only previous WP, without current wp
                                                    var wpSameName = this.workGroupService.GetAllByName(workgroupObj.Name).Where(t => t.ID != workgroupId).ToList();
                                                    if (wpSameName.Count > 0)
                                                    {
                                                        var weight = 0.0;
                                                        weight = Math.Round((wpSameName.Aggregate(weight, (current, t) => current + t.Weight.GetValueOrDefault())) / (wpSameName.Count + 1), 2);

                                                        foreach (var workGroup in wpSameName)
                                                        {
                                                            workGroup.Weight = weight;
                                                            this.workGroupService.Update(workGroup);
                                                        }
                                                        workgroupObj.Weight = weight;
                                                        this.workGroupService.Update(workgroupObj);
                                                    }

                                                    // Update total WP count of department
                                                    if (departmentObj != null && workgroupId != null)
                                                    {
                                                        departmentObj.CurrentWorkpackageCount += 1;
                                                        this.roleService.Update(departmentObj);
                                                    }

                                                    // Re-calculate weight value for Project
                                                    projectObj = this.scopeProjectService.GetById(workgroupObj.ProjectId.GetValueOrDefault());
                                                    if (projectObj != null)
                                                    {
                                                        projectObj.CanDelete = false;
                                                        var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);

                                                        if (projectObj.IsAutoCalculate.GetValueOrDefault())
                                                        {
                                                            var complete = 0.0;
                                                            complete = wpList.Aggregate(complete,
                                                                (current, t) => (current + ((t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100)));
                                                            projectObj.Complete = complete > 100 ? 100 : complete;
                                                        }

                                                        // Update total workpackage weight for Project
                                                        var totalWeight = 0.0;
                                                        totalWeight = wpList.Aggregate(totalWeight, (current, t) => current + t.Weight.GetValueOrDefault());
                                                        projectObj.TotalWorkpackageWeight = totalWeight > 100 ? 100 : totalWeight;

                                                        this.scopeProjectService.Update(projectObj);

                                                        if (projectObj.AutoCalculateWeightWorkGroup == true)
                                                        {
                                                            wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID).ToList();
                                                            var sumTotalManHours = 0.0;
                                                            sumTotalManHours = wpList.Aggregate(sumTotalManHours, (current, t) => current + t.TotalManHours.GetValueOrDefault());
                                                            foreach (var item in wpList)
                                                            {
                                                                var calWeight = ((item.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) > 0 ? ((item.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) : 0;
                                                                item.Weight = calWeight > 100 ? 100 : calWeight;
                                                                this.workGroupService.Update(item);
                                                            }
                                                        }
                                                    }

                                                    var Notify = this.roleService.GetByID(workgroupObj.DepartmentId.GetValueOrDefault());
                                                    if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false" && Notify.Notify.GetValueOrDefault())
                                                    {
                                                        this.NotificationAddNewWp(workgroupObj);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                                                {
                                                    errorMessage += "Workpackage \"" + dataRow["Column1"] +
                                                                        "\" is missing field. <br/>";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                                dataTable = dataSheet.Cells.ExportDataTable(2, 1, dataSheet.Cells.MaxDataRow, 21);

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) && !string.IsNullOrEmpty(dataRow["Column2"].ToString()) && !string.IsNullOrEmpty(dataRow["Column5"].ToString()) && !string.IsNullOrEmpty(dataRow["Column11"].ToString()) && !string.IsNullOrEmpty(dataRow["Column12"].ToString()))
                                    {
                                        if (this.workGroupService.IsExist(dataRow["Column2"].ToString().Trim(), 0))
                                        {
                                            errorMessage += "Workpackage \"" + dataRow["Column2"] +
                                                            "\" is already exist. <br/>";
                                        }
                                        else
                                        {
                                            var workgroupObj = new WorkGroup();
                                            var projectObj = this.scopeProjectService.GetByName(dataRow["Column1"].ToString().Trim());
                                            var wpRevision = this.wpRevisionService.GetByName(dataRow["Column4"].ToString().Trim());
                                            var departmentObj = this.roleService.GetByName(dataRow["Column5"].ToString().Trim());
                                            var areaObj = this.areaService.GetByName(dataRow["Column6"].ToString().Trim());
                                            var disciplineObj = this.disciplineService.GetByName(dataRow["Column7"].ToString().Trim());

                                            workgroupObj.RevisionId = wpRevision != null ? wpRevision.ID : 0;
                                            workgroupObj.RevisionName = wpRevision != null ? wpRevision.Name : string.Empty;

                                            workgroupObj.ProjectId = projectObj != null ? projectObj.ID : 0;
                                            workgroupObj.ProjectName = projectObj != null ? projectObj.Name : string.Empty;

                                            workgroupObj.ProjectManagerID = projectObj != null ? projectObj.ProjectManagerId : 0;
                                            workgroupObj.ProjectManagerFullName = projectObj != null ? projectObj.ProjectManagerFullName : string.Empty;

                                            workgroupObj.AreaId = areaObj != null ? areaObj.ID : 0;
                                            workgroupObj.AreaName = areaObj != null ? areaObj.Name : string.Empty;

                                            workgroupObj.DisciplineId = disciplineObj != null ? disciplineObj.ID : 0;
                                            workgroupObj.DisciplineName = disciplineObj != null ? disciplineObj.Name : string.Empty;

                                            workgroupObj.DepartmentId = departmentObj != null ? departmentObj.Id : 0;
                                            workgroupObj.DepartmentName = departmentObj != null ? departmentObj.Name : string.Empty;

                                            workgroupObj.Name = dataRow["Column2"].ToString().Trim();
                                            workgroupObj.Description = dataRow["Column3"].ToString().Trim();

                                            var wpType = dataRow["Column8"].ToString().Trim();
                                            switch (wpType)
                                            {
                                                case "":
                                                    workgroupObj.TypeId = 0;
                                                    workgroupObj.TypeName = string.Empty;
                                                    break;
                                                case "Planned":
                                                    workgroupObj.TypeId = 1;
                                                    workgroupObj.TypeName = "Planned";
                                                    break;
                                                case "Unplanned":
                                                    workgroupObj.TypeId = 2;
                                                    workgroupObj.TypeName = "Unplanned";
                                                    break;
                                            }
                                            try
                                            {
                                                workgroupObj.Weight = Convert.ToDouble(dataRow["Column9"].ToString().Trim());
                                            }
                                            catch (Exception)
                                            {
                                                workgroupObj.Weight = 0;
                                            }
                                            try
                                            {
                                                workgroupObj.Complete = Convert.ToDouble(dataRow["Column10"].ToString().Trim());
                                            }
                                            catch (Exception)
                                            {
                                                workgroupObj.Complete = 0;
                                            }

                                            try
                                            {
                                                workgroupObj.TotalManHours = Convert.ToDouble(dataRow["Column11"].ToString().Trim());
                                            }
                                            catch (Exception)
                                            {
                                                workgroupObj.TotalManHours = 0;
                                            }

                                            var strStartDate = dataRow["Column12"].ToString().Trim();
                                            var startDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(strStartDate, ref startDate))
                                            {
                                                workgroupObj.StartDate = startDate;
                                            }
                                            var strDeadLine = dataRow["Column13"].ToString().Trim();
                                            var deadline = new DateTime();
                                            if (Utility.ConvertStringToDateTime(strDeadLine, ref deadline))
                                            {
                                                if ((startDate.Date > deadline.Date) || deadline.Date > projectObj.Deadline.GetValueOrDefault().Date)
                                                {
                                                    errorMessage += "Deadline must be between StartDate and " + projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy");
                                                }
                                                else
                                                {
                                                    workgroupObj.Deadline = deadline;
                                                }
                                            }
                                            var strEndDate = dataRow["Column14"].ToString().Trim();
                                            var endDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(strEndDate, ref endDate))
                                            {
                                                workgroupObj.EndDate = endDate;
                                            }
                                            workgroupObj.InputDataSupplier = dataRow["Column15"].ToString().Trim();
                                            workgroupObj.InputDocument = dataRow["Column16"].ToString().Trim();
                                            workgroupObj.InformationExchange = dataRow["Column17"].ToString().Trim();
                                            workgroupObj.ReportMode = dataRow["Column18"].ToString().Trim();
                                            workgroupObj.IncomingNo = dataRow["Column19"].ToString().Trim();
                                            workgroupObj.OutgoingNo = dataRow["Column20"].ToString().Trim();
                                            workgroupObj.IsAutoCalculate = dataRow["Column21"].ToString().Trim().ToLower() == "x";
                                            workgroupObj.CanDelete = true;

                                            var projectFolder = projectObj.PathLatestRevision != null && !string.IsNullOrEmpty(projectObj.PathLatestRevision) ? Server.MapPath("../.." + projectObj.PathLatestRevision) : string.Empty;

                                            if (projectObj.PathLatestRevision != null && !string.IsNullOrEmpty(projectObj.PathLatestRevision) && projectObj.PathLatestRevision.Contains("DocumentLibrary/Latest Revision") && Directory.Exists(projectFolder) && !string.IsNullOrEmpty(projectFolder))
                                            {
                                                var physicalPath = projectFolder + @"/" + Utilities.Utility.RemoveSpecialCharacterForFolder(workgroupObj.Name.Replace("&", "-"));

                                                Directory.CreateDirectory(physicalPath);
                                                workgroupObj.PathLatestRevision = projectObj.PathLatestRevision + "/" + Utilities.Utility.RemoveSpecialCharacterForFolder(dataRow["Column2"].ToString().Trim()).Replace('&', '-');
                                            }

                                            var workgroupId = this.workGroupService.Insert(workgroupObj);
                                            //if (workgroupId != null && !string.IsNullOrEmpty(dataRow["Column21"].ToString().Trim()))
                                            //{
                                            //    var attachFiles = dataRow["Column21"].ToString().Trim();
                                            //    var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/WorkPackageFiles";
                                            //    foreach (var fileName in attachFiles.Split(';'))
                                            //    {
                                            //        var docFileName = fileName.Substring(fileName.LastIndexOf("/") + 1, fileName.Length - fileName.LastIndexOf("/") - 1);

                                            //        // Path file to download from server
                                            //        var serverFilePath = serverFolder + "/" + fileName;
                                            //        var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);

                                            //        var attachFile = new AttachFilesWorkpackage()
                                            //        {
                                            //            WorkpackageId = workgroupId,
                                            //            FileName = docFileName,
                                            //            Extension = fileExt,
                                            //            FilePath = serverFilePath,
                                            //            ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                                            //            //FileSize = (double)docFile.ContentLength / 1024,
                                            //            CreatedBy = UserSession.Current.User.Id,
                                            //            CreatedDate = DateTime.Now
                                            //        };

                                            //        this.attachFilesWorkpackageService.Insert(attachFile);
                                            //    }
                                            //}

                                            if (workgroupId != null && workgroupObj.DepartmentId != null && workgroupObj.DepartmentId.GetValueOrDefault() != 0)
                                            {
                                                var folderparent = this.folderService.GetAllByName(workgroupObj.ProjectName);
                                                if (folderparent != null)
                                                {
                                                    var folderwp = creterfolder(workgroupObj.Name, folderparent, workgroupObj);
                                                    //PDF
                                                    var pdf = creterfolder("PDF", folderwp, workgroupObj);
                                                    //Native
                                                    var native = creterfolder("Native", folderwp, workgroupObj);
                                                }
                                            }
                                            // Update weight for list workgroup have same name
                                            // Get only previous WP, without current wp
                                            var wpSameName = this.workGroupService.GetAllByName(workgroupObj.Name).Where(t => t.ID != workgroupId).ToList();
                                            if (wpSameName.Count > 0)
                                            {
                                                var weight = 0.0;
                                                weight = Math.Round((wpSameName.Aggregate(weight, (current, t) => current + t.Weight.GetValueOrDefault())) / (wpSameName.Count + 1), 2);

                                                foreach (var workGroup in wpSameName)
                                                {
                                                    workGroup.Weight = weight;
                                                    this.workGroupService.Update(workGroup);
                                                }
                                                workgroupObj.Weight = weight;
                                                this.workGroupService.Update(workgroupObj);
                                            }

                                            // Update total WP count of department
                                            if (departmentObj != null && workgroupId != null)
                                            {
                                                departmentObj.CurrentWorkpackageCount += 1;
                                                this.roleService.Update(departmentObj);
                                            }

                                            // Re-calculate weight value for Project
                                            projectObj = this.scopeProjectService.GetById(workgroupObj.ProjectId.GetValueOrDefault());
                                            if (projectObj != null)
                                            {
                                                projectObj.CanDelete = false;
                                                var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);

                                                if (projectObj.IsAutoCalculate.GetValueOrDefault())
                                                {
                                                    var complete = 0.0;
                                                    complete = wpList.Aggregate(complete,
                                                        (current, t) => (current + ((t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100)));
                                                    projectObj.Complete = complete > 100 ? 100 : complete;
                                                }

                                                // Update total workpackage weight for Project
                                                var totalWeight = 0.0;
                                                totalWeight = wpList.Aggregate(totalWeight, (current, t) => current + t.Weight.GetValueOrDefault());
                                                projectObj.TotalWorkpackageWeight = totalWeight > 100 ? 100 : totalWeight;

                                                this.scopeProjectService.Update(projectObj);

                                                if (projectObj.AutoCalculateWeightWorkGroup == true)
                                                {
                                                    wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID).ToList();
                                                    var sumTotalManHours = 0.0;
                                                    sumTotalManHours = wpList.Aggregate(sumTotalManHours, (current, t) => current + t.TotalManHours.GetValueOrDefault());
                                                    foreach (var item in wpList)
                                                    {
                                                        var calWeight = ((item.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) > 0 ? ((item.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) : 0;
                                                        item.Weight = calWeight > 100 ? 100 : calWeight;
                                                        this.workGroupService.Update(item);
                                                    }
                                                }
                                            }

                                            var Notify = this.roleService.GetByID(workgroupObj.DepartmentId.GetValueOrDefault());
                                            if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false" && Notify.Notify.GetValueOrDefault())
                                            {
                                                this.NotificationAddNewWp(workgroupObj);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(dataRow["Column2"].ToString()))
                                        {
                                            errorMessage += "Workpackage \"" + dataRow["Column2"] +
                                                                "\" is missing field. <br/>";
                                        }
                                    }
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(Request.QueryString["type"]) && Request.QueryString["type"] == "update")
                        {
                            dataSheet = workbook.Worksheets[0];
                            if (!string.IsNullOrEmpty(Request.QueryString["projId"]))
                            {
                                if (Request.QueryString["projId"] != dataSheet.Cells["A1"].StringValue)
                                {
                                    errorMessage =
                                        "Workpackage master list is incorrect for this project. Please import correct file.";
                                }
                                else
                                {
                                    var projectId = Convert.ToInt32(Request.QueryString["projId"]);
                                    var projectObj = this.scopeProjectService.GetById(projectId);
                                    if (projectObj != null)
                                    {
                                        dataTable = dataSheet.Cells.ExportDataTable(2, 0, dataSheet.Cells.MaxRow, 14);
                                        foreach (DataRow dataRow in dataTable.Rows)
                                        {
                                            string[] temp = { dataRow["Column1"].ToString(), dataRow["Column2"].ToString(), dataRow["Column13"].ToString(), dataRow["Column14"].ToString() };
                                            if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) && !string.IsNullOrEmpty(dataRow["Column14"].ToString()) && dataRow["Column14"].ToString() == "1")
                                            {
                                                var workgroupId = 0;
                                                var workgroupObj = workGroupService.GetById(Convert.ToInt32(dataRow["Column1"].ToString()));
                                                if (workgroupObj == null)
                                                {
                                                    errorMessage += "Workpackage \"" + dataRow["Column2"] +
                                                                    "\" is not already exist. <br/>";
                                                }
                                                else
                                                {
                                                    workgroupId = workgroupObj.ID;
                                                    var wpRevision = this.wpRevisionService.GetByName(dataRow["Column4"].ToString());
                                                    var departmentObj = this.roleService.GetByName(dataRow["Column5"].ToString().Trim());
                                                    var areaObj = this.areaService.GetByName(dataRow["Column6"].ToString().Trim());
                                                    var disciplineObj = this.disciplineService.GetByName(dataRow["Column7"].ToString().Trim());

                                                    workgroupObj.RevisionId = wpRevision != null ? wpRevision.ID : 0;
                                                    workgroupObj.RevisionName = wpRevision != null ? wpRevision.Name : string.Empty;

                                                    workgroupObj.ProjectId = projectObj != null ? projectObj.ID : 0;
                                                    workgroupObj.ProjectName = projectObj != null ? projectObj.Name : string.Empty;

                                                    workgroupObj.ProjectManagerID = projectObj != null ? projectObj.ProjectManagerId : 0;
                                                    workgroupObj.ProjectManagerFullName = projectObj != null ? projectObj.ProjectManagerFullName : string.Empty;

                                                    workgroupObj.AreaId = areaObj != null ? areaObj.ID : 0;
                                                    workgroupObj.AreaName = areaObj != null ? areaObj.Name : string.Empty;

                                                    workgroupObj.DisciplineId = disciplineObj != null ? disciplineObj.ID : 0;
                                                    workgroupObj.DisciplineName = disciplineObj != null ? disciplineObj.Name : string.Empty;

                                                    workgroupObj.DepartmentId = departmentObj != null ? departmentObj.Id : 0;
                                                    workgroupObj.DepartmentName = departmentObj != null ? departmentObj.Name : string.Empty;

                                                    // workgroupObj.Name = dataRow["Column1"].ToString().Trim();
                                                    // workgroupObj.Description = dataRow["Column2"].ToString().Trim();

                                                    var wpType = dataRow["Column8"].ToString().Trim();
                                                    switch (wpType)
                                                    {
                                                        case "":
                                                            workgroupObj.TypeId = 0;
                                                            workgroupObj.TypeName = string.Empty;
                                                            break;
                                                        case "Planned":
                                                            workgroupObj.TypeId = 1;
                                                            workgroupObj.TypeName = "Planned";
                                                            break;
                                                        case "Unplanned":
                                                            workgroupObj.TypeId = 2;
                                                            workgroupObj.TypeName = "Unplanned";
                                                            break;
                                                    }

                                                    try
                                                    {
                                                        workgroupObj.Weight = Convert.ToDouble(dataRow["Column9"].ToString().Trim());
                                                    }
                                                    catch (Exception)
                                                    {
                                                        workgroupObj.Weight = 0;
                                                    }

                                                    try
                                                    {
                                                        workgroupObj.Complete = Convert.ToDouble(dataRow["Column10"].ToString().Trim());
                                                    }
                                                    catch (Exception)
                                                    {
                                                        workgroupObj.Complete = 0;
                                                    }

                                                    var strStartDate = dataRow["Column11"].ToString().Trim();
                                                    var startDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strStartDate, ref startDate))
                                                    {
                                                        workgroupObj.StartDate = startDate;
                                                    }


                                                    var strDeadLine = dataRow["Column12"].ToString().Trim();
                                                    var deadline = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strDeadLine, ref deadline))
                                                    {
                                                        var maxdatedoc = this.documentPackageService.GetMaxDatedLineDoc(workgroupObj.ID);
                                                        if (maxdatedoc != null)
                                                        {
                                                            if (maxdatedoc.GetValueOrDefault().Date > deadline.Date || deadline.Date > projectObj.Deadline.GetValueOrDefault().Date)
                                                            {
                                                                errorMessage += "Deadline must be between " + maxdatedoc.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + projectObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy");
                                                            }
                                                            else
                                                            {
                                                                workgroupObj.Deadline = deadline;
                                                            }
                                                        }
                                                    }


                                                    var strEndDate = dataRow["Column13"].ToString().Trim();
                                                    var endDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strEndDate, ref endDate))
                                                    {
                                                        workgroupObj.EndDate = endDate;
                                                    }


                                                    this.workGroupService.Update(workgroupObj);


                                                    if (workgroupId != null)
                                                    {
                                                        // Update weight for list workgroup have same name
                                                        // Get only previous WP, without current wp
                                                        var wpSameName = this.workGroupService.GetAllByName(workgroupObj.Name).Where(t => t.ID != workgroupId).ToList();
                                                        if (wpSameName.Count > 0)
                                                        {
                                                            var weight = 0.0;
                                                            weight = Math.Round((wpSameName.Aggregate(weight, (current, t) => current + t.Weight.GetValueOrDefault())) / (wpSameName.Count + 1), 2);

                                                            foreach (var workGroup in wpSameName)
                                                            {
                                                                workGroup.Weight = weight;
                                                                this.workGroupService.Update(workGroup);
                                                            }

                                                            workgroupObj.Weight = weight;
                                                            this.workGroupService.Update(workgroupObj);
                                                        }
                                                    }

                                                    var Notify = this.roleService.GetByID(workgroupObj.DepartmentId.GetValueOrDefault());
                                                    if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false" && Notify.Notify.GetValueOrDefault())
                                                    {
                                                        this.NotificationUpdateWp(workgroupObj);
                                                    }
                                                }
                                            }
                                        }
                                        var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                                        if (projectObj.IsAutoCalculate.GetValueOrDefault())
                                        {
                                            var complete = 0.0;
                                            complete = wpList.Aggregate(complete,
                                            (current, t) => (current + ((t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100)));
                                            projectObj.Complete = complete;
                                        }

                                        // Update total workpackage weight for Project
                                        var totalWeight = 0.0;
                                        totalWeight = wpList.Aggregate(totalWeight, (current, t) => current + t.Weight.GetValueOrDefault());
                                        projectObj.TotalWorkpackageWeight = totalWeight;

                                        projectObj.CanDelete = false;
                                        this.scopeProjectService.Update(projectObj);

                                    }
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    this.blockError.Visible = true;
                    this.lblError.Text = errorMessage;
                }
                else
                {
                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                }

            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
            }
        }

        private Folder creterfolder(string name, Folder folder, WorkGroup wp)
        {

            var trans = new Folder()
            {
                Name = name,
                Description = name,
                ParentID = folder.ID,
                DirName = folder.DirName + "/" + Regex.Replace(name, @"[^0-9a-zA-Z]+", string.Empty),
                ProjectId = wp.ProjectId,
                CreatedBy = UserSession.Current.User.Id,
                CreatedDate = DateTime.Now
            };
            Directory.CreateDirectory(Server.MapPath(trans.DirName));
            var folderId = this.folderService.Insert(trans);
            var AllUser = this.userService.GetAll(false).Where(t => !t.IsDC.GetValueOrDefault());
            var obj = this.scopeProjectService.GetById(wp.ProjectId.GetValueOrDefault());

            var addingPermission = AllUser.Where(t => t.RoleId == wp.DepartmentId.GetValueOrDefault() || t.Id == obj.ProjectManagerId || t.Id == obj.SupervisorId).Select(t => new UserDataPermission()
            {
                ProjectId = wp.ProjectId,
                RoleId = t.RoleId,
                FolderId = folderId,
                UserId = t.Id,
                IsFullPermission = true,
                CreatedDate = DateTime.Now,
                CreatedBy = UserSession.Current.User.Id,
                OnlyView = false
            });
            this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            if (name != "Native")
            {
                List<int> groupOut = ConfigurationManager.AppSettings.Get("GroupNotInVien").Split(',').Select(Int32.Parse).ToList();
                var listgroup = this.roleService.GetAll(false).Where(t => t.IsWorking.GetValueOrDefault() && !groupOut.Contains(t.Id)).Select(t => t.Id).ToList();
                var listuserview = AllUser.Where(t => listgroup.Contains(t.RoleId.GetValueOrDefault()));

                addingPermission = listuserview.Where(t => t.RoleId != wp.DepartmentId.GetValueOrDefault() && t.Id != obj.ProjectManagerId && t.Id != obj.SupervisorId).Select(t => new UserDataPermission()
                {
                    ProjectId = wp.ProjectId,
                    RoleId = t.RoleId,
                    FolderId = folderId,
                    UserId = t.Id,
                    IsFullPermission = false,
                    CreatedDate = DateTime.Now,
                    CreatedBy = UserSession.Current.User.Id,
                    OnlyView = true
                });
                this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            }
            else if (name == "Native")
            {
                var listGipAndChief = AllUser.Where(t => t.RoleId != wp.DepartmentId.GetValueOrDefault() && (t.IsGip.GetValueOrDefault() || t.IsChief.GetValueOrDefault()));
                addingPermission = listGipAndChief.Select(t => new UserDataPermission()
                {
                    ProjectId = wp.ProjectId,
                    RoleId = t.RoleId,
                    FolderId = folderId,
                    UserId = t.Id,
                    IsFullPermission = false,
                    CreatedDate = DateTime.Now,
                    CreatedBy = UserSession.Current.User.Id,
                    OnlyView = false
                });
                this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            }
            return trans;
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
        private void NotificationUpdateWp(WorkGroup wpObj)
        {
            if (wpObj.DepartmentId != 0)
            {
                var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                var gipUser = this.userService.GetByID(projectObj != null ? projectObj.ProjectManagerId.GetValueOrDefault() : 0);
                var emailList = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault())
                    .Where(t => t.IsChief.GetValueOrDefault())
                    .Select(t => t.Email)
                    .Where(t => !string.IsNullOrEmpty(t)).ToList();

                var leader = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault())
                                          .Where(t => t.IsLeader.GetValueOrDefault() && wpObj.DisciplineId == t.DisciplineID)
                                          .Select(t => t.Email)
                                          .Where(t => !string.IsNullOrEmpty(t)).ToList();
                if (gipUser != null && !string.IsNullOrEmpty(gipUser.Email))
                {
                    emailList.Add(gipUser.Email);
                }

                var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.UpdateWP);
                if (emailList.Count > 0 && notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                {
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };

                    var wpUpdatedUser = this.userService.GetByID(wpObj.UpdatedBy.GetValueOrDefault());
                    var subject = notificationTemplate.Subject.Replace("#WPName#", wpObj.Name)
                        .Replace("#WPUpdatedUser#", wpUpdatedUser != null ? wpUpdatedUser.FullName : string.Empty);

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    message.Body = notificationTemplate.Contents
                        .Replace("#WPNumber#", wpObj.Name)
                        .Replace("#WPUpdatedUser#", wpUpdatedUser != null ? wpUpdatedUser.FullName : string.Empty)
                        .Replace("#WPProjectNumber#", projectObj != null ? projectObj.Name : string.Empty)
                        .Replace("#WPProjectName#", projectObj != null ? projectObj.Description : string.Empty)
                        .Replace("#WPName#", wpObj.Description)
                        .Replace("#WPStartDate#", wpObj.StartDate != null ? wpObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPDeadline#", wpObj.Deadline != null ? wpObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPFinishDate#", wpObj.EndDate != null ? wpObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPIncomingNo#", wpObj.IncomingNo)
                        .Replace("#WPOutgoingNo#", wpObj.OutgoingNo)
                        .Replace("#WPWeight#", wpObj.Weight != null ? wpObj.Weight.Value.ToString() : string.Empty)
                        .Replace("#WPComplete#", wpObj.Complete != null ? wpObj.Complete.Value.ToString() : string.Empty);

                    foreach (var to in emailList)
                    {
                        message.To.Add(new MailAddress(to));
                    }
                    foreach (var to in leader)
                    {
                        message.CC.Add(new MailAddress(to));
                    }

                    smtpClient.Send(message);
                }
            }
        }
        private void NotificationAddNewWp(WorkGroup wpObj)
        {
            if (wpObj.DepartmentId != 0)
            {
                var emailList = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault())
                    .Where(t => t.IsChief.GetValueOrDefault())
                    .Select(t => t.Email)
                    .Where(t => !string.IsNullOrEmpty(t)).ToList();

                var leader = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault())
                                          .Where(t => t.IsLeader.GetValueOrDefault() && wpObj.DisciplineId == t.DisciplineID)
                                          .Select(t => t.Email)
                                          .Where(t => !string.IsNullOrEmpty(t)).ToList();

                var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.CreateNewWP);
                if (emailList.Count > 0 && notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                {
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };

                    var wpCreatedUser = this.userService.GetByID(wpObj.CreatedBy.GetValueOrDefault());
                    var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                    var subject = notificationTemplate.Subject.Replace("#WPNumber#", wpObj.Name).Replace("#WPDepartment#", wpObj.DepartmentName).Replace("#WPCreatedUser#", wpCreatedUser != null ? wpCreatedUser.FullName : string.Empty);

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    message.Body = notificationTemplate.Contents
                        .Replace("#WPNumber#", wpObj.Name)
                        .Replace("#WPDepartment#", wpObj.DepartmentName)
                        .Replace("#WPProjectNumber#", projectObj != null ? projectObj.Name : string.Empty)
                        .Replace("#WPProjectName#", projectObj != null ? projectObj.Description : string.Empty)
                        .Replace("#WPName#", wpObj.Description)
                        .Replace("#WPStartDate#", wpObj.StartDate != null ? wpObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPDeadline#", wpObj.Deadline != null ? wpObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPFinishDate#", wpObj.EndDate != null ? wpObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPIncomingNo#", wpObj.IncomingNo)
                        .Replace("#WPOutgoingNo#", wpObj.OutgoingNo)
                        .Replace("#WPWeight#", wpObj.Weight != null ? wpObj.Weight.Value.ToString() : string.Empty)
                        .Replace("#WPComplete#", wpObj.Complete != null ? wpObj.Complete.Value.ToString() : string.Empty);

                    foreach (var to in emailList)
                    {
                        message.To.Add(new MailAddress(to));
                    }
                    foreach (var to in leader)
                    {
                        message.CC.Add(new MailAddress(to));
                    }

                    smtpClient.Send(message);
                }
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;
using System.Collections.Generic;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using EDMs.Business.Services.Function;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;
    using System.Globalization;
    using Telerik.Web.UI;
    using Function;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentPackageInfoEditForm : Page
    {

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The scope project service.
        /// </summary>
        private readonly ScopeProjectService scopeProjectService;

        /// <summary>
        /// The document package service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;
        /// <summary>
        /// The Role Sevice
        /// </summary>
        private readonly RoleService roleService;
        /// <summary>
        /// The WorkPackage Service
        /// </summary>
        private readonly WorkGroupService workGroupService;
        /// <summary>
        /// The Notification by Email Service
        /// </summary>
        private readonly EmailNotificationTemplateService emailNotificationTemplateService;
        /// <summary>
        /// 
        /// </summary>
        private readonly AttachFilesPackageService attachFilesPackageService = new AttachFilesPackageService();
        private readonly ManhourService manhourService;
        private readonly MilestoneService milestoneService;
        private readonly ProcessActualService processActualService = new ProcessActualService();
        private readonly UserManhourService userManhourService = new UserManhourService();
        private readonly UserManhourMonthService userManhourMonthService = new UserManhourMonthService();
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();
        private readonly ProcessBussiness processBussiness = new ProcessBussiness();
        private readonly StatusService statusService = new StatusService();
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DocumentPackageInfoEditForm()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.scopeProjectService = new ScopeProjectService();
            this.documentPackageService = new DocumentPackageService();
            this.documentTypeService = new DocumentTypeService();
            this.userService = new UserService();
            this.roleService = new RoleService();
            this.workGroupService = new WorkGroupService();
            this.emailNotificationTemplateService = new EmailNotificationTemplateService();
            this.manhourService = new ManhourService();
            this.milestoneService = new MilestoneService();
        }

        /// <summary>sss
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                if (UserSession.Current.IsSuperViewer)
                {
                    this.btnSave.Visible = false;
                }
                if (UserSession.Current.IsGip)
                {
                    var projectobj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
                    if (projectobj.ProjectManagerId != UserSession.Current.User.Id)
                    {
                        this.btnSave.Visible = false;
                    }
                }
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    this.CreatedInfo.Visible = true;
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    var documentObj = this.documentPackageService.GetById(docId);
                    if (documentObj != null)
                    {
                        var projectObj = this.scopeProjectService.GetById(documentObj.ProjectId.GetValueOrDefault());
                        if (projectObj != null)
                        {
                            if (projectObj.IsIDC.GetValueOrDefault())
                            {
                                pnIDC.Visible = true;
                                pnStatus.Visible = true;
                            }
                            else
                            {
                                pnIDC.Visible = false;
                                pnStatus.Visible = false;
                            }
                        }
                        if (documentObj.HasAttachFile == true)
                        {
                            this.txtComplete.Enabled = false;
                        }
                        else
                        {
                            this.txtComplete.Enabled = true;
                        }
                        this.LoadDocInfo(documentObj);

                        //if (string.IsNullOrEmpty(this.Request.QueryString["EditRev"]))
                        //{
                        //    this.ddlRevision.Enabled = false;
                        //}

                        var createdUser = this.userService.GetByID(documentObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + documentObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (documentObj.UpdatedBy != null && documentObj.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(documentObj.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + documentObj.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                        if (UserSession.Current.IsCheif || UserSession.Current.IsLeader)
                        {
                            PermissionRole1(false);
                            PermissionRole2(true);
                            PermissionRole3(false);
                        }
                        if (UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer)
                        {
                            PermissionRole1(true);
                            PermissionRole2(true);
                            if (documentObj.HasAttachFile == true)
                            {
                                PermissionRole3(true);
                            }
                            else
                            {
                                PermissionRole3(false);
                            }
                            //if(UserSession.Current.IsGip)
                            //{
                            //    PermissionRole3(false);
                            //}
                            //else
                            //{
                            //    PermissionRole3(true);
                            //}

                        }
                        if (UserSession.Current.IsEngineer)
                        {
                            PermissionRole1(false);
                            PermissionRole2(false);


                            if (projectObj != null)
                            {
                                if (projectObj.IsIDC.GetValueOrDefault())
                                {
                                    if (documentObj.StatusID == 6 && documentObj.HasAttachFile == true)
                                    {
                                        PermissionRole3(true);
                                    }
                                    else
                                    {
                                        PermissionRole3(false);
                                    }
                                }
                                else
                                {
                                    PermissionRole3(false);
                                }
                            }

                            if (!(documentObj.EngineerId.Split(',').ToList()).Any(UserSession.Current.User.Id.ToString().Equals))
                            {
                                this.btnSave.Visible = false;
                            }
                        }
                    }
                }
                else
                {
                    var projectId = !string.IsNullOrEmpty(this.Request.QueryString["projectId"])
                        ? Convert.ToInt32(this.Request.QueryString["projectId"])
                        : 0;
                    var projectObj = this.scopeProjectService.GetById(projectId);
                    if (projectObj != null)
                    {
                        if (projectObj.IsIDC.GetValueOrDefault())
                        {
                            pnIDC.Visible = true;
                            pnStatus.Visible = true;
                            this.txtComplete.Enabled = false;
                        }
                        else
                        {
                            pnIDC.Visible = false;
                            pnStatus.Visible = false;
                            this.txtComplete.Enabled = true;
                        }
                        this.ddlProject.SelectedValue = projectObj.ID.ToString();
                    }
                    else
                    {
                        this.ddlProject.SelectedIndex = 0;
                    }
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        protected void ddlProject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var workPackageList = UserSession.Current.IsAdmin || UserSession.Current.IsDC
                 ? this.workGroupService.GetAll()
                 : this.workGroupService.GetAllByDepartment(UserSession.Current.RoleId);
            workPackageList = workPackageList.Where(t => t.ProjectId == Convert.ToInt32(ddlProject.SelectedValue)).ToList();
            this.ddlWorkgroup.DataSource = workPackageList;
            this.ddlWorkgroup.DataTextField = "FullNameRev";
            this.ddlWorkgroup.DataValueField = "ID";
            this.ddlWorkgroup.DataBind();

            if (UserSession.Current.IsGip)
            {
                var projectobj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
                if (projectobj.ProjectManagerId != UserSession.Current.User.Id)
                {
                    this.btnSave.Visible = false;
                }
            }
        }

        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"~/Images/project.png";
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                DocumentPackage documentObj;
                var oldManhourPlan = 0.0;
                bool isDocUpdate = false;

                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    documentObj = this.documentPackageService.GetById(docId);
                    if (documentObj != null)
                    {
                        oldManhourPlan = documentObj.ManHourPlan != null ? documentObj.ManHourPlan.GetValueOrDefault() : 0.0;
                        var projectObj = this.scopeProjectService.GetById(documentObj.ProjectId.GetValueOrDefault());
                        var oldWorkpackageID = documentObj.WorkgroupId.GetValueOrDefault();
                        var oldWorkpackageObj = this.workGroupService.GetById(oldWorkpackageID);
                        var oldRevisionID = documentObj.RevisionId;
                        var newRevisionID = Convert.ToInt32(this.ddlRevision.SelectedValue);

                        var newWorkpackageObj = this.workGroupService.GetById(this.ddlWorkgroup.SelectedItem != null ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue) : 0);

                        //if (newWorkpackageObj != null && this.txtDeadline.SelectedDate != null && (this.txtDeadline.SelectedDate.Value > newWorkpackageObj.Deadline.Value))
                        //{
                        //    this.lblError.Text = "Deadline of Document Greater than deadline of Workpackage.";
                        //    this.blockError.Visible = true;
                        //   // ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "<script>alert('Deadline of Document Greater than deadline of Workpackage.');</script>", false);
                        //    return;
                        //}
                        if (oldRevisionID != 0 && newRevisionID != oldRevisionID)
                        {
                            var newDocumentObj = new DocumentPackage();
                            this.UpdateDataDocument(ref newDocumentObj);

                            //clear thong tin trans khi up rev
                            this.PrepareDataBeforeUprev(ref newDocumentObj);
                            if (projectObj != null && projectObj.IsIDC.GetValueOrDefault())
                            {
                                newDocumentObj.Complete = documentObj.Complete;
                            }
                            else
                            {
                                newDocumentObj.Complete = newDocumentObj.ManHourPlan != null && newDocumentObj.ManHourPlan.Value != 0 && newDocumentObj.Complete != 100 && documentObj.ManHourPlan != newDocumentObj.ManHourPlan ? (oldManhourPlan / newDocumentObj.ManHourPlan.GetValueOrDefault()) * 100 : (newDocumentObj.Complete != null && newDocumentObj.Complete.Value < 100 ? newDocumentObj.Complete.Value : 99);
                            }

                            newDocumentObj.ParentId = documentObj.ParentId ?? documentObj.ID;
                            newDocumentObj.Notes = documentObj.Notes;

                            var save = !documentPackageService.IsExist(newDocumentObj.DocNo, newDocumentObj.RevisionName, newWorkpackageObj.ID) ? this.documentPackageService.Insert(newDocumentObj) : -1;
                            if (save != -1 && documentObj.WorkgroupId.GetValueOrDefault() == newDocumentObj.WorkgroupId.GetValueOrDefault())
                            {
                                // Upate old doc
                                documentObj.IsLeaf = false;
                                isDocUpdate = true;
                            }
                        }
                        else
                        {
                            this.UpdateDataDocument(ref documentObj);
                            isDocUpdate = true;
                        }

                        documentObj.UpdatedBy = UserSession.Current.User.Id;
                        documentObj.UpdatedDate = DateTime.Now;
                        this.documentPackageService.Update(documentObj);

                        //document multi enggineer
                        if (documentObj.EngineerId.Contains(","))
                        {
                            var en = this.userService.GetByID(UserSession.Current.User.Id);

                            // Send Notification update
                            if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false" && en != null && en.Role.Notify.GetValueOrDefault())
                            {
                                this.NotificationUpdateDoc(documentObj);
                            }
                        }
                        else
                        {
                            var en = this.userService.GetByID(documentObj.EngineerId);

                            // Send Notification update
                            if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false" && en != null && en.Role.Notify.GetValueOrDefault())
                            {
                                this.NotificationUpdateDoc(documentObj);
                            }
                        }
                        //ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "<script>alert('Update successfully.') </script>", false);
                    }
                }
                else
                {
                    //var newWorkpackageObj = this.workGroupService.GetById(this.ddlWorkgroup.SelectedItem != null ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue) : 0);

                    //if (newWorkpackageObj != null && this.txtDeadline.SelectedDate != null && (this.txtDeadline.SelectedDate.Value > newWorkpackageObj.Deadline.Value))
                    //{
                    //    this.lblError.Text = "Deadline of Document Greater than deadline of Workpackage.";
                    //    this.blockError.Visible = true;
                    //    ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "<script>alert('Deadline of Document Greater than deadline of Workpackage.');</script>", false);
                    //    return;
                    //}
                    documentObj = new DocumentPackage()
                    {
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        IsLeaf = true,
                        HasAttachFile = false
                    };

                    this.UpdateDataDocument(ref documentObj);

                    // if (!documentPackageService.IsExist(documentObj.DocNo, documentObj.RevisionId.GetValueOrDefault(), documentObj.WorkgroupId.GetValueOrDefault()))
                    //{
                    this.documentPackageService.Insert(documentObj);
                    isDocUpdate = true;
                    //}
                    //else
                    //{
                    //    this.blockError.Visible = true;
                    //    this.lblError.Text = "Document No. is already exist. ";
                    //    return;
                    //}

                    //document multi enggineer
                    if (documentObj.EngineerId.Contains(","))
                    {
                        var en = this.userService.GetByID(UserSession.Current.User.Id);

                        // Send Notification update
                        if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false" && en != null && en.Role.Notify.GetValueOrDefault())
                        {
                            this.NotificationAddNewDoc(documentObj);
                        }
                    }
                    else
                    {
                        var en = this.userService.GetByID(documentObj.EngineerId);

                        // Send Notification update
                        if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false" && en != null && en.Role.Notify.GetValueOrDefault())
                        {
                            this.NotificationAddNewDoc(documentObj);
                        }
                    }
                }
                if (documentObj != null)
                {
                    var wpObj = this.workGroupService.GetById(documentObj.WorkgroupId.GetValueOrDefault());
                    if (wpObj != null)
                    {
                        if (isDocUpdate)
                        {
                            var docList = this.documentPackageService.GetAllEMDRByWorkgroup(wpObj.ID);
                            var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                            double complete = 0;
                            Milestone milestoneObj = new Milestone();
                            ProcessActual processActualObj = new ProcessActual();
                            List<WorkgroupManhourMonth> wmmList = new List<WorkgroupManhourMonth>();
                            WorkgroupManhourMonth wmmObj = new WorkgroupManhourMonth();
                            UserManhour userManhourObj = new UserManhour();
                            UserManhourMonth userManhourMonthObj = new UserManhourMonth();

                            //update weight cho document
                            UpdateWeightDocument(projectObj, ref docList);
                            var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                            //update data workpackage
                            UpdateDataWorkpackage(projectObj, docList, ref wpObj, ref wpList, ref complete);
                            if (wpObj.IsAutoCalculate.GetValueOrDefault())
                            {
                                if ((complete < 100 && docList.Where(t => t.Complete != 100).Any()))
                                {
                                    //update milestone
                                    UpdateMilestonActual(wpObj, UserSession.Current.User, milestoneObj);
                                }
                            }
                            //Update actual progress
                            UpdateProgressActual(wpObj, processActualObj);

                            // update data Project
                            UpdateDataProject(wpList, projectObj);

                            //Update all wpWeight trong thang workgroup manhour month cua project
                            UpdateWeightWorkgroupManhourMonth(wpObj, wmmList);

                            UpdateDataWorkgroupManhourMonth(wpObj, UserSession.Current.User, wpList, wmmObj);

                            //update manhour
                            UpdateUserManhourWeek(documentObj, UserSession.Current.User, userManhourObj);
                            UpdateUserManhourMonth(documentObj, UserSession.Current.User, userManhourMonthObj);

                            //ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if (this.txtDocNumber.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Document No.";
                this.divDocNo.Style["margin-bottom"] = "-5px;";
                args.IsValid = false;
            }
            else
            {
                var docId = !string.IsNullOrEmpty(Request.QueryString["docId"])
                    ? Convert.ToInt32(Request.QueryString["docId"])
                    : 0;

                var wpId = this.ddlWorkgroup.SelectedItem != null
                    ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue)
                    : 0;

                if (this.documentPackageService.IsExist(this.txtDocNumber.Text.Trim(), wpId) && docId == 0)
                {
                    this.fileNameValidator.ErrorMessage = "Document No. is already exist.";
                    this.divDocNo.Style["margin-bottom"] = "-5px;";
                    args.IsValid = false;
                }
            }
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
                var fileName = e.Argument.Split('$')[1];
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);

                if (this.documentService.IsDocumentExist(folderId, fileName))
                {
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var projectId = !string.IsNullOrEmpty(this.Request.QueryString["projectId"])
                        ? Convert.ToInt32(this.Request.QueryString["projectId"])
                        : 0;
            var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
            var documentObj = this.documentPackageService.GetById(docId);
            if (projectId == 0)
            {
                var pj = this.scopeProjectService.GetById(documentObj.ProjectId.GetValueOrDefault());
                if (pj != null)
                {
                    projectId = pj.ID;
                }
            }

            var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
            var currentDepartment = UserSession.Current.User.RoleId;

            var WorkpackageList = isSeeFull
                ? this.workGroupService.GetAll().ToList()
                : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()).ToList();
            var wpList = new List<WorkGroup>();
            if (UserSession.Current.IsEngineer)
            {
                var wpInWorkOfEng =
                    this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id)
                        .Select(t => t.WorkgroupId)
                        .Distinct();
                wpList = WorkpackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
            }
            else
            {
                wpList = WorkpackageList;
            }

            var projectInPermission = this.scopeProjectService.GetAll(wpList.Select(t => t.ProjectId.GetValueOrDefault()).Distinct().ToList());

            //if (UserSession.Current.IsGip)
            //{
            //    projectInPermission = projectInPermission.Where(t => t.ProjectManagerId == UserSession.Current.User.Id).ToList();
            //}

            this.ddlProject.DataSource = projectInPermission;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();

            int indexpj = this.ddlProject.FindItemIndexByValue(projectId.ToString());
            this.ddlProject.SelectedIndex = indexpj;

            this.ddlWorkgroup.DataSource = wpList.Where(
                                                    t =>
                                                        t.ProjectId ==
                                                        (this.ddlProject.SelectedItem != null
                                                            ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                                            : 0));
            this.ddlWorkgroup.DataTextField = "FullNameRev";
            this.ddlWorkgroup.DataValueField = "ID";
            this.ddlWorkgroup.DataBind();

            var projectObj = this.scopeProjectService.GetById(projectId);
            if (projectObj != null)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString["wpId"]))
                    {
                        var selectedWorkpackage = this.workGroupService.GetById(Convert.ToInt32(this.Request.QueryString["wpId"]));
                        if (selectedWorkpackage != null)
                        {
                            this.ddlWorkgroup.SelectedValue = selectedWorkpackage.ID.ToString();
                            this.txtDocNumber.Text = this.ddlWorkgroup.SelectedItem != null
                                ? this.ddlWorkgroup.SelectedItem.Text
                                : string.Empty;
                            BindDataEngineerAndLeader(selectedWorkpackage.DepartmentId.GetValueOrDefault());
                        }
                    }
                    else
                    {
                        if (documentObj != null)
                        {
                            var selectedWorkpackage = this.workGroupService.GetById(documentObj.WorkgroupId.GetValueOrDefault());
                            if (selectedWorkpackage != null)
                            {
                                this.ddlWorkgroup.SelectedValue = documentObj.WorkgroupId.GetValueOrDefault().ToString();
                                this.txtDocNumber.Text = this.ddlWorkgroup.SelectedItem != null
                                    ? this.ddlWorkgroup.SelectedItem.Text
                                    : string.Empty;
                                BindDataEngineerAndLeader(selectedWorkpackage.DepartmentId.GetValueOrDefault());
                            }
                        }
                    }
                    if (documentObj != null)
                    {
                        if (projectObj.IsIDC.GetValueOrDefault())
                        {
                            if (documentObj.StatusID == 6 && UserSession.Current.IsEngineer)
                            {
                                var listRevision = this.revisionService.GetByActive().Where(t => t.StatusID == 6).ToList();
                                listRevision.Insert(0, new Revision { ID = 0 });
                                this.ddlRevision.DataSource = listRevision.OrderBy(t => t.StatusID).ThenBy(t => t.Description);
                                this.ddlRevision.DataTextField = "Name";
                                this.ddlRevision.DataValueField = "ID";
                                this.ddlRevision.DataBind();
                            }
                            else
                            {
                                var listRevision = this.revisionService.GetByActive().ToList();
                                listRevision.Insert(0, new Revision { ID = 0 });
                                this.ddlRevision.DataSource = listRevision.OrderBy(t => t.StatusID).ThenBy(t => t.Description);
                                this.ddlRevision.DataTextField = "Name";
                                this.ddlRevision.DataValueField = "ID";
                                this.ddlRevision.DataBind();
                            }
                        }
                        else
                        {
                            var listRevision = this.revisionService.GetByActive().Where(t => t.StatusID == 8).ToList();
                            listRevision.Insert(0, new Revision { ID = 0 });
                            this.ddlRevision.DataSource = listRevision.OrderBy(t => t.StatusID).ThenBy(t => t.Description);
                            this.ddlRevision.DataTextField = "Name";
                            this.ddlRevision.DataValueField = "ID";
                            this.ddlRevision.DataBind();
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString["wpId"]))
                    {
                        var selectedWorkpackage = this.workGroupService.GetById(Convert.ToInt32(this.Request.QueryString["wpId"]));
                        if (selectedWorkpackage != null)
                        {
                            this.ddlWorkgroup.SelectedValue = selectedWorkpackage.ID.ToString();
                            this.txtDocNumber.Text = this.ddlWorkgroup.SelectedItem != null
                                ? this.ddlWorkgroup.SelectedItem.Text
                                : string.Empty;
                            BindDataEngineerAndLeader(selectedWorkpackage.DepartmentId.GetValueOrDefault());
                            if (selectedWorkpackage.IsAutoCalculate.GetValueOrDefault())
                            {
                                this.txtWeight.Enabled = false;
                            }
                        }
                    }
                    else
                    {
                        var selectedWorkpackage = this.workGroupService.GetById(Convert.ToInt32(this.ddlWorkgroup.SelectedValue));
                        if (selectedWorkpackage != null)
                        {
                            this.txtDocNumber.Text = this.ddlWorkgroup.SelectedItem != null
                                ? this.ddlWorkgroup.SelectedItem.Text
                                : string.Empty;
                            BindDataEngineerAndLeader(selectedWorkpackage.DepartmentId.GetValueOrDefault());
                            if (selectedWorkpackage.IsAutoCalculate.GetValueOrDefault())
                            {
                                this.txtWeight.Enabled = false;
                            }
                        }
                    }
                    if (projectObj.IsIDC.GetValueOrDefault())
                    {
                        var listRevision = this.revisionService.GetByActive().ToList();
                        listRevision.Insert(0, new Revision { ID = 0 });
                        this.ddlRevision.DataSource = listRevision.OrderBy(t => t.StatusID).ThenBy(t => t.Description);
                        this.ddlRevision.DataTextField = "Name";
                        this.ddlRevision.DataValueField = "ID";
                        this.ddlRevision.DataBind();
                    }
                    else
                    {
                        var listRevision = this.revisionService.GetByActive().Where(t => t.StatusID == 8).ToList();
                        listRevision.Insert(0, new Revision { ID = 0 });
                        this.ddlRevision.DataSource = listRevision.OrderBy(t => t.StatusID).ThenBy(t => t.Description);
                        this.ddlRevision.DataTextField = "Name";
                        this.ddlRevision.DataValueField = "ID";
                        this.ddlRevision.DataBind();
                    }
                }

            }

            var listDocumentType = this.documentTypeService.GetAll();
            listDocumentType.Insert(0, new DocumentType() { ID = 0 });
            this.ddlDocType.DataSource = listDocumentType;
            this.ddlDocType.DataTextField = "FullName";
            this.ddlDocType.DataValueField = "ID";
            this.ddlDocType.DataBind();

            List<string> Level = new List<string>() { "", "High", "Medium", "Low" };
            this.level.DataSource = Level;
            this.level.DataBind();

            var statusList = this.statusService.GetIsActive();
            statusList.Insert(0, new Status() { ID = 0 });
            this.ddlStatus.DataSource = statusList;
            this.ddlStatus.DataValueField = "ID";
            this.ddlStatus.DataTextField = "Name";
            this.ddlStatus.DataBind();
        }

        private void BindDataEngineerAndLeader(int departmentId)
        {
            var userList = this.userService.GetAllByRoleId(departmentId).OrderBy(t => t.Username).ToList();
            this.ddlEngineer.DataSource = userList;
            this.ddlEngineer.DataTextField = "UserNameWithFullName";
            this.ddlEngineer.DataValueField = "Id";
            this.ddlEngineer.DataBind();
            //var leader = userList.Where(t => t.IsLeader.GetValueOrDefault()).ToList();
            userList.Insert(0, new User { Id = 0 });
            this.ddlChecker.DataSource = userList;
            this.ddlChecker.DataTextField = "UserNameWithFullName";
            this.ddlChecker.DataValueField = "Id";
            this.ddlChecker.DataBind();
        }

        private void PrepareDataBeforeUprev(ref DocumentPackage documentObj)
        {
            documentObj.OutgoingTransNo = null;
            documentObj.OutgoingTransDate = null;
            documentObj.DealineComment = null;
            documentObj.IncomingTransNo = null;
            documentObj.IncomingTransDate = null;
            documentObj.FinalCodeID = null;
            documentObj.FinalCodeName = null;
            documentObj.OutgoingLeterNo = null;
            documentObj.OutgoingLeterDate = null;
            documentObj.CopyNumber = null;
            documentObj.EndDate = null;
            documentObj.CreatedBy = UserSession.Current.User.Id;
            documentObj.CreatedDate = DateTime.Now;
            documentObj.IsLeaf = true;
            documentObj.IsDelete = false;
        }

        private void UpdateDataDocument(ref DocumentPackage documentObj)
        {
            var workgroup = this.workGroupService.GetById(this.ddlWorkgroup.SelectedItem != null
                ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue)
                : 0);

            documentObj.WorkgroupId = workgroup != null ? workgroup.ID : 0;
            documentObj.WorkgroupName = workgroup != null ? workgroup.Name : string.Empty;
            documentObj.WorkgroupFullName = workgroup != null ? workgroup.FullName : string.Empty;
            documentObj.WorkgroupRevName = workgroup != null ? workgroup.FullNameRev : string.Empty;

            documentObj.ProjectId = !string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            documentObj.ProjectName = this.ddlProject.SelectedItem.Text;

            documentObj.RevisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
            documentObj.RevisionName = this.ddlRevision.SelectedItem.Text;

            documentObj.DocNo = this.txtDocNumber.Text.Trim();
            documentObj.DocTitle = this.txtDocumentTitle.Text.Trim();
            documentObj.DocumentTypeId = this.ddlDocType.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDocType.SelectedValue)
                                        : 0;
            documentObj.DocumentTypeName = this.ddlDocType.SelectedItem != null
                                          ? this.ddlDocType.SelectedItem.Text
                                          : string.Empty;
            //Select multi Engineer
            var collection = ddlEngineer.CheckedItems;
            if (collection.Count > 0)
            {
                if (collection.Count > 1)
                {
                    List<string> listEngID = new List<string>();
                    List<string> listEngName = new List<string>();
                    foreach (var item in collection)
                    {
                        listEngID.Add(item.Value);
                        listEngName.Add(item.Text);
                    }
                    documentObj.EngineerId = string.Join(",", listEngID.ToArray());
                    documentObj.EngineerName = string.Join(",", listEngName.ToArray());
                }
                else
                {
                    var item = collection.First();
                    var engineer = this.ddlEngineer.SelectedItem != null ? this.userService.GetByID(Convert.ToInt32(item.Value)) : null;

                    documentObj.EngineerId = this.ddlEngineer.SelectedItem != null ? item.Value : "0";
                    documentObj.EngineerName = this.ddlEngineer.SelectedItem != null ? item.Text : string.Empty;
                }

            }
            documentObj.LeaderId = Convert.ToInt32(ddlChecker.SelectedValue);
            documentObj.LeaderName = ddlChecker.SelectedItem.Text;
            documentObj.PersonsInvolved = this.txtPersonsInvolved.Text;
            documentObj.Notes = this.txtNote.Text;
            documentObj.DeparmentId = workgroup != null ? workgroup.DepartmentId : 0;

            documentObj.ManHourPlan = this.txtManHourplan.Value;
            documentObj.ManHours = this.txtManHour.Value;
            var hasAttachFile = this.attachFilesPackageService.GetAllDocId(documentObj.ID).Any();
            documentObj.HasAttachFile = hasAttachFile;
            documentObj.Complete = this.txtComplete.Value.GetValueOrDefault();
            documentObj.Weight = this.txtWeight.Value.GetValueOrDefault();

            documentObj.StartDate = this.txtStartDate.SelectedDate;
            documentObj.Deadline = this.txtDeadline.SelectedDate <= workgroup.Deadline ? this.txtDeadline.SelectedDate : workgroup.Deadline;
            documentObj.EndDate = this.txtEndDate.SelectedDate;
            documentObj.IDCDeadline = this.txtIDCDeadline.SelectedDate;
            documentObj.IDCEndDate = this.txtIDCEndDate.SelectedDate;
            documentObj.IFRDeadline = this.txtIFRDeadline.SelectedDate;
            documentObj.IFREndDate = this.txtIFREndDate.SelectedDate;

            documentObj.IsoReviseDate = this.txtISODate.SelectedDate;
            documentObj.OutgoingTransNo = this.txtOutgoingTransNo.Text;
            documentObj.OutgoingTransDate = this.txtOutgoingTransDate.SelectedDate;
            documentObj.OutgoingLeterNo = this.txtOutgoingLeterNo.Text;
            documentObj.OutgoingLeterDate = this.txtOutgoingLeterDate.SelectedDate;
            documentObj.IsEMDR = this.cbIsEMDR.Checked;
            documentObj.ICAReviewOutTransNo = this.txticatransno.Text;
            documentObj.ICAReviewOutDate = this.txticadate.SelectedDate;
            documentObj.ICAReviewCode = this.txtreviewcode.Text;
            documentObj.Markup = this.txtmarkup.Text;
            documentObj.StatusID = Convert.ToInt32(this.ddlStatus.SelectedValue);
            documentObj.StatusName = this.ddlStatus.SelectedItem.Text;
            documentObj.FinalCodeName = this.txtFinalcode.Text;
            documentObj.CopyNumber = this.txtCopyNumber.Text;
            var value = this.level.SelectedItem.Text;
            switch (value)
            {
                case "High":
                    documentObj.LevelPriority = 3;
                    break;
                case "Medium":
                    documentObj.LevelPriority = 2;
                    break;
                case "Low":
                    documentObj.LevelPriority = 1;
                    break;
                default:
                    documentObj.LevelPriority = 0;
                    break;
            }

        }

        private void LoadDocInfo(DocumentPackage documentObj)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["projectId"]))
            {
                var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["projectId"]));
                if (projectObj != null)
                {
                    this.ddlProject.SelectedValue = projectObj.ID.ToString();
                }
                else
                {
                    this.ddlProject.SelectedIndex = 0;
                }
            }
            else
            {
                var projectObj = this.scopeProjectService.GetById(documentObj.ProjectId.GetValueOrDefault());
                if (projectObj != null)
                {
                    this.ddlProject.SelectedValue = projectObj.ID.ToString();
                }
                else
                {
                    this.ddlProject.SelectedIndex = 0;
                }
            }

            this.txtDocNumber.Text = documentObj.DocNo;
            this.txtDocumentTitle.Text = documentObj.DocTitle;
            this.ddlWorkgroup.SelectedValue = documentObj.WorkgroupId.ToString();
            this.ddlDocType.SelectedValue = documentObj.DocumentTypeId.ToString();
            this.ddlRevision.SelectedValue = documentObj.RevisionId.ToString();
            this.txtManHour.Value = documentObj.ManHours;
            this.txtManHourplan.Value = documentObj.ManHourPlan;
            this.txtStartDate.SelectedDate = documentObj.StartDate;
            this.txtDeadline.SelectedDate = documentObj.Deadline;
            this.txtEndDate.SelectedDate = documentObj.EndDate;
            this.txtISODate.SelectedDate = documentObj.IsoReviseDate;
            this.txtNote.Text = documentObj.Notes;
            this.txtComplete.Value = documentObj.Complete;
            this.txtWeight.Value = documentObj.Weight;
            this.txtIDCDeadline.SelectedDate = documentObj.IDCDeadline;
            this.txtIDCEndDate.SelectedDate = documentObj.IDCEndDate;
            this.txtIFRDeadline.SelectedDate = documentObj.IFRDeadline;
            this.txtIFREndDate.SelectedDate = documentObj.IFREndDate;

            this.txtOutgoingTransNo.Text = documentObj.OutgoingTransNo;
            this.txtOutgoingTransDate.SelectedDate = documentObj.OutgoingTransDate;

            this.txtOutgoingLeterNo.Text = documentObj.OutgoingLeterNo;
            this.txtOutgoingLeterDate.SelectedDate = documentObj.OutgoingLeterDate;
            this.txtCopyNumber.Text = documentObj.CopyNumber;

            this.txtIncomingTransDate.SelectedDate = documentObj.IncomingTransDate;
            this.txtIncomingTransNo.Text = documentObj.IncomingTransNo;
            this.txtPersonsInvolved.Text = documentObj.PersonsInvolved;
            var value = documentObj.LevelPriority;

            this.txticatransno.Text = documentObj.ICAReviewOutTransNo;
            this.txticadate.SelectedDate = documentObj.ICAReviewOutDate;
            this.txtreviewcode.Text = documentObj.ICAReviewCode;
            this.txtmarkup.Text = documentObj.Markup;
            if (string.IsNullOrEmpty(documentObj.StatusID.ToString()))
            {
                var revObj = this.revisionService.GetById(Convert.ToInt32(ddlRevision.SelectedValue));
                this.ddlStatus.SelectedValue = revObj.StatusID.ToString();
            }
            else
            {
                this.ddlStatus.SelectedValue = documentObj.StatusID.ToString();
            }

            this.cbIsEMDR.Checked = documentObj.IsEMDR;
            this.txtFinalcode.Text = documentObj.FinalCodeName;
            switch (value)
            {
                case 3:
                    this.level.SelectedIndex = 1;
                    break;
                case 2:
                    this.level.SelectedIndex = 2;
                    break;
                case 1:
                    this.level.SelectedIndex = 3;
                    break;
                default:
                    this.level.SelectedIndex = 0;
                    break;
            }

            //checked multi enggineer
            if (documentObj.EngineerId.Contains(","))
            {
                var listEng = documentObj.EngineerId.Split(',');
                foreach (var item in listEng)
                {
                    this.ddlEngineer.SelectedValue = item;
                    if (ddlEngineer.SelectedItem != null)
                    {
                        this.ddlEngineer.SelectedItem.Checked = true;
                    }
                }
            }
            else
            {
                this.ddlEngineer.SelectedValue = documentObj.EngineerId;
                if (ddlEngineer.SelectedItem != null)
                {
                    this.ddlEngineer.SelectedItem.Checked = true;
                }
            }
            this.ddlChecker.SelectedValue = documentObj.LeaderId.ToString();
        }

        protected void ddlWorkgroup_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedWorkpackage = this.workGroupService.GetById(Convert.ToInt32(this.ddlWorkgroup.SelectedValue));
            if (selectedWorkpackage != null)
            {
                this.txtDocNumber.Text = selectedWorkpackage.Name;
                var userList = this.userService.GetAllByRoleId(selectedWorkpackage.DepartmentId.GetValueOrDefault()).OrderBy(t => t.Username).ToList();
                //userList.Insert(0, new User { Id = 0 });
                this.ddlEngineer.DataSource = userList;
                this.ddlEngineer.DataTextField = "UserNameWithFullName";
                this.ddlEngineer.DataValueField = "Id";
                this.ddlEngineer.DataBind();
                //var leader = userList.Where(t => t.IsLeader.GetValueOrDefault()).ToList();
                userList.Insert(0, new User { Id = 0 });
                this.ddlChecker.DataSource = userList;
                this.ddlChecker.DataTextField = "UserNameWithFullName";
                this.ddlChecker.DataValueField = "Id";
                this.ddlChecker.DataBind();
            }
        }

        private void NotificationAddNewDoc(DocumentPackage documentObj)
        {
            if (documentObj.EngineerId != "0")
            {
                var engineer = new List<User>();
                if (documentObj.EngineerId.Contains(","))
                {
                    var listEng = documentObj.EngineerId.Split(',');
                    foreach (var item in listEng)
                    {
                        engineer.Add(this.userService.GetByID(item));
                    }
                }
                else
                {
                    engineer.Add(this.userService.GetByID(Convert.ToInt32(documentObj.EngineerId)));
                }
                if (engineer != null && engineer.Count > 0)
                {
                    string strEnginner = string.Join(" - ", engineer.Select(t => t.FullName));
                    var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.CreateNewDoc);
                    if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                    {
                        var smtpClient = new SmtpClient
                        {
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                            EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                            Host = ConfigurationManager.AppSettings["Host"],
                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                            Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                        };

                        var wpObj = this.workGroupService.GetById(documentObj.WorkgroupId.GetValueOrDefault());
                        var subject = notificationTemplate.Subject.Replace("#DocNumber#", documentObj.DocNo).Replace("#AssignedUser#", strEnginner);

                        var message = new MailMessage();
                        message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                        message.Subject = subject;
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = notificationTemplate.Contents
                            .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                            .Replace("#DocNumber#", documentObj.DocNo)
                            .Replace("#AssignedUser#", strEnginner)
                            .Replace("#DocTitle#", documentObj.DocTitle)
                            .Replace("#DocType#", documentObj.DocumentTypeName)
                            .Replace("#DocRev#", documentObj.RevisionName)
                            .Replace("#DocEng#", strEnginner)
                            .Replace("#DocStartDate#", documentObj.StartDate != null ? documentObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocDeadline#", documentObj.Deadline != null ? documentObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocFinishDate#", documentObj.EndDate != null ? documentObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocISODate#", documentObj.IsoReviseDate != null ? documentObj.IsoReviseDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocWeight#", documentObj.Weight != null ? documentObj.Weight.Value.ToString() : string.Empty)
                            .Replace("#DocComplete#", documentObj.Complete != null ? documentObj.Complete.Value.ToString() : string.Empty);


                        if (ConfigurationManager.AppSettings["SendOnlySupport"].ToLower() == "true")
                        {
                            message.To.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                        }
                        else
                        {
                            foreach (var item in engineer)
                            {
                                if (!string.IsNullOrEmpty(item.Email))
                                {
                                    message.To.Add(item.Email);
                                }
                            }
                            var leader = this.userService.GetByID(documentObj.LeaderId.ToString());
                            message.To.Add(leader.Email);
                        }

                        smtpClient.Send(message);
                    }
                }
            }
        }

        private void NotificationUpdateDoc(DocumentPackage documentObj)
        {
            if (documentObj.EngineerId != "0")
            {
                var engineer = new List<User>();
                if (documentObj.EngineerId.Contains(","))
                {
                    var listEng = documentObj.EngineerId.Split(',');
                    foreach (var item in listEng)
                    {
                        engineer.Add(this.userService.GetByID(item));
                    }
                }
                else
                {
                    engineer.Add(this.userService.GetByID(Convert.ToInt32(documentObj.EngineerId)));
                }

                if (engineer != null && engineer.Count > 0)
                {
                    string strEnginner = string.Join(" - ", engineer.Select(t => t.FullName));

                    var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.UpdateDoc);
                    if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                    {
                        var smtpClient = new SmtpClient
                        {
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                            EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                            Host = ConfigurationManager.AppSettings["Host"],
                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                            Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                        };
                        var wpObj = this.workGroupService.GetById(documentObj.WorkgroupId.GetValueOrDefault());
                        var updatedUser = this.userService.GetByID(UserSession.Current.User.Id);
                        var subject = notificationTemplate.Subject.Replace("#DocNumber#", documentObj.DocNo)
                            .Replace("#UpdatedUser#", updatedUser != null ? updatedUser.FullName : string.Empty);

                        var message = new MailMessage();
                        message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                        message.Subject = subject;
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = notificationTemplate.Contents
                            .Replace("#UpdatedUser#", updatedUser != null ? updatedUser.FullName : string.Empty)
                            .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                            .Replace("#DocNumber#", documentObj.DocNo)
                            .Replace("#DocTitle#", documentObj.DocTitle)
                            .Replace("#DocType#", documentObj.DocumentTypeName)
                            .Replace("#DocRev#", documentObj.RevisionName)
                            .Replace("#DocEng#", strEnginner)
                            .Replace("#DocStartDate#", documentObj.StartDate != null ? documentObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocDeadline#", documentObj.Deadline != null ? documentObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocFinishDate#", documentObj.EndDate != null ? documentObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocISODate#", documentObj.IsoReviseDate != null ? documentObj.IsoReviseDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocWeight#", documentObj.Weight != null ? documentObj.Weight.Value.ToString() : string.Empty)
                            .Replace("#DocComplete#", documentObj.Complete != null ? documentObj.Complete.Value.ToString() : string.Empty);

                        if (ConfigurationManager.AppSettings["SendOnlySupport"].ToLower() == "true")
                        {
                            message.To.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                        }
                        else
                        {
                            foreach (var item in engineer)
                            {
                                if (!string.IsNullOrEmpty(item.Email))
                                {
                                    message.To.Add(item.Email);
                                }
                            }
                            var leader = this.userService.GetByID(documentObj.LeaderId.ToString());
                            message.To.Add(leader.Email);
                        }

                        smtpClient.Send(message);
                    }
                }
            }
        }

        protected void ServerValidatecomplete(object source, ServerValidateEventArgs args)
        {
            var temp = this.txtComplete.Value;
            if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
            {
                var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                var documentObj = this.documentPackageService.GetById(docId);
                if (documentObj != null)
                {
                    var projectObj = this.scopeProjectService.GetById(documentObj.ProjectId.GetValueOrDefault());
                    if (projectObj != null)
                    {
                        if (projectObj.IsIDC.GetValueOrDefault())
                        {
                            var statusObj = this.statusService.GetById(Convert.ToInt32(ddlStatus.SelectedValue));
                            if (statusObj != null)
                            {
                                if (!documentObj.HasAttachFile.GetValueOrDefault())
                                {
                                    var idcComplete = this.statusService.GetById(6).Complete.GetValueOrDefault();
                                    var ifrComplete = this.statusService.GetById(7).Complete.GetValueOrDefault();
                                    var ifaComplete = this.statusService.GetById(8).Complete.GetValueOrDefault();
                                    if (statusObj.ID == 6 && temp >= idcComplete)
                                    {
                                        this.Validatorcomplete.ErrorMessage = "Complete IDC between 0 and 60";
                                        this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                        args.IsValid = false;
                                    }
                                    else if (statusObj.ID == 7 && (temp >= ifrComplete || temp < idcComplete))
                                    {
                                        this.Validatorcomplete.ErrorMessage = "Complete IFR between 60 and 80";
                                        this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                        args.IsValid = false;
                                    }
                                    else if (statusObj.ID == 8 && (temp >= ifaComplete || temp < ifrComplete))
                                    {
                                        this.Validatorcomplete.ErrorMessage = "Complete IFA between 80 and 100";
                                        this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                        args.IsValid = false;
                                    }
                                }
                            }
                            else
                            {
                                this.Validatorcomplete.ErrorMessage = "Please select status.";
                                this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                args.IsValid = false;
                            }
                        }
                        else
                        {
                            if (this.txtComplete.Value.GetValueOrDefault() != 0 && this.txtComplete.Value.GetValueOrDefault() >= 100)
                            {
                                if (UserSession.Current.IsEngineer)
                                {
                                    this.Validatorcomplete.ErrorMessage = "Please enter the less than 100%.";
                                    this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                    args.IsValid = false;
                                }
                                else
                                {
                                    if (!documentObj.HasAttachFile.GetValueOrDefault())
                                    {
                                        this.Validatorcomplete.ErrorMessage = "Please enter the less than 100%.";
                                        this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                        args.IsValid = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["projectId"]))
                {
                    var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["projectId"]));
                    if (projectObj != null)
                    {
                        if (projectObj.IsIDC.GetValueOrDefault())
                        {
                            var statusObj = this.statusService.GetById(Convert.ToInt32(ddlStatus.SelectedValue));
                            if (statusObj != null)
                            {
                                var idcComplete = this.statusService.GetById(6).Complete.GetValueOrDefault();
                                var ifrComplete = this.statusService.GetById(7).Complete.GetValueOrDefault();
                                var ifaComplete = this.statusService.GetById(8).Complete.GetValueOrDefault();
                                if (statusObj.ID == 6 && temp >= idcComplete)
                                {
                                    this.Validatorcomplete.ErrorMessage = "Complete IDC between 0 and 60";
                                    this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                    args.IsValid = false;
                                }
                                else if (statusObj.ID == 7 && (temp >= ifrComplete || temp < idcComplete))
                                {
                                    this.Validatorcomplete.ErrorMessage = "Complete IFR between 60 and 80";
                                    this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                    args.IsValid = false;
                                }
                                else if (statusObj.ID == 8 && (temp >= ifaComplete || temp < ifrComplete))
                                {
                                    this.Validatorcomplete.ErrorMessage = "Complete IFA between 80 and 100";
                                    this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                    args.IsValid = false;
                                }
                            }
                            else
                            {
                                this.Validatorcomplete.ErrorMessage = "Please select status.";
                                this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                args.IsValid = false;
                            }
                        }
                        else
                        {
                            if (this.txtComplete.Value.GetValueOrDefault() != 0 && this.txtComplete.Value.GetValueOrDefault() >= 100)
                            {
                                if (UserSession.Current.IsEngineer)
                                {
                                    this.Validatorcomplete.ErrorMessage = "Please enter the less than 100%.";
                                    this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                    args.IsValid = false;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                                    {
                                        var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                                        var documentObj = this.documentPackageService.GetById(docId);
                                        if (documentObj != null)
                                        {
                                            if (!documentObj.HasAttachFile.GetValueOrDefault())
                                            {
                                                this.Validatorcomplete.ErrorMessage = "Please enter the less than 100%.";
                                                this.Divcomplete.Style["margin-bottom"] = "-5px;";
                                                args.IsValid = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private List<DateTime> GetBusinessDays(DateTime start, DateTime end)
        {
            List<DateTime> listdate = new List<DateTime>();
            for (var i = start; i <= end; i = i.AddDays(1))
            {
                if (i.DayOfWeek != DayOfWeek.Saturday && i.DayOfWeek != DayOfWeek.Sunday) { listdate.Add(i); }
            }
            return listdate;
        }
        public void PermissionRole1(bool flag)
        {
            this.ddlProject.Enabled = flag;
            this.ddlWorkgroup.Enabled = flag;
            this.txtDocNumber.Enabled = flag;
            this.ddlDocType.Enabled = flag;
            // this.txtManHour.Enabled = flag;
            this.txtOutgoingTransNo.Enabled = flag;
            this.txtOutgoingTransDate.Enabled = flag;
            this.txtOutgoingLeterNo.Enabled = flag;
            this.txtOutgoingLeterDate.Enabled = flag;
            this.txtIncomingTransDate.Enabled = flag;
            this.txtIncomingTransNo.Enabled = flag;
            this.txtEndDate.Enabled = flag;
            this.txtISODate.Enabled = flag;
            this.txtStartDate.Enabled = flag;
            this.txtDeadline.Enabled = flag;
            this.txtIDCDeadline.Enabled = flag;
            this.txtIDCEndDate.Enabled = flag;
            this.txtIFRDeadline.Enabled = flag;
            this.txtIFREndDate.Enabled = flag;
            this.ddlChecker.Enabled = flag;
            this.txtDocumentTitle.Enabled = flag;
        }

        public void PermissionRole2(bool flag)
        {
            this.txtManHourplan.Enabled = flag;
            this.ddlEngineer.Enabled = flag;
            this.txtWeight.Enabled = flag;
            this.level.Enabled = flag;
        }
        public void PermissionRole3(bool flag)
        {
            this.ddlRevision.Enabled = flag;
        }

        protected void Rivvalidate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if ((this.ddlRevision.SelectedItem == null || this.ddlRevision.SelectedIndex == 0))
            {
                this.Rivvalidate.ErrorMessage = "Please select revision.";
                this.divDocNo1.Style["margin-bottom"] = "-5px;";
                args.IsValid = false;
            }
        }

        //protected void EngineeringValidate_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if ((this.ddlEngineer.CheckedItems.Count == 0) && this.Request.QueryString["docId"] == null)
        //    {
        //        this.EngineeringValidate.ErrorMessage = "Please choose an Employee's.";
        //        this.divDocNo2.Style["margin-bottom"] = "-5px;";
        //        args.IsValid = false;
        //    }
        //}

        protected void startdatevalidate_ServerValidate(object source, ServerValidateEventArgs args)
        {

            if (this.txtStartDate.SelectedDate == null)
            {
                this.startdatevalidate.ErrorMessage = "Please enter a StartDate.";
                this.divDocNo3.Style["margin-bottom"] = "-5px;";
                args.IsValid = false;
            }
            else
            {
                var newWorkpackageObj = this.workGroupService.GetById(this.ddlWorkgroup.SelectedItem != null ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue) : 0);
                if (newWorkpackageObj != null && newWorkpackageObj.Deadline != null && newWorkpackageObj.StartDate != null && this.txtStartDate.SelectedDate.GetValueOrDefault().Date < newWorkpackageObj.StartDate.GetValueOrDefault().Date)
                {
                    this.startdatevalidate.ErrorMessage = "StartDate must be between " + newWorkpackageObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + newWorkpackageObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy");
                    this.divDocNo3.Style["margin-bottom"] = "-5px;";
                    args.IsValid = false;
                }
            }
        }

        protected void DeadlineValidate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtDeadline.SelectedDate == null)
            {
                this.DeadlineValidate.ErrorMessage = "Please enter a deadline.";
                this.divDocNo4.Style["margin-bottom"] = "-5px;";
                args.IsValid = false;
            }
            else
            {
                var newWorkpackageObj = this.workGroupService.GetById(this.ddlWorkgroup.SelectedItem != null ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue) : 0);
                if (this.txtStartDate.SelectedDate != null && this.txtDeadline.SelectedDate.GetValueOrDefault() < this.txtStartDate.SelectedDate.GetValueOrDefault().Date)
                {
                    this.DeadlineValidate.ErrorMessage = "Deadline must be greater than StartDate.";
                    this.divDocNo4.Style["margin-bottom"] = "-5px;";
                    args.IsValid = false;
                }
                else if (newWorkpackageObj != null && newWorkpackageObj.Deadline != null && this.txtStartDate.SelectedDate != null && this.txtDeadline.SelectedDate.GetValueOrDefault().Date > newWorkpackageObj.Deadline.GetValueOrDefault().Date)
                {
                    this.DeadlineValidate.ErrorMessage = "Deadline must be between " + this.txtStartDate.SelectedDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + newWorkpackageObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy");
                    this.divDocNo4.Style["margin-bottom"] = "-5px;";
                    args.IsValid = false;
                }

            }
        }

        protected void manhoursplanvalidate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtManHourplan.Value == null || this.txtManHourplan.Text == "" || this.txtManHourplan.Value == 0)
            {
                this.manhoursplanvalidate.ErrorMessage = "Please enter man-hour plan.";
                this.Divmahour.Style["margin-bottom"] = "-5px;";
                args.IsValid = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var docObj = this.documentPackageService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (docObj != null && docObj.ManHourPlan != null && docObj.ManHourPlan > this.txtManHourplan.Value && docObj.RevisionId != Convert.ToInt32(this.ddlRevision.SelectedValue))
                    {
                        this.txtManHourplan.Value = docObj.ManHourPlan + this.txtManHourplan.Value;
                    }
                }
            }
        }

        protected void CheckerValidate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.ddlChecker.SelectedIndex == 0 && string.IsNullOrEmpty(this.Request.QueryString["docId"]))
            {
                this.CheckerValidate.ErrorMessage = "Please select Engineer(s).";
                this.divChecker.Style["margin-bottom"] = "-5px;";
                args.IsValid = false;
            }
        }

        protected void txtComplete_TextChanged(object sender, EventArgs e)
        {
            var manhourActual = (Convert.ToDouble(this.txtComplete.Text) * Convert.ToDouble(this.txtManHourplan.Text)) / 100;
            this.txtManHour.Text = manhourActual.ToString();
        }

        protected void ddlRevision_SelectedIndexChanged(object sender, EventArgs e)
        {
            var revObj = this.revisionService.GetById(Convert.ToInt32(ddlRevision.SelectedValue));
            if (revObj != null && revObj.StatusID != null)
            {
                this.ddlStatus.SelectedValue = revObj.StatusID.ToString();
            }
        }

        protected void IDCDeadlineValiddator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtIDCDeadline.SelectedDate == null)
            {
                this.IDCDeadlineValiddator.ErrorMessage = "Please enter IDC deadline.";
                this.divIDCDeadline.Style["margin-bottom"] = "-5px;";
                args.IsValid = false;
            }
            else
            {
                var newWorkpackageObj = this.workGroupService.GetById(this.ddlWorkgroup.SelectedItem != null ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue) : 0);
                if (this.txtStartDate.SelectedDate != null && this.txtIDCDeadline.SelectedDate.GetValueOrDefault() < this.txtStartDate.SelectedDate.GetValueOrDefault().Date)
                {
                    this.IDCDeadlineValiddator.ErrorMessage = "IDC Deadline must be greater than StartDate.";
                    this.divIDCDeadline.Style["margin-bottom"] = "-5px;";
                    args.IsValid = false;
                }
                else if (newWorkpackageObj != null && newWorkpackageObj.Deadline != null && this.txtStartDate.SelectedDate != null && this.txtIDCDeadline.SelectedDate.GetValueOrDefault().Date > newWorkpackageObj.Deadline.GetValueOrDefault().Date)
                {
                    this.IDCDeadlineValiddator.ErrorMessage = "IDC Deadline must be between " + this.txtStartDate.SelectedDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + newWorkpackageObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy");
                    this.divIDCDeadline.Style["margin-bottom"] = "-5px;";
                    args.IsValid = false;
                }
            }
        }

        #region UpdateData
        private void UpdateWeightDocument(ScopeProject projectObj, ref List<DocumentPackage> docList)
        {
            try
            {
                var totalPlanManhour = 0.0;
                totalPlanManhour = docList.Aggregate(totalPlanManhour, (current, t) => current + t.ManHourPlan.GetValueOrDefault());

                foreach (var doc in docList)
                {
                    doc.Weight = (doc.ManHourPlan.GetValueOrDefault() / totalPlanManhour) * 100;
                    this.documentPackageService.Update(doc);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkpackage(ScopeProject projectObj, List<DocumentPackage> docList, ref WorkGroup wpObj, ref List<WorkGroup> wpList, ref double complete)
        {
            try
            {
                if (docList.Count > 0)
                {
                    //update complete workpackage
                    complete = 0.0;
                    complete = docList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                    complete = Math.Round(complete, 5);
                    if (complete == 100 || !docList.Where(t => t.Complete < 100).Any())
                    {
                        wpObj.EndDate = DateTime.Now;
                        wpObj.Complete = 100;
                    }
                    else
                    {
                        wpObj.Complete = complete;
                    }
                }

                // Update total document weight for workpackage
                double totalWeight = 0.0;
                totalWeight = docList.Sum(t => t.Weight.GetValueOrDefault());
                totalWeight = Math.Round(totalWeight, 2);
                totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                wpObj.TotalDocWeight = totalWeight;

                // Update total man hours plan & actual for workpackage
                double totalManhourPlan = 0.0;
                totalManhourPlan = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                wpObj.TotalManHours = totalManhourPlan;

                double totalManHourActual = 0.0;
                totalManHourActual = docList.Sum(t => t.ManHours.GetValueOrDefault());
                wpObj.UsedManHours = totalManHourActual;

                // Update can delete for workpackage
                wpObj.CanDelete = !docList.Any();
                this.workGroupService.Update(wpObj);

                //update weight all WP of PJ
                if (projectObj.AutoCalculateWeightWorkGroup == true)
                {
                    var sumTotalManHours = 0.0;
                    sumTotalManHours = wpList.Sum(t => t.TotalManHours.GetValueOrDefault());
                    foreach (var items in wpList)
                    {
                        var calWeight = ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) > 0 ? ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) : 0;
                        items.Weight = calWeight >= 100 ? 100 : calWeight;
                        this.workGroupService.Update(items);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateMilestonActual(WorkGroup wpObj, User userObj, Milestone milestoneObj)
        {
            try
            {
                var milestoneList = this.milestoneService.GetAllByWorkpackage(wpObj.ID).OrderByDescending(t => t.MilestoneDate).ToList();
                if (milestoneList.Count > 0)
                {
                    var presentMilestoneObj = milestoneList.FirstOrDefault(t => t.MilestoneDate != null
                                                                         && t.MilestoneDate.Value.Month == DateTime.Now.Month
                                                                         && t.MilestoneDate.Value.Year == DateTime.Now.Year);
                    if (presentMilestoneObj != null)
                    {
                        milestoneObj = presentMilestoneObj;
                        var prevIndex = milestoneList.IndexOf(milestoneObj) + 1;
                        var prevRealTotal = 0.0;
                        var prevPlanTotal = 0.0;
                        if (prevIndex < (milestoneList.Count - 1))
                        {
                            var prevMilestoneObj = milestoneList[prevIndex];
                            if (prevMilestoneObj != null)
                            {
                                prevRealTotal = prevMilestoneObj.RealTotal.GetValueOrDefault();
                                prevPlanTotal = prevMilestoneObj.PlanTotal.GetValueOrDefault();
                            }
                            else
                            {
                                prevRealTotal = 0;
                                prevPlanTotal = 0;
                            }
                        }
                        else
                        {
                            prevRealTotal = 0;
                            prevPlanTotal = 0;
                        }

                        // calculator real
                        double real = Math.Round(wpObj.Complete.GetValueOrDefault() - prevRealTotal, 2);
                        milestoneObj.Real = real;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PlanTotal = (prevPlanTotal + milestoneObj.PlanPercent) > 100 ? 100 : (prevPlanTotal + milestoneObj.PlanPercent);
                        milestoneObj.UpdatedBy = userObj.Id;
                        milestoneObj.UpdatedDate = DateTime.Now;
                        this.milestoneService.Update(milestoneObj);
                    }
                    else
                    {
                        var lastedMilestoneObj = milestoneList[0];
                        milestoneObj = new Milestone();
                        milestoneObj.MilestoneDate = DateTime.Now;
                        milestoneObj.PlanPercent = 0;
                        milestoneObj.PlanTotal = lastedMilestoneObj.PlanTotal;
                        double preReal = Math.Round(wpObj.Complete.GetValueOrDefault() - lastedMilestoneObj.RealTotal.GetValueOrDefault(), 2);
                        milestoneObj.Real = preReal;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PerformingUser = lastedMilestoneObj.PerformingUser;
                        milestoneObj.Note = lastedMilestoneObj.Note;
                        milestoneObj.WorkpackageId = wpObj.ID;
                        milestoneObj.WorkpackageName = wpObj.Name;
                        if (DateTime.Now.Year > lastedMilestoneObj.MilestoneDate.Value.Year && (DateTime.Now.Year - lastedMilestoneObj.MilestoneDate.Value.Year) > 0)
                        {
                            milestoneObj.PlanOfYear = 0;
                        }
                        else
                        {
                            milestoneObj.PlanOfYear = lastedMilestoneObj.PlanOfYear;
                        }
                        milestoneObj.CreatedBy = userObj.Id;
                        milestoneObj.CreatedDate = DateTime.Now;
                        this.milestoneService.Insert(milestoneObj);
                    }
                }
                else
                {
                    //milestoneObj = new Milestone();
                    milestoneObj.MilestoneDate = DateTime.Now;
                    milestoneObj.PlanPercent = 0;
                    milestoneObj.PlanTotal = 0;
                    milestoneObj.RealTotal = wpObj.Complete;
                    milestoneObj.Real = wpObj.Complete;
                    milestoneObj.PerformingUser = string.Empty;
                    milestoneObj.Note = string.Empty;
                    milestoneObj.WorkpackageId = wpObj.ID;
                    milestoneObj.WorkpackageName = wpObj.Name;
                    milestoneObj.PlanOfYear = 0;
                    milestoneObj.CreatedBy = userObj.Id;
                    milestoneObj.CreatedDate = DateTime.Now;
                    this.milestoneService.Insert(milestoneObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateProgressActual(WorkGroup wpObj, ProcessActual processActualObj)
        {
            try
            {
                var projectId = wpObj.ProjectId.GetValueOrDefault();
                var projectObj = this.scopeProjectService.GetById(projectId);
                var tempProcessActualObj = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, wpObj.ID);
                if (tempProcessActualObj != null)
                {
                    processActualObj = tempProcessActualObj;
                    if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null)
                    {
                        if (string.IsNullOrEmpty(processActualObj.Actual))
                        {
                            processActualObj.Actual = "0";
                        }
                        if (string.IsNullOrEmpty(processActualObj.ActualMonth))
                        {
                            processActualObj.ActualMonth = "0";
                        }
                        var progressActualWeekList = processActualObj.Actual.Split('$').ToList();
                        var progressActualMonthList = processActualObj.ActualMonth.Split('$').ToList();
                        var countWeek = 0;
                        var countMonth = 0;
                        for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualWeekList.Count() > countWeek)
                            {
                                if (DateTime.Now > j.AddDays(7))
                                {
                                    countWeek += 1;
                                }
                            }
                        }
                        var currentMonth = 0;
                        for (var j = projectObj.StartDate.GetValueOrDefault();
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualMonthList.Count() > countMonth)
                            {
                                if (DateTime.Now.Month > j.AddDays(7).Month && DateTime.Now > j.AddDays(7) && currentMonth != j.AddDays(7).Month)
                                {
                                    currentMonth = j.Month;
                                    countMonth++;
                                }
                            }
                        }
                        if (progressActualWeekList.Count() >= countWeek && progressActualWeekList.Count() > 0 && countWeek > 0)
                        {
                            progressActualWeekList = progressActualWeekList.Take(countWeek).ToList();
                            progressActualWeekList[countWeek - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.Actual = string.Join("$", progressActualWeekList);
                            this.processActualService.Update(processActualObj);
                        }
                        if (progressActualMonthList.Count() >= countMonth && progressActualMonthList.Count() > 0 && countMonth > 0)
                        {
                            progressActualMonthList = progressActualMonthList.Take(countMonth).ToList();
                            progressActualMonthList[countMonth - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.ActualMonth = string.Join("$", progressActualMonthList);
                            this.processActualService.Update(processActualObj);
                        }
                    }
                }
                else
                {
                    processActualObj = new ProcessActual();
                    processActualObj.ProjectId = wpObj.ProjectId;
                    processActualObj.WorkgroupId = wpObj.ID;
                    processActualObj.Actual = wpObj.Complete.ToString();
                    processActualObj.ActualMonth = wpObj.Complete.ToString();
                    this.processActualService.Insert(processActualObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private DateTime GetMondayOfWeek(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        private DateTime GetFridayOfWeek(DateTime date)
        {
            return GetMondayOfWeek(date).AddDays(4);
        }

        private void UpdateDataProject(List<WorkGroup> wpList, ScopeProject projectObj)
        {
            try
            {
                if (projectObj != null)
                {
                    projectObj.CanDelete = false;
                    if (projectObj.IsAutoCalculate.GetValueOrDefault())
                    {
                        double complete = 0.0;
                        complete = wpList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                        projectObj.Complete = Math.Round(complete, 5);
                        double totalWeight = 0.0;
                        totalWeight = wpList.Sum(t => t.Weight.GetValueOrDefault());
                        totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                        projectObj.TotalWorkpackageWeight = Math.Round(totalWeight, 2);
                    }
                    this.scopeProjectService.Update(projectObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateWeightWorkgroupManhourMonth(WorkGroup wpObj, List<WorkgroupManhourMonth> wmmList)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                wmmList = this.workgroupManhourMonthService.GetByPJ(wpObj.ProjectId.GetValueOrDefault(), month, year);
                foreach (var wmmItem in wmmList)
                {
                    var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                    if (wpItem != null)
                    {
                        wmmItem.WorkgroupName = wpItem.Name;
                        wmmItem.WorkgroupWeight = wpItem.Weight;
                        this.workgroupManhourMonthService.Update(wmmItem);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkgroupManhourMonth(WorkGroup wpObj, User userObj, List<WorkGroup> wpList, WorkgroupManhourMonth wmmObj)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                var docList = this.documentPackageService.GetAllByWorkgroup(wpObj.ID);
                var manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                var presentWMMObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), month, year);
                var prevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpObj.ID, month, year);
                if (presentWMMObj != null)
                {
                    wmmObj = presentWMMObj;
                    wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                    wmmObj.ManhourPlanTotal = manhourPlanTotal;
                    wmmObj.ManhourPlanMonth = (wmmObj.ManhourPlanTotal.GetValueOrDefault() * wmmObj.CompletePlanMonth.GetValueOrDefault()) / 100;
                    wmmObj.CompleteActualTotal = wpObj.Complete.GetValueOrDefault();
                    wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                    //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                    if (prevWMMObj != null)
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                    }
                    else
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                    }

                    wmmObj.UpdateBy = userObj.Id;
                    wmmObj.UpdateDate = DateTime.Now;
                    this.workgroupManhourMonthService.Update(wmmObj);
                }
                else
                {
                    foreach (var wpItem in wpList)
                    {
                        docList = this.documentPackageService.GetAllByWorkgroup(wpItem.ID);
                        manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                        var tempObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpItem.ID, wpItem.ProjectId.GetValueOrDefault(), month, year);
                        var tempPrevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpItem.ID, month, year);
                        if (tempObj == null)
                        {
                            wmmObj = new WorkgroupManhourMonth();
                            wmmObj.Month = month;
                            wmmObj.Year = year;
                            wmmObj.WorkgroupID = wpItem.ID;
                            wmmObj.WorkgroupName = wpItem.Name;
                            wmmObj.DeparmentID = wpItem.DepartmentId;
                            wmmObj.ProjectID = wpItem.ProjectId.GetValueOrDefault();
                            wmmObj.WorkgroupWeight = wpItem.Weight.GetValueOrDefault();
                            wmmObj.ManhourPlanTotal = manhourPlanTotal;
                            wmmObj.CompleteActualTotal = wpItem.Complete.GetValueOrDefault();
                            wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                            //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                            if (tempPrevWMMObj != null)
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - tempPrevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - tempPrevWMMObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            else
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            wmmObj.CreateBy = userObj.Id;
                            wmmObj.CreateDate = DateTime.Now;
                            this.workgroupManhourMonthService.Insert(wmmObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourWeek(DocumentPackage documentObj, User userObj, UserManhour userManhourObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;

                CultureInfo myCI = CultureInfo.CurrentCulture;
                System.Globalization.Calendar myCal = myCI.Calendar;
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var listUserManhour = this.userManhourService.GetByDocIDAndUserID(documentObj.ID, user.Id);
                            listUserManhour = listUserManhour.OrderByDescending(t => t.ID).ToList();

                            var currentUserManhourWeek = listUserManhour.FirstOrDefault(t => myCal.GetWeekOfYear(DateTime.Now.Date, myCWR, myFirstDOW) == myCal.GetWeekOfYear(t.Date.GetValueOrDefault(), myCWR, myFirstDOW));
                            int index = listUserManhour.IndexOf(currentUserManhourWeek);
                            var prevUserManhourWeek = new UserManhour();
                            if (index != -1 && (index + 1) < listUserManhour.Count)
                            {
                                prevUserManhourWeek = listUserManhour[(index + 1)];
                            }
                            if (currentUserManhourWeek != null)
                            {
                                userManhourObj = currentUserManhourWeek;
                                if (prevUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - prevUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UpdateBy = userObj.Id;
                                userManhourObj.UpdateDate = DateTime.Now;
                                this.userManhourService.Update(userManhourObj);
                            }
                            else
                            {
                                var lastedUserManhourWeek = new UserManhour();
                                if (listUserManhour.Count > 0)
                                {
                                    lastedUserManhourWeek = listUserManhour[0];
                                }
                                userManhourObj = new UserManhour();
                                userManhourObj.DocumentID = documentObj.ID;
                                userManhourObj.Date = DateTime.Now.Date;
                                userManhourObj.DeparmentID = user.RoleId;
                                userManhourObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourObj.ProjectID = documentObj.ProjectId;
                                if (lastedUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - lastedUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - lastedUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UserID = user.Id;
                                userManhourObj.UserName = user.FullName;
                                userManhourObj.CreateBy = userObj.Id;
                                userManhourObj.CreateDate = DateTime.Now;
                                this.userManhourService.Insert(userManhourObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourMonth(DocumentPackage documentObj, User userObj, UserManhourMonth userManhourMonthObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;
                var date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var currentUserManhourMonth = this.userManhourMonthService.GetByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            var prevUserManhourMonth = this.userManhourMonthService.GetLastedByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            if (currentUserManhourMonth != null)
                            {
                                userManhourMonthObj = currentUserManhourMonth;
                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UpdateBy = userObj.Id;
                                userManhourMonthObj.UpdateDate = DateTime.Now;
                                this.userManhourMonthService.Update(userManhourMonthObj);
                            }
                            else
                            {
                                userManhourMonthObj = new UserManhourMonth();
                                userManhourMonthObj.DocumentID = documentObj.ID;
                                userManhourMonthObj.Month = DateTime.Now.Month;
                                userManhourMonthObj.Year = DateTime.Now.Year;
                                userManhourMonthObj.DeparmentID = user.RoleId;
                                userManhourMonthObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourMonthObj.ProjectID = documentObj.ProjectId;

                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UserID = user.Id;
                                userManhourMonthObj.UserName = user.FullName;
                                userManhourMonthObj.CreateBy = userObj.Id;
                                userManhourMonthObj.CreateDate = DateTime.Now;
                                this.userManhourMonthService.Insert(userManhourMonthObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        //protected void IFRDeadlineValidator_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (this.txtIFRDeadline.SelectedDate == null)
        //    {
        //        this.IFRDeadlineValidator.ErrorMessage = "Please enter IFR deadline.";
        //        this.divIFRDeadline.Style["margin-bottom"] = "-5px;";
        //        args.IsValid = false;
        //    }
        //    else
        //    {
        //        var newWorkpackageObj = this.workGroupService.GetById(this.ddlWorkgroup.SelectedItem != null ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue) : 0);
        //        if (this.txtStartDate.SelectedDate != null && this.txtIFRDeadline.SelectedDate.GetValueOrDefault() < this.txtStartDate.SelectedDate.GetValueOrDefault().Date)
        //        {
        //            this.IFRDeadlineValidator.ErrorMessage = "IFR Deadline must be greater than StartDate.";
        //            this.divIFRDeadline.Style["margin-bottom"] = "-5px;";
        //            args.IsValid = false;
        //        }
        //        else if (newWorkpackageObj != null && newWorkpackageObj.Deadline != null && this.txtStartDate.SelectedDate != null && this.txtIFRDeadline.SelectedDate.GetValueOrDefault().Date > newWorkpackageObj.Deadline.GetValueOrDefault().Date)
        //        {
        //            this.IFRDeadlineValidator.ErrorMessage = "IFR Deadline must be between " + this.txtStartDate.SelectedDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + newWorkpackageObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy");
        //            this.divIFRDeadline.Style["margin-bottom"] = "-5px;";
        //            args.IsValid = false;
        //        }
        //    }
        //}
    }
}
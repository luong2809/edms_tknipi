﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransmittalAttachDocument.aspx.cs" Inherits="EDMs.Web.Controls.Document.TransmittalAttachDocument" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

    <style type="text/css">
        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
            margin: 0 0px;
        }

        .RadComboBox .rcbInputCell .rcbInput {
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
        }

        .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
            padding-left: 0px !important;
        }

        div.rgEditForm label {
            float: right;
            text-align: right;
            width: 72px;
        }

        .rgEditForm {
            text-align: right;
        }

        .RadComboBox {
            border-bottom: none !important;
        }

        div.RadGrid .rgPager .rgAdvPart {
            display: none;
        }

        div.rgDataDiv {
            overflow: auto !important;
        }

        .DropZone1 {
            width: 300px;
            height: 250px;
            padding-left: 230px;
            background: #fff url(../../Images/placeholder-add.png) no-repeat center center;
            background-color: #357A2B;
            border-color: #CCCCCC;
            color: #767676;
            float: left;
            text-align: center;
            font-size: 16px;
            color: white;
            position: relative;
        }

        #btnSavePanel {
            display: inline !important;
        }

        .RadAjaxPanel {
            height: 100% !important;
        }

        #grdDocument_GridData {
            height: 85% !important;
        }

        .RadGrid .rgSelectedRow {
            background-image: none !important;
            background-color: coral !important;
        }

        #RAD_SPLITTER_PANE_CONTENT_Radpane6 {
            overflow: hidden !important;
        }

        #ddlProject_Input {
            width: 183px !important;
        }

        .accordion dt a {
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .accordion dt span {
            color: #085B8F;
            border-bottom: 1px solid #46A3D3;
            font-size: 1.0em;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
            margin-left: 5px;
        }
    </style>
    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />

        <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical" Width="100%" Height="100%">
            <telerik:RadPane ID="Radpane6" runat="server" Scrolling="Both" Width="46%">

                <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
                    <telerik:RadPane ID="RadPane3" runat="server" Height="150px" Scrollable="false" Scrolling="None">
                        <dl class="accordion">
                            <dt style="width: 100%;">
                                <span>SEARCH CONDITION</span>
                            </dt>
                        </dl>
                        <table style="width: 100%">
                            <tr class="qlcbFormItem">
                                <td width="150px" class="qlcbTooltip"><span id="Span2">Workgroups</span></td>
                                <td width="200px">
                                    <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                        <asp:DropDownList ID="ddlWorkgroup" runat="server" CssClass="min25Percent" Width="216" />
                                    </div>
                                </td>
                                <td width="150px" class="qlcbTooltip"><span id="Span3"></span></td>
                                <td width="200px">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td height="3px" colspan="4"></td>
                            </tr>
                            <tr class="qlcbFormItem">
                                <td class="qlcbTooltip"><span id="Span6">Doc No:</span></td>
                                <td>
                                    <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                        <asp:TextBox ID="txtDocNo" runat="server" CssClass="min25Percent" Width="200px" />
                                    </div>
                                </td>
                                <td class="qlcbTooltip"><span id="Span7">Doc Title:</span></td>
                                <td>
                                    <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                        <asp:TextBox ID="txtDocTitle" runat="server" CssClass="min25Percent" Width="200px"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" align="center">
                                    <telerik:RadButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" Width="80px" Style="text-align: center">
                                        <Icon PrimaryIconUrl="../../Images/search.gif" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                                    </telerik:RadButton>
                                    <%--<telerik:RadButton ID="btnSave" runat="server" Text="Save selected doc" OnClick="btnSave_Click" Width="110px" style="text-align: center" />--%>
                                </td>
                            </tr>
                        </table>

                    </telerik:RadPane>

                    <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
                        <dl class="accordion">
                            <dt style="width: 100%;">
                                <span>DOCUMENT LIST</span>
                            </dt>
                        </dl>

                        <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True" Height="90%"
                            AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                            GridLines="None" Skin="Windows7"
                            AllowFilteringByColumn="true"
                            OnNeedDataSource="grdDocument_OnNeedDataSource"
                            OnItemDataBound="grdDocument_OnItemDataBound"
                            PageSize="100" Style="outline: none" AllowMultiRowSelection="true">
                            <GroupingSettings CaseSensitive="false" />
                            <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%">
                                <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                <GroupByExpressions>
                                    <telerik:GridGroupByExpression>
                                        <SelectFields>
                                            <telerik:GridGroupByField FieldAlias="-" FieldName="DocumentTypeName" FormatString="{0:D}"
                                                HeaderValueSeparator=""></telerik:GridGroupByField>
                                        </SelectFields>
                                        <GroupByFields>
                                            <telerik:GridGroupByField FieldName="DocumentTypeName" SortOrder="Ascending"></telerik:GridGroupByField>
                                        </GroupByFields>
                                    </telerik:GridGroupByExpression>
                                </GroupByExpressions>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />

                                <Columns>
                                    <telerik:GridBoundColumn DataField="HasAttachFile" UniqueName="HasAttachFile" Display="False" />

                                    <telerik:GridTemplateColumn UniqueName="IsSelected" Display="False">
                                        <HeaderStyle Width="2%" />
                                        <ItemStyle HorizontalAlign="Center" Width="2%" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cboxSelectDocTransmittal" runat="server" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="No." Groupable="False" AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="2%" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="2%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSoTT" runat="server" Text='<%# grdDocument.CurrentPageIndex * grdDocument.PageSize + grdDocument.Items.Count+1 %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn HeaderText="DOC. No." DataField="DocNo" UniqueName="DocNo" ShowFilterIcon="False" FilterControlWidth="97%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="DOC. Title" UniqueName="DocTitle" AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="14%" />
                                        <ItemStyle HorizontalAlign="Left" Width="14%" />
                                        <ItemTemplate>
                                            <%# Eval("DocTitle") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Rev." UniqueName="Rev" AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemTemplate>
                                            <%# Eval("RevisionName") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Outgoing Trans" UniqueName="OutgoingTransNo" AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemTemplate>
                                            <%# Eval("OutgoingTransNo") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Trans Hard Copy" UniqueName="OutgoingLeterNo" AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemTemplate>
                                            <%# Eval("OutgoingLeterNo") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Step Definition" UniqueName="StepDefinitionID" Display="False" AllowFiltering="false">
                                        <HeaderStyle Width="150px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Duration" UniqueName="Duration" Display="False" AllowFiltering="false">
                                        <HeaderStyle Width="60px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>

                                </Columns>
                            </MasterTableView>
                            <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                                <Selecting AllowRowSelect="true" />
                                <%--<ClientEvents OnRowClick="RowClick"></ClientEvents>--%>
                                <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="200" UseStaticHeaders="True" />

                            </ClientSettings>

                        </telerik:RadGrid>
                    </telerik:RadPane>

                </telerik:RadSplitter>


            </telerik:RadPane>

            <telerik:RadPane ID="Radpane1" runat="server" Scrolling="None" Width="54%">
                <telerik:RadSplitter ID="RadSplitter1" runat="server" Orientation="Horizontal" Width="100%">
                    <telerik:RadPane ID="RadPane4" runat="server" Height="150px" Scrollable="false" Scrolling="None">
                        <dl class="accordion">
                            <dt style="width: 100%;">
                                <span>STEP DEFINITION</span>
                            </dt>
                        </dl>
                        <div style="display: flex">
                            <div style="width: 400px;" runat="server" id="divContent">
                                <ul style="list-style-type: none; margin: 0; padding-left: 10px">
                                    <li style="width: 100%;" runat="server" id="UploadControl">
                                        <div>
                                            <label style="width: 80px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                                <span style="color: #2E5689; text-align: right;">Step Definition
                                                </span>
                                            </label>
                                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                                <asp:DropDownList runat="server" ID="ddlStepDefinition" Width="200" onChange="SelectedIndexChanged(this)">
                                                    <Items>
                                                        <asp:ListItem Value=" " Text="" />
                                                        <asp:ListItem Value="INF" Text="INF-Information" />
                                                        <asp:ListItem Value="IFR" Text="IFR-Review" />
                                                        <asp:ListItem Value="Response" Text="Response" />
                                                        <asp:ListItem Value="IFA" Text="IFA-Aproved" />
                                                        <asp:ListItem Value="IFC" Text="IFC-Construction" />
                                                    </Items>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div style="clear: both; font-size: 0;"></div>
                                    </li>
                                    <li style="width: 100%;" runat="server" id="Li1">
                                        <div>
                                            <label style="width: 80px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                                <span style="color: #2E5689; text-align: right;">Duration
                                                </span>
                                            </label>
                                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                                <telerik:RadNumericTextBox ID="txtDuration" Width="200" runat="server">
                                                    <NumberFormat DecimalDigits="0"></NumberFormat>
                                                </telerik:RadNumericTextBox>
                                            </div>
                                        </div>
                                        <div style="clear: both; font-size: 0;"></div>
                                    </li>
                                </ul>
                            </div>
                            <div>
                                <ul style="list-style-type: none; margin: 0; padding-left: 10px">
                                    <li style="width: 100%;" runat="server" id="Li2">
                                        <div>
                                            <label style="width: 50px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                                <span style="color: #2E5689; text-align: right;">Type
                                                </span>
                                            </label>
                                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                                <asp:DropDownList runat="server" ID="ddlType" Width="200" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                                    <Items>
                                                        <asp:ListItem Value="1" Text="Soft Copy" />
                                                        <asp:ListItem Value="2" Text="Hard Copy" />
                                                        <asp:ListItem Value="3" Text="All" />
                                                    </Items>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div style="clear: both; font-size: 0;"></div>
                                    </li>
                                    <asp:Panel ID="pnPage" runat="server">
                                        <li style="width: 100%;" runat="server" id="Li3">
                                            <div>
                                                <label style="width: 50px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                                    <span style="color: #2E5689; text-align: right;">Original
                                                    </span>
                                                </label>
                                                <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                                    <telerik:RadNumericTextBox ID="txtOriginalNumber" Width="70px" runat="server">
                                                        <NumberFormat DecimalDigits="0"></NumberFormat>
                                                    </telerik:RadNumericTextBox>
                                                </div>
                                                <label style="width: 50px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                                    <span style="color: #2E5689; text-align: right;">Copy
                                                    </span>
                                                </label>
                                                <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                                    <telerik:RadNumericTextBox ID="txtCopyNumber" Width="70px" runat="server">
                                                        <NumberFormat DecimalDigits="0"></NumberFormat>
                                                    </telerik:RadNumericTextBox>
                                                </div>
                                            </div>
                                            <div style="clear: both; font-size: 0;"></div>
                                        </li>
                                    </asp:Panel>
                                </ul>
                            </div>
                        </div>
                        <div style="margin-left:5px">
                            <asp:CheckBox ID="cbSendMail" runat="server" Text="Auto Send Mail XDCB" />
                        </div>
                        <div>
                            <ul style="list-style-type: none">
                                <li style="width: 100%; text-align: center">
                                    <telerik:RadButton ID="RadButton1" runat="server" Text="Save" OnClick="btnSave_Click" Width="70px" Style="text-align: center">
                                        <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="RadButton2" runat="server" Text="Finish" OnClick="btFinish_Click" Width="70px" Style="text-align: center">
                                        <Icon PrimaryIconUrl="../../Images/ok.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                                    </telerik:RadButton>
                                </li>
                            </ul>
                        </div>
                    </telerik:RadPane>
                    <telerik:RadPane ID="RadPane5" runat="server" Scrollable="false" Scrolling="None">
                        <dl class="accordion">
                            <dt style="width: 100%;">
                                <span>ATTACHED DOCUMENT LIST</span>
                            </dt>
                        </dl>

                        <telerik:RadGrid ID="rgdTransDocumenlist" runat="server" AllowPaging="True" Height="90%"
                            AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                            GridLines="None"
                            Skin="Windows7"
                            OnNeedDataSource="rgdTransDocumenlist_NeedDataSource"
                            OnItemCommand="rgdTransDocumenlist_ItemCommand"
                            OnDeleteCommand="rgdTransDocumenlist_DeleteCommand"
                            PageSize="200" Style="outline: none">
                            <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%">
                                <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                    <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="DeleteColumn">
                                        <HeaderStyle Width="15" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <a href='javascript:DeleteItem(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                <asp:Image ID="delete" runat="server" ImageUrl="~/Images/delete.png" Style="cursor: pointer;" AlternateText="Delete Row" />
                                                <a />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridButtonColumn UniqueName="EditColumn" CommandName="EditCmd" ButtonType="ImageButton" ImageUrl="~/Images/edit.png">
                                        <HeaderStyle Width="15" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridButtonColumn>
                                    <%--<telerik:GridTemplateColumn HeaderText="No." Groupable="False">
                                        <HeaderStyle HorizontalAlign="Center" Width="2%" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="2%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSoTT" runat="server" Text='<%# rgdTransDocumenlist.CurrentPageIndex * rgdTransDocumenlist.PageSize + rgdTransDocumenlist.Items.Count+1 %>'>
                                            </asp:Label>

                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                    <telerik:GridTemplateColumn HeaderText="DOC. No." UniqueName="DocNo">
                                        <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                        <ItemStyle HorizontalAlign="Left" Width="8%" />
                                        <ItemTemplate>
                                            <%# Eval("DocNo") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="DOC. Title" UniqueName="DocTitle">
                                        <HeaderStyle HorizontalAlign="Center" Width="14%" />
                                        <ItemStyle HorizontalAlign="Left" Width="14%" />
                                        <ItemTemplate>
                                            <%# Eval("DocTitle") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Start" UniqueName="StartDate">
                                        <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                        <ItemStyle HorizontalAlign="Center" Width="5%" />
                                        <ItemTemplate>
                                            <%# Eval("StartDate","{0:dd/MM/yyyy}") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Deadline" UniqueName="PlanedDate">
                                        <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                        <ItemStyle HorizontalAlign="Center" Width="5%" />
                                        <ItemTemplate>
                                            <%# Eval("Deadline","{0:dd/MM/yyyy}") %>
                                        </ItemTemplate>

                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Rev." UniqueName="Rev">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemTemplate>
                                            <%# Eval("RevisionName") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Step Status." UniqueName="StepStatus">
                                        <HeaderStyle HorizontalAlign="Center" Width="4%" />
                                        <ItemStyle HorizontalAlign="Center" Width="4%" />
                                        <ItemTemplate>
                                            <%# Eval("StepStatus") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Duration." UniqueName="Duration">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemTemplate>
                                            <%# Eval("Duration") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Original" UniqueName="OriginalNumber">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemTemplate>
                                            <%# Eval("OriginalNumber") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Copy No." UniqueName="CopyNumber">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemTemplate>
                                            <%# Eval("CopyNumber") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>

                            <ClientSettings AllowColumnHide="True">
                                <Selecting AllowRowSelect="true" />
                                <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="200" UseStaticHeaders="True" />

                            </ClientSettings>
                        </telerik:RadGrid>
                    </telerik:RadPane>
                </telerik:RadSplitter>



            </telerik:RadPane>
        </telerik:RadSplitter>
        <div>
        </div>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <span style="display: none">

            <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="rgdTransDocumenlist" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                    <telerik:AjaxSetting AjaxControlID="btnSearch">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>

                    <telerik:AjaxSetting AjaxControlID="ddlType">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="pnPage" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>

                    <telerik:AjaxSetting AjaxControlID="ddlProject">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="ddlWorkgroup" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                    <telerik:AjaxSetting AjaxControlID="ddlStepDefinition">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="txtDuration" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                    <telerik:AjaxSetting AjaxControlID="rgdTransDocumenlist">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="ddlStepDefinition" />
                            <telerik:AjaxUpdatedControl ControlID="txtDuration" />
                            <telerik:AjaxUpdatedControl ControlID="ddlType" />
                            <telerik:AjaxUpdatedControl ControlID="pnPage" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                    <telerik:AjaxSetting AjaxControlID="btLuuss">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="ddlStepDefinition" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="ddlStepDefinition" />
                            <telerik:AjaxUpdatedControl ControlID="txtDuration" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
        </span>

        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
            <Windows>
                <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Document Information"
                    VisibleStatusbar="false" Height="690" Width="650"
                    Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
                </telerik:RadWindow>

                <telerik:RadWindow ID="RevisionDialog" runat="server" Title="Revision history" OnClientClose="refreshGrid"
                    VisibleStatusbar="false" Height="332" Width="1200" MinHeight="332" MinWidth="1200" MaxHeight="332" MaxWidth="1200"
                    Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
                </telerik:RadWindow>

                <telerik:RadWindow ID="VersionHistory" runat="server" Title="Version history"
                    VisibleStatusbar="false" Height="332" Width="1200" MinHeight="332" MinWidth="1200" MaxHeight="332" MaxWidth="1200"
                    Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>

        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
        <asp:HiddenField runat="server" ID="FolderContextMenuAction" />
        <asp:HiddenField runat="server" ID="lblFolderId" />
        <asp:HiddenField runat="server" ID="lblDocId" />
        <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script src="../../Scripts/jquery-1.7.1.js"></script>
            <script type="text/javascript">

                var radDocuments;
                var Index;
                function refreshGrid() {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                    masterTable.rebind();

                    var masterTables = $find("<%= rgdTransDocumenlist.ClientID%>").get_masterTableView();
                    masterTables.rebind();
                }

                function GetGridObject(sender, eventArgs) {
                    radDocuments = sender;
                }
                function RowClick(sender, eventArgs) {
                    var Id = eventArgs.getDataKeyValue("ID");
                    document.getElementById("<%= lblDocId.ClientID %>").value = Id;
                var row = eventArgs.get_itemIndexHierarchical();
                // alert(row);
                Index = row;
            }
            function DeleteItem(id) {
                if (confirm("Do you want to delete document?") == true) {
                    ajaxManager.ajaxRequest("delete_" + id);
                }
            }

            //window.onbeforeunload = function (e) {
            //    var e = e || window.event;
            //    if (e) {
            //        ajaxManager.ajaxRequest("Closing");
            //    }
            //}
            //function ShowEditForm(id) {
            //    ajaxManager.ajaxRequest("EditStep_" + id);
            //}


                <%-- function gridMenuClicking(sender, args) {
                var itemValue = args.get_item().get_value();
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;



                switch (itemValue) {
                    case "RevisionHistory":
                        var owd = $find("<%=RevisionDialog.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/RevisionHistory.aspx?docId=" + docId, "RevisionDialog");
                        break;
                    case "VersionHistory":
                        var owd = $find("<%=VersionHistory.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/VersionHistory.aspx?docId=" + docId, "VersionHistory");
                        break;
                }
            }--%>
            </script>
            <script type="text/javascript">
                /* <![CDATA[ */
                var toolbar;
                var searchButton;
                var ajaxManager;

                function pageLoad() {
                    $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                        //alert($('iframe').contents());
                    });

                    ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
                }
                function SelectedIndexChanged(ddl) {
                    var DropdownList = document.getElementById('<%=ddlStepDefinition.ClientID %>').selectedIndex;

                    var LabelDropdownList = document.getElementById('<%= txtDuration.ClientID %>');
                    if (DropdownList == 2) {
                        LabelDropdownList.value = 7;
                    } else if (DropdownList == 3) {
                        LabelDropdownList.value = 1;
                    } else if (DropdownList == 4) {
                        LabelDropdownList.value = 0;
                    } else if (DropdownList == 1) {
                        LabelDropdownList.value = 0;
                    } else if (DropdownList == 5) {
                        LabelDropdownList.value = 0;
                    }
                    else if (DropdownList == 0) {
                        LabelDropdownList.value = null;
                    }
                    /// DropdownList.value = 0;
                }
                <%--function ShowEditForm(id, rowIndex, folId) {
                var grid = $find("<%= grdDocument.ClientID %>");
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;

                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                grid.get_masterTableView().selectItem(rowControl, true);
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?docId=" + id + "&folId=" + folId, "CustomerDialog");

                // window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + id, "CustomerDialog");
                //  return false;
            }--%>
           <%-- function ShowInsertForm() {

                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/CustomerEditForm.aspx", "CustomerDialog");

                //window.radopen("Controls/Customers/CustomerEditForm.aspx", "CustomerDialog");
                //return false;
            }--%>



                function refreshGrid(arg) {
                    //alert(arg);
                    if (!arg) {
                        ajaxManager.ajaxRequest("Rebind");
                    }
                    else {
                        ajaxManager.ajaxRequest("RebindAndNavigate");
                    }
                }

                function refreshTab(arg) {
                    $('.EDMsRadPageView' + arg + ' iframe').attr('src', $('.EDMsRadPageView' + arg + ' iframe').attr('src'));
                }

                <%--function RowDblClick(sender, eventArgs) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/ViewCustomerDetails.aspx?docId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
                // window.radopen("Controls/Customers/ViewCustomerDetails.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
            }--%>

                <%--function onNodeClicking(sender, args) {
                var folderValue = args.get_node().get_value();
                document.getElementById("<%= lblFolderId.ClientID %>").value = folderValue;
            }--%>

                <%-- function OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var strValue = button.get_value();

                var grid = $find("<%= grdDocument.ClientID %>");
                var customerId = null;
                var customerName = "";

                //if (grid.get_masterTableView().get_selectedItems().length > 0) {
                //    var selectedRow = grid.get_masterTableView().get_selectedItems()[0];
                //    customerId = selectedRow.getDataKeyValue("Id");
                //    //customerName = selectedRow.Items["FullName"]; 
                //    //customerName = grid.get_masterTableView().getCellByColumnUniqueName(selectedRow, "FullName").innerHTML;
                //}




                if (strText.toLowerCase() == "documents") {

                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    if (selectedFolder == "") {
                        alert("Please choice one folder to create new document");
                        return false;
                    }

                    var owd = $find("<%=CustomerDialog.ClientID %>");
                    owd.Show();
                    owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?folId=" + selectedFolder, "CustomerDialog");

                }

                if (strText == "Thêm mới") {
                    return ShowInsertForm();
                }
                else if (strText == "Import dữ liệu") {
                    return ShowImportForm();
                }
                else if (strText == "Dữ liệu thô") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_1_" + customerId);
                }
                else if (strText == "Tiềm năng") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_2_" + customerId);
                }
                else if (strText == "Chưa liên hệ được") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_3_" + customerId);
                }
                else if (strText == "Không tiềm năng") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_4_" + customerId);
                }
                else if (strText == "Thông tin sai") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_5_" + customerId);
                }
                else if (strText == "Liên hệ tư vấn") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_6_" + customerId);
                }
                else if (strText == "Hẹn tư vấn") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_7_" + customerId);
                }
                else if (strText == "Đã sử dụng dịch vụ") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_8_" + customerId);
                }
                else {
                    var commandName = args.get_item().get_commandName();
                    if (commandName == "doSearch") {
                        var searchTextBox = sender.findButtonByCommandName("searchText").findControl("txtSearch");
                        if (searchButton.get_value() == "clear") {
                            searchTextBox.set_value("");
                            searchButton.set_imageUrl("images/search.gif");
                            searchButton.set_value("search");
                        }

                        performSearch(searchTextBox);
                    } else if (commandName == "reply") {
                        window.radopen(null, "Edit");
                    }
                }
            }--%>

                //function performSearch(searchTextBox) {
                //    if (searchTextBox.get_value()) {
                //        searchButton.set_imageUrl("images/clear.gif");
                //        searchButton.set_value("clear");
                //    }

                //    ajaxManager.ajaxRequest(searchTextBox.get_value());
                //}
                //function onTabSelecting(sender, args) {
                //    if (args.get_tab().get_pageViewID()) {
                //        args.get_tab().set_postBack(false);
                //    }
                //}


                <%-- function RowContextMenu(sender, eventArgs) {
                var menu = $find("<%=radMenu.ClientID %>");
                var evt = eventArgs.get_domEvent();

                if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                    return;
                }

                var index = eventArgs.get_itemIndexHierarchical();
                document.getElementById("radGridClickedRowIndex").value = index;

                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;

                sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);

                menu.show(evt);

                evt.cancelBubble = true;
                evt.returnValue = false;

                if (evt.stopPropagation) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }--%>
                /* ]]> */
            </script>
        </telerik:RadCodeBlock>
    </form>
</body>
</html>

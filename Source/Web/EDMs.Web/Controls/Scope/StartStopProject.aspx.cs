﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Scope;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using EDMs.Business.Services.Library;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class StartStopProject : Page
    {
        private readonly StopProjectService stopProjectService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly ProjectAppendixService projectAppendixService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public StartStopProject()
        {
            this.stopProjectService = new StopProjectService();
            this.scopeProjectService = new ScopeProjectService();
            this.projectAppendixService = new ProjectAppendixService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["projectId"]))
                {
                    var objScopeProject = this.scopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["projectId"]));
                    if (objScopeProject != null)
                    {
                        txtDate.SelectedDate = DateTime.Now;
                        if (objScopeProject.IsStop.GetValueOrDefault())
                        {
                            btnSave.Text = "Start";
                            lblReason.Text = "Basis of Project";
                            lblDate.Text = "Start Date";
                            txtDeadline.SelectedDate = objScopeProject.Deadline.GetValueOrDefault();
                        }
                        else
                        {
                            btnSave.Text = "Stop";
                            pnDeadline.Visible = false;
                            lblReason.Text = "Reason";
                            lblDate.Text = "Stop Date";
                        }
                    }
                }
            }
        }

        protected void grdProject_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["id"]))
            {
                var id = Convert.ToInt32(this.Request.QueryString["id"]);
                var projectList = stopProjectService.GetAllByProject(id);
                grdProject.DataSource = projectList;
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["projectId"]))
            {
                int projectID = Convert.ToInt32(this.Request.QueryString["projectId"]);
                var objScopeProject = this.scopeProjectService.GetById(projectID);
                if (objScopeProject != null)
                {
                    const string targetFolder = "../../DocumentLibrary/ProjectFiles";
                    var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/ProjectFiles";
                    var listUpload = docuploader.UploadedFiles;
                    if (!objScopeProject.IsStop.GetValueOrDefault())
                    {
                        var stopProject = new StopProject()
                        {
                            ProjectId = projectID,
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedDate = DateTime.Now,
                            TypeId = 2,
                            TypeName = "Stop",
                            Reason = this.txtReason.Text,
                            Date = this.txtDate.SelectedDate,
                        };
                        if (listUpload.Count > 0)
                        {
                            foreach (UploadedFile docFile in listUpload)
                            {
                                var docFileName = docFile.FileName;
                                var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;
                                // Path file to save on server disc
                                var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                                // Path file to download from server
                                var serverFilePath = serverFolder + "/" + serverDocFileName;
                                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                                docFile.SaveAs(saveFilePath, true);
                                stopProject.FileName = docFileName;
                                stopProject.Extension = fileExt;
                                stopProject.FilePath = serverFilePath;
                                stopProject.ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png";
                            }
                        }

                        objScopeProject.IsStop = true;
                        objScopeProject.Notes += DateTime.Now.ToString("dd.MM.yyyy") + ". Stopped. Reason: " + this.txtReason.Text + " ";

                        this.scopeProjectService.Update(objScopeProject);
                        this.stopProjectService.Insert(stopProject);
                    }
                    else
                    {
                        if (txtDeadline.SelectedDate > DateTime.Now)
                        {
                            var stopProject = new StopProject()
                            {
                                ProjectId = projectID,
                                CreatedBy = UserSession.Current.User.Id,
                                CreatedDate = DateTime.Now,
                                TypeId = 1,
                                TypeName = "Start",
                                Reason = this.txtReason.Text,
                                Date = this.txtDate.SelectedDate,
                            };
                            if (listUpload.Count > 0)
                            {
                                foreach (UploadedFile docFile in listUpload)
                                {
                                    var docFileName = docFile.FileName;
                                    var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;
                                    // Path file to save on server disc
                                    var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                                    // Path file to download from server
                                    var serverFilePath = serverFolder + "/" + serverDocFileName;
                                    var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                                    docFile.SaveAs(saveFilePath, true);
                                    stopProject.FileName = docFileName;
                                    stopProject.Extension = fileExt;
                                    stopProject.FilePath = serverFilePath;
                                    stopProject.ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png";
                                }
                            }
                            var projectAppendixObj = projectAppendixService.GetByProject(objScopeProject.ID);
                            if (projectAppendixObj != null)
                            {
                                projectAppendixObj.Basis = this.txtReason.Text.Trim();
                                projectAppendixObj.LastUpdatedBy = UserSession.Current.User.Id;
                                projectAppendixObj.LastUpdatedDate = DateTime.Now;
                                projectAppendixService.Update(projectAppendixObj);
                            }
                            else
                            {
                                ProjectAppendix projectAppendix = new ProjectAppendix();
                                projectAppendix.Basis = this.txtReason.Text.Trim();
                                projectAppendix.CreatedBy = UserSession.Current.User.Id;
                                projectAppendix.CreatedDate = DateTime.Now;
                                projectAppendixService.Insert(projectAppendix);
                            }
                            objScopeProject.Deadline = txtDeadline.SelectedDate;
                            objScopeProject.IsStop = false;
                            objScopeProject.Notes += DateTime.Now.ToString("dd.MM.yyyy") + ". Started. Reason: " + this.txtReason.Text + " ";
                            this.scopeProjectService.Update(objScopeProject);
                            this.stopProjectService.Insert(stopProject);
                        }
                        else
                        {
                            this.blockError.Visible = true;
                            this.lblError.Text = "Deadline must be greater than current date";
                        }
                    }
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
                }
            }
            //this.grdDocument.Rebind();
        }

        //protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        //{
        //    var item = (GridDataItem)e.Item;
        //    var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
        //    var attachFileObj = this.attachFilesProjectService.GetById(docId);

        //    if (attachFileObj != null && attachFileObj.IsDefault.GetValueOrDefault())
        //    {
        //        var attachFiles = this.attachFilesProjectService.GetAllByProject(attachFileObj.ProjectId.GetValueOrDefault()).Where(t => !t.IsDefault.GetValueOrDefault()).ToList();

        //        var projectObj = this.scopeProjectService.GetById(attachFileObj.ProjectId.GetValueOrDefault());

        //        if (attachFiles.Count > 0)
        //        {
        //            attachFiles[0].IsDefault = true;
        //            this.attachFilesProjectService.Update(attachFiles[0]);


        //            if (projectObj != null)
        //            {
        //                projectObj.DefaultFilePath = attachFiles[0].FilePath;
        //                projectObj.FileExtentionIcon = attachFiles[0].ExtensionIcon;
        //                this.scopeProjectService.Update(projectObj);
        //            }

        //        }
        //        else
        //        {
        //            if (projectObj != null)
        //            {
        //                projectObj.DefaultFilePath = string.Empty;
        //                projectObj.FileExtentionIcon = string.Empty;
        //                this.scopeProjectService.Update(projectObj);
        //            }
        //        }
        //    }

        //    this.attachFilesProjectService.Delete(docId);
        //    this.grdDocument.Rebind();
        //}

        //protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
        //    {
        //        var projId = Convert.ToInt32(Request.QueryString["docId"]);
        //        this.grdDocument.DataSource = this.attachFilesProjectService.GetAllByProject(projId).OrderByDescending(t => t.CreatedDate).ThenByDescending(t => t.FileName);
        //    }
        //    else
        //    {
        //        this.grdDocument.DataSource = new List<AttachFilesProject>();
        //    }
        //}

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
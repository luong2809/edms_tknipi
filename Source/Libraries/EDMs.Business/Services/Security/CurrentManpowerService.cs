﻿

namespace EDMs.Business.Services.Library
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
    using EDMs.Data.DAO.Security;
 public   class CurrentManpowerService
    {
     private readonly CurrentManpowerDAO repo;

        public CurrentManpowerService()
        {
            this.repo = new CurrentManpowerDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All CurrentManpower
        /// </summary>
        /// <param name="isHost">
        /// The is Host.
        /// </param>
        /// <returns>
        /// </returns>
        public List<CurrentManpower> GetAll()
        {
            return  this.repo.GetAll() ;
        }

        /// <summary>
        /// Get All CurrentManpower
        /// </summary>
        /// <returns></returns>
        public CurrentManpower GetDepartment(int departmentid, int year)
        {
            return this.repo.GetDepartment(departmentid,year);
        }
        public List<CurrentManpower> GetDepartment(List<int> departmentid, int year)
        {
            return this.repo.GetAll().Where(ob => departmentid.Contains(ob.DepartmentId.GetValueOrDefault()) && ob.Year == year).ToList();
        }
        /// <summary>
        /// Get CurrentManpower By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public CurrentManpower GetByID(int ID)
        {
            return this.repo.GetByID( ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

      

        #region Insert, Update, Delete
        /// <summary>
        /// Insert CurrentManpower
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(CurrentManpower bo)
        {
            try
            {
                return this.repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update CurrentManpower
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(CurrentManpower bo)
        {
            try
            {
                return this.repo.Update(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete CurrentManpower
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(CurrentManpower bo)
        {
            try
            {
                return this.repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete CurrentManpower By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return this.repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

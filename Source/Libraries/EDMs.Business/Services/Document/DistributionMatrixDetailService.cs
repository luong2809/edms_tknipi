﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistributionMatrixDetailService.cs" company="">
//   
// </copyright>
// <summary>
//   The category service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class DistributionMatrixDetailService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DistributionMatrixDetailDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributionMatrixDetailService"/> class.
        /// </summary>
        public DistributionMatrixDetailService()
        {
            this.repo = new DistributionMatrixDetailDAO();
        }

        #region Get (Advances)
        public List<DistributionMatrixDetail> GetByWorkpackage(int wpID)
        {
            return this.repo.GetByWorkpackage(wpID);
        }

        public List<DistributionMatrixDetail> GetByProject(int pjID)
        {
            return this.repo.GetByProject(pjID);
        }

        public List<DistributionMatrixDetail> GetByDocument(int docID)
        {
            return this.repo.GetByDocument(docID);
        }

        #endregion

        #region GET (Basic)

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public DistributionMatrixDetail GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(DistributionMatrixDetail bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(DistributionMatrixDetail bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(DistributionMatrixDetail bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteByWorkpackage(int wpID)
        {
            try
            {
                var list = this.repo.GetByWorkpackage(wpID);
                foreach (var item in list)
                {
                    this.repo.Delete(item);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

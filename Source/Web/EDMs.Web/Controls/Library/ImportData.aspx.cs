﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using EDMs.Business.Services.Scope;

namespace EDMs.Web.Controls.Library
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.Hosting;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;


    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportData : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>

        private readonly AppendixService appendixListService = new AppendixService();
        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();
        private readonly ProjectAppendixService projectAppendixService = new ProjectAppendixService();
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportData()
        {

        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {

            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            //try
            //
            string query = Request.QueryString["case"];
            if (query == "appendix")
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var currentFileName = docFile.FileName;
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                         "_" + docFile.FileName;
                        docFile.SaveAs(importPath);
                        // Instantiate a new workbook
                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var wsData = workbook.Worksheets[0];
                        // Create a datatable
                        var totalDataTable = new DataTable();

                        // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class	
                        var workID = wsData.Cells["A1"].Value.ToString();
                        totalDataTable = wsData.Cells.ExportDataTableAsString(1, 0, wsData.Cells.MaxRow, 3);
                        var parentlvl1 = 0;
                        var parentlvl2 = 0;
                        var parentlvl3 = 0;
                        var flag = 0;
                        foreach (DataRow item in totalDataTable.Rows)
                        {
                            if (!string.IsNullOrEmpty(item["Column2"].ToString()))
                            {
                                Appendix appen = new Appendix();
                                var a = item["Column3"].ToString();
                                if (item["Column1"].ToString() == "1")
                                {
                                    appen.WorkID = Convert.ToInt32(workID);
                                    appen.Code = item["Column2"].ToString().Trim();
                                    processName(item, appen);
                                    appen.Year = 2019;
                                    appen.CreatedBy = UserSession.Current.User.Id;
                                    appen.CreatedDate = DateTime.Now;
                                    parentlvl1 = this.appendixListService.Insert(appen).GetValueOrDefault();
                                    flag = 1;
                                }
                                else if (item["Column1"].ToString() == "2")
                                {
                                    appen.WorkID = Convert.ToInt32(workID);
                                    appen.Code = item["Column2"].ToString().Trim();
                                    processName(item, appen);
                                    appen.ParentID = parentlvl1;
                                    appen.Year = 2019;
                                    appen.CreatedBy = UserSession.Current.User.Id;
                                    appen.CreatedDate = DateTime.Now;
                                    parentlvl2 = this.appendixListService.Insert(appen).GetValueOrDefault();
                                    flag = 2;
                                }
                                else if (item["Column1"].ToString() == "3")
                                {
                                    appen.WorkID = Convert.ToInt32(workID);
                                    appen.Code = item["Column2"].ToString().Trim();
                                    processName(item, appen);
                                    appen.ParentID = parentlvl2;
                                    appen.Year = 2019;
                                    appen.CreatedBy = UserSession.Current.User.Id;
                                    appen.CreatedDate = DateTime.Now;
                                    parentlvl3 = this.appendixListService.Insert(appen).GetValueOrDefault();
                                    flag = 3;
                                }
                                else
                                {
                                    appen.WorkID = Convert.ToInt32(workID);
                                    appen.Code = item["Column2"].ToString().Trim();
                                    processName(item, appen);
                                    if (flag == 1)
                                    {
                                        appen.ParentID = parentlvl1;
                                    }
                                    else if (flag == 2)
                                    {
                                        appen.ParentID = parentlvl2;
                                    }
                                    else if (flag == 3)
                                    {
                                        appen.ParentID = parentlvl3;
                                    }
                                    appen.Year = 2019;
                                    appen.CreatedBy = UserSession.Current.User.Id;
                                    appen.CreatedDate = DateTime.Now;
                                    this.appendixListService.Insert(appen);
                                }
                            }
                        }
                    }
                }
            }
            else if (query == "projectappendix")
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var currentFileName = docFile.FileName;
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                         "_" + docFile.FileName;
                        docFile.SaveAs(importPath);
                        // Instantiate a new workbook
                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var wsData = workbook.Worksheets[0];
                        // Create a datatable
                        var totalDataTable = new DataTable();

                        // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class	
                        totalDataTable = wsData.Cells.ExportDataTable(1, 1, wsData.Cells.MaxRow, 2);
                        List<int> list = new List<int>() { 1 };
                        foreach (DataRow item in totalDataTable.Rows)
                        {
                            if (!string.IsNullOrEmpty(item["Column1"].ToString()) && !string.IsNullOrEmpty(item["Column2"].ToString()))
                            {
                                var appendixObj = appendixListService.GetByCodeType(item["Column1"].ToString().Trim(), list);
                                var projectObj = scopeProjectService.GetByName(item["Column2"].ToString().Trim());
                                if (appendixObj != null && projectObj != null)
                                {
                                    ProjectAppendix pa = new ProjectAppendix();
                                    pa.AppendixID = appendixObj.ID;
                                    pa.WorkID = appendixObj.WorkID;
                                    pa.ProjectID = projectObj.ID;
                                    pa.Type = 1;
                                    pa.Year = 2019;
                                    pa.CreatedBy = 1;
                                    pa.CreatedDate = DateTime.Now;
                                    projectAppendixService.Insert(pa);
                                }
                            }
                        }
                    }
                }
            }


            //}
            //catch (Exception ex)
            //{
            //    this.blockError.Visible = true;
            //    this.lblError.Text = "Have error when import Progress: <br/>'" + ex.Message + "'";
            //}

            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        private static void processName(DataRow item, Appendix appen)
        {
            if (!string.IsNullOrEmpty(item["Column3"].ToString()))
            {
                if (item["Column3"].ToString().Contains("\r\n"))
                {
                    var name = item["Column3"].ToString().Trim().Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    if (name.Length > 1)
                    {
                        appen.NameRus = item["Column3"].ToString().Trim().Split(new[] { Environment.NewLine }, StringSplitOptions.None)[0];
                        appen.Name = item["Column3"].ToString().Trim().Split(new[] { Environment.NewLine }, StringSplitOptions.None)[1];
                    }
                    else
                    {
                        appen.Name = item["Column3"].ToString().Trim().Split(new[] { Environment.NewLine }, StringSplitOptions.None)[0];
                    }

                }
                else
                {
                    appen.Name = item["Column3"].ToString().Trim();
                }
            }
        }
    }
}
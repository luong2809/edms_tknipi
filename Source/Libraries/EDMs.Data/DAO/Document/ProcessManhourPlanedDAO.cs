﻿

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
     public class ProcessManhourPlanedDAO : BaseDAO
    {
          /// <summary>
        /// Initializes a new instance of the <see cref="ProcessManhourPlanedDAO"/> class.
        /// </summary>
         public ProcessManhourPlanedDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<ProcessManhourPlaned> GetIQueryable()
        {
            return this.EDMsDataContext.ProcessManhourPlaneds;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ProcessManhourPlaned> GetAll()
        {
            return this.EDMsDataContext.ProcessManhourPlaneds.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public ProcessManhourPlaned GetById(int id)
        {
            return this.EDMsDataContext.ProcessManhourPlaneds.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(ProcessManhourPlaned ob)
        {
            try
            {
                this.EDMsDataContext.AddToProcessManhourPlaneds(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(ProcessManhourPlaned src)
        {
            try
            {
                ProcessManhourPlaned des = (from rs in this.EDMsDataContext.ProcessManhourPlaneds
                                where rs.ID == src.ID
                                select rs).First();

                des.ProjectId = src.ProjectId;
                des.WorkgroupId = src.WorkgroupId;
                des.StartDate = src.StartDate;
                des.EndDate = src.EndDate;
                des.DepartmentId = src.DepartmentId;
                des.Planed = src.Planed;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(ProcessManhourPlaned src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

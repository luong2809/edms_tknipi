﻿

namespace EDMs.Business.Services.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
    using EDMs.Data.DAO.Security;
  public   class CountUserAccessService
    {
      private readonly  CountUserAccessDAO repo;

    public CountUserAccessService()
        {
            this.repo = new CountUserAccessDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// 

    public CountUserAccess GetId(int id)
       {
           return this.repo.GetByID(id);
       }
    public List<CountUserAccess> GetAll()
    {
        return this.repo.GetAll();
    }
    public List<CountUserAccess> GetAllOfDate(DateTime date)
       {
           return this.repo.GetAll().Where(t => t.DateAccess.Value == date.Date).ToList();
       }

    public CountUserAccess GetDate(DateTime date)
    {
        return this.repo.GetDate(date);
    }
    public List<CountUserAccess> GetAllFromOfDate(DateTime from ,DateTime to)
    {
        return this.repo.GetAll().Where(t => t.DateAccess.Value >= from.Date && t.DateAccess.Value<= to.Date ).ToList();
    }
        #endregion

    public int? Insert(CountUserAccess bo)
       {
           return this.repo.Insert(bo);
       }


    public bool Update(CountUserAccess bo)
       {
           try
           {
               return this.repo.Update(bo);
           }
           catch (Exception)
           {
               return false;
           }
       }

    public bool Delete(CountUserAccess bo)
       {
           try
           {
               return this.repo.Delete(bo);
           }
           catch (Exception)
           {
               return false;
           }
       }

       /// <summary>
       /// Delete Resource By ID
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public bool Delete(int id)
       {
           try
           {
               return this.repo.Delete(id);
           }
           catch (Exception)
           {
               return false;
           }
       }
    }
}

﻿
/////


//////
namespace EDMs.Business.Services.Document
{

    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;
    using System.Globalization;


    public class UserManhourMonthService
    {
        private readonly UserManhourMonthDAO repo;

        public UserManhourMonthService()
        {
            this.repo = new UserManhourMonthDAO();
        }

        public List<UserManhourMonth> GetAll()
        {
            return this.repo.GetAll();
        }

        public List<UserManhourMonth> GetByUserID(int userID, int month, int year)
        {
            return this.repo.GetByUserID(userID, month, year);
        }

        public List<UserManhourMonth> GetByUserID(List<int> userID, int month, int year)
        {
            return this.repo.GetByUserID(userID, month, year);
        }

        public List<UserManhourMonth> GetByDocIDAndUserID(int documentID, int userID)
        {
            return this.repo.GetByDocIDAndUserID(documentID, userID);
        }

        public UserManhourMonth GetByDocIDAndUserID(int documentID, int userID, int month, int year)
        {
            return this.repo.GetByDocIDAndUserID(documentID, userID, month, year);
        }

        public UserManhourMonth GetLastedByDocIDAndUserID(int documentID, int userID, int month, int year)
        {
            return this.repo.GetLastedByDocIDAndUserID(documentID, userID, month, year);
        }

        public List<UserManhourMonth> GetByDocID(int documentID, int month, int year)
        {
            return this.repo.GetByDocID(documentID, month, year);
        }

        public UserManhourMonth GetById(int id)
        {
            return this.repo.GetById(id);
        }

        public int? Insert(UserManhourMonth bo)
        {
            return this.repo.Insert(bo);
        }

        public bool Update(UserManhourMonth bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(UserManhourMonth bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

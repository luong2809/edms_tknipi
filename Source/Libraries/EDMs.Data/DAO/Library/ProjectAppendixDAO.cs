﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProjectAppendixDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Library
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class ProjectAppendixDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectAppendixDAO"/> class.
        /// </summary>
        public ProjectAppendixDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<ProjectAppendix> GetIQueryable()
        {
            return this.EDMsDataContext.ProjectAppendixes;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ProjectAppendix> GetAll()
        {
            return this.EDMsDataContext.ProjectAppendixes.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public ProjectAppendix GetById(int id)
        {
            return this.EDMsDataContext.ProjectAppendixes.OrderByDescending(ob=>ob.ID).FirstOrDefault(ob => ob.ID == id);
        }

        #endregion

        #region GET ADVANCE
        public ProjectAppendix GetByProject(int projectID)
        {
            return this.EDMsDataContext.ProjectAppendixes.OrderByDescending(ob=>ob.ID).FirstOrDefault(ob => ob.ProjectID == projectID);
        }
        public ProjectAppendix GetByProject(int projectID, int year)
        {
            return this.EDMsDataContext.ProjectAppendixes.FirstOrDefault(ob => ob.ProjectID == projectID && ob.Year == year);
        }
        public ProjectAppendix GetByAppendix(int appendixID, List<int> workID, int year)
        {
            return this.EDMsDataContext.ProjectAppendixes.FirstOrDefault(ob => ob.AppendixID == appendixID && workID.Contains(ob.WorkID.Value) && ob.Year == year);
        }

        public List<ProjectAppendix> GetByWork(int workID)
        {
            return this.EDMsDataContext.ProjectAppendixes.Where(ob => ob.WorkID == workID).ToList();
        }

        public List<ProjectAppendix> GetByWork(int workID, int year)
        {
            return this.EDMsDataContext.ProjectAppendixes.Where(ob => ob.WorkID == workID && ob.Year == year).ToList();
        }

        public List<ProjectAppendix> GetAllByAppendix(int appendixID, List<int> workID, int year)
        {
            return this.EDMsDataContext.ProjectAppendixes.Where(ob => ob.AppendixID == appendixID && workID.Contains(ob.WorkID.Value) && ob.Year == year).ToList();
        }

        public List<ProjectAppendix> GetAllByAppendix(int appendixID)
        {
            return this.EDMsDataContext.ProjectAppendixes.Where(ob => ob.AppendixID == appendixID).ToList();
        }

        public List<ProjectAppendix> GetByWork(List<int> workID, int year)
        {
            return this.EDMsDataContext.ProjectAppendixes.Where(ob => ob.Year == year && workID.Contains(ob.WorkID.Value)).ToList();
        }

        public List<ProjectAppendix> GetByYear(int year)
        {
            return this.EDMsDataContext.ProjectAppendixes.Where(ob => ob.Year == year).ToList();
        }
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(ProjectAppendix ob)
        {
            try
            {
                this.EDMsDataContext.AddToProjectAppendixes(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(ProjectAppendix src)
        {
            try
            {
                var des = (from rs in this.EDMsDataContext.ProjectAppendixes
                           where rs.ID == src.ID
                           select rs).First();

                des.ProjectID = src.ProjectID;
                des.WorkID = src.WorkID;
                des.AppendixID = src.AppendixID;
                des.Year = src.Year;
                des.Basis = src.Basis;
                des.OrderCode = src.OrderCode;

                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(ProjectAppendix src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Scope;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Security;
using System.Text;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class WorkpackageAttachFiles : Page
    {
        private readonly AttachFilesWorkpackageService attachFilesWorkpackageService;

        private readonly WorkGroupService workGroupService;
        private readonly UserService userService;
        private readonly ScopeProjectService scopeProjectService;
        private readonly EmailNotificationTemplateService emailNotificationTemplateService;
        private readonly RoleService roleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public WorkpackageAttachFiles()
        {
            this.attachFilesWorkpackageService = new AttachFilesWorkpackageService();
            this.workGroupService = new WorkGroupService();
            this.userService = new UserService();
            this.scopeProjectService = new ScopeProjectService();
            this.emailNotificationTemplateService = new EmailNotificationTemplateService();
            this.roleService = new RoleService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (UserSession.Current.IsEngineer)
                {
                    this.UploadControl.Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                    this.grdDocument.Height = 447;
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.Session.Remove("IsFillData");
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageId = Convert.ToInt32(this.Request.QueryString["docId"]);
                var wpObj = this.workGroupService.GetById(workpackageId);
                const string targetFolder = "../../DocumentLibrary/WorkPackageFiles";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/WorkPackageFiles";
                var listUpload = docuploader.UploadedFiles;

                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var docFileName = docFile.FileName;

                        var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;

                        // Path file to save on server disc
                        var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);

                        // Path file to download from server
                        var serverFilePath = serverFolder + "/" + serverDocFileName;
                        var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                        
                        docFile.SaveAs(saveFilePath, true);

                        var attachFile = new AttachFilesWorkpackage()
                        {
                            WorkpackageId = workpackageId,
                            FileName = docFileName,
                            Extension = fileExt,
                            FilePath = serverFilePath,
                            ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                            FileSize = (double)docFile.ContentLength / 1024,
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedDate = DateTime.Now,
                            IsDefault = true
                        };

                        var defaultAttachFile = this.attachFilesWorkpackageService.GetAllByWorkpackage(workpackageId).FirstOrDefault(t => t.IsDefault.GetValueOrDefault());
                        if (defaultAttachFile != null)
                        {
                            defaultAttachFile.IsDefault = false;
                            this.attachFilesWorkpackageService.Update(defaultAttachFile);
                        }

                        
                        if (wpObj != null)
                        {
                            wpObj.DefaultFilePath = attachFile.FilePath;
                            wpObj.FileExtentionIcon = attachFile.ExtensionIcon;
                          
                            this.workGroupService.Update(wpObj);
                        }

                        this.attachFilesWorkpackageService.Insert(attachFile);
                    }
                    // Send notification
                    var Notify = this.roleService.GetByID(wpObj.DepartmentId.GetValueOrDefault());
                    if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false" && Notify.Notify.GetValueOrDefault() && UserSession.Current.User.IsGip.GetValueOrDefault())
                    {
                        this.NotificationAddNewWp(wpObj);
                    }
                }
            }

            this.docuploader.UploadedFiles.Clear();

            this.grdDocument.Rebind();
        }

        private void NotificationAddNewWp(WorkGroup wpObj)
        {
            if (wpObj.DepartmentId != 0 && wpObj.Complete.GetValueOrDefault() < 100)
            {
                List<User> userDPList = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault());
                var emailList = userDPList.Where(t => t.IsChief.GetValueOrDefault() || t.IsLeader.GetValueOrDefault())
                                .Select(t => t.Email)
                                .Where(t => !string.IsNullOrEmpty(t)).ToList();

                //var leader = userDPList.Where(t => t.IsLeader.GetValueOrDefault() && wpObj.DisciplineId == t.DisciplineID)
                //             .Select(t => t.Email)
                //             .Where(t => !string.IsNullOrEmpty(t)).ToList();

                var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.CreateNewWP);
                if (emailList.Count > 0 && notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                {
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };

                    var wpCreatedUser = this.userService.GetByID(wpObj.CreatedBy.GetValueOrDefault());
                    var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                    var subject = notificationTemplate.Subject.Replace("#WPNumber#", wpObj.Name).Replace("#WPDepartment#", wpObj.DepartmentName).Replace("#WPCreatedUser#", wpCreatedUser != null ? wpCreatedUser.FullName : string.Empty);

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    message.Body = notificationTemplate.Contents
                        .Replace("#WPNumber#", wpObj.Name)
                        .Replace("#WPDepartment#", wpObj.DepartmentName)
                        .Replace("#WPProjectNumber#", projectObj != null ? projectObj.Name : string.Empty)
                        .Replace("#WPProjectName#", projectObj != null ? projectObj.Description : string.Empty)
                        .Replace("#WPName#", wpObj.Description)
                        .Replace("#WPStartDate#", wpObj.StartDate != null ? wpObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPDeadline#", wpObj.Deadline != null ? wpObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPFinishDate#", wpObj.EndDate != null ? wpObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPIncomingNo#", wpObj.IncomingNo)
                        .Replace("#WPOutgoingNo#", wpObj.OutgoingNo)
                        .Replace("#WPWeight#", wpObj.Weight != null ? wpObj.Weight.Value.ToString() : string.Empty)
                        .Replace("#WPComplete#", wpObj.Complete != null ? wpObj.Complete.Value.ToString() : string.Empty);

                    //5 user 2in1 role chief and GIP
                    List<int> userSpecial = ConfigurationManager.AppSettings.Get("ChiefandGIP").Split(',').Select(Int32.Parse).ToList();
                    var specialList = userDPList.Where(t => userSpecial.Contains(t.Id))
                        .Select(t => t.Email)
                        .Where(t => !string.IsNullOrEmpty(t)).ToList();
                    foreach (var to in specialList)
                    {
                        message.To.Add(new MailAddress(to));
                    }

                    foreach (var to in emailList)
                    {
                        message.To.Add(new MailAddress(to));
                    }

                    //foreach (var to in leader)
                    //{
                    //    message.CC.Add(new MailAddress(to));
                    //}
                    var userdc = this.userService.GetByID(projectObj.DocumentControlId.GetValueOrDefault());
                    if (userdc != null && !string.IsNullOrEmpty(userdc.Email))
                    {
                        message.CC.Add(new MailAddress(userdc.Email));
                    }
                    else
                    {
                        var userdclist = this.userService.GetAll(false).Where(t => t.IsDC.GetValueOrDefault() && !string.IsNullOrEmpty(t.Email)).ToList();
                        foreach (var item in userdclist)
                        {
                            message.CC.Add(new MailAddress(item.Email));
                        }
                    }
                    if (File.Exists(Server.MapPath(wpObj.DefaultFilePath)))
                    {
                        message.Attachments.Add(new Attachment(Server.MapPath(wpObj.DefaultFilePath)));
                    }
                    //message.To.Add(new MailAddress("edms.cd@vietsov.com.vn"));
                    smtpClient.Send(message);
                }
            }
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var wpId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var attachFileObj = this.attachFilesWorkpackageService.GetById(wpId);

            if (attachFileObj != null && attachFileObj.IsDefault.GetValueOrDefault())
            {
                var attachFiles = this.attachFilesWorkpackageService.GetAllByWorkpackage(attachFileObj.WorkpackageId.GetValueOrDefault()).Where(t => !t.IsDefault.GetValueOrDefault()).ToList();

                var projectObj = this.workGroupService.GetById(attachFileObj.WorkpackageId.GetValueOrDefault());

                if (attachFiles.Count > 0)
                {
                    attachFiles[0].IsDefault = true;
                    this.attachFilesWorkpackageService.Update(attachFiles[0]);


                    if (projectObj != null)
                    {
                        projectObj.DefaultFilePath = attachFiles[0].FilePath;
                        projectObj.FileExtentionIcon = attachFiles[0].ExtensionIcon;
                        this.workGroupService.Update(projectObj);
                    }

                }
                else
                {
                    if (projectObj != null)
                    {
                        projectObj.DefaultFilePath = string.Empty;
                        projectObj.FileExtentionIcon = string.Empty;
                        this.workGroupService.Update(projectObj);
                    }
                }
            }
            this.attachFilesWorkpackageService.Delete(wpId);
            this.grdDocument.Rebind();
        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
              
               var attachFilesProject  =  this.attachFilesWorkpackageService.GetAllByWorkpackage(workpackageId).OrderByDescending(t => t.CreatedDate).ThenByDescending(t => t.FileName).ToList();
               if (attachFilesProject != null)
               {
                   this.grdDocument.DataSource = attachFilesProject;
               }
               else
               {
                   this.grdDocument.DataSource = new List<AttachFilesProject>();
               }
            }
            else
            {
                this.grdDocument.DataSource = new List<AttachFilesProject>();
            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            throw new NotImplementedException();
        }

        protected void rbtnDefaultDoc_CheckedChanged(object sender, EventArgs e)
        {
            ((GridItem)((RadioButton)sender).Parent.Parent).Selected = ((RadioButton)sender).Checked;

            var item = ((RadioButton)sender).Parent.Parent as GridDataItem;
            var attachFileId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var attachFileObj = this.attachFilesWorkpackageService.GetById(attachFileId);
            if (attachFileObj != null)
            {
                var attachFiles = this.attachFilesWorkpackageService.GetAllByWorkpackage(attachFileObj.WorkpackageId.GetValueOrDefault());
                foreach (var attachFile in attachFiles)
                {
                    attachFile.IsDefault = (attachFile.ID == attachFileId) ? true: false;

                    if (attachFile.IsDefault.GetValueOrDefault())
                    {
                        var projectObj = this.workGroupService.GetById(attachFile.WorkpackageId.GetValueOrDefault());
                        if (projectObj != null)
                        {
                            projectObj.DefaultFilePath = attachFile.FilePath;
                            projectObj.FileExtentionIcon = attachFile.ExtensionIcon;
                            this.workGroupService.Update(projectObj);
                        }
                    }

                    this.attachFilesWorkpackageService.Update(attachFile);
                }
            }
        }
    }
}
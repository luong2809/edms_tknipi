﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.DAO;
using EDMs.Data.Entities;

namespace EDMs.Business.Services
{
    public class EmployeeService
    {      
        private readonly EmployeeDAO repo;

        public EmployeeService()
        {
            repo = new EmployeeDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All Employee
        /// </summary>
        /// <returns></returns>
        public List<Employee> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get Employee By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Employee GetByID(int ID)
        {
            return repo.GetByID( ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Employee
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(Employee bo)
        {
            try
            {
                return repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update Employee
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Employee bo)
        {
            try
            {
                return repo.Update(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Employee
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Employee bo)
        {
            try
            {
                return repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Employee By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

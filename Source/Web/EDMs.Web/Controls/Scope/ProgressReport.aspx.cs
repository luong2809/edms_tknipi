﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Scope;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ProgressReport : Page
    {
        private readonly AttachFilesWorkpackageService attachFilesWorkpackageService;

        private readonly WorkGroupService workGroupService;
        private readonly ScopeProjectService scopeProjectService;
        private readonly MilestoneService milestoneService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ProgressReport()
        {
            this.attachFilesWorkpackageService = new AttachFilesWorkpackageService();
            this.workGroupService = new WorkGroupService();
            this.scopeProjectService = new ScopeProjectService();
            this.milestoneService = new MilestoneService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["projId"]))
                {
                    var projectId = Convert.ToInt32(Request.QueryString["projId"]);

                    var lineSeries = this.LineChart.PlotArea.Series[1] as LineSeries;

                    var processReportList = new List<ProcessReport>();
                    var temp = new List<ProcessReport>();

                    var projectObj = this.scopeProjectService.GetById(projectId);
                    if (projectObj != null)
                    {
                        var wpIDList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID).Select(t => t.ID).ToList();
                        var milestoneList = this.milestoneService.GetAllByWorkpackage(wpIDList);

                        var minDate = milestoneList.Min(t => t.MilestoneDate).GetValueOrDefault();
                        var maxDate = milestoneList.Max(t => t.MilestoneDate).GetValueOrDefault();
                        var differenceMonth = ((maxDate.Year - minDate.Year) * 12) + maxDate.Month - minDate.Month;
                        for (int i = 0; i < differenceMonth; i++)
                        {
                            var processReport = new ProcessReport();
                            processReport.STRWeekDate = minDate.AddMonths(i).ToString("MM/yyyy");

                            var milstoneOfMonth = milestoneList
                                    .Where(t => t.MilestoneDate.GetValueOrDefault().Month == minDate.AddMonths(i).Month
                                                && t.MilestoneDate.GetValueOrDefault().Year == minDate.AddMonths(i).Year).ToList();

                            foreach (var milestoneOfWP in milstoneOfMonth.GroupBy(t => t.WorkpackageId))
                            {
                                var wpObj = this.workGroupService.GetById(milestoneOfWP.Key.GetValueOrDefault());
                                if (wpObj != null)
                                {
                                    processReport.Planed += (milestoneOfWP.ToList().Average(t => t.PlanTotal).GetValueOrDefault() * wpObj.Weight.GetValueOrDefault()) / 100;

                                    processReport.Actual += (milestoneOfWP.ToList().Average(t => t.RealTotal).GetValueOrDefault() * wpObj.Weight.GetValueOrDefault()) / 100;
                                }
                            }

                            processReportList.Add(processReport);
                            temp.Add(processReport);


                        }

                        this.LineChart.ChartTitle.Text = "DETAIL ENGINEERING SERVICE FOR " + projectObj.Name +
                                                         " | Cut-off: " +
                                                         DateTime.Now.ToString("dd/MM/yyyy");


                        this.LineChart.DataSource = processReportList;
                        this.LineChart.DataBind();
                        if (lineSeries != null)
                        {
                            lineSeries.Items.Clear();

                            foreach (var actual in temp)
                            {

                                if (actual.Actual == 0)
                                {
                                    lineSeries.Items.Add((decimal?)null);
                                }
                                else
                                {
                                    lineSeries.Items.Add((decimal?)actual.Actual);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.Entities;
using System.Linq.Expressions;

namespace EDMs.Data.DAO
{   
    public class ServiceTypeDAO : BaseDAO
    {
        public ServiceTypeDAO() : base() { }

        #region GET (Basic)
        public IQueryable<ServiceType> GetIQueryable()
        {
            return EDMsDataContext.ServiceTypes;
        }
        
        public List<ServiceType> GetAll()
        {
            return EDMsDataContext.ServiceTypes.ToList<ServiceType>();
        }

        public ServiceType GetByID(int ID)
        {
            return EDMsDataContext.ServiceTypes.Where(ob => ob.Id == ID).FirstOrDefault<ServiceType>();
        }
       
        #endregion

        #region GET ADVANCE
        /// <summary>
        /// Find By Any expression
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<ServiceType> Find(Expression<Func<ServiceType, bool>> where)
        {
            return EDMsDataContext.ServiceTypes.Where(where).ToList<ServiceType>();
        }
        #endregion

        #region Insert, Update, Delete
        public bool Insert(ServiceType ob)
        {
            try
            {
                EDMsDataContext.AddToServiceTypes(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(ServiceType ob)
        {
            try
            {
                //ServiceType _ob = GetByID(ob.Id);
                //if (_ob != null)
                //{
                    //EDMsDataContext.ApplyCurrentValues(_ob.EntityKey.EntitySetName, ob);
                    //EDMsDataContext.SaveChanges();
                //    return true;
                //}
                //else
                //    return false;
                ServiceType _ob;

                _ob = (from emp in EDMsDataContext.ServiceTypes
                      where emp.Id == ob.Id
                      select emp).First();

               // _ob.Id = ob.Id;
                _ob.Name = ob.Name;
                _ob.Color = ob.Color;
                _ob.CssClass = ob.CssClass;
                _ob.Description = ob.Description;               
                _ob.LastUpdate = ob.LastUpdate;               
                _ob.UpdateBy = ob.UpdateBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(ServiceType ob)
        {
            try
            {
                ServiceType _ob = GetByID(ob.Id);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                ServiceType _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

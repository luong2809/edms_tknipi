﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Scope;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using OfficeHelper.Utilities.Data;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class WorkpackageFTK05S : Page
    {

        private readonly WorkGroupService workGroupService;
        private readonly UserService userService;
        private readonly DocumentPackageService documentPackageService;
        private readonly RoleService roleService;
        private readonly ScopeProjectService scopeProjectService;
  
        /// <summary>
        /// Initializes a new instance of the <see cref="TransmittalAttachDocument"/> class.
        /// </summary>
        public WorkpackageFTK05S()
        {

            this.workGroupService = new WorkGroupService();
            this.roleService = new RoleService();
            this.scopeProjectService = new ScopeProjectService();
            this.userService = new UserService();
            this.documentPackageService = new DocumentPackageService();
          
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    var workpackageObj = this.workGroupService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
                    this.txtProject.Text = workpackageObj.ProjectName;
                    this.txtDepartment.Text = workpackageObj.DepartmentName;
                    this.txtWorkpackage.Text = workpackageObj.Name;
                    this.txtDeadline.SelectedDate = DateTime.Now.Date;
                    this.txtContent.Focus();
                }
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                var listcontent = this.documentPackageService.GetAllByWorkgroup(workpackageId)
                       .OrderBy(t => t.DocNo)
                       .ToList();

                this.grdDocument.DataSource = listcontent;
            }
            else
            {
                this.grdDocument.DataSource = new List<DocumentPackage>();
            }
        }

        /// <summary>
        /// The rad menu_ item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
       

        /// <summary>
        /// The btn save_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            
        }
        protected void grdDocument_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                //var item = e.Item as GridDataItem;
                //if (item["HasAttachFile"].Text == "True")
                //{
                //    item.BackColor = Color.Aqua;
                //    item.BorderColor = Color.Aqua;
                //}
            }
        }

        protected void btnS_Click(object sender, EventArgs e)
        {
            var selectedWP = this.workGroupService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
            if (selectedWP != null)
            {
                var dataTable = new DataTable();
                var reportInfo = new DataTable();

                var ds = new DataSet();
                var listColumn = new[]
                    {
                        new DataColumn("Index", Type.GetType("System.String")),
                        new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                        new DataColumn("DeadLine", Type.GetType("System.String")),
                        new DataColumn("Note", Type.GetType("System.String"))
                    };
                dataTable.Columns.AddRange(listColumn);

                var listColumn1 = new[]
                    {
                        new DataColumn("DepartmentName", Type.GetType("System.String")),
                        new DataColumn("ProjectName", Type.GetType("System.String")),
                        new DataColumn("WorkpackageName", Type.GetType("System.String")),
                        new DataColumn("WPStartDate", Type.GetType("System.String")),
                        new DataColumn("GIP", Type.GetType("System.String")),
                        new DataColumn("Chief", Type.GetType("System.String")),
                        new DataColumn("Date", Type.GetType("System.String")),
                        new DataColumn("Contain", Type.GetType("System.String")), 
                    };
                reportInfo.Columns.AddRange(listColumn1);

                var infoItem = reportInfo.NewRow();
                var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                var projectManager =
                    this.userService.GetByID(projectObj != null
                        ? projectObj.ProjectManagerId.GetValueOrDefault()
                        : 0);
                var Departmentmanager = this.userService.GetAllByRoleId(selectedWP.DepartmentId.GetValueOrDefault()).Where(t => t.Position != null && convertToUnSign(t.Position.ToLower()) == "truong phong").ToList();
                var deparment = this.roleService.GetByID(selectedWP.DepartmentId.GetValueOrDefault());

                infoItem["DepartmentName"] = deparment != null
                    ? deparment.RussiaName
                    : string.Empty;

                // infoItem["ProjectDescription"] = projectObj != null ? projectObj.Description : string.Empty;
                infoItem["ProjectName"] = projectObj != null ? projectObj.Name : string.Empty;
                infoItem["WorkpackageName"] = selectedWP.Name;
                infoItem["WPStartDate"] = "день " + DateTime.Now.Day + " месяц " +
                          DateTime.Now.Month + " год " +
                          DateTime.Now.Year;

                infoItem["GIP"] = projectManager != null ? projectManager.FullName : string.Empty;
                infoItem["Chief"] = Departmentmanager.Count>0 ? Departmentmanager[0].FullName : string.Empty;
                infoItem["Date"] = DateTime.Now.ToString("dd/MM/yyyy");

                infoItem["Contain"] = this.txtContent.Text + "\r\n";
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                    var docObj = this.documentPackageService.GetById(docId);
                    infoItem["Contain"] += "-" + docObj.DocNo + "_" + docObj.RevisionName + "_" + docObj.DocTitle + "\r\n";
                }

                reportInfo.Rows.Add(infoItem);

                ds.Tables.Add(dataTable);
                ds.Tables[0].TableName = "Table";

                var rootPath = Server.MapPath("../../Exports") + @"\";
                const string WordPath = @"Template\";
                const string WordPathExport = @"Generated\";
                var StrTemplateFileName = "F-TK-05 Rev2 Russ.doc";
                var strOutputFileName = "F-TK-05_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                var isSuccess = OfficeCommon.ExportToWordWithRegion(
                    rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                if (isSuccess)
                {
                    this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "F-TK-05_" + Utility.RemoveSpecialCharacter(selectedWP.Name, "-") + ".doc", true);
                }
            }
            else
            {
                var st = "Content is empty! Please enter contents of Workpackage.";
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + st + "');</script>");
            }

            this.grdDocument.Rebind();
        }

        protected void btnV_Click(object sender, EventArgs e)
        {

            var selectedWP = this.workGroupService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
            if (selectedWP != null)
            {

                  var dataTable = new DataTable();
                var reportInfo = new DataTable();

                var ds = new DataSet();
                var listColumn = new[]
                    {
                        new DataColumn("Index", Type.GetType("System.String")),
                        new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                        new DataColumn("DeadLine", Type.GetType("System.String")),
                        new DataColumn("Note", Type.GetType("System.String"))
                    };
                dataTable.Columns.AddRange(listColumn);

                var listColumn1 = new[]
                    {
                        new DataColumn("DepartmentName", Type.GetType("System.String")),
                        new DataColumn("ProjectName", Type.GetType("System.String")),
                        new DataColumn("WorkpackageName", Type.GetType("System.String")),
                        new DataColumn("WPStartDate", Type.GetType("System.String")),
                        new DataColumn("GIP", Type.GetType("System.String")),
                        new DataColumn("Chief", Type.GetType("System.String")),
                        new DataColumn("Date", Type.GetType("System.String")),
                        new DataColumn("Contain", Type.GetType("System.String")), 
                    };
                reportInfo.Columns.AddRange(listColumn1);

                var infoItem = reportInfo.NewRow();
                var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                var projectManager =
                    this.userService.GetByID(projectObj != null
                        ? projectObj.ProjectManagerId.GetValueOrDefault()
                        : 0);
                var Departmentmanager = this.userService.GetAllByRoleId(selectedWP.DepartmentId.GetValueOrDefault()).Where(t => t.Position != null && convertToUnSign(t.Position.ToLower()) == "truong phong").ToList();
               
                var deparment = this.roleService.GetByID(selectedWP.DepartmentId.GetValueOrDefault());

                infoItem["DepartmentName"] = deparment != null
                    ? deparment.Name
                    : string.Empty;

               // infoItem["ProjectDescription"] = projectObj != null ? projectObj.Description : string.Empty;
                infoItem["ProjectName"] = projectObj != null ? projectObj.Name : string.Empty;
                infoItem["WorkpackageName"] = selectedWP.Name;
                infoItem["WPStartDate"] = "Ngày " + DateTime.Now.Day + " Tháng " +
                          DateTime.Now.Month + " Năm " +
                          DateTime.Now.Year;

                infoItem["GIP"] = projectManager != null ? projectManager.FullName : string.Empty;
                infoItem["Chief"] = Departmentmanager.Count>0 ? Departmentmanager[0].FullName : string.Empty;
                infoItem["Date"] = DateTime.Now.ToString("dd/MM/yyyy");

                infoItem["Contain"] = this.txtContent.Text + "\r\n";
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                    var docObj = this.documentPackageService.GetById(docId);
                    infoItem["Contain"] += "-" + docObj.DocNo + "_" + docObj.RevisionName + "_" + docObj.DocTitle + "\r\n";
                }

                reportInfo.Rows.Add(infoItem);

                ds.Tables.Add(dataTable);
                ds.Tables[0].TableName = "Table";

                var rootPath = Server.MapPath("../../Exports") + @"\";
                const string WordPath = @"Template\";
                const string WordPathExport = @"Generated\";
                var StrTemplateFileName = "F-TK-05 Viet Rev2.doc";
                var strOutputFileName = "F-TK-05_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                var isSuccess = OfficeCommon.ExportToWordWithRegion(
                    rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                if (isSuccess)
                {
                    this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "F-TK-05_" + Utility.RemoveSpecialCharacter(selectedWP.Name, "-") + ".doc", true);
                }
            }
            else
            {
                var st = "Content is empty! Please enter contents of Workpackage.";
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + st + "');</script>");
            }

            this.grdDocument.Rebind();
        }
        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                var fs = new FileStream(strFileName, FileMode.Open);
                var streamLength = Convert.ToInt32(fs.Length);
                var data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public static string convertToUnSign(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }  
    }
}
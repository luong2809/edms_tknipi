﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PatientService.cs" company="">
//   
// </copyright>
// <summary>
//   The patient service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Data.DAO;
    using Data.Entities;

    /// <summary>
    /// The patient service.
    /// </summary>
    public class PatientService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly PatientDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientService"/> class.
        /// </summary>
        public PatientService()
        {
            this.repo = new PatientDAO();
        }

        #region Get (Advances)
         /// <summary>
        /// Get All Patient with PatientStatusId
        /// </summary>
        /// <returns>
        /// List patient
        /// </returns>
        public List<Patient> GetByPatient(int patientStatusId)
        {            
            return repo.GetByPatient(patientStatusId);
        }

        /// <summary>
        /// Find Patients with FullName
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public List<Patient> Find(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
                return repo.GetAll();
            
            return repo.Find(fullName);
        }

        /// <summary>
        /// Find Patients With FullName and PatientStatusId
        /// </summary>
        /// <param name="fullName"></param>
        /// <param name="patientStatusId"></param>
        /// <returns></returns>
        public List<Patient> Find(string fullName, int patientStatusId)
        {
            if (string.IsNullOrEmpty(fullName))
                return repo.GetByPatient(patientStatusId);
            
            return repo.Find(fullName, patientStatusId);
        }

        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Patient
        /// </summary>
        /// <returns></returns>
        public List<Patient> GetAll()
        {
            return repo.GetAll();
        }

        /// <summary>
        /// Get Patient By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Patient GetByID(int ID)
        {
            return repo.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Patient
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(Patient bo)
        {
            try
            {
                return repo.Insert(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update Patient
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Patient bo)
        {
            try
            {
                return repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Patient
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Patient bo)
        {
            try
            {
                return repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Patient By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
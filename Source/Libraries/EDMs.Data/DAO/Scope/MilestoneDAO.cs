﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MilestoneDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Scope
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class MilestoneDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MilestoneDAO"/> class.
        /// </summary>
        public MilestoneDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Milestone> GetIQueryable()
        {
            return this.EDMsDataContext.Milestones;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Milestone> GetAll()
        {
            return this.EDMsDataContext.Milestones.OrderByDescending(t => t.ID).ToList();
        }

        public Milestone GetByLasted(int wpID)
        {
            return this.EDMsDataContext.Milestones.OrderByDescending(ob=>ob.ID).FirstOrDefault(ob => ob.WorkpackageId == wpID);
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public Milestone GetById(int id)
        {
            return this.EDMsDataContext.Milestones.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(Milestone ob)
        {
            try
            {
                this.EDMsDataContext.AddToMilestones(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(Milestone src)
        {
            try
            {
                Milestone des = (from rs in this.EDMsDataContext.Milestones
                                where rs.ID == src.ID
                                select rs).First();

                des.MilestoneDate = src.MilestoneDate;
                des.PlanPercent = src.PlanPercent;
                des.PlanTotal = src.PlanTotal;
                des.PlanOfYear = src.PlanOfYear;
                des.Real = src.Real;
                des.RealTotal = src.RealTotal;
                des.PerformingUser = src.PerformingUser;
                des.Note = src.Note;
                des.WorkpackageId = src.WorkpackageId;
                des.WorkpackageName = src.WorkpackageName;
                des.UpdatedBy = src.UpdatedBy;
                des.UpdatedDate = src.UpdatedDate;
                des.DescriptionOfDuty = src.DescriptionOfDuty;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(Milestone src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

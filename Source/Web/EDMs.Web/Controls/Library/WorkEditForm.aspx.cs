﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Library
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class WorkEditForm : Page
    {
        private readonly WorkService workService = new WorkService();
        private readonly UserService userService = new UserService();
        private readonly BlockService blockService = new BlockService();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public WorkEditForm()
        {
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["id"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = this.workService.GetById(Convert.ToInt32(this.Request.QueryString["id"]));
                    if (obj != null)
                    {
                        this.txtName.Text = obj.Name;
                        this.txtDescription.Text = obj.Description;
                        this.ddlType.SelectedValue = obj.Type.ToString();
                        this.ddlYear.SelectedValue = obj.Year.ToString();
                        this.ddlBlock.SelectedValue = obj.BlockId.ToString();

                        var workList = this.workService.GetByTypeAndYear(Convert.ToInt32(ddlType.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
                        workList.Insert(0, new Work() { ID = 0, Name = string.Empty });
                        this.ddlWorkParent.DataSource = workList;
                        this.ddlWorkParent.DataTextField = "Name";
                        this.ddlWorkParent.DataValueField = "ID";
                        this.ddlWorkParent.DataBind();
                        ddlWorkParent.SelectedValue = obj.ParentID.ToString();
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        private void ExpandParentNode(RadTreeNode node)
        {
            if (node.ParentNode != null)
            {
                node.ParentNode.Expanded = true;
                this.ExpandParentNode(node.ParentNode);
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["id"]))
                {
                    var id = Convert.ToInt32(this.Request.QueryString["id"]);
                    var obj = this.workService.GetById(id);
                    if (obj != null)
                    {
                        obj.Name = txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        obj.Type = Convert.ToInt32(ddlType.SelectedValue);
                        obj.Year = Convert.ToInt32(ddlYear.SelectedValue);
                        obj.BlockId= Convert.ToInt32(ddlBlock.SelectedValue);
                        if (ddlWorkParent.SelectedValue != "0")
                        {
                            obj.ParentID = Convert.ToInt32(ddlWorkParent.SelectedValue);
                        }
                        else
                        {
                            obj.ParentID = null;
                        }
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;
                        this.workService.Update(obj);
                    }
                }
                else
                {
                    Work obj = new Work();
                    obj.Name = txtName.Text.Trim();
                    obj.Description = this.txtDescription.Text.Trim();
                    obj.Type = Convert.ToInt32(ddlType.SelectedValue);
                    obj.Year = Convert.ToInt32(ddlYear.SelectedValue);
                    obj.BlockId = Convert.ToInt32(ddlBlock.SelectedValue);
                    if (ddlWorkParent.SelectedValue != "0")
                    {
                        obj.ParentID = Convert.ToInt32(ddlWorkParent.SelectedValue);
                    }
                    else
                    {
                        obj.ParentID = null;
                    }
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    this.workService.Insert(obj);
                }
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if (this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Field name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        private void LoadComboData()
        {
            for (var i = DateTime.Now.Year + 5; i >= 2017; i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.SelectedValue = DateTime.Now.Year.ToString();

            var blockList = this.blockService.GetAll();
            blockList.Insert(0, new Block() { ID = 0 });
            this.ddlBlock.DataSource = blockList;
            this.ddlBlock.DataTextField = "Name";
            this.ddlBlock.DataValueField = "ID";
            this.ddlBlock.DataBind();
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var workList = this.workService.GetByTypeAndYear(Convert.ToInt32(ddlType.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
            workList.Insert(0, new Work() { ID = 0, Name = string.Empty });
            this.ddlWorkParent.DataSource = workList;
            this.ddlWorkParent.DataTextField = "Name";
            this.ddlWorkParent.DataValueField = "ID";
            this.ddlWorkParent.DataBind();
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            var workList = this.workService.GetByTypeAndYear(Convert.ToInt32(ddlType.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
            workList.Insert(0, new Work() { ID = 0, Name = string.Empty });
            this.ddlWorkParent.DataSource = workList;
            this.ddlWorkParent.DataTextField = "Name";
            this.ddlWorkParent.DataValueField = "ID";
            this.ddlWorkParent.DataBind();
        }
    }
}
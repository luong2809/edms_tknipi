﻿

namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;
    public class ProcessManhourActualService
    {
         private readonly ProcessManhourActualDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessManhourActualService"/> class.
        /// </summary>
         public ProcessManhourActualService()
        {
            this.repo = new ProcessManhourActualDAO();
        }

        #region Get (Advances)
        public List<ProcessManhourActual> GetAllByProject(int projectId, int workgroupId)
        {
            return this.repo.GetAll().Where(t => t.ProjectId == projectId && (workgroupId == 0 || t.WorkgroupId == workgroupId)).ToList();
        }

        public ProcessManhourActual GetByProjectAndWorkgroup(int projectId, int workgroupid)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.ProjectId == projectId && t.WorkgroupId == workgroupid);
        }
        public List<ProcessManhourActual> GetByDepartment(int  departmentId)
          {
              return this.repo.GetAll().Where(t => t.DepartmentId==departmentId).ToList();
        }
        public List<ProcessManhourActual> GetAllCurrent(List<int> DepartmentId, DateTime days)
        {
            return this.repo.GetAll().Where(t => DepartmentId.Contains(t.DepartmentId.GetValueOrDefault())
                && (t.StartDate <= days && t.EndDate >= days)).ToList();
        }
        public List<ProcessManhourActual> GetByJustProject(int projectId)
        {
            return this.repo.GetAll().Where(t => t.ProjectId == projectId).ToList();
        }
        public List<ProcessManhourActual> GetAllCurrent(int DepartmentId, DateTime days)
        {
            return this.repo.GetAll().Where(t => t.DepartmentId == DepartmentId
                && (t.StartDate <= days && t.EndDate >= days)).ToList();
        }
        public List<ProcessManhourActual> GetAllCurrentProject(int projectId, DateTime days)
        {
            return this.repo.GetAll().Where(t => t.ProjectId == projectId
                && (t.StartDate <= days && t.EndDate >= days)).ToList();
        }
        public List<ProcessManhourActual> GetAllCurrent(DateTime days)
        {
            return this.repo.GetAll().Where(t => t.StartDate <= days && t.EndDate >= days).ToList();
        }
        public ProcessManhourActual GetByDepartmentAndWorkgroup(int department, int workgroupid)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.DepartmentId == department && t.WorkgroupId == workgroupid);
        }

        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The ProcessManhourActual
        /// </returns>
        public List<ProcessManhourActual> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of ProcessManhourActual
        /// </param>
        /// <returns>
        /// The ProcessManhourActual</returns>
        public ProcessManhourActual GetById(int id)
        {
            return this.repo.GetById(id);
        }

        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(ProcessManhourActual bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(ProcessManhourActual bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(ProcessManhourActual bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceProcess;
using System.Text;
using System.Web;
using System.Data;
using System.Reflection;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using OfficeHelper.Utilities.Data;
using Telerik.Web.UI;
using CheckBox = System.Web.UI.WebControls.CheckBox;
using Image = System.Web.UI.WebControls.Image;
using RadioButton = System.Web.UI.WebControls.RadioButton;
using System.Text.RegularExpressions;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// Class customer
    /// </summary>
    public partial class ScopeProjectList : Page
    {
        private readonly PermissionService permissionService = new PermissionService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly FieldService fieldService = new FieldService();
        private readonly PlatformService platformService = new PlatformService();
        private readonly SupervisorService supervisorService = new SupervisorService();
        private readonly UserService userService = new UserService();
        private readonly WorkGroupService workGroupService = new WorkGroupService();
        private readonly WorkpackageContentService workpackageContentService = new WorkpackageContentService();
        private readonly RoleService roleService = new RoleService();
        private readonly AreaService areaService = new AreaService();
        private readonly DisciplineService disciplineService = new DisciplineService();
        private readonly WPRevisionService wpRevisionService = new WPRevisionService();
        private readonly MilestoneService milestoneService = new MilestoneService();
        //protected const string unreadPattern = @"\(\d+\)";
        private readonly TemplateManagementService templateManagementService = new TemplateManagementService();
        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();
        private readonly ProjectAppendixService projectAppendixService = new ProjectAppendixService();
        private readonly DistributionMatrixDetailService distributionMatrixDetailService = new DistributionMatrixDetailService();
        private readonly ProjectShareService projectShareService = new ProjectShareService();
        private readonly WorkService workListService = new WorkService();
        private readonly FolderService folderService = new FolderService();
        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            RadPane temp = (RadPane)this.Master.FindControl("leftPane");
            temp.Collapsed = true;
            if (!this.Page.IsPostBack)
            {
                LoadComboData();
                //chinh sua cho eng xem tat ca du an
                if (!this.roleService.IsNipi(UserSession.Current.RoleId))
                {
                    PermissionOnlyView();

                    for (int i = 0; i < this.radMenu.Items.Count; i++)
                    {
                        if (i != 11)
                        {
                            this.radMenu.Items[i].Visible = false;
                        }
                    }
                }
                else if (UserSession.Current.IsEngineer)
                {
                    PermissionOnlyView();
                    this.IsEngineer.Value = "true";

                    for (int i = 0; i < this.radMenu.Items.Count; i++)
                    {
                        if (i != 8)
                        {
                            this.radMenu.Items[i].Visible = false;
                        }
                    }
                }
                else if (UserSession.Current.IsLeader)
                {
                    PermissionOnlyExport();
                    var list = new List<int>() { 8, 9 };
                    for (int i = 0; i < this.radMenu.Items.Count; i++)
                    {
                        if (!list.Contains(i))
                        {
                            this.radMenu.Items[i].Visible = false;
                        }
                    }
                }
                else if ( UserSession.Current.IsSuperViewer || UserSession.Current.IsCheif)
                {
                    PermissionOnlyExport();
                    var list = new List<int>() { 8, 9, 25, 26 };
                    for (int i = 0; i < this.radMenu.Items.Count; i++)
                    {
                        if (!list.Contains(i))
                        {
                            this.radMenu.Items[i].Visible = false;
                        }
                    }
                }

                //this.LoadListPanel();
                //this.LoadSystemPanel();
                if (UserSession.Current.User.Username == "trongvv.rd")
                {
                    var ddlShow = (DropDownList)this.CustomerMenu.Items[4].FindControl("ddlShow");
                    ddlShow.SelectedIndex = 1;
                }
            }
        }

        private void LoadComboData()
        {
            RadToolBarItem itemToolbar = this.CustomerMenu.FindItemByValue("toolbarWork");
            var ddlYear = (DropDownList)itemToolbar.FindControl("ddlYear");
            for (var i = DateTime.Now.Year + 5; i >= 2018; i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.Items.Insert(0, new ListItem(string.Empty, "0"));
            ddlYear.SelectedIndex = 0;
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadToolBarItem itemToolbar = this.CustomerMenu.FindItemByValue("toolbarWork");
            var ddlYear = (DropDownList)itemToolbar.FindControl("ddlYear");
            var rcbWorkList = (RadComboBox)itemToolbar.FindControl("rcbWorkList");
            var workList = this.workListService.GetByTypeAndYear(1, Convert.ToInt32(ddlYear.SelectedValue));
            var rtvWorkList = (RadTreeView)rcbWorkList.Items[0].FindControl("rtvWorkList");
            if (rtvWorkList != null)
            {
                rcbWorkList.Text = "";
                rtvWorkList.DataTextField = "Name";
                rtvWorkList.DataValueField = "ID";
                rtvWorkList.DataFieldID = "ID";
                rtvWorkList.DataFieldParentID = "ParentID";
                rtvWorkList.DataSource = workList;
                rtvWorkList.DataBind();
                rtvWorkList.CollapseAllNodes();
            }
        }

        private void PermissionOnlyExport()
        {
            this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
            this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
            this.CustomerMenu.Items[0].Visible = false;
            this.CustomerMenu.Items[1].Visible = false;
            this.RadPane3.Visible = true;
        }

        private void PermissionOnlyView()
        {
            PermissionOnlyExport();
            this.CustomerMenu.Items[2].Visible = false;
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(bool isbind = false)
        {
            var ddlShow = (DropDownList)this.CustomerMenu.Items[4].FindControl("ddlShow");
            var projectList = new List<ScopeProject>();
            if (this.roleService.IsNipi(UserSession.Current.RoleId))
            {
                projectList = this.scopeProjectService.GetAll();
                if (UserSession.Current.IsEngineer || UserSession.Current.IsLeader || UserSession.Current.IsCheif)
                {
                    var projectDepartment = this.workGroupService.GetAllByDepartment(UserSession.Current.RoleId).Select(t => t.ProjectId.GetValueOrDefault()).Distinct().ToList();
                    projectList = projectList.Where(t => projectDepartment.Contains(t.ID)).ToList();
                }
                foreach (var scopeProject in projectList)
                {
                    scopeProject.IsFullPermission = (!UserSession.Current.IsGip && !UserSession.Current.IsCheif) || scopeProject.ProjectManagerId == UserSession.Current.User.Id;
                }

                if (ddlShow.SelectedValue == "ShowUncomplete")
                {
                    projectList = projectList.Where(t => (t.Complete == null || t.Complete < 100) && !t.IsStop.GetValueOrDefault()).ToList();
                    if (UserSession.Current.User.Username == "trongvv.rd")
                    {
                        projectList = projectList.Where(t => t.ProjectManagerId == 87).ToList();
                    }
                }
                else if (ddlShow.SelectedValue == "ShowMyUncomplete")
                {
                    projectList = projectList.Where(t => (t.Complete == null || t.Complete < 100) && !t.IsStop.GetValueOrDefault()
                                                && t.ProjectManagerId == UserSession.Current.User.Id).ToList();
                }
                else if (ddlShow.SelectedValue == "ShowOverDue")
                {
                    projectList = projectList.Where(t => t.IsLater && !t.IsStop.GetValueOrDefault()).ToList();
                }
                else if (ddlShow.SelectedValue == "ShowMyOverDue")
                {
                    projectList = projectList.Where(t => t.IsLater && !t.IsStop.GetValueOrDefault()
                                                && t.ProjectManagerId == UserSession.Current.User.Id).ToList();
                }
                else if (ddlShow.SelectedValue == "ShowStop")
                {
                    projectList = projectList.Where(t => t.IsStop.GetValueOrDefault()).ToList();
                }
                else if (ddlShow.SelectedValue == "ShowMyStop")
                {
                    projectList = projectList.Where(t => t.IsStop.GetValueOrDefault()
                                                && t.ProjectManagerId == UserSession.Current.User.Id).ToList();
                }
                else
                {
                    projectList = projectList.ToList();
                }

                RadToolBarItem itemToolbar = this.CustomerMenu.FindItemByValue("toolbarWork");
                var rcbWorkList = (RadComboBox)itemToolbar.FindControl("rcbWorkList");
                var rtvWorkList = (RadTreeView)rcbWorkList.Items[0].FindControl("rtvWorkList");
                if (!string.IsNullOrEmpty(rtvWorkList.SelectedValue))
                {
                    projectList = projectList.Where(t => t.WorkId == Convert.ToInt32(rtvWorkList.SelectedValue)).ToList();
                }
            }
            else
            {
                List<int> projectIDList = new List<int>();
                var projectShareList = this.projectShareService.GetByGroup(UserSession.Current.RoleId);
                foreach (var psItem in projectShareList)
                {
                    var temp = this.projectAppendixService.GetByWork(psItem.WorkID.GetValueOrDefault());
                    projectIDList.AddRange(temp.Select(t => t.ProjectID.GetValueOrDefault()).ToList());
                }
                projectIDList = projectIDList.Distinct().ToList();
                projectList = this.scopeProjectService.GetById(projectIDList);
                if (ddlShow.SelectedValue == "ShowUncomplete")
                {
                    projectList = projectList.Where(t => t.Complete == null || t.Complete < 100).OrderByDescending(t => t.ID).ToList();
                }
                else if (ddlShow.SelectedValue == "ShowMyUncomplete")
                {
                    projectList = projectList.Where(t => (t.Complete == null || t.Complete < 100)
                                                && t.ProjectManagerId == UserSession.Current.User.Id)
                                            .OrderByDescending(t => t.ID).ToList();
                }
                else if (ddlShow.SelectedValue == "ShowOverDue")
                {
                    projectList = projectList.Where(t => t.IsLater).OrderByDescending(t => t.ID).ToList();
                }
                else if (ddlShow.SelectedValue == "ShowMyOverDue")
                {
                    projectList = projectList.Where(t => t.IsLater
                                                && t.ProjectManagerId == UserSession.Current.User.Id)
                                            .OrderByDescending(t => t.ID).ToList();
                }
                else if (ddlShow.SelectedValue == "ShowStop")
                {
                    projectList = projectList.Where(t => t.IsStop.GetValueOrDefault()).ToList();
                }
                else if (ddlShow.SelectedValue == "ShowMyStop")
                {
                    projectList = projectList.Where(t => t.IsStop.GetValueOrDefault()
                                                && t.ProjectManagerId == UserSession.Current.User.Id).ToList();
                }
                else
                {
                    projectList = projectList.ToList();
                }
            }
            this.grdDocument.DataSource = projectList;
            if (isbind)
            {
                this.grdDocument.DataBind();
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "PrintProgressReport")
            {
                var projectObj = this.scopeProjectService.GetById(!string.IsNullOrEmpty(this.lblProjectId.Value) ? Convert.ToInt32(this.lblProjectId.Value) : 0);
                if (projectObj != null)
                {
                    var listWorkgroupInPermission = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);

                    var filePath = Server.MapPath("~/Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\ProgressTemplate.xls");

                    var wsProgress = workbook.Worksheets[0];

                    wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR " + projectObj.Description);
                    wsProgress.Cells["C2"].PutValue("PROJECT PROGRESS (CUT OFF: " + DateTime.Now.ToString("dd/MM/yyyy") + ")");

                    var startDate = projectObj.StartDate.GetValueOrDefault();
                    while (startDate.DayOfWeek != DayOfWeek.Monday)
                    {
                        startDate = startDate.AddDays(1);
                    }

                    var currentMonth = 0;
                    var countMerge = 0;
                    var count = 3;
                    var countNumberCurrentActual = 0;

                    var wpIDList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID).Select(t => t.ID).ToList();
                    var milestoneList = this.milestoneService.GetAllByWorkpackage(wpIDList);
                    var minDate = milestoneList.Min(t => t.MilestoneDate).GetValueOrDefault();
                    var maxDate = milestoneList.Max(t => t.MilestoneDate).GetValueOrDefault();
                    var differenceMonth = ((maxDate.Year - minDate.Year) * 12) + maxDate.Month - minDate.Month;
                    for (int i = 0; i < differenceMonth; i++)
                    {
                        wsProgress.Cells[2, 3 + i].PutValue(minDate.AddMonths(i).ToString("MM/yyyy"));
                    }

                    if (listWorkgroupInPermission.Count > 0)
                    {
                        var wgCount = listWorkgroupInPermission.Count;
                        wsProgress.Cells.InsertRows(5, wgCount * 3);
                        for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                        {
                            var workgroup = listWorkgroupInPermission[i];

                            wsProgress.Cells["B" + (5 + (i * 3))].PutValue(workgroup.Name);
                            wsProgress.Cells["C" + (5 + (i * 3))].PutValue("Planed (%)");
                            wsProgress.Cells["C" + (5 + (i * 3) + 1)].PutValue("Actual (%)");
                            wsProgress.Cells["C" + (5 + (i * 3) + 2)].PutValue("+/-");

                            wsProgress.Cells.Merge(4 + (i * 3), 1, 3, 1);

                            for (int j = 0; j < differenceMonth; j++)
                            {
                                var milstoneOfMonth = milestoneList
                           .Where(t => t.MilestoneDate.GetValueOrDefault().Month == minDate.AddMonths(j).Month
                                       && t.MilestoneDate.GetValueOrDefault().Year == minDate.AddMonths(j).Year
                                       && t.WorkpackageId == workgroup.ID).ToList();

                                var progressPlaned = ((milstoneOfMonth.Average(t => t.PlanTotal).GetValueOrDefault() *
                                                       workgroup.Weight.GetValueOrDefault()) / 100) / 100;

                                var progressActual = ((milstoneOfMonth.Average(t => t.RealTotal).GetValueOrDefault() *
                                                       workgroup.Weight.GetValueOrDefault()) / 100) / 100;

                                wsProgress.Cells[4 + (i * 3), j + 3].PutValue(progressPlaned);
                                wsProgress.Cells[5 + (i * 3), j + 3].PutValue(progressActual);
                                wsProgress.Cells[6 + (i * 3), j + 3].PutValue(progressActual - progressPlaned);
                            }
                        }

                        wsProgress.Cells.Merge(7 + (wgCount * 3), 1, 3, 1);

                        wsProgress.Cells["B" + (5 + (wgCount * 3))].PutValue("Overall Project");
                        wsProgress.Cells["C" + (5 + (wgCount * 3))].PutValue("Planed (%)");
                        wsProgress.Cells["C" + (5 + (wgCount * 3) + 1)].PutValue("Actual (%)");
                        wsProgress.Cells["C" + (5 + (wgCount * 3) + 2)].PutValue("+/-");

                        for (int i = 0; i < differenceMonth; i++)
                        {
                            var totalPlan = 0.0;
                            var totalActual = 0.0;
                            var milstoneOfMonth = milestoneList
                                    .Where(t => t.MilestoneDate.GetValueOrDefault().Month == minDate.AddMonths(i).Month
                                                && t.MilestoneDate.GetValueOrDefault().Year == minDate.AddMonths(i).Year).ToList();

                            foreach (var milestoneOfWP in milstoneOfMonth.GroupBy(t => t.WorkpackageId))
                            {
                                var wpObj = this.workGroupService.GetById(milestoneOfWP.Key.GetValueOrDefault());
                                if (wpObj != null)
                                {
                                    totalPlan += (milestoneOfWP.ToList().Average(t => t.PlanTotal).GetValueOrDefault() *
                                                  wpObj.Weight.GetValueOrDefault()) / 100 / 100;

                                    totalActual += (milestoneOfWP.ToList().Average(t => t.RealTotal).GetValueOrDefault() *
                                         wpObj.Weight.GetValueOrDefault()) / 100 / 100;
                                }
                            }

                            wsProgress.Cells[4 + (wgCount * 3), i + 3].PutValue(totalPlan);
                            wsProgress.Cells[5 + (wgCount * 3), i + 3].PutValue(totalActual);
                            wsProgress.Cells[6 + (wgCount * 3), i + 3].PutValue(totalActual - totalPlan);
                        }

                        wsProgress.Cells["A1"].PutValue(0);
                        wsProgress.Cells["A2"].PutValue(wgCount);
                        wsProgress.Cells["A3"].PutValue(differenceMonth);

                        wsProgress.Cells.Merge(0, 2, 1, count - 2);
                        wsProgress.Cells.Merge(1, 2, 1, count - 2);

                    }

                    // Save and export file
                    var filename = projectObj.Name + "_Progress report - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument == "ExportMasterList")
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\ProjectMasterListTemplate.xls");
                var sheets = workbook.Worksheets;
                var readMeSheet = sheets[0];
                var dataSheet = sheets[1];

                var fieldList = this.fieldService.GetAll().OrderBy(t => t.Name).ToList();
                var platformList = this.platformService.GetAll().OrderBy(t => t.Name).ToList();
                var supervisorList = this.supervisorService.GetAll().OrderBy(t => t.Name).ToList();
                var projectManager = this.userService.GetAll(UserSession.Current.User.Id == 1).Where(t => t.IsGip.GetValueOrDefault() || t.IsChief.GetValueOrDefault()).OrderBy(t => t.UserNameWithFullName).ToList();

                for (var i = 0; i < fieldList.Count; i++)
                {
                    readMeSheet.Cells["B" + (7 + i)].PutValue(fieldList[i].Name);
                    readMeSheet.Cells["C" + (7 + i)].PutValue(fieldList[i].Description);
                }

                for (var i = 0; i < platformList.Count; i++)
                {
                    readMeSheet.Cells["E" + (7 + i)].PutValue(platformList[i].Name);
                    readMeSheet.Cells["F" + (7 + i)].PutValue(platformList[i].Description);
                }

                for (var i = 0; i < projectManager.Count; i++)
                {
                    readMeSheet.Cells["K" + (7 + i)].PutValue(projectManager[i].Username);
                }

                for (var i = 0; i < supervisorList.Count; i++)
                {
                    readMeSheet.Cells["M" + (7 + i)].PutValue(supervisorList[i].Name);
                }

                var rangeFieldList = readMeSheet.Cells.CreateRange("B7", "B" + (7 + (fieldList.Count == 0 ? 1 : fieldList.Count)));
                rangeFieldList.Name = "FieldList";

                var rangePlatformList = readMeSheet.Cells.CreateRange("E7", "E" + (7 + (platformList.Count == 0 ? 1 : platformList.Count)));
                rangePlatformList.Name = "PlatformList";

                var rangeProjectTypeList = readMeSheet.Cells.CreateRange("H7", "H8");
                rangeProjectTypeList.Name = "ProjectTypeList";

                var rangeProjectManagerList = readMeSheet.Cells.CreateRange("K7", "K" + (7 + (projectManager.Count == 0 ? 1 : projectManager.Count)));
                rangeProjectManagerList.Name = "ProjectManagerList";

                var rangeSupervisorList = readMeSheet.Cells.CreateRange("M7", "M" + (7 + (supervisorList.Count == 0 ? 1 : supervisorList.Count)));
                rangeSupervisorList.Name = "SupervisorList";

                var validations = dataSheet.Validations;
                this.CreateValidation(rangeFieldList.Name, validations, 2, 1000, 1, 1);
                this.CreateValidation(rangePlatformList.Name, validations, 2, 1000, 2, 2);
                this.CreateValidation(rangeProjectTypeList.Name, validations, 2, 1000, 6, 6);
                this.CreateValidation(rangeProjectManagerList.Name, validations, 2, 1000, 8, 8);
                this.CreateValidation(rangeSupervisorList.Name, validations, 2, 1000, 9, 9);

                readMeSheet.AutoFitRows();

                var filename = "ProjectInfoMasterList_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, filename, true);
            }
            else if (e.Argument.Contains("ExportWorkpackage"))
            {
                var selectedProject = this.scopeProjectService.GetById(!string.IsNullOrEmpty(this.lblProjectId.Value) ? Convert.ToInt32(this.lblProjectId.Value) : 0);
                if (selectedProject != null)
                {
                    var projectId = selectedProject.ID;
                    var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip;
                    var currentDepartment = UserSession.Current.User.RoleId;
                    var workPackageList = isSeeFull
                        ? this.workGroupService.GetAll().Where(t => t.ProjectId == projectId)
                        : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()).Where(t => t.ProjectId == projectId);

                    if (UserSession.Current.IsEngineer)
                    {
                        var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                        workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
                    }

                    var data = (from t in workPackageList
                                select new
                                {
                                    t.ID,
                                    t.Name,
                                    t.Description,
                                    t.RevisionName,
                                    t.DepartmentName,
                                    t.AreaName,
                                    t.DisciplineName,
                                    t.TypeName,
                                    Weight = t.Weight,
                                    Complete = t.Complete,
                                    StartDate = t.StartDate,
                                    DeadLine = t.Deadline,
                                    EndDate = t.EndDate
                                }).OrderBy(t => t.ID).ToList();
                    var filePath = Server.MapPath("../../Exports" + @"\");
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template/WorkpackageList.xls", FileFormatType.Excel2003);

                    var worksheet = workbook.Worksheets[0];
                    var readMeSheet = workbook.Worksheets[1];
                    var HiddeSheet = workbook.Worksheets[2];


                    var wpRevisionList = this.wpRevisionService.GetAll();
                    var departmentList = this.roleService.GetAll(UserSession.Current.User.Id == 1);
                    var areaList = this.areaService.GetAll();
                    var disciplineList = this.disciplineService.GetAll();


                    for (var i = 0; i < departmentList.Count; i++)
                    {
                        readMeSheet.Cells["B" + (7 + i)].PutValue(departmentList[i].Name);
                        readMeSheet.Cells["C" + (7 + i)].PutValue(departmentList[i].Description);
                    }

                    for (var i = 0; i < areaList.Count; i++)
                    {
                        readMeSheet.Cells["E" + (7 + i)].PutValue(areaList[i].Name);
                        readMeSheet.Cells["F" + (7 + i)].PutValue(areaList[i].Description);
                    }

                    for (var i = 0; i < disciplineList.Count; i++)
                    {
                        readMeSheet.Cells["H" + (7 + i)].PutValue(disciplineList[i].Name);
                        readMeSheet.Cells["I" + (7 + i)].PutValue(disciplineList[i].Description);
                    }

                    for (var i = 0; i < wpRevisionList.Count; i++)
                    {
                        readMeSheet.Cells["N" + (7 + i)].PutValue(wpRevisionList[i].Name);
                        readMeSheet.Cells["O" + (7 + i)].PutValue(wpRevisionList[i].Description);
                    }


                    var rangeDepartmentList = readMeSheet.Cells.CreateRange("B7", "B" + (7 + (departmentList.Count == 0 ? 1 : departmentList.Count)));
                    rangeDepartmentList.Name = "DepartmentList";

                    var rangeAreaList = readMeSheet.Cells.CreateRange("E7", "E" + (7 + (areaList.Count == 0 ? 1 : areaList.Count)));
                    rangeAreaList.Name = "AreaList";

                    var rangeDisciplineList = readMeSheet.Cells.CreateRange("H7", "H" + (7 + (disciplineList.Count == 0 ? 1 : disciplineList.Count)));
                    rangeDisciplineList.Name = "DisciplineList";

                    var rangeTypeList = readMeSheet.Cells.CreateRange("K7", "K8");
                    rangeTypeList.Name = "TypeList";

                    var rangeWPRevisionList = readMeSheet.Cells.CreateRange("N7", "N" + (7 + (wpRevisionList.Count == 0 ? 1 : wpRevisionList.Count)));
                    rangeWPRevisionList.Name = "WPRevision";

                    var validations = worksheet.Validations;

                    this.CreateValidation(rangeDepartmentList.Name, validations, 2, 1000, 4, 4);
                    this.CreateValidation(rangeAreaList.Name, validations, 2, 1000, 5, 5);
                    this.CreateValidation(rangeDisciplineList.Name, validations, 2, 1000, 6, 6);
                    this.CreateValidation(rangeTypeList.Name, validations, 2, 1000, 7, 7);
                    this.CreateValidation(rangeWPRevisionList.Name, validations, 2, 1000, 3, 3);

                    worksheet.Cells["B1"].PutValue("Workpackage list of " + selectedProject.Name + "(" + selectedProject.Description + ")");
                    worksheet.Cells["A1"].PutValue(selectedProject.ID);
                    readMeSheet.AutoFitRows();


                    DataTable drRecoder = this.ToDataTables(data);

                    worksheet.Cells.ImportDataTable(drRecoder, false, 2, 0, drRecoder.Rows.Count, drRecoder.Columns.Count);
                    HiddeSheet.Cells.ImportDataTable(drRecoder, false, 2, 0, drRecoder.Rows.Count, drRecoder.Columns.Count);

                    HiddeSheet.IsVisible = false;
                    worksheet.Cells.HideColumn((byte)13);
                    worksheet.Cells.HideColumn((byte)14);
                    worksheet.Cells.HideColumn((byte)15);
                    var filename = selectedProject.Name.Trim() + "_WorkpackageInfoMasterList_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    workbook.Save(filePath + filename);
                    DownloadByWriteByte(filePath + filename, filename, true);

                }
            }
            else if (e.Argument == "ExportCMDR")
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                var docList = new List<DocumentPackage>();
                var projectName = string.Empty;
                var dtFull = new DataTable();
                var projectID = Convert.ToInt32(this.lblProjectId.Value);
                dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("RevName", typeof(String)),
                        new DataColumn("Start", typeof(String)),
                        new DataColumn("IDCDeadline", typeof(String)),
                        new DataColumn("IFRDeadline", typeof(String)),
                        new DataColumn("DeadLine", typeof(String)),
                        new DataColumn("End", typeof(String)),
                        new DataColumn("Complete", typeof(Double)),
                        new DataColumn("Weight",typeof(Double)),
                        new DataColumn("DocumentTypeName", typeof(String)),
                        new DataColumn("ManhourPlan", typeof(String)),
                        new DataColumn("ManhourActual", typeof(String)),
                        new DataColumn("Department", typeof(String)),
                        new DataColumn("Engineer", typeof(String)),
                        new DataColumn("Leader", typeof(String)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("OutgoingXDCBNo", typeof(String)),
                        new DataColumn("OutgoingXDCBDate", typeof(String)),
                        new DataColumn("CopyNo", typeof(String)),
                        new DataColumn("IncomingNo", typeof(String)),
                        new DataColumn("IncomingDate", typeof(String)),
                        new DataColumn("ICANo", typeof(String)),
                        new DataColumn("ICADate", typeof(String)),
                        new DataColumn("ICAReviewCode", typeof(String)),
                        new DataColumn("Markup",typeof(String)),
                        new DataColumn("Notes", typeof(String)),
                    });
                var selectedProject = this.scopeProjectService.GetById(!string.IsNullOrEmpty(this.lblProjectId.Value) ? Convert.ToInt32(this.lblProjectId.Value) : 0);
                if (selectedProject != null)
                {
                    projectName = selectedProject.Name;
                    var projectId = selectedProject.ID;
                    var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer || UserSession.Current.IsEngineer;
                    var currentDepartment = UserSession.Current.User.RoleId;
                    var workPackageList = isSeeFull
                        ? this.workGroupService.GetAll().Where(t => t.ProjectId == projectId).ToList()
                        : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()).Where(t => t.ProjectId == projectId).ToList();
                    //chinh sua cho eng xem tat ca du an
                    //if (UserSession.Current.IsEngineer)
                    //{
                    //    var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct().ToList();
                    //    workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
                    //}
                    if (workPackageList.Count > 0)
                    {
                        workbook.Open(Server.MapPath("~/" + @"Exports\Template\CMDRReport.xls"));
                        var sheets = workbook.Worksheets;
                        var wsSummary = sheets[0];
                        wsSummary.Cells.InsertRows(7, workPackageList.Count - 1);
                        for (int i = 0; i < workPackageList.Count; i++)
                        {
                            docList = this.documentPackageService.GetAllByWorkgroup(workPackageList[i].ID)
                                  .Where(t => t.IsEMDR)
                                  .OrderBy(t => t.DocNo)
                                  .ToList();

                            var totalDoc = 0;
                            var totalDocIssues = 0;
                            var totalDocRev5Issues = 0;
                            var totalDocRevIssues = 0;
                            var totalDocDontIssues = 0;
                            dtFull.Rows.Clear();
                            sheets.AddCopy(1);

                            string st = workPackageList[i].Name.Replace("/", " ").Replace(@"\", " ");
                            if (st.Length > 30)
                            {
                                st = st.Substring(0, 25) + "_(" + i + ")";
                            }
                            sheets[i + 2].Name = st;
                            sheets[i + 2].Cells["V4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            sheets[i + 2].Cells["C7"].PutValue(workPackageList[i].Name);
                            sheets[i + 2].Cells["E1"].PutValue("DETAIL ENGINEERING SERVICE FOR PROJECT " + projectName);

                            // add hyperlink
                            var linkName = workPackageList[i].Name;
                            wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                            wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1, "'" + st + "'!D7");
                            if (workPackageList[i].DisciplineId != null)
                            {
                                var disciplineObj = this.disciplineService.GetById(workPackageList[i].DisciplineId.GetValueOrDefault());
                                wsSummary.Cells["C" + (7 + i)].PutValue(disciplineObj != null ? disciplineObj.Description : string.Empty);
                            }
                            wsSummary.Cells["C1"].PutValue("DETAIL  ENGINEERING SERVICE FOR " + projectName + " PROJECT");
                            wsSummary.Cells["C3"].PutValue(projectName + " PROJECT - SUMMARY REPORT");
                            wsSummary.Cells["P4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            wsSummary.Cells["P3"].PutValue(workPackageList.Count.ToString());

                            var docListHasAttachFile = docList.Where(t => t.HasAttachFile.GetValueOrDefault() && t.Complete == 100);
                            var wgDoc = docList.Count;
                            var wgDocIssues = docListHasAttachFile.Count();
                            var wgDocHardCopy = docListHasAttachFile.Count(t => !string.IsNullOrEmpty(t.OutgoingTransNo));
                            var wgTotalDocRev = wgDocIssues;
                            var wgDocDontIssues = wgDoc - wgDocIssues;
                            var wgDocOverdue = docList.Where(t => !t.HasAttachFile.GetValueOrDefault() && t.Complete != 100 && t.Deadline.GetValueOrDefault() < DateTime.Now).ToList();
                            var total = docListHasAttachFile.Count(t => !string.IsNullOrEmpty(t.OutgoingLeterNo));
                            totalDoc += wgDoc;
                            totalDocIssues += wgDocIssues;
                            totalDocRev5Issues += wgDocHardCopy;
                            totalDocRevIssues += total;
                            totalDocDontIssues = totalDoc - totalDocIssues;
                            wsSummary.Cells["D" + (7 + i)].PutValue(wgDoc);
                            wsSummary.Cells["E" + (7 + i)].PutValue(wgDocDontIssues);
                            wsSummary.Cells["K" + (7 + i)].PutValue(wgTotalDocRev);
                            wsSummary.Cells["L" + (7 + i)].PutValue(wgDocOverdue.Count);
                            wsSummary.Cells["M" + (7 + i)].PutValue(totalDocRev5Issues);
                            wsSummary.Cells["N" + (7 + i)].PutValue(totalDocRevIssues);

                            var count = 1;

                            var listDocumentTypeId =
                                docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                            double complete = 0;
                            double weight = 0;

                            foreach (var documentTypeId in listDocumentTypeId)
                            {
                                var documentType = this.documentTypeService.GetById((int)documentTypeId);

                                var dataRow = dtFull.NewRow();
                                dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                dtFull.Rows.Add(dataRow);

                                var listDocByDocType =
                                    docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                foreach (var document in listDocByDocType)
                                {
                                    dataRow = dtFull.NewRow();
                                    dataRow["DocId"] = document.ID;
                                    dataRow["NoIndex"] = count;
                                    dataRow["DocNo"] = document.DocNo;
                                    dataRow["DocTitle"] = document.DocTitle;
                                    dataRow["RevName"] = document.RevisionName;
                                    dataRow["Start"] = document.StartDate != null
                                      ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;
                                    dataRow["IDCDeadline"] = document.IDCDeadline != null
                                ? document.IDCDeadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                                    dataRow["IFRDeadline"] = document.IFRDeadline != null
                                        ? document.IFRDeadline.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["DeadLine"] = document.Deadline != null
                                          ? document.Deadline.Value.ToString("dd/MM/yyyy")
                                          : string.Empty;
                                    dataRow["End"] = document.EndDate != null
                                      ? document.EndDate.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;
                                    dataRow["Complete"] = document.Complete != null ? document.Complete / 100 : 0;
                                    dataRow["Weight"] = document.Weight != null ? document.Weight / 100 : 0;
                                    dataRow["DocumentTypeName"] = document.DocumentTypeName;
                                    dataRow["ManhourPlan"] = document.ManHourPlan != null ? document.ManHourPlan.GetValueOrDefault() : 0;
                                    dataRow["ManhourActual"] = document.ManHours != null ? document.ManHours.GetValueOrDefault() : 0;
                                    dataRow["Department"] = document.DeparmentName;
                                    dataRow["Engineer"] = document.EngineerName;
                                    dataRow["Leader"] = document.LeaderName;
                                    dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                    dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                        ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["OutgoingXDCBNo"] = document.OutgoingLeterNo;
                                    dataRow["OutgoingXDCBDate"] = document.OutgoingLeterDate != null
                                        ? document.OutgoingLeterDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["CopyNo"] = document.NumberHardCopy;
                                    dataRow["IncomingNo"] = document.IncomingTransNo;
                                    dataRow["IncomingDate"] = document.IncomingTransDate != null
                                        ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["ICANo"] = document.ICAReviewOutTransNo;
                                    dataRow["ICADate"] = document.ICAReviewOutDate != null
                                        ? document.ICAReviewOutDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["ICAReviewCode"] = document.ICAReviewCode;
                                    dataRow["Markup"] = document.Markup;
                                    dataRow["Notes"] = document.Notes;
                                    count += 1;
                                    dtFull.Rows.Add(dataRow);

                                    complete += (document.Complete != null && document.Weight != null) ? (double)((document.Complete / 100) * (document.Weight / 100)) : 0;
                                    weight += document.Weight != null ? (double)document.Weight / 100 : 0;
                                }
                            }

                            sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                            sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                            sheets[i + 2].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");
                            sheets[i + 2].Cells[7 + dtFull.Rows.Count, 11].PutValue(complete);
                            sheets[i + 2].Cells[7 + dtFull.Rows.Count, 12].PutValue(weight);
                            if (selectedProject.IsIDC.GetValueOrDefault() == true)
                            {
                                sheets[i + 2].Cells.Columns[7].IsHidden = false;
                                sheets[i + 2].Cells.Columns[8].IsHidden = false;
                            }
                            else
                            {
                                sheets[i + 2].Cells.Columns[7].IsHidden = true;
                                sheets[i + 2].Cells.Columns[8].IsHidden = true;
                            }
                        }
                        sheets[1].IsVisible = false;

                        var filename = projectName.Replace("/", " ").Replace(@"\", " ") + " - " + "EMDR Report " +
                                       DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                        filename = filename.Replace("/", " ").Replace(@"\", " ");
                        workbook.Save(filePath + filename);
                        this.DownloadByWriteByte(filePath + filename, filename, true);
                    }

                }
            }
            else if (e.Argument == "ExportMergeCMDR")
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                var docList = new List<DocumentPackage>();
                var projectName = string.Empty;
                var dtFull = new DataTable();
                var projectID = Convert.ToInt32(this.lblProjectId.Value);
                dtFull.Columns.AddRange(new[]
                    {
                        //new DataColumn("Flag", typeof(String)),
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("RevName", typeof(String)),
                        //new DataColumn("Start", typeof(String)),
                        //new DataColumn("DeadLine", typeof(String)),
                        new DataColumn("End", typeof(String)),
                        //new DataColumn("Complete", typeof(Double)),
                        //new DataColumn("Weight",typeof(Double)),
                        //new DataColumn("DocumentTypeName", typeof(String)),
                        //new DataColumn("ManhourPlan", typeof(String)),
                        //new DataColumn("ManhourActual", typeof(String)),
                        //new DataColumn("Department", typeof(String)),
                        //new DataColumn("Engineer", typeof(String)),
                        //new DataColumn("PersonsInvolved", typeof(String)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("OutgoingXDCBNo", typeof(String)),
                        new DataColumn("OutgoingXDCBDate", typeof(String)),
                        //new DataColumn("IncomingNo", typeof(String)),
                        //new DataColumn("IncomingDate", typeof(String)),
                        //new DataColumn("ICANo", typeof(String)),
                        //new DataColumn("ICADate", typeof(String)),
                        //new DataColumn("ICAReviewCode", typeof(String)),
                        //new DataColumn("Markup",typeof(String)),
                        //new DataColumn("hardcopy", typeof(String)),
                        new DataColumn("Notes", typeof(String)),
                        //new DataColumn("LastestRevision", typeof(String)),
                    });
                var selectedProject = this.scopeProjectService.GetById(!string.IsNullOrEmpty(this.lblProjectId.Value) ? Convert.ToInt32(this.lblProjectId.Value) : 0);
                if (selectedProject != null)
                {
                    projectName = selectedProject.Name;
                    var projectId = selectedProject.ID;
                    var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer || UserSession.Current.IsEngineer;
                    var currentDepartment = UserSession.Current.User.RoleId;
                    var workPackageList = isSeeFull
                        ? this.workGroupService.GetAll().Where(t => t.ProjectId == projectId).ToList()
                        : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()).Where(t => t.ProjectId == projectId).ToList();
                    //chinh sua cho eng xem tat ca du an
                    //if (UserSession.Current.IsEngineer)
                    //{
                    //    var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct().ToList();
                    //    workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
                    //}


                    if (workPackageList.Count > 0)
                    {
                        workbook.Open(filePath + @"Template\CMDRReportTH.xls");
                        var sheets = workbook.Worksheets;
                        var wsSummary = sheets[0];
                        wsSummary.Cells.InsertRows(7, workPackageList.Count - 1);
                        int maxdatarow = 0;
                        for (int i = 0; i < workPackageList.Count; i++)
                        {
                            docList = this.documentPackageService.GetAllByWorkgroup(workPackageList[i].ID)
                                  .Where(t => t.IsEMDR)
                                  .OrderBy(t => t.DocumentTypeId)
                                  .ToList();

                            var totalDoc = 0;
                            var totalDocIssues = 0;
                            //var totalDocRev0Issues = 0;
                            //var totalDocRev1Issues = 0;
                            //var totalDocRev2Issues = 0;
                            //var totalDocRev3Issues = 0;
                            //var totalDocRev4Issues = 0;
                            var totalDocRev5Issues = 0;
                            var totalDocRevIssues = 0;

                            var totalDocDontIssues = 0;
                            dtFull.Rows.Clear();
                            sheets.AddCopy(1);

                            string st = workPackageList[i].Name.Replace("/", " ").Replace(@"\", " ");
                            if (st.Length > 30)
                            {
                                st = st.Substring(0, 25) + "_(" + i + ")";
                            }
                            //sheets[i + 2].Name = st;
                            //sheets[i + 2].Cells["AD4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            maxdatarow = sheets[2].Cells.MaxRow;
                            sheets[2].Cells["C" + (maxdatarow + 2)].PutValue(workPackageList[i].Name);
                            //sheets[i + 2].Cells["E1"].PutValue("DETAIL ENGINEERING SERVICE FOR PROJECT " + projectName);
                            // add hyperlink
                            var linkName = workPackageList[i].Name;
                            wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                            //wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1, "'" + st + "'!D7");
                            if (workPackageList[i].DisciplineId != null)
                            {
                                var disciplineObj = this.disciplineService.GetById(workPackageList[i].DisciplineId.GetValueOrDefault());
                                wsSummary.Cells["C" + (7 + i)].PutValue(disciplineObj != null ? disciplineObj.Description : string.Empty);
                            }
                            wsSummary.Cells["C1"].PutValue("DETAIL  ENGINEERING SERVICE FOR " + projectName + " PROJECT");
                            wsSummary.Cells["C3"].PutValue(projectName + " PROJECT - SUMMARY REPORT");
                            wsSummary.Cells["M4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            wsSummary.Cells["A5"].PutValue(workPackageList.Count.ToString());

                            var docListHasAttachFile = docList.Where(t => t.HasAttachFile.GetValueOrDefault());
                            var wgDoc = docList.Count;
                            var wgDocIssues = docListHasAttachFile.Count();
                            //var wgDocRev0Issues = docListHasAttachFile.Count(t => t.RevisionName == "0");
                            //var wgDocRev1Issues = docListHasAttachFile.Count(t => t.RevisionName == "1");
                            //var wgDocRev2Issues = docListHasAttachFile.Count(t => t.RevisionName == "2");
                            //var wgDocRev3Issues = docListHasAttachFile.Count(t => t.RevisionName == "3");
                            //var wgDocRev4Issues = docListHasAttachFile.Count(t => t.RevisionName == "4");
                            var wgDocHardCopy = docListHasAttachFile.Count(t => !string.IsNullOrEmpty(t.OutgoingTransNo));
                            var wgTotalDocRev = wgDocIssues;
                            var wgDocDontIssues = wgDoc - wgDocIssues;

                            var total = docListHasAttachFile.Count(t => !string.IsNullOrEmpty(t.OutgoingLeterNo));

                            totalDoc += wgDoc;
                            totalDocIssues += wgDocIssues;
                            //totalDocRev0Issues += wgDocRev0Issues;
                            //totalDocRev1Issues += wgDocRev1Issues;
                            //totalDocRev2Issues += wgDocRev2Issues;
                            //totalDocRev3Issues += wgDocRev3Issues;
                            //totalDocRev4Issues += wgDocRev4Issues;
                            totalDocRev5Issues += wgDocHardCopy;
                            totalDocRevIssues += total;
                            totalDocDontIssues = totalDoc - totalDocIssues;

                            wsSummary.Cells["D" + (7 + i)].PutValue(wgDoc);
                            wsSummary.Cells["E" + (7 + i)].PutValue(wgDocDontIssues);
                            wsSummary.Cells["K" + (7 + i)].PutValue(wgTotalDocRev);
                            wsSummary.Cells["L" + (7 + i)].PutValue(totalDocRev5Issues);
                            wsSummary.Cells["M" + (7 + i)].PutValue(totalDocRevIssues);

                            var count = 1;

                            var listDocumentTypeId =
                                docList.Select(t => t.DocumentTypeId).Distinct().ToList();

                            //double complete = 0;
                            //double weight = 0;

                            foreach (var documentTypeId in listDocumentTypeId)
                            {
                                var documentType = this.documentTypeService.GetById((int)documentTypeId);

                                var dataRow = dtFull.NewRow();
                                dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                dtFull.Rows.Add(dataRow);

                                var listDocByDocType =
                                    docList.Where(t => t.DocumentTypeId == documentTypeId).OrderBy(t => t.DocNo).ToList();
                                foreach (var document in listDocByDocType)
                                {
                                    dataRow = dtFull.NewRow();
                                    dataRow["DocId"] = document.ID;
                                    dataRow["NoIndex"] = count;
                                    dataRow["DocNo"] = document.DocNo;
                                    dataRow["DocTitle"] = document.DocTitle;

                                    dataRow["RevName"] = document.RevisionName;
                                    //dataRow["Start"] = document.StartDate != null
                                    //  ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                    //  : string.Empty;
                                    //dataRow["DeadLine"] = document.Deadline != null
                                    //  ? document.Deadline.Value.ToString("dd/MM/yyyy")
                                    //  : string.Empty;
                                    dataRow["End"] = document.EndDate != null
                                      ? document.EndDate.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;
                                    //dataRow["Complete"] = document.Complete != null ? document.Complete / 100 : 0;
                                    //dataRow["Weight"] = document.Weight != null ? document.Weight / 100 : 0;

                                    //dataRow["DocumentTypeName"] = document.DocumentTypeName;
                                    //dataRow["ManhourPlan"] = document.ManHourPlan != null ? document.ManHourPlan.GetValueOrDefault() : 0;
                                    //dataRow["ManhourActual"] = document.ManHours != null ? document.ManHours.GetValueOrDefault() : 0;
                                    //dataRow["Department"] = document.DeparmentName;
                                    //dataRow["Engineer"] = document.EngineerName;
                                    //dataRow["PersonsInvolved"] = document.PersonsInvolved;
                                    dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                    dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                        ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["OutgoingXDCBNo"] = document.OutgoingLeterNo;
                                    dataRow["OutgoingXDCBDate"] = document.OutgoingLeterDate != null
                                        ? document.OutgoingLeterDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    //dataRow["IncomingNo"] = document.IncomingTransNo;
                                    //dataRow["IncomingDate"] = document.IncomingTransDate != null
                                    //    ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                    //    : string.Empty;
                                    //dataRow["ICANo"] = document.ICAReviewOutTransNo;
                                    //dataRow["ICADate"] = document.ICAReviewOutDate != null
                                    //    ? document.ICAReviewOutDate.Value.ToString("dd/MM/yyyy")
                                    //    : string.Empty;
                                    //dataRow["ICAReviewCode"] = document.ICAReviewCode;
                                    //dataRow["Markup"] = document.Markup;
                                    //dataRow["hardcopy"] = string.Empty;
                                    dataRow["Notes"] = document.Notes;
                                    // dataRow["LastestRevision"] = document.IsLeaf;
                                    count += 1;
                                    dtFull.Rows.Add(dataRow);
                                    //complete += (document.Complete != null && document.Weight != null) ? (double)((document.Complete / 100) * (document.Weight / 100)) : 0;
                                    //weight += document.Weight != null ? (double)document.Weight / 100 : 0;
                                }
                            }

                            maxdatarow = sheets[2].Cells.MaxRow;
                            sheets[2].Cells["A" + (maxdatarow + 1)].PutValue(dtFull.Rows.Count);
                            sheets[2].Cells.ImportDataTable(dtFull, false, (maxdatarow + 1), 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                            //maxdatarow = sheets[2].Cells.MaxRow;
                            //sheets[2].Cells[(maxdatarow + 1) + dtFull.Rows.Count, 2].PutValue("Total");
                            //sheets[2].Cells[(maxdatarow + 1) + dtFull.Rows.Count, 9].PutValue(complete);
                            //sheets[2].Cells[(maxdatarow + 1) + dtFull.Rows.Count, 10].PutValue(weight);
                        }
                        sheets[1].IsVisible = false;
                        var filename = projectName.Replace("/", " ").Replace(@"\", " ") + " - " + "EMDR Report " +
                                       DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                        filename = filename.Replace("/", " ").Replace(@"\", " ");
                        workbook.Save(filePath + filename);
                        this.DownloadByWriteByte(filePath + filename, filename, true);
                    }
                }
            }

            else if (e.Argument.Contains("IssueNextWeek"))
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                var docList = new List<DocumentPackage>();
                var projectName = string.Empty;
                var dtFull = new DataTable();
                var projectID = Convert.ToInt32(this.lblProjectId.Value);
                dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("RevName", typeof(String)),
                        new DataColumn("Start", typeof(String)),
                        new DataColumn("IDCDeadline", typeof(String)),
                        new DataColumn("IFRDeadline", typeof(String)),
                        new DataColumn("DeadLine", typeof(String)),
                        new DataColumn("End", typeof(String)),
                        new DataColumn("Complete", typeof(Double)),
                        new DataColumn("Weight",typeof(Double)),
                        new DataColumn("DocumentTypeName", typeof(String)),
                        new DataColumn("ManhourPlan", typeof(String)),
                        new DataColumn("ManhourActual", typeof(String)),
                        new DataColumn("Department", typeof(String)),
                        new DataColumn("Engineer", typeof(String)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("OutgoingXDCBNo", typeof(String)),
                        new DataColumn("OutgoingXDCBDate", typeof(String)),
                        new DataColumn("IncomingNo", typeof(String)),
                        new DataColumn("IncomingDate", typeof(String)),
                        new DataColumn("ICANo", typeof(String)),
                        new DataColumn("ICADate", typeof(String)),
                        new DataColumn("ICAReviewCode", typeof(String)),
                        new DataColumn("Markup",typeof(String)),
                        new DataColumn("hardcopy", typeof(String)),
                        new DataColumn("Notes", typeof(String)),
                    });
                var selectedProject = this.scopeProjectService.GetById(!string.IsNullOrEmpty(this.lblProjectId.Value) ? Convert.ToInt32(this.lblProjectId.Value) : 0);
                if (selectedProject != null)
                {
                    projectName = selectedProject.Name;
                    var projectId = selectedProject.ID;
                    var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
                    var currentDepartment = UserSession.Current.User.RoleId;
                    var workPackageList = isSeeFull
                        ? this.workGroupService.GetAll().Where(t => t.ProjectId == projectId).ToList()
                        : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()).Where(t => t.ProjectId == projectId).ToList();

                    if (UserSession.Current.IsEngineer)
                    {
                        var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                        workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
                    }

                    var monday = this.Layngaythu2(DateTime.Today.Date);
                    DateTime fromdate = monday.AddDays(7);
                    DateTime todate = fromdate.AddDays(6);

                    if (workPackageList.Count > 0)
                    {
                        workbook.Open(Server.MapPath("~/" + @"Exports\Template\CMDRReport.xls"));

                        var sheets = workbook.Worksheets;
                        var wsSummary = sheets[0];
                        wsSummary.Cells.InsertRows(7, workPackageList.Count - 1);
                        for (int i = 0; i < workPackageList.Count; i++)
                        {
                            var linkName = workPackageList[i].Name;
                            wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                            wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1, "'" + workPackageList[i].Name.Replace("/", " ").Replace(@"\", " ") + "_" + workPackageList[i].RevisionName + "'" + "!D7");
                            if (workPackageList[i].DisciplineId != null)
                            {
                                var disciplineObj = this.disciplineService.GetById(workPackageList[i].DisciplineId.GetValueOrDefault());
                                wsSummary.Cells["C" + (7 + i)].PutValue(disciplineObj != null ? disciplineObj.Description : string.Empty);
                            }
                            wsSummary.Cells["C1"].PutValue("Document Will Issue from Date " + fromdate.ToString("dd/MM/yyyy") + " To Date "
                 + todate.ToString("dd/MM/yyyy") + " for Project " + projectName);
                            wsSummary.Cells["C3"].PutValue(projectName + " PROJECT - SUMMARY REPORT");
                            wsSummary.Cells["D5"].PutValue("Tổng số TL");
                            wsSummary.Cells.HideColumn(4);
                            wsSummary.Cells.HideColumn(10);
                            wsSummary.Cells.HideColumn(11);
                            wsSummary.Cells.HideColumn(12);

                            docList = this.documentPackageService.GetAllByWorkgroup(workPackageList[i].ID)
                   .Where(t => t.IsEMDR && t.Deadline != null && t.Deadline.Value.Date >= fromdate.Date && t.Deadline.Value.Date <= todate.Date && (t.Complete == null || t.Complete < 100))
                  .OrderBy(t => t.DocNo)
                  .ToList();

                            wsSummary.Cells["D" + (7 + i)].PutValue(docList.Count);
                            dtFull.Rows.Clear();
                            sheets.AddCopy(1);
                            string st = workPackageList[i].Name.Replace("/", " ").Replace(@"\", " ");
                            if (st.Length > 30)
                            {
                                st = st.Substring(0, 25) + "_(" + i + ")";
                            }
                            sheets[i + 2].Name = st;
                            sheets[i + 2].Cells["AC4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            sheets[i + 2].Cells["C7"].PutValue(workPackageList[i].Name);
                            sheets[i + 2].Cells["E1"].PutValue("Document Will Issue from Date " + fromdate.ToString("dd/MM/yyyy") + " To Date "
                 + todate.ToString("dd/MM/yyyy") + " for Workpackage " + workPackageList[i].Name);

                            var count = 1;

                            var listDocumentTypeId =
                                docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                            double complete = 0;
                            double weight = 0;
                            foreach (var documentTypeId in listDocumentTypeId)
                            {
                                var documentType = this.documentTypeService.GetById((int)documentTypeId);
                                var dataRow = dtFull.NewRow();
                                dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                dtFull.Rows.Add(dataRow);
                                var listDocByDocType =
                                    docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                foreach (var document in listDocByDocType)
                                {
                                    dataRow = dtFull.NewRow();
                                    dataRow["DocId"] = document.ID;
                                    dataRow["NoIndex"] = count;
                                    dataRow["DocNo"] = document.DocNo;
                                    dataRow["DocTitle"] = document.DocTitle;
                                    dataRow["RevName"] = document.RevisionName;
                                    dataRow["Start"] = document.StartDate != null
                                      ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;
                                    dataRow["IDCDeadline"] = document.IDCDeadline != null
                                ? document.IDCDeadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                                    dataRow["IFRDeadline"] = document.IFRDeadline != null
                                        ? document.IFRDeadline.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["DeadLine"] = document.Deadline != null
                                      ? document.Deadline.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;
                                    dataRow["End"] = document.EndDate != null
                                      ? document.EndDate.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;
                                    dataRow["Complete"] = document.Complete != null ? document.Complete / 100 : 0;
                                    dataRow["Weight"] = document.Weight != null ? document.Weight / 100 : 0;
                                    dataRow["DocumentTypeName"] = document.DocumentTypeName;
                                    dataRow["ManhourPlan"] = document.ManHourPlan != null ? document.ManHourPlan.GetValueOrDefault() : 0;
                                    dataRow["ManhourActual"] = document.ManHours != null ? document.ManHours.GetValueOrDefault() : 0;
                                    dataRow["Department"] = document.DeparmentName;
                                    dataRow["Engineer"] = document.EngineerName;
                                    dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                    dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                        ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["OutgoingXDCBNo"] = document.OutgoingLeterNo;
                                    dataRow["OutgoingXDCBDate"] = document.OutgoingLeterDate != null
                                        ? document.OutgoingLeterDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["IncomingNo"] = document.IncomingTransNo;
                                    dataRow["IncomingDate"] = document.IncomingTransDate != null
                                        ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["ICANo"] = document.ICAReviewOutTransNo;
                                    dataRow["ICADate"] = document.ICAReviewOutDate != null
                                        ? document.ICAReviewOutDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["ICAReviewCode"] = document.ICAReviewCode;
                                    dataRow["Markup"] = document.Markup;
                                    dataRow["hardcopy"] = string.Empty;
                                    dataRow["Notes"] = document.Notes;
                                    count += 1;
                                    dtFull.Rows.Add(dataRow);

                                    complete += (document.Complete != null && document.Weight != null) ? (double)((document.Complete / 100) * (document.Weight / 100)) : 0;
                                    weight += document.Weight != null ? (double)document.Weight / 100 : 0;
                                }
                            }

                            sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                            sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                            sheets[i + 2].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");
                            sheets[i + 2].Cells[7 + dtFull.Rows.Count, 11].PutValue(complete);
                            sheets[i + 2].Cells[7 + dtFull.Rows.Count, 12].PutValue(weight);
                            if (selectedProject.IsIDC.GetValueOrDefault() == true)
                            {
                                sheets[i + 2].Cells.Columns[7].IsHidden = false;
                                sheets[i + 2].Cells.Columns[8].IsHidden = false;
                            }
                            else
                            {
                                sheets[i + 2].Cells.Columns[7].IsHidden = true;
                                sheets[i + 2].Cells.Columns[8].IsHidden = true;
                            }
                        }
                        sheets[1].IsVisible = false;

                        var filename = projectName.Replace("/", " ").Replace(@"\", " ") + " - " + "EMDR Report Document Will Issue Next Week " +
                                       DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                        filename = filename.Replace("/", " ").Replace(@"\", " ");
                        workbook.Save(filePath + filename);
                        this.DownloadByWriteByte(filePath + filename, filename, true);
                    }

                }
            }
            else if (e.Argument == "ExportCMDR_C")
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                var docList = new List<DocumentPackage>();
                var projectName = string.Empty;
                var dtFull = new DataTable();
                var projectID = Convert.ToInt32(this.lblProjectId.Value);
                dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("Flag", typeof(String)),
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("RevName", typeof(String)),
                        //new DataColumn("Start", typeof(String)),
                        //new DataColumn("DeadLine", typeof(String)),
                        //new DataColumn("End", typeof(String)),
                        //new DataColumn("Complete", typeof(Double)),
                     //   new DataColumn("Weight",typeof(Double)),
                        //new DataColumn("DocumentTypeName", typeof(String)),
                    //    new DataColumn("ManhourPlan", typeof(String)),
                     //   new DataColumn("ManhourActual", typeof(String)),
                        //new DataColumn("Department", typeof(String)),
                       // new DataColumn("Engineer", typeof(String)),
                      //  new DataColumn("PersonsInvolved", typeof(String)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("OutgoingXDCBNo", typeof(String)),
                        new DataColumn("OutgoingXDCBDate", typeof(String)),
                        new DataColumn("IncomingNo", typeof(String)),
                        new DataColumn("IncomingDate", typeof(String)),
                        new DataColumn("ICANo", typeof(String)),
                        new DataColumn("ICADate", typeof(String)),
                        new DataColumn("ICAReviewCode", typeof(String)),
                        new DataColumn("Markup",typeof(String)),
                        new DataColumn("hardcopy", typeof(String)),
                        new DataColumn("Notes", typeof(String)),
                        //new DataColumn("LastestRevision", typeof(String)),
                       

                    });
                var selectedProject = this.scopeProjectService.GetById(!string.IsNullOrEmpty(this.lblProjectId.Value) ? Convert.ToInt32(this.lblProjectId.Value) : 0);
                if (selectedProject != null)
                {
                    projectName = selectedProject.Name;
                    var projectId = selectedProject.ID;
                    var currentDepartment = UserSession.Current.User.RoleId;
                    var workPackageList = this.workGroupService.GetAll().Where(t => t.ProjectId == projectId).ToList();

                    if (workPackageList.Count > 0)
                    {

                        //workbook.Open(Server.MapPath("~/" + templateManagement.FilePath));
                        workbook.Open(filePath + @"Template/CMDRReport_C.xls");

                        var sheets = workbook.Worksheets;
                        var wsSummary = sheets[0];
                        wsSummary.Cells.InsertRows(7, workPackageList.Count - 1);
                        for (int i = 0; i < workPackageList.Count; i++)
                        {
                            docList = this.documentPackageService.GetAllByWorkgroupHaveDocDelete(workPackageList[i].ID)
                              .OrderBy(t => t.DocNo)
                              .ToList();
                            var doclistnotdelete = docList.Where(t => !t.IsDelete).Select(t => t.DocNo).ToList();
                            List<int> doclistisdelete = docList.Where(t => t.IsDelete && doclistnotdelete.Contains(t.DocNo)).Select(t => (int)t.ID).ToList();

                            docList = docList.Where(t => !doclistisdelete.Contains(t.ID)).ToList();
                            var totalDoc = 0;
                            var totalDocIssues = 0;

                            var totalDocRev5Issues = 0;
                            var totalDocRevIssues = 0;

                            var totalDocDontIssues = 0;

                            dtFull.Rows.Clear();

                            sheets.AddCopy(1);

                            string st = workPackageList[i].Name.Replace("/", " ").Replace(@"\", " ");
                            if (st.Length > 30)
                            {
                                st = st.Substring(0, 25) + "_(" + i + ")";
                            }
                            sheets[i + 2].Name = st;
                            sheets[i + 2].Cells["Y4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            sheets[i + 2].Cells["C7"].PutValue(workPackageList[i].Name);
                            sheets[i + 2].Cells["E1"].PutValue("DETAIL ENGINEERING SERVICE FOR PROJECT " + projectName);

                            // add hyperlink
                            var linkName = workPackageList[i].Name;
                            wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                            wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1, "'" + st + "'" + "!D7");
                            if (workPackageList[i].DisciplineId != null)
                            {
                                var disciplineObj = this.disciplineService.GetById(workPackageList[i].DisciplineId.GetValueOrDefault());
                                wsSummary.Cells["C" + (7 + i)].PutValue(disciplineObj != null ? disciplineObj.Description : string.Empty);
                            }
                            wsSummary.Cells["C1"].PutValue("DETAIL  ENGINEERING SERVICE FOR " + projectName + " PROJECT");
                            wsSummary.Cells["C3"].PutValue(projectName + " PROJECT - SUMMARY REPORT");
                            wsSummary.Cells["O4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            wsSummary.Cells["O3"].PutValue(workPackageList.Count.ToString());


                            var docListHasAttachFile = docList.Where(t => t.HasAttachFile.GetValueOrDefault() && !t.IsDelete);
                            var wgDoc = docList.Count;
                            var wgDocIssues = docListHasAttachFile.Count();
                            var wgDocHardCopy = docListHasAttachFile.Count(t => !string.IsNullOrEmpty(t.OutgoingTransNo));
                            var wgTotalDocRev = wgDocIssues;
                            var wgDocDontIssues = wgDoc - wgDocIssues;

                            var total = docListHasAttachFile.Count(t => !string.IsNullOrEmpty(t.OutgoingLeterNo));

                            totalDoc += wgDoc;
                            totalDocIssues += wgDocIssues;

                            totalDocRev5Issues += wgDocHardCopy;
                            totalDocRevIssues += total;
                            totalDocDontIssues = totalDoc - totalDocIssues;

                            wsSummary.Cells["D" + (7 + i)].PutValue(wgDoc);
                            wsSummary.Cells["E" + (7 + i)].PutValue(wgDocDontIssues);
                            wsSummary.Cells["K" + (7 + i)].PutValue(wgTotalDocRev);
                            wsSummary.Cells["L" + (7 + i)].PutValue(totalDocRev5Issues);
                            wsSummary.Cells["M" + (7 + i)].PutValue(totalDocRevIssues);

                            var count = 1;

                            var listDocumentTypeId =
                                docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                            if (docList.Count > 0)
                            {
                                var mindate = docList.Select(t => t.CreatedDate.GetValueOrDefault()).Min();
                                mindate = mindate.AddDays(7);
                                var fromdate = (DateTime.Now.Date).AddDays(-7);
                                var todate = DateTime.Now.Date;

                                foreach (var documentTypeId in listDocumentTypeId)
                                {
                                    var documentType = this.documentTypeService.GetById((int)documentTypeId);

                                    var dataRow = dtFull.NewRow();
                                    dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                    dtFull.Rows.Add(dataRow);

                                    var listDocByDocType =
                                        docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                    foreach (var document in listDocByDocType)
                                    {
                                        dataRow = dtFull.NewRow();

                                        dataRow["Flag"] = document.IsDelete ? "D" : "";
                                        if (!document.IsDelete && (document.CreatedDate.Value.Date >= fromdate.Date) && (document.CreatedDate.Value.Date <= todate.Date))
                                        {
                                            dataRow["Flag"] = "N";
                                        }
                                        dataRow["DocId"] = document.ID;
                                        dataRow["NoIndex"] = count;
                                        dataRow["DocNo"] = document.DocNo;
                                        dataRow["DocTitle"] = document.DocTitle;

                                        dataRow["RevName"] = document.RevisionName;
                                        //dataRow["Start"] = document.StartDate != null
                                        //  ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                        //  : string.Empty;
                                        //dataRow["DeadLine"] = document.Deadline != null
                                        //  ? document.Deadline.Value.ToString("dd/MM/yyyy")
                                        //  : string.Empty;
                                        //dataRow["End"] = document.EndDate != null
                                        //  ? document.EndDate.Value.ToString("dd/MM/yyyy")
                                        //  : string.Empty;
                                        //dataRow["Complete"] = document.Complete != null ? document.Complete / 100 : 0;
                                        //   dataRow["Weight"] = document.Weight != null ? document.Weight / 100 : 0;

                                        //dataRow["DocumentTypeName"] = document.DocumentTypeName;
                                        //dataRow["ManhourPlan"] = document.ManHourPlan != null ? document.ManHourPlan.GetValueOrDefault() : 0;
                                        //dataRow["ManhourActual"] = document.ManHours != null ? document.ManHours.GetValueOrDefault() : 0;
                                        //dataRow["Department"] = document.DeparmentName;
                                        //dataRow["Engineer"] = document.EngineerName;
                                        //dataRow["PersonsInvolved"] = document.PersonsInvolved;
                                        dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                        dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                            ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["OutgoingXDCBNo"] = document.OutgoingLeterNo;
                                        dataRow["OutgoingXDCBDate"] = document.OutgoingLeterDate != null
                                            ? document.OutgoingLeterDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["IncomingNo"] = document.IncomingTransNo;
                                        dataRow["IncomingDate"] = document.IncomingTransDate != null
                                            ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["ICANo"] = document.ICAReviewOutTransNo;
                                        dataRow["ICADate"] = document.ICAReviewOutDate != null
                                            ? document.ICAReviewOutDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["ICAReviewCode"] = document.ICAReviewCode;
                                        dataRow["Markup"] = document.Markup;
                                        dataRow["hardcopy"] = string.Empty;
                                        dataRow["Notes"] = document.Notes;

                                        //  dataRow["LastestRevision"] = document.IsLeaf;
                                        count += 1;
                                        dtFull.Rows.Add(dataRow);
                                    }
                                }
                            }
                            sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                            sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 0, dtFull.Rows.Count, dtFull.Columns.Count, true);
                        }
                        sheets[1].IsVisible = false;

                        var filename = projectName.Replace("/", " ").Replace(@"\", " ") + " - " + "EMDR Report For Contractor " +
                                       DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                        filename = filename.Replace("/", " ").Replace(@"\", " ");
                        workbook.Save(filePath + filename);
                        this.DownloadByWriteByte(filePath + filename, filename, true);


                    }

                }
            }
            else if (e.Argument.Contains("ExportComment"))
            {

                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                //workbook.Open(filePath + @"Template/List_Project_Export_EMDR.xlsx");
                //var sheetPJ = workbook.Worksheets;
                //var sheetPJ1 = sheetPJ[2];
                //var data = sheetPJ1.Cells.ExportDataTableAsString(0, 0, sheetPJ1.Cells.MaxDataRow, 1);
                //var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_DocPack.rar");
                //var docPack = ZipPackage.CreateFile(serverTotalDocPackPath);
                //var error = "";
                //foreach (DataRow item in data.Rows)
                //{
                var docList = new List<DocumentPackage>();
                var projectName = string.Empty;
                var dtFull = new DataTable();
                var projectID = Convert.ToInt32(this.lblProjectId.Value);
                dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("Flag", typeof(String)),
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("RevName", typeof(String)),
                        new DataColumn("Start", typeof(String)),
                        new DataColumn("DeadLine", typeof(String)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("deadlineComment", typeof(String)),
                        new DataColumn("IncomingNo", typeof(String)),
                        new DataColumn("IncomingDate", typeof(String)),
                        new DataColumn("Code", typeof(String)),
                        new DataColumn("ResponseNo", typeof(String)),
                        new DataColumn("ResponseDate", typeof(String)),
                        new DataColumn("Responsedeadline", typeof(String)),
                        new DataColumn("Status", typeof(String)),
                        new DataColumn("Notes", typeof(String)),
                    });
                var selectedProject = this.scopeProjectService.GetById(!string.IsNullOrEmpty(this.lblProjectId.Value) ? Convert.ToInt32(this.lblProjectId.Value) : 0);
                //var selectedProject = this.scopeProjectService.GetByName(!string.IsNullOrEmpty(item[0].ToString()) ? item[0].ToString() : "");
                if (selectedProject != null)
                {
                    projectName = selectedProject.Name;
                    var projectId = selectedProject.ID;
                    var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
                    var currentDepartment = UserSession.Current.User.RoleId;
                    var workPackageList = isSeeFull
                        ? this.workGroupService.GetAll().Where(t => t.ProjectId == projectId).ToList()
                        : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()).Where(t => t.ProjectId == projectId).ToList();

                    if (UserSession.Current.IsEngineer)
                    {
                        var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                        workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
                    }

                    if (workPackageList.Count > 0)
                    {
                        // workbook.Open(Server.MapPath("~/" + templateManagement.FilePath));
                        workbook.Open(filePath + @"Template/CMDRReport_Comment.xls");
                        var sheets = workbook.Worksheets;
                        var wsSummary = sheets[0];
                        wsSummary.Cells.InsertRows(7, workPackageList.Count - 1);
                        for (int i = 0; i < workPackageList.Count; i++)
                        {
                            docList = this.documentPackageService.GetAllByWorkgroupComment(workPackageList[i].ID)
                  .OrderBy(t => t.DocNo)
                  .ToList();

                            dtFull.Rows.Clear();

                            sheets.AddCopy(1);
                            string st = workPackageList[i].Name.Replace("/", " ").Replace(@"\", " ");
                            if (st.Length > 30)
                            {
                                st = st.Substring(0, 25) + "_(" + i + ")";
                            }
                            sheets[i + 2].Name = st;
                            sheets[i + 2].Cells["S4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            sheets[i + 2].Cells["C7"].PutValue(workPackageList[i].Name);
                            sheets[i + 2].Cells["E1"].PutValue("DETAIL ENGINEERING SERVICE FOR PROJECT " + projectName);

                            // add hyperlink
                            var linkName = workPackageList[i].Name;
                            wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                            wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1, "'" + workPackageList[i].Name.Replace("/", " ").Replace(@"\", " ") + "_" + workPackageList[i].RevisionName + "'" + "!D7");
                            if (workPackageList[i].DisciplineId != null)
                            {
                                var disciplineObj = this.disciplineService.GetById(workPackageList[i].DisciplineId.GetValueOrDefault());
                                wsSummary.Cells["C" + (7 + i)].PutValue(disciplineObj != null ? disciplineObj.Description : string.Empty);
                            }
                            wsSummary.Cells["C1"].PutValue("DETAIL  ENGINEERING SERVICE FOR " + projectName + " PROJECT");
                            wsSummary.Cells["C3"].PutValue(projectName + " PROJECT - SUMMARY REPORT");
                            wsSummary.Cells["O4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            wsSummary.Cells["O3"].PutValue(workPackageList.Count.ToString());
                            var count = 1;

                            var listDocumentTypeId =
                                docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                            double complete = 0;
                            double weight = 0;

                            foreach (var documentTypeId in listDocumentTypeId)
                            {
                                var documentType = this.documentTypeService.GetById((int)documentTypeId);

                                var dataRow = dtFull.NewRow();
                                dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                dtFull.Rows.Add(dataRow);

                                var listDocByDocType =
                                    docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                foreach (var document in listDocByDocType)
                                {
                                    dataRow = dtFull.NewRow();
                                    if (document.DealineComment != null && document.DealineComment.GetValueOrDefault() < DateTime.Now.Date && string.IsNullOrEmpty(document.IncomingTransNo))
                                    {
                                        dataRow["Flag"] = "O";
                                    }
                                    dataRow["DocId"] = document.ID;
                                    dataRow["NoIndex"] = count;
                                    dataRow["DocNo"] = document.DocNo;
                                    dataRow["DocTitle"] = document.DocTitle;

                                    dataRow["RevName"] = document.RevisionName;
                                    dataRow["Start"] = document.StartDate != null
                                      ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;
                                    dataRow["DeadLine"] = document.Deadline != null
                                      ? document.Deadline.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;

                                    dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                    dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                        ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["deadlineComment"] = document.DealineComment != null
                                        ? document.DealineComment.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["IncomingNo"] = document.IncomingTransNo;
                                    dataRow["IncomingDate"] = document.IncomingTransDate != null
                                        ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["Code"] = document.FinalCodeName;
                                    dataRow["ResponseNo"] = document.ResponseTransNo;
                                    dataRow["ResponseDate"] = document.RevisionActualDate != null
                                        ? document.RevisionActualDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["Responsedeadline"] = document.ResponseDeadline != null
                                       ? document.ResponseDeadline.Value.ToString("dd/MM/yyyy")
                                       : string.Empty;
                                    dataRow["Status"] = document.StatusName;
                                    dataRow["Notes"] = document.Notes;

                                    //  dataRow["LastestRevision"] = document.IsLeaf;
                                    count += 1;
                                    dtFull.Rows.Add(dataRow);
                                    //if (!document.IsDelete)
                                    //{
                                    //    complete += (document.Complete != null && document.Weight != null) ? (double)((document.Complete / 100) * (document.Weight / 100)) : 0;
                                    //    weight += document.Weight != null ? (double)document.Weight / 100 : 0;
                                    //}
                                }
                            }

                            sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                            sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 0, dtFull.Rows.Count, dtFull.Columns.Count, true);
                            //sheets[i + 2].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");
                            //sheets[i + 2].Cells[7 + dtFull.Rows.Count, 9].PutValue(complete);
                            //sheets[i + 2].Cells[7 + dtFull.Rows.Count, 10].PutValue(weight);
                        }
                        sheets[1].IsVisible = false;

                        var filename = projectName.Replace("/", " ").Replace(@"\", " ") + " - " + "EMDR Report For Comment " +
                                       DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                        filename = filename.Replace("/", " ").Replace(@"\", " ");
                        workbook.Save(filePath + filename);
                        //if (File.Exists(filePath + filename))
                        //{
                        //    docPack.Add(filePath + filename);
                        //}
                        this.DownloadByWriteByte(filePath + filename, filename, true);
                    }
                }
                //else
                //{
                //    error += item[0].ToString() + "\n";
                //}
                //}
                //this.DownloadByWriteByte(serverTotalDocPackPath, "_DocPack.rar", true);
            }
            else if (e.Argument == "ExportWaitComment")
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                var docList = new List<DocumentPackage>();
                var projectName = string.Empty;
                var dtFull = new DataTable();
                var projectID = Convert.ToInt32(this.lblProjectId.Value);
                dtFull.Columns.AddRange(new[]
                    {
                       // new DataColumn("Flag", typeof(String)),
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("RevName", typeof(String)),
                        new DataColumn("Start", typeof(String)),
                        new DataColumn("DeadLine", typeof(String)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("deadlineComment", typeof(String)),
                        new DataColumn("IncomingNo", typeof(String)),
                        new DataColumn("IncomingDate", typeof(String)),
                        new DataColumn("Code", typeof(String)),
                        new DataColumn("ResponseNo", typeof(String)),
                        new DataColumn("ResponseDate", typeof(String)),
                        new DataColumn("Responsedeadline", typeof(String)),
                        new DataColumn("Status", typeof(String)),
                        new DataColumn("Notes", typeof(String)),
                    });
                var selectedProject = this.scopeProjectService.GetById(!string.IsNullOrEmpty(this.lblProjectId.Value) ? Convert.ToInt32(this.lblProjectId.Value) : 0);
                if (selectedProject != null)
                {
                    projectName = selectedProject.Name;
                    var projectId = selectedProject.ID;
                    var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
                    var currentDepartment = UserSession.Current.User.RoleId;
                    var workPackageList = isSeeFull
                        ? this.workGroupService.GetAll().Where(t => t.ProjectId == projectId).ToList()
                        : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault()).Where(t => t.ProjectId == projectId).ToList();

                    if (UserSession.Current.IsEngineer)
                    {
                        var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                        workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
                    }
                    if (workPackageList.Count > 0)
                    {
                        workbook.Open(filePath + @"Template/CMDRReport_WaitComment.xls");
                        var sheets = workbook.Worksheets;
                        var wsSummary = sheets[0];
                        wsSummary.Cells.InsertRows(7, workPackageList.Count - 1);
                        for (int i = 0; i < workPackageList.Count; i++)
                        {
                            docList = this.documentPackageService.GetAllByWorkgroupComment(workPackageList[i].ID)
                  .OrderBy(t => t.DocNo).Where(t => t.DealineComment != null && string.IsNullOrEmpty(t.IncomingTransNo))
                  .ToList();
                            if (docList.Count > 0)
                            {

                                dtFull.Rows.Clear();

                                sheets.AddCopy(1);
                                string st = workPackageList[i].Name.Replace("/", " ").Replace(@"\", " ");
                                if (st.Length > 30)
                                {
                                    st = st.Substring(0, 25) + "_(" + i + ")";
                                }
                                sheets[i + 2].Name = st;
                                sheets[i + 2].Cells["s4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                                sheets[i + 2].Cells["C7"].PutValue(workPackageList[i].Name);
                                sheets[i + 2].Cells["E1"].PutValue("DOCUMENTLIST WAIT COMMENT FOR PROJECT " + projectName);

                                // add hyperlink
                                var linkName = workPackageList[i].Name;
                                wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                                wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1, "'" + workPackageList[i].Name.Replace("/", " ").Replace(@"\", " ") + "_" + workPackageList[i].RevisionName + "'" + "!D7");
                                if (workPackageList[i].DisciplineId != null)
                                {
                                    var disciplineObj = this.disciplineService.GetById(workPackageList[i].DisciplineId.GetValueOrDefault());
                                    wsSummary.Cells["C" + (7 + i)].PutValue(disciplineObj != null ? disciplineObj.Description : string.Empty);
                                }
                                wsSummary.Cells["C1"].PutValue("DOCUMENTLIST WAIT COMMENT FOR " + projectName + " PROJECT");
                                wsSummary.Cells["C3"].PutValue(projectName + " PROJECT - SUMMARY REPORT");
                                wsSummary.Cells["O4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                                wsSummary.Cells["O3"].PutValue(workPackageList.Count.ToString());
                                var count = 1;

                                var listDocumentTypeId =
                                    docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                                double complete = 0;
                                double weight = 0;

                                foreach (var documentTypeId in listDocumentTypeId)
                                {
                                    var documentType = this.documentTypeService.GetById((int)documentTypeId);

                                    var dataRow = dtFull.NewRow();
                                    dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                    dtFull.Rows.Add(dataRow);

                                    var listDocByDocType =
                                        docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                    foreach (var document in listDocByDocType)
                                    {
                                        dataRow = dtFull.NewRow();
                                        dataRow["DocId"] = document.ID;
                                        dataRow["NoIndex"] = count;
                                        dataRow["DocNo"] = document.DocNo;
                                        dataRow["DocTitle"] = document.DocTitle;

                                        dataRow["RevName"] = document.RevisionName;
                                        dataRow["Start"] = document.StartDate != null
                                          ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                          : string.Empty;
                                        dataRow["DeadLine"] = document.Deadline != null
                                          ? document.Deadline.Value.ToString("dd/MM/yyyy")
                                          : string.Empty;

                                        dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                        dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                            ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["deadlineComment"] = document.DealineComment != null
                                            ? document.DealineComment.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["IncomingNo"] = document.IncomingTransNo;
                                        dataRow["IncomingDate"] = document.IncomingTransDate != null
                                            ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["Code"] = document.FinalCodeName;

                                        dataRow["ResponseNo"] = document.ResponseTransNo;
                                        dataRow["ResponseDate"] = document.RevisionActualDate != null
                                            ? document.RevisionActualDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["Responsedeadline"] = document.ResponseDeadline != null
                                           ? document.ResponseDeadline.Value.ToString("dd/MM/yyyy")
                                           : string.Empty;
                                        dataRow["Status"] = document.StatusName;
                                        dataRow["Notes"] = document.Notes;
                                        count += 1;
                                        dtFull.Rows.Add(dataRow);

                                    }
                                }
                                sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                                sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                            }
                        }

                        sheets[1].IsVisible = false;

                        var filename = projectName.Replace("/", " ").Replace(@"\", " ") + " - " + "EMDR Report For Comment " +
                                       DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                        filename = filename.Replace("/", " ").Replace(@"\", " ");
                        workbook.Save(filePath + filename);
                        this.DownloadByWriteByte(filePath + filename, filename, true);
                    }
                }
            }
            else if (e.Argument.Contains("FTK01"))
            {
                var selectedProject = this.scopeProjectService.GetById(!string.IsNullOrEmpty(this.lblProjectId.Value) ? Convert.ToInt32(this.lblProjectId.Value) : 0);
                if (selectedProject != null)
                {
                    var wpList = this.workGroupService.GetAllWorkGroupOfProject(selectedProject.ID);
                    var wpCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("Department", Type.GetType("System.String")),
                                new DataColumn("WPStartDate", Type.GetType("System.String")),
                                new DataColumn("WPEndDate", Type.GetType("System.String")),
                                new DataColumn("InputDataSupplier", Type.GetType("System.String")),
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("ProjectManager", Type.GetType("System.String")),
                                new DataColumn("Supervisor", Type.GetType("System.String")),
                                new DataColumn("StartDate", Type.GetType("System.String")),
                                new DataColumn("EndDate", Type.GetType("System.String")),
                                new DataColumn("ProjectDescription", Type.GetType("System.String")),
                                new DataColumn("ProjectName", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var infoItem = reportInfo.NewRow();
                    var projectManager = this.userService.GetByID(selectedProject.ProjectManagerId.GetValueOrDefault());

                    infoItem["ProjectManager"] = projectManager != null ? projectManager.FullName : string.Empty;
                    infoItem["Supervisor"] = selectedProject.SupervisorUserName;
                    infoItem["StartDate"] = selectedProject.StartDate != null ? selectedProject.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                    infoItem["EndDate"] = selectedProject.EndDate != null ? selectedProject.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty; ;
                    infoItem["ProjectDescription"] = selectedProject.Description;
                    infoItem["ProjectName"] = selectedProject.Name;
                    reportInfo.Rows.Add(infoItem);

                    foreach (var workpackage in wpList)
                    {
                        var dataworkpackageItem = dataTable.NewRow();
                        wpCount += 1;
                        dataworkpackageItem["Index"] = wpCount;
                        dataworkpackageItem["WorkPackageContent"] = workpackage.Name;
                        dataworkpackageItem["Department"] = workpackage.DepartmentName;
                        dataworkpackageItem["WPStartDate"] = workpackage.StartDate != null ? workpackage.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                        dataworkpackageItem["WPEndDate"] = workpackage.EndDate != null ? workpackage.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                        dataworkpackageItem["InputDataSupplier"] = workpackage.InputDataSupplier;
                        dataTable.Rows.Add(dataworkpackageItem);


                        foreach (var workpackageContent in this.workpackageContentService.GetAllByWorkpackage(workpackage.ID))
                        {
                            var contentItem = dataTable.NewRow();
                            contentItem["WorkPackageContent"] = workpackageContent.ContentInfo;
                            contentItem["WPEndDate"] = workpackageContent.Deadline != null ? workpackageContent.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                            dataTable.Rows.Add(contentItem);
                        }
                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("../../Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = e.Argument == "FTK01V" ? "F-TK-01-V_Template.doc" : "F-TK-01-R_Template.doc";
                    var strOutputFileName = "F-TK-01_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                    if (isSuccess)
                    {
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "F-TK-01_" + Utility.RemoveSpecialCharacter(selectedProject.Name, "-") + ".doc", true);
                    }
                }
            }
            else if (e.Argument == "ExportWPMaster")
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\WorkpackageMasterList_ByProject_Template.xls");
                var sheets = workbook.Worksheets;
                var readMeSheet = sheets[0];
                var dataSheet = sheets[1];

                var projectId = Convert.ToInt32(this.grdDocument.SelectedValue.ToString());
                var projecObj = this.scopeProjectService.GetById(projectId);

                if (projecObj != null)
                {
                    //var projectList = this.scopeProjectService.GetAll();
                    var wpRevisionList = this.wpRevisionService.GetAll();
                    var departmentList = this.roleService.GetAll(UserSession.Current.User.Id == 1);
                    var areaList = this.areaService.GetAll();
                    var disciplineList = this.disciplineService.GetAll();

                    //for (var i = 0; i < projectList.Count; i++)
                    //{
                    //    readMeSheet.Cells["B" + (7 + i)].PutValue(projectList[i].Name);
                    //    readMeSheet.Cells["C" + (7 + i)].PutValue(projectList[i].Description);
                    //}

                    for (var i = 0; i < departmentList.Count; i++)
                    {
                        readMeSheet.Cells["B" + (7 + i)].PutValue(departmentList[i].Name);
                        readMeSheet.Cells["C" + (7 + i)].PutValue(departmentList[i].Description);
                    }

                    for (var i = 0; i < areaList.Count; i++)
                    {
                        readMeSheet.Cells["E" + (7 + i)].PutValue(areaList[i].Name);
                        readMeSheet.Cells["F" + (7 + i)].PutValue(areaList[i].Description);
                    }

                    for (var i = 0; i < disciplineList.Count; i++)
                    {
                        readMeSheet.Cells["H" + (7 + i)].PutValue(disciplineList[i].Name);
                        readMeSheet.Cells["I" + (7 + i)].PutValue(disciplineList[i].Description);
                    }

                    for (var i = 0; i < wpRevisionList.Count; i++)
                    {
                        readMeSheet.Cells["N" + (7 + i)].PutValue(wpRevisionList[i].Name);
                        readMeSheet.Cells["O" + (7 + i)].PutValue(wpRevisionList[i].Description);
                    }

                    //var rangeProjectList = readMeSheet.Cells.CreateRange("B7", "B" + (7 + (projectList.Count == 0 ? 1 : projectList.Count)));
                    //rangeProjectList.Name = "ProjectList";

                    var rangeDepartmentList = readMeSheet.Cells.CreateRange("B7", "B" + (7 + (departmentList.Count == 0 ? 1 : departmentList.Count)));
                    rangeDepartmentList.Name = "DepartmentList";

                    var rangeAreaList = readMeSheet.Cells.CreateRange("E7", "E" + (7 + (areaList.Count == 0 ? 1 : areaList.Count)));
                    rangeAreaList.Name = "AreaList";

                    var rangeDisciplineList = readMeSheet.Cells.CreateRange("H7", "H" + (7 + (disciplineList.Count == 0 ? 1 : disciplineList.Count)));
                    rangeDisciplineList.Name = "DisciplineList";

                    var rangeTypeList = readMeSheet.Cells.CreateRange("K7", "K8");
                    rangeTypeList.Name = "TypeList";

                    var rangeWPRevisionList = readMeSheet.Cells.CreateRange("N7", "N" + (7 + (wpRevisionList.Count == 0 ? 1 : wpRevisionList.Count)));
                    rangeWPRevisionList.Name = "WPRevision";

                    var validations = dataSheet.Validations;
                    //this.CreateValidation(rangeProjectList.Name, validations, 2, 1000, 1, 1);
                    this.CreateValidation(rangeDepartmentList.Name, validations, 2, 1000, 4, 4);
                    this.CreateValidation(rangeAreaList.Name, validations, 2, 1000, 5, 5);
                    this.CreateValidation(rangeDisciplineList.Name, validations, 2, 1000, 6, 6);
                    this.CreateValidation(rangeTypeList.Name, validations, 2, 1000, 7, 7);
                    this.CreateValidation(rangeWPRevisionList.Name, validations, 2, 1000, 3, 3);

                    dataSheet.Cells["C1"].PutValue("Workpackage list of " + projecObj.Name + "(" + projecObj.Description + ")");
                    dataSheet.Cells["A1"].PutValue(projecObj.ID);
                    readMeSheet.AutoFitRows();

                    var filename = Utility.RemoveSpecialCharacter(projecObj.Name, "-") + "_WorkpackageInfoMasterList_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument == "IDCMatrix")
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\IDCMatrixTemplate.xls");
                var sheets = workbook.Worksheets;

                var projectId = Convert.ToInt32(this.grdDocument.SelectedValue.ToString());
                var projectObj = this.scopeProjectService.GetById(projectId);
                if (projectObj != null)
                {
                    if (projectObj.IsIDC.GetValueOrDefault())
                    {
                        var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectId);
                        var matrixProjectList = this.distributionMatrixDetailService.GetByProject(projectObj.ID);
                        for (int i = 0; i < wpList.Count; i++)
                        {
                            sheets.AddCopy("Temp");
                            sheets[i + 1].Name = (i + 1) + "." + wpList[i].DisciplineName;
                            sheets[i + 1].Cells["A1"].PutValue(projectId);
                            sheets[i + 1].Cells["A2"].PutValue(wpList[i].ID);
                            sheets[i + 1].Cells["A3"].PutValue(wpList.Count);
                            sheets[i + 1].Cells["C29"].PutValue(wpList[i].Name);
                            var wpInSheet = wpList.Where(t => t.ID != wpList[i].ID).ToList();
                            if (wpInSheet.Count > 0)
                            {
                                for (int j = 0; j < wpInSheet.Count; j++)
                                {
                                    var disciplineObj = this.disciplineService.GetById(wpInSheet[j].DisciplineId.GetValueOrDefault());
                                    if (disciplineObj != null)
                                    {
                                        sheets[i + 1].Cells.InsertColumn(14 + j, true);
                                        sheets[i + 1].Cells[4, 14 + j].PutValue(disciplineObj.ID);
                                        sheets[i + 1].Cells[5, 14 + j].PutValue(disciplineObj.FullName);
                                    }
                                }
                                sheets[i + 1].Cells.Merge(1, 14, 3, wpInSheet.Count);
                                sheets[i + 1].Cells["O2"].PutValue("IDC MATRIX");
                                Aspose.Cells.Style st = workbook.Styles[workbook.Styles.Add()];
                                st.Font.Name = "Arial";
                                st.Font.Size = 20;
                                st.VerticalAlignment = TextAlignmentType.Center;
                                st.HorizontalAlignment = TextAlignmentType.Center;
                                sheets[i + 1].Cells["O2"].SetStyle(st);

                                sheets[i + 1].Cells["C1"].PutValue(projectObj.Name + "\r\n" + projectObj.Description);


                                var docList = this.documentPackageService.GetAllByWorkgroup(wpList[i].ID);
                                int count = 0;
                                var dtDiscipline = new DataTable();
                                int index = 31;
                                dtDiscipline = sheets[i + 1].Cells.ExportDataTableAsString(4, 14, 2, wpInSheet.Count);
                                foreach (var docItem in docList)
                                {
                                    sheets[i + 1].Cells.InsertRow(index);
                                    sheets[i + 1].Cells["B" + index].PutValue(docItem.ID);
                                    sheets[i + 1].Cells["C" + index].PutValue(docItem.DocNo);
                                    sheets[i + 1].Cells["D" + index].PutValue(docItem.DocTitle);
                                    sheets[i + 1].Cells["E" + index].PutValue(docItem.DocumentTypeName);
                                    sheets[i + 1].Cells["F" + index].PutValue(docItem.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                    sheets[i + 1].Cells["G" + index].PutValue(docItem.IDCDeadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                    sheets[i + 1].Cells["H" + index].PutValue(docItem.IFRDeadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                    sheets[i + 1].Cells["I" + index].PutValue(docItem.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"));
                                    sheets[i + 1].Cells["J" + index].PutValue(docItem.EngineerName);
                                    sheets[i + 1].Cells["K" + index].PutValue(docItem.LeaderName);
                                    sheets[i + 1].Cells["L" + index].PutValue(docItem.ManHourPlan);
                                    for (int j = 0; j < wpInSheet.Count; j++)
                                    {
                                        var disID = Convert.ToInt32(dtDiscipline.Rows[0][j].ToString());
                                        var matrixObj = matrixProjectList.FirstOrDefault(t => (t.DocumentID == docItem.ID || t.DocumentID == docItem.ParentId) && t.DisciplineID == disID);
                                        if (matrixObj != null)
                                        {
                                            sheets[i + 1].Cells[index - 1, 14 + j].PutValue("x");
                                        }
                                    }
                                    index++;
                                }
                                sheets[i + 1].Cells.DeleteRow(29);
                                sheets[i + 1].Cells.DeleteColumn(13);
                            }
                        }
                    }
                    sheets[0].IsVisible = false;
                    var filename = "IDC Matrix - " + projectObj.Name + " - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument.Contains("CreateFolderTrans"))
            {
                var scopeProjectId = Convert.ToInt32(e.Argument.Split('_')[1]);
                var obj = this.scopeProjectService.GetById(scopeProjectId);
                var folder = this.folderService.GetAllByName(obj.Name);
                if (folder != null)
                {
                    var trans = CreateFolderLibrary("Transmittal", folder, obj);
                    // folder trans out
                    var transout = CreateFolderLibrary("Outgoing", trans, obj);
                    var templatemanagment = this.templateManagementService.GetSpecial(7, scopeProjectId);
                    if (templatemanagment != null)
                    {
                        templatemanagment.TransFolderId = transout.ID;
                        templatemanagment.ProjectId = scopeProjectId;
                        this.templateManagementService.Update(templatemanagment);
                    }
                    else
                    {
                        templatemanagment = new TemplateManagement();
                        templatemanagment.TypeId = 7;
                        templatemanagment.Name = "Folder Outgoing";
                        templatemanagment.FilePath = string.Empty;
                        templatemanagment.TransFolderId = transout.ID;
                        templatemanagment.ProjectId = scopeProjectId;
                        templatemanagment.CreatedBy = UserSession.Current.User.Id;
                        templatemanagment.CreatedDate = DateTime.Now;
                        this.templateManagementService.Insert(templatemanagment);
                    }
                }
            }
        }

        private Folder CreateFolderLibrary(string name, Folder folder, ScopeProject scopeProjectId)
        {
            var obj = new Folder()
            {
                Name = name,
                Description = name,
                ParentID = folder.ID,
                DirName = folder.DirName + "/" + Regex.Replace(name, @"[^0-9a-zA-Z]+", string.Empty),
                ProjectId = scopeProjectId.ID,
                CreatedBy = UserSession.Current.User.Id,
                CreatedDate = DateTime.Now
            };
            Directory.CreateDirectory(Server.MapPath(obj.DirName));
            var folderId = this.folderService.Insert(obj);
            var AllUser = this.userService.GetAll(false);
            var departmentManger = this.userService.GetByID(scopeProjectId.SupervisorId.GetValueOrDefault());
            var ussergiporsuper = AllUser.Where(t => t.Id == scopeProjectId.ProjectManagerId || t.Id == scopeProjectId.SupervisorId);
            var addingPermission = AllUser.Where(t => t.Id == scopeProjectId.ProjectManagerId || t.Id == scopeProjectId.SupervisorId || t.RoleId == departmentManger.RoleId).Select(t => new UserDataPermission()
            {
                ProjectId = scopeProjectId.ID,
                RoleId = t.RoleId,
                FolderId = folderId,
                UserId = t.Id,
                IsFullPermission = true,
                CreatedDate = DateTime.Now,
                CreatedBy = UserSession.Current.User.Id,
                OnlyView = false
            });
            this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            List<int> groupOut = ConfigurationManager.AppSettings.Get("GroupNotInVien").Split(',').Select(Int32.Parse).ToList();
            var listgroup = this.roleService.GetAll(false).Where(t => t.IsWorking.GetValueOrDefault() && !groupOut.Contains(t.Id) && t.Id != departmentManger.RoleId).Select(t => t.Id).ToList();
            var listuserview = AllUser.Where(t => listgroup.Contains(t.RoleId.GetValueOrDefault()) && (t.Id != scopeProjectId.ProjectManagerId && t.Id != scopeProjectId.SupervisorId) && !t.IsDC.GetValueOrDefault());

            addingPermission = listuserview.Select(t => new UserDataPermission()
            {
                ProjectId = scopeProjectId.ID,
                RoleId = t.RoleId,
                FolderId = folderId,
                UserId = t.Id,
                IsFullPermission = false,
                CreatedDate = DateTime.Now,
                CreatedBy = UserSession.Current.User.Id,
                OnlyView = true
            });
            this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            return obj;
        }

        /// <summary>
        /// Conver List<t> to datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public DataTable ToDataTables<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            // PropertyInfo and BindingFlags using library System.Reflection;
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    if (i == 10 || i == 11 || i == 12)
                    {
                        if (Props[i].GetValue(item, null) != null)
                        {
                            try
                            {
                                DateTime d = DateTime.Parse(Props[i].GetValue(item, null).ToString());

                                values[i] = d.ToString("dd/MM/yyyy");
                            }
                            catch { }
                        }
                        else
                        {
                            values[i] = Props[i].GetValue(item, null);
                        }
                    }
                    else
                    {
                        values[i] = Props[i].GetValue(item, null);
                    }
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }


        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            try
            {
                this.LoadDocuments(false);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Grid KhacHang item created
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var editLink = (HtmlControl)e.Item.FindControl("EditImage");
            }

            if (e.Item is GridEditableItem)
            {
                var item = e.Item as GridEditableItem;
                var btnDelete = item["DeleteColumn"].Controls[0] as ImageButton;
                var editLink = item["EditColumn"].FindControl("EditImage") as HtmlControl;
                if (editLink != null)
                {
                    if (item["IsFullPermission"].Text.ToLower() == "true")
                    {
                        editLink.Visible = true;

                    }
                    else
                    {
                        editLink.Visible = false;
                    }
                }

                if (btnDelete != null)
                {
                    if (item["CanDelete"].Text.ToLower() == "true")
                    {
                        btnDelete.ImageUrl = "~/Images/delete.png";
                        btnDelete.Enabled = true;
                    }
                    else
                    {
                        btnDelete.ImageUrl = "~/Images/deleteDisable.png";
                        btnDelete.Enabled = false;
                    }
                }
            }
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var disId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var projectAppendixObj = this.projectAppendixService.GetByProject(disId);
            if (projectAppendixObj != null)
            {
                this.projectAppendixService.Delete(projectAppendixObj.ID);
            }
            this.scopeProjectService.Delete(disId);

            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }

        protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {
            //AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //var cblShow = (RadioButtonList)this.CustomerMenu.Items[4].FindControl("cblShow");
            var ddlShow = (DropDownList)this.CustomerMenu.Items[4].FindControl("ddlShow");
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (ddlShow.SelectedValue != "ShowMyOverDue" && ddlShow.SelectedValue != "ShowOverDue")
                {
                    if (item["IsLater"].Text.ToLower() == "true")
                    {
                        item.BackColor = Color.Crimson;
                        item.BorderColor = Color.Crimson;
                        //item.ForeColor = Color.White;
                    }
                }
                if (ddlShow.SelectedValue != "ShowMyStop" && ddlShow.SelectedValue != "ShowStop")
                {
                    if (item["IsStop"].Text.ToLower() == "true")
                    {
                        item.BackColor = Color.Orange;
                        item.BorderColor = Color.Orange;
                        //item.ForeColor = Color.White;
                    }
                }
                Label lblWeight = (Label)item["TotalWorkpackageWeight"].FindControl("lblWeight");
                if (lblWeight.Text.Contains("%"))
                {
                    double weight = Convert.ToDouble(lblWeight.Text.Replace("%", string.Empty));
                    weight = Math.Round(weight, 2);
                    lblWeight.Text = weight.ToString() + "%";
                }
                Label lblComplete = (Label)item["Complete"].FindControl("lblComplete");
                if (lblComplete.Text.Contains("%"))
                {
                    double complete = Convert.ToDouble(lblComplete.Text.Replace("%", string.Empty));
                    complete = Math.Round(complete, 2);
                    lblComplete.Text = complete.ToString() + "%";
                }
                var projectAppendix = this.projectAppendixService.GetByProject(Convert.ToInt32(item.GetDataKeyValue("ID")));
                if (projectAppendix != null)
                {
                    item["planName"].Text = projectAppendix.OrderCode;
                }
                if (!this.roleService.IsNipi(UserSession.Current.RoleId))
                {
                    grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                    grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                    grdDocument.MasterTableView.GetColumn("DownloadColumn").Visible = false;
                    grdDocument.MasterTableView.GetColumn("ViewMDR").Visible = false;
                    grdDocument.MasterTableView.GetColumn("ViewSummary").Visible = false;
                }
            }
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"~/Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    //if (item.Text == "Originator")
                    //{
                    //    item.Selected = true;
                    //}
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = Aspose.Cells.ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;
            validation.ShowInput = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";
            validation.InputTitle = "Warning";

            // Set the error message.
            validation.ErrorMessage = "Please select item from the list";
            validation.InputMessage = "Please select item from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                var fs = new FileStream(strFileName, FileMode.Open);
                var streamLength = Convert.ToInt32(fs.Length);
                var data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        protected void ckbShowUncomplete_CheckedChange(object sender, EventArgs e)
        {
            var ckbShowLater = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowLater");
            var cbShowAll = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowAll");
            var cbShowUncomplete = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowUncomplete");
            cbShowAll.Checked = !cbShowUncomplete.Checked;
            ckbShowLater.Checked = !cbShowUncomplete.Checked;

            this.grdDocument.Rebind();
        }

        protected void ckbShowAll_CheckedChange(object sender, EventArgs e)
        {
            var ckbShowLater = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowLater");
            var cbShowAll = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowAll");
            var cbShowUncomplete = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowUncomplete");
            cbShowUncomplete.Checked = !cbShowAll.Checked;
            ckbShowLater.Checked = !cbShowAll.Checked;

            this.grdDocument.Rebind();
        }

        protected void ckbShowLater_CheckedChange(object sender, EventArgs e)
        {
            var ckbShowLater = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowLater");
            var cbShowAll = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowAll");
            var cbShowUncomplete = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowUncomplete");
            cbShowUncomplete.Checked = !ckbShowLater.Checked;
            cbShowAll.Checked = !ckbShowLater.Checked;

            this.grdDocument.Rebind();
        }

        protected void cblShow_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var cblShow = (RadioButtonList)this.CustomerMenu.Items[4].FindControl("cblShow");
            cblShow.SelectedValue = ((RadioButtonList)sender).SelectedValue;
            this.grdDocument.Rebind();
        }
        protected DateTime Layngaythu2(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        protected DateTime layngaychunhat(DateTime date)
        {
            return this.Layngaythu2(date).AddDays(6);
        }
        protected void ddlShow_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }
        protected void rtvWorkList_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            LoadDocuments(true);
        }
    }
}


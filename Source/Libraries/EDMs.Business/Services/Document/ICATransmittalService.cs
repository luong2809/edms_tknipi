﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICATransmittalService.cs" company="">
//   
// </copyright>
// <summary>
//   The category service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class ICATransmittalService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly ICATransmittalDAO repo;

        private readonly AttachDocToICATransmittalService attachDocToICATransmittalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ICATransmittalService"/> class.
        /// </summary>
        public ICATransmittalService()
        {
            this.repo = new ICATransmittalDAO();
            this.attachDocToICATransmittalService = new AttachDocToICATransmittalService();
        }
        #region Get (Advances)

        /// <summary>
        /// The get specific.
        /// </summary>
        /// <param name="tranId">
        /// The tran id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ICATransmittal> GetSpecific(int tranId)
        {
            return this.repo.GetSpecific(tranId);
        }

        public List<ICATransmittal> GetAllByDocumentId(int documentId)
        {
            var listTransId =
                this.attachDocToICATransmittalService.GetAllByDocId(documentId).Select(t => t.ICATransmittalId).Distinct();

            return listTransId.Select(tranId => this.repo.GetById(tranId.GetValueOrDefault()))
                            .Where(trans => trans != null).ToList();
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<ICATransmittal> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// The get all by owner.
        /// </summary>
        /// <param name="createdBy">
        /// The created by.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ICATransmittal> GetAllByOwner(int userId)
        {
            return this.repo.GetAllByOwner(userId);
        }

        public ICATransmittal GetByTransNo(string transNo)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.TransmittalNumber == transNo);
        }
        public List<ICATransmittal> GetAllByProject(List<int> projectId)
        {
            return this.repo.GetAll().Where(t => projectId.Contains(t.ProjectId.GetValueOrDefault())).ToList();
        }

        public List<ICATransmittal> GetAllByType(int type)
        {
            return
                this.repo.GetAll()
                    .Where(t => t.TypeTran == type)
                    .OrderByDescending(t => t.TransmittalNumber)
                    .ToList();
        }

        public List<ICATransmittal> GetAllByType(int type, int projectId)
        {
            return
                this.repo.GetAll()
                    .Where(t => t.TypeTran == type && t.ProjectId == projectId)
                    .OrderByDescending(t => t.TransmittalNumber)
                    .ToList();
        }
        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public ICATransmittal GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(ICATransmittal bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(ICATransmittal bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(ICATransmittal bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}

﻿using System;
using System.Net;
using System.Web;
using System.Globalization;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.Configuration;
using EDMs.Business.Services.Security;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using EDMs.Data.Entities;
using System.Collections.Generic;
using System.Security.Principal;


namespace EDMs.Web
{
    using System.Configuration;

    using EDMs.Business.Services.Security;

    public partial class Login : Page
    {
        #region Fields
        private readonly UserService _userService;
        private readonly UserAccessSystemService useraccess;
        private readonly CountUserAccessService countuser;
        private readonly UsersLoginHistoryService _UserLoginHistoryService;
        private readonly RoleService roleService = new RoleService();
        #endregion

        public Login()
        {
            _userService = new UserService();
            useraccess = new UserAccessSystemService();
            countuser = new CountUserAccessService();
            _UserLoginHistoryService = new UsersLoginHistoryService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            txtUsername.Focus();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            var user = _userService.GetUserByUsername(username);
            if (ConfigurationManager.AppSettings["EnableLDAP"] == "true" && username != "admin")
            {

                var adPath1 = ConfigurationManager.AppSettings["LDAPPath1"];
                var adPath2 = ConfigurationManager.AppSettings["LDAPPath2"];
                var domain = ConfigurationManager.AppSettings["Domain"];
                var adAuth1 = new LdapAuthentication(adPath1);
                var adAuth2 = new LdapAuthentication(adPath2);
                if (adAuth1.IsAuthenticated(domain, username, password))
                {
                    if (user != null)
                    {
                        UserSession.CreateSession(user);
                        GetInformationClient();
                        Session["UserName"] = user.Username;
                        FormsAuthentication.RedirectFromLoginPage(user.Username, false);

                        if (Session["ReturnURL"] != null)
                        {
                            var returnUrl = Session["ReturnURL"].ToString();
                            Session.Remove("ReturnURL");
                            Response.Redirect("~" + returnUrl);
                        }
                        else
                        {
                            if (this.roleService.IsNipi(UserSession.Current.RoleId))
                            {
                                Response.Redirect("~/EMDR.aspx");
                            }
                            else
                            {
                                Response.Redirect("~/Controls/Scope/ScopeProjectList.aspx");
                            }
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Please. Contact admin to register your account. <br/> Admin: " + ConfigurationManager.AppSettings["EmailAdmin"];

                    }
                }
                else if (adAuth2.IsAuthenticated(domain, username, password))
                {
                    if (user != null)
                    {
                        UserSession.CreateSession(user);

                        GetInformationClient();
                        Session["UserName"] = user.Username;
                        FormsAuthentication.RedirectFromLoginPage(user.Username, false);

                        if (Session["ReturnURL"] != null)
                        {
                            var returnUrl = Session["ReturnURL"].ToString();
                            Session.Remove("ReturnURL");
                            Response.Redirect("~" + returnUrl);
                        }
                        else
                        {
                            if (this.roleService.IsNipi(UserSession.Current.RoleId))
                            {
                                Response.Redirect("~/EMDR.aspx");
                            }
                            else
                            {
                                Response.Redirect("~/Controls/Scope/ScopeProjectList.aspx");
                            }
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Please. Contact admin to register your account.<br/> Admin: " + ConfigurationManager.AppSettings["EmailAdmin"];

                    }
                }
                else
                {
                    lblMessage.Text = "Username or password is incorrect. ";
                }
            }
            else
            {
                if (user != null && user.Password == Utility.GetMd5Hash(password))
                {
                    UserSession.CreateSession(user);

                    GetInformationClient();
                    Session["UserName"] = user.Username;
                    FormsAuthentication.RedirectFromLoginPage(user.Username, false);

                    if (Session["ReturnURL"] != null)
                    {
                        var returnUrl = Session["ReturnURL"].ToString();
                        Session.Remove("ReturnURL");
                        Response.Redirect("~" + returnUrl);
                    }
                    else
                    {
                        if(this.roleService.IsNipi(UserSession.Current.RoleId))
                        {
                            Response.Redirect("~/EMDR.aspx");
                        }
                        else
                        {
                            Response.Redirect("~/Controls/Scope/ScopeProjectList.aspx");
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "Username or password is incorrect. ";
                }
            }
        }
        private void GetInformationClient()
        {

            var userlogin = new UsersLoginHistory();

            var ip = Request.ServerVariables["REMOTE_HOST"];
            userlogin.IpAddress = this.IpAddress.Value.ToString().Contains(ip) ? this.IpAddress.Value : ip + ", " + this.IpAddress.Value;

            var hostname = ""; var domain = "";
            try
            {
                IPHostEntry hostinfor = new IPHostEntry();
                hostinfor = Dns.Resolve(ip);
                hostname = hostinfor.HostName;
                domain = hostname;
            }
            catch { }


            var language = Request.UserLanguages[0].ToLowerInvariant().Trim();
            var culture = System.Globalization.CultureInfo.CreateSpecificCulture(language);


            var os = this.Os.Value;
            var getBrowser = this.Browser.Value;
            var localtime = this.LocalTime.Value.Split(':');
            var timeZone = this.TimeZone.Value;
            var localdate = this.LocalDate.Value.Split('/');

            // var sizeRam = this.Memory.Value;
            userlogin.UserName = UserSession.Current.User.Username;
            userlogin.FullName = UserSession.Current.User.FullName;
            userlogin.ServerTime = DateTime.Now;

            //userlogin.LocalTime = new DateTime(Convert.ToInt32(localdate[2]), Convert.ToInt32(localdate[1]), Convert.ToInt32(localdate[0]), Convert.ToInt32(localtime[0]), Convert.ToInt32(localtime[1]), Convert.ToInt32(localtime[2]), Convert.ToInt32(localtime[3]));
            userlogin.LocalTime = DateTime.Now;
            userlogin.LocalTimeZone = timeZone;




            userlogin.WindownDomainUser = domain;
            userlogin.HostNameComputer = hostname;
            userlogin.Browser = getBrowser;
            userlogin.OSDetail = os;
            userlogin.LanguageFormat = culture.NativeName;
            userlogin.IsOn = true;


            this._UserLoginHistoryService.Insert(userlogin);
            UserSession.Current.LogId = userlogin.ID;
            Session["LogID"] = userlogin.ID;
        }
        private bool CheckUserLogined(User user)
        {
            var tracking = this._UserLoginHistoryService.GetUserByUsername(user.Username, true);
            if (tracking != null)
            {
                if (user.IsAdmin.GetValueOrDefault() || user.IsDC.GetValueOrDefault())
                {
                    return true;
                }
                else
                {
                    Session.Clear();
                    FormsAuthentication.SignOut();
                    lblMessage.Text = "User (" + user.Username + ") is logged on " + tracking.HostNameComputer + " computer.";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }


    }
}
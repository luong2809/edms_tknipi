﻿
namespace EDMs.Data.Entities
{
    /// <summary>
    /// The package.
    /// </summary>
    public partial class Area
    {
        public string FullName
        {
            get
            {
                return !string.IsNullOrEmpty(this.Description) 
                        ? this.Name + " - " + this.Description 
                        : this.Name;
            }
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkGroupEditForm.aspx.cs" Inherits="EDMs.Web.Controls.Scope.WorkGroupEditForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, form {
            overflow: visible;
        }

        .RadComboBoxDropDown_Default .rcbHovered {
            background-color: #46A3D3;
            color: #fff;
        }

        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
            margin: 0 0px;
        }

        .RadComboBox .rcbInputCell .rcbInput {
            border-left-color: #46A3D3 !important;
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 283px;
        }

        .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
            padding-left: 0px !important;
        }

        div.rgEditForm label {
            float: right;
            text-align: right;
            width: 72px;
        }

        .rgEditForm {
            text-align: right;
        }

        .RadComboBox {
            border-bottom-style: none !important;
            border-bottom-color: inherit !important;
            border-bottom-width: medium;
        }

        .RadUpload .ruFileWrap {
            overflow: visible !important;
        }

        .styleIcon {
            margin-left: -23px;
            margin-top: 3px;
        }

        .riTextBox[type="text"] {
            padding-right: 21px !important;
        }

    </style>

    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }


    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
            <div style="width: 100%;">
                <ul style="list-style-type: none;">
                    <div class="qlcbFormItem">
                        <div class="dnnFormMessage dnnFormInfo">
                            <div class="dnnFormItem dnnFormHelp dnnClear">
                                <p class="dnnFormRequired" style="float: left;">
                                    <span style="text-decoration: underline;">Notes</span>: All fields marked with a red are required.
                                </p>
                                <br />
                            </div>
                        </div>
                    </div>
                </ul>
            </div>

            <div style="width: 100%" runat="server" id="divContent">
                <ul style="list-style-type: none">
                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Project
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <telerik:RadComboBox ID="ddlProject" runat="server"
                                    CssClass="min25Percent" Width="300px" AutoPostBack="True" AllowCustomText="True" MarkFirstMatch="true" Skin="Windows7" OnItemDataBound="ddlProject_ItemDataBound"
                                    OnSelectedIndexChanged="ddlProject_OnSelectedIndexChanged" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>
                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Area
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:DropDownList ID="ddlArea" runat="server" CssClass="min25Percent" Width="315px" OnSelectedIndexChanged="ddlArea_OnSelectedIndexChanged" AutoPostBack="True" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Discipline
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:DropDownList ID="ddlDiscipline" runat="server" CssClass="min25Percent" Width="315px" OnSelectedIndexChanged="ddlDiscipline_OnSelectedIndexChanged" AutoPostBack="True" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Department
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divDepartment">
                                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="min25Percent qlcbFormRequired" Width="315px" OnSelectedIndexChanged="ddlDepartment_OnSelectedIndexChanged" AutoPostBack="True" />
                                <asp:CustomValidator runat="server" ID="departmentValidate" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                    OnServerValidate="departmentValidate_ServerValidate" Display="Dynamic" ControlToValidate="ddlDepartment" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Number
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divFileName">
                                <asp:TextBox ID="txtName" runat="server" Style="width: 283px; padding-right: 21px !important;" CssClass="min25Percent qlcbFormRequired" />
                                <asp:ImageButton ID="btnAutoGenerate" runat="server" ImageUrl="~/Images/autogenerate.png" ToolTip="Re-generate Workpackage number" OnClick="btnAutoGenerate_Click" ImageAlign="AbsMiddle" CssClass="styleIcon" />
                                <asp:CustomValidator runat="server" ID="fileNameValidator" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                    OnServerValidate="ServerValidationFileNameIsExist" Display="Dynamic" ControlToValidate="txtName" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px; display: none;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Revision
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:DropDownList ID="ddlRevision" runat="server" CssClass="min25Percent" Width="150px" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px; padding-top: 5px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Name
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="Description">
                                <asp:TextBox ID="txtDescription" runat="server" Style="width: 300px;" CssClass="min25Percent qlcbFormRequired" TextMode="MultiLine" Rows="7" />
                                <asp:CustomValidator runat="server" ID="DescriptionValidator" ValidateEmptyText="true" CssClass="dnnFormMessage dnnFormErrorModule"
                                    OnServerValidate="DescriptionValidator_ServerValidate" Display="Dynamic" ControlToValidate="txtDescription" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Workpackage type
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:DropDownList ID="ddlWorkpackageType" runat="server" CssClass="min25Percent" Style="width: 315px; max-width: 315px">
                                    <asp:ListItem Text="" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Planned" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Unplanned" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Weight
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divWeight">
                                <telerik:RadNumericTextBox Type="Percent" ID="txtWeight" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="70px" CssClass="min25Percent qlcbFormRequired">
                                    <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSeparator="" PositivePattern="n %" />
                                    <EnabledStyle HorizontalAlign="Right" />
                                </telerik:RadNumericTextBox>
                                <asp:CustomValidator runat="server" ID="WeightValidator" ValidateEmptyText="true" CssClass="dnnFormMessage dnnFormErrorModule"
                                    OnServerValidate="WeightValidator_ServerValidate" Display="Dynamic" ControlToValidate="txtWeight" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Complete
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <telerik:RadNumericTextBox Type="Percent" ID="txtComplete" MaxValue="100" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="70px" CssClass="min25Percent"
                                    OnTextChanged="txtComplete_OnTextChanged" AutoPostBack="True">
                                    <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSeparator="" PositivePattern="n %" />
                                    <EnabledStyle HorizontalAlign="Right" />
                                </telerik:RadNumericTextBox>
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Total man hours
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divTotalManHours">
                                <telerik:RadNumericTextBox Type="Number" ID="txtTotalManHours" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="70px" CssClass="min25Percent" OnTextChanged="txtTotalManHours_OnTextChanged" AutoPostBack="True">
                                    <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSeparator="" PositivePattern="n" />
                                    <EnabledStyle HorizontalAlign="Right" />
                                </telerik:RadNumericTextBox>
                                <%--<asp:CustomValidator runat="server" ID="TotalManHoursValidator" ValidateEmptyText="true" CssClass="dnnFormMessage dnnFormErrorModule"
                                OnServerValidate="TotalManHoursValidator_ServerValidate" Display="Dynamic" ControlToValidate="txtTotalManHours" />--%>
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Used man hours
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <telerik:RadNumericTextBox Type="Number" ID="txtUsedManHours" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="70px" CssClass="min25Percent" AutoPostBack="True">
                                    <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSeparator="" PositivePattern="n" />
                                    <EnabledStyle HorizontalAlign="Right" />
                                </telerik:RadNumericTextBox>

                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>
                    <asp:Panel ID="pnCost" runat="server">
                        <li style="width: 500px;">
                            <div>
                                <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                    <span style="color: #2E5689; text-align: right;">Total Cost Estimate
                                    </span>
                                </label>
                                <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="div2">
                                    <telerik:RadNumericTextBox Type="Number" ID="txtTotalCostEstimate" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="150px" CssClass="min25Percent" AutoPostBack="True">
                                        <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSizes="3" GroupSeparator="," DecimalSeparator="." PositivePattern="n" />
                                        <EnabledStyle HorizontalAlign="Right" />
                                    </telerik:RadNumericTextBox>
                                </div>
                            </div>
                            <div style="clear: both; font-size: 0;"></div>
                        </li>
                        <li style="width: 500px;">
                            <div>
                                <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                    <span style="color: #2E5689; text-align: right;">Project Planning Cost
                                    </span>
                                </label>
                                <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="div3">
                                    <telerik:RadNumericTextBox Type="Number" ID="txtProjectPlanningCost" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="150px" CssClass="min25Percent" AutoPostBack="True">
                                        <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSizes="3" GroupSeparator="," DecimalSeparator="." PositivePattern="n" />
                                        <EnabledStyle HorizontalAlign="Right" />
                                    </telerik:RadNumericTextBox>
                                </div>
                            </div>
                            <div style="clear: both; font-size: 0;"></div>
                        </li>
                        <li style="width: 500px;">
                            <div>
                                <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                    <span style="color: #2E5689; text-align: right;">Design Cost
                                    </span>
                                </label>
                                <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="div4">
                                    <telerik:RadNumericTextBox Type="Number" ID="txtDesignCost" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="150px" CssClass="min25Percent" AutoPostBack="True">
                                        <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSizes="3" GroupSeparator="," DecimalSeparator="." PositivePattern="n" />
                                        <EnabledStyle HorizontalAlign="Right" />
                                    </telerik:RadNumericTextBox>
                                </div>
                            </div>
                            <div style="clear: both; font-size: 0;"></div>
                        </li>
                    </asp:Panel>
                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;"></span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="div1">
                                <asp:CheckBox ID="cbAutoCalculate" runat="server" Text="Auto calculate" Checked="true"
                                    OnCheckedChanged="cbAutoCalculate_OnCheckedChanged" AutoPostBack="True" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>
                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Start Date
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divStartDate">
                                <telerik:RadDatePicker ID="txtStartDate" runat="server"
                                    ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                    <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormRequired" />
                                </telerik:RadDatePicker>
                                <asp:CustomValidator runat="server" ID="ValidatorStartDate" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                    OnServerValidate="ValidatorStartDate_ServerValidate" Display="Dynamic" ControlToValidate="txtStartDate" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Deadline
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divDeadline">
                                <telerik:RadDatePicker ID="txtDeadline" runat="server"
                                    ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                    <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormRequired" />
                                </telerik:RadDatePicker>
                                <asp:CustomValidator runat="server" ID="ValidatorDeadline" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                    OnServerValidate="ValidatorDeadline_ServerValidate" Display="Dynamic" ControlToValidate="txtDeadline" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Finish Date
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <telerik:RadDatePicker ID="txtEndDate" runat="server"
                                    ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                    <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                </telerik:RadDatePicker>
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>


                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Input data supplier
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:TextBox ID="txtInputDataSupplier" runat="server" Style="width: 300px;" CssClass="min25Percent" TextMode="MultiLine" Rows="5" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Input document
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:TextBox ID="txtInputDocument" runat="server" Style="width: 300px;" CssClass="min25Percent" TextMode="MultiLine" Rows="5" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Information exchange
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:TextBox ID="txtInfoExchange" runat="server" Style="width: 300px;" CssClass="min25Percent" TextMode="MultiLine" Rows="5" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Report mode
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:TextBox ID="txtReportMode" runat="server" Style="width: 300px;" CssClass="min25Percent" TextMode="MultiLine" Rows="5" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Incomming No.
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:TextBox ID="txtIncomingNo" runat="server" Style="width: 300px;" CssClass="min25Percent" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>

                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Outgoing No.
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:TextBox ID="txtOutgoingNo" runat="server" Style="width: 300px;" CssClass="min25Percent" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>
                    <li style="width: 100%;" runat="server" id="UploadControl">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Upload files
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divFileUpload">
                                <telerik:RadAsyncUpload runat="server" ID="docuploader"
                                    MultipleFileSelection="Disabled" TemporaryFileExpiration="05:00:00"
                                    EnableInlineProgress="true" Width="350px"
                                    Localization-Cancel="Cancel" CssClass="min25Percent qlcbFormRequired"
                                    Localization-Remove="Remove" Localization-Select="Select" Skin="Windows7">
                                </telerik:RadAsyncUpload>
                                <asp:CustomValidator runat="server" ID="fileUploadValidator" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                                    OnServerValidate="fileUploadValidator_ServerValidate" Display="Dynamic" />
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>


                    <div class="qlcbFormItem" runat="server" id="CreatedInfo" visible="False">
                        <div class="dnnFormMessage dnnFormInfo">
                            <div class="dnnFormItem dnnFormHelp dnnClear">
                                <p class="dnnFormRequired" style="float: left;">
                                    <asp:Label ID="lblCreated" runat="server"></asp:Label>
                                    <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                                </p>
                                <br />
                            </div>
                        </div>
                    </div>
                </ul>
            </div>

            <div style="width: 100%" runat="server" id="divExecuted">
                <ul style="list-style-type: none">
                    <li style="width: 500px; padding-top: 3px; padding-bottom: 3px; text-align: center">
                        <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" Width="70px" Style="text-align: center">
                            <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                        </telerik:RadButton>
                        <%--<telerik:RadButton ID="btncancel" runat="server" Text="Cancel" Width="70px" style="text-align: center"
                        OnClick="btncancel_Click">
                        <Icon PrimaryIconUrl="../../Images/Cancel.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>--%>
                    </li>
                </ul>
                <ul>
                    <li style="width: 100%;" runat="server" id="blockError" visible="False">
                        <div>
                            <label style="width: 60px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: red; text-align: right;">Warning:
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:Label runat="server" ID="lblError" Width="500"></asp:Label>
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>
                </ul>
            </div>
        <asp:HiddenField runat="server" ID="docUploadedIsExist" />
        <asp:HiddenField runat="server" ID="docIdUpdateUnIsLeaf" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Width="100%" Height="100px">
            <asp:Image ID="Image1" runat="server" Width="160px" Height="20px" ImageUrl="~/Images/loading1.gif"></asp:Image>
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" />
                        <telerik:AjaxUpdatedControl ControlID="divExecuted" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <%--  <telerik:AjaxSetting AjaxControlID="ddlDepartment">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtName" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
                <telerik:AjaxSetting AjaxControlID="ddlProject">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtName" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlArea">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtName" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlDiscipline">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtName" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ddlDepartment" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnAutoGenerate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtName" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="cbAutoCalculate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtComplete" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="txtTotalManHours">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtUsedManHours" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="txtComplete">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtUsedManHours" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;
                function OnClientFilesUploaded(sender, args) {
                    var name = args.get_fileName();
                    document.getElementById("txtName").value = name;
                    $find('<%=ajaxDocument.ClientID %>').ajaxRequest();
                }

                function pageLoad() {
                    ajaxManager = $find("<%=ajaxDocument.ClientID %>");
                }

                function ValidateForm() {
                    var name = document.getElementById("<%= txtName.ClientID%>");
                    if (name.value.trim() == "") {
                        alert("Please enter file name.");
                        name.focus();
                        return false;
                    }
                }

                function fileUploading(sender, args) {
                    var name = args.get_fileName();
                    document.getElementById("txtName").value = name;

                    ajaxManager.ajaxRequest("CheckFileName$" + name);
                }

            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>

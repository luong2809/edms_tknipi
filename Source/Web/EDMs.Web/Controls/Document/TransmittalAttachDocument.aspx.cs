﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Scope;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using OfficeHelper.Utilities.Data;

    using Telerik.Web.UI;
    using System.Net.Mail;
    using System.Text;
    using System.Net;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class TransmittalAttachDocument : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly TransmittalService transmittalService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The group data permission service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly DocumentPackageService documentPackageService;

        private readonly WorkGroupService workGroupService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly FolderService folderService;

        private readonly AttachFilesPackageService attachFilesPackageService;

        private readonly TemplateManagementService templateManagementService;

        //private readonly ToDoListService todolistService;
        private readonly AttachResponseFileService attachresponseFileService;
        private readonly ProjectAppendixService projectAppendixService;
        private readonly RoleService roleService = new RoleService();
        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        private readonly int TransmittalFolderId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TransFolderId"));

        /// <summary>
        /// Initializes a new instance of the <see cref="TransmittalAttachDocument"/> class.
        /// </summary>
        public TransmittalAttachDocument()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.receivedFromService = new ReceivedFromService();
            this.transmittalService = new TransmittalService();
            this.categoryService = new CategoryService();
            this.userService = new UserService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.documentPackageService = new DocumentPackageService();
            this.workGroupService = new WorkGroupService();
            this.scopeProjectService = new ScopeProjectService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.folderService = new FolderService();
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.templateManagementService = new TemplateManagementService();
            this.userDataPermissionService = new UserDataPermissionService();
            ///this.todolistService = new ToDoListService();
            this.attachresponseFileService = new AttachResponseFileService();
            this.projectAppendixService = new ProjectAppendixService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.Page.IsPostBack)
            {
                this.LoadComboData();
                if (ddlType.SelectedValue == "1")
                {
                    pnPage.Visible = false;
                }
                else
                {
                    pnPage.Visible = true;
                }
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
            else if (e.Argument.Contains("delete"))
            {
                var id = e.Argument.Split('_').ToList()[1];
                var docId = Convert.ToInt32(id);
                var temp = this.attachDocToTransmittalService.GetById(docId);
                if (temp != null)
                {
                    this.attachDocToTransmittalService.Delete(temp);
                    this.rgdTransDocumenlist.Rebind();
                }
            }
            else if (e.Argument == "Closing")
            {
                #region Hiden
                //if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                //{
                //    var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                //    var objTran = this.transmittalService.GetById(tranId);
                //    var listSelectedDocId = new List<int>();
                //    var listAttachFilePackage = new List<AttachFilesPackage>();

                //    if (objTran != null)
                //    {
                //        var count = 1;
                //        var filePath = Server.MapPath("../../Transmittals") + @"\";
                //        var workbook = new Workbook();

                //        var templateManagement = this.templateManagementService.GetSpecial(4, 8);
                //        if (templateManagement != null)
                //        {
                //            if (Request.QueryString["type"] == "1")
                //            {
                //                foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                //                {
                //                    var cboxSelected =
                //                        (System.Web.UI.WebControls.CheckBox)
                //                            item["IsSelected"].FindControl("cboxSelectDocTransmittal");
                //                    if (cboxSelected.Checked)
                //                    {
                //                        var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));
                //                        listSelectedDocId.Add(docId);

                //                        var attachDoc = new AttachDocToTransmittal()
                //                        {
                //                            TransmittalId = tranId,
                //                            DocumentId = docId
                //                        };

                //                        if (!this.attachDocToTransmittalService.IsExist(tranId, docId))
                //                        {
                //                            this.attachDocToTransmittalService.Insert(attachDoc);
                //                        }

                //                        cboxSelected.Checked = false;
                //                    }
                //                }
                //                var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                //                foreach (var item in attachDocList)
                //                {
                //                    var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                //                    if (docObj != null)
                //                    {


                //                        docObj.IncomingTransNo = objTran.TransmittalNumber;
                //                        docObj.IncomingTransDate = objTran.IssuseDate;

                //                        this.documentPackageService.Update(docObj);
                //                    }


                //                }
                //            }
                //            else if (Request.QueryString["type"] == "2")
                //            {
                //                var listdocTrans = this.attachDocToTransmittalService.GetAllByTransId(objTran.ID);
                //                if (listdocTrans.Count>0)
                //                {
                //                    foreach (var item in listdocTrans) {

                //                        var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                //                        if (docObj != null)
                //                        {
                //                            var selectitemstep = this.ddlStepDefinition.SelectedItem;
                //                            var duration = this.txtDuration.Value != null ? Convert.ToInt32(this.txtDuration.Value) : 0;
                //                            if(selectitemstep != null){
                //                                if (selectitemstep.Text == "Response")
                //                            {
                //                                docObj.OutgoingTransNo = objTran.TransmittalNumber;
                //                                docObj.OutgoingTransDate = objTran.IssuseDate;
                //                                docObj.DealineComment = objTran.IssuseDate.GetValueOrDefault().AddDays(Convert.ToInt32(duration));
                //                                docObj.StatusName = selectitemstep.Text;
                //                            }
                //                            else 
                //                            {
                //                                docObj.ResponseTransNo = objTran.TransmittalNumber;
                //                                docObj.ResponseDate = objTran.IssuseDate;
                //                                docObj.ResponseDeadline = objTran.IssuseDate.GetValueOrDefault().AddDays(Convert.ToInt32(duration));
                //                            }}
                //                            else
                //                            {
                //                                docObj.OutgoingTransNo = !string.IsNullOrEmpty(docObj.OutgoingTransNo) ? docObj.OutgoingTransNo : objTran.TransmittalNumber;
                //                                docObj.OutgoingTransDate = !string.IsNullOrEmpty(docObj.OutgoingTransNo) ? docObj.OutgoingTransDate : objTran.IssuseDate;
                //                            }
                //                            this.documentPackageService.Update(docObj);
                //                        }
                //                    }


                //                    var transInfo = new DataTable();
                //                    var docDt = new DataTable();
                //                    var ds = new DataSet();
                //                    var listColumn = new DataColumn[]
                //            {
                //                new DataColumn("Index", Type.GetType("System.String")),
                //                new DataColumn("DocumentNumber", Type.GetType("System.String")),
                //                new DataColumn("RevisionName", Type.GetType("System.String")),
                //                new DataColumn("Description", Type.GetType("System.String")),
                //                new DataColumn("Remark", Type.GetType("System.String"))
                //            };
                //                    var listColumn1 = new[]
                //                {
                //                    new DataColumn("IssuseDate", Type.GetType("System.String")),
                //                    new DataColumn("FromList", Type.GetType("System.String")),
                //                    new DataColumn("ContractorName", Type.GetType("System.String")),
                //                    new DataColumn("ToList", Type.GetType("System.String")),
                //                    new DataColumn("ProjectName", Type.GetType("System.String")),
                //                    new DataColumn("TransmittalNumber", Type.GetType("System.String")),
                //                    new DataColumn("DcName", Type.GetType("System.String")),
                //                    new DataColumn("DcEmail", Type.GetType("System.String")),
                //                };
                //                    docDt.Columns.AddRange(listColumn);
                //                    transInfo.Columns.AddRange(listColumn1);

                //                    var from = this.userService.GetByID(objTran.FromId.GetValueOrDefault());
                //                    var to = this.userService.GetByID(objTran.ToId.GetValueOrDefault());

                //                    var infoItem = transInfo.NewRow();
                //                    infoItem["IssuseDate"] = objTran.IssuseDate != null ? objTran.IssuseDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                //                    infoItem["FromList"] = from.FullName;
                //                    infoItem["ContractorName"] = objTran.ContractorName;
                //                    infoItem["ToList"] = to.FullName;
                //                    infoItem["ProjectName"] = objTran.ProjectName;
                //                    infoItem["TransmittalNumber"] = objTran.TransmittalNumber;
                //                    infoItem["DcName"] = from.FullName;
                //                    infoItem["DcEmail"] = from.Email;
                //                    transInfo.Rows.Add(infoItem);

                //                    count = 0;


                //                    var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                //                    foreach (var item in attachDocList)
                //                    {
                //                        var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                //                        if (docObj != null)
                //                        {
                //                            var dataItem = docDt.NewRow();
                //                            count += 1;
                //                            dataItem["Index"] = count;
                //                            dataItem["DocumentNumber"] = docObj.DocNo;
                //                            dataItem["RevisionName"] = docObj.RevisionName;
                //                            dataItem["Description"] = docObj.DocTitle;

                //                            dataItem["Remark"] = "1 Bộ";

                //                            docDt.Rows.Add(dataItem);


                //                        }
                //                    }

                //                    ds.Tables.Add(docDt);
                //                    ds.Tables[0].TableName = "Table";

                //                    var rootPath = Server.MapPath("../../Transmittals/");
                //                    const string WordPath = @"Template\";
                //                    const string WordPathExport = @"Generated\";
                //                    var StrTemplateFileName = templateManagement.FilePath;
                //                    var strOutputFileName = Utility.RemoveSpecialCharacter(objTran.TransmittalNumber) + "_" +
                //                                            DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                //                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                //                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName,
                //                        transInfo, ds);

                //                    if (isSuccess)
                //                    {

                //                        if (File.Exists(Server.MapPath(objTran.GeneratePath)))
                //                        {
                //                            File.Delete(Server.MapPath(objTran.GeneratePath));
                //                        }

                //                        var serverPath = "../../Transmittals/Generated/";
                //                        objTran.GeneratePath = serverPath + strOutputFileName;
                //                        objTran.IsGenerate = true;

                //                        this.transmittalService.Update(objTran);
                //                    }
                //                }

                //                // Auto create transmittal folder on Document library and share document files
                //                int type = Request.QueryString["type"] == "2" ? 7 : 6;
                //                var templateManagement1 = this.templateManagementService.GetSpecial(type, (int)objTran.ProjectId);

                //                var mainTransFolder = this.folderService.GetById(templateManagement1.TransFolderId.GetValueOrDefault());

                //                try
                //                {
                //                    var usersInPermissionOfParent = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(mainTransFolder.ID));

                //                    var transFolderId = 0;
                //                    var pdfFolderId = 0;
                //                    var nativeFilesFolderId = 0;

                //                    // Add new transmittal folder
                //                    var transfolder =
                //                        this.folderService.GetByDirName(mainTransFolder.DirName + "/" +
                //                                    Utility.RemoveSpecialCharacter(objTran.TransmittalNumber));
                //                    var pdfFolder = new Folder();
                //                    var nativeFilesFolder = new Folder();
                //                    if (transfolder == null)
                //                    {
                //                        transfolder = new Folder()
                //                        {
                //                            Name = objTran.TransmittalNumber,
                //                            ParentID = mainTransFolder.ID,
                //                            ProjectId = mainTransFolder.ProjectId,
                //                            DirName =
                //                                mainTransFolder.DirName + "/" +
                //                                Utility.RemoveSpecialCharacter(objTran.TransmittalNumber),
                //                            CreatedBy = UserSession.Current.User.Id,
                //                            CreatedDate = DateTime.Now
                //                        };

                //                        Directory.CreateDirectory(Server.MapPath(transfolder.DirName));
                //                        transFolderId = this.folderService.Insert(transfolder).GetValueOrDefault();

                //                        // Inherit permission from Parent folder
                //                        foreach (var parentPermission in usersInPermissionOfParent)
                //                        {
                //                            var childPermission = new UserDataPermission()
                //                            {
                //                                CategoryId = parentPermission.CategoryId,
                //                                RoleId = parentPermission.RoleId,
                //                                FolderId = transFolderId,
                //                                UserId = parentPermission.UserId,
                //                                IsFullPermission = parentPermission.IsFullPermission,
                //                                CreatedDate = DateTime.Now,
                //                                CreatedBy = UserSession.Current.User.Id
                //                            };

                //                            this.userDataPermissionService.Insert(childPermission);
                //                        }

                //                    }
                //                    else
                //                    {
                //                        transFolderId = transfolder.ID;
                //                    }

                //                    pdfFolder = this.folderService.GetByDirName(transfolder.DirName + "/PDF");
                //                    nativeFilesFolder = this.folderService.GetByDirName(transfolder.DirName + "/Native Files");

                //                    if (pdfFolder == null)
                //                    {
                //                        pdfFolder = new Folder()
                //                        {
                //                            Name = "PDF",
                //                            ParentID = transFolderId,
                //                            ProjectId = transfolder.ProjectId,
                //                            DirName = transfolder.DirName + "/PDF",
                //                            CreatedBy = UserSession.Current.User.Id,
                //                            CreatedDate = DateTime.Now
                //                        };
                //                        Directory.CreateDirectory(Server.MapPath(pdfFolder.DirName));
                //                        pdfFolderId = this.folderService.Insert(pdfFolder).GetValueOrDefault();

                //                        // Inherit permission from Parent folder
                //                        foreach (var parentPermission in usersInPermissionOfParent)
                //                        {
                //                            var childPermission = new UserDataPermission()
                //                            {
                //                                CategoryId = parentPermission.CategoryId,
                //                                RoleId = parentPermission.RoleId,
                //                                FolderId = pdfFolderId,
                //                                UserId = parentPermission.UserId,
                //                                IsFullPermission = parentPermission.IsFullPermission,
                //                                CreatedDate = DateTime.Now,
                //                                CreatedBy = UserSession.Current.User.Id
                //                            };

                //                            this.userDataPermissionService.Insert(childPermission);
                //                        }
                //                    }
                //                    else
                //                    {
                //                        pdfFolderId = pdfFolder.ID;


                //                    }

                //                    if (nativeFilesFolder == null)
                //                    {
                //                        nativeFilesFolder = new Folder()
                //                        {
                //                            Name = "Native Files",
                //                            ParentID = transFolderId,
                //                            ProjectId = transfolder.ProjectId,
                //                            DirName = transfolder.DirName + "/Native Files",
                //                            CreatedBy = UserSession.Current.User.Id,
                //                            CreatedDate = DateTime.Now
                //                        };
                //                        Directory.CreateDirectory(Server.MapPath(nativeFilesFolder.DirName));
                //                        nativeFilesFolderId = this.folderService.Insert(nativeFilesFolder).GetValueOrDefault();
                //                        // Inherit permission from Parent folder
                //                        foreach (var parentPermission in usersInPermissionOfParent)
                //                        {
                //                            var childPermission = new UserDataPermission()
                //                            {
                //                                CategoryId = parentPermission.CategoryId,
                //                                RoleId = parentPermission.RoleId,
                //                                FolderId = nativeFilesFolderId,
                //                                UserId = parentPermission.UserId,
                //                                IsFullPermission = parentPermission.IsFullPermission,
                //                                CreatedDate = DateTime.Now,
                //                                CreatedBy = UserSession.Current.User.Id
                //                            };

                //                            this.userDataPermissionService.Insert(childPermission);
                //                        }
                //                    }
                //                    else
                //                    {
                //                        nativeFilesFolderId = nativeFilesFolder.ID;

                //                    }


                //                    foreach (var docId in listSelectedDocId)
                //                    {
                //                        var todoitem = this.documentPackageService.GetById(docId);
                //                        var docintrans = this.attachDocToTransmittalService.GetByDoc(objTran.ID, docId);
                //                        if (todoitem != null)
                //                        {
                //                            if (docintrans != null && docintrans.StepStatus == "Response")
                //                            {
                //                                var reponsefile = this.attachresponseFileService.GetAllByDocId(docId).FirstOrDefault(t => t.IsDefault.GetValueOrDefault());
                //                                if(reponsefile != null){ // Path file to save on server disc
                //                                    var saveFilePath = reponsefile.Extension.ToLower() == "pdf" ? Path.Combine(Server.MapPath(pdfFolder.DirName), reponsefile.Filename) : Path.Combine(Server.MapPath(nativeFilesFolder.DirName),
                //                                reponsefile.Filename);
                //                                    // Path file to download from server
                //                                    var serverFilePath = reponsefile.Extension.ToLower() == "pdf" ? pdfFolder.DirName + "/" + reponsefile.Filename : nativeFilesFolder.DirName + "/" + reponsefile.Filename;
                //                                    var fileExt = reponsefile.Extension;
                //                                    if (File.Exists(Server.MapPath(reponsefile.FilePath)))
                //                                    {
                //                                        File.Copy(Server.MapPath(reponsefile.FilePath), saveFilePath, true);
                //                                    }
                //                                }
                //                            }
                //                            else
                //                            {
                //                                listAttachFilePackage.AddRange(
                //                               this.attachFilesPackageService.GetAllDocumentFileByDocId(docId));
                //                            }
                //                        }


                //                    }

                //                    var pdfFiles = listAttachFilePackage.Where(t => t.Extension.ToLower() == "pdf").ToList();
                //                    var nativeFiles = listAttachFilePackage.Where(t => t.Extension.ToLower() != "pdf").ToList();

                //                    foreach (var pdfFile in pdfFiles)
                //                    {
                //                        // Path file to save on server disc
                //                        var saveFilePath = Path.Combine(Server.MapPath(pdfFolder.DirName), pdfFile.FileName);
                //                        // Path file to download from server
                //                        var serverFilePath = pdfFolder.DirName + "/" + pdfFile.FileName;
                //                        var fileExt = pdfFile.Extension;

                //                        if (!File.Exists(saveFilePath))
                //                        {
                //                            var document = new Document()
                //                            {
                //                                Name = pdfFile.FileName,
                //                                FileExtension = fileExt,
                //                                FileExtensionIcon = pdfFile.ExtensionIcon,
                //                                FilePath = serverFilePath,
                //                                FolderID = pdfFolderId,
                //                                IsLeaf = true,
                //                                IsDelete = false,
                //                                CreatedBy = UserSession.Current.User.Id,
                //                                CreatedDate = DateTime.Now
                //                            };
                //                            this.documentService.Insert(document);
                //                        }

                //                        if (File.Exists(Server.MapPath(pdfFile.FilePath)))
                //                        {
                //                            File.Copy(Server.MapPath(pdfFile.FilePath), saveFilePath, true);
                //                        }


                //                    }

                //                    foreach (var nativeFile in nativeFiles)
                //                    {
                //                        // Path file to save on server disc
                //                        var saveFilePath = Path.Combine(Server.MapPath(nativeFilesFolder.DirName),
                //                            nativeFile.FileName);
                //                        // Path file to download from server
                //                        var serverFilePath = nativeFilesFolder.DirName + "/" + nativeFile.FileName;
                //                        var fileExt = nativeFile.Extension;

                //                        if (!File.Exists(saveFilePath))
                //                        {
                //                            var document = new Document()
                //                            {
                //                                Name = nativeFile.FileName,
                //                                FileExtension = fileExt,
                //                                FileExtensionIcon = nativeFile.ExtensionIcon,
                //                                FilePath = serverFilePath,
                //                                FolderID = nativeFilesFolderId,
                //                                IsLeaf = true,
                //                                IsDelete = false,
                //                                CreatedBy = UserSession.Current.User.Id,
                //                                CreatedDate = DateTime.Now
                //                            };
                //                            this.documentService.Insert(document);
                //                        }

                //                        if (File.Exists(Server.MapPath(nativeFile.FilePath)))
                //                        {
                //                            File.Copy(Server.MapPath(nativeFile.FilePath), saveFilePath, true);
                //                        }
                //                    }

                //                }
                //                catch (Exception)
                //                {

                //                }
                //            }
                //        }
                //    }

                //   // this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                //}
                #endregion
            }
        }
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var transobj = this.transmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                var projectId = transobj.ProjectId.GetValueOrDefault();
                var workgroupId = !string.IsNullOrEmpty(this.ddlWorkgroup.SelectedValue) ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue) : 0;
                var docNo = this.txtDocNo.Text.Trim();
                var docTitle = this.txtDocTitle.Text.Trim();

                var listDoc = this.documentPackageService.SearchDocument(
                    projectId,
                    workgroupId,
                    docNo,
                    docTitle);
                this.grdDocument.DataSource = listDoc;
            }
        }

        /// <summary>
        /// The rad menu_ item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        //protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        //{
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        private DateTime GetBusinessDays(DateTime start, int duration)
        {
            DateTime listdate = start;
            int i = 1;
            while (i <= duration)
            {
                listdate = listdate.AddDays(1);
                if (listdate.DayOfWeek != DayOfWeek.Saturday && listdate.DayOfWeek != DayOfWeek.Sunday) { i++; }

            }
            return listdate;
        }


        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var transobj = this.transmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                var projectId = transobj.ProjectId.GetValueOrDefault();
                var listWorkgroup = this.workGroupService.GetAllWorkGroupOfProject(projectId).OrderBy(t => t.ID).ToList();
                this.ddlWorkgroup.DataSource = listWorkgroup;
                this.ddlWorkgroup.DataTextField = "Name";
                this.ddlWorkgroup.DataValueField = "ID";
                this.ddlWorkgroup.DataBind();
            }
        }

        /// <summary>
        /// The btn save_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Session["EditStep"] != null)
            {
                var id = Convert.ToInt32(Session["EditStep"].ToString());
                var docintrans = this.attachDocToTransmittalService.GetById(id);
                if (docintrans != null)
                {
                    docintrans.StepStatus = this.ddlStepDefinition.SelectedItem != null ? this.ddlStepDefinition.SelectedValue : "";
                    docintrans.DurationStep = this.txtDuration.Value.ToString();
                    docintrans.TypeID = Convert.ToInt32(this.ddlType.SelectedValue);
                    docintrans.CopyNumber = this.txtCopyNumber.Value;
                    docintrans.OriginalNumber = this.txtOriginalNumber.Value;
                    this.attachDocToTransmittalService.Update(docintrans);
                }
                Session.Remove("EditStep");
                // this.ddlStepDefinition.SelectedValue="0";
                //  this.txtDuration.Value = null;

            }
            else
            {
                var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                var objTran = this.transmittalService.GetById(tranId);
                if (objTran != null)
                {
                    //var docId = Convert.ToInt32(this.lblDocId.Value);
                    //get id multi row selected
                    foreach (GridDataItem item in grdDocument.SelectedItems)
                    {
                        var docId = Convert.ToInt32(item.GetDataKeyValue("ID")); // Works if you set the DataKeyValue as ID 
                        if (docId != 0)
                        {
                            var attachDoc = new AttachDocToTransmittal();
                            attachDoc.TransmittalId = tranId;
                            attachDoc.DocumentId = docId;
                            attachDoc.StepStatus = this.ddlStepDefinition.SelectedItem != null ? this.ddlStepDefinition.SelectedValue : "";
                            attachDoc.DurationStep = this.txtDuration.Value.ToString();
                            attachDoc.TypeID = Convert.ToInt32(this.ddlType.SelectedValue);
                            if (this.ddlType.SelectedValue == "2" || this.ddlType.SelectedValue == "3")
                            {
                                attachDoc.CopyNumber = this.txtCopyNumber.Value;
                                attachDoc.OriginalNumber = this.txtOriginalNumber.Value;
                            }
                            if (!this.attachDocToTransmittalService.IsExist(tranId, docId))
                            {
                                this.attachDocToTransmittalService.Insert(attachDoc);
                            }
                        }
                    }
                }
            }

            this.ddlStepDefinition.SelectedIndex = 0;
            this.ddlStepDefinition.SelectedItem.Text = "";
            this.ddlType.SelectedIndex = 0;
            this.txtCopyNumber.Text = "";
            this.txtOriginalNumber.Text = "";
            this.pnPage.Visible = false;
            this.txtDuration.Value = null;
            this.rgdTransDocumenlist.Rebind();
        }

        //protected void ddlProject_SelectedIndexChange(object sender, EventArgs e)
        //{
        //    var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip;
        //    var currentDepartment = UserSession.Current.User.RoleId;
        //    var workPackageList = isSeeFull
        //        ? this.workGroupService.GetAll()
        //        : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault());
        //    workPackageList.Insert(0, new WorkGroup { ID = 0 });
        //    this.ddlWorkgroup.DataSource = workPackageList.Where(
        //                                        t =>
        //                                            t.ProjectId ==
        //                                            (!string.IsNullOrEmpty(this.ddlProject.SelectedValue)
        //                                                ? Convert.ToInt32(this.ddlProject.SelectedValue)
        //                                                : 0)); ;
        //    this.ddlWorkgroup.DataTextField = "Name";
        //    this.ddlWorkgroup.DataValueField = "ID";
        //    this.ddlWorkgroup.DataBind();

        //    this.grdDocument.Rebind();
        //}

        protected void grdDocument_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["HasAttachFile"].Text == "True")
                {
                    item.BackColor = Color.Aqua;
                    item.BorderColor = Color.Aqua;
                }
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            this.ddlStepDefinition.SelectedItem.Text = "";
            this.txtDuration.Value = null;
        }



        protected void rgdTransDocumenlist_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (Request.QueryString["tranId"] != null)
            {
                var tranId = Convert.ToInt32(Request.QueryString["tranId"]);
                var listDocument = new List<DocumentPackage>();

                var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                foreach (var item in attachDocList)
                {
                    var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                    if (docObj != null)
                    {
                        listDocument.Add(docObj);
                    }
                }

                listDocument = listDocument.OrderBy(t => t.DocNo).ToList();
                var newtable = (from A in listDocument
                                join B in attachDocList on A.ID equals B.DocumentId
                                select new
                                {
                                    ID = B.ID,
                                    DocumentId = B.DocumentId,
                                    TransId = B.TransmittalId,
                                    StepStatus = B.StepStatus,
                                    Duration = B.DurationStep,
                                    DocNo = A.DocNo,
                                    DocTitle = A.DocTitle,
                                    RevisionName = A.RevisionName,
                                    StartDate = A.StartDate,
                                    Deadline = A.Deadline,
                                    CopyNumber = B.CopyNumber,
                                    OriginalNumber = B.OriginalNumber,
                                });
                this.rgdTransDocumenlist.DataSource = newtable;
            }
        }

        protected void rgdTransDocumenlist_ItemCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            if (e.CommandName == "EditCmd")
            {
                var Id = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
                var docintrans = this.attachDocToTransmittalService.GetById(Convert.ToInt32(Id));
                if (docintrans != null)
                {

                    var DropdownList = docintrans.StepStatus;
                    if (DropdownList == null)
                    {
                        this.ddlStepDefinition.SelectedIndex = 0;
                    }
                    else if (DropdownList == "")
                    {
                        this.ddlStepDefinition.SelectedIndex = 0;
                    }
                    else if (DropdownList == "INF-Information")
                    {
                        this.ddlStepDefinition.SelectedIndex = 1;
                    }
                    else if (DropdownList == "IFR-Review")
                    {
                        this.ddlStepDefinition.SelectedIndex = 2;
                    }
                    else if (DropdownList == "Response")
                    {
                        this.ddlStepDefinition.SelectedIndex = 3;
                    }
                    else if (DropdownList == "IFA-Aproved")
                    {
                        this.ddlStepDefinition.SelectedIndex = 4;
                    }
                    else if (DropdownList == "IFC-Construction")
                    {
                        this.ddlStepDefinition.SelectedIndex = 5;
                    }
                    this.txtDuration.Value = docintrans.DurationStep != null && !string.IsNullOrEmpty(docintrans.DurationStep) ? Convert.ToInt32(docintrans.DurationStep) : 0;
                    this.ddlType.SelectedValue = docintrans.TypeID != null ? docintrans.TypeID.ToString() : "1";
                    if (ddlType.SelectedValue == "1")
                    {
                        pnPage.Visible = false;
                    }
                    else
                    {
                        pnPage.Visible = true;
                    }
                    this.txtCopyNumber.Text = docintrans.CopyNumber.ToString();
                    this.txtOriginalNumber.Text = docintrans.OriginalNumber.ToString();
                    Session.Add("EditStep", Id);
                }
            }

        }
        protected void rgdTransDocumenlist_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var temp = this.attachDocToTransmittalService.GetById(docId);
            if (temp != null)
            {
                this.attachDocToTransmittalService.Delete(temp);
            }
        }
        protected void btFinish_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                var objTran = this.transmittalService.GetById(tranId);
                var projectObj = this.scopeProjectService.GetById(objTran.ProjectId.GetValueOrDefault());
                var listSelectedDocId = new List<int>();
                var listAttachFilePackage = new List<AttachFilesPackage>();

                if (objTran != null)
                {
                    var templateManagement = this.templateManagementService.GetSpecial(4, 8);
                    if (templateManagement != null)
                    {
                        if (Request.QueryString["type"] == "1")
                        {
                            foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                            {
                                var cboxSelected =
                                    (System.Web.UI.WebControls.CheckBox)
                                        item["IsSelected"].FindControl("cboxSelectDocTransmittal");
                                if (cboxSelected.Checked)
                                {
                                    var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));
                                    listSelectedDocId.Add(docId);

                                    var attachDoc = new AttachDocToTransmittal()
                                    {
                                        TransmittalId = tranId,
                                        DocumentId = docId
                                    };

                                    if (!this.attachDocToTransmittalService.IsExist(tranId, docId))
                                    {
                                        this.attachDocToTransmittalService.Insert(attachDoc);
                                    }

                                    cboxSelected.Checked = false;
                                }
                            }
                            var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                            foreach (var item in attachDocList)
                            {
                                var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                if (docObj != null)
                                {


                                    docObj.IncomingTransNo = objTran.TransmittalNumber;
                                    docObj.IncomingTransDate = objTran.IssuseDate;

                                    this.documentPackageService.Update(docObj);
                                }
                            }
                        }
                        else if (Request.QueryString["type"] == "2")
                        {
                            var listdocTrans = this.attachDocToTransmittalService.GetAllByTransId(objTran.ID);
                            DateTime datenull;
                            if (listdocTrans.Count > 0)
                            {
                                foreach (var item in listdocTrans)
                                {
                                    var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                    if (docObj != null)
                                    {
                                        listSelectedDocId.Add(docObj.ID);

                                        var selectitemstep = item.StepStatus;
                                        var duration = item.DurationStep != null && !string.IsNullOrEmpty(item.DurationStep) ? Convert.ToInt32(item.DurationStep) : 0;
                                        if (!string.IsNullOrEmpty(selectitemstep))
                                        {
                                            if (selectitemstep == "Response")
                                            {
                                                docObj.ResponseTransNo = objTran.TransmittalNumber;
                                                docObj.ResponseDate = objTran.IssuseDate;
                                                docObj.ResponseDeadline = this.GetBusinessDays(objTran.IssuseDate.GetValueOrDefault(), duration);
                                            }
                                            else if (selectitemstep == "IFC-Construction")
                                            {
                                                docObj.OutgoingLeterNo = objTran.TransmittalNumber;
                                                docObj.OutgoingLeterDate = objTran.IssuseDate;
                                            }
                                            else
                                            {
                                                if (item.TypeID.GetValueOrDefault() == 1)
                                                {
                                                    docObj.OutgoingTransNo = objTran.TransmittalNumber;
                                                    docObj.OutgoingTransDate = objTran.IssuseDate;
                                                    if (duration != 0) { docObj.DealineComment = this.GetBusinessDays(objTran.IssuseDate.GetValueOrDefault(), duration); }
                                                    docObj.StatusName = selectitemstep;
                                                }
                                                else if (item.TypeID.GetValueOrDefault() == 2)
                                                {
                                                    docObj.OutgoingLeterNo = objTran.TransmittalNumber;
                                                    docObj.OutgoingLeterDate = objTran.IssuseDate;
                                                }
                                                else
                                                {
                                                    docObj.OutgoingTransNo = objTran.TransmittalNumber;
                                                    docObj.OutgoingTransDate = objTran.IssuseDate;
                                                    if (duration != 0) { docObj.DealineComment = this.GetBusinessDays(objTran.IssuseDate.GetValueOrDefault(), duration); }
                                                    docObj.StatusName = selectitemstep;
                                                    docObj.OutgoingLeterNo = objTran.TransmittalNumber;
                                                    docObj.OutgoingLeterDate = objTran.IssuseDate;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            docObj.OutgoingTransNo = !string.IsNullOrEmpty(docObj.OutgoingTransNo) ? docObj.OutgoingTransNo : objTran.TransmittalNumber;
                                            docObj.OutgoingTransDate = !string.IsNullOrEmpty(docObj.OutgoingTransNo) ? docObj.OutgoingTransDate : objTran.IssuseDate;
                                        }
                                        this.documentPackageService.Update(docObj);
                                    }
                                }

                                //    var transInfo = new DataTable();
                                //    var docDt = new DataTable();
                                //    var ds = new DataSet();
                                //    var listColumn = new DataColumn[]
                                //{
                                //    new DataColumn("Index", Type.GetType("System.String")),
                                //    new DataColumn("DocumentNumber", Type.GetType("System.String")),
                                //    new DataColumn("RevisionName", Type.GetType("System.String")),
                                //    new DataColumn("Description", Type.GetType("System.String")),
                                //    new DataColumn("Original", Type.GetType("System.String")),
                                //    new DataColumn("Copy", Type.GetType("System.String")),
                                //    new DataColumn("SoftCopy", Type.GetType("System.String")),
                                //    new DataColumn("Remark", Type.GetType("System.String"))
                                //};
                                //    var listColumn1 = new[]
                                //    {
                                //        new DataColumn("IssuseDate", Type.GetType("System.String")),
                                //        new DataColumn("FromList", Type.GetType("System.String")),
                                //        new DataColumn("ContractorName", Type.GetType("System.String")),
                                //        new DataColumn("ToList", Type.GetType("System.String")),
                                //        new DataColumn("ProjectName", Type.GetType("System.String")),
                                //        new DataColumn("ProjectTitle", Type.GetType("System.String")),
                                //        new DataColumn("OrderCode", Type.GetType("System.String")),
                                //        new DataColumn("NoDocument", Type.GetType("System.String")),
                                //        new DataColumn("EngManager", Type.GetType("System.String")),
                                //        new DataColumn("TransmittalNumber", Type.GetType("System.String")),
                                //        new DataColumn("DcName", Type.GetType("System.String")),
                                //        new DataColumn("DcEmail", Type.GetType("System.String")),
                                //    };
                                //    docDt.Columns.AddRange(listColumn);
                                //    transInfo.Columns.AddRange(listColumn1);
                                //    var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                                //    var from = this.userService.GetByID(objTran.FromId.GetValueOrDefault());
                                //    var to = this.userService.GetByID(objTran.ToId.GetValueOrDefault());
                                //    var projectObj = this.scopeProjectService.GetById(objTran.ProjectId.GetValueOrDefault());
                                //    var projectAppendixObj = this.projectAppendixService.GetByProject(objTran.ProjectId.GetValueOrDefault());
                                //    var infoItem = transInfo.NewRow();
                                //    infoItem["IssuseDate"] = objTran.IssuseDate != null ? objTran.IssuseDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                                //    infoItem["FromList"] = from.FullName;
                                //    infoItem["ContractorName"] = objTran.ContractorName;
                                //    infoItem["ToList"] = to.FullName;
                                //    infoItem["ProjectName"] = objTran.ProjectName;
                                //    infoItem["TransmittalNumber"] = objTran.TransmittalNumber;
                                //    infoItem["DcName"] = from.FullName;
                                //    infoItem["DcEmail"] = from.Email;
                                //    infoItem["ProjectTitle"] = projectObj.Description;
                                //    if (projectAppendixObj != null)
                                //    {
                                //        infoItem["OrderCode"] = projectAppendixObj.OrderCode.Replace("<br/>", "\n");
                                //    }
                                //    else
                                //    {
                                //        infoItem["OrderCode"] = projectObj.planName;
                                //    }
                                //    infoItem["NoDocument"] = attachDocList.Count().ToString();
                                //    infoItem["EngManager"] = projectObj.ProjectManagerFullName;
                                //    transInfo.Rows.Add(infoItem);
                                //    count = 0;
                                //    foreach (var item in attachDocList)
                                //    {
                                //        var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                //        if (docObj != null)
                                //        {
                                //            var attachFile = this.attachFilesPackageService.GetAllDocId(docObj.ID);
                                //            var nativeFile = attachFile.Any(t => t.Extension.ToLower() != "pdf");
                                //            var dataItem = docDt.NewRow();
                                //            count += 1;
                                //            dataItem["Index"] = count;
                                //            dataItem["DocumentNumber"] = docObj.DocNo;
                                //            dataItem["RevisionName"] = docObj.RevisionName;
                                //            dataItem["Description"] = docObj.DocTitle;
                                //            if (nativeFile)
                                //            {
                                //                dataItem["Remark"] = "Included Native File on SPF";
                                //            }
                                //            else
                                //            {
                                //                dataItem["Remark"] = "";
                                //            }
                                //            if (!string.IsNullOrEmpty(item.OriginalNumber.ToString()))
                                //            {
                                //                dataItem["Original"] = item.OriginalNumber;
                                //            }
                                //            if (!string.IsNullOrEmpty(item.CopyNumber.ToString()))
                                //            {
                                //                dataItem["Copy"] = item.CopyNumber;
                                //            }
                                //            if (item.TypeId.GetValueOrDefault() == 1 || item.TypeId.GetValueOrDefault() == 3)
                                //            {
                                //                dataItem["SoftCopy"] = "x";
                                //            }
                                //            docDt.Rows.Add(dataItem);
                                //        }
                                //    }
                                //    ds.Tables.Add(docDt);
                                //    ds.Tables[0].TableName = "Table";
                                //    var rootPath = Server.MapPath("../../Transmittals/");
                                //    const string WordPath = @"Template\";
                                //    const string WordPathExport = @"Generated\";
                                //    var StrTemplateFileName = templateManagement.FilePath;
                                //    var strOutputFileName = Utility.RemoveSpecialCharacter(objTran.TransmittalNumber) + "_" +
                                //                            DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                                //    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                                //        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName,
                                //        transInfo, ds);
                                //    if (isSuccess)
                                //    {
                                //        if (File.Exists(Server.MapPath(objTran.GeneratePath)))
                                //        {
                                //            File.Delete(Server.MapPath(objTran.GeneratePath));
                                //        }
                                //        var serverPath = "../../Transmittals/Generated/";
                                //        objTran.GeneratePath = serverPath + strOutputFileName;
                                //        objTran.IsGenerate = true;
                                //        this.transmittalService.Update(objTran);
                                //    }

                                var filePath = Server.MapPath("../../Transmittals/Template") + @"\";
                                var workbook = new Workbook();
                                workbook.Open(filePath + @"Transmittal_Template.xls");
                                var sheets = workbook.Worksheets;
                                var sheet = sheets[0];
                                var toDepartment = this.roleService.GetByID(objTran.ToDepartmentID.GetValueOrDefault());
                                var fromUser = this.userService.GetByID(objTran.FromId.GetValueOrDefault());
                                var projectAppendixObj = this.projectAppendixService.GetByProject(objTran.ProjectId.GetValueOrDefault());
                                var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                                int count = 1;
                                int indexRow = 21;
                                sheet.Cells["E4"].PutValue(projectObj.Name);
                                sheet.Cells["E5"].PutValue(projectObj.Description);
                                sheet.Cells["K3"].PutValue(objTran.TransmittalNumber);
                                sheet.Cells["L5"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                                //sheet.Cells["D7"].PutValue(objTran.FromList);
                                sheet.Cells["D9"].PutValue(objTran.ToList);
                                sheet.Cells["D10"].PutValue(toDepartment != null ? toDepartment.Name + "-" + toDepartment.Description : "");
                                if (projectAppendixObj != null)
                                {
                                    sheet.Cells["D11"].PutValue(projectAppendixObj.OrderCode.Replace("<br/>", "\n"));
                                }
                                else
                                {
                                    sheet.Cells["D11"].PutValue(projectObj.planName);
                                }
                                double sumOriginal = 0;
                                double sumCopy = 0;
                                int sumPdf = 0;
                                foreach (var item in attachDocList)
                                {
                                    var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                    if (docObj != null)
                                    {
                                        var attachFile = this.attachFilesPackageService.GetAllDocId(docObj.ID);
                                        var nativeFile = attachFile.Any(t => t.Extension.ToLower() != "pdf");
                                        var pdfFile = attachFile.Where(t => t.Extension.ToLower() == "pdf").ToList();
                                        sheet.Cells.InsertRow(indexRow);
                                        sheet.Cells.Merge(indexRow, 1, 1, 2);
                                        sheet.Cells.Merge(indexRow, 3, 1, 3);
                                        sheet.Cells["A" + indexRow].PutValue(count);
                                        sheet.Cells["B" + indexRow].PutValue(docObj.DocNo);
                                        sheet.Cells["D" + indexRow].PutValue(docObj.DocTitle);
                                        sheet.Cells["G" + indexRow].PutValue(docObj.RevisionName);
                                        sheet.Cells["H" + indexRow].PutValue(item.StepStatus);
                                        if (!string.IsNullOrEmpty(item.OriginalNumber.ToString()))
                                        {
                                            sheet.Cells["I" + indexRow].PutValue(item.OriginalNumber);
                                            sumOriginal += Convert.ToDouble(item.OriginalNumber);
                                        }
                                        if (!string.IsNullOrEmpty(item.CopyNumber.ToString()))
                                        {
                                            sheet.Cells["J" + indexRow].PutValue(item.CopyNumber);
                                            sumCopy += Convert.ToDouble(item.CopyNumber);
                                        }
                                        if (pdfFile.Count > 0)
                                        {
                                            sheet.Cells["K" + indexRow].PutValue(pdfFile.Count);
                                            sumPdf += pdfFile.Count;
                                        }
                                        if (nativeFile)
                                        {
                                            sheet.Cells["L" + indexRow].PutValue("Included Native File on SPF");
                                        }
                                        else
                                        {
                                            sheet.Cells["L" + indexRow].PutValue("");
                                        }
                                        count++;
                                        indexRow++;
                                    }
                                }
                                sheet.Cells["I" + (indexRow + 1)].PutValue(sumOriginal);
                                sheet.Cells["J" + (indexRow + 1)].PutValue(sumCopy);
                                sheet.Cells["K" + (indexRow + 1)].PutValue(sumPdf);
                                string noteVie = "Ký, ghi rõ họ tên vào biên bản và gửi về địa chỉ email: " + fromUser.Email + " hoặc support.edms@vietsov.com.vn";
                                string noteEng = "Please acknowledge receipt of this transmittal by signing and sent back to " + fromUser.Email + " or support.edms@vietsov.com.vn";
                                sheet.Cells["A" + (indexRow + 2)].PutValue(noteVie);
                                sheet.Cells["A" + (indexRow + 3)].PutValue(noteEng);
                                sheet.Cells["C" + (indexRow + 4)].PutValue(objTran.FromList);
                                sheet.Cells["H" + (indexRow + 4)].PutValue(projectObj.ProjectManagerFullName);
                                string strOutputFileName = Utility.RemoveSpecialCharacter(objTran.TransmittalNumber) + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
                                string str = Server.MapPath("../../Transmittals/Generated/" + strOutputFileName);
                                workbook.Save(str);

                                if (File.Exists(Server.MapPath(objTran.GeneratePath)))
                                {
                                    File.Delete(Server.MapPath(objTran.GeneratePath));
                                }
                                var serverPath = "../../Transmittals/Generated/";
                                objTran.GeneratePath = serverPath + strOutputFileName;
                                objTran.IsGenerate = true;
                                this.transmittalService.Update(objTran);
                            }

                            // Auto create transmittal folder on Document library and share document files
                            int type = Request.QueryString["type"] == "2" ? 7 : 6;
                            var templateManagement1 = this.templateManagementService.GetSpecial(type, (int)objTran.ProjectId);

                            var mainTransFolder = this.folderService.GetById(templateManagement1.TransFolderId.GetValueOrDefault());

                            try
                            {
                                var usersInPermissionOfParent = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(mainTransFolder.ID));

                                var transFolderId = 0;
                                var pdfFolderId = 0;
                                var nativeFilesFolderId = 0;

                                // Add new transmittal folder
                                var transfolder =
                                    this.folderService.GetByDirName(mainTransFolder.DirName + "/" +
                                                Utility.RemoveSpecialCharacter(objTran.TransmittalNumber));
                                var pdfFolder = new Folder();
                                var nativeFilesFolder = new Folder();
                                if (transfolder == null)
                                {
                                    transfolder = new Folder()
                                    {
                                        Name = objTran.TransmittalNumber,
                                        ParentID = mainTransFolder.ID,
                                        ProjectId = mainTransFolder.ProjectId,
                                        DirName =
                                            mainTransFolder.DirName + "/" +
                                            Utility.RemoveSpecialCharacter(objTran.TransmittalNumber),
                                        CreatedBy = UserSession.Current.User.Id,
                                        CreatedDate = DateTime.Now
                                    };

                                    Directory.CreateDirectory(Server.MapPath(transfolder.DirName));
                                    transFolderId = this.folderService.Insert(transfolder).GetValueOrDefault();

                                    // Inherit permission from Parent folder
                                    foreach (var parentPermission in usersInPermissionOfParent)
                                    {
                                        var childPermission = new UserDataPermission()
                                        {
                                            ProjectId = parentPermission.ProjectId,
                                            RoleId = parentPermission.RoleId,
                                            FolderId = transFolderId,
                                            UserId = parentPermission.UserId,
                                            IsFullPermission = parentPermission.IsFullPermission,
                                            OnlyView=parentPermission.OnlyView,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = UserSession.Current.User.Id
                                        };

                                        this.userDataPermissionService.Insert(childPermission);
                                    }

                                }
                                else
                                {
                                    transFolderId = transfolder.ID;
                                    if (!Directory.Exists(Server.MapPath(transfolder.DirName)))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(transfolder.DirName));
                                    }
                                }

                                pdfFolder = this.folderService.GetByDirName(transfolder.DirName + "/PDF");
                                nativeFilesFolder = this.folderService.GetByDirName(transfolder.DirName + "/Native Files");

                                if (pdfFolder == null)
                                {
                                    pdfFolder = new Folder()
                                    {
                                        Name = "PDF",
                                        ParentID = transFolderId,
                                        ProjectId = transfolder.ProjectId,
                                        DirName = transfolder.DirName + "/PDF",
                                        CreatedBy = UserSession.Current.User.Id,
                                        CreatedDate = DateTime.Now
                                    };
                                    Directory.CreateDirectory(Server.MapPath(pdfFolder.DirName));
                                    pdfFolderId = this.folderService.Insert(pdfFolder).GetValueOrDefault();

                                    // Inherit permission from Parent folder
                                    foreach (var parentPermission in usersInPermissionOfParent)
                                    {
                                        var childPermission = new UserDataPermission()
                                        {
                                            ProjectId = parentPermission.ProjectId,
                                            RoleId = parentPermission.RoleId,
                                            FolderId = pdfFolderId,
                                            UserId = parentPermission.UserId,
                                            IsFullPermission = parentPermission.IsFullPermission,
                                            OnlyView = parentPermission.OnlyView,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = UserSession.Current.User.Id
                                        };

                                        this.userDataPermissionService.Insert(childPermission);
                                    }
                                }
                                else
                                {
                                    pdfFolderId = pdfFolder.ID;
                                    if (!Directory.Exists(Server.MapPath(pdfFolder.DirName)))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(pdfFolder.DirName));
                                    }
                                }

                                if (nativeFilesFolder == null)
                                {
                                    nativeFilesFolder = new Folder()
                                    {
                                        Name = "Native Files",
                                        ParentID = transFolderId,
                                        ProjectId = transfolder.ProjectId,
                                        DirName = transfolder.DirName + "/Native Files",
                                        CreatedBy = UserSession.Current.User.Id,
                                        CreatedDate = DateTime.Now
                                    };
                                    Directory.CreateDirectory(Server.MapPath(nativeFilesFolder.DirName));
                                    nativeFilesFolderId = this.folderService.Insert(nativeFilesFolder).GetValueOrDefault();
                                    // Inherit permission from Parent folder
                                    foreach (var parentPermission in usersInPermissionOfParent)
                                    {
                                        var childPermission = new UserDataPermission()
                                        {
                                            ProjectId = parentPermission.ProjectId,
                                            RoleId = parentPermission.RoleId,
                                            FolderId = nativeFilesFolderId,
                                            UserId = parentPermission.UserId,
                                            IsFullPermission = parentPermission.IsFullPermission,
                                            OnlyView = parentPermission.OnlyView,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = UserSession.Current.User.Id
                                        };

                                        this.userDataPermissionService.Insert(childPermission);
                                    }
                                }
                                else
                                {
                                    nativeFilesFolderId = nativeFilesFolder.ID;
                                    if (!Directory.Exists(Server.MapPath(nativeFilesFolder.DirName)))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(nativeFilesFolder.DirName));
                                    }
                                }
                                foreach (var docId in listSelectedDocId)
                                {
                                    var todoitem = this.documentPackageService.GetById(docId);
                                    var docintrans = this.attachDocToTransmittalService.GetByDoc(objTran.ID, docId);
                                    if (todoitem != null)
                                    {
                                        if (docintrans != null && docintrans.StepStatus == "Response")
                                        {
                                            var reponsefile = this.attachresponseFileService.GetAllByDocId(docId).FirstOrDefault(t => t.IsDefault.GetValueOrDefault());
                                            if (reponsefile != null)
                                            { // Path file to save on server disc
                                                var saveFilePath = reponsefile.Extension.ToLower() == "pdf" ? Path.Combine(Server.MapPath(pdfFolder.DirName), reponsefile.Filename) : Path.Combine(Server.MapPath(nativeFilesFolder.DirName),
                                            reponsefile.Filename);
                                                // Path file to download from server
                                                var serverFilePath = reponsefile.Extension.ToLower() == "pdf" ? pdfFolder.DirName + "/" + reponsefile.Filename : nativeFilesFolder.DirName + "/" + reponsefile.Filename;
                                                var fileExt = reponsefile.Extension;
                                                if (File.Exists(Server.MapPath(reponsefile.FilePath)))
                                                {
                                                    File.Copy(Server.MapPath(reponsefile.FilePath), saveFilePath, true);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            listAttachFilePackage.AddRange(
                                           this.attachFilesPackageService.GetAllDocumentFileByDocId(docId));
                                        }
                                    }
                                }

                                var pdfFiles = listAttachFilePackage.Where(t => t.Extension.ToLower() == "pdf").ToList();
                                var nativeFiles = listAttachFilePackage.Where(t => t.Extension.ToLower() != "pdf").ToList();

                                foreach (var pdfFile in pdfFiles)
                                {
                                    // Path file to save on server disc
                                    var saveFilePath = Path.Combine(Server.MapPath(pdfFolder.DirName), pdfFile.FileName);
                                    // Path file to download from server
                                    var serverFilePath = pdfFolder.DirName + "/" + pdfFile.FileName;
                                    var fileExt = pdfFile.Extension;

                                    if (!documentService.IsFilePathExist(serverFilePath))
                                    {
                                        var document = new Document()
                                        {
                                            Name = pdfFile.FileName,
                                            FileExtension = fileExt,
                                            FileExtensionIcon = pdfFile.ExtensionIcon,
                                            FilePath = serverFilePath,
                                            FolderID = pdfFolderId,
                                            IsLeaf = true,
                                            IsDelete = false,
                                            CreatedBy = UserSession.Current.User.Id,
                                            CreatedDate = DateTime.Now
                                        };
                                        this.documentService.Insert(document);
                                    }

                                    if (File.Exists(Server.MapPath(pdfFile.FilePath)))
                                    {
                                        File.Copy(Server.MapPath(pdfFile.FilePath), saveFilePath, true);
                                    }
                                }

                                foreach (var nativeFile in nativeFiles)
                                {
                                    // Path file to save on server disc
                                    var saveFilePath = Path.Combine(Server.MapPath(nativeFilesFolder.DirName),
                                        nativeFile.FileName);
                                    // Path file to download from server
                                    var serverFilePath = nativeFilesFolder.DirName + "/" + nativeFile.FileName;
                                    var fileExt = nativeFile.Extension;

                                    if (!documentService.IsFilePathExist(serverFilePath))
                                    {
                                        var document = new Document()
                                        {
                                            Name = nativeFile.FileName,
                                            FileExtension = fileExt,
                                            FileExtensionIcon = nativeFile.ExtensionIcon,
                                            FilePath = serverFilePath,
                                            FolderID = nativeFilesFolderId,
                                            IsLeaf = true,
                                            IsDelete = false,
                                            CreatedBy = UserSession.Current.User.Id,
                                            CreatedDate = DateTime.Now
                                        };
                                        this.documentService.Insert(document);
                                    }

                                    if (File.Exists(Server.MapPath(nativeFile.FilePath)))
                                    {
                                        File.Copy(Server.MapPath(nativeFile.FilePath), saveFilePath, true);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                    if (cbSendMail.Checked)
                    {
                        SendMailXDCB(objTran);
                    }
                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                }
            }
        }

        private void SendMailXDCB(Transmittal objTran)
        {
            int count = 0;
            var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(objTran.ProjectId));
            var subject = "Transmittal Record No: " + objTran.TransmittalNumber;
            var toID = 406;
            //List<int> ccID = new List<int>() { 342 };
            List<int> ccID = new List<int>() { 81, 183, 230, 257, 342, 488 };
            var listAttachDocTrans = this.attachDocToTransmittalService.GetAllByTransId(Convert.ToInt32(this.Request.QueryString["tranId"]));
            count = 0;
            var bodyContent = @"Kính gửi các anh, Transmittal số: " + objTran.TransmittalNumber + @"<br/>
                                        Mã dự án: " + objTran.ProjectName + @"<br/>
                                        Tên dự án: " + projectObj.Description + @"<br/>
                                        Bao gồm tài liệu:<br/>
                                        <table border='1' cellspacing='0'>
                                        <tr>
                                        <th style='text-align:center; width:40px'>No.</th>
                                        <th style='text-align:center; width:350px'>Document No.</th>
                                        <th style='text-align:center; width:350px'>Description</th>
                                        <th style='text-align:center; width:70px'>Rev</th>
                                        <th style='text-align:center; width:70px'>Original</th>
                                        <th style='text-align:center; width:70px'>Copy</th>
                                        <th style='text-align:center; width:70px'>Remark</th>
                                        </tr>";
            if (listAttachDocTrans.Count > 0)
            {
                foreach (var docId in listAttachDocTrans)
                {
                    var document = this.documentPackageService.GetById(docId.DocumentId.GetValueOrDefault());
                    var port = ConfigurationSettings.AppSettings.Get("DocLibPort");
                    if (document != null)
                    {
                        string remark = "";
                        var attachFile = this.attachFilesPackageService.GetAllDocId(document.ID);
                        var nativeFile = attachFile.Any(t => t.Extension.ToLower() != "pdf");
                        if (nativeFile)
                        {
                            remark = "Included Native File on SPF";
                        }
                        else
                        {
                            remark = "";
                        }
                        count += 1;
                        bodyContent += @"<tr>
                            <td style='text-align: center;'>" + count + @"</td>" +
                            "<td>" + document.DocNo + @"</td>" +
                            "<td>" + document.DocTitle + @"</td>" +
                            "<td>" + document.RevisionName + @"</td>" +
                            "<td>" + docId.OriginalNumber.ToString() + @"</td>" +
                            "<td>" + docId.CopyNumber.ToString() + @"</td>" +
                            "<td>" + remark + @"</td>" +
                            "</tr>";
                    }
                }
            }
            bodyContent += @"</table>
                                        <br/>
                                        Bản mềm TL đã upload lên hệ thống Spf theo link:<br />
                                        <a href='http://spf-edms.vietsov.com.vn:8002/Controls/Document/DocumentsLibrary.aspx'>http://spf-edms.vietsov.com.vn:8002/Controls/Document/DocumentsLibrary.aspx</a><br/>";

            var folderObj = this.folderService.GetAllByName(objTran.TransmittalNumber);
            if (folderObj != null)
            {
                var selectedFolderId = folderObj.ID;
                var folderPermissionList = this.folderService.GetAllByProject(projectObj.ID);

                var childNodes = new List<int>();
                var folderIdsList = this.GetAllChildren(selectedFolderId, folderPermissionList, ref childNodes);
                childNodes = new List<int>();
                var listParentFolderId = this.GetAllParentID(Convert.ToInt32(selectedFolderId), childNodes);
                folderIdsList.AddRange(listParentFolderId);
                var toMail = this.userService.GetByID(toID);
                if (toMail != null)
                {
                    foreach (var item in folderIdsList)
                    {
                        var permission = this.userDataPermissionService.GetByUserId(toMail.Id, item);
                        if (permission == null)
                        {
                            var permissionObj = new UserDataPermission();
                            permissionObj.FolderId = item;
                            permissionObj.ProjectId = projectObj.ID;
                            permissionObj.UserId = toMail.Id;
                            permissionObj.RoleId = toMail.RoleId;
                            permissionObj.OnlyView = false;
                            permissionObj.IsFullPermission = false;
                            permissionObj.CreatedBy = UserSession.Current.User.Id;
                            permissionObj.CreatedDate = DateTime.Now;
                            this.userDataPermissionService.Insert(permissionObj);
                        }
                        else
                        {
                            permission.FolderId = item;
                            permission.ProjectId = projectObj.ID;
                            permission.UserId = toMail.Id;
                            permission.RoleId = toMail.RoleId;
                            permission.OnlyView = false;
                            permission.IsFullPermission = false;
                            permission.CreatedBy = UserSession.Current.User.Id;
                            permission.CreatedDate = DateTime.Now;
                            this.userDataPermissionService.Update(permission);
                        }
                    }
                }

                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials = new NetworkCredential(UserSession.Current.User.Email,"")
                };

                var message = new MailMessage();
                message.From = new MailAddress(UserSession.Current.User.Email, UserSession.Current.User.FullName);
                message.Subject = subject;
                message.BodyEncoding = new UTF8Encoding();
                message.IsBodyHtml = true;
                message.Body = bodyContent;

                if (toMail != null)
                {
                    if (!string.IsNullOrEmpty(toMail.Email))
                    {
                        message.To.Add(new MailAddress(toMail.Email));
                    }
                    var ccMail = this.userService.GetByListID(ccID);
                    foreach (var cc in ccMail)
                    {
                        if (!string.IsNullOrEmpty(cc.Email))
                        {
                            message.CC.Add(new MailAddress(cc.Email));
                        }
                    }
                    if (objTran.IsGenerate.GetValueOrDefault())
                    {
                        FileInfo file = new FileInfo(Server.MapPath(objTran.GeneratePath));
                        if (file.Exists)
                        {
                            Attachment data = new Attachment(file.FullName, System.Net.Mime.MediaTypeNames.Application.Octet);
                            message.Attachments.Add(data);
                        }
                    }

                    if (ConfigurationManager.AppSettings["EnableSendNotification"].ToLower() == "true")
                    {
                        smtpClient.Send(message);
                    }
                }
            }
        }

        private List<int> GetAllChildren(int parent, List<Folder> folderList, ref List<int> childNodes)
        {
            var childFolderList = this.folderService.GetSpecificFolderChild(parent);
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList.Select(t => t.ID));
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, folderList, ref childNodes);
                }
            }

            childNodes.Add(parent);

            return childNodes.Distinct().ToList();
        }

        private List<int> GetAllParentID(int folderId, List<int> listFolderId)
        {
            var folder = this.folderService.GetById(folderId);
            if (folder.ParentID != null)
            {
                listFolderId.Add(folder.ParentID.Value);
                this.GetAllParentID(folder.ParentID.Value, listFolderId);
            }

            return listFolderId;
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedValue == "1")
            {
                pnPage.Visible = false;
            }
            else
            {
                pnPage.Visible = true;
            }
        }
    }
}
﻿
using System;
using System.Linq;
using EDMs.Data.DAO.Document;
using EDMs.Data.DAO.Scope;
using EDMs.Data.DAO.Security;

namespace EDMs.Data.Entities
{
    public partial class WorkGroup
    {
        ////private readonly ScopeProjectDAO scopeProjectDAO = new ScopeProjectDAO();
        ////private readonly UserDAO _userDAO = new UserDAO();
        ////public ScopeProject Project
        ////{
        ////    get { return scopeProjectDAO.GetById(this.ProjectId.GetValueOrDefault()); }
        ////}
        private readonly ScopeProjectDAO scopeProjectDAO = new ScopeProjectDAO();
        public string FullName
        {
            get
            {
                return !string.IsNullOrEmpty(this.Description)
                        ? this.Name + " (" + this.Description + ")"
                        : this.Name;
            }
        }

        ////public User ProjectManager
        ////{
        ////    get
        ////    {
        ////        return _userDAO.GetByID(this.Project.ProjectManagerId.GetValueOrDefault());
        ////    }
        ////}
        public string FullNameRev
        {
            get
            {
                return !string.IsNullOrEmpty(this.RevisionName)
                        ? this.Name + ", " + this.RevisionName
                        : this.Name;
            }
        }

        public string Type { get; set; }

        public bool IsLater
        {
            get { return this.Deadline != null && this.EndDate == null && (this.Deadline.GetValueOrDefault().Date < DateTime.Now.Date) && this.Complete.GetValueOrDefault() < 100; }
        }

        public bool IsStop
        {
            get
            {
                var obj = scopeProjectDAO.GetById(this.ProjectId.GetValueOrDefault());
                if (obj != null)
                {
                    return obj.IsStop.GetValueOrDefault();
                }
                else { return false; }
            }
        }

        //public bool IsUsing
        //{
        //    get
        //    {
        //        return docDAO.GetByWorkpackages(this.ID).Any();
        //    }
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Document;

namespace EDMs.Web.Controls.Scope
{
    public partial class CompactSummary : System.Web.UI.Page
    {
        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();
        private readonly WorkGroupService workGroupService = new WorkGroupService();
        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {

            }
        }

        protected void grdDocument_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            this.grdDocument.DataSource = this.LoadDocument();
        }

        public DataTable LoadDocument()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("Name");
            dt.Columns.Add("PercentComplete");
            dt.Columns.Add("Working");
            dt.Columns.Add("Completed");
            dt.Columns.Add("Overdue");
            dt.Columns.Add("Total");
            if (!string.IsNullOrEmpty(this.Request.QueryString["projectId"]))
            {
                var wpList = this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.Request.QueryString["projectId"]));
                foreach (var wpitem in wpList)
                {
                    DataRow dr = dt.NewRow();
                    var docList = this.documentPackageService.GetAllByWorkgroup(wpitem.ID);
                    var docWorkingList = docList.Where(t => t.Complete < 100 && t.Deadline > DateTime.Now).ToList().Count;
                    var docCompleteList = docList.Where(t => t.Complete == 100).ToList().Count;
                    var docOverdueList = docList.Where(t => t.Complete < 100 && t.Deadline < DateTime.Now).ToList().Count;
                    dr["ID"] = wpitem.ID;
                    dr["Name"] = wpitem.Name;
                    dr["PercentComplete"] = wpitem.Complete;
                    dr["Working"] = docWorkingList;
                    dr["Completed"] = docCompleteList;
                    dr["Overdue"] = docOverdueList;
                    dr["Total"] = docList.Count;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
    }
}
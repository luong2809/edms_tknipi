﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Data.Entities;

namespace EDMs.Web.Controls.Customers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Business.Services;
    using EDMs.Web.Utilities.Sessions;
    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class Detail_Consulting_Edit : Page
    {
        /// <summary>
        /// The patient status service
        /// </summary>
        private Patient_ConsultingService _patientConsultingService;
        private ServiceTypeService _serviceTypeService;

        /// <summary>
        /// The patient service
        /// </summary>
        private PatientService _patientService;

        public Detail_Consulting_Edit()
        {
            _serviceTypeService = new ServiceTypeService();
            _patientConsultingService = new Patient_ConsultingService();
            _patientService =new PatientService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindResourceType();
                BindData();
            }
        }

        private void BindResourceType()
        {
            var services = _serviceTypeService.GetAll();
            cboService.DataSource = services;
            cboService.DataTextField = "Name";
            cboService.DataValueField = "ID";

            cboService.DataBind();
        }
        private void BindData()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["cID"]))
            {
                var cID = Request.QueryString["cID"];
                if (cID != null)
                {
                    int tempcID;
                    if (int.TryParse(cID, out tempcID))
                    {
                       var customer = _patientService.GetByID(tempcID);
                       lblTenKhachHang.Text = customer.FullName;
                    }
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                var ID = Request.QueryString["ID"];
                if (ID != null)
                {
                    int tempID;
                    if (int.TryParse(ID, out tempID))
                    {
                        Consulting consulting = _patientConsultingService.GetByID(tempID);
                        txtContent.Text = consulting.Content;
                        txtDateTime.SelectedDate = consulting.CreatedDate;

                        if (consulting.ListOfServiceType != null && consulting.ListOfServiceType.Count > 0)
                        {
                            foreach (ServiceType st in consulting.ListOfServiceType)
                            {
                                IList<RadListBoxItem> items = cboService.Items.FindAll(x => x.Value == st.Id.ToString());
                                if (items != null && items.Count > 0)
                                {
                                    foreach (RadListBoxItem item in items)
                                    {
                                        item.Checked = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

       
        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                var ID = Request.QueryString["ID"];
                if (ID != null)
                {
                    int tempID;
                    if (int.TryParse(ID, out tempID))
                    {
                        //Update
                        UpdateConsulting(tempID);
                        ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
                    }
                }
            }
            else
            {
                CreateConsulting();
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }
        private void UpdateConsulting(int ID)
        {
            //Create
            Consulting cs = _patientConsultingService.GetByID(ID);
            cs.CreatedDate = txtDateTime.SelectedDate;
            cs.UpdatedBy = UserSession.Current.User.Id;
            cs.UpdatedDate = DateTime.Now;
            //cs.UserID = UserSession.Current.User.Id;
            cs.Content = txtContent.Text.Trim();

            string serviceTypeIDs = string.Empty;
            foreach (var item in cboService.CheckedItems)
            {
                serviceTypeIDs += item.Value + ",";
            }
            cs.ServiceTypeID = serviceTypeIDs;
            
            _patientConsultingService.Update(cs);
        }
        private void CreateConsulting()
        {
            //Create
            Consulting cs = new Consulting();
            cs.UpdatedBy = UserSession.Current.User.Id;
            cs.UpdatedDate = DateTime.Now;
            cs.CreatedDate = txtDateTime.SelectedDate;
            cs.UserID = UserSession.Current.User.Id;
            cs.CustomerID = Request.QueryString["cID"] == null ? 0 : int.Parse(Request.QueryString["cID"].ToString());
            cs.Content = txtContent.Text.Trim();
            
            string serviceTypeIDs = string.Empty;
            foreach (var item in cboService.CheckedItems)
            {
                serviceTypeIDs += item.Value + ","; 
            }
            cs.ServiceTypeID = serviceTypeIDs;

            _patientConsultingService.Insert(cs);
        }
        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerImportForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Data.Entities;

namespace EDMs.Web.Controls.Customers
{
    using System;
    using System.Web.UI;
    using Business.Services;
    using Telerik.Web.UI;
using System.Data;
    using System.Data.OleDb;
    using System.IO;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class CustomerImportForm : Page
    {
        /// <summary>
        /// The patient status service
        /// </summary>
        private PatientStatusService _patientStatusService;


        /// <summary>
        /// The patient service
        /// </summary>
        private PatientService _patientService;

        public CustomerImportForm()
        {
            _patientStatusService = new PatientStatusService();
            _patientService =new PatientService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            syncUpload1.UploadedFilesRendering = Telerik.Web.UI.AsyncUpload.UploadedFilesRendering.BelowFileInput;
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            foreach (UploadedFile file in syncUpload1.UploadedFiles)
            {
                string tempPath = Path.GetDirectoryName(Server.MapPath(syncUpload1.TemporaryFolder)) + "\\Temp\\" + Guid.NewGuid().ToString() + file.FileName;
                file.SaveAs(tempPath, true);
                DataSet ds = LoadExcelFile(tempPath);                
                File.Delete(tempPath);
                if (ds != null && ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = ds.Tables[0].Rows[i];
                        Patient patient = new Patient();
                        patient.FullName = row[1].ToString();
                        if (patient.FullName != string.Empty)
                        {
                            string[] names = patient.FullName.Split(new string[1] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            patient.FirstName = names[0];
                            patient.LastName = names[names.Length - 1];
                        }
                        patient.Sex = row[2].ToString();
                        patient.DateOfBirth = DateTime.Parse(row[3].ToString());                        
                        patient.HomePhone = row[5].ToString();
                        patient.CellPhone = row[6].ToString();
                        patient.Email = row[7].ToString();
                        patient.Address1 = row[8].ToString();                        
                        patient.PatientStatus.Id = 1;                        
                        this._patientService.Insert(patient);
                    }
                }
            }
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        protected DataSet LoadExcelFile(string fileName)
        {
            DataSet ds = new DataSet();            
            string provider2007 = string.Format("Provider= Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;HDR=YES;\"",fileName);
            string provider2003 = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes;'",fileName);
            OleDbConnection con = new OleDbConnection();
            try
            {
                con = new OleDbConnection(provider2007);                
            }
            catch (Exception ex)
            {
                try
                {
                    con = new OleDbConnection(provider2003);                
                }catch
                {
                    return null;
                }
            }
            OleDbDataAdapter da = new OleDbDataAdapter("select * from [Sheet1$]", con);                
            da.Fill(ds);
            return ds;
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }
       
    }
}
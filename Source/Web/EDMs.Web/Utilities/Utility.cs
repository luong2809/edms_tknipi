﻿using System.Text.RegularExpressions;

namespace EDMs.Web.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Data.Objects.DataClasses;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Security.Cryptography;
    using System.ServiceProcess;
    using System.Text;

    using EDMs.Business.Services;

    public class Utility
    {
        /// <summary>
        /// The passphrase.
        /// </summary>
        private const string Passphrase = "MASUpilamix!987";

        public const int CreateNewWP = 1;
        public const int UpdateWP = 2;
        public const int DeleteWP = 3;
        public const int CreateNewDoc = 4;
        public const int UpdateDoc = 5;
        public const int DeleteDoc = 6;
        public const int DeadlineWP = 7;
        public const int DeadlineDoc = 8;
        public const int UploadFileIDC = 9;

        /// <summary>
        /// The alphabet dictionary.
        /// </summary>
        public static Dictionary<string, string> alphabetDictionary = new Dictionary<string, string>
            {
                { "1", "A" },
                { "2", "B" },
                { "3", "C" },
                { "4", "D" },
                { "5", "E" },
                { "6", "F" },
                { "7", "G" },
                { "8", "H" },
                { "9", "I" },
                { "10", "J" },
                { "11", "K" },
                { "12", "L" },
                { "13", "M" },
                { "14", "N" },
                { "15", "O" },
                { "16", "P" },
                { "17", "Q" },
                { "18", "R" },
                { "19", "S" },
                { "20", "T" },
                { "21", "U" },
                { "22", "V" },
                { "23", "W" },
                { "24", "X" },
                { "25", "Y" },
                { "26", "Z" },
            };

        public static Dictionary<string, string> FileIcon = new Dictionary<string, string>
            {
                { "pptx", "~/images/powerpoint.png" },
                { "ppt", "~/images/powerpoint.png" },
                { "doc", "~/images/wordfile.png" },
                { "docx", "~/images/wordfile.png" },
                { "dotx", "~/images/wordfile.png" },
                { "xls", "~/images/excelfile.png" },
                { "xlsx", "~/images/excelfile.png" },
                { "pdf", "~/images/pdffile.png" },
                { "7z", "~/images/7z.png" },
                { "dwg", "~/images/dwg.png" },
                { "dxf", "~/images/dxf.png" },
                { "rar", "~/images/rar.png" },
                { "zip", "~/images/zip.png" },
                { "txt", "~/images/txt.png" },
                { "xml", "~/images/xml.png" },
                { "xlsm", "~/images/excelfile.png" },
                { "bmp", "~/images/bmp.png" },
            };

        public static Dictionary<int, string> MonthR = new Dictionary<int, string>
            {
                { 1, "ЯНВАРЬ" },
                { 2, "ФЕВРАЛЬ" },
                { 3, "МАРТ" },
                { 4, "АПРЕЛЬ" },
                { 5, "МАЙ" },
                { 6, "ИЮНЬ" },
                { 7, "ИЮЛЬ" },
                { 8, "АВГУСТ" },
                { 9, "СЕНТЯБРЬ" },
                { 10, "ОКТЯБРЬ" },
                { 11, "НОЯБРЬ" },
                { 12, "ДЕКАБРЬ" },
            };

        public static Dictionary<int, string> MonthR2 = new Dictionary<int, string>
            {
                { 1, "01 Январь" },
                { 2, "02 Февраль" },
                { 3, "03 Март" },
                { 4, "04 Апрель" },
                { 5, "05 Май" },
                { 6, "06 Июнь" },
                { 7, "07 Июль" },
                { 8, "08 Август" },
                { 9, "09 Сентябрь" },
                { 10, "10 Октябрь" },
                { 11, "11 Ноябрь" },
                { 12, "12 Декабрь" },
            };

        public static Dictionary<string, string> DepartmentDescription = new Dictionary<string, string>
            {
                { "0", string.Empty },
                { "01", "Ban lãnh đạo" },
                { "02", "Bộ phận giúp việc lãnh đạo" },
                { "03", "Phòng định hướng và quản lý khoa học" },
                { "04", "Phòng kế hoạch, thương mại và dịch vụ" },
                { "05", "Phòng lao động, tiền lương và cán bộ" },
                { "06", "Phòng thông tin khoa học-kỹ thuật, cơ sở dữ liệu và lưu trữ" },
                { "07", "Phòng cung ứng vật tư-thiết bị và hành chính quản trị" },
                { "08", "Phòng kế toán" },
                { "09", "Phòng địa chất thăm dò" },
                { "10", "Phòng địa chất mỏ" },
                { "11", "Phòng khoan và sửa chữa giếng khoan" },
                { "12", "Phòng thiết kế khai thác mỏ dầu và khí" },
                { "13", "Phòng mô hình hóa mỏ dầu và khí" },
                { "14", "Phòng công nghệ khai thác dầu khí" },
                { "15", "Phòng kinh tế" },
                { "16", "Lãnh đạo Trung tâm" },
                { "17", "Phòng thí nghiệm trầm tích và thạch học" },
                { "18", "Phòng thí nghiệm phân tích dầu và sản phẩm dầu" },
                { "19", "Phòng thí nghiệm phân tích khí" },
                { "20", "Phòng thí nghiệm nghiên cứu dầu vỉa" },
                { "21", "Phòng thí nghiệm phân tích nước và môi trường" },
                { "22", "Phòng thí nghiệm mô hình hóa và vật lý vỉa" },
                { "23", "Phòng thí nghiệm vận chuyển dầu và khí" },
                { "24", "Nhóm chánh thiết kế" },
                { "25", "Phòng khảo sát và chứng chỉ công trình biển" },
                { "26", "Phòng khảo sát công trình" },
                { "27", "Phòng cơ khí năng lượng" },
                { "28", "Phòng thiết kế công nghệ và xây dựng mỏ" },
                { "29", "Phòng đường ống và cáp ngầm" },
                { "30", "Phòng thiết kế điện, thông tin và tự động hóa" },
                { "31", "Phòng thiết kế và phân tích kết cấu chịu lực công trình biển" },
                { "32", "Phòng chống ăn mòn" },
                { "33", "Phòng thiết kế sửa chữa công trình biển" },
                { "34", "Phòng thiết kế tổ chức thi công" },
                { "35", "Phòng định mức và dự toán" },
                { "36", "Phòng dự án mới" },
                { "37", "Nhóm quản lý dự án lô 16-2 và 42" },
            };

        public static Dictionary<string, string> DepartmentDescriptionR = new Dictionary<string, string>
            {
                { "0", string.Empty },
                { "01", "РУКОВОДСТВО" },
                { "02", "АППАРАТ ПРИ РУКОВОДСТВЕ" },
                { "03", "ОТДЕЛ КООРДИНАЦИИ И УПРАВЛЕНИЯ НАУКОЙ (ОКиУН)" },
                { "04", "ОТДЕЛ ПЛАНИРОВАНИЯ, КОММЕРЦИИ И СЕРВИСА (ОПКиС)" },
                { "05", "ОТДЕЛ ТРУДА, ЗАРПЛАТЫ И КАДРОВ (ОТЗиК)" },
                { "06", "ОТДЕЛ НАУЧНО-ТЕХНИЧЕСКОЙ ИНФОРМАЦИИ, БАЗ ДАННЫХ И АРХИВИРОВАНИЯ  (ОНТИБДиА)" },
                { "07", "ОТДЕЛ МАТЕРИАЛЬНО-ТЕХНИЧЕСКОГО СНАБЖЕНИЯ И АХО (ОМТСиАХО)" },
                { "08", "БУХГАЛТЕРИЯ" },
                { "09", "ОТДЕЛ РАЗВЕДОЧНОЙ ГЕОЛОГИИ-ОРГ" },
                { "10", "ОТДЕЛ ПРОМЫСЛОВОЙ ГЕОЛОГИИ-ОПГ" },
                { "11", "ОТДЕЛ БУРЕНИЯ И КАПИТАЛЬНОГО РЕМОНТА СКВАЖИН-ОБиКРС" },
                { "12", "ОТДЕЛ РАЗРАБОТКИ НЕФТЯНЫХ и ГАЗОВЫХ МЕСТОРОЖДЕНИЙ-ОРНиГМ" },
                { "13", "ОТДЕЛ МОДЕЛИРОВАНИЯ НЕФТЯННЫХ и ГАЗОВЫХ МЕСТОРОЖДЕНИ-ОМНиГМ" },
                { "14", "ОТДЕЛ ДОБЫЧИ НЕФТИ и ГАЗА-ОДНиГ" },
                { "15", "ОТДЕЛ ЭКОНОМИКИ-ОЭ" },
                { "16", "Руководство Центра" },
                { "17", "ЛАБОРАТОРИЯ ЛИТОЛОГИИ и ПЕТРОГРАФИИ-ЛЛиПГ" },
                { "18", "ЛАБОРАТОРИЯ АНАЛИЗА НЕФТИ и НЕФТЕПРОДУКТОВ-ЛАНиНП" },
                { "19", "ЛАБОРАТОРИЯ АНАЛИЗА ГАЗА-ЛАГ" },
                { "20", "ЛАБОРАТОРИЯ ИССЛЕДОВАНИЯ ПЛАСТОВЫХ НЕФТЕЙ-ЛИПН" },
                { "21", "ЛАБОРАТОРИЯ АНАЛИЗА ВОДЫ И ОКРУЖАЮЩЕЙ СРЕДЫ-ЛОАВиОС" },
                { "22", "ЛАБОРАТОРИЯ МОДЕЛИРОВАНИЯ и ФИЗИКИ ПЛАСТА - ЛМиФП" },
                { "23", "ЛАБОРАТОРИЯ ТРАНСПОРТА НЕФТИ и ГАЗА-ЛТНиГ" },
                { "24", "БЮРО ГЛАВНЫХ ИНЖЕНЕРОВ ПРОЕКТОВ-ГИП" },
                { "25", "ОТДЕЛ СЕРТИФИКАЦИИ и КАЧЕСТВА МНГС (ОСиК)" },
                { "26", "ОТДЕЛ ИНЖЕНЕРНЫХ ИЗЫСКАНИЙ-ОИИ" },
                { "27", "ОТДЕЛ ПРОЕКТИРОВАНИЯ МЕХАНИЧЕСКИХ СИСТЕМ (ОПМС)" },
                { "28", "ОТДЕЛ ТЕХНОЛОГИЧЕСКОГО ПРОЕКТИРОВАНИЯ и ОБУСТРОЙСТВА МЕСТ-НИЙ-ОТПиОМ" },
                { "29", "ОТДЕЛ ПОДВОДНЫХ ТРУБОПРОВОДОВ и КАБЕЛЕЙ-ОПТиК" },
                { "30", "ОТДЕЛ ЭЛЕКТРОСНАБЖЕНИЯ, СВЯЗИ,  КИПиА - ОЭСКИПиА" },
                { "31", "ОТДЕЛ ПРОЕКТИРОВАНИЯ КОНСТРУКЦИИ МОРСКИХ СООРУЖЕНИЙ (ОПКМС)" },
                { "32", "ОТДЕЛ ЗАЩИТЫ СООРУЖЕНИЙ ОТ КОРРОЗИИ-ОЗСотК" },
                { "33", "ОТДЕЛ ПРОЕКТИРОВАНИЯ РЕМОНТОВ МОРСКИХ СООРУЖЕНИЙ-ОПРМС" },
                { "34", "ОТДЕЛ ПРОЕКТИРОВАНИЯ ОРГАНИЗАЦИИ СТРОИТЕЛЬСТВА-ОПОС" },
                { "35", "ОТДЕЛ НОРМ и СМЕТ-ОНиC" },
                { "36", "ОТДЕЛ НОВЫХ ПРОЕКТОВ - ОНП" },
                { "37", "ГРУППА ПРОЕКТА БЛОКА 16-2 и 42" },
            };

        /// <summary>
        /// The clone.
        /// </summary>
        /// <param name="entityObject">
        /// The entity object.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T Clone<T>(T entityObject) where T : EntityObject, new()
        {
            return EntityCloner<T>.Clone(entityObject);
        }

        /// <summary>
        /// The clone with graph.
        /// </summary>
        /// <param name="entityObject">
        /// The entity object.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T CloneWithGraph<T>(T entityObject) where T : EntityObject, new()
        {
            return EntityCloner<T>.CloneWithGraph(entityObject);
        }

        /// <summary>
        /// The deep clone.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T DeepClone<T>(T obj)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Generate patient code
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <returns>
        /// Patient code with format
        /// </returns>
        public static string GeneratePatientCode(string code)
        {
            var patientCodeFormat = System.Configuration.ConfigurationManager.AppSettings["PatientCodeFormat"];
            var result =
                (patientCodeFormat.Remove(patientCodeFormat.Length - 1 - code.Length, code.Length) + code).Replace(
                    "x", "0");

            return result;
        }

        /// <summary>
        /// Converts the type of the string to.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="param">The param.</param>
        /// <returns></returns>
        public static object ConvertStringToType<T>(string param)
        {
            var underlyingType = Nullable.GetUnderlyingType(typeof(T));
            if (underlyingType == null)
                return Convert.ChangeType(param, typeof(T), CultureInfo.InvariantCulture);
            return String.IsNullOrEmpty(param)
              ? null
              : Convert.ChangeType(param, underlyingType, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// The encrypt.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string Encrypt(string message)
        {
            byte[] results;
            var utf8 = new UTF8Encoding();

            // to create the object for UTF8Encoding  class
            // TO create the object for MD5CryptoServiceProvider 
            var md5 = new MD5CryptoServiceProvider();
            byte[] deskey = md5.ComputeHash(utf8.GetBytes(Passphrase));

            // to convert to binary passkey
            // TO create the object for  TripleDESCryptoServiceProvider 
            var desalg = new TripleDESCryptoServiceProvider();
            desalg.Key = deskey; // to  pass encode key
            desalg.Mode = CipherMode.ECB;
            desalg.Padding = PaddingMode.PKCS7;
            byte[] encrypt_data = utf8.GetBytes(message);

            // to convert the string to utf encoding binary 

            try
            {
                // To transform the utf binary code to md5 encrypt    
                ICryptoTransform encryptor = desalg.CreateEncryptor();
                results = encryptor.TransformFinalBlock(encrypt_data, 0, encrypt_data.Length);
            }
            finally
            {
                // to clear the allocated memory
                desalg.Clear();
                md5.Clear();
            }

            // to convert to 64 bit string from converted md5 algorithm binary code
            return Convert.ToBase64String(results);
        }

        /// <summary>
        /// The decrypt.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string Decrypt(string message)
        {
            byte[] results;
            var utf8 = new UTF8Encoding();
            var md5 = new MD5CryptoServiceProvider();
            byte[] deskey = md5.ComputeHash(utf8.GetBytes(Passphrase));
            var desalg = new TripleDESCryptoServiceProvider();
            desalg.Key = deskey;
            desalg.Mode = CipherMode.ECB;
            desalg.Padding = PaddingMode.PKCS7;
            byte[] decrypt_data = Convert.FromBase64String(message);
            try
            {
                // To transform the utf binary code to md5 decrypt
                ICryptoTransform decryptor = desalg.CreateDecryptor();
                results = decryptor.TransformFinalBlock(decrypt_data, 0, decrypt_data.Length);
            }
            finally
            {
                desalg.Clear();
                md5.Clear();
            }

            // TO convert decrypted binery code to string
            return utf8.GetString(results);
        }

        public static string GetMd5Hash(string input)
        {
            var md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            var sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        /// <summary>
        /// The convert to data table.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="DataTable"/>.
        /// </returns>
        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }

                table.Rows.Add(row);
            }

            return table;

        }

        /// <summary>
        /// The service is available.
        /// </summary>
        /// <param name="serviceName">
        /// The service name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ServiceIsAvailable(string serviceName)
        {
            var ctl = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == serviceName);
            if (ctl != null)
            {
                if (ctl.Status == ServiceControllerStatus.Running)
                {
                    return true;
                }
            }

            return false;
        }

        public static string RemoveAllSpecialCharacter(string strSource)
        {
            var strDes = Regex.Replace(strSource, @"[\&\'\;\+\:\*\%\$\^\#\@\{\}\?\<\>\!\/\\]", string.Empty);
            return strDes;
        }

        public static string RemoveSpecialCharacter(string input, string replaceCharacter)
        {
            return Regex.Replace(input, @"[^0-9a-zA-Z\.\-]+", replaceCharacter);
        }
        public static string RemoveSpecialCharacter(string input)
        {
            return Regex.Replace(input, @"[^0-9a-zA-Z]+", string.Empty);
        }
        public static string RemoveSpecialCharacterForFolder(string input)
        {
            return Regex.Replace(input, @"[\\/\:\*\?\<\>\|]", string.Empty);
        }
        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            string replace = Regex.Replace(name, invalidReStr, "_").Replace(";", "").Replace(",", "");
            return replace;
        }

        public static bool ConvertStringToDateTime(string input, ref DateTime output)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var strDatetime = input.Substring(0, input.LastIndexOf("/") + 5);

                if (DateTime.TryParseExact(strDatetime, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "M/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "MM/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "M/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "d/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "dd/M/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "d/M/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool ConvertStringToDateTimeddMMyyyy(string input, ref DateTime output)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var strDatetime = input.Substring(0, input.LastIndexOf("/") + 5);

                if (DateTime.TryParseExact(strDatetime, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                else if (DateTime.TryParseExact(strDatetime, "d/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                else if (DateTime.TryParseExact(strDatetime, "dd/M/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                else if (DateTime.TryParseExact(strDatetime, "d/M/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }
                else
                {
                    return false;
                }

                //if (DateTime.TryParseExact(strDatetime, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                //{
                //    return true;
                //}

                //if (DateTime.TryParseExact(strDatetime, "M/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                //{
                //    return true;
                //}

                //if (DateTime.TryParseExact(strDatetime, "MM/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                //{
                //    return true;
                //}

                //if (DateTime.TryParseExact(strDatetime, "M/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                //{
                //    return true;
                //}
            }

            return false;
        }

        public static int DifferentMonth(DateTime date1, DateTime date2)
        {
            return ((date1.Year - date2.Year)*12) + date1.Month - date2.Month;
        }

        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("Value must be between 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("Value must be between 1 and 3999");
        }
    }
}
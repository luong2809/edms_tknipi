﻿<%@ Page Title="About" Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="EDMs.Web.About" %>

<style>
    body {
        margin: 0;
        background-color: #e8e8e8;
    }
</style>
<div style="background-color: white">
    <div class="logo" style="padding: 0px 0px 5px 15px">
        <asp:image id="imgLogo" runat="server"
            imageurl="Images/THEA_Logo_vn.png" style="height: 70px; border-color: white" />
        <span style="font-size: xx-large; font-style: oblique; font-weight: bold">SPF</span>
    </div>
</div>
<div style="">
    <div style="padding: 0px 10px 10px 15px">
        <article>
            <%--<p>
                SmartPlant Foundation (SPF)<br />
				Number: SRBY549AV-1000C <br />
                Version: 2019 (10.00.00) <br />
                Last updated: 30-Apr-2018 <br />
                © Copyright 2018 Intergraph Corporation Part of Hexagon. All rights reserved.<br />
            </p>--%>
			<p>
                SmartPlant Foundation (SPF)<br />
                Version 07.01.00.0017 <br />
                Last updated Jan 09, 2019 <br />
                © Copyright 2018 Intergraph Corporation Part of Hexagon.<br />
                All rights reserved.
            </p>
            
        </article>
    </div>
</div>


<%--<aside>
        <h3>Aside Title</h3>
        <p>        
            Use this area to provide additional information.
        </p>
        <ul>
            <li><a runat="server" href="~/">Home</a></li>
            <li><a runat="server" href="~/About.aspx">About</a></li>
            <li><a runat="server" href="~/Contact.aspx">Contact</a></li>
        </ul>
    </aside>--%>

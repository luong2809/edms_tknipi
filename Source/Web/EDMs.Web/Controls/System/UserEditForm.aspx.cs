﻿namespace EDMs.Web
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using EDMs.Business.Services.Security;
    using EDMs.Business.Services.Library;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;
    using System.Linq;

    public partial class UserEditForm : Page
    {

        #region Fields
        private readonly UserService userService;
        private readonly RoleService roleService;
        private readonly DisciplineService disciplineService;
        private readonly CurrentManpowerService currentmanpowerService;
        private const string ResourceParameterKey = "id";
        private const string UserParameterKey = "userid";
        #endregion

        #region Properties
        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <value>
        /// The user id.
        /// </value>
        private int? ResourceId
        {
            get
            {
                if (String.IsNullOrEmpty(Request[ResourceParameterKey])) return null;

                int outValue;
                if (int.TryParse(Request[ResourceParameterKey], out outValue))
                    return outValue;
                return null;
            }
        }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <value>
        /// The user id.
        /// </value>
        private int? UserId
        {
            get
            {
                if (String.IsNullOrEmpty(Request[UserParameterKey])) return null;

                int outValue;
                if (int.TryParse(Request[UserParameterKey], out outValue))
                    return outValue;
                return null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has user account.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has user account; otherwise, <c>false</c>.
        /// </value>
        private bool HasUserAccount
        {
            get { return !string.IsNullOrEmpty(txtUsername.Text.Trim()); }
        }
        #endregion

        #region Helpers

        /// <summary>
        /// Loads the data to combo.
        /// </summary>
        private void LoadDataToCombo()
        {
            var roles = this.roleService.GetAll(UserSession.Current.RoleId == 1);
            roles.Insert(0, new Role() { Id = 0, Name = string.Empty });
            this.ddlRoles.DataSource = roles;
            this.ddlRoles.DataValueField = "Id";
            this.ddlRoles.DataTextField = "Name";
            this.ddlRoles.DataBind();

            var disciplineList = this.disciplineService.GetAll().OrderBy(t=>t.Name).ToList();
            disciplineList.Insert(0, new Discipline() { ID = 0 });
            this.ddlDiscipline.DataSource = disciplineList;
            this.ddlDiscipline.DataTextField = "FullName";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataBind();
        }

        #endregion

        #region Initializes
        
        public UserEditForm()
        {
            this.userService=new UserService();
            this.roleService = new RoleService();
            this.disciplineService = new DisciplineService();
            this.currentmanpowerService = new CurrentManpowerService();
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtUsername.Focus();
                LoadDataToCombo();
                if (!string.IsNullOrEmpty(this.Request.QueryString["userId"]))
                {
                    var userId = Convert.ToInt32(this.Request.QueryString["userId"]);
                    var user = this.userService.GetByID(userId);
                    if (user != null)
                    {
                        this.txtUsername.Text = user.Username;
                        this.txtEmail.Text = user.Email;
                        this.txtCellPhone.Text = user.CellPhone;
                        this.txtHomePhone.Text = user.Phone;
                        this.txtPosition.Text = user.Position;
                        this.txtFullName.Text = user.FullName;
                        this.ddlRoles.SelectedValue = user.RoleId.ToString();

                        this.ddlDiscipline.SelectedValue = user.DisciplineID.ToString();

                        this.rbtnAdmin.Checked = user.IsAdmin.GetValueOrDefault();
                        this.rbtnChief.Checked = user.IsChief.GetValueOrDefault();
                        this.rbtnDC.Checked = user.IsDC.GetValueOrDefault();
                        this.rbtnEngineer.Checked = user.IsEngineer.GetValueOrDefault();
                        this.rbtnGip.Checked = user.IsGip.GetValueOrDefault();
                        this.rbtnSuperViewer.Checked = user.IsSupperViewer.GetValueOrDefault();
                        this.cbActive.Checked = user.Active.GetValueOrDefault();
                        this.rbtnLeader.Checked = user.IsLeader.GetValueOrDefault();
                    }
                }
            }

            if (this.rbtnLeader.Checked)
            {
                this.AddDiscipline.Visible = true;
            }
            else
            {
                this.AddDiscipline.Visible = false;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCapNhat control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["userId"]))
                {
                    var userId = Convert.ToInt32(this.Request.QueryString["userId"]);
                    var user = this.userService.GetByID(userId);
                    if (user != null)
                    {
                        user.Username = this.txtUsername.Text.Trim();
                        user.FullName = this.txtFullName.Text.Trim();
                        user.RoleId = Convert.ToInt32(this.ddlRoles.SelectedValue);
                        user.Position = this.txtPosition.Text.Trim();
                        user.Email = this.txtEmail.Text.Trim();
                        user.Phone = this.txtHomePhone.Text.Trim();
                        user.CellPhone = this.txtCellPhone.Text.Trim();
                        user.IsAdmin = this.rbtnAdmin.Checked;
                        user.IsChief = this.rbtnChief.Checked;
                        user.IsDC = this.rbtnDC.Checked;
                        user.IsEngineer = this.rbtnEngineer.Checked;
                        user.IsGip = this.rbtnGip.Checked;
                        user.IsSupperViewer = this.rbtnSuperViewer.Checked;
                        user.Active = this.cbActive.Checked;
                        user.IsLeader = this.rbtnLeader.Checked;
                        if (this.rbtnLeader.Checked)
                        { var disciplineObj = this.disciplineService.GetById(Convert.ToInt32(this.ddlDiscipline.SelectedValue));
                        user.DisciplineID = disciplineObj != null ? disciplineObj.ID : 0;
                        }
                        else
                        {
                            user.DisciplineID = 0;
                        }

                        this.userService.Update(user);
                    }
                }
                else
                {
                    var user = new User
                    {
                        Username = this.txtUsername.Text.Trim(),
                        Password = Utility.GetMd5Hash(GlobalVariables.Current.DefaultPasswordForNewUser),
                        FullName = this.txtFullName.Text.Trim(),
                        RoleId = Convert.ToInt32(this.ddlRoles.SelectedValue),
                        Position = this.txtPosition.Text.Trim(),
                        Email = this.txtEmail.Text.Trim(),
                        Phone = this.txtHomePhone.Text.Trim(),
                        CellPhone = this.txtCellPhone.Text.Trim(),
                        IsAdmin = this.rbtnAdmin.Checked,
                        IsChief = this.rbtnChief.Checked,
                        IsDC = this.rbtnDC.Checked,
                        IsEngineer = this.rbtnEngineer.Checked,
                        IsGip = this.rbtnGip.Checked,
                        IsSupperViewer = this.rbtnSuperViewer.Checked,
                        IsLeader=this.rbtnLeader.Checked,
                        Active = this.cbActive.Checked
                        
                    };
                    if (this.rbtnLeader.Checked)
                    {
                        var disciplineObj = this.disciplineService.GetById(Convert.ToInt32(this.ddlDiscipline.SelectedValue));
                        user.DisciplineID = disciplineObj != null ? disciplineObj.ID : 0;
                    }
                    else
                    {
                        user.DisciplineID = 0;
                    }
                    this.userService.Insert(user);
                }
                this.GetUpdateActual(Convert.ToInt32(this.ddlRoles.SelectedValue));
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }

        #endregion

        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            
        }

        protected void ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtUsername.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter User name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else if (this.HasUserAccount)
            {
                this.fileNameValidator.ErrorMessage = "The user name is already in use.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = !this.userService.CheckExists(this.UserId, this.txtUsername.Text.Trim());
            }
        }

        private void GetUpdateActual(int roleid)
        { var countuser=this.userService.GetAllByRoleId(roleid).Where(t=> t.IsLeader.GetValueOrDefault() ||t.IsEngineer.GetValueOrDefault()).Count().ToString();
            var daystart = new DateTime(DateTime.Now.Date.Year, 01, 01);
            var dayend = new DateTime(DateTime.Now.Date.Year, 12, 30);
                var count = 0;
                for (var i = daystart;
                  i <= dayend;
                  i = i.AddMonths(1))
                {
                    if (DateTime.Now.Month > i.Month)
                    {
                        count += 1;
                    }
                }

                var existProgressActual = this.currentmanpowerService.GetDepartment(roleid,DateTime.Now.Year);
                if (existProgressActual != null)
                {
                    if (existProgressActual.Values.Split('$').Count() >= count)
                    {
                        var actualList = existProgressActual.Values.Split('$');
                        actualList[count - 1] = countuser;
                        var newActual = string.Empty;
                        newActual = actualList.Aggregate(newActual, (current, t) => current + t + "$");

                        newActual = newActual.Substring(0, newActual.Length - 1);
                        existProgressActual.Values = newActual;

                        this.currentmanpowerService.Update(existProgressActual);
                    }

                }
                else
                {
                    var actual = "";
                    for (var i = daystart;
                      i <= dayend;
                      i = i.AddMonths(1))
                    {
                        actual += countuser + "$";
                    }
                    actual = actual.Substring(0, actual.Length - 1);
                    var current = new CurrentManpower()
                    {
                        DepartmentId = roleid,
                        Year = DateTime.Now.Year,
                        Values = actual,
                    };
                    this.currentmanpowerService.Insert(current);

                }
            
        }


        protected void rbtnLeader_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbtnLeader.Checked)
            {
                this.AddDiscipline.Visible = true;
            }
            else
            {
                this.AddDiscipline.Visible = false;
            }
        }

        protected void rbtnAdmin_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbtnAdmin.Checked)
            {
                this.AddDiscipline.Visible =false;
            }
        }

        protected void rbtnDC_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbtnDC.Checked)
            {
                this.AddDiscipline.Visible = false;
            }
        }

        protected void rbtnSuperViewer_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbtnSuperViewer.Checked)
            {
                this.AddDiscipline.Visible = false;
            }
        }

        protected void rbtnGip_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbtnGip.Checked)
            {
                this.AddDiscipline.Visible = false;
            }
        }

        protected void rbtnChief_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbtnChief.Checked)
            {
                this.AddDiscipline.Visible = false;
            }
        }

        protected void rbtnEngineer_CheckedChanged(object sender, EventArgs e)
        {
          
                this.AddDiscipline.Visible = false;
            
        }
    }
}
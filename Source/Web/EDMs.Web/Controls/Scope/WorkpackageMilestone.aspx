﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkpackageMilestone.aspx.cs" Inherits="EDMs.Web.Controls.Scope.WorkpackageMilestone" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        
        div.RadGrid .rgPager .rgAdvPart     
        {     
            display:none;
        }
        div.rgDataDiv {
            overflow: auto !important;
        }

        .DropZone1
        {
            width: 300px;
            height: 250px;
            padding-left: 230px;
            background: #fff url(../../Images/placeholder-add.png) no-repeat center center;
            background-color: #357A2B;
            border-color: #CCCCCC;
            color: #767676;
            float: left;
            text-align: center;
            font-size: 16px;
            color: white;
            position: relative;
        }
        #btnSavePanel {
            display: inline !important;
        }
        .RadComboBoxDropDown_Default .rcbHovered {
               background-color: #46A3D3;
               color: #fff;
           }
           .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
               margin: 0 0px;
           }
           .RadComboBox .rcbInputCell .rcbInput{
            border-left-color:#46A3D3 !important;
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 435px;
           }
           .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
               padding-left: 0px !important;
           }
            div.rgEditForm label {
               float: right;
            text-align: right;
            width: 72px;
           }
           .rgEditForm {
               text-align: right;
           }
           .RadComboBox {
               border-bottom-style: none !important;
            border-bottom-color: inherit !important;
            border-bottom-width: medium;
        }
           
    </style>

    <script src="~/Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }


            </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <div style="width: 100%" runat="server" ID="EditContent">
            <ul style="list-style-type: none">
                <li style="width: 650px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Milestone
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divMilestone">
                            <telerik:RadDatePicker ID="txtMilestone"  runat="server" 
                                ShowPopupOnFocus="True" CssClass="qlcbFormRequired" OnSelectedDateChanged="txtMilestone_OnSelectedDateChanged" AutoPostBack="True">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormRequired"/>
                            </telerik:RadDatePicker>
                            
                            <%--<asp:CustomValidator runat="server" ID="MilestoneValidator" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModule"
                            OnServerValidate="ServerValidationMilestone" Display="Dynamic" ControlToValidate="txtMilestone"/>--%>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 650px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Plan(%)
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Percent" id="txtPlan" runat="server" Style=" min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="70px" CssClass="min25Percent"
                                OnTextChanged="txtPlan_OnTextChanged" AutoPostBack="True">
                                <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSeparator="" PositivePattern="n %"/>
                                <EnabledStyle HorizontalAlign="Right" />
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 650px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Plan Total(%)
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Percent" id="txtPlanTotal" runat="server" Style=" min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="70px" CssClass="min25Percent">
                                <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSeparator="" PositivePattern="n %"/>
                                <EnabledStyle HorizontalAlign="Right" />
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 650px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Real(%)
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Percent" id="txtReal" runat="server" Style=" min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="70px" CssClass="min25Percent"
                                OnTextChanged="txtReal_OnTextChanged" AutoPostBack="True">
                                <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSeparator="" PositivePattern="n %"/>
                                <EnabledStyle HorizontalAlign="Right" />
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 650px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Real Total(%)
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Percent" id="txtRealTotal" runat="server" Style=" min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="70px" CssClass="min25Percent">
                                <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSeparator="" PositivePattern="n %"/>
                                <EnabledStyle HorizontalAlign="Right" />
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Plan for Next month(%)
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Percent" id="txtPlannextMonth" runat="server" Style=" min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="70px" CssClass="min25Percent" MaxValue="100" Enabled="false">
                                <NumberFormat AllowRounding="True" KeepNotRoundedValue="False" DecimalDigits="2" GroupSeparator="" PositivePattern="n %"/>
                                <EnabledStyle HorizontalAlign="Right" />
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 650px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Performing User
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                             <telerik:RadComboBox ID="txtListuser" runat="server"  AutoPostBack="True" DropDownWidth="450px" Height="250px"
                                 AutoCompleteSeparator="," AllowCustomText="True" MarkFirstMatch="True"  OnSelectedIndexChanged="txtListuser_SelectedIndexChanged"
                                CheckBoxes="true" EnableCheckAllItemsCheckBox="true" Skin="Windows7"
                                style="width: 440px !important; border-left-color: #FF0000 !important; ">
                            </telerik:RadComboBox>
                            <asp:TextBox ID="txtPerformingUser" runat="server" CssClass="min25Percent" TextMode="MultiLine" Rows="2" Enabled="false" Width="450px" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 650px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Note
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtNote" runat="server" Style="width: 450px;" CssClass="min25Percent" TextMode="MultiLine" Rows="2"/>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 650px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Description of duty performed
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtDescriptionOfDuty" runat="server" Style="width: 450px;" CssClass="min25Percent" TextMode="MultiLine" Rows="2"/>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 100%; text-align: center; padding-top: 3px">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" Width="70px" style="text-align: center">
                        <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                    
                    <telerik:RadButton ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" Width="70px" style="text-align: center">
                        <Icon PrimaryIconUrl="../../Images/clear.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>

                    <telerik:RadButton ID="btnCalculator" runat="server" Text="Calculator" OnClick="btnCalculator_Click" Width="90px" style="text-align: center">
                        <Icon PrimaryIconUrl="../../Images/calculator.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                </li>
            </ul>
        </div>
        <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True"
            AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
            GridLines="None" Skin="Windows7" Height="220"
            OnDeleteCommand="grdDocument_DeleteCommand"
            OnItemDataBound="grdDocument_ItemDataBound"
            OnItemCommand="grdDocument_OnItemCommand"
            OnNeedDataSource="grdDocument_OnNeedDataSource" 
            PageSize="10" Style="outline: none; overflow: hidden !important;">
            <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%">
                <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Contents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                <Columns>
                    <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                    <telerik:GridButtonColumn UniqueName="EditColumn" CommandName="EditCmd" ButtonType="ImageButton" ImageUrl="~/Images/edit.png">
                        <HeaderStyle Width="3%" />
                        <ItemStyle HorizontalAlign="Center"  />
                    </telerik:GridButtonColumn>

                    <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" ConfirmText="Do you want to delete content?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                        <HeaderStyle Width="3%" />
                            <ItemStyle HorizontalAlign="Center"/>
                    </telerik:GridButtonColumn>

                    <telerik:GridBoundColumn DataField="WorkpackageName" HeaderText="Workpackage" UniqueName="WorkpackageName" Display="False">
                        <HeaderStyle HorizontalAlign="Center" Width="12%" />
                        <ItemStyle HorizontalAlign="Left" Width="12%" />
                    </telerik:GridBoundColumn>
                                
                    <telerik:GridBoundColumn DataField="MilestoneDate" HeaderText="Milestone" UniqueName="MilestoneDate"
                        DataFormatString="{0:dd/MM/yyyy}" >
                        <HeaderStyle HorizontalAlign="Center" Width="8%" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    
                    <telerik:GridTemplateColumn HeaderText="Plan" UniqueName="PlanPercent" AllowFiltering="false">
                        <HeaderStyle HorizontalAlign="Center" Width="6%" />
                        <ItemStyle HorizontalAlign="Center" Width="6%" />
                        <ItemTemplate>
                            <%# Eval("PlanPercent") != null ? Eval("PlanPercent") + "%" : string.Empty%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    
                    <telerik:GridTemplateColumn HeaderText="Plan Total" UniqueName="PlanTotal" AllowFiltering="false">
                        <HeaderStyle HorizontalAlign="Center" Width="6%" />
                        <ItemStyle HorizontalAlign="Center"  />
                        <ItemTemplate>
                            <%# Eval("PlanTotal") != null ? Eval("PlanTotal") + "%" : string.Empty%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    
                    <telerik:GridTemplateColumn HeaderText="Real" UniqueName="Real" AllowFiltering="false">
                        <HeaderStyle HorizontalAlign="Center" Width="6%" />
                        <ItemStyle HorizontalAlign="Center"/>
                        <ItemTemplate>
                            <asp:Label ID="lblReal" runat="server" ForeColor='<%# Convert.ToDouble(Eval("Real")) == 0 ? Color.Red : Color.Black %>'
                                                    Text='<%# Eval("Real") != null ? Eval("Real") + "%" : string.Empty%>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    
                    <telerik:GridTemplateColumn HeaderText="Real Total" UniqueName="RealTotal" AllowFiltering="false">
                        <HeaderStyle HorizontalAlign="Center" Width="6%" />
                        <ItemStyle HorizontalAlign="Center"/>
                        <ItemTemplate>
                            <asp:Label ID="lblRealTotal" runat="server" ForeColor='<%# Convert.ToDouble(Eval("RealTotal")) == 0 ? Color.Red : Color.Black %>'
                                                    Text='<%# Eval("RealTotal") != null ? Eval("RealTotal") + "%" : string.Empty%>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    
                    <telerik:GridBoundColumn DataField="PerformingUser" HeaderText="Performing User" UniqueName="PerformingUser">
                        <HeaderStyle HorizontalAlign="Center" Width="15%" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                                
                    <telerik:GridBoundColumn DataField="Note" HeaderText="Note" UniqueName="Note">
                        <HeaderStyle HorizontalAlign="Center" Width="15%" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    
                    <telerik:GridBoundColumn DataField="DescriptionOfDuty" HeaderText="Description of duty performed" UniqueName="DescriptionOfDuty">
                        <HeaderStyle HorizontalAlign="Center" Width="15%" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True" ClipCellContentOnResize="false"></Resizing>
                <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="200" UseStaticHeaders="True" />
            </ClientSettings>
        </telerik:RadGrid>
        
        
         <%--<div style="width: 100%; text-align: center; padding-top: 270px">
           
        </div>--%>
        <asp:HiddenField runat="server" ID="docUploadedIsExist"/>
        <asp:HiddenField runat="server" ID="docIdUpdateUnIsLeaf"/>
        
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument" OnAjaxRequest="ajaxDocument_AjaxRequest">
            <AjaxSettings> 
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="txtMilestone" />
                        <telerik:AjaxUpdatedControl ControlID="txtPlan"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPlanTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtReal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtRealTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPerformingUser"/>
                        <telerik:AjaxUpdatedControl ControlID="txtNote"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPlannextMonth"/>
                        <telerik:AjaxUpdatedControl ControlID="txtDescriptionOfDuty"/>
                        <telerik:AjaxUpdatedControl ControlID="txtListuser" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="btnClear">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtMilestone" />
                        <telerik:AjaxUpdatedControl ControlID="txtPlan"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPlanTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtReal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtRealTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPerformingUser"/>
                        <telerik:AjaxUpdatedControl ControlID="txtNote"/>
                        <telerik:AjaxUpdatedControl ControlID="btnSave"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPlannextMonth"/>
                        <telerik:AjaxUpdatedControl ControlID="txtDescriptionOfDuty"/>
                        <telerik:AjaxUpdatedControl ControlID="txtListuser" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="btnCalculator">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtMilestone" />
                        <telerik:AjaxUpdatedControl ControlID="txtPlan"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPlanTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtReal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtRealTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPerformingUser"/>
                        <telerik:AjaxUpdatedControl ControlID="txtNote"/>
                        <telerik:AjaxUpdatedControl ControlID="btnSave"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPlannextMonth"/>
                        <telerik:AjaxUpdatedControl ControlID="txtDescriptionOfDuty"/>
                        <telerik:AjaxUpdatedControl ControlID="txtListuser" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="txtMilestone" />
                        <telerik:AjaxUpdatedControl ControlID="txtPlan"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPlanTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtReal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtRealTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPerformingUser"/>
                        <telerik:AjaxUpdatedControl ControlID="txtNote"/>
                        <telerik:AjaxUpdatedControl ControlID="btnSave"/>
                        <telerik:AjaxUpdatedControl ControlID="txtPlannextMonth"/>
                        <telerik:AjaxUpdatedControl ControlID="txtDescriptionOfDuty"/>
                        <telerik:AjaxUpdatedControl ControlID="txtListuser" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>

                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="txtPlan">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtPlanTotal" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="txtReal">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtRealTotal"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="txtMilestone">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="btnSave"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID ="txtListuser">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtPerformingUser" />
                    </UpdatedControls>
                    
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;

                function fileUploading(sender, args) {
                    var name = args.get_fileName();
                    document.getElementById("txtName").value = name;
                    
                    ajaxManager.ajaxRequest("CheckFileName$" + name);
                }

                function MyClick(sender, eventArgs) {
                    var inputs = document.getElementById("<%= grdDocument.MasterTableView.ClientID %>").getElementsByTagName("input");
                    for (var i = 0, l = inputs.length; i < l; i++) {
                        var input = inputs[i];
                        if (input.type != "radio" || input == sender)
                            continue;
                        input.checked = false;
                    }
                }
                
                function SelectMeOnly(objRadioButton, grdName) {

                    var i, obj;
                    for (i = 0; i < document.all.length; i++) {
                        obj = document.all(i);

                        if (obj.type == "radio") {

                            if (objRadioButton.id.substr(0, grdName.length) == grdName)
                                if (objRadioButton.id == obj.id)
                                    obj.checked = true;
                                else
                                    obj.checked = false;
                        }
                    }
                }
          </script>

        </telerik:RadScriptBlock>
    </form>
</body>
</html>

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentNew.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the DocumentNew type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.Entities
{
    using System.Linq;

    using EDMs.Data.DAO.Library;
    using EDMs.Data.DAO.Scope;

    /// <summary>
    /// The document new.
    /// </summary>
    public partial class ProjectAppendix
    {
        
        public string ProjectName
        {
            
            get
            {
                var scopeProjectDAO =  new ScopeProjectDAO();
                var pjObj = scopeProjectDAO.GetById(this.ProjectID.Value);
                return pjObj.Name;
            }
        }
    }
}

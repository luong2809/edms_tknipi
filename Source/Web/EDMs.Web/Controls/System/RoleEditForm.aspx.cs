﻿using EDMs.Business.Services.Library;

namespace EDMs.Web
{
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;

    using System;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Telerik.Web.UI;

    /// <summary>
    /// The role edit form.
    /// </summary>
    public partial class RoleEditForm : Page
    {
        /// <summary>
        /// The role service.
        /// </summary>
        private readonly RoleService roleService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        private readonly DisciplineService disciplineService;

        private readonly UserService userservice;
        private readonly GroupDataPermissionService groupDataPermissionService;
        private readonly CurrentManpowerService currentmanpowerService;

        /// <summary>
        /// The role parameter key.
        /// </summary>
        private const string RoleParameterKey = "roleid";

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleEditForm"/> class.
        /// </summary>
        public RoleEditForm()
        {
            this.roleService = new RoleService();
            this.categoryService = new CategoryService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.disciplineService = new DisciplineService();
            this.currentmanpowerService = new CurrentManpowerService();
            this.userservice = new UserService();
        }

        /// <summary>
        /// Gets the role id.
        /// </summary>
        private int? RoleId
        {
            get
            {
                if (String.IsNullOrEmpty(Request[RoleParameterKey])) return null;

                int outValue;
                if (int.TryParse(Request[RoleParameterKey], out outValue))
                    return outValue;
                return null;
            }
        }

        /// <summary>
        /// The load data.
        /// </summary>
        private void LoadData()
        {
            if (this.RoleId == null)
            {
                return;
            }

            var selectedCategory = this.groupDataPermissionService.GetByRoleId(this.RoleId.Value).Select(t => t.CategoryIdList).ToList();

            foreach (RadComboBoxItem item in this.ddlCategory.Items)
            {
                if (selectedCategory.Contains(item.Value))
                {
                    item.Checked = true;
                }
            }

            var role = this.roleService.GetByID(this.RoleId.Value);
            this.txtRoleName.Text = role.Name;
            this.txtDescription.Text = role.Description;
            this.rbtnAdmin.Checked = role.IsAdmin.GetValueOrDefault();
            this.rbtnUpdate.Checked = role.IsUpdate.GetValueOrDefault();
            this.rbtnView.Checked = !role.IsAdmin.GetValueOrDefault() && !role.IsUpdate.GetValueOrDefault();
            this.txtRussiaName.Text = role.RussiaName;
            this.txtCode.Text = role.Code;
            this.cbAutonotification.Checked = role.Notify.GetValueOrDefault() ? true : false;
            this.cbworking.Checked = role.IsWorking.GetValueOrDefault() ? true : false;
            this.cbIsNipi.Checked = role.IsNipi.GetValueOrDefault();
            //this.ddlDiscipline.SelectedValue = role.DefaultDisciplineId.GetValueOrDefault().ToString();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (RoleId != null)
                {
                    //this.LoadComboData();
                    this.LoadData();
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            var role = new Role
            {
                Name = txtRoleName.Text.Trim(),
                Description = txtDescription.Text.Trim(),
                IsAdmin = this.rbtnAdmin.Checked,
                IsUpdate = this.rbtnUpdate.Checked,
                RussiaName = this.txtRussiaName.Text.Trim(),
                Code = this.txtCode.Text.Trim(),
                Notify = this.cbAutonotification.Checked,
                IsWorking = this.cbworking.Checked,
                IsNipi = this.cbIsNipi.Checked
                //DefaultDisciplineId = Convert.ToInt32(this.ddlDiscipline.SelectedValue),
                //DefaultDisciplineName = this.ddlDiscipline.SelectedItem.Text.Trim()
            };

            if (this.RoleId == null)
            {
                this.roleService.Insert(role);
            }
            else
            {
                role.Id = this.RoleId.Value;
                role.Description = this.txtDescription.Text.Trim();
                role.IsAdmin = this.rbtnAdmin.Checked;
                role.IsUpdate = this.rbtnUpdate.Checked;
                role.RussiaName = this.txtRussiaName.Text.Trim();
                role.Code = this.txtCode.Text.Trim();
                role.Notify = this.cbAutonotification.Checked;
                role.IsWorking = this.cbworking.Checked;
                role.IsNipi = this.cbIsNipi.Checked;
                //role.DefaultDisciplineId = Convert.ToInt32(this.ddlDiscipline.SelectedValue);
                //role.DefaultDisciplineName = this.ddlDiscipline.SelectedItem.Text.Trim();
                this.roleService.Update(role);

                this.GetCurrentManpower(role.Id);
                var groupDataPermissionList = this.groupDataPermissionService.GetByRoleId(this.RoleId.Value);
                foreach (var groupDataPermission in groupDataPermissionList)
                {
                    this.groupDataPermissionService.Delete(groupDataPermission);
                }
            }

            foreach (var radComboBoxItem in this.ddlCategory.CheckedItems)
            {
                var groupDataPermission = new GroupDataPermission()
                {
                    CategoryIdList = radComboBoxItem.Value,
                    RoleId = this.RoleId.Value
                };

                this.groupDataPermissionService.Insert(groupDataPermission);
            }

            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validate.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        protected void ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtRoleName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Group name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }


        private void GetCurrentManpower(int roldeId)
        {
            var currentrole = this.currentmanpowerService.GetDepartment(roldeId, DateTime.Now.Year);
            if (currentrole == null)
            {
                var countuser = this.userservice.GetAllByRoleId(roldeId).Where(t => t.IsLeader.GetValueOrDefault() || t.IsEngineer.GetValueOrDefault()).Count().ToString();
                var daystart = new DateTime(DateTime.Now.Date.Year, 01, 01);
                var dayend = new DateTime(DateTime.Now.Date.Year, 12, 30);
                var actual = "";
                for (var i = daystart;
                  i <= dayend;
                  i = i.AddMonths(1))
                {
                    actual += countuser + "$";
                }
                actual = actual.Substring(0, actual.Length - 1);
                var current = new CurrentManpower()
                {
                    DepartmentId = roldeId,
                    Year = DateTime.Now.Year,
                    Values = actual,
                };
                this.currentmanpowerService.Insert(current);
            }

        }
        /// <summary>
        /// The load combo data.
        /// </summary>
        private void LoadComboData()
        {
            var listCategory = this.categoryService.GetAll();
            this.ddlCategory.DataSource = listCategory;
            this.ddlCategory.DataTextField = "Name";
            this.ddlCategory.DataValueField = "ID";
            this.ddlCategory.DataBind();

            var disciplineList = this.disciplineService.GetAll();
            disciplineList.Insert(0, new Discipline() { ID = 0 });
            this.ddlDiscipline.DataSource = disciplineList;
            this.ddlDiscipline.DataTextField = "FullName";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataBind();
        }
    }
}
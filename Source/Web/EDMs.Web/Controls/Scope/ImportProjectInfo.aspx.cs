﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Hosting;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportProjectInfo : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;
        private readonly ScopeProjectService scopeProjectService;
        private readonly FieldService fieldService;
        private readonly PlatformService platformService;
        private readonly UserService userService;
        private readonly SupervisorService supervisorService;
        private readonly AttachFilesProjectService attachFilesProjectService;

        

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportProjectInfo()
        {
            this.documentService = new DocumentService();
            this.scopeProjectService = new ScopeProjectService();
            this.fieldService = new FieldService();
            this.platformService = new PlatformService();
            this.userService = new UserService();
            this.supervisorService = new SupervisorService();
            this.attachFilesProjectService = new AttachFilesProjectService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var dataSheet = workbook.Worksheets[1];

                        // Create a datatable
                        var dataTable = new DataTable();

                        if (!string.IsNullOrEmpty(Request.QueryString["type"]) && Request.QueryString["type"] == "addnew")
                        {
                            // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                            dataTable = dataSheet.Cells.ExportDataTable(2, 1, dataSheet.Cells.MaxRow, 14);

                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!string.IsNullOrEmpty(dataRow["Column3"].ToString()))
                                {
                                    var projectObj = new ScopeProject();
                                    var fieldObj = this.fieldService.GetByName(dataRow["Column1"].ToString().Trim());
                                    var platformObj = this.platformService.GetByName(dataRow["Column2"].ToString().Trim());
                                    var projectManager = this.userService.GetByUserName(dataRow["Column8"].ToString().Trim());
                                    var supervisor = this.userService.GetByUserName(dataRow["Column9"].ToString().Trim());

                                    projectObj.FieldId = fieldObj != null ? fieldObj.ID : 0;
                                    projectObj.FieldName = fieldObj != null ? fieldObj.Name : string.Empty;

                                    projectObj.PlatformId = platformObj != null ? platformObj.ID : 0;
                                    projectObj.PlatformName = platformObj != null ? platformObj.Name : string.Empty;

                                    projectObj.Name = dataRow["Column3"].ToString().Trim();
                                    projectObj.Description = dataRow["Column4"].ToString().Trim();
                                    projectObj.ScopeOfWork = dataRow["Column5"].ToString().Trim();

                                    var projectType = dataRow["Column6"].ToString().Trim();
                                    switch (projectType)
                                    {
                                        case "":
                                            projectObj.ProjectTypeId = 0;
                                            projectObj.ProjectTypeName = string.Empty;
                                            break;
                                        case "Planned":
                                            projectObj.ProjectTypeId = 1;
                                            projectObj.ProjectTypeName = "Planned";
                                            break;
                                        case "Unplanned":
                                            projectObj.ProjectTypeId = 2;
                                            projectObj.ProjectTypeName = "Unplanned";
                                            break;
                                    }

                                    try
                                    {
                                        projectObj.Year = Convert.ToInt32(dataRow["Column7"].ToString().Trim());
                                    }
                                    catch (Exception)
                                    {
                                        projectObj.Year = 0;
                                    }
                                    projectObj.ProjectManagerId = projectManager != null ? projectManager.Id : 0;
                                    projectObj.ProjectManagerFullName = projectManager != null 
                                        ? projectManager.UserNameWithFullName 
                                        : string.Empty;

                                    projectObj.SupervisorId = supervisor != null ? supervisor.Id : 0;
                                    projectObj.SupervisorFullName = supervisor != null 
                                        ? supervisor.UserNameWithFullName 
                                        : string.Empty;
                                    projectObj.SupervisorUserName = supervisor != null 
                                        ? supervisor.Username 
                                        : string.Empty;

                                    var strStartDate = dataRow["Column10"].ToString().Trim();
                                    var startDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(strStartDate, ref startDate))
                                    {
                                        projectObj.StartDate = startDate;
                                    }

                                    var strEndDate = dataRow["Column11"].ToString().Trim();
                                    var endDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(strEndDate, ref endDate))
                                    {
                                        projectObj.EndDate = endDate;
                                    }

                                    try
                                    {
                                        projectObj.Complete = Convert.ToDouble(dataRow["Column12"].ToString().Trim());
                                    }
                                    catch (Exception)
                                    {
                                        projectObj.Complete = 0;
                                    }
                                    projectObj.CanDelete = true;
                                    projectObj.IsAutoCalculate = dataRow["Column13"].ToString().Trim().ToLower() == "x";
                                    var projectId = this.scopeProjectService.Insert(projectObj);

                                    if (projectId != null && !string.IsNullOrEmpty(dataRow["Column14"].ToString().Trim()))
                                    {
                                        var attachFiles = dataRow["Column14"].ToString().Trim();
                                        var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/ProjectFiles";
                                        foreach (var fileName in attachFiles.Split(';'))
                                        {
                                            var docFileName = fileName.Substring(fileName.LastIndexOf("/") + 1, fileName.Length - fileName.LastIndexOf("/") - 1);

                                            // Path file to download from server
                                            var serverFilePath = serverFolder + "/" + fileName;
                                            var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);

                                            var attachFile = new AttachFilesProject()
                                            {
                                                ProjectId = projectId,
                                                FileName = docFileName,
                                                Extension = fileExt,
                                                FilePath = serverFilePath,
                                                ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                                                //FileSize = (double)docFile.ContentLength / 1024,
                                                CreatedBy = UserSession.Current.User.Id,
                                                CreatedDate = DateTime.Now
                                            };

                                            this.attachFilesProjectService.Insert(attachFile);
                                        }
                                    }
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(Request.QueryString["type"]) && Request.QueryString["type"] == "update")
                        {
                            
                        }
                    }
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);

            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}
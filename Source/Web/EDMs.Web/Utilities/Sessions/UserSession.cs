﻿using System;
using System.Configuration;

namespace EDMs.Web.Utilities.Sessions
{
    using System.Web;
    using EDMs.Business.Services.Library;
    using EDMs.Data.Entities;

    /// <summary>
    /// The user session.
    /// </summary>
    public class UserSession
    {
        /// <summary>
        /// Creates the session.
        /// </summary>
        /// <param name="user">The user.</param>
        public static void CreateSession(User user)
        {
            var resourceService = new ResourceService();
            var resource = user.ResourceId != null ? resourceService.GetByID(user.ResourceId.Value) : null;

            var session = new UserSession
                              {
                                  User = user,
                                  IsResource = (resource != null && resource.IsResource != null) && resource.IsResource.Value,
                                  ResourceId = resource != null ? resource.Id : -1,
                                  RoleId = user.RoleId != null ? user.RoleId.Value : -1,
                                  IsAvailable = true,
                                  IsAdmin = user.IsAdmin.GetValueOrDefault(),
                                  IsCheif = user.IsChief.GetValueOrDefault(),
                                  IsDC = user.IsDC.GetValueOrDefault(),
                                  IsEngineer = user.IsEngineer.GetValueOrDefault(),
                                  IsGip = user.IsGip.GetValueOrDefault(),
                                  IsSuperViewer = user.IsSupperViewer.GetValueOrDefault(),
                                  IsLeader = user.IsLeader.GetValueOrDefault()
                              };
            HttpContext.Current.Session["__UserSession__"] = session;
            HttpContext.Current.Session.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TimeOut"));
        }

        /// <summary>
        /// Destroys the session.
        /// </summary>
        public static void DestroySession()
        {
            HttpContext.Current.Session["__UserSession__"] = null;
        }

        /// <summary>
        /// Checks the session and navigate.
        /// </summary>
        public static void CheckSessionAndNavigate()
        {
            if(HttpContext.Current.Session["__UserSession__"] == null)
            {
                HttpContext.Current.Response.Redirect(GlobalConsts.LoginFormPath);
            }
        }

        /// <summary>
        /// Gets the current.
        /// </summary>
        /// <value>
        /// The current.
        /// </value>
        public static UserSession Current
        {
            get
            {
                if (HttpContext.Current.Session != null)
                {
                    var session = (UserSession)HttpContext.Current.Session["__UserSession__"] ??
                                 new UserSession { IsAvailable = false };
                    return session;   
                }
                return null;
            }
        }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Gets the role id.
        /// </summary>
        public int RoleId { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether is resource.
        /// </summary>
        public bool IsResource { get; set; }

        /// <summary>
        /// Gets or sets the resource id.
        /// </summary>
        public int ResourceId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is available.
        /// </summary>
        public bool IsAvailable { get; set; }

        public bool IsAdmin { get; set; }
        public bool IsDC { get; set; }
        public bool IsGip { get; set; }
        public bool IsCheif { get; set; }
        public bool IsEngineer { get; set; }
        public bool IsSuperViewer { get; set; }
        public bool IsLeader { get; set; }
        public int LogId { get; set; }
    }
}
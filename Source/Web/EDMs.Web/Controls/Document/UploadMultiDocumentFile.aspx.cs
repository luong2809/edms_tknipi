﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using System.Security.Cryptography;

namespace EDMs.Web.Controls.Document
{
    using Business.Services.Function;
    using Business.Services.Security;
    using EDMs.Business.Services.Document;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;
    using Function;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Telerik.Web.UI;
    using Utilities;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class UploadMultiDocumentFile : Page
    {

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly WorkGroupService workGroupService;

        private readonly AttachFilesPackageService attachFileService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly FolderService folderService;

        private readonly DocumentService documentService;

        private readonly MilestoneService milestoneService;
        private readonly ProcessActualService processActualService;
        private readonly ProcessPlanedService processPlanedService;
        private readonly UserManhourService userManhourService = new UserManhourService();
        private readonly UserManhourMonthService userManhourMonthService = new UserManhourMonthService();
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();
        private readonly ProcessBussiness processBussiness = new ProcessBussiness();
        private readonly CountPage countPage = new CountPage();
        private readonly UserService userService = new UserService();
        private readonly EmailNotificationTemplateService emailNotificationTemplateService = new EmailNotificationTemplateService();
        private readonly DistributionMatrixDetailService distributionMatrixDetailService = new DistributionMatrixDetailService();
        private readonly StatusService statusService = new StatusService();
        private readonly DisciplineService disciplineService = new DisciplineService();
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public UploadMultiDocumentFile()
        {
            this.workGroupService = new WorkGroupService();
            this.attachFileService = new AttachFilesPackageService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.folderService = new FolderService();
            this.documentService = new DocumentService();
            this.milestoneService = new MilestoneService();
            this.processPlanedService = new ProcessPlanedService();
            this.processActualService = new ProcessActualService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.cbAutoUpdateEMDR.Visible = !string.IsNullOrEmpty(this.Request.QueryString["folId"]) ? true : false;
        }

        private void NotificationUploadFileIDC(DocumentPackage documentObj)
        {
            var projectID = Convert.ToInt32(documentObj.ProjectId.GetValueOrDefault());
            var projectObj = this.scopeProjectService.GetById(projectID);
            if (projectObj != null)
            {
                if (projectObj.IsIDC.GetValueOrDefault())
                {
                    if (documentObj.EngineerId != "0")
                    {
                        var engineer = new List<User>();
                        var leaderDiscipline = new List<User>();
                        if (documentObj.EngineerId.Contains(","))
                        {
                            var listEng = documentObj.EngineerId.Split(',');
                            foreach (var item in listEng)
                            {
                                engineer.Add(this.userService.GetByID(item));
                            }
                        }
                        else
                        {
                            engineer.Add(this.userService.GetByID(Convert.ToInt32(documentObj.EngineerId)));
                        }
                        if (engineer != null && engineer.Count > 0)
                        {
                            string strEnginner = string.Join(" - ", engineer.Select(t => t.FullName));
                            var matrix = new List<DistributionMatrixDetail>();
                            if (string.IsNullOrEmpty(documentObj.ParentId.ToString()))
                            {
                                matrix = this.distributionMatrixDetailService.GetByDocument(documentObj.ID);
                            }
                            else
                            {
                                matrix = this.distributionMatrixDetailService.GetByDocument(documentObj.ParentId.Value);
                            }
                            foreach (var item in matrix)
                            {
                                //var disciplineObj = this.disciplineService.GetById(item.DisciplineID.GetValueOrDefault());
                                //var chief = this.userService.GetAllByRoleId(disciplineObj.DefaultDepartmentId.GetValueOrDefault()).Where(t => t.IsChief.GetValueOrDefault()).ToList();
                                var docList = this.documentPackageService.GetAllEMDRByDisciplineAndProject(item.DisciplineID.GetValueOrDefault(), item.ProjectID.GetValueOrDefault());
                                var leaderList = docList.Select(t => t.LeaderId.GetValueOrDefault()).Distinct().ToList();
                                leaderDiscipline.AddRange(this.userService.GetByListID(leaderList));
                                //leaderDiscipline.AddRange(chief);
                            }
                            leaderDiscipline = leaderDiscipline.Distinct().ToList();
                            var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.UploadFileIDC);
                            if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                            {
                                var smtpClient = new SmtpClient
                                {
                                    DeliveryMethod = SmtpDeliveryMethod.Network,
                                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                                    Host = ConfigurationManager.AppSettings["Host"],
                                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                                };

                                var wpObj = this.workGroupService.GetById(documentObj.WorkgroupId.GetValueOrDefault());
                                var subject = notificationTemplate.Subject.Replace("#DocNumber#", documentObj.DocNo).Replace("#AssignedUser#", strEnginner);

                                var message = new MailMessage();
                                message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                                message.Subject = subject;
                                message.BodyEncoding = new UTF8Encoding();
                                message.IsBodyHtml = true;
                                message.Body = notificationTemplate.Contents
                                    .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                                    .Replace("#DocNumber#", documentObj.DocNo)
                                    .Replace("#AssignedUser#", strEnginner)
                                    .Replace("#DocTitle#", documentObj.DocTitle)
                                    .Replace("#DocType#", documentObj.DocumentTypeName)
                                    .Replace("#DocRev#", documentObj.RevisionName)
                                    .Replace("#DocEng#", strEnginner)
                                    .Replace("#DocStartDate#", documentObj.StartDate != null ? documentObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                                    .Replace("#DocDeadline#", documentObj.Deadline != null ? documentObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                                    .Replace("#IDCDeadline#", documentObj.IDCDeadline != null ? documentObj.IDCDeadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                                    .Replace("#DocFinishDate#", documentObj.EndDate != null ? documentObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                                    .Replace("#DocISODate#", documentObj.IsoReviseDate != null ? documentObj.IsoReviseDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                                    .Replace("#DocWeight#", documentObj.Weight != null ? documentObj.Weight.Value.ToString() : string.Empty)
                                    .Replace("#DocComplete#", documentObj.Complete != null ? documentObj.Complete.Value.ToString() : string.Empty);
                                if (ConfigurationManager.AppSettings["SendOnlySupport"].ToLower() == "true")
                                {
                                    message.To.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                                }
                                else
                                {
                                    foreach (var item in leaderDiscipline)
                                    {
                                        if (!string.IsNullOrEmpty(item.Email))
                                        {
                                            message.To.Add(item.Email);
                                        }
                                    }
                                    foreach (var item in engineer)
                                    {
                                        if (!string.IsNullOrEmpty(item.Email))
                                        {
                                            message.To.Add(item.Email);
                                        }
                                    }

                                    var leader = this.userService.GetByID(documentObj.LeaderId.ToString());
                                    if (leader != null)
                                    {
                                        if (!string.IsNullOrEmpty(leader.Email))
                                        {
                                            message.CC.Add(leader.Email);
                                        }
                                    }

                                    var GIP = this.userService.GetByID(projectObj.ProjectManagerId.ToString());
                                    if (GIP != null)
                                    {
                                        if (!string.IsNullOrEmpty(GIP.Email))
                                        {
                                            message.CC.Add(GIP.Email);
                                        }
                                    }
                                    var DC = this.userService.GetByID(projectObj.DocumentControlId.ToString());
                                    if (DC != null)
                                    {
                                        if (!string.IsNullOrEmpty(DC.Email))
                                        {
                                            message.CC.Add(DC.Email);
                                        }
                                    }
                                    var adminSPF = this.userService.GetByID("342");
                                    if (adminSPF != null)
                                    {
                                        if (!string.IsNullOrEmpty(adminSPF.Email))
                                        {
                                            message.CC.Add(adminSPF.Email);
                                        }
                                    }
                                }

                                smtpClient.Send(message);
                            }
                        }
                    }
                }
            }

        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //var oldComplete = 0.0;
                //var oldManhourActual = 0.0;
                //var oldManhourPlan = 0.0;
                //var deltaComplete = 0.0;
                if (!string.IsNullOrEmpty(this.Request.QueryString["projectId"]))
                {
                    var projectId = Convert.ToInt32(this.Request.QueryString["projectId"]);
                    var projectObj = this.scopeProjectService.GetById(projectId);
                    const string TargetFolder = "../../DocumentLibrary";
                    var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/"
                        ? string.Empty
                        : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary";
                    var listUpload = docuploader.UploadedFiles;
                    var splitAttachFileNameCharacter =
                                        ConfigurationManager.AppSettings.Get("SplitAttachFileNameCharacter");

                    var listExistFile = new List<string>();

                    var fileIcon = new Dictionary<string, string>()
                {
                    {"doc", "~/images/wordfile.png"},
                    {"docx", "~/images/wordfile.png"},
                    {"dotx", "~/images/wordfile.png"},
                    {"xls", "~/images/excelfile.png"},
                    {"xlsx", "~/images/excelfile.png"},
                    {"pdf", "~/images/pdffile.png"},
                    {"7z", "~/images/7z.png"},
                    {"dwg", "~/images/dwg.png"},
                    {"dxf", "~/images/dxf.png"},
                    {"rar", "~/images/rar.png"},
                    {"zip", "~/images/zip.png"},
                    {"txt", "~/images/txt.png"},
                    {"xml", "~/images/xml.png"},
                    {"xlsm", "~/images/excelfile.png"},
                    {"bmp", "~/images/bmp.png"},
                };

                    if (listUpload.Count > 0)
                    {
                        List<int> wpList = new List<int>();
                        var uncompleteUploadFileList = new List<string>();
                        var countPageUpload = new List<string>();
                        foreach (UploadedFile docFile in listUpload)
                        {
                            //var docFileName = docFile.FileName;
                            var docFileName = Utility.RemoveAllSpecialCharacter(docFile.FileName);
                            var serverDocFileName = docFileName;

                            var docInfo = docFileName.LastIndexOf(splitAttachFileNameCharacter.ToString());
                            var docNo = docFileName.Substring(0, docInfo - 2);
                            var docRev = docFileName.Substring(docInfo - 1, 1);
                            var documentObj = this.documentPackageService.GetOneByDocNo(docNo, docRev, projectId);
                            var listDoc = this.documentPackageService.GetAllByDocNo(docNo, docRev, projectId);
                            if (documentObj != null)
                            {
                                serverDocFileName = DateTime.Now.ToBinary() + serverDocFileName;

                                // Path file to save on server disc
                                var saveFilePath = Path.Combine(Server.MapPath(TargetFolder),
                                    serverDocFileName);

                                // Path file to download from server
                                var serverFilePath = serverFolder + "/" + serverDocFileName;
                                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1,
                                    docFileName.Length - docFileName.LastIndexOf(".") - 1);

                                docFile.SaveAs(saveFilePath, true);
                                countPageUpload = countPage.CountPageUpload(saveFilePath);

                                foreach (var item in listDoc)
                                {
                                    var attachFile = new AttachFilesPackage()
                                    {
                                        DocumentPackageID = item.ID,
                                        FileName = docFileName,
                                        Extension = fileExt,
                                        FilePath = serverFilePath,
                                        ExtensionIcon =
                                    fileIcon.ContainsKey(fileExt.ToLower())
                                        ? fileIcon[fileExt.ToLower()]
                                        : "~/images/otherfile.png",
                                        FileSize = (double)docFile.ContentLength / 1024,

                                        AttachType = 1,
                                        AttachTypeName = "Document file",
                                        CreatedBy = UserSession.Current.User.Id,
                                        CreatedDate = DateTime.Now
                                    };

                                    this.attachFileService.Insert(attachFile);
                                    if (projectObj != null)
                                    {
                                        if (projectObj.IsIDC.GetValueOrDefault())
                                        {
                                            var statusObj = this.statusService.GetById(item.StatusID.GetValueOrDefault());
                                            if (item.StatusID == 6)
                                            {
                                                item.Complete = statusObj.Complete;
                                                item.IDCEndDate = DateTime.Now;
                                                if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                                                {
                                                    this.NotificationUploadFileIDC(documentObj);
                                                }
                                            }
                                            else if (item.StatusID == 7)
                                            {
                                                item.Complete = statusObj.Complete;
                                                item.IFREndDate = DateTime.Now;
                                            }
                                            else if (item.StatusID == 8)
                                            {
                                                item.Complete = statusObj.Complete;
                                                item.EndDate = DateTime.Now;
                                            }
                                            item.HasAttachFile = true;
                                            this.documentPackageService.Update(item);
                                        }
                                        else
                                        {
                                            item.Complete = 100;
                                            item.EndDate = DateTime.Now;
                                            item.HasAttachFile = true;
                                            this.documentPackageService.Update(item);
                                        }
                                    }

                                    UserManhour userManhourObj = new UserManhour();
                                    UserManhourMonth userManhourMonthObj = new UserManhourMonth();
                                    //update manhour
                                    UpdateUserManhourWeek(documentObj, UserSession.Current.User, userManhourObj);
                                    UpdateUserManhourMonth(documentObj, UserSession.Current.User, userManhourMonthObj);
                                }
                                wpList.Add(documentObj.WorkgroupId.GetValueOrDefault());
                            }
                            else
                            {
                                uncompleteUploadFileList.Add(docFile.FileName);
                            }
                        }
                        if (wpList.Count > 0)
                        {
                            foreach (var item in wpList.Distinct())
                            {
                                var wpObj = this.workGroupService.GetById(item);
                                if (wpObj != null)
                                {
                                    var docList = this.documentPackageService.GetAllEMDRByWorkgroup(wpObj.ID);
                                    double complete = 0;
                                    Milestone milestoneObj = new Milestone();
                                    ProcessActual processActualObj = new ProcessActual();
                                    List<WorkgroupManhourMonth> wmmList = new List<WorkgroupManhourMonth>();
                                    WorkgroupManhourMonth wmmObj = new WorkgroupManhourMonth();

                                    //update weight cho document
                                    UpdateWeightDocument(projectObj, ref docList);
                                    var wpUpdateList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                                    //update data workpackage
                                    UpdateDataWorkpackage(projectObj, docList, ref wpObj, ref wpUpdateList, ref complete);
                                    if (wpObj.IsAutoCalculate.GetValueOrDefault())
                                    {
                                        if ((complete < 100 && docList.Where(t => t.Complete != 100).Any()))
                                        {
                                            //update milestone
                                            UpdateMilestonActual(wpObj, UserSession.Current.User, milestoneObj);
                                        }
                                    }
                                    //Update actual progress
                                    UpdateProgressActual(wpObj, processActualObj);

                                    // update data Project
                                    UpdateDataProject(wpUpdateList, projectObj);

                                    //Update all wpWeight trong thang workgroup manhour month cua project
                                    UpdateWeightWorkgroupManhourMonth(wpObj, wmmList);

                                    UpdateDataWorkgroupManhourMonth(wpObj, UserSession.Current.User, wpUpdateList, wmmObj);
                                }
                            }
                        }


                        this.docuploader.UploadedFiles.Clear();

                        if (uncompleteUploadFileList.Count > 0)
                        {
                            this.blockError.Visible = true;
                            this.lblError.Text = "Can't find some document with Document number: <br/>";
                            foreach (var item in uncompleteUploadFileList)
                            {
                                this.lblError.Text += "<span style='color: blue; font-weight: bold'>'" + item.Split(splitAttachFileNameCharacter.ToCharArray())[0] + "'</span> to attach file <span style='color: orange; font-weight: bold'>'" + item + "'</span> <br/>";
                            }
                        }
                        else
                        {
                            this.blockError.Visible = true;
                            this.lblError.Text += "All document files upload complete.<br/>";
                            this.lblError.Text += "Count page<br/>";
                            if (countPageUpload.Count>0)
                            {
                                foreach (var item in countPageUpload)
                                {
                                    this.lblError.Text += item + "<br/>";
                                }
                            }

                        }
                    }
                }
                else if (!string.IsNullOrEmpty(this.Request.QueryString["folId"]))
                {
                    const string TargetFolder = "../../DocumentLibrary";
                    var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/"
                        ? string.Empty
                        : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary";
                    var listUpload = docuploader.UploadedFiles;
                    var splitAttachFileNameCharacter =
                                        ConfigurationManager.AppSettings.Get("SplitAttachFileNameCharacter");

                    var listExistFile = new List<string>();

                    var fileIcon = new Dictionary<string, string>()
                {
                    {"doc", "~/images/wordfile.png"},
                    {"docx", "~/images/wordfile.png"},
                    {"dotx", "~/images/wordfile.png"},
                    {"xls", "~/images/excelfile.png"},
                    {"xlsx", "~/images/excelfile.png"},
                    {"pdf", "~/images/pdffile.png"},
                    {"7z", "~/images/7z.png"},
                    {"dwg", "~/images/dwg.png"},
                    {"dxf", "~/images/dxf.png"},
                    {"rar", "~/images/rar.png"},
                    {"zip", "~/images/zip.png"},
                    {"txt", "~/images/txt.png"},
                    {"xml", "~/images/xml.png"},
                    {"xlsm", "~/images/excelfile.png"},
                    {"bmp", "~/images/bmp.png"},
                };

                    if (listUpload.Count > 0)
                    {
                        var folderId = Convert.ToInt32(Request.QueryString["folId"]);
                        var folder = this.folderService.GetById(folderId);
                        var projectObj = this.scopeProjectService.GetById(folder.ProjectId.GetValueOrDefault());
                        //var wpObj = this.workGroupService.GetById(folder.WorkgroupID.GetValueOrDefault());
                        List<int> wpList = new List<int>();
                        var uncompleteUploadFileList = new List<string>();
                        var wrongNameUploadFileList = new List<string>();
                        foreach (UploadedFile docFile in listUpload)
                        {
                            //var docFileName = docFile.FileName.Replace('&', '-');
                            var docFileName = Utility.RemoveAllSpecialCharacter(docFile.FileName);
                            var serverDocFileName = docFileName;
                            var PathFolder = string.Empty;
                            if (folder != null)
                            {
                                // Path file to save on server disc
                                var saveFilePath = Path.Combine(Server.MapPath(folder.DirName), serverDocFileName);
                                // Path file to download from server
                                var serverFilePath = folder.DirName + "/" + serverDocFileName;
                                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1,
                                    docFileName.Length - docFileName.LastIndexOf(".") - 1);
                                if (!File.Exists(saveFilePath))
                                {
                                    //     try
                                    //     {
                                    //         FileInfo fi = new FileInfo(saveFilePath);
                                    //         if (fi.IsReadOnly)
                                    //             fi.IsReadOnly = false;
                                    //         File.SetAttributes(saveFilePath, FileAttributes.Normal);
                                    //         File.Delete(saveFilePath);
                                    //     }
                                    //     catch { }
                                    // }
                                    //// else{

                                    if (this.cbAutoUpdateEMDR.Checked)
                                    {
                                        var docNo = "";
                                        var docRev = "";
                                        try
                                        {
                                            var docInfo = docFileName.LastIndexOf(splitAttachFileNameCharacter.ToString());
                                            var vartemp = docFileName.Substring(0, docInfo).LastIndexOf(splitAttachFileNameCharacter.ToString());
                                            docNo = docFileName.Substring(0, vartemp);
                                            docRev = docFileName.Substring(vartemp + 1, docInfo - vartemp - 1);
                                        }
                                        catch (Exception ex)
                                        {
                                            wrongNameUploadFileList.Add(docFileName);
                                        }
                                        var documentObj = this.documentPackageService.GetOneByDocNo(docNo, docRev, Convert.ToInt32(folder.ProjectId));
                                        var listDoc = this.documentPackageService.GetAllByDocNo(docNo, docRev, Convert.ToInt32(folder.ProjectId));
                                        if (documentObj != null)
                                        {

                                            var documentinFolder = this.documentService.GetAllByNameAndFolder(docFile.FileName, folderId);
                                            if (documentinFolder == null)
                                            {
                                                var document = new Document()
                                                {
                                                    Name = docFileName,
                                                    FileExtension = fileExt,
                                                    FileExtensionIcon =
                                            fileIcon.ContainsKey(fileExt.ToLower())
                                                ? fileIcon[fileExt.ToLower()]
                                                : "~/images/otherfile.png",
                                                    FilePath = serverFilePath,
                                                    FolderID = folderId,
                                                    IsLeaf = true,
                                                    IsDelete = false,
                                                    CreatedBy = UserSession.Current.User.Id,
                                                    CreatedDate = DateTime.Now
                                                };
                                                var idFile = this.documentService.Insert(document);
                                            }
                                            docFile.SaveAs(saveFilePath, true);

                                            serverDocFileName = DateTime.Now.ToBinary() + serverDocFileName;

                                            // Path file to save on server disc
                                            var newsaveFilePath = Path.Combine(Server.MapPath(TargetFolder),
                                                serverDocFileName);

                                            // Path file to download from server
                                            serverFilePath = serverFolder + "/" + serverDocFileName;
                                            fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1,
                                                docFileName.Length - docFileName.LastIndexOf(".") - 1);

                                            if (File.Exists(saveFilePath))
                                            {
                                                File.Copy(saveFilePath, newsaveFilePath, true);
                                            }

                                            foreach (var item in listDoc)
                                            {
                                                var attachFile = new AttachFilesPackage()
                                                {
                                                    DocumentPackageID = item.ID,
                                                    FileName = docFileName,
                                                    Extension = fileExt,
                                                    FilePath = serverFilePath,
                                                    ExtensionIcon =
                                                fileIcon.ContainsKey(fileExt.ToLower())
                                                    ? fileIcon[fileExt.ToLower()]
                                                    : "~/images/otherfile.png",
                                                    FileSize = (double)docFile.ContentLength / 1024,

                                                    AttachType = 1,
                                                    AttachTypeName = "Document file",
                                                    CreatedBy = UserSession.Current.User.Id,
                                                    CreatedDate = DateTime.Now
                                                };

                                                this.attachFileService.Insert(attachFile);

                                                if (projectObj.IsIDC.GetValueOrDefault())
                                                {
                                                    if (item.StatusID == 6)
                                                    {
                                                        item.Complete = 60;
                                                        item.IDCEndDate = DateTime.Now;
                                                        item.HasAttachFile = true;
                                                        this.documentPackageService.Update(item);
                                                        if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                                                        {
                                                            this.NotificationUploadFileIDC(documentObj);
                                                        }
                                                    }
                                                    else if (item.StatusID == 7)
                                                    {
                                                        item.Complete = 80;
                                                        item.IFREndDate = DateTime.Now;
                                                        item.HasAttachFile = true;
                                                        this.documentPackageService.Update(item);
                                                    }
                                                    else if (item.StatusID == 8)
                                                    {
                                                        item.Complete = 100;
                                                        item.EndDate = DateTime.Now;
                                                        item.HasAttachFile = true;
                                                        this.documentPackageService.Update(item);
                                                    }
                                                }
                                                else
                                                {
                                                    item.Complete = 100;
                                                    item.EndDate = DateTime.Now;
                                                    item.HasAttachFile = true;
                                                    this.documentPackageService.Update(item);
                                                }

                                                UserManhour userManhourObj = new UserManhour();
                                                UserManhourMonth userManhourMonthObj = new UserManhourMonth();
                                                //update manhour
                                                UpdateUserManhourWeek(documentObj, UserSession.Current.User, userManhourObj);
                                                UpdateUserManhourMonth(documentObj, UserSession.Current.User, userManhourMonthObj);
                                            }
                                            var wpObj = this.workGroupService.GetById(documentObj.WorkgroupId.GetValueOrDefault());
                                            if (wpObj != null)
                                            {
                                                wpList.Add(wpObj.ID);
                                                //processBussiness.UpdateDataAfterEditDocument(wpObj, UserSession.Current.User);

                                                //Latest Revision
                                                PathFolder = "../.." + wpObj.PathLatestRevision;
                                                if (!string.IsNullOrEmpty(wpObj.PathLatestRevision) && Directory.Exists(PathFolder) && wpObj.PathLatestRevision.Contains("DocumentLibrary/Latest Revision"))
                                                {
                                                    var newsaveFile = Path.Combine(Server.MapPath(PathFolder), docFileName);

                                                    File.Copy(saveFilePath, newsaveFile, true);
                                                    // Delete old docRev on Latest folder
                                                    if (documentObj.ParentId != null)
                                                    {
                                                        var docRelatedList = this.documentPackageService.GetAllRevDoc(documentObj.ParentId.Value).Where(t => t.ID != documentObj.ID).Select(t => t.ID).ToList();
                                                        var attachDocList = this.attachFileService.GetAllByDocId(docRelatedList);
                                                        foreach (var attachDoc in attachDocList)
                                                        {
                                                            var attachFilePath = Path.Combine(Server.MapPath(PathFolder), attachDoc.FileName);
                                                            if (File.Exists(attachFilePath))
                                                            {
                                                                File.Delete(attachFilePath);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //    var documentObjfile = this.documentService.GetById((int)idFile);
                                            //    if (documentObjfile != null)
                                            //    {
                                            //        var filePathServer = Server.MapPath(documentObjfile.FilePath);
                                            //        if (File.Exists(filePathServer))
                                            //        {
                                            //            File.Delete(filePathServer);
                                            //        }
                                            //        this.documentService.Delete(documentObjfile);
                                            //    }
                                            uncompleteUploadFileList.Add(docFile.FileName);
                                        }
                                    }
                                    else
                                    {
                                        var document = new Document()
                                        {
                                            Name = docFileName,
                                            FileExtension = fileExt,
                                            FileExtensionIcon =
                                        fileIcon.ContainsKey(fileExt.ToLower())
                                            ? fileIcon[fileExt.ToLower()]
                                            : "~/images/otherfile.png",
                                            FilePath = serverFilePath,
                                            FolderID = folderId,
                                            IsLeaf = true,
                                            IsDelete = false,
                                            CreatedBy = UserSession.Current.User.Id,
                                            CreatedDate = DateTime.Now
                                        };

                                        var idFile = this.documentService.Insert(document);
                                        docFile.SaveAs(saveFilePath, true);
                                    }
                                }
                                else
                                {
                                    listExistFile.Add(docFileName);
                                }
                            }
                        }
                        if (wpList.Count > 0 && this.cbAutoUpdateEMDR.Checked)
                        {
                            foreach (var item in wpList.Distinct())
                            {
                                var wpObj = this.workGroupService.GetById(item);
                                if (wpObj != null)
                                {
                                    var docList = this.documentPackageService.GetAllEMDRByWorkgroup(wpObj.ID);
                                    double complete = 0;
                                    Milestone milestoneObj = new Milestone();
                                    ProcessActual processActualObj = new ProcessActual();
                                    List<WorkgroupManhourMonth> wmmList = new List<WorkgroupManhourMonth>();
                                    WorkgroupManhourMonth wmmObj = new WorkgroupManhourMonth();

                                    //update weight cho document
                                    UpdateWeightDocument(projectObj, ref docList);
                                    var wpUpdateList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                                    //update data workpackage
                                    UpdateDataWorkpackage(projectObj, docList, ref wpObj, ref wpUpdateList, ref complete);
                                    if (wpObj.IsAutoCalculate.GetValueOrDefault())
                                    {
                                        if ((complete < 100 && docList.Where(t => t.Complete != 100).Any()))
                                        {
                                            //update milestone
                                            UpdateMilestonActual(wpObj, UserSession.Current.User, milestoneObj);
                                        }
                                    }
                                    //Update actual progress
                                    UpdateProgressActual(wpObj, processActualObj);

                                    // update data Project
                                    UpdateDataProject(wpUpdateList, projectObj);

                                    //Update all wpWeight trong thang workgroup manhour month cua project
                                    UpdateWeightWorkgroupManhourMonth(wpObj, wmmList);

                                    UpdateDataWorkgroupManhourMonth(wpObj, UserSession.Current.User, wpUpdateList, wmmObj);
                                }
                            }
                        }

                        this.docuploader.UploadedFiles.Clear();

                        if (uncompleteUploadFileList.Count > 0)
                        {
                            this.blockError.Visible = true;
                            this.lblError.Text = "Can't find some document with Document number: <br/>";
                            foreach (var item in uncompleteUploadFileList)
                            {
                                this.lblError.Text += "<span style='color: blue; font-weight: bold'>'" + item.Split(splitAttachFileNameCharacter.ToCharArray())[0] + "'</span> to attach file <span style='color: orange; font-weight: bold'>'" + item + "'</span> <br/>";
                            }
                        }
                        if (listExistFile.Count > 0)
                        {
                            this.blockError.Visible = true;
                            this.lblError.Text += "This folder is have exist files: <br/>";
                            foreach (var item in listExistFile)
                            {
                                this.lblError.Text += "<span style='color: blue; font-weight: bold'>'" + item + "'</span> <br/>";
                            }
                        }
                        if (wrongNameUploadFileList.Count > 0)
                        {
                            this.blockError.Visible = true;
                            this.lblError.Text += "Wrong file name: <br/>";
                            foreach (var item in wrongNameUploadFileList)
                            {
                                this.lblError.Text += "<span style='color: blue; font-weight: bold'>'" + item + "'</span> <br/>";
                            }
                        }
                        if (uncompleteUploadFileList.Count == 0 && listExistFile.Count == 0 && wrongNameUploadFileList.Count == 0)
                        {
                            this.blockError.Visible = true;
                            this.lblError.Text = "All document files upload complete.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error when upload document file: <br/>'" + ex.Message + "'";
            }
        }
        #region UpdateData
        private void UpdateWeightDocument(ScopeProject projectObj, ref List<DocumentPackage> docList)
        {
            try
            {
                var totalPlanManhour = 0.0;
                totalPlanManhour = docList.Aggregate(totalPlanManhour, (current, t) => current + t.ManHourPlan.GetValueOrDefault());

                foreach (var doc in docList)
                {
                    doc.Weight = (doc.ManHourPlan.GetValueOrDefault() / totalPlanManhour) * 100;
                    this.documentPackageService.Update(doc);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkpackage(ScopeProject projectObj, List<DocumentPackage> docList, ref WorkGroup wpObj, ref List<WorkGroup> wpList, ref double complete)
        {
            try
            {
                if (docList.Count > 0)
                {
                    //update complete workpackage
                    complete = 0.0;
                    complete = docList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                    complete = Math.Round(complete, 5);
                    if (complete == 100 || !docList.Where(t => t.Complete < 100).Any())
                    {
                        wpObj.EndDate = DateTime.Now;
                        wpObj.Complete = 100;
                    }
                    else
                    {
                        wpObj.Complete = complete;
                    }
                }

                // Update total document weight for workpackage
                double totalWeight = 0.0;
                totalWeight = docList.Sum(t => t.Weight.GetValueOrDefault());
                totalWeight = Math.Round(totalWeight, 2);
                totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                wpObj.TotalDocWeight = totalWeight;

                // Update total man hours plan & actual for workpackage
                double totalManhourPlan = 0.0;
                totalManhourPlan = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                wpObj.TotalManHours = totalManhourPlan;

                double totalManHourActual = 0.0;
                totalManHourActual = docList.Sum(t => t.ManHours.GetValueOrDefault());
                wpObj.UsedManHours = totalManHourActual;

                // Update can delete for workpackage
                wpObj.CanDelete = !docList.Any();
                this.workGroupService.Update(wpObj);

                //update weight all WP of PJ
                if (projectObj.AutoCalculateWeightWorkGroup == true)
                {
                    var sumTotalManHours = 0.0;
                    sumTotalManHours = wpList.Sum(t => t.TotalManHours.GetValueOrDefault());
                    foreach (var items in wpList)
                    {
                        var calWeight = ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) > 0 ? ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) : 0;
                        items.Weight = calWeight >= 100 ? 100 : calWeight;
                        this.workGroupService.Update(items);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateMilestonActual(WorkGroup wpObj, User userObj, Milestone milestoneObj)
        {
            try
            {
                var milestoneList = this.milestoneService.GetAllByWorkpackage(wpObj.ID).OrderByDescending(t => t.MilestoneDate).ToList();
                if (milestoneList.Count > 0)
                {
                    var presentMilestoneObj = milestoneList.FirstOrDefault(t => t.MilestoneDate != null
                                                                         && t.MilestoneDate.Value.Month == DateTime.Now.Month
                                                                         && t.MilestoneDate.Value.Year == DateTime.Now.Year);
                    if (presentMilestoneObj != null)
                    {
                        milestoneObj = presentMilestoneObj;
                        var prevIndex = milestoneList.IndexOf(milestoneObj) + 1;
                        var prevRealTotal = 0.0;
                        var prevPlanTotal = 0.0;
                        if (prevIndex < (milestoneList.Count - 1))
                        {
                            var prevMilestoneObj = milestoneList[prevIndex];
                            if (prevMilestoneObj != null)
                            {
                                prevRealTotal = prevMilestoneObj.RealTotal.GetValueOrDefault();
                                prevPlanTotal = prevMilestoneObj.PlanTotal.GetValueOrDefault();
                            }
                            else
                            {
                                prevRealTotal = 0;
                                prevPlanTotal = 0;
                            }
                        }
                        else
                        {
                            prevRealTotal = 0;
                            prevPlanTotal = 0;
                        }

                        // calculator real
                        double real = Math.Round(wpObj.Complete.GetValueOrDefault() - prevRealTotal, 2);
                        milestoneObj.Real = real;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PlanTotal = (prevPlanTotal + milestoneObj.PlanPercent) > 100 ? 100 : (prevPlanTotal + milestoneObj.PlanPercent);
                        milestoneObj.UpdatedBy = userObj.Id;
                        milestoneObj.UpdatedDate = DateTime.Now;
                        this.milestoneService.Update(milestoneObj);
                    }
                    else
                    {
                        var lastedMilestoneObj = milestoneList[0];
                        milestoneObj = new Milestone();
                        milestoneObj.MilestoneDate = DateTime.Now;
                        milestoneObj.PlanPercent = 0;
                        milestoneObj.PlanTotal = lastedMilestoneObj.PlanTotal;
                        double preReal = Math.Round(wpObj.Complete.GetValueOrDefault() - lastedMilestoneObj.RealTotal.GetValueOrDefault(), 2);
                        milestoneObj.Real = preReal;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PerformingUser = lastedMilestoneObj.PerformingUser;
                        milestoneObj.Note = lastedMilestoneObj.Note;
                        milestoneObj.WorkpackageId = wpObj.ID;
                        milestoneObj.WorkpackageName = wpObj.Name;
                        if (DateTime.Now.Year > lastedMilestoneObj.MilestoneDate.Value.Year && (DateTime.Now.Year - lastedMilestoneObj.MilestoneDate.Value.Year) > 0)
                        {
                            milestoneObj.PlanOfYear = 0;
                        }
                        else
                        {
                            milestoneObj.PlanOfYear = lastedMilestoneObj.PlanOfYear;
                        }
                        milestoneObj.CreatedBy = userObj.Id;
                        milestoneObj.CreatedDate = DateTime.Now;
                        this.milestoneService.Insert(milestoneObj);
                    }
                }
                else
                {
                    //milestoneObj = new Milestone();
                    milestoneObj.MilestoneDate = DateTime.Now;
                    milestoneObj.PlanPercent = 0;
                    milestoneObj.PlanTotal = 0;
                    milestoneObj.RealTotal = wpObj.Complete;
                    milestoneObj.Real = wpObj.Complete;
                    milestoneObj.PerformingUser = string.Empty;
                    milestoneObj.Note = string.Empty;
                    milestoneObj.WorkpackageId = wpObj.ID;
                    milestoneObj.WorkpackageName = wpObj.Name;
                    milestoneObj.PlanOfYear = 0;
                    milestoneObj.CreatedBy = userObj.Id;
                    milestoneObj.CreatedDate = DateTime.Now;
                    this.milestoneService.Insert(milestoneObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateProgressActual(WorkGroup wpObj, ProcessActual processActualObj)
        {
            try
            {
                var projectId = wpObj.ProjectId.GetValueOrDefault();
                var projectObj = this.scopeProjectService.GetById(projectId);
                var tempProcessActualObj = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, wpObj.ID);
                if (tempProcessActualObj != null)
                {
                    processActualObj = tempProcessActualObj;
                    if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null)
                    {
                        if (string.IsNullOrEmpty(processActualObj.Actual))
                        {
                            processActualObj.Actual = "0";
                        }
                        if (string.IsNullOrEmpty(processActualObj.ActualMonth))
                        {
                            processActualObj.ActualMonth = "0";
                        }
                        var progressActualWeekList = processActualObj.Actual.Split('$').ToList();
                        var progressActualMonthList = processActualObj.ActualMonth.Split('$').ToList();
                        var countWeek = 0;
                        var countMonth = 0;
                        for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualWeekList.Count() > countWeek)
                            {
                                if (DateTime.Now > j.AddDays(7))
                                {
                                    countWeek += 1;
                                }
                            }
                        }
                        var currentMonth = 0;
                        for (var j = projectObj.StartDate.GetValueOrDefault();
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualMonthList.Count() > countMonth)
                            {
                                if (DateTime.Now.Month > j.AddDays(7).Month && DateTime.Now > j.AddDays(7) && currentMonth != j.AddDays(7).Month)
                                {
                                    currentMonth = j.Month;
                                    countMonth++;
                                }
                            }
                        }
                        if (progressActualWeekList.Count() >= countWeek && progressActualWeekList.Count() > 0 && countWeek > 0)
                        {
                            progressActualWeekList = progressActualWeekList.Take(countWeek).ToList();
                            progressActualWeekList[countWeek - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.Actual = string.Join("$", progressActualWeekList);
                            this.processActualService.Update(processActualObj);
                        }
                        if (progressActualMonthList.Count() >= countMonth && progressActualMonthList.Count() > 0 && countMonth > 0)
                        {
                            progressActualMonthList = progressActualMonthList.Take(countMonth).ToList();
                            progressActualMonthList[countMonth - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.ActualMonth = string.Join("$", progressActualMonthList);
                            this.processActualService.Update(processActualObj);
                        }
                    }
                }
                else
                {
                    processActualObj = new ProcessActual();
                    processActualObj.ProjectId = wpObj.ProjectId;
                    processActualObj.WorkgroupId = wpObj.ID;
                    processActualObj.Actual = wpObj.Complete.ToString();
                    processActualObj.ActualMonth = wpObj.Complete.ToString();
                    this.processActualService.Insert(processActualObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private DateTime GetMondayOfWeek(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        private DateTime GetFridayOfWeek(DateTime date)
        {
            return GetMondayOfWeek(date).AddDays(4);
        }

        private void UpdateDataProject(List<WorkGroup> wpList, ScopeProject projectObj)
        {
            try
            {
                if (projectObj != null)
                {
                    projectObj.CanDelete = false;
                    if (projectObj.IsAutoCalculate.GetValueOrDefault())
                    {
                        double complete = 0.0;
                        complete = wpList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                        projectObj.Complete = Math.Round(complete, 5);
                        double totalWeight = 0.0;
                        totalWeight = wpList.Sum(t => t.Weight.GetValueOrDefault());
                        totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                        projectObj.TotalWorkpackageWeight = Math.Round(totalWeight, 2);
                    }
                    this.scopeProjectService.Update(projectObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateWeightWorkgroupManhourMonth(WorkGroup wpObj, List<WorkgroupManhourMonth> wmmList)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                wmmList = this.workgroupManhourMonthService.GetByPJ(wpObj.ProjectId.GetValueOrDefault(), month, year);
                foreach (var wmmItem in wmmList)
                {
                    var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                    if (wpItem != null)
                    {
                        wmmItem.WorkgroupName = wpItem.Name;
                        wmmItem.WorkgroupWeight = wpItem.Weight;
                        this.workgroupManhourMonthService.Update(wmmItem);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkgroupManhourMonth(WorkGroup wpObj, User userObj, List<WorkGroup> wpList, WorkgroupManhourMonth wmmObj)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                var docList = this.documentPackageService.GetAllByWorkgroup(wpObj.ID);
                var manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                var presentWMMObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), month, year);
                var prevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpObj.ID, month, year);
                if (presentWMMObj != null)
                {
                    wmmObj = presentWMMObj;
                    wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                    wmmObj.ManhourPlanTotal = manhourPlanTotal;
                    wmmObj.ManhourPlanMonth = (wmmObj.ManhourPlanTotal.GetValueOrDefault() * wmmObj.CompletePlanMonth.GetValueOrDefault()) / 100;
                    wmmObj.CompleteActualTotal = wpObj.Complete.GetValueOrDefault();
                    wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                    //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                    if (prevWMMObj != null)
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                    }
                    else
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                    }

                    wmmObj.UpdateBy = userObj.Id;
                    wmmObj.UpdateDate = DateTime.Now;
                    this.workgroupManhourMonthService.Update(wmmObj);
                }
                else
                {
                    foreach (var wpItem in wpList)
                    {
                        docList = this.documentPackageService.GetAllByWorkgroup(wpItem.ID);
                        manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                        var tempObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpItem.ID, wpItem.ProjectId.GetValueOrDefault(), month, year);
                        var tempPrevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpItem.ID, month, year);
                        if (tempObj == null)
                        {
                            wmmObj = new WorkgroupManhourMonth();
                            wmmObj.Month = month;
                            wmmObj.Year = year;
                            wmmObj.WorkgroupID = wpItem.ID;
                            wmmObj.WorkgroupName = wpItem.Name;
                            wmmObj.DeparmentID = wpItem.DepartmentId;
                            wmmObj.ProjectID = wpItem.ProjectId.GetValueOrDefault();
                            wmmObj.WorkgroupWeight = wpItem.Weight.GetValueOrDefault();
                            wmmObj.ManhourPlanTotal = manhourPlanTotal;
                            wmmObj.CompleteActualTotal = wpItem.Complete.GetValueOrDefault();
                            wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                            //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                            if (tempPrevWMMObj != null)
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - tempPrevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - tempPrevWMMObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            else
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            wmmObj.CreateBy = userObj.Id;
                            wmmObj.CreateDate = DateTime.Now;
                            this.workgroupManhourMonthService.Insert(wmmObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourWeek(DocumentPackage documentObj, User userObj, UserManhour userManhourObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;

                CultureInfo myCI = CultureInfo.CurrentCulture;
                System.Globalization.Calendar myCal = myCI.Calendar;
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var listUserManhour = this.userManhourService.GetByDocIDAndUserID(documentObj.ID, user.Id);
                            listUserManhour = listUserManhour.OrderByDescending(t => t.ID).ToList();

                            var currentUserManhourWeek = listUserManhour.FirstOrDefault(t => myCal.GetWeekOfYear(DateTime.Now.Date, myCWR, myFirstDOW) == myCal.GetWeekOfYear(t.Date.GetValueOrDefault(), myCWR, myFirstDOW));
                            int index = listUserManhour.IndexOf(currentUserManhourWeek);
                            var prevUserManhourWeek = new UserManhour();
                            if (index != -1 && (index + 1) < listUserManhour.Count)
                            {
                                prevUserManhourWeek = listUserManhour[(index + 1)];
                            }
                            if (currentUserManhourWeek != null)
                            {
                                userManhourObj = currentUserManhourWeek;
                                if (prevUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - prevUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UpdateBy = userObj.Id;
                                userManhourObj.UpdateDate = DateTime.Now;
                                this.userManhourService.Update(userManhourObj);
                            }
                            else
                            {
                                var lastedUserManhourWeek = new UserManhour();
                                if (listUserManhour.Count > 0)
                                {
                                    lastedUserManhourWeek = listUserManhour[0];
                                }
                                userManhourObj = new UserManhour();
                                userManhourObj.DocumentID = documentObj.ID;
                                userManhourObj.Date = DateTime.Now.Date;
                                userManhourObj.DeparmentID = user.RoleId;
                                userManhourObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourObj.ProjectID = documentObj.ProjectId;
                                if (lastedUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - lastedUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - lastedUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UserID = user.Id;
                                userManhourObj.UserName = user.FullName;
                                userManhourObj.CreateBy = userObj.Id;
                                userManhourObj.CreateDate = DateTime.Now;
                                this.userManhourService.Insert(userManhourObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourMonth(DocumentPackage documentObj, User userObj, UserManhourMonth userManhourMonthObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;
                var date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var currentUserManhourMonth = this.userManhourMonthService.GetByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            var prevUserManhourMonth = this.userManhourMonthService.GetLastedByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            if (currentUserManhourMonth != null)
                            {
                                userManhourMonthObj = currentUserManhourMonth;
                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UpdateBy = userObj.Id;
                                userManhourMonthObj.UpdateDate = DateTime.Now;
                                this.userManhourMonthService.Update(userManhourMonthObj);
                            }
                            else
                            {
                                userManhourMonthObj = new UserManhourMonth();
                                userManhourMonthObj.DocumentID = documentObj.ID;
                                userManhourMonthObj.Month = DateTime.Now.Month;
                                userManhourMonthObj.Year = DateTime.Now.Year;
                                userManhourMonthObj.DeparmentID = user.RoleId;
                                userManhourMonthObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourMonthObj.ProjectID = documentObj.ProjectId;

                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UserID = user.Id;
                                userManhourMonthObj.UserName = user.FullName;
                                userManhourMonthObj.CreateBy = userObj.Id;
                                userManhourMonthObj.CreateDate = DateTime.Now;
                                this.userManhourMonthService.Insert(userManhourMonthObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}
﻿
/////


//////
namespace EDMs.Business.Services.Document
{

    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;
    using System.Globalization;


    public class UserManhourService
    {
        private readonly UserManhourDAO repo;

        public UserManhourService()
        {
            this.repo = new UserManhourDAO();
        }

        public List<UserManhour> GetAll()
        {
            return this.repo.GetAll().ToList();

        }

        public List<UserManhour> GetByDocIDAndUserID(int documentID, int userID)
        {
            return this.repo.GetByDocIDAndUserID(documentID,userID);

        }

        public UserManhour GetById(int id)
        {
            return this.repo.GetById(id);
        }

        public int? Insert(UserManhour bo)
        {
            return this.repo.Insert(bo);
        }


        public bool Update(UserManhour bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(UserManhour bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

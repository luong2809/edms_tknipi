﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GeneralReport.aspx.cs" Inherits="EDMs.Web.GeneralReport" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--[if gte IE 8]>
        <style type="text/css">
            #ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important;}
            #ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important;}
        </style>
    <![endif]-->

    <style type="text/css">
        /*Custom CSS of Grid documents for FF browser*/
        #ContentPlaceHolder2_grdDocument_ctl00_Header {
            table-layout: auto !important;
        }

        #ContentPlaceHolder2_grdDocument_ctl00 {
            table-layout: auto !important;
        }
        /*End*/
        @-moz-document url-prefix() {
            #ContentPlaceHolder2_grdDocument_ctl00_Header {
                table-layout: auto !important;
            }

            #ContentPlaceHolder2_grdDocument_ctl00 {
                table-layout: auto !important;
            }
        }

        #RAD_SPLITTER_PANE_CONTENT_ctl00_rightPane {
            overflow: hidden !important;
        }

        .rgExpandCol {
            width: 1% !important;
        }

        .rgGroupCol {
            width: 1% !important;
        }

        #ContentPlaceHolder2_grdDocument_ctl00_ctl02_ctl03_txtDate_popupButton {
            display: none;
        }

        .rgExpandCol {
            width: 1% !important;
        }

        .rgGroupCol {
            width: 1% !important;
        }

        .RadGrid .rgSelectedRow {
            background-image: none !important;
            background-color: coral !important;
        }

        .rgMasterTable .rgClipCells .rgClipCells {
            table-layout: auto !important;
        }

        .rgGroupCol {
            padding-left: 0 !important;
            padding-right: 0 !important;
            font-size: 1px !important;
        }

        div.RadGrid .rgPager .rgAdvPart {
            display: none;
        }
        /*#RAD_SPLITTER_PANE_CONTENT_ctl00_leftPane {
            width: 250px !important;
        }
        #RAD_SPLITTER_PANE_CONTENT_ctl00_topLeftPane {
            width: 250px !important;
        }*/

        .RadAjaxPanel {
            height: 100% !important;
        }

        .rpExpandHandle {
            display: none !important;
        }

        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 0px !important;
            padding-right: 0px !important;
        }

        /*Hide change page size control*/
        /*div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none;        sssssss
        }*/

        a.tooltip {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong {
                line-height: 30px;
            }

            a.tooltip:hover {
                text-decoration: none;
            }

            a.tooltip span {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }


        #ctl00_ContentPlaceHolder2_radTreeFolder {
            overflow: visible !important;
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5 {
            height: 100% !important;
        }

        #divContainerLeft {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
            margin: 0 0px;
        }

        /*.RadComboBox .rcbInputCell .rcbInput {
            /*border-left-color: #FF0000;*/
        /*border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 283px;
        }*/

        .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
            padding-left: 5px !important;
        }

        .TemplateMenu {
            z-index: 10;
        }

        .RadGrid_Windows7 .rgGroupHeader {
            font-size: 14px !important;
            line-height: 19px !important;
        }

        /*.RadComboBox {
               border-bottom: none !important;
           }*/

        .accordion dt a {
            color: #085B8F;
            border-bottom: 2px solid #46A3D3;
            font-size: 1.5em;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .accordion dt span {
            color: #085B8F;
            border-bottom: 2px solid #46A3D3;
            font-size: 1.5em;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
            padding-left: 5px;
        }

        .btnInline {
            display: inline!important;
        }
    </style>
    <telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="REPORT" runat="server" Expanded="True" Width="100%">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Milestones" ImageUrl="~/Images/milestone.png" NavigateUrl="MilestoneReport.aspx" />
                    <telerik:RadPanelItem runat="server" Text="Generals" ImageUrl="~/Images/report3.png" NavigateUrl="GeneralReport.aspx" Selected="True" />
                </Items>
            </telerik:RadPanelItem>

            <telerik:RadPanelItem Text="DOCUMENT MANAGEMENT" runat="server" Expanded="True" Width="100%">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Update EMDR" ImageUrl="~/Images/emdr.png" NavigateUrl="EMDR.aspx" />
                    <telerik:RadPanelItem runat="server" Text="Search Documents" ImageUrl="~/Images/search.png" NavigateUrl="SearchNew.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal">
        <telerik:RadPane ID="RadPane3" runat="server" Height="30px">

            <telerik:RadToolBar ID="CustomerMenu" runat="server" Width="100%">
                <Items>
                    <%-- <telerik:RadToolBarButton runat="server" IsSeparator="true"/>
                    <telerik:RadToolBarDropDown runat="server" Text="Action" ImageUrl="~/Images/action.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Month Report" Value="MonthReport" ImageUrl="~/Images/report3.png"/>
                                <telerik:RadToolBarButton runat="server" Text="Next Month Plan Report" Value="NextMonthReport" ImageUrl="~/Images/report3.png"/>
                        </Buttons>
                    </telerik:RadToolBarDropDown>--%>
                </Items>
            </telerik:RadToolBar>

        </telerik:RadPane>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter3" runat="server" Orientation="Horizontal">
                <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None">
                    <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical">
                        <telerik:RadPane ID="Radpane6" runat="server">
                            <div id="GIPReport" runat="server">
                                <dl class="accordion">
                                    <dt style="width: 100%;">
                                        <span>GIP Report</span>
                                    </dt>
                                </dl>
                                <table>
                                    <tr>
                                        <td>From Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtGIPFromDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>To Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtGIPToDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export GIP Report" ID="btnExportGipReport" OnClick="btnExportGipReport_OnClick">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            <label style="float: left; padding-top: 5px; padding-right: 5px; text-align: left; width: 55px">
                                                <span style="text-align: right;">Project</span>
                                            </label>
                                            <div style="width: 365px; padding-right: 5px; float: left;">
                                                <telerik:RadComboBox ID="ddlProject" runat="server" Width="100%" Skin="Windows7" Filter="Contains" OnItemDataBound="ddlProject_ItemDataBound" />
                                            </div>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export Manhours Report" ID="btnExportManhoursGip" OnClick="btnExportManhoursGip_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table>
                                    <tr>
                                        <td>From Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtmhgipfromdate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>To Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtmhgipTodate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export ManHour Report" ID="btnexportmhgip" OnClick="btnexportmhgip_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div id="PMReport" runat="server">
                                <dl class="accordion">
                                    <dt style="width: 100%;">
                                        <span>PM Report</span>
                                    </dt>
                                </dl>
                                <table>
                                    <tr>
                                        <td>
                                            <label style="float: left; padding-top: 5px; padding-right: 5px; text-align: left; width: 55px">
                                                <span style="text-align: right;">Project</span>
                                            </label>
                                            <div style="width: 365px; padding-right: 5px; float: left;">
                                                <telerik:RadComboBox ID="ddlPMProject" runat="server" Width="100%" Skin="Windows7" Filter="Contains" OnItemDataBound="ddlProject_ItemDataBound" />
                                            </div>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export Manhour of Project" ID="btnExportManhourProject" OnClick="btnExportManhourProject_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table>
                                    <tr>
                                        <td>From Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtPMFromDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>To Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtPMToDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export PM Report" ID="btnExportPMReport" OnClick="btnExportPMReport_OnClick">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>

                                    </tr>
                                </table>
                                <br />
                                <table>
                                    <tr>
                                        <td>From Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtFromDateProject" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>To Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtToDateProject" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export Project Null EMDR" ID="rdbExportProjectNull" OnClick="rdbExportProjectNull_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table>
                                    <tr>
                                        <td>From Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtUnplannedProjectFromDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>To Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtUnplannedProjectToDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export Unplanned Project Report" ID="btnExportUnplannedProject" OnClick="btnExportUnplannedProject_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            <label style="float: left; padding-top: 5px; padding-right: 5px; text-align: left; width: 90px">
                                                <span style="text-align: right;">Year</span>
                                            </label>
                                            <div style="width: 470px; padding-right: 5px; float: left;">
                                                <asp:DropDownList ID="ddlYearProgressReport" runat="server" CssClass="min25Percent" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlYearProgressReport_SelectedIndexChanged" />
                                                <asp:DropDownList ID="ddlWorkList" runat="server" CssClass="min25Percent" Width="365px"></asp:DropDownList>
                                            </div>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Progress Report" ID="btnProgressReport" OnClick="btnProgressReport_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td>
                                            <label style="float: left; padding-top: 5px; padding-right: 5px; text-align: left; width: 90px">
                                                <span style="text-align: right;">Choose Month</span>
                                            </label>
                                            <div style="width: 470px; padding-right: 5px; float: left;">
                                                <asp:DropDownList ID="ddlMonthManhourReport" runat="server" CssClass="min25Percent" Width="232px">
                                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                    <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                    <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                    <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddlYearManhourReport" runat="server" CssClass="min25Percent" Width="232px"/>
                                                
                                            </div>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Monthly Manhour Report" ID="btnMonthlyManhourReport" OnClick="btnMonthlyManhourReport_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            <telerik:RadButton runat="server" Text="Chart for Leader Vien" ID="btnChartLeaderVien" OnClick="btnChartLeaderVien_Click" Visible="false">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            
                                            <telerik:RadButton runat="server" Text="Chart Department" ID="btnChartDepartment" OnClick="btnChartDepartment_Click" Visible="false">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label style="float: left; padding-top: 5px; padding-right: 5px; text-align: left; width: 90px">
                                                <span style="text-align: right;">Dep.</span>
                                            </label>
                                            <asp:DropDownList ID="ddlDepartmentReport" Width="468px" runat="server"></asp:DropDownList>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Department Manhour Report" ID="btnDepartmentManhourReport" OnClick="btnDepartmentManhourReport_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div id="MasterReport" runat="server">
                                <dl class="accordion">
                                    <dt style="width: 100%;">
                                        <span>Master Report</span>
                                    </dt>
                                </dl>
                                <table>
                                    <tr>
                                        <td>From Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtMasterFromDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>To Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtMasterToDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export Master Report" ID="btnExportMasterReport" OnClick="btnExportMasterReport_OnClick">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table>
                                    <tr>
                                        <td>From Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtdatefrom" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>To Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtdateto" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export All Document Incomplete" ID="btnincompletedocument" OnClick="btnincompletedocument_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="ChiefReport" runat="server">
                                <dl class="accordion">
                                    <dt style="width: 100%;">
                                        <span>Chief Report</span>
                                    </dt>
                                </dl>
                                <table>
                                    <tr>
                                        <td>Month</td>
                                        <%--<td>
                                            <telerik:RadDatePicker ID="txtChiefFromDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>To Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtChiefToDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>--%>
                                        <td>
                                            <asp:DropDownList ID="ddlMonthManhourChiefReport" runat="server" CssClass="min25Percent" Width="100px">
                                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                    <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                    <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                    <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddlYearManhourChiefReport" runat="server" CssClass="min25Percent" Width="100px"/>
                                            <asp:DropDownList ID="ddlDepartmentManhourChiefReport" runat="server" Visible="false"></asp:DropDownList>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export Chief Report" ID="btnExportChiefReport" OnClick="btnExportChiefReport_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <%--<table>
                                    <tr>
                                        <td>From Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtManhourFromDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>To Date</td>
                                        <td>
                                            <telerik:RadDatePicker ID="txtManhourToDate" runat="server" CssClass="qlcbFormNonRequired">
                                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>
                                            <telerik:RadButton runat="server" Text="Export Manhours Report" ID="btnReportManhourForCheif" OnClick="btnReportManhourForCheif_Click">
                                                <Icon PrimaryIconUrl="Images/report1.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>--%>
                            </div>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>

    <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="rtvWorkgroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="CustomerMenu" />
                        <telerik:AjaxUpdatedControl ControlID="IsFullPermission" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="radMenu">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="btnMonthlyReport">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="docuploader" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContainer" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="rtvWorkgroup" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="CustomerMenu" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="ddlDepartment">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rtvWorkgroup" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="txtMonthYear">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rtvWorkgroup" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlYear">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ddlWorkList" UpdatePanelCssClass="btnInline" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Milestone Information"
                VisibleStatusbar="false" Height="600" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="UploadMulti" runat="server" Title="Create multiple documents"
                VisibleStatusbar="false" Height="520" MinHeight="520" MaxHeight="520" Width="640" MinWidth="640" MaxWidth="640"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="RevisionDialog" runat="server" Title="Revision History"
                VisibleStatusbar="false" Height="400" Width="1250" MinHeight="400" MinWidth="1250" MaxHeight="400" MaxWidth="1250"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="TransmittalList" runat="server" Title="Transmittal List"
                VisibleStatusbar="false" Height="400" Width="1250" MinHeight="400" MinWidth="1250" MaxHeight="400" MaxWidth="1250"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="SendMail" runat="server" Title="Send mail"
                VisibleStatusbar="false" Height="560" Width="992" MinHeight="560" MinWidth="992" MaxHeight="560" MaxWidth="992"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="AttachDoc" runat="server" Title="Attach document files"
                VisibleStatusbar="false" Height="500" Width="700" MinHeight="500" MinWidth="700" MaxHeight="500" MaxWidth="700"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="AttachMulti" runat="server" Title="Attach multi document files"
                VisibleStatusbar="false" Height="500" Width="700" MinHeight="500" MinWidth="700" MaxHeight="500" MaxWidth="700"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="AttachComment" runat="server" Title="Attach Comment/Response"
                VisibleStatusbar="false" Height="500" Width="900" MinHeight="500" MinWidth="900" MaxHeight="500" MaxWidth="900"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="ImportData" runat="server" Title="Import master list"
                VisibleStatusbar="false" Height="400" Width="520"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="ImportEMDRReport" runat="server" Title="Import EMDR Report"
                VisibleStatusbar="false" Height="400" Width="520"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="ShareDocument" runat="server" Title="Share documents"
                VisibleStatusbar="false" Height="600" Width="520"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="ViewerFile" runat="server" Title="Viewer File"
                VisibleStatusbar="false" Height="600" Width="1250" MinHeight="600" MinWidth="1250" MaxHeight="600" MaxWidth="1250"
                Left="50px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <asp:HiddenField runat="server" ID="FolderContextMenuAction" />
    <asp:HiddenField runat="server" ID="lblWorkgroupId" />
    <asp:HiddenField runat="server" ID="lblFolderId" />
    <asp:HiddenField runat="server" ID="lblDocId" />
    <asp:HiddenField runat="server" ID="lblCategoryId" />
    <asp:HiddenField runat="server" ID="IsFullPermission" />

    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">

            function OnClientSelectedIndexChanging(sender, eventArgs) {
                var input = sender.get_inputDomElement();
                input.style.background = "url(" + eventArgs.get_item().get_imageUrl() + ") no-repeat";
            }
            function OnClientLoad(sender) {
                var input = sender.get_inputDomElement();
                var selectedItem = sender.get_selectedItem();
                input.style.background = "url(" + selectedItem.get_imageUrl() + ") no-repeat";
            }

            var radDocuments;


            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

            function onRequestStart(sender, args) {
                ////alert(args.get_eventTarget());
                if (args.get_eventTarget().indexOf("ExportTo") >= 0 || args.get_eventTarget().indexOf("btnDownloadPackage") >= 0 ||
                    args.get_eventTarget().indexOf("ajaxCustomer") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function onColumnHidden(sender) {

                var masterTableView = sender.get_masterTableView().get_element();
                masterTableView.style.tableLayout = "auto";
                //window.setTimeout(function () { masterTableView.style.tableLayout = "auto"; }, 0);
            }

            // Undocked and Docked event slide bar Tree folder
            function OnClientUndocked(sender, args) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;

                radDocuments.get_masterTableView().showColumn(8);
                radDocuments.get_masterTableView().showColumn(9);
                radDocuments.get_masterTableView().showColumn(10);
                radDocuments.get_masterTableView().showColumn(11);

                radDocuments.get_masterTableView().showColumn(12);
                radDocuments.get_masterTableView().showColumn(13);
                radDocuments.get_masterTableView().showColumn(14);
                radDocuments.get_masterTableView().showColumn(15);

                if (selectedFolder != "") {
                    ajaxManager.ajaxRequest("ListAllDocuments");
                }
            }

            function OnClientDocked(sender, args) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;

                radDocuments.get_masterTableView().hideColumn(8);
                radDocuments.get_masterTableView().hideColumn(9);
                radDocuments.get_masterTableView().hideColumn(10);
                radDocuments.get_masterTableView().hideColumn(11);

                radDocuments.get_masterTableView().hideColumn(12);
                radDocuments.get_masterTableView().hideColumn(13);
                radDocuments.get_masterTableView().hideColumn(14);
                radDocuments.get_masterTableView().hideColumn(15);

                if (selectedFolder != "") {
                    ajaxManager.ajaxRequest("TreeView");
                }
            }

            function RowClick(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;
            }

            function gridMenuClicking(sender, args) {
                var itemValue = args.get_item().get_value();
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                var isFullPermission = document.getElementById("<%= IsFullPermission.ClientID %>").value;

                var packageId = document.getElementById("<%= lblWorkgroupId.ClientID %>").value;

                switch (itemValue) {
                    case "RevisionHistory":
                        var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                        var owd = $find("<%=RevisionDialog.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/DocumentPackageRevisionHistory.aspx?docId=" + docId + "&categoryId=" + categoryId, "RevisionDialog");
                        break;
                    case "AttachDocument":
                        var owd = $find("<%=AttachDoc.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/AttachDocument.aspx?docId=" + docId + "&isFullPermission=" + isFullPermission, "AttachDoc");
                        break;
                    case "AttachComment":
                        var owd = $find("<%=AttachComment.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/AttachComment.aspx?docId=" + docId + "&isFullPermission=" + isFullPermission, "AttachComment");
                        break;
                    case "EditProperties":
                        var owd = $find("<%=CustomerDialog.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Scope/WorkpackageMilestoneEditForm.aspx?milestoneId=" + docId + "&wgId=" + packageId, "CustomerDialog");
                        break;
                    case "Transmittals":
                        var owd = $find("<%=TransmittalList.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/TransmittalListByDoc.aspx?docId=" + docId, "TransmittalList");
                        break;
                }
            }

            function onClientContextMenuItemClicking(sender, args) {
                var menuItem = args.get_menuItem();
                var treeNode = args.get_node();
                menuItem.get_menu().hide();

                switch (menuItem.get_value()) {
                    case "Rename":
                        treeNode.startEdit();
                        break;
                    case "Delete":
                        var result = confirm("Are you sure you want to delete the folder: " + treeNode.get_text());
                        args.set_cancel(!result);
                        break;
                }
            }

            function rtvExplore_OnNodeExpandedCollapsed(sender, eventArgs) {
                var allNodes = eventArgs._node.get_treeView().get_allNodes();

                var i;
                var selectedNodes = "";

                for (i = 0; i < allNodes.length; i++) {
                    if (allNodes[i].get_expanded())
                        selectedNodes += allNodes[i].get_value() + "*";
                }

                Set_Cookie("expandedNodesObjTree", selectedNodes, 30);
            }

            function Set_Cookie(name, value, expires, path, domain, secure) {
                // set time, it's in milliseconds
                var today = new Date();
                today.setTime(today.getTime());

                /*
                if the expires variable is set, make the correct 
                expires time, the current script below will set 
                it for x number of days, to make it for hours, 
                delete * 24, for minutes, delete * 60 * 24
                */
                if (expires) {
                    expires = expires * 1000 * 60 * 60 * 24;
                }
                var expires_date = new Date(today.getTime() + (expires));

                document.cookie = name + "=" + escape(value) +
                    ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
                    ((path) ? ";path=" + path : "") +
                    ((domain) ? ";domain=" + domain : "") +
                    ((secure) ? ";secure" : "");
            }

            function gridContextMenuShowing(menu, args) {
                <%--var isfullpermission = document.getElementById("<%= IsFullPermission.ClientID %>").value;
                if (isfullpermission == "true") {
                    menu.get_allItems()[0].enable();
                    menu.get_allItems()[1].enable();
                    menu.get_allItems()[3].enable();
                    menu.get_allItems()[4].enable();
                    menu.get_allItems()[6].enable();
                }
                else {
                    menu.get_allItems()[0].disable();
                    menu.get_allItems()[1].disable();
                    menu.get_allItems()[3].enable();
                    menu.get_allItems()[4].enable();
                    menu.get_allItems()[6].enable();
                }--%>
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents());
                });

                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function ShowEditForm(id) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Scope/WorkpackageMilestoneEditForm.aspx?milestoneId=" + id, "CustomerDialog");

                // window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + id, "CustomerDialog");
                //  return false;
            }

            function ShowUploadForm(id) {
                var owd = $find("<%=AttachDoc.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/UploadDragDrop.aspx?docId=" + id, "AttachDoc");
            }

            function ShowRevisionForm(id) {
                var owd = $find("<%=RevisionDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/RevisionHistory.aspx?docId=" + id, "RevisionDialog");
            }


            function refreshTab(arg) {
                $('.EDMsRadPageView' + arg + ' iframe').attr('src', $('.EDMsRadPageView' + arg + ' iframe').attr('src'));
            }

            function RowDblClick(sender, eventArgs) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/ViewCustomerDetails.aspx?docId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
                // window.radopen("Controls/Customers/ViewCustomerDetails.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
            }

            function rtvWorkgroup_ClientNodeClicking(sender, args) {
                var workgroupValue = args.get_node().get_value();
                document.getElementById("<%= lblWorkgroupId.ClientID %>").value = workgroupValue;
            }

            function radPbCategories_OnClientItemClicking(sender, args) {
                var item = args.get_item();
                var categoryId = item.get_value();
                document.getElementById("<%= lblCategoryId.ClientID %>").value = categoryId;

            }

            function ShowViewerFile(Id) {

                // if (path.toLowerCase().search(".pdf")) {
                var owd = $find("<%= ViewerFile.ClientID %>");
                owd.Show();
                owd.moveTo(120, 20);
                owd.setSize(1200, window.availHeight);
                owd.setUrl("../ViewerFilePDF.aspx?DocId=" + Id + "&File=3", "ViewerFile");
                //}

            }

            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("images/clear.gif");
                    searchButton.set_value("clear");
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value());
            }
            function onTabSelecting(sender, args) {
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }
            }

            /* ]]> */
        </script>
    </telerik:RadCodeBlock>
</asp:Content>

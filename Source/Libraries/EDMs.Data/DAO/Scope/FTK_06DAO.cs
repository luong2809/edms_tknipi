﻿

namespace EDMs.Data.DAO.Scope
{
    using System.Collections.Generic;
    using System.Linq;
    using EDMs.Data.Entities;
   public  class FTK_06DAO : BaseDAO
    {
        public FTK_06DAO() : base(){}
        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<FTK_06> GetIQueryable()
        {
            return this.EDMsDataContext.FTK_06;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<FTK_06> GetAll()
        {
            return this.EDMsDataContext.FTK_06.OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public FTK_06 GetById(int id)
        {
            return this.EDMsDataContext.FTK_06.FirstOrDefault(ob => ob.ID == id);
        }

        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(FTK_06 ob)
        {
            try
            {
                this.EDMsDataContext.AddToFTK_06(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(FTK_06 src)
        {
            try
            {
                FTK_06 des = (from rs in this.EDMsDataContext.FTK_06
                                 where rs.ID == src.ID
                                 select rs).First();

                des.Date = src.Date;
                des.Deadline = src.Deadline;
                des.ChangeCategories = src.ChangeCategories;
                des.Notation = src.Notation;
                des.AccompanyingMaterial = src.AccompanyingMaterial;
                des.ProposedUnit = src.ProposedUnit;
                des.Number = src.Number;
                des.ProjectName = src.ProjectName;
                des.DepartmentName = src.DepartmentName;
                des.Containt = src.Containt;
                des.WorkpackageId = src.WorkpackageId;
                des.WorkpackageName = src.WorkpackageName;
                des.UpdatedBy = src.UpdatedBy;
                des.UpdatedDate = src.UpdatedDate;
               
                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(FTK_06 src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportDocData : Page
    {
        private readonly DocumentPackageService documentPackageService;

        private readonly WorkGroupService workGroupService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly MilestoneService milestoneService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportDocData()
        {
            this.workGroupService = new WorkGroupService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.milestoneService = new MilestoneService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

           // this.btnSave.Text = "Please Wait..";
            foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
            {
                var extension = docFile.GetExtension();
                if (extension == ".xls")
                {
                    var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                     "_" + docFile.FileName;
                    docFile.SaveAs(importPath);

                    var workbook = new Workbook();
                    workbook.Open(importPath);
                    var strError = "Have error: </br>";
                    var currentWP = string.Empty;
                    var currentDocNo = string.Empty;

                    var docList = new List<DocumentPackage>();

                    foreach (Worksheet worksheet in workbook.Worksheets)
                    {
                        //var isEdited = worksheet.Cells["B4"].Value.ToString();
                        var wpID = worksheet.Cells["B5"].Value.ToString();
                        var wpObj = this.workGroupService.GetById(!string.IsNullOrEmpty(wpID)
                            ? Convert.ToInt32(wpID)
                            : 0);
                        if (wpObj != null )//&& isEdited == "1")
                        {
                            currentWP = wpObj.Name;

                            var dataTable = new DataTable();
                            dataTable = worksheet.Cells.ExportDataTable(5, 1, worksheet.Cells.MaxRow, 12);

                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                                {
                                    var docId = Convert.ToInt32(dataRow["Column1"].ToString());
                                    var docObj = this.documentPackageService.GetById(docId);
                                    if (docObj != null)
                                    {
                                        currentDocNo = dataRow["Column3"].ToString();

                                        docObj.HaveWpAutoCalculate = wpObj.IsAutoCalculate.GetValueOrDefault();
                                        docObj.WorkGroup = wpObj;

                                        try
                                        {
                                            docObj.ManHours = !string.IsNullOrEmpty(dataRow["Column6"].ToString())
                                                ? Convert.ToDouble(dataRow["Column6"].ToString())
                                                : (double?) null;
                                        }
                                        catch (Exception)
                                        {
                                            strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                                        currentDocNo + "</b> have invalid 'Man hours' data.";
                                        }

                                        docObj.MilestoneDate = DateTime.Now;

                                        var strstartDate = dataRow["Column7"].ToString();
                                        if (!string.IsNullOrEmpty(strstartDate))
                                        {
                                            var startDate = new DateTime();
                                            if (Utility.ConvertStringToDateTimeddMMyyyy(strstartDate, ref startDate))
                                            {
                                                docObj.StartDate = startDate;
                                            }
                                            else
                                            {
                                                strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                                            currentDocNo + "</b> have invalid 'Start date' data.";
                                            }
                                        }
                                        else
                                        {
                                            docObj.StartDate = null;
                                        }

                                        var strDeadline = dataRow["Column8"].ToString();
                                        if (!string.IsNullOrEmpty(strDeadline))
                                        {
                                            var deadline = new DateTime();
                                            if (Utility.ConvertStringToDateTimeddMMyyyy(strDeadline, ref deadline))
                                            {
                                                docObj.Deadline = deadline;
                                            }
                                            else
                                            {
                                                strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                                            currentDocNo + "</b> have invalid 'Deadline' data.";
                                            }
                                        }
                                        else
                                        {
                                            docObj.Deadline = null;
                                        }

                                        var strFinishDate = dataRow["Column9"].ToString();
                                        if (!string.IsNullOrEmpty(strFinishDate))
                                        {
                                            var finishDate = new DateTime();
                                            if (Utility.ConvertStringToDateTimeddMMyyyy(strFinishDate, ref finishDate))
                                            {
                                                docObj.EndDate = finishDate;
                                            }
                                            else
                                            {
                                                strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                                            currentDocNo + "</b> have invalid 'Finish date' data.";
                                            }
                                        }
                                        else
                                        {
                                            docObj.EndDate = null;
                                        }

                                        var strIsoDate = dataRow["Column10"].ToString();
                                        if (!string.IsNullOrEmpty(strIsoDate))
                                        {
                                            var isoDate = new DateTime();
                                            if (Utility.ConvertStringToDateTimeddMMyyyy(strIsoDate, ref isoDate))
                                            {
                                                docObj.IsoReviseDate = isoDate;
                                            }
                                            else
                                            {
                                                strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                                            currentDocNo + "</b> have invalid 'Iso Revise date' data.";
                                            }
                                        }
                                        else
                                        {
                                            docObj.IsoReviseDate = null;
                                        }

                                        try
                                        {
                                            docObj.Weight = !string.IsNullOrEmpty(dataRow["Column11"].ToString())
                                            ? Math.Round(Convert.ToDouble(dataRow["Column11"]) * 100, 2)
                                            : (double?) null;
                                        }
                                        catch (Exception)
                                        {
                                            strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                                        currentDocNo + "</b> have invalid 'Weight' data.";
                                        }

                                        try
                                        {
                                            docObj.Complete = !string.IsNullOrEmpty(dataRow["Column12"].ToString())
                                            ? Math.Round(Convert.ToDouble(dataRow["Column12"]) * 100, 2)
                                            : (double?) null;
                                        }
                                        catch (Exception)
                                        {
                                            strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                                        currentDocNo + "</b> have invalid 'Complete' data.";
                                        }

                                        if (strError == "Have error: </br>")
                                        {
                                            docList.Add(docObj);
                                        }
                                    }
                                }
                                
                            }
                        }
                    }

                    if (strError == "Have error: </br>")
                    {
                        foreach (var wpDocList in docList.GroupBy(t => t.WorkgroupId))
                        {
                            var wpObj = this.workGroupService.GetById(wpDocList.Key.GetValueOrDefault());

                            if (wpObj != null)
                            {
                                if (wpObj.IsAutoCalculate.GetValueOrDefault())
                                {
                                    var complete = 0.0;
                                    complete = wpDocList.Aggregate(complete,
                                        (current, t) => (double)(current + ((t.Weight * t.Complete) / 100)));
                                    wpObj.Complete = complete;
                                }

                                // Update total document weight for workpackage
                                var totalWeight = 0.0;
                                totalWeight = wpDocList.Aggregate(totalWeight, (current, t) => current + t.Weight.GetValueOrDefault());
                                wpObj.TotalDocWeight = totalWeight;

                                // Update total man hours for workpackage
                                var totalManHours = 0.0;
                                totalManHours = wpDocList.Aggregate(totalManHours, (current, t) => current + t.ManHours.GetValueOrDefault());
                                wpObj.TotalManHours = totalManHours;
                                wpObj.UsedManHours = Math.Round(totalManHours * (wpObj.Complete.GetValueOrDefault() / 100), 2);

                                // Update candelete for workpackage
                                wpObj.CanDelete = !wpDocList.Any();

                                this.workGroupService.Update(wpObj);

                                // Re-calculate complete value for Project
                                var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                                if (projectObj != null)
                                {
                                    projectObj.CanDelete = false;
                                    var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);

                                    if (projectObj.IsAutoCalculate.GetValueOrDefault())
                                    {
                                        var complete = 0.0;
                                        complete = wpList.Aggregate(complete,
                                            (current, t) => (current + ((t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100)));
                                        projectObj.Complete = complete;
                                    }

                                    this.scopeProjectService.Update(projectObj);
                                }

                                
                            }

                            foreach (var documentPackage in wpDocList)
                            {
                                this.documentPackageService.Update(documentPackage);
                            }

                            if (wpObj != null && wpObj.IsAutoCalculate.GetValueOrDefault())
                            {
                                var milestoneList = this.milestoneService.GetAllByWorkpackage(wpObj.ID).OrderByDescending(t => t.MilestoneDate).ToList();
                                if (milestoneList.Count > 0)
                                {
                                    var milestoneObj = milestoneList.FirstOrDefault(t => t.MilestoneDate != null
                                                                                         && t.MilestoneDate.Value.Month == DateTime.Now.Month
                                                                                         && t.MilestoneDate.Value.Year == DateTime.Now.Year);

                                    //this.milestoneService.GetAllByWorkpackage(wpObj.ID,
                                    //documentPackage.MilestoneDate.Month, documentPackage.MilestoneDate.Year)
                                    //.OrderByDescending(t => t.MilestoneDate).FirstOrDefault();
                                    if (milestoneObj != null)
                                    {
                                        milestoneObj.Real = wpObj.Complete - milestoneObj.RealTotal;
                                        milestoneObj.RealTotal = wpObj.Complete;

                                        this.milestoneService.Update(milestoneObj);

                                        //if (milestoneObj.PlanTotal < 100 && milestoneObj.RealTotal < 100
                                        //&& milestoneList[0].MilestoneDate != null
                                        //&& DateTime.ParseExact("01/" + milestoneList[0].MilestoneDate.Value.ToString("MM/yyyy"), "dd/MM/yyyy", null) < DateTime.ParseExact("01/" + DateTime.Now.AddMonths(1).ToString("MM/yyyy"), "dd/MM/yyyy", null)
                                        //&& Utility.DifferentMonth(DateTime.ParseExact("01/" + milestoneList[0].MilestoneDate.Value.ToString("MM/yyyy"), "dd/MM/yyyy", null), DateTime.ParseExact("01/" + DateTime.Now.AddMonths(1).ToString("MM/yyyy"), "dd/MM/yyyy", null)) < 2)
                                        //{
                                        //    var nextPlan = new Milestone();
                                        //    nextPlan.MilestoneDate = milestoneObj.MilestoneDate != null
                                        //        ? milestoneObj.MilestoneDate.Value.AddMonths(1)
                                        //        : (DateTime?)null;
                                        //    //nextPlan.PlanPercent = 100 - milestoneObj.PlanTotal.GetValueOrDefault();
                                        //    //nextPlan.PlanTotal = 100;
                                        //    nextPlan.RealTotal = 0;
                                        //    nextPlan.Real = 0;
                                        //    nextPlan.PerformingUser = milestoneObj.PerformingUser;
                                        //    nextPlan.Note = milestoneObj.Note;
                                        //    nextPlan.WorkpackageId = milestoneObj.WorkpackageId;
                                        //    nextPlan.WorkpackageName = milestoneObj.WorkpackageName;
                                        //    this.milestoneService.Insert(nextPlan);
                                        //}
                                    }
                                    else
                                    {
                                        milestoneObj = milestoneList[0];
                                        var newMilestoneObj = new Milestone();
                                        newMilestoneObj.MilestoneDate = DateTime.Now;
                                        //newMilestoneObj.PlanPercent = 100;
                                        //newMilestoneObj.PlanTotal = 100;
                                        newMilestoneObj.Real = wpObj.Complete - milestoneObj.RealTotal.GetValueOrDefault();
                                        newMilestoneObj.RealTotal = wpObj.Complete;
                                        newMilestoneObj.PerformingUser = milestoneObj.PerformingUser;
                                        newMilestoneObj.Note = milestoneObj.Note;
                                        newMilestoneObj.WorkpackageId = wpObj.ID;
                                        newMilestoneObj.WorkpackageName = wpObj.Name;
                                        this.milestoneService.Insert(newMilestoneObj);
                                    }
                                }
                                else
                                {
                                    var milestoneObj = new Milestone();
                                    milestoneObj.MilestoneDate = DateTime.Now;
                                    ////milestoneObj.PlanPercent = 100;
                                    ////milestoneObj.PlanTotal = 100;
                                    milestoneObj.RealTotal = wpObj.Complete;
                                    milestoneObj.Real = wpObj.Complete;
                                    milestoneObj.PerformingUser = string.Empty;
                                    milestoneObj.Note = string.Empty;
                                    milestoneObj.WorkpackageId = wpObj.ID;
                                    milestoneObj.WorkpackageName = wpObj.Name;
                                    this.milestoneService.Insert(milestoneObj);
                                }
                            }
                        }

                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                    }
                    else
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text = strError;
                    }
                }
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Data.Entities.Document objDoc)
        {
            var listUpload = uploadDocControl.UploadedFiles;
            if (listUpload.Count > 0)
            {
                foreach (UploadedFile docFile in listUpload)
                {
                    var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    docFile.SaveAs(revisionFilePath, true);
                }
            }
        }
    }
}
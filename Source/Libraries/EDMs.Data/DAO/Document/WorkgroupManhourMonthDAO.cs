﻿

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
    using System;

    public class WorkgroupManhourMonthDAO : BaseDAO
    {

        public WorkgroupManhourMonthDAO() : base() { }

        public IQueryable<WorkgroupManhourMonth> GetIQueryable()
        {
            return this.EDMsDataContext.WorkgroupManhourMonths;
        }
        public List<WorkgroupManhourMonth> GetAll()
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.OrderByDescending(t => t.ID).ToList();
        }

        public WorkgroupManhourMonth GetById(int id)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.FirstOrDefault(ob => ob.ID == id);
        }

        public List<WorkgroupManhourMonth> GetByMonthAndYear(int month, int year)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.Where(ob => ob.Month == month && ob.Year == year).ToList();
        }

        public WorkgroupManhourMonth GetByWP(int wpID)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.FirstOrDefault(ob => ob.WorkgroupID == wpID);
        }

        public WorkgroupManhourMonth GetByWP(int wpID, int month, int year)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.FirstOrDefault(ob => ob.WorkgroupID == wpID && ob.Month == month && ob.Year == year);
        }

        public List<WorkgroupManhourMonth> GetByPJ(int pjID, int month, int year)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.Where(ob => ob.ProjectID == pjID && ob.Month == month && ob.Year == year).ToList();
        }

        public List<WorkgroupManhourMonth> GetByPJ(int pjID, int departmentID, int month, int year)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.Where(ob => ob.ProjectID == pjID && ob.DeparmentID == departmentID && ob.Month == month && ob.Year == year).ToList();
        }

        public List<WorkgroupManhourMonth> GetLastedByPJ(int pjID)
        {
            var wmmObjLasted = this.EDMsDataContext.WorkgroupManhourMonths.OrderByDescending(ob => ob.ID).FirstOrDefault(ob => ob.ProjectID == pjID && ob.CompleteActualTotal != null);
            if (wmmObjLasted != null)
            {
                int month = wmmObjLasted.Month.Value;
                int year = wmmObjLasted.Year.Value;

                return this.EDMsDataContext.WorkgroupManhourMonths.Where(ob => ob.ProjectID == pjID && ob.Month == month && ob.Year == year).ToList();
            }
            else
            {
                return new List<WorkgroupManhourMonth>();
            }
        }

        public List<WorkgroupManhourMonth> GetLastedByPJAndWP(int pjID, int wpID)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.Where(ob => ob.ProjectID == pjID && ob.WorkgroupID == wpID).OrderByDescending(ob => ob.ID).ToList();
        }

        public List<WorkgroupManhourMonth> GetByPJ(List<int> pjID, int year)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.Where(ob => pjID.Contains(ob.ProjectID.Value) && ob.Year == year).ToList();
        }

        public List<WorkgroupManhourMonth> GetByWPAndPJ(int wpID, int pjID)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.Where(ob => ob.WorkgroupID == wpID && ob.ProjectID == pjID).ToList();
        }

        public WorkgroupManhourMonth GetByWPAndPJ(int wpID, int pjID, int month, int year)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.FirstOrDefault(ob => ob.WorkgroupID == wpID && ob.ProjectID == pjID && ob.Month == month && ob.Year == year);
        }

        public List<WorkgroupManhourMonth> GetByDP(int dpID, int month, int year)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.Where(ob => ob.DeparmentID == dpID && ob.Month == month && ob.Year == year).ToList();
        }

        public List<WorkgroupManhourMonth> GetByDP(int dpID, int year)
        {
            return this.EDMsDataContext.WorkgroupManhourMonths.Where(ob => ob.DeparmentID == dpID && ob.Year == year).ToList();
        }

        public int? Insert(WorkgroupManhourMonth ob)
        {
            try
            {
                this.EDMsDataContext.AddToWorkgroupManhourMonths(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        public bool Update(WorkgroupManhourMonth src)
        {
            try
            {
                WorkgroupManhourMonth des = (from rs in this.EDMsDataContext.WorkgroupManhourMonths
                                             where rs.ID == src.ID
                                             select rs).First();

                des.WorkgroupID = src.WorkgroupID;
                des.WorkgroupName = src.WorkgroupName;
                des.DeparmentID = src.DeparmentID;
                des.ProjectID = src.ProjectID;
                des.Month = src.Month;
                des.Year = src.Year;
                des.ManhourPlanMonth = src.ManhourPlanMonth;
                des.ManhourPlanTotal = src.ManhourPlanTotal;
                des.ManhourActualMonth = src.ManhourActualMonth;
                des.ManhourActualAccumulation = src.ManhourActualAccumulation;
                des.CompletePlanMonth = src.CompletePlanMonth;
                des.CompleteActualMonth = src.CompleteActualMonth;
                des.CompletePlanTotal = src.CompletePlanTotal;
                des.CompleteActualTotal = src.CompleteActualTotal;
                des.WorkgroupWeight = src.WorkgroupWeight;
                des.UpdateBy = src.UpdateBy;
                des.UpdateDate = src.UpdateDate;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(WorkgroupManhourMonth src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

    }
}

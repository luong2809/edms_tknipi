﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using System.Linq;
using System.Web.Hosting;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities.Sessions;
using System.Text.RegularExpressions;
using Telerik.Web.UI;
using Utility = EDMs.Web.Utilities.Utility;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace EDMs.Web.Controls.Scope
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Collections.Generic;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ScopeProjectEditForm : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly ScopeProjectService scopeProjectService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;
        private readonly RoleService roleservice;

        private readonly FieldService fieldService;
        private readonly PlatformService platformService;
        private readonly SupervisorService supervisorService;
        private readonly AttachFilesProjectService attachFilesProjectService;
        private readonly DocumentService documentService;
        private readonly WorkGroupService workgroupservice;

        private readonly FolderService folderService = new FolderService();
        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();
        private readonly TemplateManagementService templateManagmentService = new TemplateManagementService();
        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();
        private readonly WorkService workListService = new WorkService();
        private readonly AppendixService appendixListService = new AppendixService();
        private readonly ProjectAppendixService projectAppendixService = new ProjectAppendixService();
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();
        private readonly BlockService blockService = new BlockService();
        protected const string UnreadPattern = @"\(\d+\)";

        private const string RegexValidateEmail =
            @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ScopeProjectEditForm()
        {
            this.userService = new UserService();
            this.scopeProjectService = new ScopeProjectService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.categoryService = new CategoryService();
            this.fieldService = new FieldService();
            this.platformService = new PlatformService();
            this.supervisorService = new SupervisorService();
            this.attachFilesProjectService = new AttachFilesProjectService();
            this.workgroupservice = new WorkGroupService();
            this.documentService = new DocumentService();
            this.roleservice = new RoleService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.LoadComboData();
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var objScopeProject = this.scopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objScopeProject != null)
                    {
                        this.CreatedInfo.Visible = true;

                        this.txtName.Text = objScopeProject.Name;
                        this.txtDescription.Text = objScopeProject.Description;
                        this.txtTransferDoc.Text = objScopeProject.TransferDocOfProject;
                        this.txtScopeOfWork.Text = objScopeProject.ScopeOfWork;
                        this.txtYear.Value = objScopeProject.Year;
                        this.txtPercentComplete.Value = objScopeProject.Complete;
                        this.cbAutoCalculate.Checked = objScopeProject.IsAutoCalculate.GetValueOrDefault();
                        this.txtStartDate.SelectedDate = objScopeProject.StartDate;
                        this.txtEndDate.SelectedDate = objScopeProject.EndDate;
                        this.txtDeadline.SelectedDate = objScopeProject.Deadline;
                        this.ddlBlock.SelectedValue = objScopeProject.BlockId.ToString();
                        var fieldList = this.fieldService.GetByBlock(Convert.ToInt32(ddlBlock.SelectedValue));
                        fieldList.Insert(0, new Field() { ID = 0 });
                        this.ddlField.DataSource = fieldList;
                        this.ddlField.DataTextField = "FullName";
                        this.ddlField.DataValueField = "ID";
                        this.ddlField.DataBind();
                        this.ddlField.SelectedValue = objScopeProject.FieldId.ToString();
                        this.ddldocumentcontroller.SelectedValue = objScopeProject.DocumentControlId.ToString();
                        this.txtNotes.Text = objScopeProject.Notes;
                        //this.IdexOrderCode.Visible = true;

                        this.hidPlanName.Value = objScopeProject.planName;

                        //this.txtPlanNane.Text = objScopeProject.planName;
                        //this.ddlOderType.SelectedValue = objScopeProject.TypePlanID.ToString();
                        var platFormList = this.platformService.GetAllView();
                        platFormList.Insert(0, new Platform() { ID = 0 });
                        this.ddlPlatform.DataSource = platFormList;
                        this.ddlPlatform.DataTextField = "FullName";
                        this.ddlPlatform.DataValueField = "ID";
                        this.ddlPlatform.DataBind();

                        this.ddlPlatform.SelectedValue = objScopeProject.PlatformId.ToString();
                        this.ddlProjectManager.SelectedValue = objScopeProject.ProjectManagerId.ToString();
                        //this.ddlProjectType.SelectedValue = objScopeProject.ProjectTypeId.ToString();
                        this.ddlSupervisor.SelectedValue = objScopeProject.SupervisorId.ToString();
                        this.cbWeeklyProgress.Checked = objScopeProject.WeeklyProgress.GetValueOrDefault();
                        this.cbAutoCalculateWeight.Checked = objScopeProject.AutoCalculateWeightWorkGroup.GetValueOrDefault();
                        this.cbIDC.Checked = objScopeProject.IsIDC.GetValueOrDefault();

                        var projectAppendix = projectAppendixService.GetByProject(objScopeProject.ID);
                        if (projectAppendix != null)
                        {
                            txtBasis.Text = projectAppendix.Basis;
                            ddlYear.SelectedValue = projectAppendix.Year.GetValueOrDefault().ToString();
                            rblTypeWork.SelectedValue = projectAppendix.Type.GetValueOrDefault().ToString();
                            var workList = this.workListService.GetByTypeAndYear(Convert.ToInt32(rblTypeWork.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
                            foreach (var item in workList)
                            {
                                if (item.ParentID == 28)
                                {
                                    item.ParentID = null;
                                }
                            }
                            var rtvWorkList = (RadTreeView)this.rcbWorkList.Items[0].FindControl("rtvWorkList");
                            if (rtvWorkList != null)
                            {
                                rtvWorkList.DataTextField = "Name";
                                rtvWorkList.DataValueField = "ID";
                                rtvWorkList.DataFieldID = "ID";
                                rtvWorkList.DataFieldParentID = "ParentID";
                                rtvWorkList.DataSource = workList;
                                rtvWorkList.DataBind();
                                rtvWorkList.CollapseAllNodes();
                                RadTreeNode nodeWork = rtvWorkList.FindNodeByValue(projectAppendix.WorkID.GetValueOrDefault().ToString());
                                if (nodeWork != null)
                                {
                                    nodeWork.Selected = true;
                                    expandNode(nodeWork.ParentNode);
                                    treeViewWorkStatus.Value = "1";
                                }
                                else
                                {
                                    treeViewWorkStatus.Value = "0";
                                }
                            }
                            else
                            {
                                treeViewWorkStatus.Value = "0";
                            }
                            var appendixList = this.appendixListService.GetByWork(Convert.ToInt32(rtvWorkList.SelectedValue));
                            var rtvAppendixList = (RadTreeView)this.rcbAppendixList.Items[0].FindControl("rtvAppendixList");
                            if (rtvAppendixList != null)
                            {
                                rtvAppendixList.DataTextField = "FullName";
                                rtvAppendixList.DataValueField = "ID";
                                rtvAppendixList.DataFieldID = "ID";
                                rtvAppendixList.DataFieldParentID = "ParentID";
                                rtvAppendixList.DataSource = appendixList;
                                rtvAppendixList.DataBind();
                                rtvAppendixList.CollapseAllNodes();
                                RadTreeNode node = rtvAppendixList.FindNodeByValue(projectAppendix.AppendixID.GetValueOrDefault().ToString());
                                if (node != null)
                                {
                                    node.Selected = true;
                                    expandNode(node.ParentNode);
                                    treeViewAppendixStatus.Value = "1";
                                }
                                else
                                {
                                    treeViewAppendixStatus.Value = "0";
                                }
                            }
                            else
                            {
                                treeViewAppendixStatus.Value = "0";
                            }
                        }
                        else
                        {
                            treeViewWorkStatus.Value = "0";
                            treeViewAppendixStatus.Value = "0";
                        }

                        var createdUser = this.userService.GetByID(objScopeProject.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objScopeProject.CreatdDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objScopeProject.UpdatedBy != null && objScopeProject.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objScopeProject.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objScopeProject.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.txtYear.Value = DateTime.Now.Year;
                    this.ddlProjectManager.SelectedValue = UserSession.Current.User.Id.ToString();
                    //this.ddlProjectType.SelectedValue = "1";

                    this.CreatedInfo.Visible = false;
                }

                this.txtPercentComplete.Enabled = !this.cbAutoCalculate.Checked;
            }
        }

        public void expandNode(RadTreeNode node)
        {
            if (node != null)
            {
                node.Expanded = true;
                if (node.ParentNode != null)
                {
                    expandNode(node.ParentNode);
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                int? scopeProjectId;
                var obj = new ScopeProject();
                var platformObj = this.platformService.GetById(this.ddlPlatform.SelectedItem != null
                        ? Convert.ToInt32(this.ddlPlatform.SelectedValue)
                        : 0);

                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    scopeProjectId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    obj = this.scopeProjectService.GetById(scopeProjectId.GetValueOrDefault());
                    if (obj != null)
                    {
                        var oldvalidProject = obj.Name;
                        //var oldvalidProjectName = Utilities.Utility.RemoveSpecialCharacter(obj.Name);
                        obj.UpdatedBy = UserSession.Current.User.Id;
                        obj.UpdatedDate = DateTime.Now;
                        var newvalidProject = this.txtName.Text.Trim();
                        //var validProjectName = Utilities.Utility.RemoveSpecialCharacter(this.txtName.Text.Trim());
                        //var targetFolder = "../../DocumentLibrary/Latest Revision";
                        //var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/Latest Revision/" + validProjectName;
                        //var oldphysicalProjectFolder = Server.MapPath(targetFolder);
                        //oldphysicalProjectFolder = oldphysicalProjectFolder + @"/" + oldvalidProjectName;
                        //var newphysicalProjectFolder = Server.MapPath(targetFolder);
                        //newphysicalProjectFolder = newphysicalProjectFolder + @"/" + validProjectName;
                        //if (oldphysicalProjectFolder != newphysicalProjectFolder && oldvalidProjectName != validProjectName && oldvalidProject != validProject)
                        //{

                        //doi ten project
                        if (oldvalidProject != newvalidProject)
                        {
                            var folderRoot = this.folderService.GetAllByName(oldvalidProject);
                            folderRoot.Name = newvalidProject;
                            folderRoot.Description = newvalidProject;
                            this.folderService.Update(folderRoot);
                            var wpProject = this.workgroupservice.GetAllWorkGroupOfProject(folderRoot.ProjectId.GetValueOrDefault());
                            if (wpProject.Count > 0)
                            {
                                foreach (var wpObj in wpProject)
                                {
                                    wpObj.ProjectName = newvalidProject;
                                    this.workgroupservice.Update(wpObj);
                                }
                            }
                            obj.Name = newvalidProject;
                            this.scopeProjectService.Update(obj);
                            //if (Directory.Exists(oldphysicalProjectFolder))
                            //{
                            //    var folderRoot = this.folderService.GetAllByName(oldvalidProject);
                            //    UpdateNameProject(obj, oldvalidProjectName, validProject, validProjectName, serverFolder, oldphysicalProjectFolder, newphysicalProjectFolder, folderRoot);
                            //}
                            //else
                            //{
                            //    Directory.CreateDirectory(oldphysicalProjectFolder);
                            //    var folderRoot = this.folderService.GetAllByName(oldvalidProject);
                            //    UpdateNameProject(obj, oldvalidProjectName, validProject, validProjectName, serverFolder, oldphysicalProjectFolder, newphysicalProjectFolder, folderRoot);
                            //}
                        }

                        //doi deadline project thi tinh toan lai bang workgroupManhourMonth
                        var oldDeadline = obj.Deadline.GetValueOrDefault();
                        var newDeadline = this.txtDeadline.SelectedDate.GetValueOrDefault();
                        var diffMonth = ((newDeadline.Year - oldDeadline.Year) * 12) + newDeadline.Month - oldDeadline.Month;
                        if (diffMonth > 0)
                        {
                            var wpList = this.workgroupservice.GetAllWorkGroupOfProject(obj.ID);
                            for (int i = 1; i <= diffMonth; i++)
                            {
                                foreach (var wpItem in wpList)
                                {
                                    int month = oldDeadline.AddMonths(i).Month;
                                    int year = oldDeadline.AddMonths(i).Year;
                                    var presentWMMObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpItem.ID, wpItem.ProjectId.GetValueOrDefault(), month, year);
                                    var prevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpItem.ProjectId.GetValueOrDefault(), wpItem.ID, month, year);
                                    var docList = this.documentPackageService.GetAllByWorkgroup(wpItem.ID);
                                    var manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                                    if (presentWMMObj != null)
                                    {
                                        //wmmObj = presentWMMObj;
                                        presentWMMObj.WorkgroupWeight = wpItem.Weight.GetValueOrDefault();
                                        presentWMMObj.ManhourPlanTotal = manhourPlanTotal;
                                        presentWMMObj.ManhourPlanMonth = (presentWMMObj.ManhourPlanTotal.GetValueOrDefault() * presentWMMObj.CompletePlanMonth.GetValueOrDefault()) / 100;
                                        presentWMMObj.CompleteActualTotal = wpItem.Complete.GetValueOrDefault();
                                        presentWMMObj.ManhourActualAccumulation = (presentWMMObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                                        //presentWMMObj.ManhourActualMonth = presentWMMObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                                        if (prevWMMObj != null)
                                        {
                                            presentWMMObj.ManhourActualMonth = presentWMMObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                            presentWMMObj.CompleteActualMonth = presentWMMObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                                        }
                                        else
                                        {
                                            presentWMMObj.ManhourActualMonth = presentWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                            presentWMMObj.CompleteActualMonth = presentWMMObj.CompleteActualTotal.GetValueOrDefault();
                                        }

                                        presentWMMObj.UpdateBy = UserSession.Current.User.Id;
                                        presentWMMObj.UpdateDate = DateTime.Now;
                                        this.workgroupManhourMonthService.Update(presentWMMObj);
                                    }
                                    else
                                    {
                                        WorkgroupManhourMonth wmmObj = new WorkgroupManhourMonth();
                                        wmmObj.Month = month;
                                        wmmObj.Year = year;
                                        wmmObj.WorkgroupID = wpItem.ID;
                                        wmmObj.WorkgroupName = wpItem.Name;
                                        wmmObj.DeparmentID = wpItem.DepartmentId;
                                        wmmObj.ProjectID = wpItem.ProjectId.GetValueOrDefault();
                                        wmmObj.WorkgroupWeight = wpItem.Weight.GetValueOrDefault();
                                        wmmObj.ManhourPlanTotal = manhourPlanTotal;
                                        wmmObj.CompleteActualTotal = wpItem.Complete.GetValueOrDefault();
                                        wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                                        //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                                        if (prevWMMObj != null)
                                        {
                                            wmmObj.CompletePlanMonth = 0;
                                            wmmObj.CompletePlanTotal = prevWMMObj.CompletePlanTotal.GetValueOrDefault();
                                            wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                            wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                                        }
                                        else
                                        {
                                            wmmObj.CompletePlanMonth = 0;
                                            wmmObj.CompletePlanTotal = 100;
                                            wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                                            wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                                        }
                                        wmmObj.CreateBy = UserSession.Current.User.Id;
                                        wmmObj.CreateDate = DateTime.Now;
                                        this.workgroupManhourMonthService.Insert(wmmObj);
                                    }
                                }
                            }
                        }
                        var ListWorkgroup = this.workgroupservice.GetAllWorkGroupOfProject(obj.ID);
                        foreach (WorkGroup wp in ListWorkgroup)
                        {
                            wp.ProjectManagerID = obj.ProjectManagerId;
                            wp.ProjectManagerFullName = obj.ProjectManagerFullName;
                            this.workgroupservice.Update(wp);
                        }
                        this.CollectData(ref obj, false);
                        this.scopeProjectService.Update(obj);
                    }
                }
                else
                {
                    obj = new ScopeProject();
                    this.CollectData(ref obj, true);
                    obj.CanDelete = true;
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatdDate = DateTime.Now;
                    obj.IsStop = false;
                    scopeProjectId = this.scopeProjectService.Insert(obj);
                    if (scopeProjectId != null)
                    {
                        ////tao folder ben doclibrary
                        //folder froject
                        var folder = new Folder();
                        var parentFol = this.folderService.GetById(1276);
                        folder = CreateProjectLibrary(obj.Name, parentFol, obj);
                        // creater folder transmittal.Transmittal
                        var trans = CreateFolderLibrary("Transmittal", folder, obj);
                        // folder trans out
                        var transout = CreateFolderLibrary("Outgoing", trans, obj);

                        // Create document Folder
                        var validProjectName = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name);
                        var targetFolder = "../../DocumentLibrary/Latest Revision";
                        var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/Latest Revision/" + validProjectName;
                        var physicalProjectFolder = Server.MapPath(targetFolder);
                        physicalProjectFolder = physicalProjectFolder + @"/" + validProjectName;
                        if (!Directory.Exists(physicalProjectFolder))
                        {
                            Directory.CreateDirectory(physicalProjectFolder);
                            obj.PathLatestRevision = serverFolder;
                            this.scopeProjectService.Update(obj);
                        }
                        //Notifycation create new project
                        if (ConfigurationManager.AppSettings["EnableSendNotification"].ToLower() != "false")
                        {
                            this.NotificationAddNewWp(obj);
                        }
                    }
                    if (platformObj != null && scopeProjectId != null)
                    {
                        platformObj.CurrentProjectCount += 1;
                        this.platformService.Update(platformObj);
                    }
                }
                //them thong tin cong viec va phu luc
                if (obj != null)
                {
                    var rtvWorkList = (RadTreeView)this.rcbWorkList.Items[0].FindControl("rtvWorkList");
                    if (rtvWorkList != null)
                    {
                        // save phu luc
                        var rtvAppendixList = (RadTreeView)this.rcbAppendixList.Items[0].FindControl("rtvAppendixList");
                        if (rtvAppendixList != null)
                        {
                            //add Appendix Project
                            var projectAppendixObj = projectAppendixService.GetByProject(obj.ID, Convert.ToInt32(ddlYear.SelectedValue));
                            if (projectAppendixObj != null)
                            {
                                projectAppendixObj.ProjectID = obj.ID;
                                projectAppendixObj.Type = Convert.ToInt32(rblTypeWork.SelectedValue.ToString());
                                projectAppendixObj.WorkID = Convert.ToInt32(rtvWorkList.SelectedValue);
                                projectAppendixObj.Year = Convert.ToInt32(ddlYear.SelectedValue);
                                projectAppendixObj.Basis = this.txtBasis.Text.Trim();
                                var orderCode = "";
                                if (!string.IsNullOrEmpty(txtBasis.Text))
                                {
                                    orderCode = txtBasis.Text.Trim() + ";<br/>";
                                }
                                if (rtvAppendixList.SelectedNode != null)
                                {
                                    projectAppendixObj.AppendixID = Convert.ToInt32(rtvAppendixList.SelectedValue);
                                    var appendixObj = this.appendixListService.GetById(Convert.ToInt32(rtvAppendixList.SelectedValue));
                                    orderCode += appendixObj.AliasCode + ";<br/>";
                                    orderCode += appendixObj.Code + "; ";
                                }
                                else
                                {
                                    projectAppendixObj.AppendixID = null;
                                }
                                orderCode += rtvWorkList.SelectedNode.Text + "; ";
                                orderCode += ddlYear.SelectedValue;
                                projectAppendixObj.OrderCode = orderCode;
                                projectAppendixObj.LastUpdatedBy = UserSession.Current.User.Id;
                                projectAppendixObj.LastUpdatedDate = DateTime.Now;
                                projectAppendixService.Update(projectAppendixObj);
                            }
                            else
                            {
                                ProjectAppendix projectAppendix = new ProjectAppendix();
                                projectAppendix.ProjectID = obj.ID;
                                projectAppendix.Type = rblTypeWork.SelectedValue != null ? Convert.ToInt32(rblTypeWork.SelectedValue) : 0;
                                projectAppendix.WorkID = rtvWorkList.SelectedValue != null ? Convert.ToInt32(rtvWorkList.SelectedValue) : 0;
                                projectAppendix.Year = Convert.ToInt32(ddlYear.SelectedValue);
                                projectAppendix.Basis = this.txtBasis.Text.Trim();
                                var orderCode = "";
                                if (!string.IsNullOrEmpty(txtBasis.Text))
                                {
                                    orderCode = txtBasis.Text.Trim() + ";<br/>";
                                }
                                if (rtvAppendixList.SelectedNode != null)
                                {
                                    projectAppendix.AppendixID = Convert.ToInt32(rtvAppendixList.SelectedValue);
                                    var appendixObj = this.appendixListService.GetById(Convert.ToInt32(rtvAppendixList.SelectedValue));
                                    orderCode += appendixObj.AliasCode + ";<br/>";
                                    orderCode += appendixObj.Code + "; ";
                                }
                                else
                                {
                                    projectAppendix.AppendixID = null;
                                }
                                orderCode += rtvWorkList.SelectedNode.Text + "; ";
                                orderCode += ddlYear.SelectedValue;
                                projectAppendix.OrderCode = orderCode;
                                projectAppendix.CreatedBy = UserSession.Current.User.Id;
                                projectAppendix.CreatedDate = DateTime.Now;
                                projectAppendixService.Insert(projectAppendix);
                            }
                        }
                    }
                    //attach file project
                    const string TargetFolder = "../../DocumentLibrary/ProjectFiles";
                    var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/ProjectFiles";
                    var listUpload = docuploader.UploadedFiles;
                    if (listUpload.Count > 0)
                    {
                        foreach (UploadedFile docFile in listUpload)
                        {
                            var docFileName = docFile.FileName;
                            var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;
                            // Path file to save on server disc
                            var saveFilePath = Path.Combine(Server.MapPath(TargetFolder), serverDocFileName);
                            // Path file to download from server
                            var serverFilePath = serverFolder + "/" + serverDocFileName;
                            var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                            docFile.SaveAs(saveFilePath, true);
                            var attachFile = new AttachFilesProject()
                            {
                                ProjectId = scopeProjectId,
                                FileName = docFileName,
                                Extension = fileExt,
                                FilePath = serverFilePath,
                                ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                                FileSize = (double)docFile.ContentLength / 1024,
                                CreatedBy = UserSession.Current.User.Id,
                                CreatedDate = DateTime.Now,
                                IsDefault = true
                            };

                            this.attachFilesProjectService.Insert(attachFile);
                            if (obj != null)
                            {
                                obj.DefaultFilePath = attachFile.FilePath;
                                obj.FileExtentionIcon = attachFile.ExtensionIcon;
                                this.scopeProjectService.Update(obj);
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }

        private void UpdateNameProject(ScopeProject obj, string oldvalidProjectName, string validProject, string validProjectName, string serverFolder, string oldphysicalProjectFolder, string newphysicalProjectFolder, Folder folderRoot)
        {
            if (folderRoot != null)
            {
                //try
                //{
                var oldPath = folderRoot.DirName;
                var folderProject = this.folderService.GetAllByProject(folderRoot.ProjectId.GetValueOrDefault());
                if (folderProject.Count > 0)
                {
                    foreach (var folObj in folderProject)
                    {
                        var key = "SharedDoc/" + oldvalidProjectName;
                        var place = folObj.DirName.IndexOf(key);
                        if (place != -1)
                        {
                            place = place + 10;
                            folObj.DirName = folObj.DirName.Remove(place, oldvalidProjectName.Length).Insert(place, validProjectName);
                            this.folderService.Update(folObj);
                        }
                    }
                }

                var docProject = this.documentService.GetAllByFolder(folderProject.Select(t => Convert.ToInt32(t.ID)).ToList<int>());
                if (docProject.Count > 0)
                {
                    foreach (var docObj in docProject)
                    {
                        var key = "SharedDoc/" + oldvalidProjectName;
                        var place = docObj.FilePath.IndexOf(key);
                        if (place != -1)
                        {
                            place = place + 10;
                            docObj.FilePath = docObj.FilePath.Remove(place, oldvalidProjectName.Length).Insert(place, validProjectName);
                            this.documentService.Update(docObj);
                        }
                    }
                }
                var wpProject = this.workgroupservice.GetAllWorkGroupOfProject(folderRoot.ProjectId.GetValueOrDefault());
                if (wpProject.Count > 0)
                {
                    foreach (var wpObj in wpProject)
                    {
                        wpObj.ProjectName = validProject;
                        this.workgroupservice.Update(wpObj);
                    }
                }

                var docPKProject = this.documentPackageService.GetAllDocProject(folderRoot.ProjectId.GetValueOrDefault());
                if (wpProject.Count > 0)
                {
                    foreach (var docPKObj in docPKProject)
                    {
                        docPKObj.ProjectName = validProject;
                        this.documentPackageService.Update(docPKObj);
                    }
                }
                this.CollectData(ref obj, true);
                obj.PathLatestRevision = serverFolder;

                this.scopeProjectService.Update(obj);
                if (!Directory.Exists(newphysicalProjectFolder))
                {
                    Directory.Move(oldphysicalProjectFolder, newphysicalProjectFolder);
                }
                folderRoot.Name = validProject;
                folderRoot.Description = validProject;
                this.folderService.Update(folderRoot);
                var newpath = Server.MapPath("../../DocumentLibrary/SharedDoc") + @"/" + validProjectName;
                if (Directory.Exists(Server.MapPath(oldPath)))
                {
                    Directory.Move(Server.MapPath(oldPath), newpath);
                }
                else
                {
                    Directory.CreateDirectory(newpath);
                }

                //folderpj.DirName = folderpj.DirName.Replace(oldvalidProjectName, validProjectName);
                //var RadTreeViewObj = new RadTreeView();
                //var listFolder = this.folderService.GetAll();
                //RadTreeViewObj.DataSource = listFolder;
                //RadTreeViewObj.DataFieldParentID = "ParentID";
                //RadTreeViewObj.DataTextField = "Name";
                //RadTreeViewObj.DataValueField = "ID";
                //RadTreeViewObj.DataFieldID = "ID";
                //RadTreeViewObj.DataBind();
                //RadTreeNode selectedFolder = RadTreeViewObj.FindNodeByValue(folderpj.ID.ToString());
                //if (selectedFolder != null)
                //{
                //    foreach (var childNode in selectedFolder.GetAllNodes())
                //    {
                //        var childFolder = this.folderService.GetById(Convert.ToInt32(childNode.Value));
                //        if (childFolder != null)
                //        {
                //            childFolder.DirName = childFolder.DirName.Replace(oldvalidProjectName, validProjectName);
                //            this.folderService.Update(childFolder);
                //        }
                //    }
                //    var tempListFolderId = new List<int>();
                //    tempListFolderId.AddRange(selectedFolder.GetAllNodes().Select(t => Convert.ToInt32(t.Value)));
                //    tempListFolderId.Add(folderpj.ID);
                //    var listDocuments = this.documentService.GetAllByFolder(tempListFolderId);
                //    foreach (var document in listDocuments)
                //    {
                //        document.FilePath = document.FilePath.Replace(oldvalidProjectName, validProjectName);
                //        document.LastUpdatedBy = UserSession.Current.User.Id;
                //        document.LastUpdatedDate = DateTime.Now;
                //        this.documentService.Update(document);
                //    }
                //}
                //}
                //catch (Exception ex)
                //{ }
            }
        }

        private Folder CreateProjectLibrary(string name, Folder folder, ScopeProject scopeProjectId)
        {
            var obj = this.folderService.GetAllByName(name);
            if (obj != null)
            {
                var childFolder = this.folderService.GetAllByProject(obj.ProjectId.GetValueOrDefault());
                obj.Name = name;
                obj.Description = name;
                obj.ParentID = folder.ID;
                obj.ProjectId = scopeProjectId.ID;
                this.folderService.Update(obj);
                foreach (var item in childFolder)
                {
                    item.ProjectId = scopeProjectId.ID;
                    this.folderService.Update(item);
                }
            }
            else
            {
                obj = CreateFolderLibrary(name, folder, scopeProjectId);
            }
            return obj;
        }

        private Folder CreateFolderLibrary(string name, Folder folder, ScopeProject scopeProjectId)
        {
            var dirname = folder.DirName + "/" + Regex.Replace(name, @"[^0-9a-zA-Z]+", string.Empty);
            var obj = this.folderService.GetByDirName(dirname);
            if (obj == null)
            {
                obj = new Folder()
                {
                    Name = name,
                    Description = name,
                    ParentID = folder.ID,
                    DirName = folder.DirName + "/" + Regex.Replace(name, @"[^0-9a-zA-Z]+", string.Empty),
                    ProjectId = scopeProjectId.ID,
                    CreatedBy = UserSession.Current.User.Id,
                    CreatedDate = DateTime.Now
                };
                Directory.CreateDirectory(Server.MapPath(obj.DirName));
                var folderId = this.folderService.Insert(obj);
                var AllUser = this.userService.GetAll(false);
                var departmentManger = this.userService.GetByID(scopeProjectId.SupervisorId.GetValueOrDefault());
                var ussergiporsuper = AllUser.Where(t => t.Id == scopeProjectId.ProjectManagerId || t.Id == scopeProjectId.SupervisorId);
                var addingPermission = AllUser.Where(t => t.Id == scopeProjectId.ProjectManagerId || t.Id == scopeProjectId.SupervisorId || t.RoleId == departmentManger.RoleId).Select(t => new UserDataPermission()
                {
                    ProjectId = scopeProjectId.ID,
                    RoleId = t.RoleId,
                    FolderId = folderId,
                    UserId = t.Id,
                    IsFullPermission = true,
                    CreatedDate = DateTime.Now,
                    CreatedBy = UserSession.Current.User.Id,
                    OnlyView = false
                });
                this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
                List<int> groupOut = ConfigurationManager.AppSettings.Get("GroupNotInVien").Split(',').Select(Int32.Parse).ToList();
                var listgroup = this.roleservice.GetAll(false).Where(t => t.IsWorking.GetValueOrDefault() && !groupOut.Contains(t.Id) && t.Id != departmentManger.RoleId).Select(t => t.Id).ToList();
                var listuserview = AllUser.Where(t => listgroup.Contains(t.RoleId.GetValueOrDefault()) && (t.Id != scopeProjectId.ProjectManagerId && t.Id != scopeProjectId.SupervisorId) && !t.IsDC.GetValueOrDefault());

                addingPermission = listuserview.Select(t => new UserDataPermission()
                {
                    ProjectId = scopeProjectId.ID,
                    RoleId = t.RoleId,
                    FolderId = folderId,
                    UserId = t.Id,
                    IsFullPermission = false,
                    CreatedDate = DateTime.Now,
                    CreatedBy = UserSession.Current.User.Id,
                    OnlyView = true
                });
                this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            }
            if (name == "Outgoing")
            {
                var templatemanagment = new TemplateManagement()
                {
                    TypeId = 7,
                    Name = "Folder Outgoing",
                    FilePath = string.Empty,
                    TransFolderId = obj.ID,
                    ProjectId = scopeProjectId.ID,
                    CreatedBy = UserSession.Current.User.Id,
                    CreatedDate = DateTime.Now
                };
                this.templateManagmentService.Insert(templatemanagment);
            }
            return obj;
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if (this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Project Number.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else
            {
                this.fileNameValidator.ErrorMessage = "";

                var regx = new Regex(@"[\&\'\;\+\:\*\%\$\^\#\@\{\}\[\]\?\<\>\!\/\\]");
                if (regx.IsMatch(this.txtName.Text.Trim()))
                {
                    this.fileNameValidator.ErrorMessage += "Please remove special character.";
                    this.divFileName.Style["margin-bottom"] = "-26px;";
                    args.IsValid = false;
                }
                if (string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var obj = this.scopeProjectService.GetByName(this.txtName.Text.Trim());
                    if (obj != null)
                    {
                        this.fileNameValidator.ErrorMessage += "Project name is already exist.";
                        this.divFileName.Style["margin-bottom"] = "-26px;";
                        args.IsValid = false;
                    }
                }
            }
        }

        protected void btnAutoGenerate_Click(object sender, ImageClickEventArgs e)
        {
            var platFormObj =
                        this.platformService.GetById(Convert.ToInt32(this.ddlPlatform.SelectedItem != null
                            ? this.ddlPlatform.SelectedValue
                            : "0"));

            if (platFormObj != null)
            {
                var seq = string.Empty;
                var projectObj = this.scopeProjectService.GetAllByPlatform(platFormObj.ID);
                if (projectObj != null)
                {
                    var temp = projectObj.Name.Split('-');
                    int count = 0;
                    var flag = int.TryParse(temp[temp.Length - 1], out count);
                    count++;
                    if (flag)
                    {

                        switch (count.ToString().Length)
                        {
                            case 1:
                                seq = "00" + count;
                                break;
                            case 2:
                                seq = "0" + count;
                                break;
                            default:
                                seq = count.ToString();
                                break;
                        }
                    }
                }
                this.txtName.Text = platFormObj.Name + "-" + seq;
            }
        }

        protected void ddlField_SelectedIndexChange(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.Request.QueryString["disId"]))
            {
                this.txtName.Text = string.Empty;
                var platFormList = this.platformService.GetByField(Convert.ToInt32(this.ddlField.SelectedValue));
                platFormList.Insert(0, new Platform() { ID = 0 });
                this.ddlPlatform.DataSource = platFormList;
                this.ddlPlatform.DataTextField = "FullName";
                this.ddlPlatform.DataValueField = "ID";
                this.ddlPlatform.DataBind();
            }
            else
            {
                var platFormList = this.platformService.GetAllView();
                platFormList.Insert(0, new Platform() { ID = 0 });
                this.ddlPlatform.DataSource = platFormList;
                this.ddlPlatform.DataTextField = "FullName";
                this.ddlPlatform.DataValueField = "ID";
                this.ddlPlatform.DataBind();
            }
        }

        protected void ddlPlatform_SelectedIndexChange(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.Request.QueryString["disId"]))
            {
                var platFormObj =
                        this.platformService.GetById(Convert.ToInt32(this.ddlPlatform.SelectedItem != null
                            ? this.ddlPlatform.SelectedValue
                            : "0"));

                if (platFormObj != null)
                {
                    var seq = string.Empty;
                    var projectObj = this.scopeProjectService.GetAllByPlatform(platFormObj.ID);
                    if (projectObj != null)
                    {
                        var temp = projectObj.Name.Split('-');
                        int count = 0;
                        var flag = int.TryParse(temp[temp.Length - 1], out count);

                        if (flag)
                        {
                            count++;
                            switch (count.ToString().Length)
                            {
                                case 1:
                                    seq = "00" + count;
                                    break;
                                case 2:
                                    seq = "0" + count;
                                    break;
                                default:
                                    seq = count.ToString();
                                    break;
                            }
                        }
                    }
                    this.txtName.Text = platFormObj.Name + "-" + seq;
                }
            }
        }

        private void CollectData(ref ScopeProject obj, bool ChangeName)
        {
            var blockObj = this.blockService.GetById(this.ddlBlock.SelectedItem != null
                        ? Convert.ToInt32(this.ddlBlock.SelectedValue)
                        : 0);
            var platformObj = this.platformService.GetById(this.ddlPlatform.SelectedItem != null
                        ? Convert.ToInt32(this.ddlPlatform.SelectedValue)
                        : 0);
            var fieldObj = this.fieldService.GetById(this.ddlField.SelectedItem != null
                ? Convert.ToInt32(this.ddlField.SelectedValue)
                : 0);

            var projectManagerObj = this.userService.GetByID(Convert.ToInt32(this.ddlProjectManager.SelectedValue));
            var supervisorObj = this.userService.GetByID(Convert.ToInt32(this.ddlSupervisor.SelectedValue));
            var documentcontrollerObj = this.userService.GetByID(Convert.ToInt32(this.ddldocumentcontroller.SelectedValue));
            if (ChangeName)
            {
                obj.Name = this.txtName.Text.Trim();
            }

            obj.Description = this.txtDescription.Text.Trim();
            obj.TransferDocOfProject = this.txtTransferDoc.Text.Trim();
            obj.ScopeOfWork = this.txtScopeOfWork.Text.Trim();
            obj.Year = (int?)this.txtYear.Value;

            obj.BlockId = blockObj != null ? blockObj.ID : 0;
            obj.BlockName = blockObj != null ? blockObj.Name : string.Empty;
            obj.FieldId = fieldObj != null ? fieldObj.ID : 0;
            obj.FieldName = fieldObj != null ? fieldObj.Name : string.Empty;

            obj.PlatformId = platformObj != null ? platformObj.ID : 0;
            obj.PlatformName = platformObj != null ? platformObj.Name : string.Empty;

            if (rblTypeWork.SelectedValue == "1")
            {
                obj.ProjectTypeId = 1;
                obj.ProjectTypeName = "Planed";
            }
            else
            {
                obj.ProjectTypeId = 2;
                obj.ProjectTypeName = "Unplanned";
            }

            obj.ProjectManagerId = projectManagerObj != null
                ? projectManagerObj.Id
                : 0;
            obj.ProjectManagerFullName = projectManagerObj != null
                ? projectManagerObj.UserNameWithFullName
                : string.Empty;

            obj.SupervisorId = supervisorObj != null
                ? supervisorObj.Id
                : 0;
            obj.SupervisorFullName = supervisorObj != null
                ? supervisorObj.UserNameWithFullName
                : string.Empty;
            obj.SupervisorUserName = supervisorObj != null
                ? supervisorObj.Username
                : string.Empty;
            obj.DocumentControlId = documentcontrollerObj != null
                ? documentcontrollerObj.Id : 0;
            obj.DocumentControlName = documentcontrollerObj != null ?
                documentcontrollerObj.UserNameWithFullName : string.Empty;
            obj.StartDate = this.txtStartDate.SelectedDate;
            obj.EndDate = this.txtEndDate.SelectedDate;
            obj.Deadline = this.txtDeadline.SelectedDate;

            obj.Notes = this.txtNotes.Text;
            if (!this.cbAutoCalculate.Checked)
            {
                obj.Complete = this.txtPercentComplete.Value ?? 0;
            }
            //obj.planName = this.GetOrderName();
            //obj.TypePlanID = Convert.ToInt32(this.ddlOderType.SelectedValue);
            //obj.TypePlanName = this.ddlOderType.SelectedItem.Text;
            obj.IsAutoCalculate = this.cbAutoCalculate.Checked;
            obj.WeeklyProgress = this.cbWeeklyProgress.Checked;
            obj.AutoCalculateWeightWorkGroup = this.cbAutoCalculateWeight.Checked;
            obj.IsIDC = this.cbIDC.Checked;
        }

        private void LoadComboData()
        {
            var blockList = this.blockService.GetAll();
            blockList.Insert(0, new Block() { ID = 0 });
            this.ddlBlock.DataSource = blockList;
            this.ddlBlock.DataTextField = "Name";
            this.ddlBlock.DataValueField = "ID";
            this.ddlBlock.DataBind();

            if (this.ddlBlock.SelectedItem != null)
            {
                var fieldList = this.fieldService.GetByBlock(Convert.ToInt32(ddlBlock.SelectedValue));
                fieldList.Insert(0, new Field() { ID = 0 });
                this.ddlField.DataSource = fieldList;
                this.ddlField.DataTextField = "FullName";
                this.ddlField.DataValueField = "ID";
                this.ddlField.DataBind();
            }

            var userListFull = this.userService.GetAll(UserSession.Current.User.Id == 1);

            var userList = userListFull.Where(t => t.IsGip.GetValueOrDefault()).OrderBy(t => t.Username).ToList();
            userList.Insert(0, new User() { Id = 0 });

            this.ddlProjectManager.DataSource = userList;
            this.ddlProjectManager.DataTextField = "UserNameWithFullName";
            this.ddlProjectManager.DataValueField = "Id";
            this.ddlProjectManager.DataBind();

            var supervisorList = userListFull.Where(t => t.IsSupperViewer.GetValueOrDefault()).ToList();
            supervisorList.Insert(0, new User() { Id = 0 });
            this.ddlSupervisor.DataSource = supervisorList;
            this.ddlSupervisor.DataTextField = "UserNameWithFullName";
            this.ddlSupervisor.DataValueField = "ID";
            this.ddlSupervisor.DataBind();

            var dcuser = userListFull.Where(t => t.IsDC.GetValueOrDefault()).ToList();

            dcuser.Insert(0, new User() { Id = 0 });
            this.ddldocumentcontroller.DataSource = dcuser;
            this.ddldocumentcontroller.DataTextField = "UserNameWithFullName";
            this.ddldocumentcontroller.DataValueField = "ID";
            this.ddldocumentcontroller.DataBind();

            this.txtYear.Value = DateTime.Now.Year;

            //for (var i = DateTime.Now.Year + 5; i >= 1975; i--)
            //{
            //    this.txtOrderYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            //}
            //this.txtOrderYear.Items.Insert(0, new ListItem(string.Empty, "0"));
            //this.txtOrderYear.SelectedIndex = 0;
            if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
            {
                for (var i = DateTime.Now.Year + 2; i >= 2018; i--)
                {
                    this.ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            else
            {
                for (var i = DateTime.Now.Year + 2; i >= 2020; i--)
                {
                    this.ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            this.ddlYear.Items.Insert(0, new ListItem(string.Empty, "0"));
            this.ddlYear.SelectedIndex = 0;
        }

        protected void cbAutoCalculate_OnCheckedChanged(object sender, EventArgs e)
        {
            this.txtPercentComplete.Enabled = !this.cbAutoCalculate.Checked;
        }

        private void NotificationAddNewWp(ScopeProject projectObj)
        {
            if (projectObj != null)
            {
                var email = ConfigurationManager.AppSettings["ToEmail"];
                if (!string.IsNullOrEmpty(email))
                {
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };

                    var wpCreatedUser = this.userService.GetByID(projectObj.CreatedBy.GetValueOrDefault());
                    var subject = "Project #WPNumber# has been created on EDMS System by #WPCreatedUser#".Replace("#WPNumber#", projectObj.Name).Replace("#WPCreatedUser#", wpCreatedUser != null ? wpCreatedUser.FullName : string.Empty);
                    var body = @"Project #WPNumber# has been created on EDMS System.
                                 Project information:
                                   Project number: #WPProjectNumber#
                                   Project name: #WPProjectName#
                                   Start date: #WPStartDate#
                                   Deadline: #WPDeadline#
                                   Project type: #Type#
                                Please access http://spf.vietsov.com.vn:8002/ for more information.";

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    message.Body = body
                        .Replace("#WPNumber#", projectObj.Name)
                        .Replace("#WPProjectNumber#", projectObj != null ? projectObj.Name : string.Empty)
                        .Replace("#WPProjectName#", projectObj != null ? projectObj.Description : string.Empty)
                        .Replace("#WPStartDate#", projectObj.StartDate != null ? projectObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPDeadline#", projectObj.Deadline != null ? projectObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#Type#", projectObj.ProjectTypeName);

                    message.To.Add(new MailAddress(email));
                    var userdc = this.userService.GetByID(projectObj.DocumentControlId.GetValueOrDefault());
                    if (userdc != null && !string.IsNullOrEmpty(userdc.Email))
                    {
                        message.CC.Add(new MailAddress(userdc.Email));
                    }
                    else
                    {
                        var userdclist = this.userService.GetAll(false).Where(t => t.IsDC.GetValueOrDefault() && !string.IsNullOrEmpty(t.Email)).ToList();
                        foreach (var item in userdclist)
                        {
                            message.CC.Add(new MailAddress(item.Email));
                        }
                    }
                    smtpClient.Send(message);
                }
            }
        }

        protected void vldStartDate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtStartDate.SelectedDate == null)
            {
                this.vldStartDate.ErrorMessage = "Please enter Start Date.";
                this.divStatdate.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        protected void vldDeadline_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtDeadline.SelectedDate == null)
            {
                this.vldDeadline.ErrorMessage = "Please enter Deadline.";
                this.divDeadline.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var obj = this.scopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (obj != null)
                    {
                        var maxDeadline = this.workgroupservice.GetMaxDeadline(obj.ID);
                        if (maxDeadline != null)
                        {
                            if (this.txtDeadline.SelectedDate.GetValueOrDefault().Date < maxDeadline.GetValueOrDefault().Date)
                            {
                                this.vldDeadline.ErrorMessage = "Deadline must be between " + obj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + maxDeadline.GetValueOrDefault().ToString("dd/MM/yyyy");
                                this.divDeadline.Style["margin-bottom"] = "-26px;";
                                args.IsValid = false;
                            }
                        }
                    }
                }
            }
        }

        //protected void vldPlan_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    //var projId = !string.IsNullOrEmpty(this.Request.QueryString["disId"])
        //    //          ? Convert.ToInt32(this.Request.QueryString["disId"])
        //    //          : 0;
        //    if (this.txtPlanNane.Text.Trim().Length == 0)
        //    {
        //        this.vldPlan.ErrorMessage = "Please enter Plan Name.";
        //        this.divPlanName.Style["margin-bottom"] = "-26px;";
        //        args.IsValid = false;
        //    }
        //}
        //private string GetOrderName()
        //{
        //    string st = "";
        //    var ddlorderType =
        //              Convert.ToInt32(this.ddlOderType.SelectedItem != null
        //                  ? this.ddlOderType.SelectedValue
        //                  : "0");
        //    if (ddlorderType != 0)
        //    {
        //        switch (ddlorderType)
        //        {
        //            case 1:
        //                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
        //                {
        //                    var planName = txtPlanNane.Text;
        //                    if (planName.Length > 0)
        //                    {
        //                        //var orderCode = planName.Substring(0, planName.IndexOf("Phụ lục XDCB") - 2).Trim();
        //                        //float n;
        //                        //var appendix = float.TryParse(planName.Substring(planName.IndexOf("Phụ lục XDCB") + 13, 3).Trim(), out n) ? planName.Substring(planName.IndexOf("Phụ lục XDCB") + 13, 3).Trim() : "0";
        //                        //var orderYear = float.TryParse(planName.Substring(planName.IndexOf("Phụ lục XDCB") + 13, 3).Trim(), out n) ? planName.Substring(planName.IndexOf("Hội đồng năm") + 13, 3).Trim() : "0";

        //                        if (ddlAppendix.SelectedValue != "4" || txtOrderYear.SelectedValue != "0")
        //                        {
        //                            st = planName + ", Phụ lục XDCB " + this.ddlAppendix.SelectedItem.Text.Substring(0, this.ddlAppendix.SelectedItem.Text.IndexOf("-") - 1).Trim() + " của Hội đồng năm " + this.txtOrderYear.SelectedValue;
        //                        }
        //                        else
        //                        {
        //                            st = this.hidPlanName.Value;
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    st = this.txtPlanNane.Text + ", Phụ lục XDCB " + this.ddlAppendix.SelectedItem.Text.Substring(0, this.ddlAppendix.SelectedItem.Text.IndexOf("-") - 1).Trim() + " của Hội đồng năm " + this.txtOrderYear.SelectedValue;
        //                }
        //                break;

        //            case 2:
        //                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
        //                {
        //                    var planName = txtPlanNane.Text;
        //                    if (planName.Length > 0)
        //                    {
        //                        if (ddlExpectedlist.SelectedValue != "0" || txtOrderYear.SelectedValue != "0" || txtOrderRevision.Text != "")
        //                        {
        //                            //var orderCode = planName.Substring(0, planName.IndexOf("Danh mục dự kiến") - 2).Trim();
        //                            st = planName + ", " + this.ddlExpectedlist.SelectedItem.Text + this.txtOrderYear.SelectedValue + ", Phiên bản số " + this.txtOrderRevision.Text;
        //                        }
        //                        else
        //                        {
        //                            st = this.hidPlanName.Value;
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    st = this.txtPlanNane.Text + ", " + this.ddlExpectedlist.SelectedItem.Text + this.txtOrderYear.SelectedValue + ", Phiên bản số " + this.txtOrderRevision.Text;
        //                }
        //                break;
        //            case 3:
        //                st = this.txtPlanNane.Text;
        //                break;
        //            case 4:
        //                st = this.txtPlanNane.Text;
        //                break;
        //        }
        //    }
        //    return st;
        //}

        //protected void ddlOderType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
        //    {
        //        var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["disId"].ToString()));
        //        if (projectObj != null)
        //        {
        //            var planName = this.hidPlanName.Value;

        //            var orderCode = "";
        //            if (planName != "")
        //            {
        //                switch (projectObj.TypePlanID)
        //                {
        //                    case 1:
        //                        orderCode = planName.Substring(0, planName.IndexOf("Phụ lục XDCB") - 2).Trim();
        //                        break;
        //                    case 2:
        //                        orderCode = planName.Substring(0, planName.IndexOf("Danh mục dự kiến") - 2).Trim();
        //                        break;
        //                    case 3:
        //                        orderCode = planName;
        //                        break;
        //                    case 4:
        //                        orderCode = planName;
        //                        break;
        //                    default:
        //                        break;
        //                }
        //            }
        //            txtPlanNane.Text = orderCode;
        //        }

        //    }
        //    var ddlorderType =
        //          Convert.ToInt32(this.ddlOderType.SelectedItem != null
        //              ? this.ddlOderType.SelectedValue
        //              : "0");
        //    if (ddlorderType != 0)
        //    {
        //        switch (ddlorderType)
        //        {
        //            case 1:
        //                this.IdexOrderCode.Visible = true;
        //                this.IndexAppendix.Visible = true;
        //                this.IndexOrderYear.Visible = true;
        //                this.IndexOrderRevision.Visible = false;
        //                this.IndexExpected.Visible = false;
        //                break;
        //            case 2:
        //                this.IdexOrderCode.Visible = true;
        //                this.IndexAppendix.Visible = false;
        //                this.IndexOrderYear.Visible = true;
        //                this.IndexOrderRevision.Visible = true;
        //                this.IndexExpected.Visible = true;
        //                break;
        //            case 3:
        //                this.IdexOrderCode.Visible = true;
        //                this.IndexAppendix.Visible = false;
        //                this.IndexOrderYear.Visible = false;
        //                this.IndexOrderRevision.Visible = false;
        //                this.IndexExpected.Visible = false;
        //                break;
        //            case 4:
        //                this.IdexOrderCode.Visible = true;
        //                this.IndexAppendix.Visible = false;
        //                this.IndexOrderYear.Visible = false;
        //                this.IndexOrderRevision.Visible = false;
        //                this.IndexExpected.Visible = false;
        //                break;
        //        }
        //    }
        //}

        //protected void validateOrderType_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (this.ddlOderType.SelectedIndex == 0)
        //    {
        //        this.validateOrderType.ErrorMessage = "Please enter Order Type.";
        //        this.divOrderType.Style["margin-bottom"] = "-26px;";
        //        args.IsValid = false;
        //    }
        //}

        protected void descriptionValidate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtDescription.Text.Trim().Length == 0)
            {
                this.descriptionValidate.ErrorMessage = "Please enter Project Name.";
                this.divDescription.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else
            {

            }
        }

        //protected void vldAppendix_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (this.ddlAppendix.SelectedIndex == 0)
        //    {
        //        this.vldAppendix.ErrorMessage = "Please enter Appendix.";
        //        this.divAppendix.Style["margin-bottom"] = "-26px;";
        //        args.IsValid = false;
        //    }
        //}

        //protected void ddlWorkList_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    var appendixList = this.appendixListService.GetByWork(Convert.ToInt32(ddlWorkList.SelectedValue));
        //    var rtvAppendixList = (RadTreeView)this.ddlSystem.Items[0].FindControl("rtvAppendixList");
        //    if (rtvAppendixList != null)
        //    {
        //        rtvAppendixList.DataTextField = "FullName";
        //        rtvAppendixList.DataValueField = "ID";
        //        rtvAppendixList.DataFieldID = "ID";
        //        rtvAppendixList.DataFieldParentID = "ParentID";
        //        rtvAppendixList.DataSource = appendixList;
        //        rtvAppendixList.DataBind();
        //        rtvAppendixList.CollapseAllNodes();
        //    }
        //}

        protected void vldAppendixList_ServerValidate(object source, ServerValidateEventArgs args)
        {
            var rtvAppendixList = (RadTreeView)this.rcbAppendixList.Items[0].FindControl("rtvAppendixList");
            var rtvWorkList = (RadTreeView)this.rcbWorkList.Items[0].FindControl("rtvWorkList");
            if (!UserSession.Current.IsAdmin)
            {
                if (rtvAppendixList.SelectedNode != null)
                {
                    var appendixID = rtvAppendixList.SelectedNode.Value;
                    var appendixObj = this.appendixListService.GetById(Convert.ToInt32(appendixID));
                    var workList = workListService.GetByParent(Convert.ToInt32(rtvWorkList.SelectedNode.Value)).Select(t => t.ID).ToList();
                    var projectAppendixObj = this.projectAppendixService.GetByAppendix(Convert.ToInt32(appendixID), workList, Convert.ToInt32(ddlYear.SelectedValue));
                    if (projectAppendixObj != null && appendixObj.MultipleProject.GetValueOrDefault() == false)
                    {
                        if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                        {
                            if (projectAppendixObj.ProjectID.ToString() != this.Request.QueryString["disId"])
                            {
                                this.vldAppendixList.ErrorMessage = "Appendix already use in another project";
                                this.divAppendixList.Style["margin-bottom"] = "-26px;";
                                args.IsValid = false;
                            }
                        }
                        else
                        {
                            this.vldAppendixList.ErrorMessage = "Appendix already use in another project";
                            this.divAppendixList.Style["margin-bottom"] = "-26px;";
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    if (rblTypeWork.SelectedValue == "1" && rtvWorkList.SelectedValue != "12")
                    {
                        this.vldAppendixList.ErrorMessage = "Please enter Appendix.";
                        this.divAppendixList.Style["margin-bottom"] = "-26px;";
                        args.IsValid = false;
                    }
                }
            }
        }

        protected void rblTypeWork_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var workList = this.workListService.GetByTypeAndYear(Convert.ToInt32(rblTypeWork.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
            //workList.Insert(0, new WorkList() { ID = 0 });
            //this.ddlWorkList.DataSource = workList;
            //this.ddlWorkList.DataTextField = "Name";
            //this.ddlWorkList.DataValueField = "ID";
            //this.ddlWorkList.DataBind();
            rcbWorkList.ClearSelection();
            rcbWorkList.Text = string.Empty;
            rcbAppendixList.ClearSelection();
            rcbAppendixList.Text = string.Empty;

            var workList = new List<Work>();
            if (this.ddlBlock.SelectedValue != "0")
            {
                workList = this.workListService.GetByTypeAndYearAndBlock(Convert.ToInt32(rblTypeWork.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(this.ddlBlock.SelectedValue));
                List<Work> listTemp = new List<Work>();
                foreach (var child in workList)
                {
                    if (child.ParentID.GetValueOrDefault() != 0)
                    {
                        listTemp.Add(this.workListService.GetById(child.ParentID.GetValueOrDefault()));
                    }
                }
                workList.AddRange(listTemp);
            }
            else
            {
                workList = this.workListService.GetByTypeAndYear(Convert.ToInt32(rblTypeWork.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
            }
            foreach (var item in workList)
            {
                if (item.ParentID == 28)
                {
                    item.ParentID = null;
                }
            }
            var rtvWorkList = (RadTreeView)this.rcbWorkList.Items[0].FindControl("rtvWorkList");
            if (rtvWorkList != null)
            {
                rtvWorkList.DataTextField = "Name";
                rtvWorkList.DataValueField = "ID";
                rtvWorkList.DataFieldID = "ID";
                rtvWorkList.DataFieldParentID = "ParentID";
                rtvWorkList.DataSource = workList;
                rtvWorkList.DataBind();
                rtvWorkList.CollapseAllNodes();
                if (rblTypeWork.SelectedIndex == 1)
                {
                    rtvWorkList.Nodes[0].Selected = true;
                    rcbWorkList.Text = rtvWorkList.SelectedNode.Text;
                }
            }
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            rcbWorkList.ClearSelection();
            rcbWorkList.Text = string.Empty;
            rcbAppendixList.ClearSelection();
            rcbAppendixList.Text = string.Empty;
            var workList = new List<Work>();
            if (this.ddlBlock.SelectedValue != "0")
            {
                workList = this.workListService.GetByTypeAndYearAndBlock(Convert.ToInt32(rblTypeWork.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(this.ddlBlock.SelectedValue));
                List<Work> listTemp = new List<Work>();
                foreach (var child in workList)
                {
                    if (child.ParentID.GetValueOrDefault() != 0)
                    {
                        listTemp.Add(this.workListService.GetById(child.ParentID.GetValueOrDefault()));
                    }
                }
                workList.AddRange(listTemp);
            }
            else
            {
                workList = this.workListService.GetByTypeAndYear(Convert.ToInt32(rblTypeWork.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
            }

            foreach (var item in workList)
            {
                if (item.ParentID == 28)
                {
                    item.ParentID = null;
                }
            }
            var rtvWorkList = (RadTreeView)this.rcbWorkList.Items[0].FindControl("rtvWorkList");
            if (rtvWorkList != null)
            {
                rtvWorkList.DataTextField = "Name";
                rtvWorkList.DataValueField = "ID";
                rtvWorkList.DataFieldID = "ID";
                rtvWorkList.DataFieldParentID = "ParentID";
                rtvWorkList.DataSource = workList;
                rtvWorkList.DataBind();
                rtvWorkList.CollapseAllNodes();
                if (rblTypeWork.SelectedIndex == 1)
                {
                    rtvWorkList.Nodes[0].Selected = true;
                    rcbWorkList.Text = rtvWorkList.SelectedNode.Text;
                }
            }
        }

        protected void rtvWorkList_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            rcbAppendixList.ClearSelection();
            rcbAppendixList.Text = string.Empty;

            var rtvWorkList = (RadTreeView)this.rcbWorkList.Items[0].FindControl("rtvWorkList");
            if (rtvWorkList != null)
            {
                var appendixList = this.appendixListService.GetByWork(Convert.ToInt32(rtvWorkList.SelectedValue));
                var rtvAppendixList = (RadTreeView)this.rcbAppendixList.Items[0].FindControl("rtvAppendixList");
                if (rtvAppendixList != null)
                {
                    rtvAppendixList.DataTextField = "FullName";
                    rtvAppendixList.DataValueField = "ID";
                    rtvAppendixList.DataFieldID = "ID";
                    rtvAppendixList.DataFieldParentID = "ParentID";
                    rtvAppendixList.DataSource = appendixList;
                    rtvAppendixList.DataBind();
                    rtvAppendixList.CollapseAllNodes();
                }
            }
        }

        protected void validatePM_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.ddlSupervisor.SelectedIndex == 0)
            {
                this.validatePM.ErrorMessage = "Please enter Project Manager.";
                this.divPM.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        protected void fileUploadValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (string.IsNullOrEmpty(this.Request.QueryString["disId"]))
            {
                if (this.docuploader.UploadedFiles.Count <= 0)
                {
                    if (!UserSession.Current.IsAdmin)
                    {
                        this.fileUploadValidator.ErrorMessage = "Please select file.";
                        this.divFileUpload.Style["margin-bottom"] = "-26px;";
                        args.IsValid = false;
                    }
                }
                else
                {
                    if (docuploader.UploadedFiles[0].GetExtension().ToLower() != ".pdf")
                    {
                        this.fileUploadValidator.ErrorMessage = "Please upload file .PDF";
                        this.divFileUpload.Style["margin-bottom"] = "-26px;";
                        args.IsValid = false;
                    }
                }
            }
        }

        protected void vldBasis_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (string.IsNullOrEmpty(txtBasis.Text) && !UserSession.Current.IsAdmin)
            {
                this.vldBasis.ErrorMessage = "Please enter Basis of Project.";
                this.divBasis.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        protected void CustomValidatorYear_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.ddlYear.SelectedValue == null || this.ddlYear.SelectedValue == "0")
            {
                this.CustomValidatorYear.ErrorMessage = "Please enter Year.";
                this.DivYear.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        protected void CustomValidatorWork_ServerValidate(object source, ServerValidateEventArgs args)
        {
            var rtvWorkList = (RadTreeView)this.rcbWorkList.Items[0].FindControl("rtvWorkList");
            if (!UserSession.Current.IsAdmin && rtvWorkList != null && rtvWorkList.SelectedNode == null)
            {
                this.CustomValidatorWork.ErrorMessage = "Please select Type Of Work.";
                this.DivWork.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }

        }

        protected void ddlBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            var fieldList = this.fieldService.GetByBlock(Convert.ToInt32(ddlBlock.SelectedValue));
            fieldList.Insert(0, new Field() { ID = 0 });
            this.ddlField.DataSource = fieldList;
            this.ddlField.DataTextField = "FullName";
            this.ddlField.DataValueField = "ID";
            this.ddlField.DataBind();

            rcbWorkList.ClearSelection();
            rcbWorkList.Text = string.Empty;
            rcbAppendixList.ClearSelection();
            rcbAppendixList.Text = string.Empty;
            var workList = new List<Work>();
            if (this.ddlBlock.SelectedValue != "0")
            {
                workList = this.workListService.GetByTypeAndYearAndBlock(Convert.ToInt32(rblTypeWork.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(this.ddlBlock.SelectedValue));
                List<Work> listTemp = new List<Work>();
                foreach (var child in workList)
                {
                    if (child.ParentID.GetValueOrDefault() != 0)
                    {
                        listTemp.Add(this.workListService.GetById(child.ParentID.GetValueOrDefault()));
                    }
                }
                workList.AddRange(listTemp);
            }
            else
            {
                workList = this.workListService.GetByTypeAndYear(Convert.ToInt32(rblTypeWork.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
            }

            foreach (var item in workList)
            {
                if (item.ParentID == 28)
                {
                    item.ParentID = null;
                }
            }
            var rtvWorkList = (RadTreeView)this.rcbWorkList.Items[0].FindControl("rtvWorkList");
            if (rtvWorkList != null)
            {
                rtvWorkList.DataTextField = "Name";
                rtvWorkList.DataValueField = "ID";
                rtvWorkList.DataFieldID = "ID";
                rtvWorkList.DataFieldParentID = "ParentID";
                rtvWorkList.DataSource = workList;
                rtvWorkList.DataBind();
                rtvWorkList.CollapseAllNodes();
                if (rblTypeWork.SelectedIndex == 1)
                {
                    rtvWorkList.Nodes[0].Selected = true;
                    rcbWorkList.Text = rtvWorkList.SelectedNode.Text;
                }
            }
        }
    }
}
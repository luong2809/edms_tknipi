﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web.Hosting;
    using System.Web.UI;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;
    using System.IO;
    using Business.Services.Scope;
    using System.Linq;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ICAIncomingTransmittalEditForm : Page
    {
        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly ICATransmittalService iCATransmittalService;

        private readonly UserService userService;
        private readonly ScopeProjectService scopeProjectService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ICAIncomingTransmittalEditForm()
        {
            this.iCATransmittalService = new ICATransmittalService();
            this.userService = new UserService();
            this.scopeProjectService = new ScopeProjectService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.txtDate.SelectedDate = DateTime.Now;
                this.LoadComboData();
                this.UploadControl.Visible = true;

                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {
                    this.CreatedInfo.Visible = true;
                    var objTran = this.iCATransmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                    if (objTran != null)
                    {
                        this.txtTransmittalNumber.Text = objTran.TransmittalNumber;
                        this.ddlProject.SelectedValue = objTran.ProjectId.ToString();
                        this.txtDate.SelectedDate = objTran.IssuseDate;
                        this.ddlDivision.SelectedValue = objTran.CADivision.ToString();

                        var createdUser = this.userService.GetByID(objTran.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objTran.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objTran.LastUpdatedBy != null && objTran.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objTran.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objTran.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var objTran = new ICATransmittal();
            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                objTran = this.iCATransmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                if (objTran != null)
                {
                    objTran.TransmittalNumber = this.txtTransmittalNumber.Text.Trim();
                    objTran.ProjectName = this.ddlProject.SelectedItem.Text;
                    objTran.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                    objTran.CADivision = Convert.ToInt32(this.ddlDivision.SelectedValue);
                    objTran.IssuseDate = this.txtDate.SelectedDate;
                    objTran.TypeTran = 1;
                    objTran.LastUpdatedBy = UserSession.Current.User.Id;
                    objTran.LastUpdatedDate = DateTime.Now;

                    this.iCATransmittalService.Update(objTran);
                }
            }
            else
            {
                objTran = new ICATransmittal()
                {
                    TransmittalNumber = this.txtTransmittalNumber.Text.Trim(),
                    ProjectName = this.ddlProject.SelectedItem.Text,
                    ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                    CADivision = Convert.ToInt32(this.ddlDivision.SelectedValue),
                    IssuseDate = this.txtDate.SelectedDate,
                    TypeTran = 1,
                    CreatedBy = UserSession.Current.User.Id,
                    CreatedDate = DateTime.Now,
                };

                var transId = this.iCATransmittalService.Insert(objTran);
                
            }
            if (objTran != null)
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var filename = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                    var oldfilePath = Server.MapPath(objTran.GeneratePath);
                    var newServerPath = "../../Transmittals/Generated/" + filename;
                    var newFilePath = Server.MapPath("../../Transmittals/Generated/") + filename;
                    docFile.SaveAs(newFilePath);
                    objTran.IsGenerate = true;
                    objTran.GeneratePath = newServerPath;
                    if (File.Exists(oldfilePath))
                    {
                        File.Delete(oldfilePath);
                    }
                    this.iCATransmittalService.Update(objTran);
                }
            }
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var projectInPermission = UserSession.Current.IsGip || UserSession.Current.IsCheif
                    ? this.scopeProjectService.GetAll().Where(t => t.ProjectManagerId == UserSession.Current.User.Id)
                    : this.scopeProjectService.GetAll();
            projectInPermission = projectInPermission.OrderBy(t => t.Name).ToList();
            this.ddlProject.DataSource = projectInPermission;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc, bool isUpdateOldRev)
        {
            var listUpload = uploadDocControl.UploadedFiles;
            var revisionPath = "../../DocumentLibrary/RevisionHistory/";
            var serverRevisionFolder = HostingEnvironment.ApplicationVirtualPath + "/DocumentLibrary/RevisionHistory/";
            if (listUpload.Count > 0)
            {
                foreach (UploadedFile docFile in listUpload)
                {

                    ////if (isUpdateOldRev)
                    ////{
                    ////    docFile.SaveAs(saveFileRevisionPath, true);
                    ////}
                    ////else
                    ////{
                    ////    docFile.SaveAs(saveFilePath, true);
                    ////    var fileinfo = new FileInfo(saveFilePath);
                    ////    fileinfo.CopyTo(saveFileRevisionPath, true);
                    ////}

                    ////if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                    ////{
                    ////    watcherService.ExecuteCommand(129);
                    ////}
                }
            }
        }
    }
}
﻿namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.DAO.Scope;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class DocumentPackageService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DocumentPackageDAO repo;

        private readonly WorkGroupDAO wp;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentPackageService"/> class.
        /// </summary>
        public DocumentPackageService()
        {
            this.repo = new DocumentPackageDAO();
            this.wp = new WorkGroupDAO();
        }

        #region Get (Advances)

        public DocumentPackage GetOneByDocNo(string docNo, string revName, int projectId)
        {
            return this.repo.GetOneByDocNo(docNo, revName, projectId);
        }

        public List<DocumentPackage> GetAllByDocNo(string docNo, string revName, int projectId)
        {
            return this.repo.GetAllByDocNo(docNo, revName, projectId);
        }

        public List<DocumentPackage> GetAllByRQSM(int projectID)
        {
            return this.repo.GetAllByRQSM(projectID);
        }

        public List<DocumentPackage> GetAllByWithoutPOC(int projectID)
        {
            return this.repo.GetAllByWithoutPOC(projectID);
        }

        public List<DocumentPackage> GetAllByWithPOC(int projectID)
        {
            return this.repo.GetAllByWithPOC(projectID);
        }
        /// <summary>
        /// The get all by package.
        /// </summary>
        /// <param name="packageId">
        /// The package id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DocumentPackage> GetAllByPackage(int packageId)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete && t.IsLeaf && t.PackageId == packageId).ToList();
        }

        public List<DocumentPackage> GetAllByEngineer(int engId, int departmentId)
        {
            var temp = this.repo.GetAllByDepartment(departmentId);
            return temp.Where(t => t.EngineerId != null && (t.EngineerId.Split(',').ToList()).Any(engId.ToString().Equals)).ToList();
        }

        public List<DocumentPackage> GetAllByEngineerAndWP(int engId, int workgroupId)
        {
            var temp = this.repo.GetAllByWorkgroup(workgroupId);
            return temp.Where(t => t.EngineerId != null && (t.EngineerId.Split(',').ToList()).Any(engId.ToString().Equals)).ToList();
        }

        public List<DocumentPackage> GetAllByEngineer(int engId)
        {
            return this.repo.GetAllByLeaf().Where(t => t.EngineerId != null && (t.EngineerId.Split(',').ToList()).Any(engId.ToString().Equals)).ToList();
        }

        public List<DocumentPackage> GetAllByEngineer(List<string> engId)
        {
            return this.repo.GetAllByLeaf().Where(t => t.EngineerId != null && engId.Intersect(t.EngineerId.Split(',').ToList()).Any()).ToList();
        }

        public List<DocumentPackage> GetAllByWorkgroup(int workgroupId)
        {
            return repo.GetAllByWorkgroup(workgroupId);
        }

        public List<DocumentPackage> GetAllEMDRByWorkgroup(int workgroupId)
        {
            return repo.GetAllEMDRByWorkgroup(workgroupId);
        }

        public List<DocumentPackage> GetAllEMDRByDisciplineAndProject(int disciplineId, int projectId)
        {
            var temp = wp.GetWorkGroupByDiscipline(projectId, disciplineId);
            return repo.GetAllEMDRByWorkgroup(temp.ID);
        }

        public List<DocumentPackage> GetAllByWorkgroupHaveDocDelete(int workgroupId)
        {
            return this.repo.GetAllByWorkgroupHaveDocDelete(workgroupId);
        }
        public List<DocumentPackage> GetAllByWorkgroupComment(int workgroupId)
        {
            return this.repo.GetAllByWorkgroupComment(workgroupId);
        }
        public List<DocumentPackage> GetAllByWorkgroupForEng(int workgroupId, string engId)
        {
            return this.repo.GetAllByWorkgroupForEng(workgroupId, engId).Where(t => (t.EngineerId.Split(',').ToList()).Any(engId.ToString().Equals)).ToList();
        }

        public List<DocumentPackage> GetAllByWorkgroupInPermission(List<int> listworkgroupId)
        {
            return this.repo.GetAllByWorkgroupInPermission(listworkgroupId);
        }

        public List<DocumentPackage> GetAllEMDRByWorkgroup(int workgroupId, bool isGetAll)
        {
            //return this.repo.GetAll().Where(t => !t.IsDelete && t.IsLeaf && t.WorkgroupId == workgroupId && (isGetAll || t.IsEMDR)).ToList();
            return this.repo.GetAllByWorkgroup(workgroupId).Where(t => (isGetAll || t.IsEMDR)).ToList();
        }

        public List<DocumentPackage> GetAllDocProject(int project)
        {
            return this.repo.GetAllDocProject(project);
        }

        public List<DocumentPackage> GetAllDocList(List<int> listdoc)
        {
            return this.repo.GetAllDocList(listdoc);
        }

        public List<DocumentPackage> GetAllDocListIncomplete()
        {
            return this.repo.GetAllDocListIncomplete();
        }

        public bool IsExistByDocNo(string docNo)
        {
            return this.repo.IsExistByDocNo(docNo);
        }
        /// <summary>
        ///  get the max date of deadline document in Workpackage.
        /// </summary>
        /// <returns></returns>
        public DateTime? GetMaxDatedLineDoc(int wpId)
        {
            return this.repo.GetMaxDatedlineDocOfWorkpackage(wpId);
        }
        /// <summary>
        /// The get all related document.
        /// </summary>
        /// <param name="docId">
        /// The doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DocumentPackage> GetAllRelatedDocument(int docId)
        {
            return repo.GetAllRelatedDocument(docId);
        }

        public List<DocumentPackage> GetAllRevDoc(int parentId)
        {
            return this.repo.GetAllRevDoc(parentId);
        }

        public List<DocumentPackage> SearchDocument(int projectId, int workgroupId, string docNo, string docTitle)
        {
            return this.repo.SearchDocument(projectId, workgroupId, docNo, docTitle);
        }

        public List<DocumentPackage> SearchDocument(string searchAll, string docNo, string title, int docTypeId, int projectId, int workpackageId, int revisonid, int engineerId, List<int> wpInPermission)
        {
            return this.repo.GetAll().Where(
                t =>
                t.IsLeaf
                && !t.IsDelete
                && wpInPermission.Contains(t.WorkgroupId.GetValueOrDefault())
                && (string.IsNullOrEmpty(docNo) || t.DocNo.ToLower().Contains(docNo.ToLower()))
                && (string.IsNullOrEmpty(title) || t.DocTitle.ToLower().Contains(title.ToLower()))
                && (workpackageId == 0 || (t.WorkgroupId != null && t.WorkgroupId == workpackageId))
                && (projectId == 0 || (t.ProjectId != null && t.ProjectId == projectId))
                && (docTypeId == 0 || (t.DocumentTypeId != null && t.DocumentTypeId == docTypeId))
                && (revisonid == 0 || (t.RevisionId != null && t.RevisionId == revisonid))
                && (engineerId == 0 || (t.EngineerId != null && t.EngineerId == engineerId.ToString()))

                && (string.IsNullOrEmpty(searchAll)
                    || (!string.IsNullOrEmpty(t.DocNo) && t.DocNo.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.DocTitle) && t.DocTitle.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.ProjectName) && t.ProjectName.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.WorkgroupName) && t.WorkgroupName.ToLower().Contains(searchAll.ToLower()))

                    )).OrderBy(t => t.DocNo).ToList();
        }

        public bool IsExist(string documentNumber, int revId, int wpId)
        {
            return this.repo.IsExist(documentNumber, revId, wpId);
        }

        public bool IsExist(string documentNumber, int wpId)
        {
            return this.repo.IsExist(documentNumber, wpId);
        }

        public bool IsExistNonWP(string documentNumber, int docId)
        {
            var docObj = this.GetById(docId);
            return this.repo.GetAll().Any(t =>
                !t.IsDelete
                && t.DocNo == documentNumber
                && (docId == 0
                    || (t.ID != docObj.ID
                        && t.ID != docObj.ParentId)));
        }

        public bool IsExist(string documentNumber, string revname, int wpId)
        {

            return this.repo.IsExist(documentNumber, revname.Trim(), wpId);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<DocumentPackage> GetAll()
        {
            return this.repo.GetAll().ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public DocumentPackage GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(DocumentPackage bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(DocumentPackage bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(DocumentPackage bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

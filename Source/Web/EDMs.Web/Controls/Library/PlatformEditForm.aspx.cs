﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Library
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class PlatformEditForm : Page
    {
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly FieldService fieldService;

        private readonly PlatformService platformService;

        /// <summary>
        /// The role service.
        /// </summary>
        private readonly RoleService roleService;

        /// <summary>
        /// The role service.
        /// </summary>
        private readonly BlockService blockService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public PlatformEditForm()
        {
            this.userService = new UserService();
            this.fieldService = new FieldService();
            this.roleService = new RoleService();
            this.blockService = new BlockService();
            this.platformService = new PlatformService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["id"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = this.platformService.GetById(Convert.ToInt32(this.Request.QueryString["id"]));
                    if (obj != null)
                    {
                        this.txtName.Text = obj.Name;
                        this.txtDescription.Text = obj.Description;
                        this.ddlField.SelectedValue = obj.FieldId.GetValueOrDefault().ToString();
                        this.txtCurrentProjectCount.Value = obj.CurrentProjectCount;
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                        this.cbActive.Checked = obj.Active.GetValueOrDefault();

                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                var roleIds = string.Empty;
                var roleNames = string.Empty;
                roleIds = this.ddlDepartment.CheckedItems.Aggregate(roleIds, (current, t) => current + t.Value + ", ");
                roleNames = this.ddlDepartment.CheckedItems.Aggregate(roleNames, (current, t) => current + t.Text + ", ");
                if (!string.IsNullOrEmpty(this.Request.QueryString["id"]))
                {
                    var id = Convert.ToInt32(this.Request.QueryString["id"]);
                    var obj = this.platformService.GetById(id);
                    if (obj != null)
                    {
                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        obj.FieldId = Convert.ToInt32(this.ddlField.SelectedValue);
                        obj.FieldName = this.ddlField.SelectedItem != null
                            ? this.ddlField.SelectedItem.Text
                            : string.Empty;
                        obj.RoleId = roleIds;
                        obj.RoleName = roleNames;
                        obj.CurrentProjectCount = (int?) this.txtCurrentProjectCount.Value;
                        obj.Active = this.cbActive.Checked;
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;

                        this.platformService.Update(obj);
                    }
                }
                else
                {
                    var obj = new Platform()
                    {
                        Name = this.txtName.Text.Trim(),
                        Description = this.txtDescription.Text.Trim(),
                        FieldId = Convert.ToInt32(this.ddlField.SelectedValue),
                        FieldName = this.ddlField.SelectedItem != null
                            ? this.ddlField.SelectedItem.Text
                            : string.Empty,
                        RoleName = roleNames,
                        RoleId = roleIds,
                        CurrentProjectCount = (int?) this.txtCurrentProjectCount.Value,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        Active = this.cbActive.Checked
                    };

                    this.platformService.Insert(obj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Platform name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        private void LoadComboData()
        {
            ////var listDeparment = this.roleService.GetAllSpecial();
            
            ////this.ddlDepartment.DataSource = listDeparment;
            ////this.ddlDepartment.DataTextField = "Name";
            ////this.ddlDepartment.DataValueField = "Id";
            ////this.ddlDepartment.DataBind();
            ////this.ddlDepartment.SelectedIndex = 0;

            var listField = this.fieldService.GetAll();
            listField.Insert(0, new Field() { ID = 0, Name = string.Empty });
            this.ddlField.DataSource = listField;
            this.ddlField.DataTextField = "FullName";
            this.ddlField.DataValueField = "Id";
            this.ddlField.DataBind();
            this.ddlField.SelectedIndex = 0;
        }
    }
}
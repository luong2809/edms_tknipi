﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentNew.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the DocumentNew type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.Entities
{
    using System.Linq;

    using EDMs.Data.DAO.Document;

    /// <summary>
    /// The document new.
    /// </summary>
    public partial class MonthlyReport
    {
        public int Month
        {
            get
            {
                if (!string.IsNullOrEmpty(this.CreatedDate.ToString()))
                {
                    return this.CreatedDate.GetValueOrDefault().Month;
                }

                return 0;
            }
        }

        public int Year
        {
            get
            {
                if (!string.IsNullOrEmpty(this.CreatedDate.ToString()))
                {
                    return this.CreatedDate.GetValueOrDefault().Year;
                }

                return 0;
            }
        }
    }
}

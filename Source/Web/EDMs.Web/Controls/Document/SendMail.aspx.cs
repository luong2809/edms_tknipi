﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Scope;
    using EDMs.Business.Services.Security;
    using EDMs.Web.Utilities.Sessions;
    using EDMs.Web.Utilities;

    using Telerik.Web.UI;
    using System.IO;
    using System.Collections.Generic;
    using Data.Entities;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class SendMail : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The document new service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;
        private readonly TransmittalService transmittalService = new TransmittalService();
        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();
        private readonly FolderService folderService = new FolderService();
        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();
        private readonly AttachDocToTransmittalService attachDocToTransmittalService = new AttachDocToTransmittalService();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public SendMail()
        {
            this.documentService = new DocumentService();
            this.userService = new UserService();
            this.documentPackageService = new DocumentPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.LoadComboData();
                this.ddlEmail.Focus();
                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {

                    var tranObj = this.transmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                    var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(tranObj.ProjectId));
                    this.txtSubject.Text = "Transmittal Record No: " + tranObj.TransmittalNumber;
                    this.ddlEmail.SelectedValue = "406";
                    List<int> list = new List<int>() { 78, 81, 183, 230, 257, 342 };
                    foreach (var item in list)
                    {
                        this.ddlCC.SelectedValue = item.ToString();
                        if (ddlCC.SelectedItem != null)
                        {
                            this.ddlCC.SelectedItem.Checked = true;
                        }
                    }

                    var listAttachDocTrans = this.attachDocToTransmittalService.GetAllByTransId(Convert.ToInt32(this.Request.QueryString["tranId"]));
                    var count = 0;
                    var bodyContent = @"Kính gửi các anh, Transmittal số: " + tranObj.TransmittalNumber + @"<br/>
                                        Mã dự án: " + tranObj.ProjectName + @"<br/>
                                        Tên dự án: " + projectObj.Description + @"<br/>
                                        Bao gồm tài liệu:<br/>
                                        <table border='1' cellspacing='0'>
                                        <tr>
                                        <th style='text-align:center; width:40px'>No.</th>
                                        <th style='text-align:center; width:350px'>Name</th>
                                        <th style='text-align:center; width:350px'>Description</th>
                                        <th style='text-align:center; width:70px'>Rev</th>
                                        </tr>";
                    if (listAttachDocTrans.Count > 0)
                    {
                        foreach (var docId in listAttachDocTrans)
                        {
                            var document = this.documentPackageService.GetById(docId.DocumentId.GetValueOrDefault());
                            var port = ConfigurationSettings.AppSettings.Get("DocLibPort");
                            if (document != null)
                            {
                                count += 1;
                                bodyContent += @"<tr>
                                    <td style='text-align: center;'>" + count + @"</td>" +
                                    "<td>" + document.DocNo + @"</td>" +
                                    "<td>" + document.DocTitle + @"</td>" +
                                    "<td>" + document.RevisionName + @"</td>" +
                                    "</tr>";
                            }
                        }
                    }

                    bodyContent += @"</table>
                                        <br/>
                                        Bản mềm TL đã upload lên hệ thống Spf theo link:<br />
                                        <a href='http://spf-edms.vietsov.com.vn:8002/Controls/Document/DocumentsLibrary.aspx'>http://spf-edms.vietsov.com.vn:8002/Controls/Document/DocumentsLibrary.aspx</a><br/>";

                    this.txtEmailBody.Content = bodyContent;
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["listDoc"]))
                {


                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var listUser = this.userService.GetAll(UserSession.Current.RoleId == 1).Where(t => !string.IsNullOrEmpty(t.Email));
            this.ddlEmail.DataSource = listUser;
            this.ddlEmail.DataTextField = "Email";
            this.ddlEmail.DataValueField = "ID";
            this.ddlEmail.DataBind();

            this.ddlCC.DataSource = listUser;
            this.ddlCC.DataTextField = "Email";
            this.ddlCC.DataValueField = "ID";
            this.ddlCC.DataBind();
        }

        protected void SendMailMenu_OnButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (this.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {

                    var tranObj = this.transmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                    var projectObj = this.scopeProjectService.GetById(tranObj.ProjectId.GetValueOrDefault());

                    var folderObj = this.folderService.GetAllByName(tranObj.TransmittalNumber);
                    if (folderObj != null)
                    {
                        var selectedFolderId = folderObj.ID;
                        var folderPermissionList = this.folderService.GetAllByProject(projectObj.ID);

                        var childNodes = new List<int>();
                        var folderIdsList = this.GetAllChildren(selectedFolderId, folderPermissionList, ref childNodes);
                        childNodes = new List<int>();
                        var listParentFolderId = this.GetAllParentID(Convert.ToInt32(selectedFolderId), childNodes);
                        folderIdsList.AddRange(listParentFolderId);
                        foreach (var item in folderIdsList)
                        {
                            var user = this.userService.GetByID(Convert.ToInt32(ddlEmail.SelectedValue));
                            if (user != null)
                            {
                                var permission = this.userDataPermissionService.GetByUserId(user.Id, item);
                                if (permission == null)
                                {
                                    var permissionObj = new UserDataPermission();
                                    permissionObj.FolderId = item;
                                    permissionObj.UserId = user.Id;
                                    permissionObj.RoleId = user.RoleId;
                                    permissionObj.OnlyView = false;
                                    permissionObj.IsFullPermission = false;
                                    permissionObj.CreatedBy = UserSession.Current.User.Id;
                                    permissionObj.CreatedDate = DateTime.Now;
                                    this.userDataPermissionService.Insert(permissionObj);
                                }
                            }
                        }

                    }

                    var gipObj = this.userService.GetByID(projectObj.ProjectManagerId.GetValueOrDefault());
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(UserSession.Current.User.Email, "")
                    };

                    var message = new MailMessage();
                    message.From = new MailAddress(UserSession.Current.User.Email, UserSession.Current.User.FullName);
                    message.Subject = this.txtSubject.Text;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    message.Body = this.txtEmailBody.Content;

                    if (!string.IsNullOrEmpty(this.ddlEmail.Text))
                    {
                        var toList = this.ddlEmail.Text.Split(',').Where(t => !string.IsNullOrEmpty(t));
                        foreach (var to in toList)
                        {
                            message.To.Add(new MailAddress(to));
                        }
                        foreach (RadComboBoxItem checkeditem in ddlCC.CheckedItems)
                        {
                            message.CC.Add(new MailAddress(checkeditem.Text));
                        }
                        //message.CC.Add(gipObj.Email);

                        if (tranObj.IsGenerate.GetValueOrDefault())
                        {
                            FileInfo file = new FileInfo(Server.MapPath(tranObj.GeneratePath));
                            if (file.Exists)
                            {
                                Attachment data = new Attachment(file.FullName, System.Net.Mime.MediaTypeNames.Application.Octet);
                                message.Attachments.Add(data);
                            }
                        }

                        if (ConfigurationManager.AppSettings["EnableSendNotification"].ToLower() == "true")
                        {
                            smtpClient.Send(message);
                        }
                    }

                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "Close();", true);
                }
            }
        }

        private List<int> GetAllChildren(int parent, List<Folder> folderList, ref List<int> childNodes)
        {
            var childFolderList = this.folderService.GetSpecificFolderChild(parent);
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList.Select(t => t.ID));
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, folderList, ref childNodes);
                }
            }

            childNodes.Add(parent);

            return childNodes.Distinct().ToList();
        }

        private List<int> GetAllParentID(int folderId, List<int> listFolderId)
        {
            var folder = this.folderService.GetById(folderId);
            if (folder.ParentID != null)
            {
                listFolderId.Add(folder.ParentID.Value);
                this.GetAllParentID(folder.ParentID.Value, listFolderId);
            }

            return listFolderId;
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationEmptyEmailAddress(object source, ServerValidateEventArgs args)
        {
            if (this.ddlEmail.Text.Trim().Length == 0)
            {
                this.selectEmailValidate.ErrorMessage = "Please enter email address.";
                args.IsValid = false;
            }
        }
    }
}
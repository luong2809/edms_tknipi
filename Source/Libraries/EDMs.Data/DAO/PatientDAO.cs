﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO
{   
    public class PatientDAO : BaseDAO
    {
        public PatientDAO() : base() { }

        #region GET (Basic)
        public IQueryable<Patient> GetIQueryable()
        {
            return EDMsDataContext.Patients;
        }
        
        public List<Patient> GetAll()
        {
            return EDMsDataContext.Patients.ToList<Patient>();
        }

        public Patient GetByID(int ID)
        {
            return EDMsDataContext.Patients.Where(ob => ob.Id == ID).FirstOrDefault<Patient>();
        }

       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// Find By Full Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<Patient> Find(string name)
        {
            return EDMsDataContext.Patients.Where(ob => ob.FullName.Contains(name)).ToList<Patient>();
        }

        /// <summary>
        /// Find By FullName and PatientStatusId
        /// </summary>
        /// <param name="name"></param>
        /// <param name="patientStatusId"></param>
        /// <returns></returns>
        public List<Patient> Find(string name,int patientStatusId)
        {
            return EDMsDataContext.Patients.ToList().Where(ob => ob.FullName.Contains(name) &&  ob.PatientStatus.Id == patientStatusId).ToList<Patient>();
        }

        /// <summary>
        /// Get list of Patients By PatientStatusId
        /// </summary>
        /// <param name="patientStatusId"></param>
        /// <returns></returns>
        public List<Patient> GetByPatient(int patientStatusId)
        {
            return EDMsDataContext.Patients.ToList().Where(ob => ob.PatientStatus.Id == patientStatusId).ToList<Patient>();
        }
       
        #endregion

        #region Insert, Update, Delete
        public bool Insert(Patient ob)
        {
            try
            {
                EDMsDataContext.AddToPatients(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Patient ob)
        {
            try
            {
                //Patient _ob = GetByID(ob.Id);
                //if (_ob != null)
                //{
                //    EDMsDataContext.ApplyCurrentValues(_ob.EntityKey.EntitySetName, ob);
                //    EDMsDataContext.SaveChanges();
                //    return true;
                //}
                //else
                //    return false;

                Patient _ob;

                _ob = (from rs in EDMsDataContext.Patients
                       where rs.Id == ob.Id
                       select rs).First();

                //_ob.Id = ob.Id;
                _ob.AccountStatusId = ob.AccountStatusId;
                _ob.Address1 = ob.Address1;
                _ob.Address2 = ob.Address2;
                _ob.CellPhone = ob.CellPhone;
                _ob.City = ob.City;
                _ob.DateOfBirth = ob.DateOfBirth;
                _ob.DriverLicenseNumber = ob.DriverLicenseNumber;
                _ob.Email = ob.Email;
                _ob.EmergencyContactName = ob.EmergencyContactPhone;
                _ob.EMRPatientId = ob.EMRPatientId;
                _ob.FirstName = ob.FirstName;
                _ob.FirstSeenDate = ob.FirstSeenDate;
                _ob.FullName = ob.FullName;
                _ob.Generation = ob.Generation;
                _ob.HomePhone = ob.HomePhone;
                _ob.IdentityCard = ob.IdentityCard;
                _ob.IsCheckIn = ob.IsCheckIn;
                _ob.LastName = ob.LastName;
                _ob.LastSeenDate = ob.LastSeenDate;
                _ob.MailLabel = ob.MailLabel;
                _ob.MarialStatus = ob.MarialStatus;
                _ob.MedicalAlert = ob.MedicalAlert;
                _ob.MiddleName = ob.MiddleName;
                _ob.Occupation = ob.Occupation;
                _ob.Sex = ob.Sex;
                _ob.SSN = ob.SSN;
                _ob.StateCode = ob.StateCode;
                _ob.StatementMessage = ob.StatementMessage;
                _ob.StateName = ob.StateName;
                _ob.WorkPhone = ob.WorkPhone;
                _ob.ZipCode = ob.ZipCode;


                _ob.LastUpdate = ob.LastUpdate;
                _ob.UpdateBy = ob.UpdateBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Patient ob)
        {
            try
            {
                Patient _ob = GetByID(ob.Id);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                Patient _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

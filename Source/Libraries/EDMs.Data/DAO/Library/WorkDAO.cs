﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Library
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class WorkDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkDAO"/> class.
        /// </summary>
        public WorkDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Work> GetIQueryable()
        {
            return this.EDMsDataContext.Works;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Work> GetAll()
        {
            return this.EDMsDataContext.Works.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public Work GetById(int id)
        {
            return this.EDMsDataContext.Works.FirstOrDefault(ob => ob.ID == id);
        }

        #endregion

        #region GET ADVANCE
        public List<Work> GetByTypeAndYearAndBlock(int type, int year, int block)
        {
            return this.EDMsDataContext.Works.Where(t => t.Type == type && t.Year == year && t.ID != 28 && t.BlockId == block).ToList();
        }
        public List<Work> GetByTypeAndYear(int type, int year)
        {
            return this.EDMsDataContext.Works.Where(t => t.Type == type && t.Year == year && t.ID != 28).ToList();
        }

        public List<Work> GetByYear(int year)
        {
            return this.EDMsDataContext.Works.Where(t => t.Year == year).ToList();
        }

        public List<Work> GetByParent(int parentID)
        {
            return this.EDMsDataContext.Works.Where(t => t.ParentID == parentID || t.ID == parentID).ToList();
        }

        public List<Work> GetAllParent(int year)
        {
            return this.EDMsDataContext.Works.Where(t => t.ParentID == null && t.Year == year).ToList();
        }
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(Work ob)
        {
            try
            {
                this.EDMsDataContext.AddToWorks(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(Work src)
        {
            try
            {
                var des = (from rs in this.EDMsDataContext.Works
                           where rs.ID == src.ID
                           select rs).First();

                des.Name = src.Name;
                des.Description = src.Description;
                des.Type = src.Type;
                des.Year = src.Year;
                des.BlockId = src.BlockId;

                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(Work src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using Telerik.Web.UI.Calendar;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class WorkpackageMilestone : Page
    {
        private readonly WorkGroupService workGroupService;
        private readonly MilestoneService milestoneService;
        private readonly UserService userservice;

        private WorkGroup wpObj
        {
            get
            {
                return this.workGroupService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public WorkpackageMilestone()
        {
            this.workGroupService = new WorkGroupService();
            this.milestoneService = new MilestoneService();
            this.userservice = new UserService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.btnSave.Enabled = this.txtMilestone.SelectedDate != null;

                if (UserSession.Current.IsEngineer)
                {
                    this.EditContent.Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                    this.grdDocument.Height = 547;
                }

                // check auto calculate
                //this.txtReal.Enabled = wpObj.IsAutoCalculate.GetValueOrDefault() ? false : true;
                //this.txtRealTotal.Enabled = true;
                //this.txtPlanTotal.Enabled = true;

                this.txtPlanTotal.Enabled = false;
                this.txtReal.Enabled = false;
                this.txtRealTotal.Enabled = false;

                var userlistwp = this.userservice.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault());
                this.txtListuser.DataSource = userlistwp;
                this.txtListuser.DataTextField = "FullName";
                this.txtListuser.DataValueField = "FullName";
                this.txtListuser.DataBind();

                this.ClearData();

            }
        }




        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            this.Session.Remove("IsFillData");
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageObj = this.workGroupService.GetById(Convert.ToInt32(Request.QueryString["docId"]));

                if (Session["EditingId"] != null)
                {
                    //double differenceplan = 0;
                    double differenceReal = 0;
                    var milestoneId = Convert.ToInt32(Session["EditingId"]);
                    var milestoneObj = this.milestoneService.GetById(milestoneId);
                    if (milestoneObj != null)
                    {
                        if (milestoneObj.RealTotal != null && this.txtRealTotal.Value != null) differenceReal = (double)this.txtRealTotal.Value - milestoneObj.RealTotal.GetValueOrDefault();
                        //if (milestoneObj.PlanTotal != null && this.txtPlanTotal.Value != null) differenceplan = (double)this.txtPlanTotal.Value - milestoneObj.PlanTotal.GetValueOrDefault();

                        milestoneObj.MilestoneDate = this.txtMilestone.SelectedDate;
                        milestoneObj.PlanPercent = this.txtPlan.Value;
                        milestoneObj.PlanTotal = this.txtPlanTotal.Value;
                        milestoneObj.Real = this.txtReal.Value;
                        milestoneObj.RealTotal = this.txtRealTotal.Value;
                        milestoneObj.PerformingUser = this.txtPerformingUser.Text;
                        milestoneObj.Note = this.txtNote.Text;
                        milestoneObj.WorkpackageId = workpackageObj != null ? workpackageObj.ID : 0;
                        milestoneObj.WorkpackageName = workpackageObj != null ? workpackageObj.Name : string.Empty;
                        milestoneObj.DescriptionOfDuty = this.txtDescriptionOfDuty.Text.Trim();

                        //Tinh lai real bi am khi sua realtotal
                        //if (differenceReal != 0)
                        //{
                        var prevRealTotal = 0.0;
                        var prevMilestone = new Milestone();
                        var nextMilestone = new Milestone();
                        var milestoneList = this.milestoneService.GetAllByWorkpackage(workpackageObj.ID).OrderByDescending(t => t.MilestoneDate).ToList();
                        if (milestoneList.Count > 0)
                        {
                            var prevIndex = milestoneList.IndexOf(milestoneObj) + 1;
                            var nextIndex = milestoneList.IndexOf(milestoneObj) - 1;
                            if (prevIndex <= (milestoneList.Count - 1))
                            {
                                prevMilestone = milestoneList[prevIndex];
                                if (prevMilestone != null)
                                {
                                    prevRealTotal = prevMilestone.RealTotal.GetValueOrDefault();
                                    milestoneObj.Real = this.txtRealTotal.Value - prevRealTotal;
                                }
                            }
                            else
                            {
                                milestoneObj.Real = this.txtReal.Value;
                            }
                            if (nextIndex != -1)
                            {
                                nextMilestone = milestoneList[nextIndex];
                                if (nextMilestone != null)
                                {
                                    nextMilestone.Real = nextMilestone.RealTotal - this.txtRealTotal.Value;
                                    this.milestoneService.Update(nextMilestone);
                                }
                            }
                        }
                        //}
                        this.milestoneService.Update(milestoneObj);
                        if (this.txtPlannextMonth.Value.HasValue)
                        {
                            if (milestoneList.IndexOf(milestoneObj) == 0)
                            {
                                var nextMilestoneObj = new Milestone();
                                nextMilestoneObj.MilestoneDate = milestoneObj.MilestoneDate.Value.AddMonths(1);
                                nextMilestoneObj.PlanPercent = this.txtPlannextMonth.Value;
                                nextMilestoneObj.PlanTotal = milestoneObj.PlanTotal + this.txtPlannextMonth.Value;
                                nextMilestoneObj.Real = 0;
                                nextMilestoneObj.RealTotal = milestoneObj.RealTotal;
                                nextMilestoneObj.PerformingUser = milestoneObj.PerformingUser;
                                nextMilestoneObj.Note = milestoneObj.Note;
                                nextMilestoneObj.WorkpackageId = wpObj.ID;
                                nextMilestoneObj.WorkpackageName = wpObj.Name;
                                if (DateTime.Now.Year > milestoneObj.MilestoneDate.Value.Year && (DateTime.Now.Year - milestoneObj.MilestoneDate.Value.Year) > 0)
                                {
                                    nextMilestoneObj.PlanOfYear = 0;
                                }
                                else
                                {
                                    nextMilestoneObj.PlanOfYear = milestoneObj.PlanOfYear;
                                }
                                nextMilestoneObj.CreatedBy = UserSession.Current.User.Id;
                                nextMilestoneObj.CreatedDate = DateTime.Now;
                                this.milestoneService.Insert(nextMilestoneObj);
                            }
                        }

                        //if (milestoneObj.MilestoneDate.GetValueOrDefault().Month == DateTime.Now.Month
                        //    && milestoneObj.MilestoneDate.GetValueOrDefault().Year == DateTime.Now.Year
                        //    && !this.wpObj.IsAutoCalculate.GetValueOrDefault()
                        //    && milestoneObj.PlanTotal.GetValueOrDefault() != 0)
                        //{
                        //    this.wpObj.Complete = milestoneObj.RealTotal;
                        //    if (this.wpObj.Complete.GetValueOrDefault() == 100 && string.IsNullOrEmpty(this.wpObj.EndDate.GetValueOrDefault().ToString())) { this.wpObj.EndDate = DateTime.Now; }
                        //    this.workGroupService.Update(this.wpObj);
                        //}

                        //auto update % difference total
                        //var wpcomplete = this.wpObj.Complete;
                        //var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                        //var allmileston = this.milestoneService.GetAllByWorkpackage(workpackageId).OrderBy(t => t.ID).ToList();
                        //foreach (var milestoncurren in allmileston)
                        //{
                        //    if (milestoncurren.ID > milestoneObj.ID)
                        //    {
                        //        milestoncurren.PlanTotal = milestoncurren.PlanTotal + differenceplan;
                        //        if (!this.wpObj.IsAutoCalculate.GetValueOrDefault())
                        //        {
                        //            milestoncurren.RealTotal = milestoncurren.RealTotal.GetValueOrDefault() > 0 ? milestoncurren.RealTotal + differenceReal : 0;
                        //        }
                        //        if (milestoncurren.RealTotal.GetValueOrDefault() > 0) wpcomplete = milestoncurren.RealTotal;
                        //        this.milestoneService.Update(milestoncurren);
                        //    }
                        //}
                        //this.wpObj.Complete = wpcomplete;
                        //this.workGroupService.Update(this.wpObj);
                        // Auto add next plan
                        //var milestoneList =
                        //    this.milestoneService.GetAllByWorkpackage(milestoneObj.WorkpackageId.GetValueOrDefault())
                        //        .OrderByDescending(t => t.MilestoneDate)
                        //        .ToList();
                        //if (this.txtPlannextMonth.Value != null && this.txtPlannextMonth.Value != 0 &&
                        //    milestoneList.Any()
                        //    && milestoneObj.RealTotal < 100
                        //    && milestoneList[0].MilestoneDate != null
                        //    &&
                        //    DateTime.ParseExact("01/" + milestoneList[0].MilestoneDate.Value.ToString("MM/yyyy"),
                        //        "dd/MM/yyyy", null) <
                        //    DateTime.ParseExact("01/" + DateTime.Now.AddMonths(1).ToString("MM/yyyy"), "dd/MM/yyyy",
                        //        null)
                        //    &&
                        //    Utility.DifferentMonth(
                        //        DateTime.ParseExact(
                        //            "01/" + milestoneList[0].MilestoneDate.Value.ToString("MM/yyyy"), "dd/MM/yyyy",
                        //            null),
                        //        DateTime.ParseExact("01/" + DateTime.Now.AddMonths(1).ToString("MM/yyyy"),
                        //            "dd/MM/yyyy", null)) < 2)
                        //{
                        //    var nextPlan = new Milestone();
                        //    nextPlan.MilestoneDate = this.txtMilestone.SelectedDate != null
                        //        ? this.txtMilestone.SelectedDate.GetValueOrDefault().AddMonths(1)
                        //        : (DateTime?)null;
                        //    nextPlan.PlanPercent = this.txtPlannextMonth.Value;
                        //    nextPlan.PlanTotal = (this.txtPlannextMonth.Value.GetValueOrDefault() +
                        //        milestoneObj.PlanTotal.GetValueOrDefault()) > 100 ? 100 : this.txtPlannextMonth.Value.GetValueOrDefault() +
                        //                         milestoneObj.PlanTotal.GetValueOrDefault();
                        //    nextPlan.RealTotal = 0;
                        //    nextPlan.Real = 0;
                        //    nextPlan.PerformingUser = this.txtPerformingUser.Text.Trim();
                        //    nextPlan.Note = this.txtNote.Text.Trim();
                        //    nextPlan.WorkpackageId = milestoneObj.WorkpackageId;
                        //    nextPlan.WorkpackageName = milestoneObj.WorkpackageName;
                        //    nextPlan.DescriptionOfDuty = milestoneObj.DescriptionOfDuty;
                        //    this.milestoneService.Insert(nextPlan);
                        //}
                    }

                    Session.Remove("EditingId");
                }
                else
                {
                    var milestoneList = this.milestoneService.GetAllByWorkpackage(wpObj.ID).OrderByDescending(t => t.MilestoneDate).ToList();

                    if (milestoneList.Count > 0)
                    {
                        var milestoneObj = milestoneList.FirstOrDefault(t => t.MilestoneDate != null
                                                                             && t.MilestoneDate.Value.Month == this.txtMilestone.SelectedDate.Value.Month
                                                                             && t.MilestoneDate.Value.Year == this.txtMilestone.SelectedDate.Value.Year);
                        if (milestoneObj == null)
                        {
                            milestoneObj = milestoneList[0];
                            var newMilestoneObj = new Milestone();
                            newMilestoneObj.MilestoneDate = this.txtMilestone.SelectedDate;
                            newMilestoneObj.PlanPercent = this.txtPlan.Value;
                            newMilestoneObj.PlanTotal = milestoneObj.PlanTotal + this.txtPlan.Value > 100 ? 100 : milestoneObj.PlanTotal + this.txtPlan.Value;
                            newMilestoneObj.Real = 0;
                            newMilestoneObj.RealTotal = workpackageObj.Complete;
                            newMilestoneObj.PerformingUser = this.txtPerformingUser.Text;
                            newMilestoneObj.Note = this.txtNote.Text;
                            newMilestoneObj.WorkpackageId = wpObj.ID;
                            newMilestoneObj.WorkpackageName = wpObj.Name;
                            if (DateTime.Now.Year > milestoneObj.MilestoneDate.Value.Year && (DateTime.Now.Year - milestoneObj.MilestoneDate.Value.Year) > 0)
                            {
                                newMilestoneObj.PlanOfYear = 0;
                            }
                            else
                            {
                                newMilestoneObj.PlanOfYear = milestoneObj.PlanOfYear;
                            }
                            newMilestoneObj.CreatedBy = UserSession.Current.User.Id;
                            newMilestoneObj.CreatedDate = DateTime.Now;
                            this.milestoneService.Insert(newMilestoneObj);
                        }
                    }
                    else
                    {
                        var milestoneObj = new Milestone();
                        milestoneObj.MilestoneDate = this.txtMilestone.SelectedDate;
                        milestoneObj.PlanPercent = this.txtPlan.Value;
                        milestoneObj.PlanTotal = this.txtPlan.Value;
                        milestoneObj.Real = 0;
                        milestoneObj.RealTotal = workpackageObj.Complete;
                        milestoneObj.PerformingUser = this.txtPerformingUser.Text;
                        milestoneObj.Note = this.txtNote.Text;
                        milestoneObj.WorkpackageId = wpObj.ID;
                        milestoneObj.WorkpackageName = wpObj.Name;
                        milestoneObj.PlanOfYear = 0;
                        milestoneObj.CreatedBy = UserSession.Current.User.Id;
                        milestoneObj.CreatedDate = DateTime.Now;
                        this.milestoneService.Insert(milestoneObj);
                    }

                    //var wpId = this.milestoneService.Insert(workpackageMilestone);
                    //if (wpId != null)
                    //{
                    //    if (workpackageMilestone.MilestoneDate.GetValueOrDefault().Month == DateTime.Now.Month
                    //        && workpackageMilestone.MilestoneDate.GetValueOrDefault().Year == DateTime.Now.Year
                    //        && !this.wpObj.IsAutoCalculate.GetValueOrDefault()
                    //        && workpackageMilestone.RealTotal > 0)
                    //    {
                    //        this.wpObj.Complete = workpackageMilestone.RealTotal;
                    //        this.workGroupService.Update(this.wpObj);
                    //    }
                    //    // Auto add next plan
                    //    var milestoneList =
                    //        this.milestoneService.GetAllByWorkpackage(
                    //            workpackageMilestone.WorkpackageId.GetValueOrDefault())
                    //            .OrderByDescending(t => t.MilestoneDate)
                    //            .ToList();
                    //    if (milestoneList.Any()
                    //        && milestoneList[0].RealTotal < 100
                    //        && milestoneList[0].MilestoneDate != null
                    //        &&
                    //        DateTime.ParseExact("01/" + milestoneList[0].MilestoneDate.Value.ToString("MM/yyyy"),
                    //            "dd/MM/yyyy", null) <
                    //        DateTime.ParseExact("01/" + DateTime.Now.AddMonths(1).ToString("MM/yyyy"), "dd/MM/yyyy",
                    //            null)
                    //        &&
                    //        Utility.DifferentMonth(
                    //            DateTime.ParseExact(
                    //                "01/" + milestoneList[0].MilestoneDate.Value.ToString("MM/yyyy"), "dd/MM/yyyy",
                    //                null),
                    //            DateTime.ParseExact("01/" + DateTime.Now.AddMonths(1).ToString("MM/yyyy"),
                    //                "dd/MM/yyyy", null)) < 2)
                    //    {
                    //        var nextPlan = new Milestone();
                    //        nextPlan.MilestoneDate = this.txtMilestone.SelectedDate != null
                    //            ? this.txtMilestone.SelectedDate.GetValueOrDefault().AddMonths(1)
                    //            : (DateTime?)null;
                    //        nextPlan.PlanPercent = this.txtPlannextMonth.Value;
                    //        nextPlan.PlanTotal =( this.txtPlannextMonth.Value.GetValueOrDefault() +
                    //            workpackageMilestone.PlanTotal.GetValueOrDefault()) > 100 ? 100 : this.txtPlannextMonth.Value.GetValueOrDefault() +
                    //                           workpackageMilestone.PlanTotal.GetValueOrDefault();
                    //        nextPlan.RealTotal = 0;
                    //        nextPlan.Real = 0;
                    //        nextPlan.PerformingUser = this.txtPerformingUser.Text.Trim();
                    //        nextPlan.Note = this.txtNote.Text.Trim();
                    //        nextPlan.WorkpackageId = milestoneList[0].WorkpackageId;
                    //        nextPlan.WorkpackageName = milestoneList[0].WorkpackageName;
                    //        nextPlan.DescriptionOfDuty = milestoneList[0].DescriptionOfDuty;
                    //        this.milestoneService.Insert(nextPlan);
                    //    }
                    //}
                }
            }
            this.txtPlannextMonth.Enabled = false;
            this.ClearData();
            this.grdDocument.Rebind();
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                var wp = this.milestoneService.GetAllByWorkpackage(workpackageId).ToList();
                if (wp.Count > 0)
                {
                    if (wp[0].ID == docId)
                    {
                        this.milestoneService.Delete(docId);

                        if (!wpObj.IsAutoCalculate.GetValueOrDefault())
                        {
                            wp = this.milestoneService.GetAllByWorkpackage(workpackageId).ToList();
                            if (wp.Count > 0)
                            {
                                wpObj.Complete = wp[0].RealTotal > 0 ? wp[0].RealTotal : wpObj.Complete;
                                this.workGroupService.Update(wpObj);
                            }
                            else
                            {
                                wpObj.Complete = 0;
                                this.workGroupService.Update(wpObj);
                            }
                        }
                        this.grdDocument.Rebind();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(
                                      this,
                                      typeof(Page),
                                      "Alert",
                                      "<script>alert('Only allowed to delete the latest milestone.');</script>",
                                      false);

                    }

                }
            }


        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                this.grdDocument.DataSource = this.milestoneService.GetAllByWorkpackage(workpackageId);

            }
            else
            {
                this.grdDocument.DataSource = new List<Milestone>();
            }

        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            throw new NotImplementedException();
        }

        protected void rbtnDefaultDoc_CheckedChanged(object sender, EventArgs e)
        {
            //((GridItem)((RadioButton)sender).Parent.Parent).Selected = ((RadioButton)sender).Checked;

            //var item = ((RadioButton)sender).Parent.Parent as GridDataItem;
            //var attachFileId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            //var attachFileObj = this.attachFileService.GetById(attachFileId);
            //if (attachFileObj != null)
            //{
            //    var attachFiles = this.attachFileService.GetAllByDocId(attachFileObj.DocumentId.GetValueOrDefault());
            //    foreach (var attachFile in attachFiles)
            //    {
            //        attachFile.IsDefault = attachFile.ID == attachFileId;
            //        this.attachFileService.Update(attachFile);
            //    }
            //}
        }

        protected void grdDocument_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = (GridDataItem)e.Item;
                if (e.CommandName == "EditCmd")
                {
                    var milestoneId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
                    var milestoneObj = this.milestoneService.GetById(milestoneId);
                    if (milestoneObj != null)
                    {
                        Session.Add("EditingId", milestoneObj.ID);
                        this.txtPlanTotal.Enabled = true;
                        this.txtReal.Enabled = true;
                        this.txtRealTotal.Enabled = true;
                        this.txtMilestone.SelectedDate = milestoneObj.MilestoneDate;
                        this.txtPlan.Value = milestoneObj.PlanPercent;
                        this.txtPlanTotal.Value = milestoneObj.PlanTotal;
                        this.txtReal.Value = milestoneObj.Real;
                        this.txtRealTotal.Value = milestoneObj.RealTotal;
                        this.txtPerformingUser.Text = milestoneObj.PerformingUser;
                        if (!string.IsNullOrEmpty(milestoneObj.PerformingUser))
                        {
                            var userlist = milestoneObj.PerformingUser;
                            foreach (RadComboBoxItem items in this.txtListuser.Items)
                            {
                                if (userlist.Contains(items.Value))
                                {
                                    items.Checked = true;
                                }
                            }
                        }

                        this.txtNote.Text = milestoneObj.Note;
                    }

                    this.btnSave.Enabled = this.txtMilestone.SelectedDate != null;
                    this.txtPlannextMonth.Enabled = true;

                    //kiem tra thang co phai moi nhat ko neu ko an txtplannextmonth
                    var date = txtMilestone.SelectedDate;
                    if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                    {
                        var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                        var wp = this.milestoneService.GetAllByWorkpackage(workpackageId).ToList();
                        if (wp.Count > 1)
                        {
                            DateTime dateofmileston = wp[0].MilestoneDate.GetValueOrDefault();
                            this.txtPlannextMonth.Enabled = (date.GetValueOrDefault().Month < dateofmileston.Month) ? false : true;
                        }
                    }
                    Session.Add("LastId", FindLastIdMileston(milestoneObj.ID));
                }
            }
        }

        private void ClearData()
        {
            this.txtMilestone.SelectedDate = null;
            this.txtPerformingUser.Text = string.Empty;
            this.txtListuser.ClearCheckedItems();
            this.txtNote.Text = string.Empty;
            this.txtPlan.Value = null;
            this.txtPlanTotal.Value = null;
            this.txtReal.Value = null;
            this.txtRealTotal.Value = null;
            this.txtPlannextMonth.Value = null;
            this.txtDescriptionOfDuty.Text = string.Empty;
            Session.Remove("EditingId");
            Session.Remove("LastId");
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            this.ClearData();
            this.btnSave.Enabled = this.txtMilestone.SelectedDate != null;
        }

        protected void txtReal_OnTextChanged(object sender, EventArgs e)
        {
            if (wpObj != null)
            {
                if (Session["LastId"] != null)
                {
                    var milestoneId = Convert.ToInt32(Session["LastId"]);
                    var milestoneObj = this.milestoneService.GetById(milestoneId);
                    if (milestoneObj != null)
                    {

                        this.txtRealTotal.Value = (milestoneObj.RealTotal.GetValueOrDefault() + this.txtReal.Value) > 100 ? 100 : milestoneObj.RealTotal.GetValueOrDefault() + this.txtReal.Value;
                    }
                    else
                    {
                        milestoneId = Convert.ToInt32(Session["EditingId"]);
                        milestoneObj = this.milestoneService.GetById(milestoneId);
                        if (milestoneObj != null)
                        {
                            var tempdifference = this.txtReal.Value - milestoneObj.Real.GetValueOrDefault();
                            this.txtRealTotal.Value = this.txtRealTotal.Value + tempdifference;
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                    {
                        var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                        var wp = this.milestoneService.GetAllByWorkpackage(workpackageId).ToList();
                        if (wp.Count != 0)
                        {
                            this.txtRealTotal.Value = (wp[0].RealTotal.GetValueOrDefault() + this.txtReal.Value) > 100 ? 100 : wp[0].RealTotal.GetValueOrDefault() + this.txtReal.Value;
                        }
                        else
                        {
                            this.txtRealTotal.Value = this.txtReal.Value;
                        }
                    }
                }
            }
        }

        protected void txtPlan_OnTextChanged(object sender, EventArgs e)
        {
            if (wpObj != null)
            {
                if (Session["LastId"] != null)
                {
                    var milestoneId = Convert.ToInt32(Session["LastId"]);
                    var milestoneObj = this.milestoneService.GetById(milestoneId);
                    if (milestoneObj != null)
                    {

                        this.txtPlanTotal.Value = (milestoneObj.PlanTotal.GetValueOrDefault() + this.txtPlan.Value) > 100 ? 100 : milestoneObj.PlanTotal.GetValueOrDefault() + this.txtPlan.Value;
                    }
                    else
                    {
                        milestoneId = Convert.ToInt32(Session["EditingId"]);
                        milestoneObj = this.milestoneService.GetById(milestoneId);
                        if (milestoneObj != null)
                        {
                            var tempdifference = this.txtPlan.Value - milestoneObj.PlanPercent.GetValueOrDefault();
                            this.txtPlanTotal.Value = this.txtPlanTotal.Value + tempdifference;
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                    {
                        var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                        var wp = this.milestoneService.GetAllByWorkpackage(workpackageId).ToList();
                        if (wp.Count != 0)
                        {
                            this.txtPlanTotal.Value = (wp[0].PlanTotal.GetValueOrDefault() + this.txtPlan.Value) > 100 ? 100 : wp[0].PlanTotal.GetValueOrDefault() + this.txtPlan.Value;
                        }
                        else
                        {
                            this.txtPlanTotal.Value = this.txtPlan.Value;
                        }
                    }
                }
            }
        }
        ////protected void ServerValidationMilestone(object source, ServerValidateEventArgs args)
        ////{
        ////    if (this.txtMilestone.SelectedDate == null)
        ////    {
        ////        this.MilestoneValidator.ErrorMessage = "Please enter Milestone.";
        ////        this.divMilestone.Style["margin-bottom"] = "-26px;";
        ////        args.IsValid = false;
        ////    }
        ////}
        protected bool checkDateOfMileston()
        {
            //kiem tra thang co phai moi nhat ko neu ko an txtplannextmonth
            var date = txtMilestone.SelectedDate;
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]) && !string.IsNullOrEmpty(date.ToString()))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                var wp = this.milestoneService.GetAllByWorkpackage(workpackageId).ToList();
                if (wp.Count > 1)
                {
                    DateTime dateofmileston = wp[0].MilestoneDate.GetValueOrDefault();
                    if (date.GetValueOrDefault().Month < dateofmileston.Month) return true;

                }
            }
            return false;
        }
        protected void txtMilestone_OnSelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            this.btnSave.Enabled = this.txtMilestone.SelectedDate != null;
            //kiem tra thang co phai moi nhat ko neu ko an txtplannextmonth
            var date = txtMilestone.SelectedDate;
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                var wp = this.milestoneService.GetAllByWorkpackage(workpackageId).ToList();
                if (wp.Count > 1)
                {
                    DateTime dateofmileston = wp[0].MilestoneDate.GetValueOrDefault();
                    this.txtPlannextMonth.Enabled = (date.GetValueOrDefault().Month < dateofmileston.Month) ? false : true;
                }
            }
        }

        protected int FindLastIdMileston(int milestonId)
        {
            var Id = -1;
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                var wp = this.milestoneService.GetAllByWorkpackage(workpackageId).OrderBy(t => t.MilestoneDate).ToList();
                foreach (var miles in wp)
                {
                    if (miles.ID > Id && miles.ID < milestonId) Id = miles.ID;
                }
            }

            return Id;
        }

        protected void txtListuser_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var tolist = string.Empty;
            tolist = this.txtListuser.CheckedItems.Aggregate(tolist, (current, t) => current + t.Value + ",");
            this.txtPerformingUser.Text = tolist;
        }

        protected void btnCalculator_Click(object sender, EventArgs e)
        {
            if (Session["EditingId"] == null)
            {
                var lastedMilestone = this.milestoneService.GetAllByWorkpackage(wpObj.ID).OrderByDescending(t => t.MilestoneDate).FirstOrDefault();
                if (lastedMilestone != null)
                {
                    this.txtMilestone.SelectedDate = lastedMilestone.MilestoneDate.Value.AddMonths(1);
                    this.txtPlan.Text = "0";
                    this.txtPlanTotal.Text = lastedMilestone.PlanTotal.ToString();
                    this.txtReal.Text = Math.Round(wpObj.Complete.GetValueOrDefault() - lastedMilestone.RealTotal.GetValueOrDefault(), 2).ToString();
                    this.txtRealTotal.Text = wpObj.Complete.ToString();
                    this.txtPerformingUser.Text = lastedMilestone.PerformingUser;
                    if (!string.IsNullOrEmpty(lastedMilestone.PerformingUser))
                    {
                        var userlist = lastedMilestone.PerformingUser;
                        foreach (RadComboBoxItem items in this.txtListuser.Items)
                        {
                            if (userlist.Contains(items.Value))
                            {
                                items.Checked = true;
                            }
                        }
                    }
                    this.txtNote.Text = lastedMilestone.Note;
                    btnSave.Enabled = true;
                }
            }
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;

                Label lblReal = (Label)item["Real"].FindControl("lblReal");
                if (lblReal.Text.Contains("%"))
                {
                    double weight = Convert.ToDouble(lblReal.Text.Replace("%", string.Empty));
                    weight = Math.Round(weight, 2);
                    lblReal.Text = weight.ToString() + "%";
                }
                Label lblRealTotal = (Label)item["RealTotal"].FindControl("lblRealTotal");
                if (lblRealTotal.Text.Contains("%"))
                {
                    double complete = Convert.ToDouble(lblRealTotal.Text.Replace("%", string.Empty));
                    complete = Math.Round(complete, 2);
                    lblRealTotal.Text = complete.ToString() + "%";
                }
            }
        }
    }
}
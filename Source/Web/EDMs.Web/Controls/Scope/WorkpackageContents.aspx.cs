﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Linq;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class WorkpackageContents : Page
    {
        private readonly WorkGroupService workGroupService;
        private readonly WorkpackageContentService workpackageContentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public WorkpackageContents()
        {
            this.workGroupService = new WorkGroupService();
            this.workpackageContentService = new WorkpackageContentService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (UserSession.Current.IsEngineer)
                {
                    this.EditContent.Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                    this.grdDocument.Height = 547;
                }

                this.ClearData();
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.Session.Remove("IsFillData");
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageObj = this.workGroupService.GetById(Convert.ToInt32(Request.QueryString["docId"]));

                if (Session["EditingId"] != null)
                {
                    var contentId = Convert.ToInt32(Session["EditingId"]);
                    var contentObj = this.workpackageContentService.GetById(contentId);
                    if (contentObj != null)
                    {
                        contentObj.ContentInfo = this.txtContent.Text;
                        contentObj.Deadline = this.txtDeadline.SelectedDate;
                        contentObj.Note = this.txtNote.Text;
                        contentObj.WorkpackageId = workpackageObj != null ? workpackageObj.ID : 0;
                        contentObj.WorkPackageName = workpackageObj != null ? workpackageObj.Name : string.Empty;

                        this.workpackageContentService.Update(contentObj);
                    }

                    Session.Remove("EditingId");
                }
                else
                {
                    var workpackageContent = new WorkpackageContent()
                    {
                        ContentInfo = this.txtContent.Text,
                        Deadline = this.txtDeadline.SelectedDate < workpackageObj.Deadline ? this.txtDeadline.SelectedDate: workpackageObj.Deadline,
                        Note = this.txtNote.Text,
                        WorkpackageId = workpackageObj != null ? workpackageObj.ID : 0,
                        WorkPackageName = workpackageObj != null ? workpackageObj.Name : string.Empty
                    };
                    var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                    var listcontent = this.workpackageContentService.GetAllByWorkpackage(workpackageId).ToList();
                    workpackageContent.Ordinal=listcontent.Count;
                    this.workpackageContentService.Insert(workpackageContent);
                }
            }

            this.ClearData();
            this.grdDocument.Rebind();
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());

            this.workpackageContentService.Delete(docId);
            this.grdDocument.Rebind();
        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var workpackageId = Convert.ToInt32(Request.QueryString["docId"]);
                var listcontent = this.workpackageContentService.GetAllByWorkpackage(workpackageId).ToList();
                int count=listcontent.Where(t=>!string.IsNullOrEmpty( t.Ordinal.ToString())).ToList().Count;
                if (count == 0)
                {
                    int i = 0;
                    foreach(var item in listcontent){
                        item.Ordinal = i;
                        this.workpackageContentService.Update(item);
                        i++;
                    }
                    listcontent = this.workpackageContentService.GetAllByWorkpackage(workpackageId);
                }

                this.grdDocument.DataSource = listcontent.OrderBy(t => t.Ordinal).ToList();
            }
            else
            {
                this.grdDocument.DataSource = new List<AttachFilesProject>();
            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            throw new NotImplementedException();
        }

        protected void rbtnDefaultDoc_CheckedChanged(object sender, EventArgs e)
        {
            //((GridItem)((RadioButton)sender).Parent.Parent).Selected = ((RadioButton)sender).Checked;

            //var item = ((RadioButton)sender).Parent.Parent as GridDataItem;
            //var attachFileId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            //var attachFileObj = this.attachFileService.GetById(attachFileId);
            //if (attachFileObj != null)
            //{
            //    var attachFiles = this.attachFileService.GetAllByDocId(attachFileObj.DocumentId.GetValueOrDefault());
            //    foreach (var attachFile in attachFiles)
            //    {
            //        attachFile.IsDefault = attachFile.ID == attachFileId;
            //        this.attachFileService.Update(attachFile);
            //    }
            //}
        }

        protected void grdDocument_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            if (e.CommandName == "EditCmd")
            {
                var contentId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
                var contentObj = this.workpackageContentService.GetById(contentId);
                if (contentObj != null)
                {
                    Session.Add("EditingId", contentObj.ID);
                    this.txtContent.Text = contentObj.ContentInfo;
                    this.txtDeadline.SelectedDate = contentObj.Deadline;
                    this.txtNote.Text = contentObj.Note;
                }
            }
        }

        private void ClearData()
        {
            this.txtDeadline.SelectedDate = null;
            this.txtContent.Text = string.Empty;
            this.txtNote.Text = string.Empty;

            Session.Remove("EditingId");
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            this.ClearData();
        }

        protected void Upbtn_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton upbtn = (ImageButton)sender;
            GridDataItem item = (GridDataItem)upbtn.NamingContainer;
            int index = item.ItemIndex;
            if (index > 0)
            {
                var key1 = grdDocument.MasterTableView.DataKeyValues[index]["ID"];
                var key2 = grdDocument.MasterTableView.DataKeyValues[index - 1]["ID"];

                var movefItem = this.workpackageContentService.GetById(Convert.ToInt32(key1));
                var beforeItem = this.workpackageContentService.GetById(Convert.ToInt32(key2));
                movefItem.Ordinal--;
                beforeItem.Ordinal++;

                this.workpackageContentService.Update(movefItem);
                this.workpackageContentService.Update(beforeItem);
                this.grdDocument.Rebind();
            }
        }

        protected void Downbtn_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton upbtn = (ImageButton)sender;
            GridDataItem item = (GridDataItem)upbtn.NamingContainer;
            int index = item.ItemIndex;
            var rowcout = this.grdDocument.Items.Count;
            if (index <rowcout-1)
            {
                var key1 = grdDocument.MasterTableView.DataKeyValues[index]["ID"];
                var key2 = grdDocument.MasterTableView.DataKeyValues[index + 1]["ID"];

                var movefItem = this.workpackageContentService.GetById(Convert.ToInt32(key1));
                var afterItem = this.workpackageContentService.GetById(Convert.ToInt32(key2));
                movefItem.Ordinal++;
                afterItem.Ordinal--;

                this.workpackageContentService.Update(movefItem);
                this.workpackageContentService.Update(afterItem);
                this.grdDocument.Rebind();
            }
        }
    }
}
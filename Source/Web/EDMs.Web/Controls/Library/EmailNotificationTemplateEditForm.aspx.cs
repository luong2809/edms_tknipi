﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Words.Lists;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web.Controls.Library
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class EmailNotificationTemplateEditForm : Page
    {
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly EmailNotificationTemplateService emailNotificationTemplateService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private List<EmailWhen> emailWhen = new List<EmailWhen>()
        {
            new EmailWhen() {ID = 1, Name = "Create new workpackage"},
            new EmailWhen() {ID = 2, Name = "Update workpackage"},
            new EmailWhen() {ID = 3, Name = "Delete workpackage"},
            new EmailWhen() {ID = 4, Name = "Create new document"},
            new EmailWhen() {ID = 5, Name = "Update document"},
            new EmailWhen() {ID = 6, Name = "Delete document"},
            new EmailWhen() {ID = 7, Name = "Deadline Workpackage"},
            new EmailWhen() {ID = 8, Name = "Deadline Document"},
            new EmailWhen() {ID = 9, Name = "Upload IDC Document"},
        };
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public EmailNotificationTemplateEditForm()
        {
            this.userService = new UserService();
            this.emailNotificationTemplateService = new EmailNotificationTemplateService();
        }

        

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["doctypeId"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = this.emailNotificationTemplateService.GetById(Convert.ToInt32(this.Request.QueryString["doctypeId"]));
                    if (obj != null)
                    {

                        this.txtName.Text = obj.Name;
                        this.txtContent.Content = obj.Contents;
                        this.txtSubject.Text = obj.Subject;
                        this.ddlType.SelectedValue = obj.TypeId.ToString();
                        this.cbActive.Checked = obj.IsActive.GetValueOrDefault();

                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (obj.UpdatedBy != null && obj.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["doctypeId"]))
                {
                    var doctypeId = Convert.ToInt32(this.Request.QueryString["doctypeId"]);
                    var obj = this.emailNotificationTemplateService.GetById(doctypeId);
                    if (obj != null)
                    {
                        obj.Name = this.txtName.Text.Trim();
                        obj.Subject = this.txtSubject.Text.Trim();
                        obj.Contents = this.txtContent.Content;
                        obj.TypeId = Convert.ToInt32(this.ddlType.SelectedValue);
                        obj.TypeName = this.ddlType.SelectedItem.Text;
                        obj.IsActive = this.cbActive.Checked;
                        obj.UpdatedBy = UserSession.Current.User.Id;
                        obj.UpdatedDate = DateTime.Now;

                        this.emailNotificationTemplateService.Update(obj);
                    }
                }
                else
                {
                    var obj = new EmailNotificationTemplate()
                    {
                        Name = this.txtName.Text.Trim(),
                        Subject = this.txtSubject.Text.Trim(),
                        Contents = this.txtContent.Content,
                        TypeId = Convert.ToInt32(this.ddlType.SelectedValue),                        
                        TypeName = this.ddlType.SelectedItem.Text,
                        IsActive = this.cbActive.Checked,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };

                    this.emailNotificationTemplateService.Insert(obj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Notification template name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        private void LoadComboData()
        {
            //var temp = this.emailNotificationTemplateService.GetAll().Select(t => t.TypeId).Distinct().ToList();
            //var type = this.emailWhen.Where(t => !temp.Contains(t.ID));

            this.ddlType.DataSource = this.emailWhen;
            this.ddlType.DataValueField = "ID";
            this.ddlType.DataTextField = "Name";
            this.ddlType.DataBind();
        }
    }

    public class EmailWhen
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using Aspose.Cells;
using System.Net;
using System.Net.Mail;
using System.Text;
using Aspose.Words.Tables;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using OfficeHelper.Utilities.Data;

namespace EDMs.Web
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;


    using EDMs.Business.Services.Library;
    using EDMs.Web.Utilities.Sessions;
    using Aspose.Words;
    using Telerik.Web.UI;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class ManhoursProccessReport : Page
    {
        /// <summary>
        /// The scope project service.
        /// </summary>

        private readonly WorkGroupService workGroupService = new WorkGroupService();
        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();
        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();
        private readonly ProcessActualService processActualService = new ProcessActualService();
        private readonly ProcessPlanedService processPlanedService = new ProcessPlanedService();
        private readonly RoleService roleService = new RoleService();
        private readonly UserService userService = new UserService();
        private readonly ManhourService manhourService = new ManhourService();
        private readonly ProcessManhourActualService processmanhouractualservice = new ProcessManhourActualService();
        private readonly ProcessManhourPlanedService processmanhourplanedservice = new ProcessManhourPlanedService();
        private readonly CurrentManpowerService currentmanpowerService = new CurrentManpowerService();
        private readonly MilestoneService milestoneService = new MilestoneService();

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {
                this.InitData();
                this.LoadProcessReport(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, 0);
            }
        }

        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"Images/project.png";
        }

        protected void rtvWorkgroup_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.LoadProcessReport(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, Convert.ToInt32(e.Node.Value));
        }

        protected void ddlProject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //var projectId = this.ddlProject.SelectedItem != null
            //       ? Convert.ToInt32(this.ddlProject.SelectedValue)
            //       : 0;
            //var rolelist = this.roleService.GetAll(false).Where(t => t.IsWorking.GetValueOrDefault()).Select(t => (int)t.Id).ToList();
            //var listmanhour = projectId != 0 ? this.processmanhourplanedservice.GetAllCurrent(projectId, DateTime.Now.Date).Select(t => t.WorkgroupId) : this.processmanhourplanedservice.GetAllCurrent(rolelist, DateTime.Now.Date).Select(t => t.WorkgroupId);
            //var wp = this.workGroupService.GetAll().Where(t => listmanhour.Contains(t.ID));
            //this.rtvWorkgroup.DataSource = wp;
            //this.rtvWorkgroup.DataTextField = "Name";
            //this.rtvWorkgroup.DataValueField = "ID";
            //this.rtvWorkgroup.DataFieldID = "ID";
            //this.rtvWorkgroup.DataBind();
            //this.LoadProcessReport(Convert.ToInt32(this.ddlProject.SelectedValue), 0);
            var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
            var currentDepartment = UserSession.Current.User.RoleId;
            var workPackageList = isSeeFull
                ? this.workGroupService.GetAll()
                : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault());

            if (UserSession.Current.IsEngineer)
            {
                var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
            }

            this.rtvWorkgroup.DataSource = workPackageList.Where(
                                                t =>
                                                    t.ProjectId ==
                                                    (!string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                                                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                                        : 0));
            this.rtvWorkgroup.DataTextField = "Name";
            this.rtvWorkgroup.DataValueField = "ID";
            this.rtvWorkgroup.DataFieldID = "ID";
            this.rtvWorkgroup.DataBind();

            this.LoadProcessReport(Convert.ToInt32(this.ddlProject.SelectedValue), 0);
        }


        public DateTime GetMondayOfWeek(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        public DateTime GetSaturdayOfWeek(DateTime date)
        {
            return GetMondayOfWeek(date).AddDays(4);
        }
        protected void rtvWorkgroup_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/package.png";
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            var isSeeFull = UserSession.Current.IsAdmin
                        || UserSession.Current.IsDC
                        || UserSession.Current.IsSuperViewer;
            var month = DateTime.Now.Month;
            var year = DateTime.Now.Year;
            var nextMonth = DateTime.Now.AddMonths(1).Month;
            var nextYear = DateTime.Now.AddMonths(1).Year;

            if (e.Argument == "RefreshProgressReport")
            {
                this.rtvWorkgroup.UnselectAllNodes();
                this.LoadProcessReport(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, 0);
            }

            //else if (e.Argument == "PrintProgress")
            //{
            //    var DepartmentID = this.ddlProject.SelectedItem != null
            //        ? Convert.ToInt32(this.ddlProject.SelectedValue)
            //        : 0;
            //    var processReportList = new List<ProcessReport>();
            //    var role = this.roleService.GetByID(DepartmentID);

            //    var rolelist = this.roleService.GetAll(false).Where(t => t.IsWorking.GetValueOrDefault()).Select(t => (int)t.Id).ToList();
            //    var uservalue = "0$0$0".Split('$');
            //    var uservaluelist = new List<CurrentManpower>();
            //    var processPlanedList = new List<ProcessManhourPlaned>();

            //    var processActualList = new List<ProcessManhourActual>();
            //    if (DepartmentID == 0)
            //    {
            //        processPlanedList = this.processmanhourplanedservice.GetAllCurrent(rolelist, DateTime.Now.Date);

            //        processActualList = this.processmanhouractualservice.GetAllCurrent(rolelist, DateTime.Now.Date);
            //        uservaluelist = this.currentmanpowerService.GetDepartment(rolelist, DateTime.Now.Year);
            //    }
            //    else
            //    {
            //        processPlanedList = this.processmanhourplanedservice.GetAllCurrent(DepartmentID, DateTime.Now.Date);

            //        processActualList = this.processmanhouractualservice.GetAllCurrent(DepartmentID, DateTime.Now.Date);
            //        uservalue = this.currentmanpowerService.GetDepartment(DepartmentID, DateTime.Now.Year).Values.Split('$');
            //    }

            //    if (processPlanedList != null)
            //    {
            //        var dateList = new List<DateTime>();

            //        var filePath = Server.MapPath("Exports") + @"\";
            //        var workbook = new Workbook();


            //        workbook.Open(filePath + @"Template\ProgressTemplate_v2.xls");

            //        var wsProgress = workbook.Worksheets[0];
            //        wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR (" + (DepartmentID != 0 ? role.FullName : "All") + ") Department.");
            //        wsProgress.Cells["C2"].PutValue("Mahour PROGRESS (CUT OFF: " + DateTime.Now.ToString("dd/MM/yyyy") + ")");

            //        var daystart = new DateTime(DateTime.Now.Date.Year, 01, 01);
            //        var dayend = new DateTime(DateTime.Now.Date.Year, 12, 30);
            //        var count = 3;
            //        var totalPlan = new List<double>();
            //        var totalActual = new List<double>();

            //        var differenceMonth = (((dayend.Year - daystart.Year) * 12) + dayend.Month - daystart.Month) + 1;
            //        for (int i = 0; i < differenceMonth; i++)
            //        {
            //            wsProgress.Cells[2, 3 + i].PutValue(daystart.AddMonths(i).ToString("MM/yyyy"));
            //            wsProgress.Cells[3, 3 + i].PutValue(i);
            //            totalPlan.Add(0.0);
            //            totalActual.Add(0.0);
            //        }


            //        var wgCount = processPlanedList.Count;
            //        wsProgress.Cells.InsertRows(8, wgCount * 3);
            //        wsProgress.Cells.Merge(7 + (wgCount * 3), 1, 3, 1);

            //        wsProgress.Cells["B" + (5 + (wgCount * 3))].PutValue("Overall Project");
            //        wsProgress.Cells["C" + (5 + (wgCount * 3))].PutValue("Planed (%)");
            //        wsProgress.Cells["C" + (5 + (wgCount * 3) + 1)].PutValue("Actual (%)");
            //        wsProgress.Cells["C" + (5 + (wgCount * 3) + 2)].PutValue("+/-");

            //        for (int i = 0; i < processPlanedList.Count; i++)
            //        {
            //            var workgroup = this.workGroupService.GetById(processPlanedList[i].WorkgroupId.GetValueOrDefault());

            //            wsProgress.Cells["B" + (8 + (i * 3))].PutValue(workgroup.Name);
            //            wsProgress.Cells["C" + (8 + (i * 3))].PutValue("Planed (%)");
            //            wsProgress.Cells["C" + (8 + (i * 3) + 1)].PutValue("Actual (%)");
            //            wsProgress.Cells["C" + (8 + (i * 3) + 2)].PutValue("+/-");

            //            wsProgress.Cells.Merge(7 + (i * 3), 1, 3, 1);

            //            var progressPlaned = processPlanedList[i];
            //            var progressActual = processActualList.FirstOrDefault(t => t.WorkgroupId == progressPlaned.WorkgroupId);

            //            if (progressPlaned != null && progressActual != null)
            //            {
            //                var planedList = progressPlaned.Planed.Split('$').Select(Convert.ToDouble).ToList();
            //                var actualList = progressActual.Actual.Split('$').Select(Convert.ToDouble).ToList();
            //                var currentMonth = 0;
            //                for (var j = progressPlaned.StartDate.GetValueOrDefault(); j <= progressPlaned.EndDate.GetValueOrDefault(); j = j.AddMonths(1))
            //                {

            //                    var planed = Math.Round(planedList[currentMonth] / (8 * 22), 2);
            //                    var actual = 0.0;

            //                    if (j.Month >= daystart.Month && j.Month <= dayend.Month && j.Year >= daystart.Year && j.Year <= dayend.Year)
            //                    {
            //                        var k = 0;
            //                        for (var l = daystart;
            //                          l <= dayend;
            //                          l = l.AddMonths(1))
            //                        {
            //                            if (j.Month == l.Month && j.Year == l.Year)
            //                            {
            //                                wsProgress.Cells[7 + (i * 3), k + 3].PutValue(planed);
            //                                totalPlan[k] += planed;
            //                                if (currentMonth < actualList.Count())
            //                                {
            //                                    actual = Math.Round(actualList[currentMonth] / (8 * 22), 0);
            //                                    totalActual[k] += actual;
            //                                    wsProgress.Cells[8 + (i * 3), k + 3].PutValue(actual);
            //                                    wsProgress.Cells[9 + (i * 3), k + 3].PutValue(actual - planed);
            //                                }
            //                            }
            //                            k++;
            //                        }
            //                    }
            //                    currentMonth++;
            //                }
            //            }
            //        }
            //        for (int i = 0; i < differenceMonth; i++)
            //        {
            //            wsProgress.Cells[4 + ((wgCount + 1) * 3), i + 3].PutValue(Math.Round(totalPlan[i], 0));
            //            wsProgress.Cells[5 + ((wgCount + 1) * 3), i + 3].PutValue(Math.Round(totalActual[i], 0));
            //            wsProgress.Cells[6 + ((wgCount + 1) * 3), i + 3].PutValue(Math.Round(totalActual[i] - totalPlan[i], 0));
            //            wsProgress.Cells[7 + ((wgCount + 1) * 3), i + 3].PutValue(DepartmentID != 0 ? Convert.ToInt32(uservalue[i]) : this.sumuser(uservaluelist, i));
            //        }
            //        wsProgress.Cells[7 + ((wgCount + 1) * 3), 1].PutValue("Current workforce");
            //        wsProgress.Cells["A1"].PutValue(0);
            //        wsProgress.Cells["A2"].PutValue(wgCount);
            //        wsProgress.Cells["A3"].PutValue(differenceMonth);

            //        //wsProgress.Cells.Merge(0, 2, 1, count - 2);
            //        //   wsProgress.Cells.Merge(1, 2, 1, count - 2);

            //        // Save and export file
            //        var filename = (this.ddlProject.SelectedItem != null
            //                ? this.ddlProject.SelectedItem.Text + "_"
            //                : string.Empty) + "Progress Manhors report - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
            //        workbook.Save(filePath + filename);
            //        this.DownloadByWriteByte(filePath + filename, filename, true);

            //    }
            //}
            else if (e.Argument == "PrintProgress")
            {
                var projectId = this.ddlProject.SelectedItem != null
                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                    : 0;
                var processReportList = new List<ProcessReport>();

                var projectObj = this.scopeProjectService.GetById(projectId);
                if (projectObj != null)
                {

                    var listWorkgroupInPermission = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);

                    var dateList = new List<DateTime>();

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();

                    var processPlanedList = this.processPlanedService.GetAllByProject(projectId, 0);
                    //if (processPlanedList.Count == 0)
                    //{
                    //    workbook.Open(filePath + @"Template\ProgressTemplate.xls");

                    //    var wsProgress = workbook.Worksheets[0];
                    //    wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR " + projectObj.Description);
                    //    wsProgress.Cells["C2"].PutValue("PROJECT PROGRESS (CUT OFF: " + DateTime.Now.ToString("dd/MM/yyyy") + ")");

                    //    var workgroupCount = 0;
                    //    var startDate = projectObj.StartDate.GetValueOrDefault();
                    //    while (startDate.DayOfWeek != DayOfWeek.Monday)
                    //    {
                    //        startDate = startDate.AddDays(1);
                    //    }

                    //    var currentMonth = 0;
                    //    var countMerge = 0;
                    //    var count = 3;
                    //    var countNumberCurrentActual = 0;

                    //    var wpIDList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID).Select(t => t.ID).ToList();
                    //    var milestoneList = this.milestoneService.GetAllByWorkpackage(wpIDList);
                    //    var minDate = milestoneList.Min(t => t.MilestoneDate).GetValueOrDefault();
                    //    var maxDate = milestoneList.Max(t => t.MilestoneDate).GetValueOrDefault();
                    //    var differenceMonth = ((maxDate.Year - minDate.Year) * 12) + maxDate.Month - minDate.Month;
                    //    for (int i = 0; i < differenceMonth; i++)
                    //    {
                    //        wsProgress.Cells[2, 3 + i].PutValue(minDate.AddMonths(i).ToString("MM/yyyy"));
                    //    }

                    //    if (listWorkgroupInPermission.Count > 0)
                    //    {
                    //        var wgCount = listWorkgroupInPermission.Count;
                    //        wsProgress.Cells.InsertRows(5, wgCount * 3);
                    //        for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                    //        {
                    //            var workgroup = listWorkgroupInPermission[i];

                    //            wsProgress.Cells["B" + (5 + (i * 3))].PutValue(workgroup.Name);
                    //            wsProgress.Cells["C" + (5 + (i * 3))].PutValue("Planed (%)");
                    //            wsProgress.Cells["C" + (5 + (i * 3) + 1)].PutValue("Actual (%)");
                    //            wsProgress.Cells["C" + (5 + (i * 3) + 2)].PutValue("+/-");

                    //            wsProgress.Cells.Merge(4 + (i * 3), 1, 3, 1);

                    //            for (int j = 0; j < differenceMonth; j++)
                    //            {
                    //                var milstoneOfMonth = milestoneList
                    //           .Where(t => t.MilestoneDate.GetValueOrDefault().Month == minDate.AddMonths(j).Month
                    //                       && t.MilestoneDate.GetValueOrDefault().Year == minDate.AddMonths(j).Year
                    //                       && t.WorkpackageId == workgroup.ID).ToList();

                    //                var progressPlaned = ((milstoneOfMonth.Average(t => t.PlanTotal).GetValueOrDefault() *
                    //                                       workgroup.Weight.GetValueOrDefault()) / 100) / 100;

                    //                var progressActual = ((milstoneOfMonth.Average(t => t.RealTotal).GetValueOrDefault() *
                    //                                       workgroup.Weight.GetValueOrDefault()) / 100) / 100;

                    //                wsProgress.Cells[4 + (i * 3), j + 3].PutValue(progressPlaned);
                    //                wsProgress.Cells[5 + (i * 3), j + 3].PutValue(progressActual);
                    //                wsProgress.Cells[6 + (i * 3), j + 3].PutValue(progressActual - progressPlaned);
                    //            }
                    //        }

                    //        wsProgress.Cells.Merge(7 + (wgCount * 3), 1, 3, 1);

                    //        wsProgress.Cells["B" + (5 + (wgCount * 3))].PutValue("Overall Project");
                    //        wsProgress.Cells["C" + (5 + (wgCount * 3))].PutValue("Planed (%)");
                    //        wsProgress.Cells["C" + (5 + (wgCount * 3) + 1)].PutValue("Actual (%)");
                    //        wsProgress.Cells["C" + (5 + (wgCount * 3) + 2)].PutValue("+/-");

                    //        for (int i = 0; i < differenceMonth; i++)
                    //        {
                    //            var totalPlan = 0.0;
                    //            var totalActual = 0.0;
                    //            var milstoneOfMonth = milestoneList
                    //                    .Where(t => t.MilestoneDate.GetValueOrDefault().Month == minDate.AddMonths(i).Month
                    //                                && t.MilestoneDate.GetValueOrDefault().Year == minDate.AddMonths(i).Year).ToList();

                    //            foreach (var milestoneOfWP in milstoneOfMonth.GroupBy(t => t.WorkpackageId))
                    //            {
                    //                var wpObj = this.workGroupService.GetById(milestoneOfWP.Key.GetValueOrDefault());
                    //                if (wpObj != null)
                    //                {
                    //                    totalPlan += (milestoneOfWP.ToList().Average(t => t.PlanTotal).GetValueOrDefault() *
                    //                                  wpObj.Weight.GetValueOrDefault()) / 100 / 100;

                    //                    totalActual += (milestoneOfWP.ToList().Average(t => t.RealTotal).GetValueOrDefault() *
                    //                         wpObj.Weight.GetValueOrDefault()) / 100 / 100;
                    //                }
                    //            }

                    //            wsProgress.Cells[4 + (wgCount * 3), i + 3].PutValue(totalPlan);
                    //            wsProgress.Cells[5 + (wgCount * 3), i + 3].PutValue(totalActual);
                    //            wsProgress.Cells[6 + (wgCount * 3), i + 3].PutValue(totalActual - totalPlan);
                    //        }

                    //        wsProgress.Cells["A1"].PutValue(0);
                    //        wsProgress.Cells["A2"].PutValue(wgCount);
                    //        wsProgress.Cells["A3"].PutValue(differenceMonth);

                    //        wsProgress.Cells.Merge(0, 2, 1, count - 2);
                    //        wsProgress.Cells.Merge(1, 2, 1, count - 2);

                    //    }
                    //}
                    //else if (projectObj.StartDate != null && projectObj.Deadline != null)
                        if (projectObj.StartDate != null && projectObj.Deadline != null)
                        {
                        var totalManhourPJ = 0.0;
                        var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectId);
                        totalManhourPJ = wpList.Sum(t => t.TotalManHours.GetValueOrDefault());

                        var workgroupCount = 0;
                        var startDate = GetSaturdayOfWeek(projectObj.StartDate.GetValueOrDefault());
                        //while (startDate.DayOfWeek != DayOfWeek.Monday)
                        //{
                        //    startDate = startDate.AddDays(1);
                        //}
                        if (projectObj.WeeklyProgress.GetValueOrDefault())
                        {
                            workbook.Open(filePath + @"Template\ProgressManhourTemplate_v1.xls");

                            var wsProgress = workbook.Worksheets[0];
                            wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR " + projectObj.Description);
                            wsProgress.Cells["C2"].PutValue("PROJECT PROGRESS (CUT OFF: " + DateTime.Now.ToString("dd/MM/yyyy") + ")");
                            var currentMonth = 0;
                            var countMerge = 0;
                            var count = 3;
                            var countNumberCurrentActual = 0;
                            for (var j = startDate;
                                    j <= projectObj.Deadline.GetValueOrDefault();
                                    j = j.AddDays(7))
                            {
                                var processReport = new ProcessReport();
                                processReport.WeekDate = j;
                                processReportList.Add(processReport);

                                if (DateTime.Now > j)
                                {
                                    countNumberCurrentActual += 1;
                                }

                                if (currentMonth != j.AddDays(7).Month)
                                {
                                    wsProgress.Cells[2, count].PutValue(j.AddDays(7));
                                    currentMonth = j.AddDays(7).Month;

                                    if (countMerge != 0)
                                    {
                                        wsProgress.Cells.Merge(2, count - countMerge, 1, countMerge);
                                    }

                                    countMerge = 1;
                                }
                                else
                                {
                                    countMerge += 1;
                                }

                                wsProgress.Cells[3, count].PutValue(count - 2);
                                wsProgress.Cells[4, count].PutValue(j);
                                //wsProgress.Cells[5, count].PutValue(j.AddDays(5));

                                count += 1;
                            }

                            if (listWorkgroupInPermission.Count > 0)
                            {
                                var wgCount = listWorkgroupInPermission.Count;
                                wsProgress.Cells.InsertRows(8, wgCount * 3);
                                for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                                {
                                    var workgroup = listWorkgroupInPermission[i];

                                    wsProgress.Cells["B" + (8 + (i * 3))].PutValue(workgroup.Name);
                                    wsProgress.Cells["C" + (8 + (i * 3))].PutValue("Planed");
                                    wsProgress.Cells["C" + (8 + (i * 3) + 1)].PutValue("Actual");
                                    wsProgress.Cells["C" + (8 + (i * 3) + 2)].PutValue("+/-");

                                    wsProgress.Cells.Merge(7 + (i * 3), 1, 3, 1);

                                    var progressPlaned = this.processPlanedService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                                    var progressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);

                                    if (progressPlaned != null && progressActual != null)
                                    {
                                        var planedList = progressPlaned.Planed.Split('$');
                                        var actualList = progressActual.Actual.Split('$');
                                        for (int j = 0; j < planedList.Count(); j++)
                                        {
                                            var planed = !string.IsNullOrEmpty(planedList[j]) ? Convert.ToDouble(planedList[j]) : 0;
                                            var actual = 0.0;

                                            wsProgress.Cells[7 + (i * 3), j + 3].PutValue(Math.Round((planed * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0));
                                            if (j < actualList.Count() && j <= countNumberCurrentActual)
                                            {
                                                actual = !string.IsNullOrEmpty(actualList[j]) ? Convert.ToDouble(actualList[j]) : 0;
                                                if (actual != 0.0)
                                                {
                                                    wsProgress.Cells[8 + (i * 3), j + 3].PutValue(Math.Round((actual * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0));
                                                    wsProgress.Cells[9 + (i * 3), j + 3].PutValue(Math.Round((actual * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0) - Math.Round((planed * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0));
                                                }
                                            }

                                            if (j < processReportList.Count)
                                            {
                                                processReportList[j].Planed += Math.Round((planed * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0);
                                            }

                                            if (j < actualList.Count() && j < processReportList.Count)
                                            {
                                                processReportList[j].Actual += Math.Round((actual * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0);
                                            }
                                        }
                                    }
                                }

                                wsProgress.Cells.Merge(7 + (wgCount * 3), 1, 3, 1);

                                wsProgress.Cells["B" + (8 + (wgCount * 3))].PutValue("Overall Project");
                                wsProgress.Cells["C" + (8 + (wgCount * 3))].PutValue("Planed");
                                wsProgress.Cells["C" + (8 + (wgCount * 3) + 1)].PutValue("Actual");
                                wsProgress.Cells["C" + (8 + (wgCount * 3) + 2)].PutValue("+/-");

                                for (int i = 0; i < processReportList.Count; i++)
                                {
                                    wsProgress.Cells[7 + (wgCount * 3), i + 3].PutValue(processReportList[i].Planed);
                                    if (processReportList[i].Actual != 0.0)
                                    {
                                        wsProgress.Cells[8 + (wgCount * 3), i + 3].PutValue(processReportList[i].Actual);
                                        if (i < countNumberCurrentActual)
                                        {
                                            wsProgress.Cells[9 + (wgCount * 3), i + 3].PutValue(processReportList[i].Actual - processReportList[i].Planed);
                                        }
                                    }
                                }

                                wsProgress.Cells["A1"].PutValue(0);
                                wsProgress.Cells["A2"].PutValue(wgCount);
                                wsProgress.Cells["A3"].PutValue(processReportList.Count);

                                wsProgress.Cells.Merge(0, 2, 1, count - 2);
                                wsProgress.Cells.Merge(1, 2, 1, count - 2);
                            }
                        }
                        else
                        {
                            workbook.Open(filePath + @"Template\ProgressManhourTemplateMonth_v1.xls");

                            var wsProgress = workbook.Worksheets[0];
                            wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR " + projectObj.Description);
                            wsProgress.Cells["C2"].PutValue("PROJECT PROGRESS (CUT OFF: " + DateTime.Now.ToString("dd/MM/yyyy") + ")");
                            //var currentMonth = 0;
                            //var countMerge = 0;
                            var count = 3;
                            var countNumberCurrentActual = 0;
                            for (var j = startDate;
                                    j <= projectObj.Deadline.GetValueOrDefault();
                                    j = j.AddMonths(1))
                            {
                                var processReport = new ProcessReport();
                                processReport.STRWeekDate = j.ToString("MM/yyyy");
                                processReportList.Add(processReport);

                                if (DateTime.Now > j)
                                {
                                    countNumberCurrentActual += 1;
                                }
                                wsProgress.Cells[2, count].PutValue(j);

                                wsProgress.Cells[3, count].PutValue(count - 2);
                                wsProgress.Cells[4, count].PutValue(j);

                                count += 1;
                            }

                            if (listWorkgroupInPermission.Count > 0)
                            {
                                var wgCount = listWorkgroupInPermission.Count;
                                wsProgress.Cells.InsertRows(8, wgCount * 3);
                                for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                                {
                                    var workgroup = listWorkgroupInPermission[i];

                                    wsProgress.Cells["B" + (8 + (i * 3))].PutValue(workgroup.Name);
                                    wsProgress.Cells["C" + (8 + (i * 3))].PutValue("Planed");
                                    wsProgress.Cells["C" + (8 + (i * 3) + 1)].PutValue("Actual");
                                    wsProgress.Cells["C" + (8 + (i * 3) + 2)].PutValue("+/-");

                                    wsProgress.Cells.Merge(7 + (i * 3), 1, 3, 1);

                                    var progressPlaned = this.processPlanedService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                                    var progressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);

                                    if (progressPlaned != null && progressActual != null)
                                    {
                                        var planedList = progressPlaned.PlanedMonth.Split('$');
                                        var actualList = progressActual.ActualMonth.Split('$');
                                        for (int j = 0; j < planedList.Count(); j++)
                                        {
                                            var planed = !string.IsNullOrEmpty(planedList[j]) ? Convert.ToDouble(planedList[j]) : 0;
                                            var actual = 0.0;

                                            wsProgress.Cells[7 + (i * 3), j + 3].PutValue(Math.Round((planed * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0));
                                            if (j < actualList.Count() && j <= countNumberCurrentActual)
                                            {
                                                actual = !string.IsNullOrEmpty(actualList[j]) ? Convert.ToDouble(actualList[j]) : 0;
                                                if (actual != 0.0)
                                                {
                                                    wsProgress.Cells[8 + (i * 3), j + 3].PutValue(Math.Round((actual * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0));
                                                    wsProgress.Cells[9 + (i * 3), j + 3].PutValue(Math.Round((actual * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0) - Math.Round((planed * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0));
                                                }
                                            }

                                            if (j < processReportList.Count)
                                            {
                                                processReportList[j].Planed += Math.Round((planed * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0);
                                            }

                                            if (j < actualList.Count() && j < processReportList.Count)
                                            {
                                                processReportList[j].Actual += Math.Round((actual * workgroup.TotalManHours.GetValueOrDefault()) / 100, 0);
                                            }
                                        }
                                    }
                                }

                                wsProgress.Cells.Merge(7 + (wgCount * 3), 1, 3, 1);

                                wsProgress.Cells["B" + (8 + (wgCount * 3))].PutValue("Overall Project");
                                wsProgress.Cells["C" + (8 + (wgCount * 3))].PutValue("Planed");
                                wsProgress.Cells["C" + (8 + (wgCount * 3) + 1)].PutValue("Actual");
                                wsProgress.Cells["C" + (8 + (wgCount * 3) + 2)].PutValue("+/-");

                                for (int i = 0; i < processReportList.Count; i++)
                                {
                                    wsProgress.Cells[7 + (wgCount * 3), i + 3].PutValue(processReportList[i].Planed);
                                    if (processReportList[i].Actual != 0.0)
                                    {
                                        wsProgress.Cells[8 + (wgCount * 3), i + 3].PutValue(processReportList[i].Actual);
                                        if (i < countNumberCurrentActual)
                                        {
                                            wsProgress.Cells[9 + (wgCount * 3), i + 3].PutValue(processReportList[i].Actual - processReportList[i].Planed);
                                        }
                                    }
                                }

                                wsProgress.Cells["A1"].PutValue(0);
                                wsProgress.Cells["A2"].PutValue(wgCount);
                                wsProgress.Cells["A3"].PutValue(processReportList.Count);

                                //wsProgress.Cells.Merge(0, 2, 1, count - 2);
                                //wsProgress.Cells.Merge(1, 2, 1, count - 2);
                            }
                        }
                    }

                    // Save and export file
                    var filename = (this.ddlProject.SelectedItem != null
                            ? Utility.RemoveSpecialCharacter(this.ddlProject.SelectedItem.Text) + "_"
                            : string.Empty) + "Manhour Progress report - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument == "GetLatestData")
            {
                var listDate = new List<DateTime>();
                var DepartmentID = this.ddlProject.SelectedItem != null
                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                    : 0;
                var listmanhour = this.processmanhourplanedservice.GetAllCurrent(DepartmentID, DateTime.Now.Date);
                if (listmanhour != null)
                {
                    foreach (var workgroup in listmanhour)
                    {

                        var workpackageObj = this.workGroupService.GetById(workgroup.WorkgroupId.GetValueOrDefault());
                        if (workpackageObj != null && workpackageObj.StartDate != null && workpackageObj.Deadline != null)
                        {



                            var docList = this.documentPackageService.GetAllEMDRByWorkgroup(workpackageObj.ID)
                                                .Select(t => (int)t.ID)
                                                .ToList();
                            var docmanhourlist = this.manhourService.GetAllByDocumentCurrentMonth(docList, DateTime.Now.Date);
                            double complete = 0;
                            complete = docmanhourlist.Aggregate(complete, (current, t) => current + t.Total.GetValueOrDefault());

                            var existProgressActual = this.processmanhouractualservice.GetByDepartmentAndWorkgroup(DepartmentID, workpackageObj.ID);
                            if (existProgressActual != null)
                            {
                                //if (existProgressActual.Actual.Split('$').Count() >= count)
                                //{
                                var differenceMonth = ((workpackageObj.Deadline.GetValueOrDefault().Year - workpackageObj.StartDate.GetValueOrDefault().Year) * 12) + workpackageObj.Deadline.GetValueOrDefault().Month - workpackageObj.StartDate.GetValueOrDefault().Month + 1;

                                var count = 0;
                                var st = workpackageObj.StartDate.GetValueOrDefault();
                                for (int i = 0; i < differenceMonth; i++)
                                {
                                    count += 1;
                                    st = st.AddMonths(1);
                                    if (st.Month == DateTime.Now.Date.Month && st.Year == DateTime.Now.Date.Year)
                                    {
                                        break;
                                    }
                                }
                                if (st.Month == DateTime.Now.Date.Month && st.Year == DateTime.Now.Date.Year)
                                {
                                    var actualList = existProgressActual.Actual.Split('$');
                                    actualList[count] = Math.Round(complete, 2).ToString();
                                    var newActual = string.Empty;
                                    newActual = actualList.Aggregate(newActual, (current, t) => current + t + "$");

                                    newActual = newActual.Substring(0, newActual.Length - 1);
                                    existProgressActual.Actual = newActual;

                                    this.processmanhouractualservice.Update(existProgressActual);
                                }
                                // }
                            }
                        }
                    }
                    this.LoadProcessReport(DepartmentID, 0);
                }
            }
            #region hidden3
            //else if (e.Argument.Contains("ExportProgress"))
            //{
            //    var type = e.Argument.Split('_')[1];


            //    var projectId = this.ddlProject.SelectedItem != null
            //        ? Convert.ToInt32(this.ddlProject.SelectedValue)
            //        : 0;


            //    var projectObj = this.scopeProjectService.GetById(projectId);
            //    if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null && !UserSession.Current.IsEngineer)
            //    {
            //        var listWorkgroupInPermission = WorkpackageList.Where(t =>t.ProjectId == projectId).ToList();

            //        var dateList = new List<DateTime>();

            //        var filePath = Server.MapPath("Exports") + @"\";
            //        var workbook = new Workbook();
            //        if (type == "Planed")
            //        {
            //            workbook.Open(filePath + @"Template\ProgressPlanedTemplate.xls");
            //        }
            //        else
            //        {
            //            workbook.Open(filePath + @"Template\ProgressActualTemplate.xls");
            //        }
            //        var wsProgress = workbook.Worksheets[0];

            //        wsProgress.Cells["A7"].PutValue(type == "Planed" ? "Planed" : "Actual");
            //        wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR " + projectObj.Description);
            //        var workgroupCount = 0;
            //        var startDate = projectObj.StartDate.GetValueOrDefault();
            //        while (startDate.DayOfWeek != DayOfWeek.Monday)
            //        {
            //            startDate = startDate.AddDays(1);
            //        }

            //        var currentMonth = 0;
            //        var countMerge = 0;
            //        var count = 3;
            //        for (var j = startDate;
            //                j < projectObj.Deadline.GetValueOrDefault();
            //                j = j.AddDays(7))
            //        {
            //            if (currentMonth != j.AddDays(5).Month)
            //            {
            //                wsProgress.Cells[2, count].PutValue(j.AddDays(5));
            //                currentMonth = j.AddDays(5).Month;

            //                if (countMerge != 0)
            //                {
            //                    wsProgress.Cells.Merge(2, count - countMerge, 1, countMerge);
            //                }

            //                countMerge = 1;
            //            }
            //            else
            //            {
            //                countMerge += 1;
            //            }

            //            wsProgress.Cells[3, count].PutValue(count - 2);
            //            wsProgress.Cells[4, count].PutValue(j);
            //            wsProgress.Cells[5, count].PutValue(j.AddDays(5));

            //            count += 1;
            //        }

            //        if (listWorkgroupInPermission.Count > 0)
            //        {
            //            wsProgress.Cells.InsertRows(8, listWorkgroupInPermission.Count);
            //            for (int i = 0; i < listWorkgroupInPermission.Count; i++)
            //            {
            //                var workgroup = listWorkgroupInPermission[i];

            //                wsProgress.Cells["A" + (8 + i)].PutValue(workgroup.ID);
            //                wsProgress.Cells["B" + (8 + i)].PutValue(workgroup.Name);
            //                wsProgress.Cells["C" + (8 + i)].PutValue(type == "Planed" ? "Planed (%)" : "Actual (%)");

            //                if (type == "Planed")
            //                {
            //                    var progressPlaned = this.processPlanedService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
            //                    if (progressPlaned != null)
            //                    {
            //                        var planedList = progressPlaned.Planed.Split('$');
            //                        for (int j = 0; j < planedList.Count(); j++)
            //                        {
            //                            wsProgress.Cells[workgroupCount + 7, j + 3].PutValue(!string.IsNullOrEmpty(planedList[j]) ? Convert.ToDouble(planedList[j]) / 100 : 0);
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    var progressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
            //                    if (progressActual != null)
            //                    {
            //                        var actualList = progressActual.Actual.Split('$');
            //                        for (int j = 0; j < actualList.Count(); j++)
            //                        {
            //                            wsProgress.Cells[workgroupCount + 7, j + 3].PutValue(!string.IsNullOrEmpty(actualList[j]) ? Convert.ToDouble(actualList[j]) / 100 : 0);
            //                        }
            //                    }
            //                }

            //                workgroupCount += 1;
            //            }

            //            wsProgress.Cells.Merge(0, 2, 1, count - 2);
            //            wsProgress.Cells.Merge(1, 2, 1, count - 2);
            //        }

            //        // Save and export file
            //        var filename = (this.ddlProject.SelectedItem != null
            //                ? this.ddlProject.SelectedItem.Text + "$"
            //                : string.Empty)
            //                + (type == "Planed" ? "ProgressPlaned.xls" : "ProgressActual.xls");
            //        workbook.Save(filePath + filename);
            //        this.DownloadByWriteByte(filePath + filename, filename, true);
            //    }
            //}
            //#region Hidden1
            /*
            else if (e.Argument == "MonthReport")
            {
                var milestoneListOfMonth = isSeeFull
                    ? this.milestoneService.GetAllByMonth(month, year)
                    : this.milestoneService.GetAllByMonth(month, year, UserSession.Current.RoleId);
                //var milestoneListOfMonth = this.milestoneService.GetAllByMonth(month, year);
                if (milestoneListOfMonth != null && milestoneListOfMonth.Count > 0)
                {
                    var workpackageList = milestoneListOfMonth
                                        .Select(t => t.WorkpackageId)
                                        .Distinct()
                                        .Select(t => this.workGroupService.GetById(t.GetValueOrDefault()));

                    var wpPlannedList = workpackageList.Where(t => t.TypeId == 1).ToList();
                    var wpUnPlannedList = workpackageList.Where(t => t.TypeId == 2).ToList();
                    var wpOtherList = workpackageList.Where(t => t.TypeId == 0).ToList();
                    var wpCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("Text1", Type.GetType("System.String")),
                                new DataColumn("Plan", Type.GetType("System.String")),
                                new DataColumn("PlanTotal", Type.GetType("System.String")),
                                new DataColumn("EndDate", Type.GetType("System.String")),
                                new DataColumn("OutgoingNumber", Type.GetType("System.String")),
                                new DataColumn("Real", Type.GetType("System.String")),
                                new DataColumn("RealTotal", Type.GetType("System.String")),
                                new DataColumn("RealPerPlan", Type.GetType("System.String")),
                                new DataColumn("TotalRealPerTotalPlan", Type.GetType("System.String")),
                                new DataColumn("MilestoneDate", Type.GetType("System.String")),
                                new DataColumn("TotalManHours", Type.GetType("System.String")),
                                new DataColumn("UsedManHours", Type.GetType("System.String")),
                                new DataColumn("PerformingUser", Type.GetType("System.String")),
                                new DataColumn("Note", Type.GetType("System.String")),
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("DepartmentR", Type.GetType("System.String")),
                                new DataColumn("MonthR", Type.GetType("System.String")),
                                new DataColumn("Year", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var deparment = this.roleService.GetByID(UserSession.Current.RoleId);

                    var infoItem = reportInfo.NewRow();
                    infoItem["DepartmentR"] = deparment != null ? deparment.RussiaName : string.Empty;
                    infoItem["MonthR"] = Utility.MonthR[DateTime.Now.Month];
                    infoItem["Year"] = DateTime.Now.Year.ToString();
                    reportInfo.Rows.Add(infoItem);


                    var dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "1";
                    dataworkpackageItem["WorkPackageContent"] = "Плановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    foreach (var workpackage in wpPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData(workpackage, milestoneObj, ref dataTable, wpCount, "1.");
                        }
                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "2";
                    dataworkpackageItem["WorkPackageContent"] = "Внеплановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpUnPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData(workpackage, milestoneObj, ref dataTable, wpCount, "2.");
                        }

                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "3";
                    dataworkpackageItem["WorkPackageContent"] = "Другие работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpOtherList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData(workpackage, milestoneObj, ref dataTable, wpCount, "3.");
                        }

                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = "MonthReportTemplate.doc";
                    var strOutputFileName = "MonthReport_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);

                    // Apply style for rows have RealTotal == 100%
                    var doc = new Document(rootPath + WordPathExport + strOutputFileName);
                    var table = (Table)doc.GetChild(NodeType.Table, 2, true);

                    for (var i = 2; i < table.Rows.Count; i++)
                    {
                        if (table.Rows[i].Cells.Count > 12)
                        {
                            var temp = table.Rows[i].Cells[8].GetText();
                            if (temp.Contains("100%"))
                            {
                                foreach (Aspose.Words.Tables.Cell cell in table.Rows[i].Cells)
                                {
                                    cell.CellFormat.Shading.BackgroundPatternColor = Color.DarkGray;
                                }
                            }
                        }
                    }

                    for (var i = 2; i < table.Rows.Count; i++)
                    {
                        if (table.Rows[i].Cells.Count > 12)
                        {
                            var temp = table.Rows[i].Cells[0].GetText();
                            if (temp == "1\a" || temp == "2\a" || temp == "3\a")
                            {
                                foreach (Aspose.Words.Tables.Cell cell in table.Rows[i].Cells)
                                {
                                    // Get Runs in this cell.
                                    NodeCollection runs = cell.GetChildNodes(NodeType.Run, true);

                                    // Loop through all runs and make them bold.
                                    foreach (Run run in runs)
                                    {
                                        run.Font.Bold = true;
                                    }
                                }
                            }
                        }
                    }

                    doc.Save(rootPath + WordPathExport + strOutputFileName);

                    if (isSuccess)
                    {
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "MonthReport_" + DateTime.Now.ToString("MM-yyyy") + ".doc", true);
                    }

                }
            }
            else if (e.Argument == "NextMonthReport")
            {
                var milestoneListOfMonth = isSeeFull
                    ? this.milestoneService.GetAllByMonth(nextMonth, nextYear)
                    : this.milestoneService.GetAllByMonth(nextMonth, nextYear, UserSession.Current.RoleId);
                //var milestoneListOfMonth = this.milestoneService.GetAllByMonth(nextMonth, nextYear);
                if (milestoneListOfMonth != null && milestoneListOfMonth.Count > 0)
                {
                    var workpackageList = milestoneListOfMonth
                                        .Select(t => t.WorkpackageId)
                                        .Distinct()
                                        .Select(t => this.workGroupService.GetById(t.GetValueOrDefault()));

                    var wpPlannedList = workpackageList.Where(t => t.TypeId == 1).ToList();
                    var wpUnPlannedList = workpackageList.Where(t => t.TypeId == 2).ToList();
                    var wpOtherList = workpackageList.Where(t => t.TypeId == 0).ToList();
                    var wpCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("Text1", Type.GetType("System.String")),
                                new DataColumn("Plan", Type.GetType("System.String")),
                                new DataColumn("PlanTotal", Type.GetType("System.String")),
                                new DataColumn("MaxPlan", Type.GetType("System.String")),
                                new DataColumn("MilestoneDate", Type.GetType("System.String")),
                                new DataColumn("PerformingUser", Type.GetType("System.String")),
                                new DataColumn("Note", Type.GetType("System.String")),
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("DepartmentR", Type.GetType("System.String")),
                                new DataColumn("MonthR", Type.GetType("System.String")),
                                new DataColumn("Year", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var deparment = this.roleService.GetByID(UserSession.Current.RoleId);

                    var infoItem = reportInfo.NewRow();
                    infoItem["DepartmentR"] = deparment != null ? deparment.RussiaName : string.Empty;
                    infoItem["MonthR"] = Utility.MonthR[DateTime.Now.AddMonths(1).Month];
                    infoItem["Year"] = DateTime.Now.AddMonths(1).Year.ToString();
                    reportInfo.Rows.Add(infoItem);


                    var dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "1";
                    dataworkpackageItem["WorkPackageContent"] = "Плановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    foreach (var workpackage in wpPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData1(workpackage, milestoneObj, ref dataTable, wpCount, "1.");
                        }
                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "2";
                    dataworkpackageItem["WorkPackageContent"] = "Внеплановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpUnPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData1(workpackage, milestoneObj, ref dataTable, wpCount, "2.");
                        }

                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "3";
                    dataworkpackageItem["WorkPackageContent"] = "Другие работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpOtherList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData1(workpackage, milestoneObj, ref dataTable, wpCount, "3.");
                        }

                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = "NextMonthReportTemplate.doc";
                    var strOutputFileName = "NextMonthReport_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);

                    // Apply style for rows have RealTotal == 100%
                    var doc = new Aspose.Words.Document(rootPath + WordPathExport + strOutputFileName);
                    var table = (Aspose.Words.Tables.Table)doc.GetChild(NodeType.Table, 2, true);

                    for (var i = 2; i < table.Rows.Count; i++)
                    {
                        if (table.Rows[i].Cells.Count > 12)
                        {
                            var temp = table.Rows[i].Cells[0].GetText();
                            if (temp == "1\a" || temp == "2\a" || temp == "3\a")
                            {
                                foreach (Aspose.Words.Tables.Cell cell in table.Rows[i].Cells)
                                {
                                    // Get Runs in this cell.
                                    NodeCollection runs = cell.GetChildNodes(NodeType.Run, true);

                                    // Loop through all runs and make them bold.
                                    foreach (Run run in runs)
                                    {
                                        run.Font.Bold = true;
                                    }
                                }
                            }
                        }
                    }

                    doc.Save(rootPath + WordPathExport + strOutputFileName);

                    if (isSuccess)
                    {
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "PlanNextMonthReport_" + (DateTime.Now.Month + 1) + "-" + DateTime.Now.Year + ".doc", true);
                    }
                }
            }
            else if (e.Argument.Contains("FTK01"))
            {
                var selectedProject = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
                if (selectedProject != null)
                {
                    var wpList = this.workGroupService.GetAllWorkGroupOfProject(selectedProject.ID);
                    var wpCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("Department", Type.GetType("System.String")),
                                new DataColumn("WPStartDate", Type.GetType("System.String")),
                                new DataColumn("WPEndDate", Type.GetType("System.String")),
                                new DataColumn("InputDataSupplier", Type.GetType("System.String")),
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("ProjectManager", Type.GetType("System.String")),
                                new DataColumn("Supervisor", Type.GetType("System.String")),
                                new DataColumn("StartDate", Type.GetType("System.String")),
                                new DataColumn("EndDate", Type.GetType("System.String")),
                                new DataColumn("ProjectDescription", Type.GetType("System.String")),
                                new DataColumn("ProjectName", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var infoItem = reportInfo.NewRow();
                    var projectManager = this.userService.GetByID(selectedProject.ProjectManagerId.GetValueOrDefault());

                    infoItem["ProjectManager"] = projectManager != null ? projectManager.FullName : string.Empty;
                    infoItem["Supervisor"] = selectedProject.SupervisorUserName;
                    infoItem["StartDate"] = selectedProject.StartDate != null ? selectedProject.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                    infoItem["EndDate"] = selectedProject.EndDate != null ? selectedProject.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty; ;
                    infoItem["ProjectDescription"] = selectedProject.Description;
                    infoItem["ProjectName"] = selectedProject.Name;
                    reportInfo.Rows.Add(infoItem);

                    foreach (var workpackage in wpList)
                    {
                        var dataworkpackageItem = dataTable.NewRow();
                        wpCount += 1;
                        dataworkpackageItem["Index"] = wpCount;
                        dataworkpackageItem["WorkPackageContent"] = workpackage.Name;
                        dataworkpackageItem["Department"] = workpackage.DepartmentName;
                        dataworkpackageItem["WPStartDate"] = workpackage.StartDate != null ? workpackage.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                        dataworkpackageItem["WPEndDate"] = workpackage.EndDate != null ? workpackage.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                        dataworkpackageItem["InputDataSupplier"] = workpackage.InputDataSupplier;
                        dataTable.Rows.Add(dataworkpackageItem);


                        foreach (var workpackageContent in this.workpackageContentService.GetAllByWorkpackage(workpackage.ID))
                        {
                            var contentItem = dataTable.NewRow();
                            contentItem["WorkPackageContent"] = workpackageContent.ContentInfo;
                            contentItem["WPEndDate"] = workpackageContent.Deadline != null ? workpackageContent.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                            dataTable.Rows.Add(contentItem);
                        }

                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = e.Argument == "FTK01V" ? "F-TK-01-V_Template.doc" : "F-TK-01-R_Template.doc";
                    var strOutputFileName = "F-TK-01_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                    if (isSuccess)
                    {
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "F-TK-01_" + Utility.RemoveSpecialCharacter(selectedProject.Name, "-") + ".doc", true);
                    }

                }
            }
            else if (e.Argument.Contains("FTK02"))
            {
                var selectedWP =
                    this.workGroupService.GetById(this.rtvWorkgroup.SelectedNode != null
                        ? Convert.ToInt32(this.rtvWorkgroup.SelectedValue)
                        : 0);
                if (selectedWP != null)
                {
                    var contentCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("DeadLine", Type.GetType("System.String")),
                                new DataColumn("Note", Type.GetType("System.String"))
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("DepartmentName", Type.GetType("System.String")),
                                new DataColumn("DepartmentDescription", Type.GetType("System.String")),
                                new DataColumn("ProjectDescription", Type.GetType("System.String")),
                                new DataColumn("ProjectName", Type.GetType("System.String")),
                                new DataColumn("WorkpackageName", Type.GetType("System.String")),
                                new DataColumn("WPStartDate", Type.GetType("System.String")),
                                new DataColumn("ProjectManager", Type.GetType("System.String")),
                                new DataColumn("InputDocument", Type.GetType("System.String")),
                                new DataColumn("InfoExchange", Type.GetType("System.String")),
                                new DataColumn("ReportMode", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var infoItem = reportInfo.NewRow();
                    var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                    var projectManager = this.userService.GetByID(projectObj != null ? projectObj.ProjectManagerId.GetValueOrDefault() : 0);
                    var deparment = this.roleService.GetByID(selectedWP.DepartmentId.GetValueOrDefault());

                    infoItem["DepartmentName"] = deparment != null ? (e.Argument == "FTK02V" ? deparment.Name : deparment.RussiaName) : string.Empty;
                    infoItem["DepartmentDescription"] = deparment != null ? (e.Argument == "FTK02V" ? deparment.Description : string.Empty) : string.Empty;
                    infoItem["ProjectDescription"] = projectObj != null ? projectObj.Description : string.Empty;
                    infoItem["ProjectName"] = projectObj != null ? projectObj.Name : string.Empty;
                    infoItem["WorkpackageName"] = selectedWP.Name;
                    infoItem["WPStartDate"] = selectedWP.StartDate != null
                        ? (e.Argument == "FTK02V"
                            ? "Ngày " + selectedWP.StartDate.GetValueOrDefault().Day + " Tháng " + selectedWP.StartDate.GetValueOrDefault().Month + " Năm " + selectedWP.StartDate.GetValueOrDefault().Year
                            : selectedWP.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"))
                        : string.Empty;

                    infoItem["ProjectManager"] = projectManager != null ? projectManager.FullName : string.Empty;
                    infoItem["InputDocument"] = selectedWP.InputDocument;
                    infoItem["InfoExchange"] = selectedWP.InformationExchange;
                    infoItem["ReportMode"] = selectedWP.ReportMode;
                    reportInfo.Rows.Add(infoItem);

                    foreach (var workpackageContent in this.workpackageContentService.GetAllByWorkpackage(selectedWP.ID))
                    {
                        var contentItem = dataTable.NewRow();
                        contentCount += 1;
                        contentItem["Index"] = contentCount;
                        contentItem["WorkPackageContent"] = workpackageContent.ContentInfo;
                        contentItem["DeadLine"] = workpackageContent.Deadline != null ? workpackageContent.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                        contentItem["Note"] = workpackageContent.Note;
                        dataTable.Rows.Add(contentItem);
                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = e.Argument == "FTK02V" ? "F-TK-02-V_Template.doc" : "F-TK-02-R_Template.doc";
                    var strOutputFileName = "F-TK-02_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                    if (isSuccess)
                    {
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "F-TK-02_" + Utility.RemoveSpecialCharacter(selectedWP.Name, "-") + ".doc", true);
                    }
                }
            }*/
            #endregion
        }
        private void GetUpdateActual(int roleid)
        {
            var countuser = this.userService.GetAllByRoleId(roleid).Where(t => t.IsLeader.GetValueOrDefault() || t.IsEngineer.GetValueOrDefault()).Count().ToString();
            var daystart = new DateTime(DateTime.Now.Date.Year, 01, 01);
            var dayend = new DateTime(DateTime.Now.Date.Year, 12, 30);
            var count = 0;
            for (var i = daystart;
              i <= dayend;
              i = i.AddMonths(1))
            {
                if (DateTime.Now.Month > i.Month)
                {
                    count += 1;
                }
            }

            var existProgressActual = this.currentmanpowerService.GetDepartment(roleid, DateTime.Now.Year);
            if (existProgressActual != null)
            {
                if (existProgressActual.Values.Split('$').Count() >= count)
                {
                    var actualList = existProgressActual.Values.Split('$');
                    actualList[count - 1] = countuser;
                    var newActual = string.Empty;
                    newActual = actualList.Aggregate(newActual, (current, t) => current + t + "$");

                    newActual = newActual.Substring(0, newActual.Length - 1);
                    existProgressActual.Values = newActual;

                    this.currentmanpowerService.Update(existProgressActual);
                }

            }
        }
        //private void CollectData(WorkGroup workpackage, Milestone milestone, ref DataTable dt, int wpCount, string index)
        //{
        //    var dataitem = dt.NewRow();
        //    dataitem["Index"] = index + wpCount;
        //    dataitem["WorkPackageContent"] = workpackage.Description + "\n" + workpackage.Name;
        //    dataitem["Text1"] = workpackage.StartDate != null || !string.IsNullOrEmpty(workpackage.IncomingNo)
        //        ? "Тех.задание ГИПа " + workpackage.IncomingNo + " от " + (workpackage.StartDate != null ? workpackage.StartDate.Value.ToString("dd.MM.yy") : string.Empty)
        //        : string.Empty;
        //    dataitem["Plan"] = milestone.MilestoneDate <= workpackage.Deadline
        //                                    ? "(+)" + milestone.PlanPercent + "%"
        //                                    : "(-)" + milestone.PlanPercent + "%";
        //    dataitem["PlanTotal"] = milestone.MilestoneDate <= workpackage.Deadline
        //                                    ? "(+)" + milestone.PlanTotal + "%"
        //                                    : "(-)" + milestone.PlanTotal + "%";
        //    dataitem["EndDate"] = workpackage.Deadline != null
        //                                    ? workpackage.Deadline.Value.ToString("dd.MM.yyyy")
        //                                    : string.Empty;
        //    dataitem["OutgoingNumber"] = milestone.RealTotal != null && milestone.RealTotal == 100
        //                                            ? workpackage.OutgoingNo
        //                                            : "В работе";
        //    dataitem["Real"] = milestone.MilestoneDate <= workpackage.Deadline
        //                                    ? "(+)" + milestone.Real + "%"
        //                                    : "(-)" + milestone.Real + "%";
        //    dataitem["RealTotal"] = milestone.MilestoneDate <= workpackage.Deadline
        //                                    ? "(+)" + milestone.RealTotal + "%"
        //                                    : "(-)" + milestone.RealTotal + "%";

        //    dataitem["RealPerPlan"] = milestone.MilestoneDate <= workpackage.Deadline
        //                                    ? "(+)" + Math.Round((double)((milestone.Real / milestone.PlanPercent) * 100), 0) + "%"
        //                                    : "(-)" + Math.Round((double)((milestone.Real / milestone.PlanPercent) * 100), 0) + "%";
        //    dataitem["TotalRealPerTotalPlan"] = milestone.MilestoneDate <= workpackage.Deadline
        //                                    ? "(+)" + Math.Round((double)((milestone.RealTotal / milestone.PlanTotal) * 100), 0) + "%"
        //                                    : "(-)" + Math.Round((double)((milestone.RealTotal / milestone.PlanTotal) * 100), 0) + "%";

        //    dataitem["MilestoneDate"] = milestone.MilestoneDate != null
        //                                    ? milestone.MilestoneDate.Value.ToString("dd.MM.yy")
        //                                    : string.Empty;
        //    dataitem["TotalManHours"] = workpackage.TotalManHours != null
        //                                    ? Math.Round(workpackage.TotalManHours.Value, 2).ToString()
        //                                    : string.Empty;
        //    dataitem["UsedManHours"] = workpackage.UsedManHours != null
        //                                    ? Math.Round(workpackage.UsedManHours.Value, 2).ToString()
        //                                    : string.Empty;

        //    dataitem["PerformingUser"] = milestone.PerformingUser;
        //    dataitem["Note"] = milestone.Note;

        //    dt.Rows.Add(dataitem);
        //}

        //private void CollectData1(WorkGroup workpackage, Milestone milestone, ref DataTable dt, int wpCount, string index)
        //{
        //    var dataitem = dt.NewRow();
        //    dataitem["Index"] = index + wpCount;
        //    dataitem["WorkPackageContent"] = workpackage.Description + "\n" + workpackage.Name;
        //    dataitem["Text1"] = workpackage.StartDate != null || !string.IsNullOrEmpty(workpackage.OutgoingNo)
        //        ? "Тех.задание ГИПа " + workpackage.OutgoingNo + " от " + (workpackage.StartDate != null ? workpackage.StartDate.Value.ToString("dd.MM.yy") : string.Empty)
        //        : string.Empty;
        //    dataitem["Plan"] = milestone.PlanPercent + "%";
        //    dataitem["PlanTotal"] = milestone.PlanTotal + "%";
        //    dataitem["MaxPlan"] = "100%";
        //    dataitem["MilestoneDate"] = milestone.MilestoneDate != null
        //                                    ? milestone.MilestoneDate.Value.ToString("dd.MM.yyyy")
        //                                    : string.Empty;
        //    dataitem["PerformingUser"] = milestone.PerformingUser;
        //    dataitem["Note"] = milestone.Note;

        //    dt.Rows.Add(dataitem);
        //}

        private int sumuser(List<CurrentManpower> listuser, int i)
        {
            var sum = 0;
            foreach (var item in listuser)
            {
                var st = item.Values.Split('$');
                sum += Convert.ToInt32(st[i]);
            }
            return sum;
        }
        private void LoadProcessReport(int projectId, int workgroupId)
        {
            //if (department != 0)
            //{
            //    this.GetUpdateActual(department);
            //    LineChart.PlotArea.YAxis.MaxValue = 100;
            //}
            //else
            //{
            //    LineChart.PlotArea.YAxis.MaxValue = 150;
            //}

            //var lineSeries = this.LineChart.PlotArea.Series[1] as ColumnSeries;
            var lineSeries = this.LineChart.PlotArea.Series[1] as LineSeries;
            //  var lineSeries = this.LineChart.PlotArea.Series[3] as LineSeries;
            //var processReportList = new List<ProcessReport>();
            //var temp = new List<ProcessReport>();
            if (lineSeries != null)
            {
                lineSeries.Items.Clear();
            }

            //var rolelist = this.roleService.GetAll(false).Where(t => t.IsWorking.GetValueOrDefault()).Select(t => (int)t.Id).ToList();
            //var uservalue = "0$0$0".Split('$');
            //var uservaluelist = new List<CurrentManpower>();
            //var processPlanedList = new List<ProcessManhourPlaned>();
            //var processActualList = new List<ProcessManhourActual>();
            //if (department == 0)
            //{
            //    processPlanedList = this.processmanhourplanedservice.GetAllCurrent(rolelist, DateTime.Now.Date);
            //    processActualList = this.processmanhouractualservice.GetAllCurrent(rolelist, DateTime.Now.Date);
            //    uservaluelist = this.currentmanpowerService.GetDepartment(rolelist, DateTime.Now.Year);
            //}
            //else
            //{
            //    processPlanedList = this.processmanhourplanedservice.GetAllCurrent(department, DateTime.Now.Date);
            //    processActualList = this.processmanhouractualservice.GetAllCurrent(department, DateTime.Now.Date);
            //    uservalue = this.currentmanpowerService.GetDepartment(department, DateTime.Now.Year).Values.Split('$');
            //}
            //if (processPlanedList != null)
            //{
            //    if (processPlanedList.Count > 0)
            //    {
            //        var daystart = new DateTime(DateTime.Now.Date.Year, 01, 01);
            //        var dayend = new DateTime(DateTime.Now.Date.Year, 12, 30);
            //        var k = 0;
            //        for (var i = daystart;
            //          i <= dayend;
            //          i = i.AddMonths(1))
            //        {
            //            var processReport = new ProcessReport();
            //            processReport.STRWeekDate = i.ToString("MM/yyyy");
            //            processReport.Number = department != 0 ? Convert.ToInt32(uservalue[k]) : this.sumuser(uservaluelist, k);
            //            processReportList.Add(processReport);
            //            temp.Add(processReport);
            //            k++;
            //        }
            //        foreach (var processPlaned in processPlanedList)
            //        {
            //            var actualList = new List<double>();
            //            var PlanedList = new List<double>();
            //            var processActual = processActualList.FirstOrDefault(t => t.WorkgroupId == processPlaned.WorkgroupId);
            //            if (processActual != null)
            //            {
            //                actualList = processActual.Actual.Split('$').Select(Convert.ToDouble).ToList();
            //            }
            //            PlanedList = processPlaned.Planed.Split('$').Select(Convert.ToDouble).ToList();
            //            var count = 0;
            //            for (var j = processPlaned.StartDate.GetValueOrDefault(); j <= processPlaned.EndDate.GetValueOrDefault(); j = j.AddMonths(1))
            //            {
            //                if (j.Month >= daystart.Month && j.Month <= dayend.Month && j.Year >= daystart.Year && j.Year <= dayend.Year)
            //                {
            //                    k = 0;
            //                    for (var i = daystart;
            //                      i <= dayend;
            //                      i = i.AddMonths(1))
            //                    {
            //                        if (j.Month == i.Month && j.Year == i.Year)
            //                        {
            //                            processReportList[k].Planed += Math.Round((PlanedList[count] / (8 * 22)), 2);
            //                            processReportList[k].Actual += Math.Round((actualList[count] / (8 * 22)), 2);
            //                            break;
            //                        }
            //                        k++;
            //                    }
            //                }
            //                count++;
            //            }
            //        }
            //    }
            //    LineChart.PlotArea.XAxis.DataLabelsField = "STRWeekDate";
            //    LineChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Auto;
            //    LineChart.PlotArea.XAxis.Type = Telerik.Web.UI.HtmlChart.AxisType.Auto;
            //    LineChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "{0}";
            //    LineChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 0;
            //    LineChart.PlotArea.XAxis.LabelsAppearance.Step = 1;
            //    LineChart.PlotArea.XAxis.LabelsAppearance.Skip = 0;
            //    LineChart.PlotArea.XAxis.TitleAppearance.Text = "MONTH";
            //    foreach (var st in processReportList)
            //    {
            //        st.Planed = Math.Round(st.Planed, 0);
            //        st.Actual = Math.Round(st.Actual, 0);
            //    }
            //    //this.LineChart.ChartTitle.Text = "DETAIL ENGINEERING SERVICE FOR " + projectObj.Name +                                                         " | Cut-off: " +                                                                                          DateTime.Now.ToString("dd/MM/yyyy");
            //    this.LineChart.DataSource = processReportList;
            //    this.LineChart.DataBind();
            //    //if (lineSeries != null)
            //    //{
            //    //    lineSeries.Items.Clear();
            //    //    foreach (var actual in temp)
            //    //    {
            //    //        if (actual.Actual == 0)
            //    //        {
            //    //            lineSeries.Items.Add((decimal?)null);
            //    //        }
            //    //        else
            //    //        {
            //    //            lineSeries.Items.Add((decimal?)actual.Actual);
            //    //        }
            //    //    }
            //    //}
            //}

            var processReportList = new List<ProcessReport>();
            var temp = new List<ProcessReport>();
            var totalManhourPJ = 0.0;
            var projectObj = this.scopeProjectService.GetById(projectId);
            var processPlanedList = this.processPlanedService.GetAllByProject(projectId, workgroupId);
            var processActualList = this.processActualService.GetAllByProject(projectId, workgroupId);
            if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null)
            {
                if (workgroupId == 0)
                {
                    var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectId);
                    totalManhourPJ = wpList.Sum(t => t.TotalManHours.GetValueOrDefault());
                    LineChart.PlotArea.YAxis.MaxValue = Convert.ToDecimal(totalManhourPJ);
                    LineChart.PlotArea.YAxis.Step = Convert.ToDecimal(Math.Round(totalManhourPJ / 10, 0));
                }
                else
                {
                    var wpList = this.workGroupService.GetById(workgroupId);
                    LineChart.PlotArea.YAxis.MaxValue = Convert.ToDecimal(wpList.TotalManHours.GetValueOrDefault());
                    LineChart.PlotArea.YAxis.Step = Convert.ToDecimal(Math.Round(wpList.TotalManHours.GetValueOrDefault() / 10, 0));
                }

                if (!projectObj.WeeklyProgress.GetValueOrDefault())
                {
                    //var startDate = projectObj.StartDate.GetValueOrDefault();
                    //var deadline = projectObj.Deadline.GetValueOrDefault();
                    //var diffMonth = ((deadline.Year - startDate.Year) * 12) + deadline.Month - startDate.Month;

                    for (var j = projectObj.StartDate.GetValueOrDefault();
                        j < projectObj.Deadline.GetValueOrDefault();
                        j = j.AddMonths(1))
                    {
                        var processReport = new ProcessReport();
                        processReport.STRWeekDate = j.ToString("MM/yyyy");

                        processReportList.Add(processReport);
                        if (DateTime.Now > j)
                        {
                            temp.Add(processReport);
                        }
                    }
                    foreach (var processPlaned in processPlanedList)
                    {
                        var countPlan = 0;

                        var workgroupObj = this.workGroupService.GetById(processPlaned.WorkgroupId.GetValueOrDefault());
                        if (workgroupObj != null)
                        {
                            foreach (var planed in processPlaned.PlanedMonth.Split('$').Select(Convert.ToDouble))
                            {
                                if (countPlan < processReportList.Count)
                                {
                                    processReportList[countPlan].Planed += workgroupId == 0
                                        ? Math.Round(((planed * workgroupObj.Weight.GetValueOrDefault()) / 100) * totalManhourPJ / 100, 0)
                                        : Math.Round((planed * workgroupObj.TotalManHours.GetValueOrDefault()) / 100, 0);
                                }
                                countPlan += 1;
                            }
                        }
                    }
                    foreach (var processActual in processActualList)
                    {
                        var countActual = 0;
                        var workgroupObj = this.workGroupService.GetById(processActual.WorkgroupId.GetValueOrDefault());
                        if (workgroupObj != null)
                        {
                            foreach (var actual in processActual.ActualMonth.Split('$').Select(Convert.ToDouble).ToList())
                            {
                                if (countActual < temp.Count)
                                {
                                    temp[countActual].Actual += workgroupId == 0
                                        ? Math.Round(((actual * workgroupObj.Weight.GetValueOrDefault()) / 100) * totalManhourPJ / 100, 0)
                                        : Math.Round((actual * workgroupObj.TotalManHours.GetValueOrDefault()) / 100, 0);
                                    countActual++;
                                }
                            }
                        }
                    }

                    LineChart.PlotArea.XAxis.DataLabelsField = "STRWeekDate";
                    LineChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Auto;
                    LineChart.PlotArea.XAxis.Type = Telerik.Web.UI.HtmlChart.AxisType.Auto;
                    LineChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "{0}";
                    LineChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 270;
                    LineChart.PlotArea.XAxis.LabelsAppearance.Step = 1;
                    LineChart.PlotArea.XAxis.LabelsAppearance.Skip = 0;
                    LineChart.PlotArea.XAxis.TitleAppearance.Text = "MONTH";
                }
                else
                {
                    for (var i = GetSaturdayOfWeek(projectObj.StartDate.GetValueOrDefault());
                        i < projectObj.Deadline.GetValueOrDefault();
                        i = i.AddDays(7))
                    {
                        var processReport = new ProcessReport();
                        processReport.WeekDate = i;

                        processReportList.Add(processReport);
                        if (DateTime.Now > i)
                        {
                            temp.Add(processReport);
                        }
                    }
                    foreach (var processPlaned in processPlanedList)
                    {
                        var countPlan = 0;

                        var workgroupObj = this.workGroupService.GetById(processPlaned.WorkgroupId.GetValueOrDefault());
                        if (workgroupObj != null)
                        {
                            foreach (var planed in processPlaned.Planed.Split('$').Select(Convert.ToDouble))
                            {
                                if (countPlan < processReportList.Count)
                                {
                                    processReportList[countPlan].Planed += workgroupId == 0
                                        ? Math.Round(((planed * workgroupObj.Weight.GetValueOrDefault()) / 100) * totalManhourPJ / 100, 0)
                                        : Math.Round((planed * workgroupObj.TotalManHours.GetValueOrDefault()) / 100, 0);
                                }
                                countPlan += 1;
                            }
                        }
                    }
                    foreach (var processActual in processActualList)
                    {
                        var countActual = 0;
                        var workgroupObj = this.workGroupService.GetById(processActual.WorkgroupId.GetValueOrDefault());
                        if (workgroupObj != null)
                        {
                            foreach (var actual in processActual.Actual.Split('$').Select(Convert.ToDouble).ToList())
                            {
                                if (countActual < temp.Count)
                                {
                                    temp[countActual].Actual += workgroupId == 0
                                        ? Math.Round(((actual * workgroupObj.Weight.GetValueOrDefault()) / 100) * totalManhourPJ / 100, 0)
                                        : Math.Round((actual * workgroupObj.TotalManHours.GetValueOrDefault()) / 100, 0);
                                    countActual++;
                                }
                            }
                        }
                    }
                    LineChart.PlotArea.XAxis.DataLabelsField = "WeekDate";
                    LineChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Days;
                    LineChart.PlotArea.XAxis.Type = Telerik.Web.UI.HtmlChart.AxisType.Date;
                    LineChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "d";
                    LineChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 270;
                    LineChart.PlotArea.XAxis.LabelsAppearance.Step = 7;
                    LineChart.PlotArea.XAxis.TitleAppearance.Text = "";
                }
                this.LineChart.ChartTitle.Text = "DETAIL ENGINEERING SERVICE FOR " + projectObj.Name +
                                                 " | Cut-off: " +
                                                 DateTime.Now.ToString("dd/MM/yyyy");


                this.LineChart.DataSource = processReportList;
                this.LineChart.DataBind();
                if (lineSeries != null)
                {
                    lineSeries.Items.Clear();

                    foreach (var actual in temp)
                    {

                        if (actual.Actual == 0)
                        {
                            lineSeries.Items.Add((decimal?)null);
                        }
                        else
                        {
                            lineSeries.Items.Add((decimal?)actual.Actual);
                        }
                    }
                }
            }

        }

        //private void InitData()
        //{
        //    var full = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsSuperViewer;
        //    var role = full ? this.roleService.GetAll(false) : this.roleService.GetAll(false).Where(t=> t.Id==UserSession.Current.User.RoleId.GetValueOrDefault()).ToList();
        //    role = role.Where(t => t.IsWorking.GetValueOrDefault()).ToList();
        //    role.Add(new Role() { Id = 0, Name = "All" });
        //    this.ddlProject.DataSource = role.OrderBy(t=>t.Name);
        //        this.ddlProject.DataTextField = "Name";
        //        this.ddlProject.DataValueField = "Id";
        //        this.ddlProject.DataBind();
        //        this.ddlProject.SelectedIndex = 0;
        //        var rolelist = this.roleService.GetAll(false).Where(t => t.IsWorking.GetValueOrDefault()).Select(t => (int)t.Id).ToList();
        //      //  if (role.Any())
        //        //{  
        //            var projectId = this.ddlProject.SelectedItem != null
        //            ? Convert.ToInt32(this.ddlProject.SelectedValue)
        //            : 0;
        //            var listmanhour = projectId != 0 ? this.processmanhourplanedservice.GetAllCurrent(projectId, DateTime.Now.Date).Select(t => t.WorkgroupId) : this.processmanhourplanedservice.GetAllCurrent(rolelist, DateTime.Now.Date).Select(t => t.WorkgroupId);
        //             var wp = this.workGroupService.GetAll().Where(t => listmanhour.Contains(t.ID));
        //             this.rtvWorkgroup.DataSource = wp;
        //            this.rtvWorkgroup.DataTextField = "Name";
        //            this.rtvWorkgroup.DataValueField = "ID";
        //            this.rtvWorkgroup.DataFieldID = "ID";
        //            this.rtvWorkgroup.DataBind();
        //       // }
        //}

        private List<WorkGroup> WorkpackageList
        {
            get
            {
                var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
                var currentDepartment = UserSession.Current.User.RoleId;

                return isSeeFull
                    ? this.workGroupService.GetAll()
                    : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault());
            }
        }

        private void InitData()
        {
            var wpList = new List<WorkGroup>();
            if (UserSession.Current.IsEngineer)
            {
                var wpInWorkOfEng =
                    this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id)
                        .Select(t => t.WorkgroupId)
                        .Distinct();
                wpList = this.WorkpackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
            }
            else
            {
                wpList = WorkpackageList;
            }

            var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
            //var currentDepartment = UserSession.Current.User.RoleId;

            var projectInPermission = this.scopeProjectService.GetAll(wpList.Select(t => t.ProjectId.GetValueOrDefault()).Distinct().ToList());

            if (UserSession.Current.IsGip)
            {
                projectInPermission = projectInPermission.Where(t => t.ProjectManagerId == UserSession.Current.User.Id).ToList();
            }
            var workPackageList = WorkpackageList;
            this.ddlProject.DataSource = projectInPermission;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();
            this.ddlProject.SelectedIndex = 0;

            if (projectInPermission.Any())
            {
                this.rtvWorkgroup.DataSource = workPackageList.Where(
                                            t =>
                                                t.ProjectId ==
                                                (!string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                                                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                                    : 0));
                this.rtvWorkgroup.DataTextField = "Name";
                this.rtvWorkgroup.DataValueField = "ID";
                this.rtvWorkgroup.DataFieldID = "ID";
                this.rtvWorkgroup.DataBind();
            }
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }

                //Mo file de doc
                var fs = new FileStream(strFileName, FileMode.Open);
                var streamLength = Convert.ToInt32(fs.Length);
                var data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        //private void NotificationDeadlineDoc()
        //{
        //    var notifi = this.emailNotificationTemplateService.GetByType(Utility.DeadlineDoc);
        //    if (notifi.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy") != DateTime.Now.ToString("dd/MM/yyyy"))
        //    {
        //        DateTime daytomorrow = DateTime.Now.AddDays(1);
        //        var dealinedocument = this.documentPackageService.GetAll().Where(t => t.Complete < 100 && DateTime.ParseExact(t.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"), "dd/MM/yyyy", null) == DateTime.ParseExact(daytomorrow.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null)).ToList();
        //        foreach (DocumentPackage docObj in dealinedocument)
        //        {

        //            if (docObj.EngineerId != 0)
        //            {
        //                var engineer = this.userService.GetByID(docObj.EngineerId.GetValueOrDefault());
        //                var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());

        //                //var emailList = this.userService.GetAllByRoleId(wpObj != null ? wpObj.DepartmentId.GetValueOrDefault() : 0)
        //                //    .Where(t => t.IsChief.GetValueOrDefault())
        //                //    .Select(t => t.Email)
        //                //    .Where(t => !string.IsNullOrEmpty(t)).ToList();

        //                if (engineer != null)
        //                {

        //                    var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.DeadlineDoc);
        //                    if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault() )
        //                    {
        //                        var smtpClient = new SmtpClient
        //                        {
        //                            DeliveryMethod = SmtpDeliveryMethod.Network,
        //                            UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
        //                            EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
        //                            Host = ConfigurationManager.AppSettings["Host"],
        //                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
        //                            Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
        //                        };
        //                        var updatedUser = this.userService.GetByID(UserSession.Current.User.Id);
        //                        var subject = notificationTemplate.Subject.Replace("#DocNumber#", docObj.DocNo);

        //                        var message = new MailMessage();
        //                        message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
        //                        message.Subject = subject;
        //                        message.BodyEncoding = new UTF8Encoding();
        //                        message.IsBodyHtml = true;
        //                        message.Body = notificationTemplate.Contents
        //                            .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
        //                            .Replace("#DocNumber#", docObj.DocNo)
        //                            .Replace("#DocTitle#", docObj.DocTitle)
        //                            .Replace("#DocType#", docObj.DocumentTypeName)
        //                            .Replace("#DocRev#", docObj.RevisionName)
        //                            .Replace("#DocEng#", engineer.FullName)
        //                            .Replace("#DocStartDate#", docObj.StartDate != null ? docObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
        //                            .Replace("#DocDeadline#", docObj.Deadline != null ? docObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
        //                            .Replace("#DocWeight#", docObj.Weight != null ? docObj.Weight.Value.ToString() : string.Empty)
        //                            .Replace("#DocComplete#", docObj.Complete != null ? docObj.Complete.Value.ToString() : string.Empty);

        //                        if (!string.IsNullOrEmpty(engineer.Email))
        //                        {
        //                            message.To.Add(new MailAddress(engineer.Email));
        //                            smtpClient.Send(message);
        //                        }

        //                    }
        //                }
        //            }
        //        }
        //        notifi.UpdatedDate = DateTime.Now;
        //        this.emailNotificationTemplateService.Update(notifi);
        //    }

        //}

        //private void NotificationDeadlineWp()
        //{
        //    var notifi = this.emailNotificationTemplateService.GetByType(Utility.DeadlineWP);
        //    if (notifi.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy") != DateTime.Now.ToString("dd/MM/yyyy"))
        //    {
        //        DateTime daytomorrow = DateTime.Now.AddDays(1);
        //        var listwp = this.workGroupService.GetAll().Where(t => t.Complete < 100 && DateTime.ParseExact(t.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"), "dd/MM/yyyy", null) == DateTime.ParseExact(daytomorrow.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null)).ToList();
        //        foreach (WorkGroup wpObj in listwp)
        //        {
        //            if (wpObj.DepartmentId != 0)
        //            {
        //                var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
        //                var gipUser = this.userService.GetByID(projectObj != null ? projectObj.ProjectManagerId.GetValueOrDefault() : 0);
        //                //var emailList = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault())
        //                //    .Where(t => t.IsChief.GetValueOrDefault())
        //                //    .Select(t => t.Email)
        //                //    .Where(t => !string.IsNullOrEmpty(t)).ToList();



        //                var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.DeadlineWP);
        //                if ( notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
        //                {
        //                    var smtpClient = new SmtpClient
        //                    {
        //                        DeliveryMethod = SmtpDeliveryMethod.Network,
        //                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
        //                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
        //                        Host = ConfigurationManager.AppSettings["Host"],
        //                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
        //                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
        //                    };

        //                    var wpUpdatedUser = this.userService.GetByID(wpObj.UpdatedBy.GetValueOrDefault());
        //                    var subject = notificationTemplate.Subject.Replace("#WPName#", wpObj.Name);

        //                    var message = new MailMessage();
        //                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
        //                    message.Subject = subject;
        //                    message.BodyEncoding = new UTF8Encoding();
        //                    message.IsBodyHtml = true;
        //                    message.Body = notificationTemplate.Contents
        //                        .Replace("#WPNumber#", wpObj.Name)
        //                        .Replace("#WPProjectNumber#", projectObj != null ? projectObj.Name : string.Empty)
        //                        .Replace("#WPProjectName#", projectObj != null ? projectObj.Description : string.Empty)
        //                        .Replace("#WPName#", wpObj.Description)
        //                        .Replace("#WPStartDate#", wpObj.StartDate != null ? wpObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
        //                        .Replace("#WPDeadline#", wpObj.Deadline != null ? wpObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
        //                        .Replace("#WPComplete#", wpObj.Complete != null ? wpObj.Complete.Value.ToString() : string.Empty);

        //                    if (gipUser != null && !string.IsNullOrEmpty(gipUser.Email))
        //                    {
        //                    message.To.Add(new MailAddress(gipUser.Email));
        //                    smtpClient.Send(message); 
        //                    }
        //                }
        //            }
        //        }

        //        notifi.UpdatedDate = DateTime.Now;
        //        this.emailNotificationTemplateService.Update(notifi);
        //    }
        //}
    }
}


﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistributionMatrixDetailDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class DistributionMatrixDetailDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DistributionMatrixDetailDAO"/> class.
        /// </summary>
        public DistributionMatrixDetailDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DistributionMatrixDetail> GetAll()
        {
            return this.EDMsDataContext.DistributionMatrixDetails.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public DistributionMatrixDetail GetById(int id)
        {
            return this.EDMsDataContext.DistributionMatrixDetails.FirstOrDefault(ob => ob.ID == id);
        }

        #endregion

        #region GET ADVANCE

        public List<DistributionMatrixDetail> GetByWorkpackage(int wpID)
        {
            return this.EDMsDataContext.DistributionMatrixDetails.Where(ob => ob.WorkgroupID == wpID).ToList();
        }

        public List<DistributionMatrixDetail> GetByProject(int pjID)
        {
            return this.EDMsDataContext.DistributionMatrixDetails.Where(ob => ob.ProjectID == pjID).ToList();
        }

        public List<DistributionMatrixDetail> GetByDocument(int docID)
        {
            return this.EDMsDataContext.DistributionMatrixDetails.Where(ob => ob.DocumentID == docID).ToList();
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see>
        ///       <cref>int?</cref>
        ///     </see> .
        /// </returns>
        public int? Insert(DistributionMatrixDetail ob)
        {
            try
            {
                this.EDMsDataContext.AddToDistributionMatrixDetails(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(DistributionMatrixDetail src)
        {
            try
            {
                DistributionMatrixDetail des = (from rs in this.EDMsDataContext.DistributionMatrixDetails
                                                where rs.ID == src.ID
                                                select rs).First();

                des.WorkgroupID = src.WorkgroupID;
                des.DisciplineID = src.DisciplineID;
                des.DisciplineName = src.DisciplineName;
                des.ProjectID = src.ProjectID;
                des.DocumentID = src.DocumentID;
                des.DocumentName = src.DocumentName;

                des.UpdateBy = src.UpdateBy;
                des.UpdateDate = src.UpdateDate;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(DistributionMatrixDetail src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace EDMs.Web.Controls.Customers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Business.Services;
    using EDMs.Web.Utilities.Sessions;
    using Telerik.Web.UI;
    using EDMs.Data.Entities;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class Detail_Description_Edit : Page
    {
        /// <summary>
        /// The patient status service
        /// </summary>
        private Patient_DescriptionService _patientDescriptionService;
        private ServiceTypeService _serviceTypeService;

        /// <summary>
        /// The patient service
        /// </summary>
        private PatientService _patientService;

        public Detail_Description_Edit()
        {
            _serviceTypeService = new ServiceTypeService();
            _patientDescriptionService = new Patient_DescriptionService();
            _patientService =new PatientService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindData();
            }
        }

       
        private void BindData()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["cID"]))
            {
                var cID = Request.QueryString["cID"];
                if (cID != null)
                {
                    int tempcID;
                    if (int.TryParse(cID, out tempcID))
                    {
                       var customer = _patientService.GetByID(tempcID);
                       lblTenKhachHang.Text = customer.FullName;
                    }
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                var ID = Request.QueryString["ID"];
                if (ID != null)
                {
                    int tempID;
                    if (int.TryParse(ID, out tempID))
                    {
                        Description Description = _patientDescriptionService.GetByID(tempID);
                        txtContent.Text = Description.Content;
                    }
                }
            }
        }

        
        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                var ID = Request.QueryString["ID"];
                if (ID != null)
                {
                    int tempID;
                    if (int.TryParse(ID, out tempID))
                    {
                        //Update
                        UpdateDescription(tempID);
                        ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
                    }
                }
            }
            else
            {
                CreateDescription();
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }
        private void UpdateDescription(int ID)
        {
            //Create
            Description cs = _patientDescriptionService.GetByID(ID);
            cs.CreatedDate = DateTime.Now;
            cs.UpdatedBy = UserSession.Current.User.Id;
            cs.UpdatedDate = DateTime.Now;
            //cs.UserID = UserSession.Current.User.Id;
            cs.Content = txtContent.Text.Trim();

            _patientDescriptionService.Update(cs);
        }
        private void CreateDescription()
        {
            //Create
            Description cs = new Description();
            cs.UpdatedBy = UserSession.Current.User.Id;
            cs.UpdatedDate = DateTime.Now;
            
            cs.UserID = UserSession.Current.User.Id;
            cs.CustomerID = Request.QueryString["cID"] == null ? 0 : int.Parse(Request.QueryString["cID"].ToString());
            cs.Content = txtContent.Text.Trim();
            
            _patientDescriptionService.Insert(cs);
        }
        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}
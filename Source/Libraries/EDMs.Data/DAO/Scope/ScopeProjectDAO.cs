﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScopeProjectDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Scope
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class ScopeProjectDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScopeProjectDAO"/> class.
        /// </summary>
        public ScopeProjectDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<ScopeProject> GetIQueryable()
        {
            return this.EDMsDataContext.ScopeProjects;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ScopeProject> GetAll()
        {
            return this.EDMsDataContext.ScopeProjects.OrderByDescending(t => t.ID).ToList();
        }
        public List<ScopeProject> GetAll(List<int> ids)
        {
            return this.EDMsDataContext.ScopeProjects.Where(t => ids.Contains(t.ID)).OrderBy(t => t.Name).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public ScopeProject GetById(int id)
        {
            return this.EDMsDataContext.ScopeProjects.FirstOrDefault(ob => ob.ID == id);
        }

        public List<ScopeProject> GetById(List<int> id)
        {
            return this.EDMsDataContext.ScopeProjects.Where(ob => id.Contains(ob.ID)).ToList();
        }

        public bool IsExist(string projectName)
        {
            return this.EDMsDataContext.ScopeProjects.Any(ob => ob.Name == projectName);
        }

        #endregion

        #region GET ADVANCE

        public List<ScopeProject> GetByPM(int id)
        {
            return this.EDMsDataContext.ScopeProjects.Where(ob => ob.ProjectManagerId == id).ToList();
        }

        public List<ScopeProject> GetAllByPlatform(int platformID)
        {
            return this.EDMsDataContext.ScopeProjects.Where(ob => ob.PlatformId == platformID).OrderByDescending(ob => ob.ID).ToList();
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(ScopeProject ob)
        {
            try
            {
                this.EDMsDataContext.AddToScopeProjects(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(ScopeProject src)
        {
            try
            {
                var des = (from rs in this.EDMsDataContext.ScopeProjects
                           where rs.ID == src.ID
                           select rs).First();

                des.Name = src.Name;
                des.Description = src.Description;
                des.BlockId = src.BlockId;
                des.BlockName = src.BlockName;
                des.StartDate = src.StartDate;
                des.EndDate = src.EndDate;
                des.TransferDocOfProject = src.TransferDocOfProject;
                des.ScopeOfWork = src.ScopeOfWork;
                des.Year = src.Year;
                des.ProjectTypeId = src.ProjectTypeId;
                des.ProjectTypeName = src.ProjectTypeName;
                des.ProjectManagerId = src.ProjectManagerId;
                des.ProjectManagerFullName = src.ProjectManagerFullName;
                des.SupervisorId = src.SupervisorId;
                des.SupervisorFullName = src.SupervisorFullName;
                des.SupervisorUserName = src.SupervisorUserName;
                des.Complete = src.Complete;
                des.IsAutoCalculate = src.IsAutoCalculate;
                des.FieldId = src.FieldId;
                des.FieldName = src.FieldName;
                des.PlatformId = src.PlatformId;
                des.PlatformName = src.PlatformName;
                des.DefaultFilePath = src.DefaultFilePath;
                des.FileExtentionIcon = src.FileExtentionIcon;
                des.PathLatestRevision = src.PathLatestRevision;
                des.CanDelete = src.CanDelete;
                des.TotalWorkpackageWeight = src.TotalWorkpackageWeight;
                des.Notes = src.Notes;
                des.UpdatedBy = src.UpdatedBy;
                des.UpdatedDate = src.UpdatedDate;
                des.PlanNo = src.PlanNo;
                des.planName = src.planName;
                des.TypePlanID = src.TypePlanID;
                des.TypePlanName = src.TypePlanName;
                des.IsIDC = src.IsIDC;
                des.IsStop = src.IsStop;
                des.TotalCostEstimate = src.TotalCostEstimate;
                des.DesignCost = src.DesignCost;
                des.ProjectPlanningCost = src.ProjectPlanningCost;
                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(ScopeProject src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

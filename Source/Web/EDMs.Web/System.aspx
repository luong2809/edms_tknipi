﻿<%@ Page Title="System" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="System.aspx.cs" Inherits="EDMs.Web._System" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_pnlSystemPanel {
            height:100% !important;
        }
        #divContainerLeft
        {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight
        {
            width: 72%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted
        {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper
        {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip
        {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage
        {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow
        {
            z-index: 8000 !important;
        }
        .TemplateMenu
        {
            z-index: 10;
        }
    </style>
    <telerik:RadTreeView 
        ID="treeSystem" 
        runat="server" 
        DataFieldID = "Id"
        DataFieldParentID = "ParentId"
        DataNavigateUrlField = "Url"
        DataTextField = "Description"
        DataValueField = "Name"
        OnNodeClick="treeSystem_OnNodeClick">
        <DataBindings>
            <telerik:RadTreeNodeBinding Expanded="True"/>
        </DataBindings>
    </telerik:RadTreeView>
    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:Panel runat="server" ID="pnlSystem" Height="100%">
        <asp:PlaceHolder ID="phdUser" runat="server"/>
        <asp:PlaceHolder ID="phdRole" runat="server"/>
        <asp:PlaceHolder ID="phdPermission" runat="server"/>
    </asp:Panel>
    <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxSystem">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="pnlSystem">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="pnlSystem" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>
    
    <!--Rad Windows-->
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RoleDialog" runat="server" Title="Thông tin vai trò" Height="400px" VisibleStatusbar="False"
                Width="600px" MaxHeight="500px" MaxWidth="630px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false"
                Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="UserDialog" runat="server" Title="Thông tin người dùng" Height="500px" VisibleStatusbar="False"
                Width="630px" MaxHeight="500px" MaxWidth="630px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false"
                Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
</asp:Content>
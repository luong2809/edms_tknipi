﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttachFilesWorkpackageDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO.Scope
{
    /// <summary>
    /// The category dao.
    /// </summary>
    public class AttachFilesWorkpackageDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AttachFilesWorkpackageDAO"/> class.
        /// </summary>
        public AttachFilesWorkpackageDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<AttachFilesWorkpackage> GetIQueryable()
        {
            return this.EDMsDataContext.AttachFilesWorkpackages;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AttachFilesWorkpackage> GetAll()
        {
            return this.EDMsDataContext.AttachFilesWorkpackages.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public AttachFilesWorkpackage GetById(int id)
        {
            return this.EDMsDataContext.AttachFilesWorkpackages.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// The get specific.
        /// </summary>
        /// <param name="tranId">
        /// The tran id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AttachFilesWorkpackage> GetSpecific(int tranId)
        {
            return this.EDMsDataContext.AttachFilesWorkpackages.Where(t => t.ID == tranId).ToList();
        }
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see>
        ///       <cref>int?</cref>
        ///     </see> .
        /// </returns>
        public int Insert(AttachFilesWorkpackage ob)
        {
            try
            {
                this.EDMsDataContext.AddToAttachFilesWorkpackages(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(AttachFilesWorkpackage src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(AttachFilesWorkpackage src)
        {
            try
            {
                var des = (from rs in this.EDMsDataContext.AttachFilesWorkpackages
                           where rs.ID == src.ID
                           select rs).First();

                des.IsDefault = src.IsDefault;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Scope;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using System.Linq;

    using EDMs.Web.Utilities.Sessions;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class OutTransmittalEditForm : Page
    {
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;
        /// <summary>
        /// The transmittal service.
        /// </summary>
        private readonly TransmittalService transmittalService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly ContractorService contractorService;

        private readonly RoleService rolesevice;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public OutTransmittalEditForm()
        {
            this.userService = new UserService();
            this.rolesevice = new RoleService();
            this.transmittalService = new TransmittalService();
            this.scopeProjectService=new ScopeProjectService();
            this.contractorService = new ContractorService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {
                    this.CreatedInfo.Visible = true;

                    var objTran = this.transmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                    if (objTran != null)
                    {
                        this.ddlProject.SelectedValue = objTran.ProjectId.ToString();
                        this.txtTransmittalNumber.Text = objTran.TransmittalNumber;
                        this.ddlReason.SelectedValue = !string.IsNullOrEmpty(objTran.ReasonForIssue)
                            ? objTran.ReasonForIssue
                            : "0";
                        this.ddlContractor.SelectedValue = objTran.ContractorId.GetValueOrDefault().ToString();
                        this.ddlGroupFrom.SelectedValue = objTran.FromDepartmentID.ToString();
                        this.ddlGroupTo.SelectedValue = objTran.ToDepartmentID.ToString();

                        var userListFrom = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroupFrom.SelectedValue)).OrderBy(t => t.FullName).ToList();

                        this.ddlFromList.DataSource = userListFrom;
                        this.ddlFromList.DataTextField = "FullName";
                        this.ddlFromList.DataValueField = "Id";
                        this.ddlFromList.DataBind();

                        var userListTo = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroupTo.SelectedValue)).OrderBy(t => t.FullName).ToList();

                        this.ddlToList.DataSource = userListTo;
                        this.ddlToList.DataTextField = "FullName";
                        this.ddlToList.DataValueField = "Id";
                        this.ddlToList.DataBind();

                        this.ddlToList.SelectedValue = objTran.ToId.ToString();
                        this.ddlFromList.SelectedValue = objTran.FromId.ToString();
                        
                        this.txtDate.SelectedDate = objTran.IssuseDate;
                        this.ddlReplyForTrans.SelectedValue = objTran.ReplyForTransId.ToString();

                        this.UploadControl.Visible = !objTran.IsGenerate.GetValueOrDefault();
                        
                        var createdUser = this.userService.GetByID(objTran.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objTran.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objTran.LastUpdatedBy != null && objTran.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objTran.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objTran.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                var toId = Convert.ToInt32(this.ddlToList.SelectedValue);
                var fromId = Convert.ToInt32(this.ddlFromList.SelectedValue);

                var toObj = this.userService.GetByID(toId);
                var fromObj = this.userService.GetByID(fromId);

                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {
                    var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                    var objTran = this.transmittalService.GetById(tranId);
                    if (objTran != null)
                    {
                        objTran.TransmittalNumber = this.txtTransmittalNumber.Text.Trim();
                        objTran.ProjectName = this.ddlProject.SelectedItem.Text;
                        objTran.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                        objTran.ReasonForIssue = this.ddlReason.SelectedItem.Text;
                        objTran.ToId = toObj != null ? toObj.Id : 0;
                        objTran.ToList = toObj != null ? toObj.FullName : string.Empty;

                        objTran.FromId = fromObj != null ? fromObj.Id : 0;
                        objTran.FromList = fromObj != null ? fromObj.FullName : string.Empty;
                        objTran.ToDepartmentID = Convert.ToInt32(ddlGroupTo.SelectedValue);
                        objTran.FromDepartmentID = Convert.ToInt32(ddlGroupFrom.SelectedValue);
                        objTran.IssuseDate = this.txtDate.SelectedDate;
                        objTran.Type = 2;

                        objTran.ContractorId = this.ddlContractor.SelectedItem != null
                            ? Convert.ToInt32(this.ddlContractor.SelectedValue)
                            : 0;
                        objTran.ContractorName = this.ddlContractor.SelectedItem != null
                            ? this.ddlContractor.SelectedItem.Text
                            : string.Empty;

                        objTran.ReplyForTransId = this.ddlReplyForTrans.SelectedItem != null
                            ? Convert.ToInt32(this.ddlReplyForTrans.SelectedValue)
                            : 0;
                        objTran.ReplyForTrans = this.ddlReplyForTrans.SelectedItem != null
                            ? this.ddlReplyForTrans.SelectedItem.Text
                            : string.Empty;

                        objTran.LastUpdatedBy = UserSession.Current.User.Id;
                        objTran.LastUpdatedDate = DateTime.Now;

                        this.transmittalService.Update(objTran);
                    }
                }
                else
                {
                    var objTran = new Transmittal()
                    {
                        TransmittalNumber = this.txtTransmittalNumber.Text.Trim(),
                        ProjectName = this.ddlProject.SelectedItem.Text,
                        ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                       ReasonForIssue = this.ddlReason.SelectedItem.Text,
                        ToId = toObj != null ? toObj.Id : 0,
                        ToList = toObj != null ? toObj.FullName : string.Empty,
                        FromId = fromObj != null ? fromObj.Id : 0,
                        FromList = fromObj != null ? fromObj.FullName : string.Empty,
                        ToDepartmentID = Convert.ToInt32(ddlGroupTo.SelectedValue),
                        FromDepartmentID = Convert.ToInt32(ddlGroupFrom.SelectedValue),
                        IssuseDate = this.txtDate.SelectedDate,
                        Type = 2,
                        ContractorId = this.ddlContractor.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlContractor.SelectedValue)
                                        : 0,
                        ContractorName = this.ddlContractor.SelectedItem != null
                                        ? this.ddlContractor.SelectedItem.Text
                                        : string.Empty,
                        ReplyForTransId = this.ddlReplyForTrans.SelectedItem != null
                            ? Convert.ToInt32(this.ddlReplyForTrans.SelectedValue)
                            : 0,
                        ReplyForTrans = this.ddlReplyForTrans.SelectedItem != null
                            ? this.ddlReplyForTrans.SelectedItem.Text
                            : string.Empty,

                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                    };

                    var transId = this.transmittalService.Insert(objTran);
                    if (transId != null)
                    {
                        foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                        {
                            var filename = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                            var oldfilePath = Server.MapPath(objTran.GeneratePath);

                            var newServerPath = "../../Transmittals/Generated/" + filename;
                            var newFilePath = Server.MapPath("../../Transmittals/Generated/") + filename;

                            docFile.SaveAs(newFilePath);
                            objTran.IsGenerate = true;
                            objTran.GeneratePath = newServerPath;

                            if (File.Exists(oldfilePath))
                            {
                                File.Delete(oldfilePath);
                            }

                            this.transmittalService.Update(objTran);
                        }
                    }
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtTransmittalNumber.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter transmittal Record No.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {


            var projectInPermission = UserSession.Current.IsGip || UserSession.Current.IsCheif
                    ? this.scopeProjectService.GetAll().Where(t => t.ProjectManagerId == UserSession.Current.User.Id)
                    : this.scopeProjectService.GetAll();
            projectInPermission = projectInPermission.OrderBy(t => t.Name).ToList();
            this.ddlProject.DataSource = projectInPermission;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();

            var grouplist = this.rolesevice.GetAll(false).OrderBy(t => t.FullName).ToList();

            this.ddlGroupFrom.DataSource = grouplist;
            this.ddlGroupFrom.DataTextField = "Name";
            this.ddlGroupFrom.DataValueField = "ID";
            this.ddlGroupFrom.DataBind();

            this.ddlGroupTo.DataSource = grouplist;
            this.ddlGroupTo.DataTextField = "Name";
            this.ddlGroupTo.DataValueField = "ID";
            this.ddlGroupTo.DataBind();



            var userListFrom = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroupFrom.SelectedValue)).OrderBy(t => t.FullName).ToList();

            this.ddlFromList.DataSource = userListFrom;
            this.ddlFromList.DataTextField = "FullName";
            this.ddlFromList.DataValueField = "Id";
            this.ddlFromList.DataBind();
            
            var userListTo = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroupTo.SelectedValue)).OrderBy(t => t.FullName).ToList();

            this.ddlToList.DataSource = userListTo;
            this.ddlToList.DataTextField = "FullName";
            this.ddlToList.DataValueField = "Id";
            this.ddlToList.DataBind();


            var contractorList = this.contractorService.GetAll();
            this.ddlContractor.DataSource = contractorList;
            this.ddlContractor.DataTextField = "Name";
            this.ddlContractor.DataValueField = "ID";
            this.ddlContractor.DataBind();

            ////if (this.ddlContractor.SelectedItem != null)
            ////{
            ////    var listIncomingTrans = this.transmittalService.GetAllByContractor(Convert.ToInt32(this.ddlContractor.SelectedValue), 1);
            ////    listIncomingTrans.Insert(0, new Transmittal() { ID = 0 });
            ////    this.ddlReplyForTrans.DataSource = listIncomingTrans;
            ////    this.ddlReplyForTrans.DataTextField = "TransmittalNumber";
            ////    this.ddlReplyForTrans.DataValueField = "ID";
            ////    this.ddlReplyForTrans.DataBind();
            ////}
        }

        protected void ddlContractor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var listIncomingTrans = this.transmittalService.GetAllByContractor(Convert.ToInt32(this.ddlContractor.SelectedValue), 1);
            listIncomingTrans.Insert(0, new Transmittal() { ID = 0 });
            this.ddlReplyForTrans.DataSource = listIncomingTrans;
            this.ddlReplyForTrans.DataTextField = "TransmittalNumber";
            this.ddlReplyForTrans.DataValueField = "ID";
            this.ddlReplyForTrans.DataBind();
        }

        protected void ddlGroupTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var userListTo = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroupTo.SelectedValue)).OrderBy(t => t.FullName).ToList();
            this.ddlToList.DataSource = userListTo;
            this.ddlToList.DataTextField = "FullName";
            this.ddlToList.DataValueField = "Id";
            this.ddlToList.DataBind(); 
        }

        protected void ddlGroupFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            var userListFrom = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroupFrom.SelectedValue)).OrderBy(t => t.FullName).ToList();

            this.ddlFromList.DataSource = userListFrom;
            this.ddlFromList.DataTextField = "FullName";
            this.ddlFromList.DataValueField = "Id";
            this.ddlFromList.DataBind();
        }
    }
}
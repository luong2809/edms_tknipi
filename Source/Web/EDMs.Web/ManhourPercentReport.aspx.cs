﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using System.Data.SqlClient;
using Aspose.Cells;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;
using Telerik.Web.UI.GridExcelBuilder;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Web.UI;
    using System.Drawing;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class ManhourPercentReport : Page
    {

        private readonly UserService userService = new UserService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly WorkGroupService workGroupService = new WorkGroupService();

        private readonly RoleService roleService = new RoleService();

        private readonly ManhourService manhourService = new ManhourService();

        private readonly UserManhourMonthService UserManhourMonthService = new UserManhourMonthService();

        // private List<int> ListUserId = new List<int>();


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {

                this.LoadCombobox();
                //var monday = DateTime.Today;
                //int valueday = monday.DayOfWeek - DayOfWeek.Monday;
                //monday = monday.AddDays(-valueday);
                //var sunday = monday.AddDays(6);
                //this.txtChiefFromDate.SelectedDate = monday;
                //this.txtChiefToDate.SelectedDate = sunday;

                // if(UserSession.Current.IsCheif || UserSession.Current.IsLeader)
                //{

                //}    
            }
        }

        protected void LoadDocumentManhour()
        {
            List<int> listuser = this.userService.GetAllByRoleId(Convert.ToInt32(ddlDepartment.SelectedValue)).Where(t => t.IsEngineer.GetValueOrDefault() || t.IsLeader.GetValueOrDefault()).Select(t => (int)t.Id).ToList();

            var doclist = new List<UserManhourMonth>();

            if (this.rtvEngineer.SelectedNode != null)
            {
                doclist = this.UserManhourMonthService.GetByUserID(Convert.ToInt32(this.rtvEngineer.SelectedNode.Value), Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
                //doclist = this.manhourService.GetAllByEngineer(this.txtChiefFromDate.SelectedDate.Value, this.txtChiefToDate.SelectedDate.Value, Convert.ToInt32(this.rtvEngineer.SelectedNode.Value));
            }
            else
            {
                doclist = this.UserManhourMonthService.GetByUserID(listuser, Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
                //doclist = this.manhourService.GetAllByDocument(this.txtChiefFromDate.SelectedDate.Value, this.txtChiefToDate.SelectedDate.Value, listuser);
            }


            this.grdDocument.DataSource = doclist.OrderByDescending(t => t.DocumentName);
            //  ListUserId = doclist.Select(t => t.Engineer.GetValueOrDefault()).ToList();
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
            }
        }

        private void LoadCombobox()
        {
            for (var i = DateTime.Now.Year + 5; i >= 2019; i--)
            {
                this.ddlYear.Items.Add(new System.Web.UI.WebControls.ListItem(i.ToString(), i.ToString()));
            }
            this.ddlYear.SelectedValue = DateTime.Now.Year.ToString();
            this.ddlMonth.SelectedValue = DateTime.Now.Month.ToString();

            var departmentList = this.roleService.GetAllKhoiThietKe();
            if (!UserSession.Current.IsAdmin && !UserSession.Current.IsDC && !UserSession.Current.IsSuperViewer)
            {
                departmentList = departmentList.Where(t => t.Id == UserSession.Current.RoleId).ToList();
            }

            this.ddlDepartment.DataSource = departmentList;
            this.ddlDepartment.DataTextField = "FullName";
            this.ddlDepartment.DataValueField = "Id";
            this.ddlDepartment.DataBind();

            ddlDepartment.SelectedIndex = 0;
            if (!UserSession.Current.IsAdmin && !UserSession.Current.IsDC && !UserSession.Current.IsSuperViewer)
            {
                this.ddlDepartment.Enabled = false;
                this.ddlDepartment.SelectedValue = UserSession.Current.RoleId.ToString();
            }

            LoadEngineer();
        }

        private void LoadEngineer()
        {
            var listuser = this.userService.GetAllByRoleId(Convert.ToInt32(ddlDepartment.SelectedValue));

            this.rtvEngineer.DataSource = listuser;
            this.rtvEngineer.DataTextField = "FullName";
            this.rtvEngineer.DataValueField = "Id";
            this.rtvEngineer.DataFieldID = "Id";
            this.rtvEngineer.DataBind();
        }

        protected void rtvEngineer_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }

        protected void rtvEngineer_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            //if (!ListUserId.Contains(int.Parse(e.Node.Value)))
            //{
            //    e.Node.BackColor = Color.Aqua;
            //}

        }

        protected void View_Click(object sender, EventArgs e)
        {
            if (this.ddlMonth.SelectedValue != null && this.ddlYear.SelectedValue != null)
            {
                this.rtvEngineer.UnselectAllNodes();
                this.grdDocument.CurrentPageIndex = 0;
                this.grdDocument.Rebind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert",
                    "<script> alert('Please selected Date.')", false);
            }

        }

        protected void grdDocument_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            this.LoadDocumentManhour();
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                var id = item.GetDataKeyValue("ID");
                var userManhourMonthObj = this.UserManhourMonthService.GetById(Convert.ToInt32(id));
                if (string.IsNullOrEmpty(userManhourMonthObj.UpdateDate.ToString()))
                {
                    item["LastUpdatedDate"].Text = userManhourMonthObj.CreateDate.GetValueOrDefault().ToString("dd/MM/yyyy");
                }
                else
                {
                    item["LastUpdatedDate"].Text = userManhourMonthObj.UpdateDate.GetValueOrDefault().ToString("dd/MM/yyyy");
                }
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadEngineer();
            this.grdDocument.Rebind();
        }

        protected void ddlDepartment_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"Images/group.png";
        }

        protected void Export_Click(object sender, EventArgs e)
        {
            List<int> listuser = this.userService.GetAllByRoleId(Convert.ToInt32(ddlDepartment.SelectedValue)).Where(t => t.IsEngineer.GetValueOrDefault() || t.IsLeader.GetValueOrDefault()).Select(t => (int)t.Id).ToList();

            var groupObj = this.roleService.GetByID(Convert.ToInt32(this.ddlDepartment.SelectedValue));

            var userManhourMonthList = new List<UserManhourMonth>();

            if (this.rtvEngineer.SelectedNode != null)
            {
                //userManhourMonthList = this.manhourService.GetAllByEngineer(this.txtChiefFromDate.SelectedDate.Value, this.txtChiefToDate.SelectedDate.Value, Convert.ToInt32(this.rtvEngineer.SelectedNode.Value));
                userManhourMonthList = this.UserManhourMonthService.GetByUserID(Convert.ToInt32(this.rtvEngineer.SelectedNode.Value), Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
            }
            else
            {
                // userManhourMonthList = this.manhourService.GetAllByDocument(this.txtChiefFromDate.SelectedDate.Value, this.txtChiefToDate.SelectedDate.Value, listuser);
                userManhourMonthList = this.UserManhourMonthService.GetByUserID(listuser, Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
            }

            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\Perform.xls");
            var sheets = workbook.Worksheets;
            var sheet = sheets[0];
            var dataTable = new DataTable();
            var listColumn = new[]
                            {
                                new DataColumn("DocumentName", Type.GetType("System.String")),
                                new DataColumn("LastUpdatedDate", Type.GetType("System.String")),
                                new DataColumn("PercentComplete", Type.GetType("System.String")),
                                new DataColumn("ManhourActual", Type.GetType("System.String")),
                            };
            dataTable.Columns.AddRange(listColumn);
            double sumManhour = 0;
            foreach (var docObj in userManhourMonthList)
            {
                var dr = dataTable.NewRow();
                dr["DocumentName"] = docObj.DocumentName;
                if (string.IsNullOrEmpty(docObj.UpdateDate.ToString()))
                {
                    dr["LastUpdatedDate"] = docObj.CreateDate.GetValueOrDefault().ToString("dd/MM/yyyy");
                }
                else
                {
                    dr["LastUpdatedDate"] = docObj.UpdateDate.GetValueOrDefault().ToString("dd/MM/yyyy");
                }
                dr["PercentComplete"] = docObj.CompleteMonth;
                dr["ManhourActual"] = docObj.ManhourActualMonth;
                sumManhour += docObj.ManhourActualMonth.GetValueOrDefault();
                dataTable.Rows.Add(dr);
            }
            var drSum = dataTable.NewRow();
            drSum["DocumentName"] = "SUM";
            drSum["ManhourActual"] = sumManhour;
            dataTable.Rows.Add(drSum);
            sheet.Cells["B1"].PutValue("Perform - " + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue + "\r\n" + groupObj.Name + " - " + groupObj.Description);
            sheet.Cells.ImportDataTable(dataTable, false, 3, 1, dataTable.Rows.Count, dataTable.Columns.Count, true);
            var filename = "Report Perform_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, "Report Perform_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls", false);
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
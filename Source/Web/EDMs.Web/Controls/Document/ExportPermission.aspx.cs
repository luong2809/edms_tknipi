﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Business.Services.Document;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Scope;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;
    using System.Web.UI.HtmlControls;

    using Telerik.Web.UI;
    using Aspose.Cells;
    using System.Data;
    using System.IO;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ExportPermission : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The user data permission.
        /// </summary>
        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService;

        /// <summary>
        /// The role service.
        /// </summary>
        private readonly RoleService roleService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private bool ProjectRoot;
        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";


        /// <summary>
        /// Initializes a new instance of the <see cref="FolderPermission"/> class.
        /// </summary>
        public ExportPermission()
        {
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.folderService = new FolderService();
            this.userService = new UserService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.roleService = new RoleService();

        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                LoadComboData();

            }
        }


        private void LoadComboData()
        {
            //var groupsInPermission = this.groupDataPermissionService.GetAllByFolder(Request.QueryString["folderId"]).Select(t => t.RoleId).Distinct().ToList();
            var listGroup = this.roleService.GetAll(false).ToList();
            listGroup.Insert(0, new Role { Id = 0 });
            this.ddlGroup.DataSource = listGroup;
            this.ddlGroup.DataTextField = "FullName";
            this.ddlGroup.DataValueField = "Id";
            this.ddlGroup.DataBind();

            var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"])).Select(t => t.UserId).Distinct().ToList();
            var listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();

            listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();

        }

        protected void ddlGroup_SelectedIndexChange(object sender, EventArgs e)
        {
            var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"])).Select(t => t.UserId).Distinct().ToList();
            var listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();

            listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\exportPermission.xls");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var firstrow = 3;

            var dtFull = new DataTable();
            var ListColumn = new[]
            {
                        new DataColumn("Project", typeof(String)),
                        new DataColumn("Permission", typeof(String)),

            };
            dtFull.Columns.AddRange(ListColumn);

            var permissionList = this.userDataPermissionService.GetByUserId(Convert.ToInt32(ddlUser.SelectedValue));

            var folderList = this.folderService.GetAll();

            var userObj = this.userService.GetByID(Convert.ToInt32(ddlUser.SelectedValue));

            for (int i = 0; i < permissionList.Count; i++)
            {
                var folderObj = folderList.FirstOrDefault(t => t.ID == permissionList[i].FolderId);
                if (folderObj != null)
                {
                    firstrow++;
                    dataSheet.Cells.InsertRow(firstrow);
                    dataSheet.Cells["B" + firstrow].PutValue(folderObj.Name);
                    if (permissionList[i].IsFullPermission != null)
                    {
                        if (permissionList[i].IsFullPermission == false)
                        {
                            dataSheet.Cells["C" + firstrow].PutValue("READ");
                        }
                        else
                        {
                            dataSheet.Cells["C" + firstrow].PutValue("FULL PERMISSION");
                        }

                    }
                    if (permissionList[i].OnlyView != null)
                    {
                        dataSheet.Cells["C" + firstrow].PutValue("ONLY VIEW");

                    }
                }

            }

            dataSheet.Cells["B1"].PutValue(userObj.FullName);
            dataSheet.Cells["C1"].PutValue(userObj.Username);

            var filename = "Report_Permission " + DateTime.Now.ToString("ddMMyyyy") + ".xls";
            workbook.Save(filePath + filename);
            this.DownloadByWriteByte(filePath + filename, filename, true);
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentPackageDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class DocumentPackageDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentPackageDAO"/> class.
        /// </summary>
        public DocumentPackageDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<DocumentPackage> GetIQueryable()
        {
            return this.EDMsDataContext.DocumentPackages;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DocumentPackage> GetAll()
        {
            return this.EDMsDataContext.DocumentPackages.OrderByDescending(t => t.ID).ToList();
        }

        public DateTime? GetMaxDatedlineDocOfWorkpackage(int wpId)
        {
            return
               this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.WorkgroupId == wpId)
                   .Max(t => t.Deadline);
        }
        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public DocumentPackage GetById(int id)
        {
            return this.EDMsDataContext.DocumentPackages.FirstOrDefault(ob => ob.ID == id);
        }

        public List<DocumentPackage> GetAllByLeaf()
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf).ToList();
        }

        public List<DocumentPackage> GetAllByDepartment(int departmentId)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.DeparmentId == departmentId).ToList();
        }

        #endregion

        #region GET ADVANCE

        public List<DocumentPackage> GetByWorkpackages(int wpId)
        {
            return
                this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.WorkgroupId == wpId)
                    .ToList();
        }

        public List<DocumentPackage> GetAllRevDoc(int parentId)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && (t.ID == parentId || t.ParentId == parentId) && t.RevisionId != 0).OrderByDescending(t => t.RevisionId).ToList();
        }

        public List<DocumentPackage> GetAllDocListIncomplete()
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf).ToList();
        }

        public bool IsExistByDocNo(string docNo)
        {
            return this.EDMsDataContext.DocumentPackages.Any(t => t.IsLeaf && !t.IsDelete && t.DocNo == docNo);
        }
        public List<DocumentPackage> GetAllByWorkgroupHaveDocDelete(int workgroupId)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t =>t.WorkgroupId == workgroupId && t.IsEMDR).ToList();
        }
        public List<DocumentPackage> GetAllByWorkgroupComment(int workgroupId)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.WorkgroupId == workgroupId).ToList();
        }

        public List<DocumentPackage> GetAllByWorkgroupForEng(int workgroupId, string engId)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.WorkgroupId == workgroupId && t.EngineerId != null).ToList();
        }

        public List<DocumentPackage> GetAllByWorkgroupInPermission(List<int> listworkgroupId)
        {
            //var query = this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && listworkgroupId.Contains(t.WorkgroupId.Value));
            //string sql = ((System.Data.Objects.ObjectQuery)query).ToTraceString();
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && listworkgroupId.Contains(t.WorkgroupId.Value)).ToList();
        }

        public List<DocumentPackage> GetAllByWorkgroup(int workgroupId)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.WorkgroupId == workgroupId).ToList();
        }

        public List<DocumentPackage> GetAllEMDRByWorkgroup(int workgroupId)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.WorkgroupId == workgroupId && t.IsEMDR).ToList();
        }

        public List<DocumentPackage> GetAllDocProject(int project)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.ProjectId.Value == project).ToList();
        }

        public List<DocumentPackage> GetAllDocList(List<int> listdoc)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && listdoc.Contains(t.ID)).ToList();
        }

        public List<DocumentPackage> GetAllRelatedDocument(int docId)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => (t.ID == docId || t.ParentId == docId) && !t.IsDelete).ToList();
        }

        public DocumentPackage GetOneByDocNo(string docNo, string revName, int projectId)
        {
            return this.EDMsDataContext.DocumentPackages.FirstOrDefault(t => !t.IsDelete
                    && t.DocNo == docNo
                    && t.RevisionName == revName
                    && t.ProjectId == projectId);
        }

        public List<DocumentPackage> GetAllByDocNo(string docNo, string revName, int projectId)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete
                    && t.DocNo.Trim().Replace(" ", string.Empty).ToLower() == docNo.Trim().Replace(" ", string.Empty).ToLower()
                    && t.RevisionName == revName
                    && t.ProjectId == projectId).ToList();
        }
        //&& !t.DocNo.Contains("POC") && t.DocumentTypeId!=177
        public List<DocumentPackage> GetAllByRQSM(int projectID)
        {
            //List<string> list = new List<string>() { "RQ", "SM" };
            //var temp = this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.ProjectId == projectID && (t.DocNo.Contains("RQ") || t.DocNo.Contains("SM"))).ToList();
            //temp = temp.Where(t => !t.DocNo.Contains("POC") && t.DocumentTypeId != 177).ToList();
            var temp = this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.ProjectId == projectID && t.DocNo.Contains("RQ")).ToList();
            temp = temp.Where(t => !t.DocTitle.ToLower().Contains("top-up")).ToList();
            return temp;
        }

        public List<DocumentPackage> GetAllByWithoutPOC(int projectID)
        {
            var temp = this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.ProjectId == projectID && !t.DocNo.Contains("POC")).ToList();
            temp = temp.Where(t => !t.DocNo.Contains("GEN-PM-MDR-01") && !t.DocNo.Contains("GEN-PM-RPT-01") && !t.DocNo.Contains("GEN-GEN6-DC")).ToList();
            return temp;
        }

        public List<DocumentPackage> GetAllByWithPOC(int projectID)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && t.IsLeaf && t.ProjectId == projectID && (t.DocNo.Contains("POC") || t.DocNo.Contains("EC"))).ToList();
        }

        public List<DocumentPackage> SearchDocument(int projectId, int workgroupId, string docNo, string docTitle)
        {
            var temp = this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete && (projectId == t.ProjectId.Value));
            return temp.Where(t => (workgroupId == 0 || t.WorkgroupId.Value == workgroupId)
                && (string.IsNullOrEmpty(docNo) || t.DocNo.ToLower().Contains(docNo.ToLower()))
                && (string.IsNullOrEmpty(docTitle) || t.DocTitle.ToLower().Contains(docTitle.ToLower()))
                ).OrderBy(t => t.DocNo).ThenBy(t => t.RevisionName).ToList();
        }

        public bool IsExist(string documentNumber, int revID, int wpId)
        {
            return this.EDMsDataContext.DocumentPackages.Any(t =>
            !t.IsDelete
            && t.DocNo == documentNumber
            && (t.RevisionId == revID)
            && (t.WorkgroupId == wpId));
        }

        public bool IsExist(string documentNumber, int wpId)
        {
            return this.EDMsDataContext.DocumentPackages.Any(t =>
            !t.IsDelete
            && t.DocNo == documentNumber
            && (t.WorkgroupId == wpId));
        }

        public bool IsExist(string documentNumber, string revname, int wpId)
        {

            return this.EDMsDataContext.DocumentPackages.Any(t =>
                !t.IsDelete
                && t.DocNo == documentNumber
                && t.RevisionName == revname
                && (t.WorkgroupId == wpId));
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(DocumentPackage ob)
        {
            try
            {
                this.EDMsDataContext.AddToDocumentPackages(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(DocumentPackage src)
        {
            try
            {
                DocumentPackage des = (from rs in this.EDMsDataContext.DocumentPackages
                                       where rs.ID == src.ID
                                       select rs).First();

                des.DocNo = src.DocNo;
                des.DocTitle = src.DocTitle;
                des.WorkgroupId = src.WorkgroupId;
                des.WorkgroupName = src.WorkgroupName;
                des.WorkgroupFullName = src.WorkgroupFullName;
                des.WorkgroupRevName = src.WorkgroupRevName;

                des.DeparmentId = src.DeparmentId;
                des.DeparmentName = src.DeparmentName;
                des.StartDate = src.StartDate;
                des.PlanedDate = src.PlanedDate;
                des.RevisionId = src.RevisionId;
                des.RevisionName = src.RevisionName;
                des.RevisionActualDate = src.RevisionActualDate;
                des.RevisionPlanedDate = src.RevisionPlanedDate;
                des.RevisionCommentCode = src.RevisionCommentCode;
                des.Complete = src.Complete;
                des.Weight = src.Weight;
                des.OutgoingTransNo = src.OutgoingTransNo;
                des.OutgoingTransDate = src.OutgoingTransDate;
                des.IncomingTransDate = src.IncomingTransDate;
                des.IncomingTransNo = src.IncomingTransNo;
                des.ICAReviewCode = src.ICAReviewCode;
                des.ICAReviewOutTransNo = src.ICAReviewOutTransNo;
                des.ICAReviewOutDate = src.ICAReviewOutDate;
                des.ICAReviewInTransNo = src.ICAReviewInTransNo;
                des.ICAReviewInDate = src.ICAReviewInDate;
                des.ICAStatus = src.ICAStatus;
                des.VRMarkup = src.VRMarkup;
                des.DNVMarkup = src.DNVMarkup;
                des.MWSMarkup = src.MWSMarkup;
                des.Markup = src.Markup;
                des.Notes = src.Notes;
                des.DocumentTypeId = src.DocumentTypeId;
                des.DocumentTypeName = src.DocumentTypeName;
                des.DisciplineId = src.DisciplineId;
                des.DisciplineName = src.DisciplineName;
                des.PackageId = src.PackageId;
                des.PackageName = src.PackageName;
                des.ProjectId = src.ProjectId;
                des.ProjectName = src.ProjectName;
                des.IsLeaf = src.IsLeaf;
                des.IsDelete = src.IsDelete;
                des.IsEMDR = src.IsEMDR;
                des.IndexNumber = src.IndexNumber;

                des.Deadline = src.Deadline;
                des.EndDate = src.EndDate;
                des.EngineerIds = src.EngineerIds;
                des.EngineerName = src.EngineerName;
                des.IsoReviseDate = src.IsoReviseDate;
                des.ManHours = src.ManHours;
                des.EngineerId = src.EngineerId;
                des.LeaderId = src.LeaderId;
                des.LeaderName = src.LeaderName;
                des.UpdatedBy = src.UpdatedBy;
                des.UpdatedDate = src.UpdatedDate;
                des.HasAttachFile = src.HasAttachFile;

                des.ManHourPlan = src.ManHourPlan;
                des.LevelPriority = src.LevelPriority;
                des.PersonsInvolved = src.PersonsInvolved;
                des.StatusID = src.StatusID;
                des.StatusName = src.StatusName;
                des.StatusFullName = src.StatusFullName;
                des.FinalCodeName = src.FinalCodeName;
                des.FinalCodeID = src.FinalCodeID;
                des.DealineComment = src.DealineComment;
                des.ResponseDate = src.ResponseDate;
                des.ResponseDeadline = src.ResponseDeadline;
                des.ResponseTransNo = src.ResponseTransNo;
                des.IDCDeadline = src.IDCDeadline;
                des.IDCEndDate = src.IDCEndDate;
                des.IFRDeadline = src.IFRDeadline;
                des.IFREndDate = src.IFREndDate;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(DocumentPackage src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

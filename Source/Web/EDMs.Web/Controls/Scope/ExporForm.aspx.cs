﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Hosting;
using System.Web.UI;
using System.IO;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using OfficeHelper.Utilities.Data;
namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ExporForm : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;
        private readonly ScopeProjectService scopeProjectService;
        private readonly FieldService fieldService;
        private readonly PlatformService platformService;
        private readonly UserService userService;
        private readonly SupervisorService supervisorService;
        private readonly AttachFilesProjectService attachFilesProjectService;
        private readonly ContractorService contractorService;

        

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ExporForm()
        {
            this.documentService = new DocumentService();
            this.scopeProjectService = new ScopeProjectService();
            this.fieldService = new FieldService();
            this.platformService = new PlatformService();
            this.userService = new UserService();
            this.supervisorService = new SupervisorService();
            this.attachFilesProjectService = new AttachFilesProjectService();
            this.contractorService = new ContractorService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                var contractorList = this.contractorService.GetAll();
                contractorList.Insert(0, new Contractor() { Name = string.Empty });
                this.ddlContractor.DataSource = contractorList;
                this.ddlContractor.DataTextField = "Name";
                this.ddlContractor.DataValueField = "ID";
                this.ddlContractor.DataBind();
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
           
            try
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var contractor = this.contractorService.GetById(Convert.ToInt32(this.ddlContractor.SelectedValue));
                    var selectedProject = this.scopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                if (selectedProject != null)
                {
                   
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("Department", Type.GetType("System.String")),
                                new DataColumn("WPStartDate", Type.GetType("System.String")),
                                new DataColumn("WPEndDate", Type.GetType("System.String")),
                                new DataColumn("InputDataSupplier", Type.GetType("System.String")),
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("ProjectManager", Type.GetType("System.String")),
                                new DataColumn("DC", Type.GetType("System.String")),
                                new DataColumn("Number", Type.GetType("System.String")),
                                new DataColumn("Date", Type.GetType("System.String")),
                                new DataColumn("ProjectDescription", Type.GetType("System.String")),
                                new DataColumn("ProjectName", Type.GetType("System.String")),
                                new DataColumn("PlanName", Type.GetType("System.String")),
                                new DataColumn("ToName", Type.GetType("System.String")),
                                new DataColumn("ToNo", Type.GetType("System.String")),
                                new DataColumn("Version", Type.GetType("System.String")),
                                new DataColumn("TypePlanName", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var infoItem = reportInfo.NewRow();
                    var projectManager = this.userService.GetByID(selectedProject.ProjectManagerId.GetValueOrDefault());

                    infoItem["ProjectManager"] = projectManager != null ? projectManager.FullName : string.Empty;
                    infoItem["DC"] = UserSession.Current.User.FullName;
                    infoItem["Date"] = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    infoItem["Number"] = this.txtTransmittalNumber.Text;
                    infoItem["ProjectDescription"] = selectedProject.Description;
                    infoItem["ProjectName"] = selectedProject.Name;
                    infoItem["PlanName"] = selectedProject.planName;
                    infoItem["TypePlanName"] = selectedProject.TypePlanName;
                    infoItem["ToName"] = contractor.Description;
                    infoItem["ToNo"] = contractor.Name;
                    infoItem["Version"] = this.txtversion.Text ;
                    reportInfo.Rows.Add(infoItem);

                    

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("../../Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = "Formchuyentailieu.doc";
                    var strOutputFileName = "Formchuyentailieu_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                    //if (isSuccess)
                    //{
                    //    this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                    //    "Formchuyentailieu_" + Utility.RemoveSpecialCharacter(selectedProject.Name, "-") + ".doc", true);
                    //}
                    //cover
                   var  StrTemplateFileName1 = "FormofEMDRcover.doc";
                   var strOutputFileName1 = "FormofEMDRcover_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                     isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName1, strOutputFileName1, reportInfo, ds);
                    if (isSuccess)
                    {
                        //this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        //"FormofEMDRcover_" + Utility.RemoveSpecialCharacter(selectedProject.Name, "-") + ".doc", true);

                      
                        //var serverDocPackPath = Server.MapPath("~/Exports/DocPack/" + Utility.RemoveSpecialCharacter(selectedProject.Name, "-") + "_" + DateTime.Now.ToString("ddMMyyyhhmmss") + ".rar");
                        //var temp = ZipPackage.CreateFile(serverDocPackPath);
                        //var file = rootPath + WordPathExport + strOutputFileName;
                        //if (File.Exists(file))
                        //{
                        //     temp.Add(file);
                        //     File.SetAttributes(file, FileAttributes.Normal);
                        //     File.Delete(file);
                        //}
                       
                        //file = rootPath + WordPathExport + strOutputFileName1;
                        //if (File.Exists(file))
                        //{
                        //    temp.Add(file);
                        //    File.SetAttributes(file, FileAttributes.Normal);
                        //    File.Delete(file);
                        //}
                       // this.DownloadByWriteByte(serverDocPackPath, "Form_" + Utility.RemoveSpecialCharacter(selectedProject.Name, "-") + "_Package.rar", true);
                    }

                }}

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);

            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error: '" + ex.Message + "'";
            }
        }
        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if (this.txtTransmittalNumber.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter transmittal Record No.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }
        protected void ddlContractor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
            {
                var contractor = this.contractorService.GetById(Convert.ToInt32(this.ddlContractor.SelectedValue));
                var objScopeProject = this.scopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                if (objScopeProject != null)
                {
                    this.txtTransmittalNumber.Text = objScopeProject.Name + "-NIPI-" + contractor.Name + "-00";
                }
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                var fs = new FileStream(strFileName, FileMode.Open);
                var streamLength = Convert.ToInt32(fs.Length);
                var data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

    }
}
﻿using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using EDMs.Business.Services.Scope;


namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.ServiceProcess;
    using System.Text;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using CheckBox = System.Web.UI.WebControls.CheckBox;
    using Label = System.Web.UI.WebControls.Label;
    using TextBox = System.Web.UI.WebControls.TextBox;
    using OfficeHelper.Utilities.Data;

    public partial class ToDoList : System.Web.UI.Page
    {
        private readonly OptionalTypeService optionalTypeService = new OptionalTypeService();

        /// <summary>
        /// The permission service.
        /// </summary>
        private readonly PermissionService permissionService = new PermissionService();

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService = new RevisionService();

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        /// <summary>
        /// The folder service.
        /// </summary>
        //  private readonly FolderService folderService = new FolderService();

        private readonly DocumentNewService documentNewService = new DocumentNewService();

        private readonly NotificationRuleService notificationRuleService = new NotificationRuleService();

        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly UserService userService = new UserService();

        private readonly AttachFileService attachFileService = new AttachFileService();

        private readonly AttachFilesPackageService attachFilesPackageService = new AttachFilesPackageService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly PackageService packageService = new PackageService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly WorkGroupService workGroupService = new WorkGroupService();

        private readonly TemplateManagementService templateManagementService = new TemplateManagementService();

        private readonly EmailNotificationTemplateService emailNotificationTemplateService = new EmailNotificationTemplateService();

        private readonly RoleService roleservice = new RoleService();

        protected const string ServiceName = "EDMSFolderWatcher";

        public static RadTreeNode editedNode = null;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

        /// <summary>
        /// The list folder id.
        /// </summary>
        private List<int> listFolderId = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {

                Session.Add("IsListAll", false);
                // if (UserSession.Current.IsEngineer )
                // {
                //foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
                //{
                //    if (item.Value == "Adminfunc")
                //    {
                //        item.Visible = false;
                //    }
                //}

                this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                //this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
                //this.grdDocument.AllowMultiRowSelection = false;

                // }

                this.LoadObjectTree();

                var ddlShow = (DropDownList)this.CustomerMenu.Items[3].FindControl("ddlShow");
                ddlShow.SelectedValue = "ShowAll";
                #region 1
                //if (UserSession.Current.User.Role.IsAdmin.GetValueOrDefault())
                //{
                //    this.IsFullPermission.Value = "true";
                //}


                //this.LoadListPanel();
                //this.LoadSystemPanel();

                //var wpId = !string.IsNullOrEmpty(this.Request.QueryString["WpID"]) ? Convert.ToInt32(this.Request.QueryString["WpID"]) : 0;
                //if (wpId != 0)
                //{
                //    var wpcurrent = this.workGroupService.GetById(wpId);
                //    if (wpcurrent != null)
                //    {
                //        int indexpj = this.ddlProject.FindItemIndexByValue(wpcurrent.ProjectId.GetValueOrDefault().ToString());
                //        // this.ddlProject.SelectedValue = wpcurrent.ProjectId.GetValueOrDefault().ToString();
                //        this.ddlProject.SelectedIndex = indexpj;

                //        var wpList = new List<WorkGroup>();
                //        if (UserSession.Current.IsEngineer)
                //        {
                //            var wpInWorkOfEng =
                //                this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id)
                //                    .Select(t => t.WorkgroupId)
                //                    .Distinct();
                //            wpList = this.WorkpackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
                //        }
                //        else
                //        {
                //            wpList = this.WorkpackageList;
                //        }
                //        this.rtvWorkgroup.DataSource = wpList.Where(
                //                                    t =>
                //                                        t.ProjectId ==
                //                                        (this.ddlProject.SelectedItem != null
                //                                            ? Convert.ToInt32(this.ddlProject.SelectedValue)
                //                                            : 0));
                //        this.rtvWorkgroup.DataTextField = "FullNameRev";
                //        this.rtvWorkgroup.DataValueField = "ID";
                //        this.rtvWorkgroup.DataFieldID = "ID";
                //        this.rtvWorkgroup.DataBind();

                //        RadTreeNode node = (RadTreeNode)this.rtvWorkgroup.Nodes.FindNodeByValue(wpcurrent.ID.ToString());
                //        node.Selected = true;

                //    }
                //}
                #endregion
            }
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(bool isbind = false, bool isListAll = false)
        {
            var docList = new List<DocumentPackage>();
            var ddlShow = (DropDownList)this.CustomerMenu.Items[3].FindControl("ddlShow");
            if (this.rtvWorkgroup.SelectedNode != null)
            {
                //if (!UserSession.Current.IsEngineer)
                //  {
                //docList = this.documentPackageService.GetAllByWorkgroup(Convert.ToInt32(this.rtvWorkgroup.SelectedNode.Value)).
                //   Where(t => t.Complete < 100).OrderByDescending(t => t.CreatedDate).OrderByDescending(t => t.LevelPriority)
                //    .ToList();

                docList = this.documentPackageService.GetAllByWorkgroupForEng(Convert.ToInt32(this.rtvWorkgroup.SelectedNode.Value), UserSession.Current.User.Id.ToString())
                       .Where(t => t.Complete < 100 && !t.IsStop).OrderBy(t => t.DocNo).ThenByDescending(t => t.LevelPriority)
                       .ToList();
                var txtWorkgroupComplete = this.CustomerMenu.Items[3].FindControl("txtWorkgroupComplete") as RadNumericTextBox;
                var txtWorkgroupWeight = this.CustomerMenu.Items[3].FindControl("txtWorkgroupWeight") as RadNumericTextBox;

                if (txtWorkgroupComplete != null)
                {
                    double complete = 0;
                    complete = docList.Aggregate(complete, (current, t) => current + (t.Complete.GetValueOrDefault() * t.Weight.GetValueOrDefault()) / 100);
                    txtWorkgroupComplete.Value = complete;
                }

                if (txtWorkgroupWeight != null)
                {
                    double weight = 0;
                    weight = docList.Aggregate(weight, (current, t) => current + t.Weight.GetValueOrDefault());
                    txtWorkgroupWeight.Value = weight;
                }
                // }
                //  else
                // {

                //  this.CustomerMenu.Items[3].Visible = false;
                // }
            }
            else
            {
                docList = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Where(t => t.Complete < 100 && !t.IsStop).OrderBy(t => t.DocNo).ThenByDescending(t => t.LevelPriority).ToList();
            }

            if (ddlShow.SelectedValue == "ShowOverDue")
            {
                docList = docList.Where(t => t.Deadline.Value.Date < DateTime.Now.Date).ToList();
            }

            #region 2
            //else
            //{
            //    var listWorkgroupId = this.WorkpackageList.Where(
            //        t =>
            //            t.ProjectId ==
            //            (this.ddlProject.SelectedItem != null
            //                ? Convert.ToInt32(this.ddlProject.SelectedValue)
            //                : 0)).Select(t => t.ID).ToList();
            //    if (!UserSession.Current.IsEngineer)
            //    {
            //        docList = this.documentPackageService.GetAllByWorkgroupInPermission(listWorkgroupId)
            //            .OrderBy(t => t.DocNo)
            //            .ToList();
            //    }
            //    else
            //    {
            //        docList = this.documentPackageService.GetAllByWorkgroupInPermission(listWorkgroupId)
            //            .Where(t => t.EngineerId == UserSession.Current.User.Id)
            //            .OrderBy(t => t.DocNo)
            //            .ToList();
            //    }

            //    this.CustomerMenu.Items[3].Visible = false;
            //}
            #endregion
            this.grdDocument.DataSource = docList;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlShow_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }


        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "DeleteAllDoc")
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        if (docObj.ParentId == null)
                        {
                            docObj.IsDelete = true;
                            this.documentPackageService.Update(docObj);
                        }
                        else
                        {
                            var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                            if (listRelateDoc != null)
                            {
                                foreach (var objDoc in listRelateDoc)
                                {
                                    objDoc.IsDelete = true;
                                    this.documentPackageService.Update(objDoc);
                                }
                            }
                        }

                        if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                        {
                            this.NotificationDeleteDoc(docObj);
                        }
                    }
                }

                this.grdDocument.Rebind();
            }
            else if (e.Argument == "ExportEMDRReport")
            {
                //if (this.rtvWorkgroup.SelectedNode != null)
                //{
                //var selectedWP = this.workGroupService.GetById(Convert.ToInt32(this.rtvWorkgroup.SelectedNode.Value));
                var dtFull = new DataTable();
                dtFull.Columns.AddRange(new[]
                {
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("Rev", typeof(String)),
                        new DataColumn("Start", typeof(String)),
                        new DataColumn("IDCDeadline", typeof(String)),
                        new DataColumn("IFRDeadline", typeof(String)),
                        new DataColumn("Deadline", typeof(String)),
                        new DataColumn("Fisnish", typeof(String)),
                        new DataColumn("Weight", typeof(Double)),
                        new DataColumn("Complete", typeof(Double)),
                        new DataColumn("DocType", typeof(String)),
                        new DataColumn("ManHourPlan", typeof(Double)),
                        new DataColumn("ManHour", typeof(Double)),
                        new DataColumn("Department", typeof(String)),
                        new DataColumn("Leader", typeof(String)),
                        new DataColumn("Engineer", typeof(String)),
                        new DataColumn("PersonsInvolved", typeof(String)),
                        new DataColumn("OutgoingLeterNo", typeof(String)),
                        new DataColumn("OutgoingLeterDate", typeof(String)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("ICATrans",typeof(String)),
                        new DataColumn("ICADate",typeof(String)),
                        new DataColumn("ICACode",typeof(String)),
                        new DataColumn("Markup",typeof(String)),
                        new DataColumn("Note",typeof(String)),
                    });
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\CMDRReportTemplate_Report.xls");
                var sheets = workbook.Worksheets;
                var readMeSheet = sheets[2];
                sheets[0].Cells["E1"].PutValue("TO DO LIST of Engineer - " + UserSession.Current.User.Username);
                //sheets[0].Cells["C7"].PutValue(selectedWP.Name);
                //sheets[0].Cells["B7"].PutValue(selectedWP.ProjectId + "," + selectedWP.ID);
                sheets[0].Cells[3, 28].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                //var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                //if (projectObj.IsIDC.GetValueOrDefault() == true)
                //{
                //    sheets[0].Cells.Columns[7].IsHidden = false;
                //    sheets[0].Cells.Columns[8].IsHidden = false;
                //}
                //else
                //{
                //    sheets[0].Cells.Columns[7].IsHidden = true;
                //    sheets[0].Cells.Columns[8].IsHidden = true;
                //}
                var count = 1;
                //var docList = this.documentPackageService.GetAllByEngineerAndWP(UserSession.Current.User.Id, selectedWP.ID).Where(t => t.Complete < 100).OrderByDescending(t => t.CreatedDate).OrderByDescending(t => t.LevelPriority).ToList();
                var docList = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Where(t => t.Complete < 100 && !t.IsStop).OrderByDescending(t => t.CreatedDate).ToList();

                var wpInWorkOfEng = docList.Select(t => t.WorkgroupId.GetValueOrDefault()).Distinct().ToList();
                var wpList = this.workGroupService.GetAllByTodolist(wpInWorkOfEng).ToList();
                foreach (var wpItem in wpList)
                {
                    var dataRow = dtFull.NewRow();
                    dataRow["DocId"] = -1;
                    dataRow["NoIndex"] = wpItem != null ? wpItem.Name : string.Empty;
                    dtFull.Rows.Add(dataRow);
                    var listDocWP = docList.Where(t => t.WorkgroupId == wpItem.ID).ToList();
                    var listDocumentTypeId = listDocWP.Select(t => Convert.ToInt32(t.DocumentTypeId)).Distinct().OrderBy(t => t).ToList();
                    foreach (var documentTypeId in listDocumentTypeId)
                    {
                        var documentType = this.documentTypeService.GetById(documentTypeId);
                        dataRow = dtFull.NewRow();
                        dataRow["DocId"] = -1;
                        dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                        dtFull.Rows.Add(dataRow);
                        var listDocByDocType = listDocWP.Where(t => t.DocumentTypeId == documentTypeId).OrderBy(t => t.DocNo).ToList();
                        foreach (var documentPackage in listDocByDocType)
                        {
                            dataRow = dtFull.NewRow();
                            dataRow["DocId"] = documentPackage.ID;
                            dataRow["NoIndex"] = count;
                            dataRow["DocNo"] = documentPackage.DocNo;
                            dataRow["DocTitle"] = documentPackage.DocTitle;
                            dataRow["Rev"] = documentPackage.RevisionName;
                            dataRow["Start"] = documentPackage.StartDate != null
                                ? documentPackage.StartDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["IDCDeadline"] = documentPackage.IDCDeadline != null
                                ? documentPackage.IDCDeadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["IFRDeadline"] = documentPackage.IFRDeadline != null
                                ? documentPackage.IFRDeadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Deadline"] = documentPackage.Deadline != null
                                ? documentPackage.Deadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Fisnish"] = documentPackage.EndDate != null
                                ? documentPackage.EndDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Weight"] = documentPackage.Weight != null ? documentPackage.Weight / 100 : 0;
                            dataRow["Complete"] = documentPackage.Complete != null ? documentPackage.Complete / 100 : 0;
                            dataRow["DocType"] = documentPackage.DocumentTypeName;
                            dataRow["ManHourPlan"] = documentPackage.ManHourPlan != null ? documentPackage.ManHourPlan.GetValueOrDefault() : 0;
                            dataRow["ManHour"] = documentPackage.ManHours != null ? documentPackage.ManHours.GetValueOrDefault() : 0;
                            dataRow["Department"] = documentPackage.DeparmentName;
                            dataRow["Leader"] = documentPackage.LeaderName;
                            dataRow["Engineer"] = documentPackage.EngineerName;
                            dataRow["PersonsInvolved"] = documentPackage.PersonsInvolved;
                            dataRow["OutgoingLeterNo"] = documentPackage.OutgoingLeterNo;
                            dataRow["OutgoingLeterDate"] = documentPackage.OutgoingLeterDate != null
                                ? documentPackage.OutgoingLeterDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["OutgoingNo"] = documentPackage.OutgoingTransNo;
                            dataRow["OutgoingDate"] = documentPackage.OutgoingTransDate != null
                                ? documentPackage.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["ICATrans"] = documentPackage.ICAReviewOutTransNo;
                            dataRow["ICADate"] = documentPackage.ICAReviewOutDate != null
                                ? documentPackage.ICAReviewOutDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["ICACode"] = documentPackage.ICAReviewCode;
                            dataRow["Markup"] = documentPackage.Markup;
                            dataRow["Note"] = documentPackage.Notes;
                            count += 1;
                            dtFull.Rows.Add(dataRow);
                        }
                    }
                }
                if (count > 1)
                {
                    sheets[0].Cells["A7"].PutValue(dtFull.Rows.Count);
                    sheets[0].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, 28, true);
                    sheets[1].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, 28, true);
                    //sheets[0].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");
                    //sheets[0].Cells[7 + dtFull.Rows.Count, 10].Formula = "=SUMPRODUCT(J9:J" + (7 + dtFull.Rows.Count) + ",K9:K" + (7 + dtFull.Rows.Count) + ")";
                    //sheets[0].Cells[7 + dtFull.Rows.Count, 9].Formula = "=SUM(J9:J" + (7 + dtFull.Rows.Count) + ")";
                    sheets[0].Name = "TO DO LIST";
                    sheets[0].AutoFitRows();
                    sheets[1].IsVisible = false;
                    sheets[2].IsVisible = false;
                    var filename = "TO DO LIST of Engineer - " + UserSession.Current.User.Username + " - " +
                                          DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    filename = filename.Replace(@"\", " ").Replace("/", " ");
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument == "DownloadMulti")
            {
                //var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_DocPack.rar");
                //var docPack = ZipPackage.CreateFile(serverTotalDocPackPath);

                //foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                //{
                //    var cboxSelected = (CheckBox)item["IsSelected"].FindControl("IsSelected");
                //    if (cboxSelected.Checked)
                //    {
                //        var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                //        var name = (Label)item["Index1"].FindControl("lblName");
                //        var serverDocPackPath = Server.MapPath("~/Exports/DocPack/" + name.Text + "_" + DateTime.Now.ToString("ddMMyyyhhmmss") + ".rar");

                //        var attachFiles = this.attachFileService.GetAllByDocId(docId);

                //        var temp = ZipPackage.CreateFile(serverDocPackPath);

                //        foreach (var attachFile in attachFiles)
                //        {
                //            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                //            {
                //                temp.Add(Server.MapPath(attachFile.FilePath));
                //            }
                //        }

                //        docPack.Add(serverDocPackPath);
                //    }
                //}

                //this.DownloadByWriteByte(serverTotalDocPackPath, "DocumentPackage.rar", true);

            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "SendNotification")
            {
                var listDisciplineId = new List<int>();
                var listSelectedDoc = new List<Document>();
                var count = 0;
                foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                {
                    var cboxSelected = (CheckBox)item["IsSelected"].FindControl("IsSelected");
                    if (cboxSelected.Checked)
                    {
                        count += 1;
                        var docItem = new Document();
                        var disciplineId = item["DisciplineID"].Text != @"&nbsp;"
                                                     ? item["DisciplineID"].Text
                                                     : string.Empty;
                        if (!string.IsNullOrEmpty(disciplineId) && disciplineId != "0")
                        {
                            listDisciplineId.Add(Convert.ToInt32(disciplineId));

                            docItem.ID = count;
                            docItem.DocumentNumber = item["DocumentNumber"].Text != @"&nbsp;"
                                                     ? item["DocumentNumber"].Text
                                                     : string.Empty;
                            docItem.Title = item["Title"].Text != @"&nbsp;"
                                                         ? item["Title"].Text
                                                         : string.Empty;
                            docItem.RevisionName = item["Revision"].Text != @"&nbsp;"
                                                         ? item["Revision"].Text
                                                         : string.Empty;
                            docItem.FilePath = item["FilePath"].Text != @"&nbsp;"
                                                         ? item["FilePath"].Text
                                                         : string.Empty;
                            docItem.DisciplineID = Convert.ToInt32(disciplineId);
                            listSelectedDoc.Add(docItem);
                        }
                    }
                }

                listDisciplineId = listDisciplineId.Distinct().ToList();

                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials = new NetworkCredential(UserSession.Current.User.Email, "")
                };

                foreach (var disciplineId in listDisciplineId)
                {
                    var notificationRule = this.notificationRuleService.GetAllByDiscipline(disciplineId);

                    if (notificationRule != null)
                    {
                        var message = new MailMessage();
                        message.From = new MailAddress(UserSession.Current.User.Email, UserSession.Current.User.FullName);
                        message.Subject = "Test send notification from EDMs";
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = @"******<br/>
                                        Dear users,<br/><br/>

                                        Please be informed that the following documents are now available on the BDPOC Document Library System for your information.<br/><br/>

                                        <table border='1' cellspacing='0'>
	                                        <tr>
		                                        <th style='text-align:center; width:40px'>No.</th>
		                                        <th style='text-align:center; width:350px'>Document number</th>
		                                        <th style='text-align:center; width:350px'>Document title</th>
		                                        <th style='text-align:center; width:60px'>Revision</th>
	                                        </tr>";

                        if (!string.IsNullOrEmpty(notificationRule.ReceiverListId))
                        {
                            var listUserId = notificationRule.ReceiverListId.Split(';').Select(t => Convert.ToInt32(t)).ToList();
                            foreach (var userId in listUserId)
                            {
                                var user = this.userService.GetByID(userId);
                                if (user != null)
                                {
                                    message.To.Add(new MailAddress(user.Email));
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(notificationRule.ReceiveGroupId) && string.IsNullOrEmpty(notificationRule.ReceiverListId))
                        {
                            var listGroupId = notificationRule.ReceiveGroupId.Split(';').Select(t => Convert.ToInt32(t)).ToList();
                            var listUser = this.userService.GetSpecialListUser(listGroupId);
                            foreach (var user in listUser)
                            {
                                message.To.Add(new MailAddress(user.Email));
                            }
                        }

                        var subBody = string.Empty;
                        foreach (var document in listSelectedDoc)
                        {
                            var port = ConfigurationSettings.AppSettings.Get("DocLibPort");
                            if (document.DisciplineID == disciplineId)
                            {
                                subBody += @"<tr>
                                <td>" + document.ID + @"</td>
                                <td><a href='http://" + Server.MachineName + (!string.IsNullOrEmpty(port) ? ":" + port : string.Empty)
                                           + document.FilePath + "' download='" + document.DocumentNumber + "'>"
                                           + document.DocumentNumber + @"</a></td>
                                <td>"
                                           + document.Title + @"</td>
                                <td>"
                                           + document.RevisionName + @"</td>";
                            }
                        }
                        message.Body += subBody + @"</table>
                                        <br/><br/>
                                        Thanks and regards,<br/>
                                        ******";

                        smtpClient.Send(message);
                    }
                }
            }
            else if (e.Argument.Contains("FTK04"))
            {
                var st = e.Argument.Replace("FTK04_", string.Empty);
                List<string> listid = st.Split(',').ToList();

                var dt = new DataTable();
                var reportInfo = new DataTable();

                var ds = new DataSet();
                var listColumn = new[]
                        {
                                new DataColumn("STT", Type.GetType("System.String")),
                                new DataColumn("DocumentNo", Type.GetType("System.String")),
                                new DataColumn("Department", Type.GetType("System.String")),
                                new DataColumn("Manhour", Type.GetType("System.String"))
                            };
                dt.Columns.AddRange(listColumn);

                var listColumn1 = new[]
                        {
                                new DataColumn("Date", Type.GetType("System.String")),
                                new DataColumn("ProjectDescription", Type.GetType("System.String")),
                                new DataColumn("ProjectName", Type.GetType("System.String")),
                                new DataColumn("SumManhour", Type.GetType("System.String"))
                            };
                reportInfo.Columns.AddRange(listColumn1);

                int stt = 0;
                var projectID = 0;
                double sumManhour = 0;
                foreach (var selectedItem in listid)
                {
                    var docObj = this.documentPackageService.GetById(Convert.ToInt32(selectedItem));
                    var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());
                    var dr = dt.NewRow();
                    stt++;
                    dr["STT"] = stt;
                    dr["DocumentNo"] = docObj.DocNo + "_" + docObj.RevisionName;
                    dr["Department"] = wpObj.DepartmentName;
                    dr["Manhour"] = docObj.ManHours.GetValueOrDefault();
                    dt.Rows.Add(dr);
                    projectID = docObj.ProjectId.GetValueOrDefault();
                    sumManhour += docObj.ManHours.GetValueOrDefault();
                }
                var projectObj = this.scopeProjectService.GetById(projectID);
                var infoItem = reportInfo.NewRow();

                infoItem["Date"] = DateTime.Now.ToString("dd/MM/yyyy");
                infoItem["ProjectDescription"] = projectObj.Description;
                infoItem["ProjectName"] = projectObj.Name;
                infoItem["SumManhour"] = sumManhour;
                reportInfo.Rows.Add(infoItem);

                ds.Tables.Add(dt);
                ds.Tables[0].TableName = "Table";

                var rootPath = Server.MapPath("Exports") + @"\";
                const string WordPath = @"Template\";
                const string WordPathExport = @"Generated\";
                var StrTemplateFileName = "F-TK-04_Viet_Rev2.doc";
                var strOutputFileName = "F-TK-04_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                var isSuccess = OfficeCommon.ExportToWordWithRegion(
                    rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                if (isSuccess)
                {
                    this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                    "F-TK-04_" + Utility.RemoveSpecialCharacter(projectObj.Name, "-") + ".doc", true);
                }
            }
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(false, isListAll);
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentPackageService.GetById(docId);
            if (docObj != null)
            {
                if (docObj.ParentId == null)
                {
                    docObj.IsDelete = true;
                    this.documentPackageService.Update(docObj);
                }
                else
                {
                    var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                    if (listRelateDoc != null)
                    {
                        foreach (var objDoc in listRelateDoc)
                        {
                            objDoc.IsDelete = true;
                            this.documentPackageService.Update(objDoc);
                        }
                    }
                }

                var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());
                if (wpObj != null)
                {
                    var docList = this.documentPackageService.GetAllByWorkgroup(wpObj.ID);
                    if (docList.Count == 0)
                    {
                        wpObj.CanDelete = true;
                        this.workGroupService.Update(wpObj);
                    }
                }

                if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                {
                    this.NotificationDeleteDoc(docObj);
                }
            }
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid")
            {

            }
            if (e.CommandName == RadGrid.RebindGridCommandName)
            {
                // this.CustomerMenu.Items[3].Visible = false;
                this.rtvWorkgroup.UnselectAllNodes();
                this.grdDocument.Rebind();
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {

            }
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                int projectID = Convert.ToInt32(item["ProjectId"].Text);
                var projectObj = this.scopeProjectService.GetById(projectID);
                if (item["HasAttachFile"].Text == "True")
                {
                    if (projectObj != null)
                    {
                        if (projectObj.IsIDC.GetValueOrDefault())
                        {
                            if (item["StatusID"].Text == "8")
                            {
                                item.BackColor = Color.Aqua;
                                item.BorderColor = Color.Aqua;
                            }
                            else if (item["StatusID"].Text == "7")
                            {
                                item.BackColor = Color.LightGreen;
                                item.BorderColor = Color.LightGreen;


                            }
                            else if (item["StatusID"].Text == "6")
                            {
                                item.BackColor = Color.LightBlue;
                                item.BorderColor = Color.LightBlue;
                            }
                        }
                        else
                        {
                            item.BackColor = Color.Aqua;
                            item.BorderColor = Color.Aqua;
                        }

                    }
                }
                try
                {
                    var ddlShow = (DropDownList)this.CustomerMenu.Items[3].FindControl("ddlShow");
                    string value = item["LevelPriority"].Text;


                    var deadline = new DateTime();
                    Utility.ConvertStringToDateTimeddMMyyyy(item["Deadline"].Text, ref deadline);
                    var today = DateTime.Now.Date;
                    if ((deadline.Date < today) && (ddlShow.SelectedValue == "ShowAll"))
                    {
                        item.BackColor = Color.DarkOrange;
                        item.BorderColor = Color.DarkOrange;
                    }
                    if (value == "3")
                    {
                        item["LevelPriority"].BackColor = Color.Red;
                        item["LevelPriority"].BorderColor = Color.Red;
                        item["LevelPriority"].ForeColor = Color.Red;
                    }
                    else if (value == "2")
                    {
                        item["LevelPriority"].BackColor = Color.Yellow;
                        item["LevelPriority"].BorderColor = Color.Yellow;
                        item["LevelPriority"].ForeColor = Color.Yellow;
                    }
                    else if (value == "1")
                    {
                        item["LevelPriority"].BackColor = Color.Aqua;
                        item["LevelPriority"].BorderColor = Color.Aqua;
                        item["LevelPriority"].ForeColor = Color.Aqua;

                    }
                }
                catch { }
            }
            #region
            ////if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            ////{
            ////    var item = e.Item as GridEditableItem;
            ////    var lblWorkgroupName = item.FindControl("lblWorkgroupName") as Label;
            ////    var lbldocNo = item.FindControl("lbldocNo") as Label;
            ////    var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
            ////    var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
            ////    var txtManHours = item.FindControl("txtManHours") as RadNumericTextBox;
            ////    var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
            ////    var txtDeadline = item.FindControl("txtDeadline") as RadDatePicker;
            ////    var txtEndDate = item.FindControl("txtEndDate") as RadDatePicker;
            ////    var txtIsoReviseDate = item.FindControl("txtIsoReviseDate") as RadDatePicker;
            ////    var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
            ////    var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;
            ////    var listRevision = this.revisionService.GetAll();
            ////    listRevision.Insert(0, new Revision() {ID = 0});
            ////    if (ddlRevision != null)
            ////    {
            ////        ddlRevision.DataSource = listRevision;
            ////        ddlRevision.DataTextField = "Name";
            ////        ddlRevision.DataValueField = "ID";
            ////        ddlRevision.DataBind();
            ////    }

            ////    if (txtStartDate != null)
            ////    {
            ////        txtStartDate.DatePopupButton.Visible = false;
            ////    }

            ////    if (txtDeadline != null)
            ////    {
            ////        txtDeadline.DatePopupButton.Visible = false;
            ////    }

            ////    if (txtEndDate != null)
            ////    {
            ////        txtEndDate.DatePopupButton.Visible = false;
            ////    }

            ////    if (txtIsoReviseDate != null)
            ////    {
            ////        txtIsoReviseDate.DatePopupButton.Visible = false;
            ////    }

            ////    var WorkgroupName = (item.FindControl("WorkgroupName") as HiddenField).Value;
            ////    var DocNo = (item.FindControl("DocNo") as HiddenField).Value;
            ////    var DocTitle = (item.FindControl("DocTitle") as HiddenField).Value;
            ////    var RevisionId = (item.FindControl("RevisionId") as HiddenField).Value;
            ////    var ManHours = (item.FindControl("ManHours") as HiddenField).Value;
            ////    var StartDate = (item.FindControl("StartDate") as HiddenField).Value;
            ////    var Deadline = (item.FindControl("Deadline") as HiddenField).Value;
            ////    var EndDate = (item.FindControl("EndDate") as HiddenField).Value;
            ////    var IsoReviseDate = (item.FindControl("IsoReviseDate") as HiddenField).Value;
            ////    var complete = (item.FindControl("Complete") as HiddenField).Value;
            ////    var weight = (item.FindControl("Weight") as HiddenField).Value;

            ////    if (!string.IsNullOrEmpty(StartDate))
            ////    {
            ////        txtStartDate.SelectedDate = Convert.ToDateTime(StartDate);
            ////    }

            ////    if (!string.IsNullOrEmpty(Deadline))
            ////    {
            ////        txtDeadline.SelectedDate = Convert.ToDateTime(Deadline);
            ////    }

            ////    if (!string.IsNullOrEmpty(EndDate))
            ////    {
            ////        txtEndDate.SelectedDate = Convert.ToDateTime(EndDate);
            ////    }

            ////    if (!string.IsNullOrEmpty(IsoReviseDate))
            ////    {
            ////        txtIsoReviseDate.SelectedDate = Convert.ToDateTime(IsoReviseDate);
            ////    }

            ////    lblWorkgroupName.Text = WorkgroupName;
            ////    lbldocNo.Text = DocNo;
            ////    txtDocTitle.Text = DocTitle;


            ////    ddlRevision.SelectedValue = RevisionId;
            ////    txtComplete.Value = Convert.ToDouble(complete);
            ////    txtWeight.Value = Convert.ToDouble(weight);
            ////    txtManHours.Value = Convert.ToDouble(ManHours);
            ////}
            #endregion
        }

        protected void ckbEnableFilter_OnCheckedChanged(object sender, EventArgs e)
        {
            this.grdDocument.AllowFilteringByColumn = ((CheckBox)sender).Checked;
            this.grdDocument.Rebind();
        }

        protected void radTreeFolder_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "Images/folderdir16.png";
        }

        /// <summary>
        /// The get all child folder id.
        /// </summary>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<int> GetAllChildFolderId(int parentId, List<int> folderPermission)
        {
            if (!this.listFolderId.Contains(parentId))
            {
                this.listFolderId.Add(parentId);
            }
            return this.listFolderId;
        }

        /// <summary>
        /// The custom folder tree.
        /// </summary>
        /// <param name="radTreeView">
        /// The rad tree view.
        /// </param>
        private void CustomFolderTree(RadTreeNode radTreeView)
        {
            foreach (var node in radTreeView.Nodes)
            {
                var nodetemp = (RadTreeNode)node;
                if (nodetemp.Nodes.Count > 0)
                {
                    this.CustomFolderTree(nodetemp);
                }

                nodetemp.ImageUrl = "Images/folderdir16.png";
            }
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }
                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }
                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });
            }
        }

        private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = Aspose.Cells.ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;
            validation.ShowInput = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";
            validation.InputTitle = "Warning";

            // Set the error message.
            validation.ErrorMessage = "Please select item from the list";
            validation.InputMessage = "Please select item from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void LoadObjectTree()
        {
            var wpList = new List<WorkGroup>();
            var docList = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id, UserSession.Current.RoleId).Where(t => t.Complete < 100);
            var wpInWorkOfEng = docList.Select(t => t.WorkgroupId.GetValueOrDefault()).Distinct().ToList();
            wpList = this.workGroupService.GetAllByTodolist(wpInWorkOfEng).ToList();
            //var listwp = docList.Where(t => t.Complete < 100).Select(t => (int)t.WorkgroupId.GetValueOrDefault()).Distinct().ToList();
            this.rtvWorkgroup.DataSource = wpList;
            this.rtvWorkgroup.DataTextField = "FullNameRev";
            this.rtvWorkgroup.DataValueField = "ID";
            this.rtvWorkgroup.DataFieldID = "ID";
            this.rtvWorkgroup.DataBind();
        }

        /// <summary>
        /// The repair list.
        /// </summary>
        /// <param name="listOptionalTypeDetail">
        /// The list optional type detail.
        /// </param>
        private void RepairList(ref List<OptionalTypeDetail> listOptionalTypeDetail)
        {
            var temp = listOptionalTypeDetail.Where(t => t.ParentId != null).Select(t => t.ParentId).Distinct().ToList();
            var temp2 = listOptionalTypeDetail.Select(t => t.ID).ToList();
            var tempList = new List<OptionalTypeDetail>();
            foreach (var x in temp)
            {
                if (!temp2.Contains(x.Value))
                {
                    tempList.AddRange(listOptionalTypeDetail.Where(t => t.ParentId == x.Value).ToList());
                }
            }

            var listOptionalType = tempList.Where(t => t.OptionalTypeId != null).Select(t => t.OptionalTypeId).Distinct().ToList();

            foreach (var optionalTypeId in listOptionalType)
            {
                var optionalType = this.optionalTypeService.GetById(optionalTypeId.Value);
                var tempOptTypeDetail = new OptionalTypeDetail() { ID = optionalType.ID * 9898, Name = optionalType.Name + "s" };
                listOptionalTypeDetail.Add(tempOptTypeDetail);
                ////tempList.Add(tempOptTypeDetail);
                OptionalType type = optionalType;
                foreach (var optionalTypeDetail in tempList.Where(t => t.OptionalTypeId == type.ID).ToList())
                {
                    optionalTypeDetail.ParentId = tempOptTypeDetail.ID;
                }
            }
        }

        //protected void ddlProject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        //{
        //    this.CustomerMenu.Items[3].Visible = false;

        //    var wpList = new List<WorkGroup>();
        //    if (UserSession.Current.IsEngineer)
        //    {
        //        var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
        //        wpList = this.WorkpackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
        //    }
        //    else
        //    {
        //        wpList = this.WorkpackageList;
        //    }

        //    this.rtvWorkgroup.DataSource = wpList.Where(
        //                                        t =>
        //                                            t.ProjectId ==
        //                                            (this.ddlProject.SelectedItem != null
        //                                                ? Convert.ToInt32(this.ddlProject.SelectedValue)
        //                                                : 0));
        //    this.rtvWorkgroup.DataTextField = "FullNameRev";
        //    this.rtvWorkgroup.DataValueField = "ID";
        //    this.rtvWorkgroup.DataFieldID = "ID";
        //    this.rtvWorkgroup.DataBind();

        //    this.grdDocument.Rebind();
        //}

        protected void grdDocument_Init(object sender, EventArgs e)
        {
        }

        protected void grdDocument_DataBound(object sender, EventArgs e)
        {
        }

        protected void rtvWorkgroup_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            ////if (UserSession.Current.User.Role.IsAdmin.GetValueOrDefault() || this.permissionWorkgroupService.IsFullPermission(UserSession.Current.User.Id,
            ////    Convert.ToInt32(e.Node.Value)))
            ////{
            ////    this.CustomerMenu.Items[0].Visible = true;
            ////    this.CustomerMenu.Items[1].Visible = true;
            ////    foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
            ////    {
            ////        if (item.Value == "Adminfunc")
            ////        {
            ////            item.Visible = true;
            ////        }
            ////    }
            ////    this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = true;
            ////    this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = true;
            ////    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = true;

            ////    this.IsFullPermission.Value = "true";
            ////}
            ////else
            ////{
            ////    this.CustomerMenu.Items[0].Visible = false;
            ////    this.CustomerMenu.Items[1].Visible = false;
            ////    foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
            ////    {
            ////        if (item.Value == "Adminfunc")
            ////        {
            ////            item.Visible = false;
            ////        }
            ////    }

            ////    this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
            ////    this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
            ////    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;

            ////    this.IsFullPermission.Value = "false";
            ////}
            //try
            //{
            // this.CustomerMenu.Items[3].Visible = true;

            // var txtWorkgroupComplete = this.CustomerMenu.Items[3].FindControl("txtWorkgroupComplete") as RadNumericTextBox;
            ///var txtWorkgroupWeight = this.CustomerMenu.Items[3].FindControl("txtWorkgroupWeight") as RadNumericTextBox;

            var workgroup = this.workGroupService.GetById(Convert.ToInt32(e.Node.Value));
            //if (workgroup != null)
            //{
            //    if (txtWorkgroupComplete != null)
            //    {
            //        txtWorkgroupComplete.Value = workgroup.Complete;
            //    }

            //    if (txtWorkgroupWeight != null)
            //    {
            //        txtWorkgroupWeight.Value = workgroup.Weight;
            //    }
            //}
            this.lblProjectId.Value = workgroup.ProjectId.Value.ToString();
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
            //}
            //catch(Exception ex) { }
        }

        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {
                var filterItem = (GridFilteringItem)e.Item;
                var selectedProperty = new List<string>();

                var ddlFilterRev = (RadComboBox)filterItem.FindControl("ddlFilterRev");
            }
        }

        protected DateTime? SetPublishDate(GridItem item)
        {
            if (item.OwnerTableView.GetColumn("Index27").CurrentFilterValue == string.Empty)
            {
                return new DateTime?();
            }
            else
            {
                return DateTime.Parse(item.OwnerTableView.GetColumn("Index27").CurrentFilterValue);
            }
        }

        /// <summary>
        /// The bind tree view combobox.
        /// </summary>
        /// <param name="optionalType">
        /// The optional type.
        /// </param>
        /// <param name="ddlObj">
        /// The ddl obj.
        /// </param>
        /// <param name="rtvName">
        /// The rtv name.
        /// </param>
        /// <param name="listOptionalTypeDetailFull">
        /// The list optional type detail full.
        /// </param>
        private void BindTreeViewCombobox(int optionalType, RadComboBox ddlObj, string rtvName, IEnumerable<OptionalTypeDetail> listOptionalTypeDetailFull)
        {
            var rtvobj = (RadTreeView)ddlObj.Items[0].FindControl(rtvName);
            if (rtvobj != null)
            {
                var listOptionalTypeDetail = listOptionalTypeDetailFull.Where(t => t.OptionalTypeId == optionalType).ToList();
                this.RepairList(ref listOptionalTypeDetail);

                rtvobj.DataSource = listOptionalTypeDetail;
                rtvobj.DataFieldParentID = "ParentId";
                rtvobj.DataTextField = "Name";
                rtvobj.DataValueField = "ID";
                rtvobj.DataFieldID = "ID";
                rtvobj.DataBind();
            }
        }

        protected void rtvWorkgroup_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/package.png";
        }

        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"Images/project.png";
        }

        private void NotificationDeleteDoc(DocumentPackage docObj)
        {
            if (docObj.EngineerId != "0")
            {
                var engineer = this.userService.GetByID(docObj.EngineerId);
                var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());

                var emailList = this.userService.GetAllByRoleId(wpObj != null ? wpObj.DepartmentId.GetValueOrDefault() : 0)
                    .Where(t => t.IsChief.GetValueOrDefault())
                    .Select(t => t.Email)
                    .Where(t => !string.IsNullOrEmpty(t)).ToList();

                if (engineer != null)
                {
                    if (!string.IsNullOrEmpty(engineer.Email))
                    {
                        emailList.Add(engineer.Email);
                    }
                    var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.DeleteDoc);
                    if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                    {
                        var smtpClient = new SmtpClient
                        {
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                            EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                            Host = ConfigurationManager.AppSettings["Host"],
                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                            Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                        };
                        var deletedUser = this.userService.GetByID(UserSession.Current.User.Id);
                        var subject = notificationTemplate.Subject.Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#DeletedUser#", deletedUser != null ? deletedUser.FullName : string.Empty);

                        var message = new MailMessage();
                        message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                        message.Subject = subject;
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = notificationTemplate.Contents
                            .Replace("#DeletedUser#", deletedUser != null ? deletedUser.FullName : string.Empty)
                            .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                            .Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#DocTitle#", docObj.DocTitle)
                            .Replace("#DocType#", docObj.DocumentTypeName)
                            .Replace("#DocRev#", docObj.RevisionName)
                            .Replace("#DocEng#", engineer.FullName)
                            .Replace("#DocStartDate#", docObj.StartDate != null ? docObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocDeadline#", docObj.Deadline != null ? docObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocFinishDate#", docObj.EndDate != null ? docObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocISODate#", docObj.IsoReviseDate != null ? docObj.IsoReviseDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocWeight#", docObj.Weight != null ? docObj.Weight.Value.ToString() : string.Empty)
                            .Replace("#DocComplete#", docObj.Complete != null ? docObj.Complete.Value.ToString() : string.Empty);

                        //if (ConfigurationManager.AppSettings["SendOnlySupport"] == "True")
                        //{
                        //    message.To.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                        //}
                        //else
                        //{
                        foreach (var to in emailList)
                        {
                            message.To.Add(new MailAddress(to));
                        }
                        //}
                        smtpClient.Send(message);
                    }
                }
            }
        }
    }
}
﻿
using System;
using EDMs.Data.DAO.Security;
using EDMs.Data.DAO.Library;

namespace EDMs.Data.Entities
{
    public partial class ScopeProject
    {
        private readonly ProjectAppendixDAO projectAppendixDAO = new ProjectAppendixDAO();
        ////private readonly UserDAO _userDAO = new UserDAO();

        ////public User ProjectManager
        ////{
        ////    get
        ////    {
        ////        return _userDAO.GetByID(this.ProjectManagerId.GetValueOrDefault());
        ////    }
        ////}

        ////public User Supervisor
        ////{
        ////    get
        ////    {
        ////        return _userDAO.GetByID(this.SupervisorId.GetValueOrDefault());
        ////    }
        ////}

        public string FullName
        {
            get
            {
                return !string.IsNullOrEmpty(this.Description)
                        ? this.Name + " (" + this.Description + ")"
                        : this.Name;
            }
        }

        public bool IsLater
        {
            get { return this.Deadline != null && this.Deadline < DateTime.Now && this.Complete < 100; }
        }

        public bool IsFullPermission { get; set; }

        public int WorkId
        {
            get
            {
                var obj = projectAppendixDAO.GetByProject(this.ID);
                if (obj != null)
                {
                    return obj.WorkID.GetValueOrDefault();
                }
                else
                {
                    return 0;
                }
            }

            //public bool IsUsing
            //{
            //    get
            //    {
            //        return wpDAO.GetByProject(this.ID).Any();
            //    }
            //}

            //public double TotalWorkpackageWeight
            //{
            //    get
            //    {
            //        var totalWeight = 0.0;
            //        totalWeight = wpDAO.GetByProject(this.ID).Aggregate(totalWeight, (current, t) => current + t.Weight.GetValueOrDefault());
            //        return totalWeight;
            //    }
            //}
        }
    }
}

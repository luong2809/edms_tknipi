﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;
    using System.Globalization;

    using Telerik.Web.UI;
    using Business.Services.Function;
    using Function;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportEMDRUpdate : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly PackageService packageService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly WorkGroupService workGroupService;

        private readonly EmailNotificationTemplateService emailNotificationTemplateService;
        private readonly UserService userService;
        private readonly MilestoneService milestoneService;
        private readonly ProcessActualService processActualService = new ProcessActualService();
        private readonly UserManhourService userManhourService = new UserManhourService();
        private readonly UserManhourMonthService userManhourMonthService = new UserManhourMonthService();
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();
        private readonly ProcessBussiness processBussiness = new ProcessBussiness();
        private readonly StatusService statusService = new StatusService();
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportEMDRUpdate()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.documentService = new DocumentService();
            this.packageService = new PackageService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.emailNotificationTemplateService = new EmailNotificationTemplateService();
            this.userService = new UserService();
            this.milestoneService = new MilestoneService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {

                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentFileName = string.Empty;
            var currentDocumentNo = string.Empty;
            var documentPackageList = new List<DocumentPackage>();

            DataTable manhours = new DataTable();
            manhours.Columns.AddRange(new[]{
                new DataColumn ("DocID", typeof(Int32)),
                new DataColumn ("DocNo", typeof(String)),
                new DataColumn ("Total", typeof(Double)),
                new DataColumn ("EngId", typeof(Int32)),
                new DataColumn ("EngName", typeof(String)),
                 new DataColumn ("PercentTotal", typeof(Double)),
            });

            //try
            //{
            foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
            {
                currentFileName = docFile.FileName;
                var extension = docFile.GetExtension();
                if (extension == ".xls" || extension == ".xlsx")
                {
                    var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                     "_" + docFile.FileName;
                    docFile.SaveAs(importPath);
                    // Instantiate a new workbook
                    var workbook = new Workbook();
                    workbook.Open(importPath);
                    var wsData = workbook.Worksheets[0];
                    //    var wsNewData = workbook.Worksheets["Add New Documents"];
                    var projectId = string.Empty;
                    var workgroupId = string.Empty;

                    // Create a datatable
                    var totalDataTable = new DataTable();
                    var dataTable = new DataTable();

                    // var insertNewDoc = new DataTable();
                    // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class	
                    totalDataTable = wsData.Cells.ExportDataTable(6, 0, wsData.Cells.MaxRow, 29);
                    var haveEdit = totalDataTable.AsEnumerable().Any(t => t["Column1"].ToString().ToLower() == "1" || t["Column1"].ToString().ToLower() == "true");
                    dataTable = haveEdit
                        ? totalDataTable.AsEnumerable()
                            .Where(t => t["Column1"].ToString().ToLower() == "1" || t["Column1"].ToString().ToLower() == "true")
                            .CopyToDataTable()
                        : new DataTable();

                    //   insertNewDoc = wsNewData.Cells.ExportDataTable(6, 0, wsNewData.Cells.MaxRow, 23);
                    if (totalDataTable.Rows.Count > 0)
                    {
                        projectId = totalDataTable.Rows[0][1].ToString().Split(',')[0];
                        workgroupId = totalDataTable.Rows[0][1].ToString().Split(',')[1];
                    }
                    var currentWP = string.Empty;
                    var project = this.scopeProjectService.GetById(Convert.ToInt32(projectId));
                    var wpObj = this.workGroupService.GetById(Convert.ToInt32(workgroupId));
                    var strError = "Have error: </br>";
                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        if (!string.IsNullOrEmpty(dataRow["Column2"].ToString()) && dataRow["Column2"].ToString() != "-1")
                        {
                            currentDocumentNo = dataRow["Column4"].ToString();
                            currentWP = wpObj.Name;
                            var documentId = Convert.ToInt32(dataRow["Column2"].ToString());
                            var documentObj = this.documentPackageService.GetById(documentId);
                            var rev = this.revisionService.GetByName(dataRow["Column6"].ToString());
                            if (documentObj != null)
                            {
                                var TypeDoc = dataRow["Column14"].ToString().Trim();
                                TypeDoc = TypeDoc.Contains('-') ? TypeDoc.Substring(0, TypeDoc.IndexOf('-') - 1) : TypeDoc;
                                var documentType =
                                 this.documentTypeService.GetByName(TypeDoc);

                                var role = this.roleService.GetByName(dataRow["Column17"].ToString().Trim());

                                var exist = documentPackageService.IsExist(documentObj.DocNo, rev.ID, wpObj.ID);
                                if (UserSession.Current.User.IsAdmin.GetValueOrDefault() || UserSession.Current.User.IsGip.GetValueOrDefault() || UserSession.Current.User.IsDC.GetValueOrDefault() && rev != null && (documentObj.RevisionId != rev.ID) && !exist)
                                {
                                    if (documentObj.HasAttachFile == true)
                                    {
                                        var docObj = new DocumentPackage();

                                        docObj.WorkgroupId = wpObj != null ? wpObj.ID : 0;
                                        docObj.WorkgroupName = wpObj != null ? wpObj.Name : string.Empty;
                                        docObj.WorkgroupFullName = wpObj != null ? wpObj.FullName : string.Empty;
                                        docObj.WorkgroupRevName = wpObj != null ? wpObj.FullNameRev : string.Empty;

                                        docObj.ProjectId = wpObj != null ? wpObj.ProjectId : 0;
                                        docObj.ProjectName = wpObj != null ? wpObj.ProjectName : string.Empty;
                                        docObj.DocNo = documentObj.DocNo;
                                        docObj.RevisionId = rev.ID;
                                        docObj.RevisionName = rev.Name;
                                        docObj.StatusID = rev.StatusID;
                                        docObj.StatusName = rev.StatusName;
                                        docObj.IsoReviseDate = documentObj.IsoReviseDate;
                                        docObj.IsEMDR = true;
                                        docObj.CreatedBy = UserSession.Current.User.Id;
                                        docObj.CreatedDate = DateTime.Now;
                                        docObj.IsLeaf = true;
                                        docObj.IsDelete = false;
                                        docObj.ParentId = documentObj.ParentId != null ? documentObj.ParentId : documentId;
                                        docObj.HasAttachFile = false;

                                        UpdateInfo(currentDocumentNo, currentWP, ref strError, dataRow, docObj, documentType, role, wpObj);

                                        //docObj.Complete = 0;

                                        // Upate old doc
                                        documentObj.IsLeaf = false;
                                        this.documentPackageService.Update(documentObj);

                                        this.documentPackageService.Insert(docObj);
                                        //}
                                        //else
                                        //{
                                        //    strError += "Please export a new file to update the data";
                                        //}
                                    }
                                }
                                else
                                {
                                    documentObj.HaveWpAutoCalculate = wpObj.IsAutoCalculate.GetValueOrDefault();
                                    UpdateInfo(currentDocumentNo, currentWP, ref strError, dataRow, documentObj, documentType, role, wpObj);

                                    documentObj.OutgoingLeterNo = dataRow["Column21"].ToString();
                                    var strLeterDate = dataRow["Column22"].ToString();
                                    var LeterDate = new DateTime();
                                    if (Utility.ConvertStringToDateTimeddMMyyyy(strLeterDate, ref LeterDate))
                                    {
                                        documentObj.OutgoingLeterDate = LeterDate;
                                    }
                                    else
                                    {
                                        documentObj.OutgoingLeterDate = null;
                                    }
                                    documentObj.OutgoingTransNo = dataRow["Column23"].ToString();
                                    var OutDate = dataRow["Column24"].ToString();
                                    var OutTransDate = new DateTime();
                                    if (Utility.ConvertStringToDateTimeddMMyyyy(OutDate, ref OutTransDate))
                                    {
                                        documentObj.OutgoingTransDate = OutTransDate;
                                    }
                                    else
                                    {
                                        documentObj.OutgoingTransDate = null;
                                    }
                                    documentObj.ICAReviewOutTransNo = dataRow["Column25"].ToString();
                                    var strICADate = dataRow["Column26"].ToString();
                                    var ICADate = new DateTime();
                                    if (Utility.ConvertStringToDateTimeddMMyyyy(strICADate, ref ICADate))
                                    {
                                        documentObj.ICAReviewOutDate = ICADate;
                                    }
                                    else
                                    {
                                        documentObj.ICAReviewOutDate = null;
                                    }
                                    documentObj.ICAReviewCode = dataRow["Column27"].ToString();
                                    documentObj.Markup = dataRow["Column28"].ToString();
                                    documentObj.UpdatedBy = UserSession.Current.User.Id;
                                    documentObj.UpdatedDate = DateTime.Now;
                                    this.documentPackageService.Update(documentObj);

                                    UserManhour userManhourObj = new UserManhour();
                                    UserManhourMonth userManhourMonthObj = new UserManhourMonth();
                                    //update manhour
                                    UpdateUserManhourWeek(documentObj, UserSession.Current.User, userManhourObj);
                                    UpdateUserManhourMonth(documentObj, UserSession.Current.User, userManhourMonthObj);
                                }
                                if (strError == "Have error: </br>")
                                {
                                    var en = this.userService.GetByID(UserSession.Current.User.Id);
                                    // Send Notification update
                                    if (ConfigurationManager.AppSettings["EnableSendNotification"].ToLower() != "false" && en != null && en.Role.Notify.GetValueOrDefault())
                                    {
                                        this.NotificationUpdateDoc(documentObj);
                                    }

                                }
                            }
                        }
                    }

                    if (strError == "Have error: </br>")
                    {
                        if (wpObj != null)
                        {
                            var docList = this.documentPackageService.GetAllEMDRByWorkgroup(wpObj.ID);
                            var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                            double complete = 0;
                            Milestone milestoneObj = new Milestone();
                            ProcessActual processActualObj = new ProcessActual();
                            List<WorkgroupManhourMonth> wmmList = new List<WorkgroupManhourMonth>();
                            WorkgroupManhourMonth wmmObj = new WorkgroupManhourMonth();

                            //update weight cho document
                            UpdateWeightDocument(projectObj, ref docList);
                            var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                            //update data workpackage
                            UpdateDataWorkpackage(projectObj, docList, ref wpObj, ref wpList, ref complete);
                            if (wpObj.IsAutoCalculate.GetValueOrDefault())
                            {
                                if ((complete < 100 && docList.Where(t => t.Complete != 100).Any()))
                                {
                                    //update milestone
                                    UpdateMilestonActual(wpObj, UserSession.Current.User, milestoneObj);
                                }
                            }
                            //Update actual progress
                            UpdateProgressActual(wpObj, processActualObj);

                            // update data Project
                            UpdateDataProject(wpList, projectObj);

                            //Update all wpWeight trong thang workgroup manhour month cua project
                            UpdateWeightWorkgroupManhourMonth(wpObj, wmmList);

                            UpdateDataWorkgroupManhourMonth(wpObj, UserSession.Current.User, wpList, wmmObj);
                        }

                        ScriptManager.RegisterStartupScript(this, typeof(Page), "mykey", "CloseAndRebind();", true);
                    }
                    else
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text = strError;
                    }
                }
            }

            //}
            //    catch (Exception ex)
            //    {
            //        this.blockError.Visible = true;
            //        this.lblError.Text = "Have error at EMDR file: '" + currentFileName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
            //    }
        }

        private void UpdateInfo(string currentDocumentNo, string currentWP, ref string strError, DataRow dataRow, DocumentPackage documentObj, DocumentType documentType, Role role, WorkGroup wpObj)
        {
            documentObj.DocTitle = dataRow["Column5"].ToString();
            var strstartDate = dataRow["Column7"].ToString();
            if (!string.IsNullOrEmpty(strstartDate))
            {
                var startDate = new DateTime();
                if (Utility.ConvertStringToDateTimeddMMyyyy(strstartDate, ref startDate))
                {
                    if (wpObj.StartDate != null && wpObj.Deadline != null && startDate.Date <= wpObj.Deadline.GetValueOrDefault().Date && startDate.Date >= wpObj.StartDate.GetValueOrDefault().Date)
                    {
                        documentObj.StartDate = startDate;
                    }
                    else
                    {
                        strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                currentDocumentNo + "</b>Startdate must be between " + wpObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + wpObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") + " <br/>";
                    }
                }
                else
                {
                    strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                currentDocumentNo + "</b> have invalid 'Start date' data.";
                }
            }
            else
            {
                documentObj.StartDate = null;
            }
            var strIDCDeadline = dataRow["Column8"].ToString();
            if (!string.IsNullOrEmpty(strIDCDeadline))
            {
                var deadline = new DateTime();
                if (Utility.ConvertStringToDateTimeddMMyyyy(strIDCDeadline, ref deadline))
                {
                    if (documentObj.StartDate != null && wpObj.Deadline != null && deadline.Date <= wpObj.Deadline.GetValueOrDefault().Date && deadline.Date >= documentObj.StartDate.GetValueOrDefault().Date)
                    {
                        documentObj.IDCDeadline = deadline;
                    }
                    else
                    {
                        strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                               currentDocumentNo + "</b>IDCDeadline must be between " + documentObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + wpObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") + " <br/>";
                    }
                }
                else
                {
                    strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                currentDocumentNo + "</b> have invalid 'IDCDeadline' data.";
                }
            }
            else
            {
                documentObj.IDCDeadline = null;
            }
            var strIFRDeadline = dataRow["Column9"].ToString();
            if (!string.IsNullOrEmpty(strIFRDeadline))
            {
                var deadline = new DateTime();
                if (Utility.ConvertStringToDateTimeddMMyyyy(strIFRDeadline, ref deadline))
                {
                    if (documentObj.StartDate != null && wpObj.Deadline != null && deadline.Date <= wpObj.Deadline.GetValueOrDefault().Date && deadline.Date >= documentObj.StartDate.GetValueOrDefault().Date)
                    {
                        documentObj.IFRDeadline = deadline;
                    }
                    else
                    {
                        strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                               currentDocumentNo + "</b>IFRDeadline must be between " + documentObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + wpObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") + " <br/>";
                    }
                }
                else
                {
                    strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                currentDocumentNo + "</b> have invalid 'IFRDeadline' data.";
                }
            }
            else
            {
                documentObj.IFRDeadline = null;
            }
            var strDeadline = dataRow["Column10"].ToString();
            if (!string.IsNullOrEmpty(strDeadline))
            {
                var deadline = new DateTime();
                if (Utility.ConvertStringToDateTimeddMMyyyy(strDeadline, ref deadline))
                {
                    if (documentObj.StartDate != null && wpObj.Deadline != null && deadline.Date <= wpObj.Deadline.GetValueOrDefault().Date && deadline.Date > documentObj.StartDate.GetValueOrDefault().Date)
                    {
                        documentObj.Deadline = deadline;
                    }
                    else
                    {
                        strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                               currentDocumentNo + "</b>Deadline must be between " + documentObj.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") + " and " + wpObj.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") + " <br/>";
                    }
                }
                else
                {
                    strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                currentDocumentNo + "</b> have invalid 'Deadline' data.";
                }
            }
            else
            {
                documentObj.Deadline = null;
            }
            //var strFinishDate = dataRow["Column10"].ToString();
            //if (!string.IsNullOrEmpty(strFinishDate))
            //{
            //    var finishDate = new DateTime();
            //    if (Utility.ConvertStringToDateTimeddMMyyyy(strFinishDate, ref finishDate))
            //    {
            //        documentObj.EndDate = finishDate;
            //    }
            //    else
            //    {
            //        strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
            //                    currentDocumentNo + "</b> have invalid 'Finish date' data.";
            //    }
            //}
            //else
            //{
            //    documentObj.EndDate = null;
            //}
            try
            {
                documentObj.Weight = !string.IsNullOrEmpty(dataRow["Column12"].ToString())
                ? Convert.ToDouble(dataRow["Column12"]) * 100
                : 0.0;
            }
            catch (Exception)
            {
                strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                            currentDocumentNo + "</b> have invalid 'Weight' data.";
            }
            if (documentObj.HasAttachFile == false)
            {
                try
                {
                    var newComplete = !string.IsNullOrEmpty(dataRow["Column13"].ToString())
                 ? Math.Round(Convert.ToDouble(dataRow["Column13"]) * 100, 2) : 0;
                    newComplete = newComplete > 100 ? 100 : newComplete;
                    var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(wpObj.ProjectId));
                    if (projectObj.IsIDC.GetValueOrDefault())
                    {
                        var statusObj = this.statusService.GetById(documentObj.StatusID.GetValueOrDefault());
                        if (statusObj != null)
                        {
                            var idcComplete = this.statusService.GetById(6).Complete.GetValueOrDefault();
                            var ifrComplete = this.statusService.GetById(7).Complete.GetValueOrDefault();
                            var ifaComplete = this.statusService.GetById(8).Complete.GetValueOrDefault();
                            if (statusObj.ID == 6)
                            {
                                documentObj.Complete = newComplete < idcComplete ? newComplete : 59;
                            }
                            else if (statusObj.ID == 7)
                            {
                                documentObj.Complete = newComplete < ifrComplete ? (newComplete > idcComplete ? newComplete : 60) : 79;
                            }
                            else if (statusObj.ID == 8)
                            {
                                documentObj.Complete = newComplete < ifaComplete ? (newComplete > ifrComplete ? newComplete : 80) : 99;
                            }
                        }
                    }
                    else
                    {
                        if (newComplete == 100)
                        {
                            documentObj.Complete = documentObj.HasAttachFile.GetValueOrDefault() ? newComplete : 99;
                        }
                        else
                        {
                            documentObj.Complete = newComplete;
                        }
                    }
                }
                catch (Exception)
                {
                    strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                                currentDocumentNo + "</b> have invalid Complete data.";
                }
            }

            try
            {
                var newManhourPlan = !string.IsNullOrEmpty(dataRow["Column15"].ToString())
             ? Math.Round(Convert.ToDouble(dataRow["Column15"]), 2) : 0;
                documentObj.ManHourPlan = newManhourPlan;
            }
            catch (Exception)
            {
                strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                            currentDocumentNo + "</b> have invalid 'Man hour Plan' data.";
            }

            try
            {
                var newManhourActual = !string.IsNullOrEmpty(dataRow["Column16"].ToString())
             ? Math.Round(Convert.ToDouble(dataRow["Column16"]), 2) : 0;
                documentObj.ManHours = newManhourActual;
            }
            catch (Exception)
            {
                strError += "Workpackage: <b>" + currentWP + "</b> - Doc No.: <b>" +
                            currentDocumentNo + "</b> have invalid 'Man hour Actual' data.";
            }

            documentObj.MilestoneDate = DateTime.Now;
            var leader = this.userService.GetByUserFullName(dataRow["Column18"].ToString().Trim());
            if (leader != null)
            {
                documentObj.LeaderId = leader.Id;
                documentObj.LeaderName = leader.UserNameWithFullName;
            }
            var stringEng = dataRow["Column19"].ToString().Trim();
            List<string> listEngID = new List<string>();
            List<string> listEngName = new List<string>();
            if (stringEng.Contains(","))
            {
                foreach (var item in stringEng.Split(','))
                {
                    var engineerTemp = this.userService.GetByUserFullName(item.Trim());
                    if (engineerTemp != null)
                    {
                        listEngID.Add(engineerTemp.Id.ToString());
                        listEngName.Add(engineerTemp.UserNameWithFullName);
                    }
                }
            }
            else
            {
                var engineer =
            this.userService.GetByUserFullName(stringEng);
                if (engineer != null)
                {
                    listEngID.Add(engineer.Id.ToString());
                    listEngName.Add(engineer.UserNameWithFullName);
                }
            }
            listEngID = listEngID.Distinct().ToList();
            if (listEngID.Count > 0)
            {
                documentObj.EngineerId = string.Join(",", listEngID.ToArray());
                documentObj.EngineerName = string.Join(",", listEngName.ToArray());
            }
            documentObj.PersonsInvolved = dataRow["Column20"].ToString();
            documentObj.DeparmentId = role != null ? role.Id : 0;
            documentObj.DeparmentName = role != null ? role.Name : string.Empty;

            documentObj.DocumentTypeId = documentType != null ? documentType.ID : 0;
            documentObj.DocumentTypeName = documentType != null
                ? documentType.FullName
                : string.Empty;
            documentObj.Notes = dataRow["Column29"].ToString();
        }

        private List<DateTime> GetBusinessDays(DateTime start, DateTime end)
        {
            List<DateTime> listdate = new List<DateTime>();
            for (var i = start; i <= end; i = i.AddDays(1))
            {
                if (i.DayOfWeek != DayOfWeek.Saturday && i.DayOfWeek != DayOfWeek.Sunday) { listdate.Add(i); }
            }
            return listdate;
        }

        private void NotificationUpdateDoc(DocumentPackage docObj)
        {
            if (docObj.EngineerId != "0")
            {
                var engineer = new List<User>();
                if (docObj.EngineerId.Contains(","))
                {
                    var listEng = docObj.EngineerId.Split(',');
                    foreach (var item in listEng)
                    {
                        engineer.Add(this.userService.GetByID(item));
                    }
                }
                else
                {
                    engineer.Add(this.userService.GetByID(Convert.ToInt32(docObj.EngineerId)));
                }
                if (engineer != null && engineer.Count > 0)
                {
                    string strEnginner = string.Join(" - ", engineer.Select(t => t.FullName));

                    var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.UpdateDoc);
                    if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                    {
                        var smtpClient = new SmtpClient
                        {
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                            EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                            Host = ConfigurationManager.AppSettings["Host"],
                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                            Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                        };
                        var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());
                        var updatedUser = this.userService.GetByID(UserSession.Current.User.Id);
                        var subject = notificationTemplate.Subject.Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#UpdatedUser#", updatedUser != null ? updatedUser.FullName : string.Empty);

                        var message = new MailMessage();
                        message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                        message.Subject = subject;
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = notificationTemplate.Contents
                            .Replace("#UpdatedUser#", updatedUser != null ? updatedUser.FullName : string.Empty)
                            .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                            .Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#DocTitle#", docObj.DocTitle)
                            .Replace("#DocType#", docObj.DocumentTypeName)
                            .Replace("#DocRev#", docObj.RevisionName)
                            .Replace("#DocEng#", strEnginner)
                            .Replace("#DocStartDate#", docObj.StartDate != null ? docObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocDeadline#", docObj.Deadline != null ? docObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocFinishDate#", docObj.EndDate != null ? docObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocISODate#", docObj.IsoReviseDate != null ? docObj.IsoReviseDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocWeight#", docObj.Weight != null ? docObj.Weight.Value.ToString() : string.Empty)
                            .Replace("#DocComplete#", docObj.Complete != null ? docObj.Complete.Value.ToString() : string.Empty);

                        if (ConfigurationManager.AppSettings["SendOnlySupport"].ToLower() == "true")
                        {
                            message.To.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                        }
                        else
                        {
                            foreach (var item in engineer)
                            {
                                if (!string.IsNullOrEmpty(item.Email))
                                {
                                    message.To.Add(item.Email);
                                }
                            }
                            var leader = this.userService.GetByID(docObj.LeaderId.ToString());
                            message.To.Add(leader.Email);
                        }

                        smtpClient.Send(message);
                    }
                }
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        #region UpdateData
        private void UpdateWeightDocument(ScopeProject projectObj, ref List<DocumentPackage> docList)
        {
            try
            {
                var totalPlanManhour = 0.0;
                totalPlanManhour = docList.Aggregate(totalPlanManhour, (current, t) => current + t.ManHourPlan.GetValueOrDefault());

                foreach (var doc in docList)
                {
                    doc.Weight = (doc.ManHourPlan.GetValueOrDefault() / totalPlanManhour) * 100;
                    this.documentPackageService.Update(doc);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkpackage(ScopeProject projectObj, List<DocumentPackage> docList, ref WorkGroup wpObj, ref List<WorkGroup> wpList, ref double complete)
        {
            try
            {
                if (docList.Count > 0)
                {
                    //update complete workpackage
                    complete = 0.0;
                    complete = docList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                    complete = Math.Round(complete, 5);
                    if (complete == 100 || !docList.Where(t => t.Complete < 100).Any())
                    {
                        wpObj.EndDate = DateTime.Now;
                        wpObj.Complete = 100;
                    }
                    else
                    {
                        wpObj.Complete = complete;
                    }
                }

                // Update total document weight for workpackage
                double totalWeight = 0.0;
                totalWeight = docList.Sum(t => t.Weight.GetValueOrDefault());
                totalWeight = Math.Round(totalWeight, 2);
                totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                wpObj.TotalDocWeight = totalWeight;

                // Update total man hours plan & actual for workpackage
                double totalManhourPlan = 0.0;
                totalManhourPlan = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                wpObj.TotalManHours = totalManhourPlan;

                double totalManHourActual = 0.0;
                totalManHourActual = docList.Sum(t => t.ManHours.GetValueOrDefault());
                wpObj.UsedManHours = totalManHourActual;

                // Update can delete for workpackage
                wpObj.CanDelete = !docList.Any();
                this.workGroupService.Update(wpObj);

                //update weight all WP of PJ
                if (projectObj.AutoCalculateWeightWorkGroup == true)
                {
                    var sumTotalManHours = 0.0;
                    sumTotalManHours = wpList.Sum(t => t.TotalManHours.GetValueOrDefault());
                    foreach (var items in wpList)
                    {
                        var calWeight = ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) > 0 ? ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) : 0;
                        items.Weight = calWeight >= 100 ? 100 : calWeight;
                        this.workGroupService.Update(items);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateMilestonActual(WorkGroup wpObj, User userObj, Milestone milestoneObj)
        {
            try
            {
                var milestoneList = this.milestoneService.GetAllByWorkpackage(wpObj.ID).OrderByDescending(t => t.MilestoneDate).ToList();
                if (milestoneList.Count > 0)
                {
                    var presentMilestoneObj = milestoneList.FirstOrDefault(t => t.MilestoneDate != null
                                                                         && t.MilestoneDate.Value.Month == DateTime.Now.Month
                                                                         && t.MilestoneDate.Value.Year == DateTime.Now.Year);
                    if (presentMilestoneObj != null)
                    {
                        milestoneObj = presentMilestoneObj;
                        var prevIndex = milestoneList.IndexOf(milestoneObj) + 1;
                        var prevRealTotal = 0.0;
                        var prevPlanTotal = 0.0;
                        if (prevIndex < (milestoneList.Count - 1))
                        {
                            var prevMilestoneObj = milestoneList[prevIndex];
                            if (prevMilestoneObj != null)
                            {
                                prevRealTotal = prevMilestoneObj.RealTotal.GetValueOrDefault();
                                prevPlanTotal = prevMilestoneObj.PlanTotal.GetValueOrDefault();
                            }
                            else
                            {
                                prevRealTotal = 0;
                                prevPlanTotal = 0;
                            }
                        }
                        else
                        {
                            prevRealTotal = 0;
                            prevPlanTotal = 0;
                        }

                        // calculator real
                        double real = Math.Round(wpObj.Complete.GetValueOrDefault() - prevRealTotal, 2);
                        milestoneObj.Real = real;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PlanTotal = (prevPlanTotal + milestoneObj.PlanPercent) > 100 ? 100 : (prevPlanTotal + milestoneObj.PlanPercent);
                        milestoneObj.UpdatedBy = userObj.Id;
                        milestoneObj.UpdatedDate = DateTime.Now;
                        this.milestoneService.Update(milestoneObj);
                    }
                    else
                    {
                        var lastedMilestoneObj = milestoneList[0];
                        milestoneObj = new Milestone();
                        milestoneObj.MilestoneDate = DateTime.Now;
                        milestoneObj.PlanPercent = 0;
                        milestoneObj.PlanTotal = lastedMilestoneObj.PlanTotal;
                        double preReal = Math.Round(wpObj.Complete.GetValueOrDefault() - lastedMilestoneObj.RealTotal.GetValueOrDefault(), 2);
                        milestoneObj.Real = preReal;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PerformingUser = lastedMilestoneObj.PerformingUser;
                        milestoneObj.Note = lastedMilestoneObj.Note;
                        milestoneObj.WorkpackageId = wpObj.ID;
                        milestoneObj.WorkpackageName = wpObj.Name;
                        if (DateTime.Now.Year > lastedMilestoneObj.MilestoneDate.Value.Year && (DateTime.Now.Year - lastedMilestoneObj.MilestoneDate.Value.Year) > 0)
                        {
                            milestoneObj.PlanOfYear = 0;
                        }
                        else
                        {
                            milestoneObj.PlanOfYear = lastedMilestoneObj.PlanOfYear;
                        }
                        milestoneObj.CreatedBy = userObj.Id;
                        milestoneObj.CreatedDate = DateTime.Now;
                        this.milestoneService.Insert(milestoneObj);
                    }
                }
                else
                {
                    //milestoneObj = new Milestone();
                    milestoneObj.MilestoneDate = DateTime.Now;
                    milestoneObj.PlanPercent = 0;
                    milestoneObj.PlanTotal = 0;
                    milestoneObj.RealTotal = wpObj.Complete;
                    milestoneObj.Real = wpObj.Complete;
                    milestoneObj.PerformingUser = string.Empty;
                    milestoneObj.Note = string.Empty;
                    milestoneObj.WorkpackageId = wpObj.ID;
                    milestoneObj.WorkpackageName = wpObj.Name;
                    milestoneObj.PlanOfYear = 0;
                    milestoneObj.CreatedBy = userObj.Id;
                    milestoneObj.CreatedDate = DateTime.Now;
                    this.milestoneService.Insert(milestoneObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateProgressActual(WorkGroup wpObj, ProcessActual processActualObj)
        {
            try
            {
                var projectId = wpObj.ProjectId.GetValueOrDefault();
                var projectObj = this.scopeProjectService.GetById(projectId);
                var tempProcessActualObj = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, wpObj.ID);
                if (tempProcessActualObj != null)
                {
                    processActualObj = tempProcessActualObj;
                    if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null)
                    {
                        if (string.IsNullOrEmpty(processActualObj.Actual))
                        {
                            processActualObj.Actual = "0";
                        }
                        if (string.IsNullOrEmpty(processActualObj.ActualMonth))
                        {
                            processActualObj.ActualMonth = "0";
                        }
                        var progressActualWeekList = processActualObj.Actual.Split('$').ToList();
                        var progressActualMonthList = processActualObj.ActualMonth.Split('$').ToList();
                        var countWeek = 0;
                        var countMonth = 0;
                        for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualWeekList.Count() > countWeek)
                            {
                                if (DateTime.Now > j.AddDays(7))
                                {
                                    countWeek += 1;
                                }
                            }
                        }
                        var currentMonth = 0;
                        for (var j = projectObj.StartDate.GetValueOrDefault();
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualMonthList.Count() > countMonth)
                            {
                                if (DateTime.Now.Month > j.AddDays(7).Month && DateTime.Now > j.AddDays(7) && currentMonth != j.AddDays(7).Month)
                                {
                                    currentMonth = j.Month;
                                    countMonth++;
                                }
                            }
                        }
                        if (progressActualWeekList.Count() >= countWeek && progressActualWeekList.Count() > 0 && countWeek > 0)
                        {
                            progressActualWeekList = progressActualWeekList.Take(countWeek).ToList();
                            progressActualWeekList[countWeek - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.Actual = string.Join("$", progressActualWeekList);
                            this.processActualService.Update(processActualObj);
                        }
                        if (progressActualMonthList.Count() >= countMonth && progressActualMonthList.Count() > 0 && countMonth > 0)
                        {
                            progressActualMonthList = progressActualMonthList.Take(countMonth).ToList();
                            progressActualMonthList[countMonth - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.ActualMonth = string.Join("$", progressActualMonthList);
                            this.processActualService.Update(processActualObj);
                        }
                    }
                }
                else
                {
                    processActualObj = new ProcessActual();
                    processActualObj.ProjectId = wpObj.ProjectId;
                    processActualObj.WorkgroupId = wpObj.ID;
                    processActualObj.Actual = wpObj.Complete.ToString();
                    processActualObj.ActualMonth = wpObj.Complete.ToString();
                    this.processActualService.Insert(processActualObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private DateTime GetMondayOfWeek(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        private DateTime GetFridayOfWeek(DateTime date)
        {
            return GetMondayOfWeek(date).AddDays(4);
        }

        private void UpdateDataProject(List<WorkGroup> wpList, ScopeProject projectObj)
        {
            try
            {
                if (projectObj != null)
                {
                    projectObj.CanDelete = false;
                    if (projectObj.IsAutoCalculate.GetValueOrDefault())
                    {
                        double complete = 0.0;
                        complete = wpList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                        projectObj.Complete = Math.Round(complete, 5);
                        double totalWeight = 0.0;
                        totalWeight = wpList.Sum(t => t.Weight.GetValueOrDefault());
                        totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                        projectObj.TotalWorkpackageWeight = Math.Round(totalWeight, 2);
                    }
                    this.scopeProjectService.Update(projectObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateWeightWorkgroupManhourMonth(WorkGroup wpObj, List<WorkgroupManhourMonth> wmmList)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                wmmList = this.workgroupManhourMonthService.GetByPJ(wpObj.ProjectId.GetValueOrDefault(), month, year);
                foreach (var wmmItem in wmmList)
                {
                    var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                    if (wpItem != null)
                    {
                        wmmItem.WorkgroupName = wpItem.Name;
                        wmmItem.WorkgroupWeight = wpItem.Weight;
                        this.workgroupManhourMonthService.Update(wmmItem);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkgroupManhourMonth(WorkGroup wpObj, User userObj, List<WorkGroup> wpList, WorkgroupManhourMonth wmmObj)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                var docList = this.documentPackageService.GetAllByWorkgroup(wpObj.ID);
                var manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                var presentWMMObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), month, year);
                var prevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpObj.ID, month, year);
                if (presentWMMObj != null)
                {
                    wmmObj = presentWMMObj;
                    wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                    wmmObj.ManhourPlanTotal = manhourPlanTotal;
                    wmmObj.ManhourPlanMonth = (wmmObj.ManhourPlanTotal.GetValueOrDefault() * wmmObj.CompletePlanMonth.GetValueOrDefault()) / 100;
                    wmmObj.CompleteActualTotal = wpObj.Complete.GetValueOrDefault();
                    wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                    //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                    if (prevWMMObj != null)
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                    }
                    else
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                    }

                    wmmObj.UpdateBy = userObj.Id;
                    wmmObj.UpdateDate = DateTime.Now;
                    this.workgroupManhourMonthService.Update(wmmObj);
                }
                else
                {
                    foreach (var wpItem in wpList)
                    {
                        docList = this.documentPackageService.GetAllByWorkgroup(wpItem.ID);
                        manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                        var tempObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpItem.ID, wpItem.ProjectId.GetValueOrDefault(), month, year);
                        var tempPrevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpItem.ID, month, year);
                        if (tempObj == null)
                        {
                            wmmObj = new WorkgroupManhourMonth();
                            wmmObj.Month = month;
                            wmmObj.Year = year;
                            wmmObj.WorkgroupID = wpItem.ID;
                            wmmObj.WorkgroupName = wpItem.Name;
                            wmmObj.DeparmentID = wpItem.DepartmentId;
                            wmmObj.ProjectID = wpItem.ProjectId.GetValueOrDefault();
                            wmmObj.WorkgroupWeight = wpItem.Weight.GetValueOrDefault();
                            wmmObj.ManhourPlanTotal = manhourPlanTotal;
                            wmmObj.CompleteActualTotal = wpItem.Complete.GetValueOrDefault();
                            wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                            //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                            if (tempPrevWMMObj != null)
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - tempPrevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - tempPrevWMMObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            else
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            wmmObj.CreateBy = userObj.Id;
                            wmmObj.CreateDate = DateTime.Now;
                            this.workgroupManhourMonthService.Insert(wmmObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourWeek(DocumentPackage documentObj, User userObj, UserManhour userManhourObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;

                CultureInfo myCI = CultureInfo.CurrentCulture;
                System.Globalization.Calendar myCal = myCI.Calendar;
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var listUserManhour = this.userManhourService.GetByDocIDAndUserID(documentObj.ID, user.Id);
                            listUserManhour = listUserManhour.OrderByDescending(t => t.ID).ToList();

                            var currentUserManhourWeek = listUserManhour.FirstOrDefault(t => myCal.GetWeekOfYear(DateTime.Now.Date, myCWR, myFirstDOW) == myCal.GetWeekOfYear(t.Date.GetValueOrDefault(), myCWR, myFirstDOW));
                            int index = listUserManhour.IndexOf(currentUserManhourWeek);
                            var prevUserManhourWeek = new UserManhour();
                            if (index != -1 && (index + 1) < listUserManhour.Count)
                            {
                                prevUserManhourWeek = listUserManhour[(index + 1)];
                            }
                            if (currentUserManhourWeek != null)
                            {
                                userManhourObj = currentUserManhourWeek;
                                if (prevUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - prevUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UpdateBy = userObj.Id;
                                userManhourObj.UpdateDate = DateTime.Now;
                                this.userManhourService.Update(userManhourObj);
                            }
                            else
                            {
                                var lastedUserManhourWeek = new UserManhour();
                                if (listUserManhour.Count > 0)
                                {
                                    lastedUserManhourWeek = listUserManhour[0];
                                }
                                userManhourObj = new UserManhour();
                                userManhourObj.DocumentID = documentObj.ID;
                                userManhourObj.Date = DateTime.Now.Date;
                                userManhourObj.DeparmentID = user.RoleId;
                                userManhourObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourObj.ProjectID = documentObj.ProjectId;
                                if (lastedUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - lastedUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - lastedUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UserID = user.Id;
                                userManhourObj.UserName = user.FullName;
                                userManhourObj.CreateBy = userObj.Id;
                                userManhourObj.CreateDate = DateTime.Now;
                                this.userManhourService.Insert(userManhourObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourMonth(DocumentPackage documentObj, User userObj, UserManhourMonth userManhourMonthObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;
                var date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var currentUserManhourMonth = this.userManhourMonthService.GetByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            var prevUserManhourMonth = this.userManhourMonthService.GetLastedByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            if (currentUserManhourMonth != null)
                            {
                                userManhourMonthObj = currentUserManhourMonth;
                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UpdateBy = userObj.Id;
                                userManhourMonthObj.UpdateDate = DateTime.Now;
                                this.userManhourMonthService.Update(userManhourMonthObj);
                            }
                            else
                            {
                                userManhourMonthObj = new UserManhourMonth();
                                userManhourMonthObj.DocumentID = documentObj.ID;
                                userManhourMonthObj.Month = DateTime.Now.Month;
                                userManhourMonthObj.Year = DateTime.Now.Year;
                                userManhourMonthObj.DeparmentID = user.RoleId;
                                userManhourMonthObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourMonthObj.ProjectID = documentObj.ProjectId;

                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UserID = user.Id;
                                userManhourMonthObj.UserName = user.FullName;
                                userManhourMonthObj.CreateBy = userObj.Id;
                                userManhourMonthObj.CreateDate = DateTime.Now;
                                this.userManhourMonthService.Insert(userManhourMonthObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using Aspose.Cells;
using System.Net;
using System.Net.Mail;
using System.Text;
using Aspose.Words.Tables;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using OfficeHelper.Utilities.Data;

namespace EDMs.Web
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;


    using EDMs.Business.Services.Library;
    using EDMs.Web.Utilities.Sessions;
    using Aspose.Words;
    using Telerik.Web.UI;

    public class MonthWeekProgress
    {
        private int month;
        private string week;

        public MonthWeekProgress()
        {

        }

        public MonthWeekProgress(int month, string week)
        {
            this.month = month;
            this.week = week;
        }

        public int Month
        {
            get
            {
                return month;
            }

            set
            {
                month = value;
            }
        }

        public string Week
        {
            get
            {
                return week;
            }

            set
            {
                week = value;
            }
        }
    }

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class ProjectProccessReport : Page
    {
        /// <summary>
        /// The scope project service.
        /// </summary>
        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly ProcessActualService processActualService = new ProcessActualService();
        private readonly ProcessPlanedService processPlanedService = new ProcessPlanedService();
        private readonly WorkGroupService workGroupService = new WorkGroupService();
        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly MilestoneService milestoneService = new MilestoneService();
        private readonly RoleService roleService = new RoleService();
        private readonly UserService userService = new UserService();
        private readonly EmailNotificationTemplateService emailNotificationTemplateService = new EmailNotificationTemplateService();
        private readonly WorkpackageContentService workpackageContentService = new WorkpackageContentService();
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();
        private readonly ProjectAppendixService projectAppendixService = new ProjectAppendixService();
        private readonly WorkService workService = new WorkService();
        private List<WorkGroup> WorkpackageList
        {
            get
            {
                var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
                var currentDepartment = UserSession.Current.User.RoleId;

                return isSeeFull
                    ? this.workGroupService.GetAll()
                    : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault());
            }
        }




        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {
                this.InitData();
                this.LoadProcessReport(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, 0);

                // if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                // {
                //     NotificationDeadlineDoc();
                //     NotificationDeadlineWp();
                //}
            }
        }

        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"Images/project.png";
        }

        protected void ddlProject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
            var currentDepartment = UserSession.Current.User.RoleId;
            var workPackageList = isSeeFull
                ? this.workGroupService.GetAll()
                : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault());

            if (UserSession.Current.IsEngineer)
            {
                var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
            }

            this.rtvWorkgroup.DataSource = workPackageList.Where(
                                                t =>
                                                    t.ProjectId ==
                                                    (!string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                                                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                                        : 0));
            this.rtvWorkgroup.DataTextField = "Name";
            this.rtvWorkgroup.DataValueField = "ID";
            this.rtvWorkgroup.DataFieldID = "ID";
            this.rtvWorkgroup.DataBind();


            this.LoadProcessReport(Convert.ToInt32(this.ddlProject.SelectedValue), 0);
        }

        protected void rtvWorkgroup_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.LoadProcessReport(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, Convert.ToInt32(e.Node.Value));
        }
        public DateTime GetMondayOfWeek(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        public DateTime GetFridayOfWeek(DateTime date)
        {
            return GetMondayOfWeek(date).AddDays(4);
        }
        protected void rtvWorkgroup_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/package.png";
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            var isSeeFull = UserSession.Current.IsAdmin
                        || UserSession.Current.IsDC
                        || UserSession.Current.IsSuperViewer;
            var month = DateTime.Now.Month;
            var year = DateTime.Now.Year;
            var nextMonth = DateTime.Now.AddMonths(1).Month;
            var nextYear = DateTime.Now.AddMonths(1).Year;

            if (e.Argument == "RefreshProgressReport")
            {
                this.rtvWorkgroup.UnselectAllNodes();
                this.LoadProcessReport(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, 0);
            }
            else if (e.Argument == "PrintProgress")
            {
                var projectId = this.ddlProject.SelectedItem != null
                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                    : 0;
                var processReportList = new List<ProcessReport>();

                var projectObj = this.scopeProjectService.GetById(projectId);
                if (projectObj != null)
                {

                    var listWorkgroupInPermission = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);

                    var dateList = new List<DateTime>();

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();

                    var processPlanedList = this.processPlanedService.GetAllByProject(projectId, 0);
                    if (projectObj.StartDate != null && projectObj.Deadline != null)
                    {
                        var workgroupCount = 0;
                        var startDate = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                        //while (startDate.DayOfWeek != DayOfWeek.Monday)
                        //{
                        //    startDate = startDate.AddDays(1);
                        //}
                        if (projectObj.WeeklyProgress.GetValueOrDefault())
                        {
                            workbook.Open(filePath + @"Template\ProgressTemplate_v1.xls");

                            var wsProgress = workbook.Worksheets[0];
                            wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR " + projectObj.Description);
                            wsProgress.Cells["C2"].PutValue("PROJECT PROGRESS (CUT OFF: " + DateTime.Now.ToString("dd/MM/yyyy") + ")");
                            var currentMonth = 0;
                            var countMerge = 0;
                            var count = 3;
                            var countNumberCurrentActual = 0;
                            for (var j = startDate;
                                    j <= projectObj.Deadline.GetValueOrDefault();
                                    j = j.AddDays(7))
                            {
                                var processReport = new ProcessReport();
                                processReport.WeekDate = j;
                                processReportList.Add(processReport);

                                if (DateTime.Now > j)
                                {
                                    countNumberCurrentActual += 1;
                                }

                                if (currentMonth != j.AddDays(7).Month)
                                {
                                    wsProgress.Cells[2, count].PutValue(j.AddDays(7));
                                    currentMonth = j.AddDays(7).Month;

                                    if (countMerge != 0)
                                    {
                                        wsProgress.Cells.Merge(2, count - countMerge, 1, countMerge);
                                    }

                                    countMerge = 1;
                                }
                                else
                                {
                                    countMerge += 1;
                                }

                                wsProgress.Cells[3, count].PutValue(count - 2);
                                wsProgress.Cells[4, count].PutValue(j);
                                //wsProgress.Cells[5, count].PutValue(j.AddDays(5));

                                count += 1;
                            }

                            if (listWorkgroupInPermission.Count > 0)
                            {
                                var wgCount = listWorkgroupInPermission.Count;
                                wsProgress.Cells.InsertRows(8, wgCount * 3);
                                for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                                {
                                    var workgroup = listWorkgroupInPermission[i];

                                    wsProgress.Cells["B" + (8 + (i * 3))].PutValue(workgroup.Name);
                                    wsProgress.Cells["C" + (8 + (i * 3))].PutValue("Planed (%)");
                                    wsProgress.Cells["C" + (8 + (i * 3) + 1)].PutValue("Actual (%)");
                                    wsProgress.Cells["C" + (8 + (i * 3) + 2)].PutValue("+/-");

                                    wsProgress.Cells.Merge(7 + (i * 3), 1, 3, 1);

                                    var progressPlaned = this.processPlanedService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                                    var progressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);

                                    if (progressPlaned != null && progressActual != null)
                                    {
                                        var planedList = progressPlaned.Planed.Split('$');
                                        var actualList = progressActual.Actual.Split('$');
                                        for (int j = 0; j < planedList.Count(); j++)
                                        {
                                            var planed = !string.IsNullOrEmpty(planedList[j]) ? Convert.ToDouble(planedList[j]) / 100 : 0;
                                            var actual = 0.0;

                                            wsProgress.Cells[7 + (i * 3), j + 3].PutValue(planed);
                                            if (j < actualList.Count() && j <= countNumberCurrentActual)
                                            {
                                                actual = !string.IsNullOrEmpty(actualList[j]) ? Convert.ToDouble(actualList[j]) / 100 : 0;
                                                if (actual != 0.0)
                                                {
                                                    wsProgress.Cells[8 + (i * 3), j + 3].PutValue(actual);
                                                    wsProgress.Cells[9 + (i * 3), j + 3].PutValue(actual - planed);
                                                }
                                            }

                                            if (j < processReportList.Count)
                                            {
                                                processReportList[j].Planed += (planed * workgroup.Weight.GetValueOrDefault()) / 100;
                                            }

                                            if (j < actualList.Count() && j < processReportList.Count)
                                            {
                                                processReportList[j].Actual += (Convert.ToDouble(actualList[j]) / 100 * workgroup.Weight.GetValueOrDefault()) / 100;
                                            }
                                        }
                                    }
                                }

                                wsProgress.Cells.Merge(7 + (wgCount * 3), 1, 3, 1);

                                wsProgress.Cells["B" + (8 + (wgCount * 3))].PutValue("Overall Project");
                                wsProgress.Cells["C" + (8 + (wgCount * 3))].PutValue("Planed (%)");
                                wsProgress.Cells["C" + (8 + (wgCount * 3) + 1)].PutValue("Actual (%)");
                                wsProgress.Cells["C" + (8 + (wgCount * 3) + 2)].PutValue("+/-");

                                for (int i = 0; i < processReportList.Count; i++)
                                {
                                    wsProgress.Cells[7 + (wgCount * 3), i + 3].PutValue(processReportList[i].Planed);
                                    if (processReportList[i].Actual != 0.0)
                                    {
                                        wsProgress.Cells[8 + (wgCount * 3), i + 3].PutValue(processReportList[i].Actual);
                                        if (i < countNumberCurrentActual)
                                        {
                                            wsProgress.Cells[9 + (wgCount * 3), i + 3].PutValue(processReportList[i].Actual - processReportList[i].Planed);
                                        }
                                    }
                                }

                                wsProgress.Cells["A1"].PutValue(0);
                                wsProgress.Cells["A2"].PutValue(wgCount);
                                wsProgress.Cells["A3"].PutValue(processReportList.Count);

                                //wsProgress.Cells.Merge(0, 2, 1, count - 2);
                                //wsProgress.Cells.Merge(1, 2, 1, count - 2);
                            }
                        }
                        else
                        {
                            workbook.Open(filePath + @"Template\ProgressTemplateMonth_v1.xls");

                            var wsProgress = workbook.Worksheets[0];
                            wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR " + projectObj.Description);
                            wsProgress.Cells["C2"].PutValue("PROJECT PROGRESS (CUT OFF: " + DateTime.Now.ToString("dd/MM/yyyy") + ")");
                            //var currentMonth = 0;
                            //var countMerge = 0;
                            var count = 3;
                            var countNumberCurrentActual = 0;
                            for (var j = startDate;
                                    j <= projectObj.Deadline.GetValueOrDefault();
                                    j = j.AddMonths(1))
                            {
                                var processReport = new ProcessReport();
                                processReport.STRWeekDate = j.ToString("MM/yyyy");
                                processReportList.Add(processReport);

                                if (DateTime.Now > j)
                                {
                                    countNumberCurrentActual += 1;
                                }
                                wsProgress.Cells[2, count].PutValue(j);

                                wsProgress.Cells[3, count].PutValue(count - 2);
                                wsProgress.Cells[4, count].PutValue(j);

                                count += 1;
                            }

                            if (listWorkgroupInPermission.Count > 0)
                            {
                                var wgCount = listWorkgroupInPermission.Count;
                                wsProgress.Cells.InsertRows(8, wgCount * 3);
                                for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                                {
                                    var workgroup = listWorkgroupInPermission[i];

                                    wsProgress.Cells["B" + (8 + (i * 3))].PutValue(workgroup.Name);
                                    wsProgress.Cells["C" + (8 + (i * 3))].PutValue("Planed (%)");
                                    wsProgress.Cells["C" + (8 + (i * 3) + 1)].PutValue("Actual (%)");
                                    wsProgress.Cells["C" + (8 + (i * 3) + 2)].PutValue("+/-");

                                    wsProgress.Cells.Merge(7 + (i * 3), 1, 3, 1);

                                    var progressPlaned = this.processPlanedService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                                    var progressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);

                                    if (progressPlaned != null)
                                    {
                                        var planedList = progressPlaned.PlanedMonth.Split('$');
                                        for (int j = 0; j < planedList.Count(); j++)
                                        {
                                            var planed = !string.IsNullOrEmpty(planedList[j]) ? Convert.ToDouble(planedList[j]) / 100 : 0;

                                            wsProgress.Cells[7 + (i * 3), j + 3].PutValue(planed);
                                            if (j < processReportList.Count)
                                            {
                                                processReportList[j].Planed += (planed * workgroup.Weight.GetValueOrDefault()) / 100;
                                            }
                                        }
                                    }
                                    if (progressActual != null)
                                    {
                                        string[] planedList;
                                        if (progressPlaned != null)
                                        {
                                            planedList = progressPlaned.PlanedMonth.Split('$');
                                        }
                                        else
                                        {
                                            planedList = new string[1] { "0" };
                                        }
                                        var actualList = progressActual.ActualMonth.Split('$');
                                        for (int j = 0; j < actualList.Count(); j++)
                                        {

                                            var actual = 0.0;

                                            if (j < actualList.Count() && j <= countNumberCurrentActual)
                                            {
                                                actual = !string.IsNullOrEmpty(actualList[j]) ? Convert.ToDouble(actualList[j]) / 100 : 0;
                                                if (actual != 0.0)
                                                {
                                                    wsProgress.Cells[8 + (i * 3), j + 3].PutValue(actual);
                                                    if (j < planedList.Count())
                                                    {
                                                        var planed = !string.IsNullOrEmpty(planedList[j]) ? Convert.ToDouble(planedList[j]) / 100 : 0;
                                                        wsProgress.Cells[9 + (i * 3), j + 3].PutValue(actual - planed);
                                                    }

                                                }
                                            }

                                            if (j < actualList.Count() && j < processReportList.Count)
                                            {
                                                processReportList[j].Actual += (Convert.ToDouble(actualList[j]) / 100 * workgroup.Weight.GetValueOrDefault()) / 100;
                                            }
                                        }
                                    }
                                }

                                wsProgress.Cells.Merge(7 + (wgCount * 3), 1, 3, 1);

                                wsProgress.Cells["B" + (8 + (wgCount * 3))].PutValue("Overall Project");
                                wsProgress.Cells["C" + (8 + (wgCount * 3))].PutValue("Planed (%)");
                                wsProgress.Cells["C" + (8 + (wgCount * 3) + 1)].PutValue("Actual (%)");
                                wsProgress.Cells["C" + (8 + (wgCount * 3) + 2)].PutValue("+/-");

                                for (int i = 0; i < processReportList.Count; i++)
                                {
                                    wsProgress.Cells[7 + (wgCount * 3), i + 3].PutValue(processReportList[i].Planed);
                                    if (processReportList[i].Actual != 0.0)
                                    {
                                        wsProgress.Cells[8 + (wgCount * 3), i + 3].PutValue(processReportList[i].Actual);
                                        if (i < countNumberCurrentActual)
                                        {
                                            wsProgress.Cells[9 + (wgCount * 3), i + 3].PutValue(processReportList[i].Actual - processReportList[i].Planed);
                                        }
                                    }
                                }

                                wsProgress.Cells["A1"].PutValue(0);
                                wsProgress.Cells["A2"].PutValue(wgCount);
                                wsProgress.Cells["A3"].PutValue(processReportList.Count);

                                //wsProgress.Cells.Merge(0, 2, 1, count - 2);
                                //wsProgress.Cells.Merge(1, 2, 1, count - 2);
                            }
                        }
                    }

                    // Save and export file
                    var filename = (this.ddlProject.SelectedItem != null
                            ? Utility.RemoveSpecialCharacter(this.ddlProject.SelectedItem.Text) + "_"
                            : string.Empty) + "Progress report - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument == "GetLatestData")
            {
                //var listDate = new List<DateTime>();
                var projectId = this.ddlProject.SelectedItem != null
                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                    : 0;
                //var projectObj = this.scopeProjectService.GetById(projectId);
                var projectObj = this.scopeProjectService.GetById(projectId);

                //foreach (var projectObj in projectlist)
                //{
                if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null)
                {
                    var listWorkgroup = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                    foreach (var itemWP in listWorkgroup)
                    {
                        var existProgressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, itemWP.ID);
                        if (existProgressActual != null)
                        {
                            var progressActualList = existProgressActual.Actual.Split('$').ToList();
                            var progressActualMonthList = existProgressActual.ActualMonth.Split('$').ToList();
                            var count = 0;
                            var currentMonth = 0;
                            //if (projectObj.WeeklyProgress.GetValueOrDefault())
                            //{
                            for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                j < projectObj.Deadline.GetValueOrDefault();
                                j = j.AddDays(7))
                            {
                                if (DateTime.Now > j.AddDays(7))
                                {
                                    count += 1;
                                    if (progressActualList.Count() < count)
                                    {
                                        progressActualList.Add("0");
                                    }
                                }
                            }
                            if (progressActualList.Count() >= count && count > 0)
                            {
                                progressActualList = progressActualList.Take(count).ToList();
                                progressActualList[count - 1] = Math.Round(itemWP.Complete.GetValueOrDefault(), 2).ToString();
                                existProgressActual.Actual = string.Join("$", progressActualList);
                                this.processActualService.Update(existProgressActual);
                            }
                            for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                    j < projectObj.Deadline.GetValueOrDefault();
                                    j = j.AddDays(7))
                            {
                                if (currentMonth != j.AddDays(7).Month)
                                {
                                    currentMonth = j.AddDays(7).Month;
                                    count++;
                                    if (progressActualMonthList.Count() < count)
                                    {
                                        progressActualMonthList.Add("0");
                                    }
                                }

                            }
                            if (progressActualMonthList.Count() >= count && count > 0)
                            {
                                progressActualMonthList = progressActualMonthList.Take(count).ToList();
                                progressActualMonthList[count - 1] = Math.Round(itemWP.Complete.GetValueOrDefault(), 2).ToString();
                                existProgressActual.ActualMonth = string.Join("$", progressActualMonthList);
                                this.processActualService.Update(existProgressActual);
                            }
                        }
                        else
                        {
                            ProcessActual actual = new ProcessActual();
                            actual.ProjectId = itemWP.ProjectId;
                            actual.WorkgroupId = itemWP.ID;
                            actual.Actual = itemWP.Complete.ToString();
                            actual.ActualMonth = itemWP.Complete.ToString();
                            this.processActualService.Insert(actual);
                        }
                    }
                }
                #region Tu tinh plan, actual Month
                if (projectObj.WeeklyProgress.GetValueOrDefault())
                {
                    var processPlanList = this.processPlanedService.GetAllByProject(projectObj.ID, 0);
                    var processActualList = this.processActualService.GetAllByProject(projectObj.ID, 0);
                    var planMonthList = new List<double>();
                    var actualMonthList = new List<double>();
                    foreach (var processActualItem in processActualList)
                    {
                        actualMonthList = new List<double>();
                        var actualList = processActualItem.Actual.Split('$').ToList();
                        var currentMonth = 0;
                        var weekInMonth = 0;
                        var countWeek = 0;
                        var countMonth = 0;
                        var tempValue = 0.0;
                        var newMonth = false;
                        for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                    j < projectObj.Deadline.GetValueOrDefault();
                                    j = j.AddDays(7))
                        {
                            if (actualList.Count > countWeek)
                            {
                                var months = j.AddDays(7).Month;
                                if (currentMonth != months)
                                {
                                    double temp;
                                    tempValue = Double.TryParse(actualList[countWeek], out temp) ? Double.Parse(actualList[countWeek]) : 0;
                                    weekInMonth = 1;
                                    countMonth++;
                                    currentMonth = j.AddDays(7).Month;
                                    newMonth = true;
                                }
                                else
                                {
                                    newMonth = false;
                                    weekInMonth++;
                                    double temp;
                                    tempValue += Double.TryParse(actualList[countWeek], out temp) ? Double.Parse(actualList[countWeek]) : 0;
                                }
                                if (newMonth)
                                {
                                    actualMonthList.Add(tempValue / weekInMonth);
                                }
                                else
                                {
                                    actualMonthList[countMonth - 1] = tempValue / weekInMonth;
                                }
                                countWeek++;
                            }
                        }

                        if (actualMonthList.Count == 0)
                        {
                            actualMonthList.Add(0);
                        }
                        processActualItem.ActualMonth = string.Join("$", actualMonthList);
                        this.processActualService.Update(processActualItem);
                    }
                    foreach (var processPlanItem in processPlanList)
                    {
                        planMonthList = new List<double>();
                        var planList = processPlanItem.Planed.Split('$').ToList();
                        var currentMonth = 0;
                        var weekInMonth = 0;
                        var countWeek = 0;
                        var countMonth = 0;
                        var tempValue = 0.0;
                        var newMonth = false;
                        for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                    j < projectObj.Deadline.GetValueOrDefault();
                                    j = j.AddDays(7))
                        {
                            if (planList.Count > countWeek)
                            {
                                if (currentMonth != j.AddDays(7).Month)
                                {
                                    double temp;
                                    tempValue = Double.TryParse(planList[countWeek], out temp) ? Double.Parse(planList[countWeek]) : 0;
                                    weekInMonth = 1;
                                    countMonth++;
                                    currentMonth = j.AddDays(7).Month;
                                    newMonth = true;
                                }
                                else
                                {
                                    newMonth = false;
                                    weekInMonth++;
                                    double temp;
                                    tempValue += Double.TryParse(planList[countWeek], out temp) ? Double.Parse(planList[countWeek]) : 0;
                                }
                                if (newMonth)
                                {
                                    planMonthList.Add(tempValue / weekInMonth);
                                }
                                else
                                {
                                    planMonthList[countMonth - 1] = tempValue / weekInMonth;
                                }
                                countWeek++;
                            }
                        }
                        if (planMonthList.Count == 0)
                        {
                            planMonthList.Add(0);
                        }
                        processPlanItem.PlanedMonth = string.Join("$", planMonthList);
                        this.processPlanedService.Update(processPlanItem);
                    }
                }
                //}
                #endregion
                this.LoadProcessReport(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, 0);
                //    //var listWorkgroupInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                //    //    ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue))
                //    //        .OrderBy(t => t.ID)
                //    //        .ToList()
                //    //    : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id,
                //    //        !string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                //    //            ? Convert.ToInt32(this.ddlProject.SelectedValue)
                //    //            : 0)
                //    //        .OrderBy(t => t.ID).ToList();
                //    //foreach (var workgroup in listWorkgroupInPermission)
                //    //{
                //    //    var docList = this.documentPackageService.GetAllByWorkgroup(workgroup.ID)
                //    //                        .OrderBy(t => t.DocNo)
                //    //                        .ToList();
                //    //    double complete = 0;
                //    //    complete = docList.Aggregate(complete, (current, t) => current + (t.Complete.GetValueOrDefault() * t.Weight.GetValueOrDefault()) / 100);
                //    //    var existProgressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                //    //    if (existProgressActual != null)
                //    //    {
                //    //        if (existProgressActual.Actual.Split('$').Count() >= count)
                //    //        {
                //    //            var actualList = existProgressActual.Actual.Split('$');
                //    //            actualList[count - 1] = Math.Round(complete, 2).ToString();
                //    //            var newActual = string.Empty;
                //    //            newActual = actualList.Aggregate(newActual, (current, t) => current + t + "$");
                //    //            newActual = newActual.Substring(0, newActual.Length - 1);
                //    //            existProgressActual.Actual = newActual;
                //    //            this.processActualService.Update(existProgressActual);
                //    //        }
                //    //    }
                //    //}
                //    this.LoadProcessReport(projectObj.ID, 0);
                //}
            }
            else if (e.Argument == "CalculatorProgress")
            {
                var projectList = this.scopeProjectService.GetAll();
                //var projectList = new List<ScopeProject>();
                //projectList.Add(this.scopeProjectService.GetById(1925));
                var progressActualWeekList = new List<string>();
                var progressActualMonthList = new List<string>();
                var progressPlanedWeekList = new List<string>();
                var progressPlanedMonthList = new List<string>();
                var currentMonth = 0;
                var weekInMonth = 0;
                var countWeek = 0;
                var countMonth = 0;
                var tempValue = 0.0;
                var newMonth = false;
                foreach (var projectObj in projectList)
                {
                    var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                    foreach (var wpObj in wpList)
                    {
                        progressActualWeekList = new List<string>();
                        progressActualMonthList = new List<string>();
                        progressPlanedWeekList = new List<string>();
                        progressPlanedMonthList = new List<string>();
                        var progressPlanObj = this.processPlanedService.GetByWP(wpObj.ID);
                        if (progressPlanObj != null) //Neu wp ton tai, kt project theo tuan hay thang
                        {
                            if (projectObj.WeeklyProgress.GetValueOrDefault()) //project theo tuan, tu tinh gia tri cho thang
                            {
                                //tinh cho Plan
                                progressPlanedWeekList = progressPlanObj.Planed.Split('$').ToList();
                                currentMonth = 0;
                                weekInMonth = 0;
                                countWeek = 0;
                                countMonth = 0;
                                tempValue = 0.0;
                                newMonth = false;
                                for (var i = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                         i < projectObj.Deadline.GetValueOrDefault();
                                         i = i.AddDays(7))
                                {
                                    if (progressPlanedWeekList.Count > countWeek)
                                    {
                                        if (currentMonth != i.Month)
                                        {
                                            double temp;
                                            tempValue = Double.TryParse(progressPlanedWeekList[countWeek], out temp) ? Double.Parse(progressPlanedWeekList[countWeek]) : 0;
                                            weekInMonth = 1;
                                            countMonth++;
                                            currentMonth = i.Month;
                                            newMonth = true;
                                        }
                                        else
                                        {
                                            newMonth = false;
                                            weekInMonth++;
                                            double temp;
                                            tempValue += Double.TryParse(progressPlanedWeekList[countWeek], out temp) ? Double.Parse(progressPlanedWeekList[countWeek]) : 0;
                                        }
                                        if (newMonth)
                                        {
                                            progressPlanedMonthList.Add((tempValue).ToString());
                                        }
                                        else
                                        {
                                            progressPlanedMonthList[countMonth - 1] = (tempValue).ToString();
                                        }
                                        countWeek++;
                                    }
                                }
                                progressPlanObj.Planed = string.Join("$", progressPlanedWeekList);
                                progressPlanObj.PlanedMonth = string.Join("$", progressPlanedMonthList);
                                this.processPlanedService.Update(progressPlanObj);

                                //Tinh cho Actual
                                var progressActualObj = this.processActualService.GetByWP(wpObj.ID);
                                if (progressActualObj != null)
                                {
                                    progressActualWeekList = progressActualObj.Actual.Split('$').ToList();
                                    currentMonth = 0;
                                    weekInMonth = 0;
                                    countWeek = 0;
                                    countMonth = 0;
                                    tempValue = 0.0;
                                    newMonth = false;
                                    for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                                j < projectObj.Deadline.GetValueOrDefault();
                                                j = j.AddDays(7))
                                    {
                                        if (progressActualWeekList.Count > countWeek)
                                        {
                                            var months = j.AddDays(7).Month;
                                            if (currentMonth != months)
                                            {
                                                double temp;
                                                tempValue = Double.TryParse(progressActualWeekList[countWeek], out temp) ? Double.Parse(progressActualWeekList[countWeek]) : 0;
                                                weekInMonth = 1;
                                                countMonth++;
                                                currentMonth = j.AddDays(7).Month;
                                                newMonth = true;
                                            }
                                            else
                                            {
                                                newMonth = false;
                                                weekInMonth++;
                                                double temp;
                                                tempValue = Double.TryParse(progressActualWeekList[countWeek], out temp) ? Double.Parse(progressActualWeekList[countWeek]) : 0;
                                            }
                                            if (newMonth)
                                            {
                                                progressActualMonthList.Add((tempValue).ToString());
                                            }
                                            else
                                            {
                                                progressActualMonthList[countMonth - 1] = (tempValue).ToString();
                                            }
                                            countWeek++;
                                        }
                                    }
                                    progressActualObj.Actual = string.Join("$", progressActualWeekList);
                                    progressActualObj.ActualMonth = string.Join("$", progressActualMonthList);

                                    this.processActualService.Update(progressActualObj);
                                }
                            }
                            else
                            {
                                //tinh cho Plan
                                progressPlanedMonthList = progressPlanObj.PlanedMonth.Split('$').ToList();
                                currentMonth = 0;
                                countWeek = 0;
                                countMonth = 0;
                                List<int> weekinMonth = new List<int>();
                                List<MonthWeekProgress> monthweekList = new List<MonthWeekProgress>();
                                progressPlanedWeekList = new List<string>();
                                for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                            j <= projectObj.Deadline.GetValueOrDefault();
                                            j = j.AddDays(7))
                                {
                                    progressPlanedWeekList.Add("0");
                                    int monthTemp = j.AddDays(7).Month;
                                    if (currentMonth != monthTemp)
                                    {
                                        MonthWeekProgress progress = new MonthWeekProgress();
                                        progress.Month = countMonth;
                                        monthweekList.Add(progress);
                                        countMonth++;
                                        currentMonth = j.AddDays(7).Month;
                                        weekinMonth.Clear();
                                        weekinMonth.Add(countWeek);
                                        var a = j.AddDays(7);
                                        if (currentMonth != j.AddDays(7).Month || j.AddDays(7) > projectObj.Deadline.GetValueOrDefault())
                                        {
                                            monthweekList[countMonth - 1].Week = string.Join("-", weekinMonth);
                                        }
                                    }
                                    else
                                    {
                                        weekinMonth.Add(countWeek);
                                        monthweekList[countMonth - 1].Week = string.Join("-", weekinMonth);
                                    }
                                    countWeek++;
                                    if (countMonth > progressPlanedMonthList.Count)
                                    {
                                        progressPlanedMonthList.Add("0");
                                    }
                                }
                                double temp;
                                var progressPlanedMonthListDouble = progressPlanedMonthList.Select(t => double.TryParse(t, out temp) ? double.Parse(t) : 0).ToList();
                                foreach (var item in monthweekList)
                                {
                                    var monthValue = 0.0;
                                    if (item.Month == 0)
                                    {
                                        monthValue = progressPlanedMonthListDouble[item.Month];
                                    }
                                    else
                                    {
                                        monthValue = progressPlanedMonthListDouble[item.Month] - progressPlanedMonthListDouble[item.Month - 1];
                                    }
                                    weekinMonth = new List<int>();
                                    weekinMonth = item.Week.Split('-').Select(int.Parse).ToList();
                                    foreach (var week in weekinMonth)
                                    {
                                        if (week != 0)
                                        {
                                            progressPlanedWeekList[week] = Math.Round((double.Parse(progressPlanedWeekList[week - 1]) + monthValue / weekinMonth.Count()), 2).ToString();
                                        }
                                        else
                                        {
                                            progressPlanedWeekList[week] = Math.Round((monthValue / weekinMonth.Count()), 2).ToString();
                                        }
                                    }
                                }

                                progressPlanObj.Planed = string.Join("$", progressPlanedWeekList);
                                progressPlanObj.PlanedMonth = string.Join("$", progressPlanedMonthList);
                                this.processPlanedService.Update(progressPlanObj);

                                var progressActualObj = this.processActualService.GetByWP(wpObj.ID);
                                if (progressActualObj != null)
                                {
                                    //Tinh cho actual
                                    progressActualMonthList = progressActualObj.ActualMonth.Split('$').ToList();
                                    currentMonth = 0;
                                    countWeek = 0;
                                    countMonth = 0;
                                    weekinMonth = new List<int>();
                                    monthweekList = new List<MonthWeekProgress>();
                                    progressActualWeekList = new List<string>();
                                    for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                                j <= projectObj.Deadline.GetValueOrDefault();
                                                j = j.AddDays(7))
                                    {
                                        progressActualWeekList.Add("0");
                                        if (currentMonth != j.AddDays(7).Month)
                                        {
                                            MonthWeekProgress progress = new MonthWeekProgress();
                                            progress.Month = countMonth;
                                            monthweekList.Add(progress);
                                            countMonth++;
                                            currentMonth = j.AddDays(7).Month;
                                            weekinMonth.Clear();
                                            weekinMonth.Add(countWeek);
                                            if (currentMonth != j.AddDays(7).Month || j.AddDays(7) > projectObj.Deadline.GetValueOrDefault())
                                            {
                                                monthweekList[countMonth - 1].Week = string.Join("-", weekinMonth);
                                            }
                                        }
                                        else
                                        {
                                            weekinMonth.Add(countWeek);
                                            monthweekList[countMonth - 1].Week = string.Join("-", weekinMonth);
                                        }
                                        countWeek++;
                                        if (countMonth > progressActualMonthList.Count)
                                        {
                                            progressActualMonthList.Add("0");
                                        }
                                    }
                                    var progressActualMonthListDouble = progressActualMonthList.Select(t => double.TryParse(t, out temp) ? double.Parse(t) : 0).ToList();
                                    foreach (var item in monthweekList)
                                    {
                                        var monthValue = 0.0;
                                        if (item.Month == 0)
                                        {
                                            monthValue = progressActualMonthListDouble[item.Month];
                                        }
                                        else
                                        {
                                            monthValue = progressActualMonthListDouble[item.Month] - progressActualMonthListDouble[item.Month - 1];
                                        }
                                        weekinMonth = new List<int>();
                                        weekinMonth = item.Week.Split('-').Select(int.Parse).ToList();
                                        foreach (var week in weekinMonth)
                                        {
                                            if (week != 0)
                                            {
                                                progressActualWeekList[week] = Math.Round((double.Parse(progressActualWeekList[week - 1]) + monthValue / weekinMonth.Count()), 2).ToString();
                                            }
                                            else
                                            {
                                                progressActualWeekList[week] = Math.Round((monthValue / weekinMonth.Count()), 2).ToString();
                                            }
                                        }
                                    }
                                    progressActualObj.Actual = string.Join("$", progressActualWeekList);
                                    progressActualObj.ActualMonth = string.Join("$", progressActualMonthList);

                                    this.processActualService.Update(progressActualObj);
                                }
                            }
                        }
                        else //Neu wp chua co plan thi ve duong tuyen tinh
                        {
                            if (!string.IsNullOrEmpty(projectObj.StartDate.ToString()) && !string.IsNullOrEmpty(projectObj.Deadline.ToString()))
                            {
                                var day = projectObj.Deadline - projectObj.StartDate;
                                if (day.Value.Days > 7)
                                {
                                    var numberofWeek = 0;
                                    var numberofMonth = 0;
                                    for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                        j < projectObj.Deadline.GetValueOrDefault();
                                        j = j.AddDays(7))
                                    {
                                        numberofWeek += 1;
                                    }
                                    currentMonth = 0;
                                    for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                                j < projectObj.Deadline.GetValueOrDefault();
                                                j = j.AddDays(7))
                                    {
                                        if (currentMonth != j.AddDays(7).Month)
                                        {
                                            currentMonth = j.AddDays(7).Month;
                                            numberofMonth += 1;
                                        }
                                    }
                                    var avgWeek = numberofWeek != 0 ? Math.Round((decimal)(100 / numberofWeek), 2) : 0;
                                    var avgMonth = numberofMonth != 0 ? Math.Round((decimal)(100 / numberofMonth), 2) : 0;
                                    progressPlanedWeekList = new List<string>();
                                    progressPlanedMonthList = new List<string>();
                                    decimal tempDecimalValue = 0;
                                    for (int i = 0; i < numberofWeek; i++)
                                    {
                                        if (i == (numberofWeek - 1))
                                        {
                                            tempDecimalValue += avgWeek;
                                            progressPlanedWeekList.Add("100");
                                        }
                                        else
                                        {
                                            tempDecimalValue += avgWeek;
                                            progressPlanedWeekList.Add(tempValue.ToString());
                                        }
                                    }
                                    tempDecimalValue = 0;
                                    for (int i = 0; i < numberofMonth; i++)
                                    {
                                        if (i == (numberofMonth - 1))
                                        {
                                            tempDecimalValue += avgMonth;
                                            progressPlanedMonthList.Add("100");
                                        }
                                        else
                                        {
                                            tempDecimalValue += avgMonth;
                                            progressPlanedMonthList.Add(tempValue.ToString());
                                        }
                                    }
                                    var progressPlaned = new ProcessPlaned();
                                    progressPlaned.ProjectId = wpObj.ProjectId;
                                    progressPlaned.WorkgroupId = wpObj.ID;
                                    progressPlaned.Planed = string.Join("$", progressPlanedWeekList);
                                    progressPlaned.PlanedMonth = string.Join("$", progressPlanedMonthList);

                                    this.processPlanedService.Insert(progressPlaned);
                                }
                            }
                        }
                    }
                }
            }
            else if (e.Argument == "CalculatorWMMPlan")
            {
                var processPlanList = this.processPlanedService.GetAllByProject(Convert.ToInt32(this.ddlProject.SelectedValue));
                foreach (var processPlanObj in processPlanList)
                {
                    var planValue = processPlanObj.PlanedMonth.Split('$');
                    var projectObj = this.scopeProjectService.GetById(processPlanObj.ProjectId.GetValueOrDefault());
                    var wpObj = this.workGroupService.GetById(processPlanObj.WorkgroupId.GetValueOrDefault());
                    var docList = this.documentPackageService.GetAllEMDRByWorkgroup(processPlanObj.WorkgroupId.GetValueOrDefault());
                    var totalManhourPlanWP = docList.Sum(t => t.ManHourPlan);
                    if (projectObj != null)
                    {
                        if (!string.IsNullOrEmpty(projectObj.StartDate.GetValueOrDefault().ToString()))
                        {
                            if (wpObj != null)
                            {
                                for (int i = 0; i < planValue.Length; i++)
                                {
                                    var date = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault()).AddMonths(i);
                                    int monthPJ = date.Month;
                                    int yearPJ = date.Year;
                                    int monthPrevPJ = date.AddMonths(-1).Month;
                                    int yearPrevPJ = date.AddMonths(-1).Year;
                                    var wmmObj = this.workgroupManhourMonthService.GetByWP(wpObj.ID, monthPJ, yearPJ);
                                    var prevWMMObj = this.workgroupManhourMonthService.GetByWP(wpObj.ID, monthPrevPJ, yearPrevPJ);
                                    var doclist = this.documentPackageService.GetAllEMDRByWorkgroup(wpObj.ID);
                                    if (wmmObj != null)
                                    {
                                        wmmObj.Month = monthPJ;
                                        wmmObj.Year = yearPJ;
                                        wmmObj.WorkgroupID = wpObj.ID;
                                        wmmObj.WorkgroupName = wpObj.Name;
                                        wmmObj.ProjectID = projectObj.ID;
                                        wmmObj.DeparmentID = wpObj.DepartmentId;
                                        wmmObj.UpdateBy = UserSession.Current.User.Id;
                                        wmmObj.UpdateDate = DateTime.Now;
                                        wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                                        wmmObj.ManhourPlanTotal = doclist.Sum(t => t.ManHourPlan.GetValueOrDefault());
                                        wmmObj.CompletePlanTotal = Convert.ToDouble(planValue[i]);
                                        if (prevWMMObj != null)
                                        {
                                            double plan = Convert.ToDouble(planValue[i]) - prevWMMObj.CompletePlanTotal.GetValueOrDefault();
                                            wmmObj.CompletePlanMonth = plan;
                                            if (plan > 0)
                                            {
                                                wmmObj.ManhourPlanMonth = (plan * totalManhourPlanWP) / 100;
                                            }
                                        }
                                        else
                                        {
                                            wmmObj.CompletePlanMonth = Convert.ToDouble(planValue[i]);
                                            if (Convert.ToDouble(planValue[i]) > 0)
                                            {
                                                wmmObj.ManhourPlanMonth = (Convert.ToDouble(planValue[i]) * totalManhourPlanWP) / 100;
                                            }
                                        }
                                        this.workgroupManhourMonthService.Update(wmmObj);
                                    }
                                    else
                                    {
                                        WorkgroupManhourMonth workgroupManhourMonthObj = new WorkgroupManhourMonth();
                                        workgroupManhourMonthObj.Month = monthPJ;
                                        workgroupManhourMonthObj.Year = yearPJ;
                                        workgroupManhourMonthObj.WorkgroupID = wpObj.ID;
                                        workgroupManhourMonthObj.WorkgroupName = wpObj.Name;
                                        workgroupManhourMonthObj.ProjectID = projectObj.ID;
                                        workgroupManhourMonthObj.DeparmentID = wpObj.DepartmentId;
                                        workgroupManhourMonthObj.CreateBy = UserSession.Current.User.Id;
                                        workgroupManhourMonthObj.CreateDate = DateTime.Now;
                                        workgroupManhourMonthObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                                        workgroupManhourMonthObj.ManhourPlanTotal = doclist.Sum(t => t.ManHourPlan.GetValueOrDefault());
                                        workgroupManhourMonthObj.CompletePlanTotal = Convert.ToDouble(planValue[i]);
                                        if (prevWMMObj != null)
                                        {
                                            double plan = Convert.ToDouble(planValue[i]) - prevWMMObj.CompletePlanTotal.GetValueOrDefault();
                                            workgroupManhourMonthObj.CompletePlanMonth = plan;
                                            if (plan > 0)
                                            {
                                                workgroupManhourMonthObj.ManhourPlanMonth = (plan * totalManhourPlanWP) / 100;
                                            }
                                        }
                                        else
                                        {
                                            workgroupManhourMonthObj.CompletePlanMonth = Convert.ToDouble(planValue[i]);
                                            if (Convert.ToDouble(planValue[i]) > 0)
                                            {
                                                workgroupManhourMonthObj.ManhourPlanMonth = (Convert.ToDouble(planValue[i]) * totalManhourPlanWP) / 100;
                                            }
                                        }
                                        this.workgroupManhourMonthService.Insert(workgroupManhourMonthObj);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (e.Argument == "CalculatorWMMReal")
            {
                List<int> lst = new List<int>() { 1, 2, 3, 4, 5, 6, 12, 27 };
                foreach (var lstItem in lst)
                {
                    var workList = this.workService.GetByParent(lstItem).Select(t => t.ID).ToList();
                    var pjList = this.projectAppendixService.GetByWork(workList, 2019).Select(t => t.ProjectID.GetValueOrDefault()).ToList();
                    var processActualList = this.processActualService.GetAllByProject(pjList);
                    foreach (var processActualObj in processActualList)
                    {
                        var actualValue = processActualObj.ActualMonth.Split('$');
                        var projectObj = this.scopeProjectService.GetById(processActualObj.ProjectId.GetValueOrDefault());
                        var wpObj = this.workGroupService.GetById(processActualObj.WorkgroupId.GetValueOrDefault());
                        var docList = this.documentPackageService.GetAllEMDRByWorkgroup(processActualObj.WorkgroupId.GetValueOrDefault());
                        var totalManhourPlanWP = docList.Sum(t => t.ManHourPlan);
                        if (projectObj != null)
                        {
                            if (!string.IsNullOrEmpty(projectObj.StartDate.GetValueOrDefault().ToString()))
                            {
                                if (wpObj != null)
                                {
                                    for (int i = 0; i < actualValue.Length; i++)
                                    {
                                        var date = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault()).AddMonths(i);
                                        int monthPJ = date.Month;
                                        int yearPJ = date.Year;
                                        int monthPrevPJ = date.AddMonths(-1).Month;
                                        int yearPrevPJ = date.AddMonths(-1).Year;
                                        var wmmObj = this.workgroupManhourMonthService.GetByWP(wpObj.ID, monthPJ, yearPJ);
                                        var prevWMMObj = this.workgroupManhourMonthService.GetByWP(wpObj.ID, monthPrevPJ, yearPrevPJ);
                                        if (wmmObj != null)
                                        {
                                            wmmObj.Month = monthPJ;
                                            wmmObj.Year = yearPJ;
                                            wmmObj.WorkgroupID = wpObj.ID;
                                            wmmObj.WorkgroupName = wpObj.Name;
                                            wmmObj.ProjectID = projectObj.ID;
                                            wmmObj.DeparmentID = wpObj.DepartmentId;
                                            wmmObj.UpdateBy = UserSession.Current.User.Id;
                                            wmmObj.UpdateDate = DateTime.Now;
                                            wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                                            wmmObj.CompleteActualTotal = Convert.ToDouble(actualValue[i]);
                                            if (prevWMMObj != null)
                                            {
                                                wmmObj.ManhourActualAccumulation = (Convert.ToDouble(actualValue[i]) * totalManhourPlanWP) / 100;
                                                double plan = Convert.ToDouble(actualValue[i]) - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                                                wmmObj.CompleteActualMonth = plan;
                                                if (prevWMMObj != null)
                                                {
                                                    wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                                }
                                                else
                                                {
                                                    wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                                                }
                                            }
                                            else
                                            {
                                                wmmObj.CompleteActualMonth = Convert.ToDouble(actualValue[i]);
                                                wmmObj.ManhourActualAccumulation = (Convert.ToDouble(actualValue[i]) * totalManhourPlanWP) / 100;
                                                if (prevWMMObj != null)
                                                {
                                                    wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                                }
                                                else
                                                {
                                                    wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                                                }
                                            }
                                            this.workgroupManhourMonthService.Update(wmmObj);
                                        }
                                        else
                                        {
                                            WorkgroupManhourMonth workgroupManhourMonthObj = new WorkgroupManhourMonth();
                                            workgroupManhourMonthObj.Month = monthPJ;
                                            workgroupManhourMonthObj.Year = yearPJ;
                                            workgroupManhourMonthObj.WorkgroupID = wpObj.ID;
                                            workgroupManhourMonthObj.WorkgroupName = wpObj.Name;
                                            workgroupManhourMonthObj.ProjectID = projectObj.ID;
                                            workgroupManhourMonthObj.DeparmentID = wpObj.DepartmentId;
                                            workgroupManhourMonthObj.CreateBy = UserSession.Current.User.Id;
                                            workgroupManhourMonthObj.CreateDate = DateTime.Now;
                                            workgroupManhourMonthObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                                            workgroupManhourMonthObj.CompleteActualTotal = Convert.ToDouble(actualValue[i]);
                                            if (prevWMMObj != null)
                                            {
                                                double plan = Convert.ToDouble(actualValue[i]) - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                                                workgroupManhourMonthObj.CompleteActualMonth = plan;
                                                workgroupManhourMonthObj.ManhourActualAccumulation = (Convert.ToDouble(actualValue[i]) * totalManhourPlanWP) / 100;
                                                if (prevWMMObj != null)
                                                {
                                                    workgroupManhourMonthObj.ManhourActualMonth = workgroupManhourMonthObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                                }
                                                else
                                                {
                                                    workgroupManhourMonthObj.ManhourActualMonth = workgroupManhourMonthObj.ManhourActualAccumulation.GetValueOrDefault();
                                                }
                                            }
                                            else
                                            {
                                                workgroupManhourMonthObj.CompleteActualMonth = Convert.ToDouble(actualValue[i]);
                                                workgroupManhourMonthObj.ManhourActualAccumulation = (Convert.ToDouble(actualValue[i]) * totalManhourPlanWP) / 100;
                                                if (prevWMMObj != null)
                                                {
                                                    workgroupManhourMonthObj.ManhourActualMonth = workgroupManhourMonthObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                                }
                                                else
                                                {
                                                    workgroupManhourMonthObj.ManhourActualMonth = workgroupManhourMonthObj.ManhourActualAccumulation.GetValueOrDefault();
                                                }
                                            }
                                            this.workgroupManhourMonthService.Insert(workgroupManhourMonthObj);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if(e.Argument == "UpdateLastedWMMByNow")
            {
                DateTime date = DateTime.Now;
                int currentMonth = date.Month;
                int currentYear = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                var projectList = this.scopeProjectService.GetAll();
                foreach (var projectObj in projectList)
                {
                    var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                    foreach (var wpObj in wpList)
                    {
                        var wmmObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, projectObj.ID, currentMonth, currentYear);
                        var docList = this.documentPackageService.GetAllEMDRByWorkgroup(wpObj.ID);
                        var manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                        if (wmmObj!=null)
                        {
                            var prevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(projectObj.ID, wpObj.ID, currentMonth, currentYear);
                            wmmObj.ManhourPlanTotal = manhourPlanTotal;
                            wmmObj.ManhourPlanMonth = (wmmObj.ManhourPlanTotal.GetValueOrDefault() * wmmObj.CompletePlanMonth.GetValueOrDefault()) / 100;
                            wmmObj.CompleteActualTotal = wpObj.Complete.GetValueOrDefault();
                            wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                            if (prevWMMObj != null)
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            else
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            this.workgroupManhourMonthService.Update(wmmObj);
                        }
                        else
                        {
                            var wmmLastedObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(projectObj.ID, wpObj.ID, currentMonth, currentYear);
                            if(wmmLastedObj!=null)
                            {
                                wmmLastedObj.CompleteActualTotal = wpObj.Complete.GetValueOrDefault();
                                wmmLastedObj.ManhourActualAccumulation= (wmmLastedObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                                this.workgroupManhourMonthService.Update(wmmLastedObj);
                            }
                        }
                    }
                }
            }
            else if (e.Argument.Contains("ExportProgress"))
            {
                var type = e.Argument.Split('_')[1];


                var projectId = this.ddlProject.SelectedItem != null
                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                    : 0;


                var projectObj = this.scopeProjectService.GetById(projectId);
                if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null && !UserSession.Current.IsEngineer)
                {
                    var listWorkgroupInPermission = WorkpackageList.Where(t => t.ProjectId == projectId).ToList();

                    var dateList = new List<DateTime>();

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    if (type == "Planed")
                    {
                        workbook.Open(filePath + @"Template\ProgressPlanedTemplate.xls");
                    }
                    else
                    {
                        workbook.Open(filePath + @"Template\ProgressActualTemplate.xls");
                    }
                    var wsProgress = workbook.Worksheets[0];

                    wsProgress.Cells["A7"].PutValue(type == "Planed" ? "Planed" : "Actual");
                    wsProgress.Cells["A1"].PutValue(projectObj.ID);
                    wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR " + projectObj.Description);
                    var workgroupCount = 0;
                    var startDate = projectObj.StartDate.GetValueOrDefault();
                    //while (startDate.DayOfWeek != DayOfWeek.Monday)
                    //{
                    //    startDate = startDate.AddDays(1);
                    //}

                    var currentMonth = 0;
                    var countMerge = 0;
                    var count = 3;
                    if (projectObj.WeeklyProgress.GetValueOrDefault())
                    {
                        for (var j = GetFridayOfWeek(startDate);
                                j < projectObj.Deadline.GetValueOrDefault();
                                j = j.AddDays(7))
                        {
                            if (currentMonth != j.AddDays(7).Month)
                            {
                                wsProgress.Cells[2, count].PutValue(j.AddDays(7));
                                currentMonth = j.AddDays(7).Month;

                                if (countMerge != 0)
                                {
                                    wsProgress.Cells.Merge(2, count - countMerge, 1, countMerge);
                                }

                                countMerge = 1;
                            }
                            else
                            {
                                countMerge += 1;
                            }

                            wsProgress.Cells[3, count].PutValue(count - 2);
                            wsProgress.Cells[4, count].PutValue(j);
                            wsProgress.Cells[5, count].PutValue(j.AddDays(7));

                            count += 1;
                        }

                        if (listWorkgroupInPermission.Count > 0)
                        {
                            wsProgress.Cells.InsertRows(8, listWorkgroupInPermission.Count);
                            for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                            {
                                var workgroup = listWorkgroupInPermission[i];

                                wsProgress.Cells["A" + (8 + i)].PutValue(workgroup.ID);
                                wsProgress.Cells["B" + (8 + i)].PutValue(workgroup.Name);
                                wsProgress.Cells["C" + (8 + i)].PutValue(type == "Planed" ? "Planed (%)" : "Actual (%)");

                                if (type == "Planed")
                                {
                                    var progressPlaned = this.processPlanedService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                                    if (progressPlaned != null)
                                    {
                                        var planedList = progressPlaned.Planed.Split('$');
                                        for (int j = 0; j < planedList.Count(); j++)
                                        {
                                            wsProgress.Cells[workgroupCount + 7, j + 3].PutValue(!string.IsNullOrEmpty(planedList[j]) ? Convert.ToDouble(planedList[j]) / 100 : 0);
                                        }
                                    }
                                }
                                else
                                {
                                    var progressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                                    if (progressActual != null)
                                    {
                                        var actualList = progressActual.Actual.Split('$');
                                        for (int j = 0; j < actualList.Count(); j++)
                                        {
                                            wsProgress.Cells[workgroupCount + 7, j + 3].PutValue(!string.IsNullOrEmpty(actualList[j]) ? Convert.ToDouble(actualList[j]) / 100 : 0);
                                        }
                                    }
                                }

                                workgroupCount += 1;
                            }

                            wsProgress.Cells.Merge(0, 2, 1, count - 2);
                            wsProgress.Cells.Merge(1, 2, 1, count - 2);
                        }
                    }
                    else
                    {
                        //for (var j = startDate;
                        //        j < projectObj.Deadline.GetValueOrDefault();
                        //        j = j.AddMonths(1))
                        //{
                        //    wsProgress.Cells[2, count].PutValue(j);
                        //    count += 1;
                        //}
                        currentMonth = 0;
                        for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                    j < projectObj.Deadline.GetValueOrDefault();
                                    j = j.AddDays(7))
                        {
                            if (currentMonth != j.AddDays(7).Month)
                            {
                                currentMonth = j.AddDays(7).Month;
                                wsProgress.Cells[2, count].PutValue(j.AddDays(7));
                                count += 1;
                            }

                        }
                        if (listWorkgroupInPermission.Count > 0)
                        {
                            wsProgress.Cells.InsertRows(8, listWorkgroupInPermission.Count);
                            for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                            {
                                var workgroup = listWorkgroupInPermission[i];

                                wsProgress.Cells["A" + (8 + i)].PutValue(workgroup.ID);
                                wsProgress.Cells["B" + (8 + i)].PutValue(workgroup.Name);
                                wsProgress.Cells["C" + (8 + i)].PutValue(type == "Planed" ? "Planed (%)" : "Actual (%)");

                                if (type == "Planed")
                                {
                                    var progressPlaned = this.processPlanedService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                                    if (progressPlaned != null)
                                    {
                                        var planedList = progressPlaned.PlanedMonth.Split('$');
                                        for (int j = 0; j < planedList.Count(); j++)
                                        {
                                            wsProgress.Cells[workgroupCount + 7, j + 3].PutValue(!string.IsNullOrEmpty(planedList[j]) ? Convert.ToDouble(planedList[j]) / 100 : 0);
                                        }
                                    }
                                }
                                else
                                {
                                    var progressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                                    if (progressActual != null)
                                    {
                                        var actualList = progressActual.ActualMonth.Split('$');
                                        for (int j = 0; j < actualList.Count(); j++)
                                        {
                                            wsProgress.Cells[workgroupCount + 7, j + 3].PutValue(!string.IsNullOrEmpty(actualList[j]) ? Convert.ToDouble(actualList[j]) / 100 : 0);
                                        }
                                    }
                                }

                                workgroupCount += 1;
                            }

                            wsProgress.Cells.Merge(0, 2, 1, count - 2);
                            wsProgress.Cells.Merge(1, 2, 1, count - 2);
                        }
                    }
                    // Save and export file
                    var filename = (this.ddlProject.SelectedItem != null
                            ? Utilities.Utility.RemoveAllSpecialCharacter(this.ddlProject.SelectedItem.Text) + "$"
                            : string.Empty)
                            + (type == "Planed" ? "ProgressPlaned.xls" : "ProgressActual.xls");
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument == "MonthReport")
            {
                var milestoneListOfMonth = isSeeFull
                    ? this.milestoneService.GetAllByMonth(month, year)
                    : this.milestoneService.GetAllByMonth(month, year, UserSession.Current.RoleId);
                //var milestoneListOfMonth = this.milestoneService.GetAllByMonth(month, year);
                if (milestoneListOfMonth != null && milestoneListOfMonth.Count > 0)
                {
                    var workpackageList = milestoneListOfMonth
                                        .Select(t => t.WorkpackageId)
                                        .Distinct()
                                        .Select(t => this.workGroupService.GetById(t.GetValueOrDefault()));

                    var wpPlannedList = workpackageList.Where(t => t.TypeId == 1).ToList();
                    var wpUnPlannedList = workpackageList.Where(t => t.TypeId == 2).ToList();
                    var wpOtherList = workpackageList.Where(t => t.TypeId == 0).ToList();
                    var wpCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("Text1", Type.GetType("System.String")),
                                new DataColumn("Plan", Type.GetType("System.String")),
                                new DataColumn("PlanTotal", Type.GetType("System.String")),
                                new DataColumn("EndDate", Type.GetType("System.String")),
                                new DataColumn("OutgoingNumber", Type.GetType("System.String")),
                                new DataColumn("Real", Type.GetType("System.String")),
                                new DataColumn("RealTotal", Type.GetType("System.String")),
                                new DataColumn("RealPerPlan", Type.GetType("System.String")),
                                new DataColumn("TotalRealPerTotalPlan", Type.GetType("System.String")),
                                new DataColumn("MilestoneDate", Type.GetType("System.String")),
                                new DataColumn("TotalManHours", Type.GetType("System.String")),
                                new DataColumn("UsedManHours", Type.GetType("System.String")),
                                new DataColumn("PerformingUser", Type.GetType("System.String")),
                                new DataColumn("Note", Type.GetType("System.String")),
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("DepartmentR", Type.GetType("System.String")),
                                new DataColumn("MonthR", Type.GetType("System.String")),
                                new DataColumn("Year", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var deparment = this.roleService.GetByID(UserSession.Current.RoleId);

                    var infoItem = reportInfo.NewRow();
                    infoItem["DepartmentR"] = deparment != null ? deparment.RussiaName : string.Empty;
                    infoItem["MonthR"] = Utility.MonthR[DateTime.Now.Month];
                    infoItem["Year"] = DateTime.Now.Year.ToString();
                    reportInfo.Rows.Add(infoItem);


                    var dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "1";
                    dataworkpackageItem["WorkPackageContent"] = "Плановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    foreach (var workpackage in wpPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData(workpackage, milestoneObj, ref dataTable, wpCount, "1.");
                        }
                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "2";
                    dataworkpackageItem["WorkPackageContent"] = "Внеплановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpUnPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData(workpackage, milestoneObj, ref dataTable, wpCount, "2.");
                        }

                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "3";
                    dataworkpackageItem["WorkPackageContent"] = "Другие работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpOtherList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData(workpackage, milestoneObj, ref dataTable, wpCount, "3.");
                        }

                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = "MonthReportTemplate.doc";
                    var strOutputFileName = "MonthReport_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);

                    // Apply style for rows have RealTotal == 100%
                    var doc = new Document(rootPath + WordPathExport + strOutputFileName);
                    var table = (Table)doc.GetChild(NodeType.Table, 2, true);

                    for (var i = 2; i < table.Rows.Count; i++)
                    {
                        if (table.Rows[i].Cells.Count > 12)
                        {
                            var temp = table.Rows[i].Cells[8].GetText();
                            if (temp.Contains("100%"))
                            {
                                foreach (Aspose.Words.Tables.Cell cell in table.Rows[i].Cells)
                                {
                                    cell.CellFormat.Shading.BackgroundPatternColor = Color.DarkGray;
                                }
                            }
                        }
                    }

                    for (var i = 2; i < table.Rows.Count; i++)
                    {
                        if (table.Rows[i].Cells.Count > 12)
                        {
                            var temp = table.Rows[i].Cells[0].GetText();
                            if (temp == "1\a" || temp == "2\a" || temp == "3\a")
                            {
                                foreach (Aspose.Words.Tables.Cell cell in table.Rows[i].Cells)
                                {
                                    // Get Runs in this cell.
                                    NodeCollection runs = cell.GetChildNodes(NodeType.Run, true);

                                    // Loop through all runs and make them bold.
                                    foreach (Run run in runs)
                                    {
                                        run.Font.Bold = true;
                                    }
                                }
                            }
                        }
                    }

                    doc.Save(rootPath + WordPathExport + strOutputFileName);

                    if (isSuccess)
                    {
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "MonthReport_" + DateTime.Now.ToString("MM-yyyy") + ".doc", true);
                    }

                }
            }
            else if (e.Argument == "NextMonthReport")
            {
                var milestoneListOfMonth = isSeeFull
                    ? this.milestoneService.GetAllByMonth(nextMonth, nextYear)
                    : this.milestoneService.GetAllByMonth(nextMonth, nextYear, UserSession.Current.RoleId);
                //var milestoneListOfMonth = this.milestoneService.GetAllByMonth(nextMonth, nextYear);
                if (milestoneListOfMonth != null && milestoneListOfMonth.Count > 0)
                {
                    var workpackageList = milestoneListOfMonth
                                        .Select(t => t.WorkpackageId)
                                        .Distinct()
                                        .Select(t => this.workGroupService.GetById(t.GetValueOrDefault()));

                    var wpPlannedList = workpackageList.Where(t => t.TypeId == 1).ToList();
                    var wpUnPlannedList = workpackageList.Where(t => t.TypeId == 2).ToList();
                    var wpOtherList = workpackageList.Where(t => t.TypeId == 0).ToList();
                    var wpCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("Text1", Type.GetType("System.String")),
                                new DataColumn("Plan", Type.GetType("System.String")),
                                new DataColumn("PlanTotal", Type.GetType("System.String")),
                                new DataColumn("MaxPlan", Type.GetType("System.String")),
                                new DataColumn("MilestoneDate", Type.GetType("System.String")),
                                new DataColumn("PerformingUser", Type.GetType("System.String")),
                                new DataColumn("Note", Type.GetType("System.String")),
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("DepartmentR", Type.GetType("System.String")),
                                new DataColumn("MonthR", Type.GetType("System.String")),
                                new DataColumn("Year", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var deparment = this.roleService.GetByID(UserSession.Current.RoleId);

                    var infoItem = reportInfo.NewRow();
                    infoItem["DepartmentR"] = deparment != null ? deparment.RussiaName : string.Empty;
                    infoItem["MonthR"] = Utility.MonthR[DateTime.Now.AddMonths(1).Month];
                    infoItem["Year"] = DateTime.Now.AddMonths(1).Year.ToString();
                    reportInfo.Rows.Add(infoItem);


                    var dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "1";
                    dataworkpackageItem["WorkPackageContent"] = "Плановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    foreach (var workpackage in wpPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData1(workpackage, milestoneObj, ref dataTable, wpCount, "1.");
                        }
                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "2";
                    dataworkpackageItem["WorkPackageContent"] = "Внеплановые работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpUnPlannedList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData1(workpackage, milestoneObj, ref dataTable, wpCount, "2.");
                        }

                    }

                    dataworkpackageItem = dataTable.NewRow();
                    dataworkpackageItem["Index"] = "3";
                    dataworkpackageItem["WorkPackageContent"] = "Другие работы";
                    dataTable.Rows.Add(dataworkpackageItem);
                    wpCount = 0;
                    foreach (var workpackage in wpOtherList)
                    {
                        var milestoneObj = milestoneListOfMonth.FirstOrDefault(t => t.WorkpackageId == workpackage.ID);
                        if (milestoneObj != null)
                        {
                            wpCount += 1;

                            this.CollectData1(workpackage, milestoneObj, ref dataTable, wpCount, "3.");
                        }

                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = "NextMonthReportTemplate.doc";
                    var strOutputFileName = "NextMonthReport_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);

                    // Apply style for rows have RealTotal == 100%
                    var doc = new Aspose.Words.Document(rootPath + WordPathExport + strOutputFileName);
                    var table = (Aspose.Words.Tables.Table)doc.GetChild(NodeType.Table, 2, true);

                    for (var i = 2; i < table.Rows.Count; i++)
                    {
                        if (table.Rows[i].Cells.Count > 12)
                        {
                            var temp = table.Rows[i].Cells[0].GetText();
                            if (temp == "1\a" || temp == "2\a" || temp == "3\a")
                            {
                                foreach (Aspose.Words.Tables.Cell cell in table.Rows[i].Cells)
                                {
                                    // Get Runs in this cell.
                                    NodeCollection runs = cell.GetChildNodes(NodeType.Run, true);

                                    // Loop through all runs and make them bold.
                                    foreach (Run run in runs)
                                    {
                                        run.Font.Bold = true;
                                    }
                                }
                            }
                        }
                    }

                    doc.Save(rootPath + WordPathExport + strOutputFileName);

                    if (isSuccess)
                    {
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "PlanNextMonthReport_" + (DateTime.Now.Month + 1) + "-" + DateTime.Now.Year + ".doc", true);
                    }
                }
            }
            else if (e.Argument.Contains("FTK01"))
            {
                var selectedProject = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
                if (selectedProject != null)
                {
                    var wpList = this.workGroupService.GetAllWorkGroupOfProject(selectedProject.ID);
                    var wpCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("Department", Type.GetType("System.String")),
                                new DataColumn("WPStartDate", Type.GetType("System.String")),
                                new DataColumn("WPEndDate", Type.GetType("System.String")),
                                new DataColumn("InputDataSupplier", Type.GetType("System.String")),
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("ProjectManager", Type.GetType("System.String")),
                                new DataColumn("Supervisor", Type.GetType("System.String")),
                                new DataColumn("StartDate", Type.GetType("System.String")),
                                new DataColumn("EndDate", Type.GetType("System.String")),
                                new DataColumn("ProjectDescription", Type.GetType("System.String")),
                                new DataColumn("ProjectName", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var infoItem = reportInfo.NewRow();
                    var projectManager = this.userService.GetByID(selectedProject.ProjectManagerId.GetValueOrDefault());

                    infoItem["ProjectManager"] = projectManager != null ? projectManager.FullName : string.Empty;
                    infoItem["Supervisor"] = selectedProject.SupervisorUserName;
                    infoItem["StartDate"] = selectedProject.StartDate != null ? selectedProject.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                    infoItem["EndDate"] = selectedProject.EndDate != null ? selectedProject.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty; ;
                    infoItem["ProjectDescription"] = selectedProject.Description;
                    infoItem["ProjectName"] = selectedProject.Name;
                    reportInfo.Rows.Add(infoItem);

                    foreach (var workpackage in wpList)
                    {
                        var dataworkpackageItem = dataTable.NewRow();
                        wpCount += 1;
                        dataworkpackageItem["Index"] = wpCount;
                        dataworkpackageItem["WorkPackageContent"] = workpackage.Name;
                        dataworkpackageItem["Department"] = workpackage.DepartmentName;
                        dataworkpackageItem["WPStartDate"] = workpackage.StartDate != null ? workpackage.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                        dataworkpackageItem["WPEndDate"] = workpackage.EndDate != null ? workpackage.EndDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                        dataworkpackageItem["InputDataSupplier"] = workpackage.InputDataSupplier;
                        dataTable.Rows.Add(dataworkpackageItem);


                        foreach (var workpackageContent in this.workpackageContentService.GetAllByWorkpackage(workpackage.ID))
                        {
                            var contentItem = dataTable.NewRow();
                            contentItem["WorkPackageContent"] = workpackageContent.ContentInfo;
                            contentItem["WPEndDate"] = workpackageContent.Deadline != null ? workpackageContent.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                            dataTable.Rows.Add(contentItem);
                        }

                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = e.Argument == "FTK01V" ? "F-TK-01-V_Template.doc" : "F-TK-01-R_Template.doc";
                    var strOutputFileName = "F-TK-01_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                    if (isSuccess)
                    {
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "F-TK-01_" + Utility.RemoveSpecialCharacter(selectedProject.Name, "-") + ".doc", true);
                    }

                }
            }
            else if (e.Argument.Contains("FTK02"))
            {
                var selectedWP =
                    this.workGroupService.GetById(this.rtvWorkgroup.SelectedNode != null
                        ? Convert.ToInt32(this.rtvWorkgroup.SelectedValue)
                        : 0);
                if (selectedWP != null)
                {
                    var contentCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                                new DataColumn("DeadLine", Type.GetType("System.String")),
                                new DataColumn("Note", Type.GetType("System.String"))
                            };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                            {
                                new DataColumn("DepartmentName", Type.GetType("System.String")),
                                new DataColumn("DepartmentDescription", Type.GetType("System.String")),
                                new DataColumn("ProjectDescription", Type.GetType("System.String")),
                                new DataColumn("ProjectName", Type.GetType("System.String")),
                                new DataColumn("WorkpackageName", Type.GetType("System.String")),
                                new DataColumn("WPStartDate", Type.GetType("System.String")),
                                new DataColumn("ProjectManager", Type.GetType("System.String")),
                                new DataColumn("InputDocument", Type.GetType("System.String")),
                                new DataColumn("InfoExchange", Type.GetType("System.String")),
                                new DataColumn("ReportMode", Type.GetType("System.String")),
                            };
                    reportInfo.Columns.AddRange(listColumn1);

                    var infoItem = reportInfo.NewRow();
                    var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                    var projectManager = this.userService.GetByID(projectObj != null ? projectObj.ProjectManagerId.GetValueOrDefault() : 0);
                    var deparment = this.roleService.GetByID(selectedWP.DepartmentId.GetValueOrDefault());

                    infoItem["DepartmentName"] = deparment != null ? (e.Argument == "FTK02V" ? deparment.Name : deparment.RussiaName) : string.Empty;
                    infoItem["DepartmentDescription"] = deparment != null ? (e.Argument == "FTK02V" ? deparment.Description : string.Empty) : string.Empty;
                    infoItem["ProjectDescription"] = projectObj != null ? projectObj.Description : string.Empty;
                    infoItem["ProjectName"] = projectObj != null ? projectObj.Name : string.Empty;
                    infoItem["WorkpackageName"] = selectedWP.Name;
                    infoItem["WPStartDate"] = selectedWP.StartDate != null
                        ? (e.Argument == "FTK02V"
                            ? "Ngày " + selectedWP.StartDate.GetValueOrDefault().Day + " Tháng " + selectedWP.StartDate.GetValueOrDefault().Month + " Năm " + selectedWP.StartDate.GetValueOrDefault().Year
                            : selectedWP.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"))
                        : string.Empty;

                    infoItem["ProjectManager"] = projectManager != null ? projectManager.FullName : string.Empty;
                    infoItem["InputDocument"] = selectedWP.InputDocument;
                    infoItem["InfoExchange"] = selectedWP.InformationExchange;
                    infoItem["ReportMode"] = selectedWP.ReportMode;
                    reportInfo.Rows.Add(infoItem);

                    foreach (var workpackageContent in this.workpackageContentService.GetAllByWorkpackage(selectedWP.ID))
                    {
                        var contentItem = dataTable.NewRow();
                        contentCount += 1;
                        contentItem["Index"] = contentCount;
                        contentItem["WorkPackageContent"] = workpackageContent.ContentInfo;
                        contentItem["DeadLine"] = workpackageContent.Deadline != null ? workpackageContent.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                        contentItem["Note"] = workpackageContent.Note;
                        dataTable.Rows.Add(contentItem);
                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = e.Argument == "FTK02V" ? "F-TK-02-V_Template.doc" : "F-TK-02-R_Template.doc";
                    var strOutputFileName = "F-TK-02_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                    if (isSuccess)
                    {
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                        "F-TK-02_" + Utility.RemoveSpecialCharacter(selectedWP.Name, "-") + ".doc", true);
                    }
                }
            }
        }

        private void CollectData(WorkGroup workpackage, Milestone milestone, ref DataTable dt, int wpCount, string index)
        {
            var dataitem = dt.NewRow();
            dataitem["Index"] = index + wpCount;
            dataitem["WorkPackageContent"] = workpackage.Description + "\n" + workpackage.Name;
            dataitem["Text1"] = workpackage.StartDate != null || !string.IsNullOrEmpty(workpackage.IncomingNo)
                ? "Тех.задание ГИПа " + workpackage.IncomingNo + " от " + (workpackage.StartDate != null ? workpackage.StartDate.Value.ToString("dd.MM.yy") : string.Empty)
                : string.Empty;
            dataitem["Plan"] = milestone.MilestoneDate <= workpackage.Deadline
                                            ? "(+)" + milestone.PlanPercent + "%"
                                            : "(-)" + milestone.PlanPercent + "%";
            dataitem["PlanTotal"] = milestone.MilestoneDate <= workpackage.Deadline
                                            ? "(+)" + milestone.PlanTotal + "%"
                                            : "(-)" + milestone.PlanTotal + "%";
            dataitem["EndDate"] = workpackage.Deadline != null
                                            ? workpackage.Deadline.Value.ToString("dd.MM.yyyy")
                                            : string.Empty;
            dataitem["OutgoingNumber"] = milestone.RealTotal != null && milestone.RealTotal == 100
                                                    ? workpackage.OutgoingNo
                                                    : "В работе";
            dataitem["Real"] = milestone.MilestoneDate <= workpackage.Deadline
                                            ? "(+)" + milestone.Real + "%"
                                            : "(-)" + milestone.Real + "%";
            dataitem["RealTotal"] = milestone.MilestoneDate <= workpackage.Deadline
                                            ? "(+)" + milestone.RealTotal + "%"
                                            : "(-)" + milestone.RealTotal + "%";

            dataitem["RealPerPlan"] = milestone.MilestoneDate <= workpackage.Deadline
                                            ? "(+)" + Math.Round((double)((milestone.Real / milestone.PlanPercent) * 100), 0) + "%"
                                            : "(-)" + Math.Round((double)((milestone.Real / milestone.PlanPercent) * 100), 0) + "%";
            dataitem["TotalRealPerTotalPlan"] = milestone.MilestoneDate <= workpackage.Deadline
                                            ? "(+)" + Math.Round((double)((milestone.RealTotal / milestone.PlanTotal) * 100), 0) + "%"
                                            : "(-)" + Math.Round((double)((milestone.RealTotal / milestone.PlanTotal) * 100), 0) + "%";

            dataitem["MilestoneDate"] = milestone.MilestoneDate != null
                                            ? milestone.MilestoneDate.Value.ToString("dd.MM.yy")
                                            : string.Empty;
            dataitem["TotalManHours"] = workpackage.TotalManHours != null
                                            ? Math.Round(workpackage.TotalManHours.Value, 2).ToString()
                                            : string.Empty;
            dataitem["UsedManHours"] = workpackage.UsedManHours != null
                                            ? Math.Round(workpackage.UsedManHours.Value, 2).ToString()
                                            : string.Empty;

            dataitem["PerformingUser"] = milestone.PerformingUser;
            dataitem["Note"] = milestone.Note;

            dt.Rows.Add(dataitem);
        }

        private void CollectData1(WorkGroup workpackage, Milestone milestone, ref DataTable dt, int wpCount, string index)
        {
            var dataitem = dt.NewRow();
            dataitem["Index"] = index + wpCount;
            dataitem["WorkPackageContent"] = workpackage.Description + "\n" + workpackage.Name;
            dataitem["Text1"] = workpackage.StartDate != null || !string.IsNullOrEmpty(workpackage.OutgoingNo)
                ? "Тех.задание ГИПа " + workpackage.OutgoingNo + " от " + (workpackage.StartDate != null ? workpackage.StartDate.Value.ToString("dd.MM.yy") : string.Empty)
                : string.Empty;
            dataitem["Plan"] = milestone.PlanPercent + "%";
            dataitem["PlanTotal"] = milestone.PlanTotal + "%";
            dataitem["MaxPlan"] = "100%";
            dataitem["MilestoneDate"] = milestone.MilestoneDate != null
                                            ? milestone.MilestoneDate.Value.ToString("dd.MM.yyyy")
                                            : string.Empty;
            dataitem["PerformingUser"] = milestone.PerformingUser;
            dataitem["Note"] = milestone.Note;

            dt.Rows.Add(dataitem);
        }
        private void LoadProcessReport(int projectId, int workgroupId)
        {
            //try
            //{

            var lineSeries = this.LineChart.PlotArea.Series[1] as LineSeries;

            var processReportList = new List<ProcessReport>();
            var temp = new List<ProcessReport>();

            var projectObj = this.scopeProjectService.GetById(projectId);
            var processPlanedList = this.processPlanedService.GetAllByProject(projectId, workgroupId);
            var processActualList = this.processActualService.GetAllByProject(projectId, workgroupId);
            if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null)
            {
                if (!projectObj.WeeklyProgress.GetValueOrDefault())
                {
                    //var startDate = projectObj.StartDate.GetValueOrDefault();
                    //var deadline = projectObj.Deadline.GetValueOrDefault();
                    //var diffMonth = ((deadline.Year - startDate.Year) * 12) + deadline.Month - startDate.Month;
                    //for (var j = projectObj.StartDate.GetValueOrDefault();
                    //    j < projectObj.Deadline.GetValueOrDefault();
                    //    j = j.AddMonths(1))
                    //{
                    //    var processReport = new ProcessReport();
                    //    processReport.STRWeekDate = j.ToString("MM/yyyy");
                    //    //List node plan
                    //    processReportList.Add(processReport);
                    //    if (DateTime.Now > j)
                    //    {
                    //        //List node actual(chi lay den thoi diem hien tai)
                    //        temp.Add(processReport);
                    //    }
                    //}

                    var currentMonth = 0;
                    for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                                j < projectObj.Deadline.GetValueOrDefault();
                                j = j.AddDays(7))
                    {
                        if (currentMonth != j.AddDays(7).Month)
                        {
                            currentMonth = j.AddDays(7).Month;
                            var processReport = new ProcessReport();
                            processReport.STRWeekDate = j.AddDays(7).ToString("MM/yyyy");

                            //List node plan
                            processReportList.Add(processReport);
                            if (DateTime.Now > j.AddDays(7))
                            {
                                //List node actual(chi lay den thoi diem hien tai)
                                temp.Add(processReport);
                            }
                        }

                    }
                    //dua du lieu vao node plan
                    foreach (var processPlaned in processPlanedList)
                    {
                        var countPlan = 0;
                        var workgroupObj = this.workGroupService.GetById(processPlaned.WorkgroupId.GetValueOrDefault());
                        if (workgroupObj != null)
                        {
                            if (!processPlaned.PlanedMonth.Contains('$'))
                            {
                                processPlaned.PlanedMonth = "0";
                                this.processPlanedService.Update(processPlaned);
                            }
                            foreach (var planed in processPlaned.PlanedMonth.Split('$').Select(Convert.ToDouble))
                            {
                                if (countPlan < processReportList.Count)
                                {
                                    processReportList[countPlan].Planed += workgroupId == 0
                                        ? (planed * workgroupObj.Weight.GetValueOrDefault()) / 100
                                        : planed;
                                }
                                countPlan += 1;
                            }
                        }
                    }
                    //dua du lieu vao node actual
                    foreach (var processActual in processActualList)
                    {
                        var countActual = 0;
                        var workgroupObj = this.workGroupService.GetById(processActual.WorkgroupId.GetValueOrDefault());
                        if (workgroupObj != null)
                        {
                            if (!processActual.ActualMonth.Contains('$'))
                            {
                                processActual.ActualMonth = "0";
                                this.processActualService.Update(processActual);
                            }
                            foreach (var actual in processActual.ActualMonth.Split('$').Select(Convert.ToDouble).ToList())
                            {
                                if (countActual < temp.Count)
                                {
                                    temp[countActual].Actual += (workgroupId == 0
                                        ? (actual * workgroupObj.Weight.GetValueOrDefault()) / 100
                                        : actual);
                                    countActual++;
                                }
                            }
                        }
                    }

                    LineChart.PlotArea.XAxis.DataLabelsField = "STRWeekDate";
                    LineChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Auto;
                    LineChart.PlotArea.XAxis.Type = Telerik.Web.UI.HtmlChart.AxisType.Auto;
                    LineChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "{0}";
                    LineChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 270;
                    LineChart.PlotArea.XAxis.LabelsAppearance.Step = 1;
                    LineChart.PlotArea.XAxis.LabelsAppearance.Skip = 0;
                    LineChart.PlotArea.XAxis.TitleAppearance.Text = "MONTH";

                    //var wpIDList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID).Select(t => t.ID).ToList();
                    //var milestoneList = this.milestoneService.GetAllByWorkpackage(wpIDList);
                    //var minDate = milestoneList.Min(t => t.MilestoneDate).GetValueOrDefault();
                    //var maxDate = milestoneList.Max(t => t.MilestoneDate).GetValueOrDefault();
                    //var differenceMonth = ((maxDate.Year - minDate.Year) * 12) + maxDate.Month - minDate.Month;
                    //for (int i = 0; i < differenceMonth; i++)
                    //{
                    //    var processReport = new ProcessReport();
                    //    processReport.STRWeekDate = minDate.AddMonths(i).ToString("MM/yyyy");
                    //    var milstoneOfMonth = milestoneList
                    //            .Where(t => t.MilestoneDate.GetValueOrDefault().Month == minDate.AddMonths(i).Month
                    //                        && t.MilestoneDate.GetValueOrDefault().Year == minDate.AddMonths(i).Year).ToList();
                    //    foreach (var milestoneOfWP in milstoneOfMonth.GroupBy(t => t.WorkpackageId))
                    //    {
                    //        var wpObj = this.workGroupService.GetById(milestoneOfWP.Key.GetValueOrDefault());
                    //        if (wpObj != null)
                    //        {
                    //            processReport.Planed += (milestoneOfWP.ToList().Average(t => t.PlanTotal).GetValueOrDefault() * wpObj.Weight.GetValueOrDefault()) / 100;
                    //            processReport.Actual += (milestoneOfWP.ToList().Average(t => t.RealTotal).GetValueOrDefault() * wpObj.Weight.GetValueOrDefault()) / 100;
                    //        }
                    //}
                }
                else
                {
                    for (var i = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                        i < projectObj.Deadline.GetValueOrDefault();
                        i = i.AddDays(7))
                    {
                        var processReport = new ProcessReport();
                        processReport.WeekDate = i.AddDays(7);

                        //List node plan
                        processReportList.Add(processReport);
                        if (DateTime.Now > i.AddDays(7))
                        {
                            //List node actual(chi lay den thoi diem hien tai)
                            temp.Add(processReport);
                        }
                    }

                    //dua du lieu vao node plan
                    foreach (var processPlaned in processPlanedList)
                    {
                        var countPlan = 0;

                        var workgroupObj = this.workGroupService.GetById(processPlaned.WorkgroupId.GetValueOrDefault());
                        if (workgroupObj != null)
                        {
                            foreach (var planed in processPlaned.Planed.Split('$').Select(Convert.ToDouble))
                            {
                                if (countPlan < processReportList.Count)
                                {
                                    processReportList[countPlan].Planed += workgroupId == 0
                                        ? (planed * workgroupObj.Weight.GetValueOrDefault()) / 100
                                        : planed;
                                }
                                countPlan += 1;
                            }
                        }
                    }
                    //dua du lieu vao node actual
                    foreach (var processActual in processActualList)
                    {
                        var countActual = 0;
                        var workgroupObj = this.workGroupService.GetById(processActual.WorkgroupId.GetValueOrDefault());
                        if (workgroupObj != null)
                        {
                            foreach (var actual in processActual.Actual.Split('$').Select(Convert.ToDouble).ToList())
                            {
                                if (countActual < temp.Count)
                                {
                                    temp[countActual].Actual += (workgroupId == 0
                                        ? (actual * workgroupObj.Weight.GetValueOrDefault()) / 100
                                        : actual);
                                    countActual++;
                                }
                            }
                        }
                    }
                    LineChart.PlotArea.XAxis.DataLabelsField = "WeekDate";
                    LineChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Days;
                    LineChart.PlotArea.XAxis.Type = Telerik.Web.UI.HtmlChart.AxisType.Date;
                    LineChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "d";
                    LineChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 270;
                    LineChart.PlotArea.XAxis.LabelsAppearance.Step = 7;
                    LineChart.PlotArea.XAxis.TitleAppearance.Text = "";
                }
                this.LineChart.ChartTitle.Text = "DETAIL ENGINEERING SERVICE FOR " + projectObj.Name +
                                                 " | Cut-off: " +
                                                 DateTime.Now.ToString("dd/MM/yyyy");
                this.LineChart.DataSource = processReportList;
                this.LineChart.DataBind();
                if (lineSeries != null)
                {
                    lineSeries.Items.Clear();

                    foreach (var actual in temp)
                    {
                        if (actual.Actual == 0)
                        {
                            lineSeries.Items.Add((decimal?)null);
                        }
                        else
                        {
                            lineSeries.Items.Add((decimal?)actual.Actual);
                        }
                    }
                }
            }

            //}
            //catch (Exception ex)
            //{

            //}
        }

        private void InitData()
        {
            var wpList = new List<WorkGroup>();
            if (UserSession.Current.IsEngineer)
            {
                var wpInWorkOfEng =
                    this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id)
                        .Select(t => t.WorkgroupId)
                        .Distinct();
                wpList = this.WorkpackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
            }
            else
            {
                wpList = WorkpackageList;
            }

            var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
            //var currentDepartment = UserSession.Current.User.RoleId;

            var projectInPermission = this.scopeProjectService.GetAll(wpList.Select(t => t.ProjectId.GetValueOrDefault()).Distinct().ToList());

            List<int> userSpecial = ConfigurationManager.AppSettings.Get("ChiefandGIP").Split(',').Select(Int32.Parse).ToList();
            if(userSpecial.Contains(UserSession.Current.User.Id))
            {
               
            }
            else if (UserSession.Current.IsGip)
            {
                projectInPermission = projectInPermission.Where(t => t.ProjectManagerId == UserSession.Current.User.Id).ToList();
            }
            var workPackageList = WorkpackageList;

            //if (UserSession.Current.IsEngineer)
            //{
            //    var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
            //    workPackageList = workPackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
            //}

            //if (isSeeFull)
            // {
            //var workPackageList = this.workGroupService.GetAll();

            //var projectList = UserSession.Current.IsGip || UserSession.Current.IsCheif
            //    ? this.scopeProjectService.GetAll().Where(t => t.ProjectManagerId == UserSession.Current.User.Id)
            //    : this.scopeProjectService.GetAll();

            this.ddlProject.DataSource = projectInPermission;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();
            this.ddlProject.SelectedIndex = 0;

            if (projectInPermission.Any())
            {
                this.rtvWorkgroup.DataSource = workPackageList.Where(
                                            t =>
                                                t.ProjectId ==
                                                (!string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                                                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                                    : 0));
                this.rtvWorkgroup.DataTextField = "Name";
                this.rtvWorkgroup.DataValueField = "ID";
                this.rtvWorkgroup.DataFieldID = "ID";
                this.rtvWorkgroup.DataBind();
            }
        }
        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }

                //Mo file de doc
                var fs = new FileStream(strFileName, FileMode.Open);
                var streamLength = Convert.ToInt32(fs.Length);
                var data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        private void NotificationDeadlineDoc()
        {
            var notifi = this.emailNotificationTemplateService.GetByType(Utility.DeadlineDoc);
            if (notifi.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy") != DateTime.Now.ToString("dd/MM/yyyy"))
            {
                DateTime daytomorrow = DateTime.Now.AddDays(1);
                var dealinedocument = this.documentPackageService.GetAll().Where(t => t.Complete < 100 && DateTime.ParseExact(t.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"), "dd/MM/yyyy", null) == DateTime.ParseExact(daytomorrow.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null)).ToList();
                foreach (DocumentPackage docObj in dealinedocument)
                {

                    if (docObj.EngineerId != "0")
                    {
                        var engineer = this.userService.GetByID(docObj.EngineerId);
                        var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());

                        //var emailList = this.userService.GetAllByRoleId(wpObj != null ? wpObj.DepartmentId.GetValueOrDefault() : 0)
                        //    .Where(t => t.IsChief.GetValueOrDefault())
                        //    .Select(t => t.Email)
                        //    .Where(t => !string.IsNullOrEmpty(t)).ToList();

                        if (engineer != null)
                        {

                            var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.DeadlineDoc);
                            if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                            {
                                var smtpClient = new SmtpClient
                                {
                                    DeliveryMethod = SmtpDeliveryMethod.Network,
                                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                                    Host = ConfigurationManager.AppSettings["Host"],
                                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                                };
                                var updatedUser = this.userService.GetByID(UserSession.Current.User.Id);
                                var subject = notificationTemplate.Subject.Replace("#DocNumber#", docObj.DocNo);

                                var message = new MailMessage();
                                message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                                message.Subject = subject;
                                message.BodyEncoding = new UTF8Encoding();
                                message.IsBodyHtml = true;
                                message.Body = notificationTemplate.Contents
                                    .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                                    .Replace("#DocNumber#", docObj.DocNo)
                                    .Replace("#DocTitle#", docObj.DocTitle)
                                    .Replace("#DocType#", docObj.DocumentTypeName)
                                    .Replace("#DocRev#", docObj.RevisionName)
                                    .Replace("#DocEng#", engineer.FullName)
                                    .Replace("#DocStartDate#", docObj.StartDate != null ? docObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                                    .Replace("#DocDeadline#", docObj.Deadline != null ? docObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                                    .Replace("#DocWeight#", docObj.Weight != null ? docObj.Weight.Value.ToString() : string.Empty)
                                    .Replace("#DocComplete#", docObj.Complete != null ? docObj.Complete.Value.ToString() : string.Empty);

                                if (!string.IsNullOrEmpty(engineer.Email))
                                {
                                    message.To.Add(new MailAddress(engineer.Email));
                                    smtpClient.Send(message);
                                }

                            }
                        }
                    }
                }
                notifi.UpdatedDate = DateTime.Now;
                this.emailNotificationTemplateService.Update(notifi);
            }

        }
        private void NotificationDeadlineWp()
        {
            var notifi = this.emailNotificationTemplateService.GetByType(Utility.DeadlineWP);
            if (notifi.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy") != DateTime.Now.ToString("dd/MM/yyyy"))
            {
                DateTime daytomorrow = DateTime.Now.AddDays(1);
                var listwp = this.workGroupService.GetAll().Where(t => t.Complete < 100 && DateTime.ParseExact(t.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy"), "dd/MM/yyyy", null) == DateTime.ParseExact(daytomorrow.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null)).ToList();
                foreach (WorkGroup wpObj in listwp)
                {
                    if (wpObj.DepartmentId != 0)
                    {
                        var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                        var gipUser = this.userService.GetByID(projectObj != null ? projectObj.ProjectManagerId.GetValueOrDefault() : 0);
                        //var emailList = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault())
                        //    .Where(t => t.IsChief.GetValueOrDefault())
                        //    .Select(t => t.Email)
                        //    .Where(t => !string.IsNullOrEmpty(t)).ToList();



                        var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.DeadlineWP);
                        if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                        {
                            var smtpClient = new SmtpClient
                            {
                                DeliveryMethod = SmtpDeliveryMethod.Network,
                                UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                                EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                                Host = ConfigurationManager.AppSettings["Host"],
                                Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                            };

                            var wpUpdatedUser = this.userService.GetByID(wpObj.UpdatedBy.GetValueOrDefault());
                            var subject = notificationTemplate.Subject.Replace("#WPName#", wpObj.Name);

                            var message = new MailMessage();
                            message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                            message.Subject = subject;
                            message.BodyEncoding = new UTF8Encoding();
                            message.IsBodyHtml = true;
                            message.Body = notificationTemplate.Contents
                                .Replace("#WPNumber#", wpObj.Name)
                                .Replace("#WPProjectNumber#", projectObj != null ? projectObj.Name : string.Empty)
                                .Replace("#WPProjectName#", projectObj != null ? projectObj.Description : string.Empty)
                                .Replace("#WPName#", wpObj.Description)
                                .Replace("#WPStartDate#", wpObj.StartDate != null ? wpObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                                .Replace("#WPDeadline#", wpObj.Deadline != null ? wpObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                                .Replace("#WPComplete#", wpObj.Complete != null ? wpObj.Complete.Value.ToString() : string.Empty);

                            if (gipUser != null && !string.IsNullOrEmpty(gipUser.Email))
                            {
                                message.To.Add(new MailAddress(gipUser.Email));
                                smtpClient.Send(message);
                            }
                        }
                    }
                }

                notifi.UpdatedDate = DateTime.Now;
                this.emailNotificationTemplateService.Update(notifi);
            }
        }
    }
}


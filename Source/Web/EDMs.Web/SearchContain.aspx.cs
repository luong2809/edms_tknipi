﻿using Nest;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EDMs.Web
{
    public partial class SearchContain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["path"] != null && Request.QueryString["type"] != null)
            {
                string path = Request.QueryString["path"].ToString();
                string type = Request.QueryString["type"].ToString();
                //Download(path, type);
            }
            var node = new Uri("http://localhost:9200");
            var settings = new ConnectionSettings(node);
            var client = new ElasticClient(settings);
        }

        private void Download(string doc, string type)
        {
            string file = Server.MapPath(string.Format("~/DocLib{0}", doc));
            string path = file;
            string name = Path.GetFileName(doc);
            string ext = Path.GetExtension(name); //get file extension

            Response.AppendHeader("content-disposition", "attachment; filename=" + name);


            Response.ContentType = type;
            Response.WriteFile(path);

            Response.End(); //give POP to user for file download
        }

    }
}
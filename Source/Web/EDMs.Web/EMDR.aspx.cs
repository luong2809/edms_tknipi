﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using EDMs.Business.Services.Scope;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.ServiceProcess;
    using System.Text;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using CheckBox = System.Web.UI.WebControls.CheckBox;
    using Label = System.Web.UI.WebControls.Label;
    using TextBox = System.Web.UI.WebControls.TextBox;
    using System.Security.Principal;
    using System.Globalization;
    using Business.Services.Function;
    using Function;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class EMDR : Page
    {
        private readonly OptionalTypeService optionalTypeService = new OptionalTypeService();

        /// <summary>
        /// The permission service.
        /// </summary>
        private readonly PermissionService permissionService = new PermissionService();

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService = new RevisionService();

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService = new FolderService();

        private readonly DocumentNewService documentNewService = new DocumentNewService();

        private readonly NotificationRuleService notificationRuleService = new NotificationRuleService();

        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly UserService userService = new UserService();

        private readonly AttachFileService attachFileService = new AttachFileService();

        private readonly AttachFilesPackageService attachFilesPackageService = new AttachFilesPackageService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly PackageService packageService = new PackageService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly WorkGroupService workGroupService = new WorkGroupService();
        private readonly DisciplineService disciplineService = new DisciplineService();
        private readonly TemplateManagementService templateManagementService = new TemplateManagementService();

        private readonly EmailNotificationTemplateService emailNotificationTemplateService = new EmailNotificationTemplateService();
        private readonly ProcessManhourPlanedService processmanhourplanedsevice = new ProcessManhourPlanedService();
        private readonly ProcessManhourActualService processmanhouractualservice = new ProcessManhourActualService();
        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();
        private readonly RoleService roleservice = new RoleService();
        private readonly BlockService blockService = new BlockService();
        private readonly FieldService fieldService = new FieldService();
        private readonly PlatformService platformService = new PlatformService();
        private readonly MilestoneService milestoneService = new MilestoneService();
        private readonly ProcessActualService processActualService = new ProcessActualService();
        private readonly UserManhourService userManhourService = new UserManhourService();
        private readonly UserManhourMonthService userManhourMonthService = new UserManhourMonthService();
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();
        private readonly ProcessBussiness processBussiness = new ProcessBussiness();

        protected const string ServiceName = "EDMSFolderWatcher";

        public static RadTreeNode editedNode = null;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

        /// <summary>
        /// The list folder id.
        /// </summary>
       // private List<int> listFolderId = new List<int>();

        private List<WorkGroup> WorkpackageList
        {
            get
            {
                List<int> roleList = ConfigurationManager.AppSettings.Get("GroupNotInVien").Split(',').Select(Int32.Parse).ToList();
                roleList.Remove(108);
                var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer || UserSession.Current.IsEngineer && !roleList.Contains(UserSession.Current.RoleId);
                var currentDepartment = UserSession.Current.User.RoleId;

                return isSeeFull
                    ? this.workGroupService.GetAll()
                    : this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault());
            }
        }
        //List<DocumentPackage> docListFilter = new List<DocumentPackage>();
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Title = ConfigurationManager.AppSettings.Get("AppName");

                if (!Page.IsPostBack)
                {
                    this.LoadProject();
                    this.LoadWP();
                    Session.Add("IsListAll", false);
                    if (!UserSession.Current.IsDC || UserSession.Current.IsGip)
                    {
                        var DownRaseRev = this.radMenu.Items.FindItemByValue("DeleteRev");
                        if (DownRaseRev != null)
                        {
                            DownRaseRev.Visible = false;
                        }
                    }
                    if (UserSession.Current.IsAdmin || UserSession.Current.IsDC)
                    {
                        this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = true;
                        this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = true;
                        this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = true;
                        this.IsFullPermission.Value = "true";
                    }
                    else if (UserSession.Current.IsGip)
                    {
                        this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = true;
                        this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = true;
                        this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = true;
                        this.grdDocument.AllowMultiRowSelection = false;
                        RadToolBarButton toolbar = (RadToolBarButton)this.CustomerMenu.FindItemByValue("DeleteAll");
                        toolbar.Visible = false;
                        this.IsFullPermission.Value = "true";
                        //var rtvProject = (RadTreeView)this.ddlProject.Items[0].FindControl("rtvProject");
                        var projectId = Convert.ToInt32(ddlProject.SelectedValue);
                        var projectObj = this.scopeProjectService.GetById(projectId);
                        if (projectObj != null)
                        {
                            if (projectObj.ProjectManagerId != UserSession.Current.User.Id)
                            {
                                this.CustomerMenu.Items[0].Visible = false;
                                this.CustomerMenu.Items[1].Visible = false;
                                this.CustomerMenu.Items[2].Controls[4].Visible = false;
                            }
                        }
                    }
                    else if (UserSession.Current.IsLeader || UserSession.Current.IsCheif)
                    {
                        this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                        this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
                        this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                        this.grdDocument.AllowMultiRowSelection = false;
                        this.CustomerMenu.Items[0].Visible = false;
                        this.CustomerMenu.Items[1].Visible = false;
                        this.IsFullPermission.Value = "false";
                    }
                    else if (UserSession.Current.IsEngineer || UserSession.Current.IsSuperViewer)
                    {
                        this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                        this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
                        this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                        this.grdDocument.AllowMultiRowSelection = false;
                        this.CustomerMenu.Items[0].Visible = false;
                        this.CustomerMenu.Items[1].Visible = false;
                        this.CustomerMenu.Items[2].Visible = false;
                        this.IsFullPermission.Value = "false";
                    }

                    if (!UserSession.Current.IsAdmin && !UserSession.Current.User.IsGip.GetValueOrDefault() && !UserSession.Current.User.IsDC.GetValueOrDefault())
                    {
                        foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
                        {
                            if (item.Value == "Importmaster" || item.Value == "getmanhour")//item.Value == "Exportmaster" || 
                            {
                                item.Visible = false;
                            }
                        }
                    }
                    if (UserSession.Current.User.Id != 1)
                    {
                        foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
                        {
                            if (item.Value == "Latest" || item.Value == "copy")
                            {
                                item.Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// The rad tree view 1_ node click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void radTreeFolder_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            var folder = this.folderService.GetById(Convert.ToInt32(e.Node.Value));
            var temp = (RadToolBarButton)this.CustomerMenu.FindItemByText("View explorer");
            temp.NavigateUrl = ConfigurationSettings.AppSettings.Get("ServerName") + folder.DirName;


            ////var originalURL = @"\\" + ConfigurationSettings.AppSettings.Get("ServerName") + @"\" + folder.DirName.Replace(@"/", @"\");
            ////var tempURI = new Uri(originalURL);

            ////var temp = (RadToolBarButton)this.CustomerMenu.FindItemByText("View explorer");
            ////temp.NavigateUrl = tempURI.AbsoluteUri;

            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(true, isListAll);
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(bool isbind = false, bool isListAll = false)
        {
            var docList = new List<DocumentPackage>();
            try
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["WpID"]))
                {
                    var wpId = this.Request.QueryString["WpID"];
                    if (wpId != "0")
                    {

                        RadTreeNode node = (RadTreeNode)this.rtvWorkgroup.Nodes.FindNodeByValue(wpId);
                        node.Selected = true;
                    }
                }
                var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
                if (!projectObj.IsIDC.GetValueOrDefault())
                {
                    grdDocument.MasterTableView.GetColumn("IDCDeadline").Display = false;
                    grdDocument.MasterTableView.GetColumn("IFRDeadline").Display = false;
                }
                else
                {
                    grdDocument.MasterTableView.GetColumn("IDCDeadline").Display = true;
                    grdDocument.MasterTableView.GetColumn("IFRDeadline").Display = true;
                }
                List<int> roleList = ConfigurationManager.AppSettings.Get("GroupNotInVien").Split(',').Select(Int32.Parse).ToList();
                roleList.Remove(108);
                if (this.rtvWorkgroup.SelectedNode != null && this.ddlProject.SelectedValue != null)
                {
                    //if (!UserSession.Current.IsEngineer)

                    if (!roleList.Contains(UserSession.Current.RoleId))
                    {
                        docList = this.documentPackageService.GetAllByWorkgroup(Convert.ToInt32(this.rtvWorkgroup.SelectedNode.Value))
                        .OrderBy(t => t.DocNo)
                        .ToList();
                        var docEMDRList = this.documentPackageService.GetAllEMDRByWorkgroup(Convert.ToInt32(this.rtvWorkgroup.SelectedNode.Value));

                        var txtWorkgroupComplete = this.CustomerMenu.Items[3].FindControl("txtWorkgroupComplete") as RadNumericTextBox;
                        var txtWorkgroupWeight = this.CustomerMenu.Items[3].FindControl("txtWorkgroupWeight") as RadNumericTextBox;

                        if (txtWorkgroupComplete != null)
                        {
                            double complete = 0;
                            complete = docEMDRList.Aggregate(complete, (current, t) => current + (t.Complete.GetValueOrDefault() * t.Weight.GetValueOrDefault()) / 100);
                            complete = Math.Round(complete, 5);
                            txtWorkgroupComplete.Value = complete;
                        }

                        if (txtWorkgroupWeight != null)
                        {
                            double weight = 0;
                            weight = docEMDRList.Aggregate(weight, (current, t) => current + t.Weight.GetValueOrDefault());
                            weight = Math.Round(weight, 5);
                            txtWorkgroupWeight.Value = weight;
                        }
                    }
                    else
                    {
                        docList = this.documentPackageService.GetAllByWorkgroupForEng(Convert.ToInt32(this.rtvWorkgroup.SelectedNode.Value), UserSession.Current.User.Id.ToString())
                            .OrderBy(t => t.DocNo)
                            .ToList();
                        this.CustomerMenu.Items[3].Visible = false;
                    }
                }
                else
                {
                    var listWorkgroupId = this.WorkpackageList.Where(
                     t =>
                         t.ProjectId ==
                         (this.ddlProject.SelectedItem != null
                             ? Convert.ToInt32(this.ddlProject.SelectedValue)
                             : 0)).Select(t => t.ID).ToList();
                    //if (!UserSession.Current.IsEngineer)
                    //{

                    //if (!UserSession.Current.IsEngineer)
                    if (!roleList.Contains(UserSession.Current.RoleId))
                    {
                        docList = this.documentPackageService.GetAllByWorkgroupInPermission(listWorkgroupId)
                        .OrderBy(t => t.DocNo)
                        .ToList();
                    }
                    else
                    {
                        docList = this.documentPackageService.GetAllByWorkgroupInPermission(listWorkgroupId)
                            .Where(t => t.EngineerId == UserSession.Current.User.Id.ToString())
                            .OrderBy(t => t.DocNo)
                            .ToList();
                    }

                    this.CustomerMenu.Items[3].Visible = false;
                }
            }
            catch (Exception ex) { }
            this.grdDocument.DataSource = docList;
            //  docListFilter = docList;
        }


        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "latest")
            {

                var projectobj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
                var validProjectName = Utilities.Utility.RemoveSpecialCharacterForFolder(projectobj.Name);
                var targetFolder = "~/DocumentLibrary/Latest Revision";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/Latest Revision/" + validProjectName;
                var physicalProjectFolder = Server.MapPath(targetFolder);
                var physicalWPFolder = "";
                physicalProjectFolder = physicalProjectFolder + @"/" + validProjectName;
                if (!Directory.Exists(physicalProjectFolder))
                {
                    Directory.CreateDirectory(physicalProjectFolder);
                }
                else
                {
                    var wplist = this.workGroupService.GetAllWorkGroupOfProject(projectobj.ID);
                    foreach (var wp in wplist)
                    {
                        var validWPName = Utilities.Utility.RemoveSpecialCharacterForFolder(wp.Name);
                        physicalWPFolder = Path.Combine(physicalProjectFolder, validWPName);
                        if (!Directory.Exists(physicalWPFolder))
                        {
                            Directory.CreateDirectory(physicalWPFolder);
                        }
                        Array.ForEach(Directory.GetFiles(physicalWPFolder), File.Delete);
                        var lisdocwp = this.documentPackageService.GetAllByWorkgroup(wp.ID);
                        foreach (var doc in lisdocwp)
                        {
                            var listfile = this.attachFilesPackageService.GetAllDocumentFileByDocId(doc.ID).ToList();
                            foreach (var file in listfile)
                            {
                                var curentfile = Server.MapPath(file.FilePath);
                                var newfile = Path.Combine(physicalWPFolder, file.FileName.Replace("&", " "));
                                if (File.Exists(curentfile))
                                {
                                    File.Copy(curentfile, newfile, true);
                                }
                            }

                        }
                        wp.PathLatestRevision = serverFolder + @"/" + validWPName;
                        this.workGroupService.Update(wp);
                    }
                    projectobj.PathLatestRevision = serverFolder;
                    this.scopeProjectService.Update(projectobj);
                }

            }
            else if (e.Argument.Contains("DeleteAllDoc"))
            {
                var st = e.Argument.Replace("DeleteAllDoc_", string.Empty);
                var wid = 0;
                List<string> listid = st.Split(',').ToList();
                foreach (var selectedItem in listid)
                {
                    int docID = 0;
                    bool isInt = Int32.TryParse(selectedItem, out docID);
                    if (isInt)
                    {
                        //var docId = Convert.ToInt32(selectedItem);
                        var docObj = this.documentPackageService.GetById(docID);
                        wid = docObj.WorkgroupId.GetValueOrDefault();
                        if (docObj != null)
                        {
                            if (!docObj.HasAttachFile.GetValueOrDefault())
                            {
                                if (docObj.ParentId == null)
                                {
                                    docObj.IsLeaf = false;
                                    docObj.IsDelete = true;
                                    docObj.UpdatedBy = UserSession.Current.User.Id;
                                    docObj.UpdatedDate = DateTime.Now;
                                    this.documentPackageService.Update(docObj);
                                }
                                else
                                {
                                    var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                                    if (listRelateDoc != null)
                                    {
                                        foreach (var objDoc in listRelateDoc)
                                        {
                                            objDoc.IsLeaf = false;
                                            objDoc.IsDelete = true;
                                            docObj.UpdatedBy = UserSession.Current.User.Id;
                                            docObj.UpdatedDate = DateTime.Now;
                                            this.documentPackageService.Update(objDoc);
                                        }
                                    }
                                }
                                var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());
                                if (wpObj != null)
                                {
                                    var docList = this.documentPackageService.GetAllEMDRByWorkgroup(wpObj.ID);
                                    var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                                    double complete = 0;
                                    Milestone milestoneObj = new Milestone();
                                    ProcessActual processActualObj = new ProcessActual();
                                    List<WorkgroupManhourMonth> wmmList = new List<WorkgroupManhourMonth>();
                                    WorkgroupManhourMonth wmmObj = new WorkgroupManhourMonth();

                                    //update weight cho document
                                    UpdateWeightDocument(projectObj, ref docList);
                                    var wpUpdateList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                                    //update data workpackage
                                    UpdateDataWorkpackage(projectObj, docList, ref wpObj, ref wpUpdateList, ref complete);
                                    if (wpObj.IsAutoCalculate.GetValueOrDefault())
                                    {
                                        if ((complete < 100 && docList.Where(t => t.Complete != 100).Any()))
                                        {
                                            //update milestone
                                            UpdateMilestonActual(wpObj, UserSession.Current.User, milestoneObj);
                                        }
                                    }
                                    //Update actual progress
                                    UpdateProgressActual(wpObj, processActualObj);

                                    // update data Project
                                    UpdateDataProject(wpUpdateList, projectObj);

                                    //Update all wpWeight trong thang workgroup manhour month cua project
                                    UpdateWeightWorkgroupManhourMonth(wpObj, wmmList);

                                    UpdateDataWorkgroupManhourMonth(wpObj, UserSession.Current.User, wpUpdateList, wmmObj);
                                }
                                if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                                {
                                    this.NotificationDeleteDoc(docObj);
                                }
                            }
                        }
                    }
                }

                this.grdDocument.Rebind();
            }
            //else if (e.Argument == "ClearEMDRData")
            //{
            //    var listDocPack = this.documentPackageService.GetAll();
            //    foreach (var documentPackage in listDocPack)
            //    {
            //        this.documentPackageService.Delete(documentPackage);
            //    }

            //    var attachFilePackage = this.attachFilesPackageService.GetAll();
            //    foreach (var attachFilesPackage in attachFilePackage)
            //    {
            //        var filePath = Server.MapPath(attachFilesPackage.FilePath);
            //        if (File.Exists(filePath))
            //        {
            //            File.Delete(filePath);
            //        }

            //        this.attachFilesPackageService.Delete(attachFilesPackage);
            //    }

            //    this.grdDocument.Rebind();
            //}
            else if (e.Argument == "ExportMasterList")
            {
                //var listWorkgroupInPermission = UserSession.Current.IsAdmin
                //       ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue)).OrderBy(t => t.ID).ToList()
                //       : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id, !string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0)
                //       .OrderBy(t => t.ID).ToList();

                var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsGip || UserSession.Current.IsSuperViewer;
                var currentDepartment = UserSession.Current.RoleId;
                var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
                var listWorkgroupInPermission = isSeeFull
                    ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue)).OrderBy(t => t.ID).ToList()
                    : this.workGroupService.GetAllByDepartment(currentDepartment, Convert.ToInt32(this.ddlProject.SelectedValue)).OrderBy(t => t.ID).ToList();

                if (UserSession.Current.IsEngineer)
                {
                    var wpInWorkOfEng = this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id).Select(t => t.WorkgroupId).Distinct();
                    listWorkgroupInPermission = listWorkgroupInPermission.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
                }

                var filePath = Server.MapPath("Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\MasterListTemplate.xls");
                var sheets = workbook.Worksheets;
                var tempSheet = sheets[0];
                var readMeSheet = sheets[1];
                var revisionList = this.revisionService.GetByActive().OrderBy(t => t.StatusID).ThenBy(t => t.Description).ToList();
                var documenttypeList = this.documentTypeService.GetAll();
                var departmentlist = this.roleservice.GetAll(false);

                for (int i = 0; i < revisionList.Count; i++)
                {
                    readMeSheet.Cells["B" + (7 + i)].PutValue(revisionList[i].Name);
                    readMeSheet.Cells["C" + (7 + i)].PutValue(revisionList[i].Description);
                }

                for (int i = 0; i < documenttypeList.Count; i++)
                {
                    readMeSheet.Cells["E" + (7 + i)].PutValue(documenttypeList[i].Name);
                    readMeSheet.Cells["F" + (7 + i)].PutValue(documenttypeList[i].Description);
                }

                for (int i = 0; i < departmentlist.Count; i++)
                {
                    readMeSheet.Cells["H" + (7 + i)].PutValue(departmentlist[i].Name);
                    readMeSheet.Cells["I" + (7 + i)].PutValue(departmentlist[i].Description);
                }

                var rangeRevisionList = readMeSheet.Cells.CreateRange("B7", "B" + (7 + (revisionList.Count == 0 ? 1 : revisionList.Count)));
                rangeRevisionList.Name = "RevisionList";
                var rangeDocTypeList = readMeSheet.Cells.CreateRange("E7", "E" + (7 + (documenttypeList.Count == 0 ? 1 : documenttypeList.Count)));
                rangeDocTypeList.Name = "DocumentTypeList";
                //var rangDepartmentList = readMeSheet.Cells.CreateRange("H7", "H" + (7 + (departmentlist.Count == 0 ? 1 : departmentlist.Count)));
                //rangDepartmentList.Name = "DepartmentList";
                workbook.Worksheets.RemoveAt(3);
                if (listWorkgroupInPermission.Count > 0)
                {
                    sheets[2].Cells["A2"].PutValue(this.ddlProject.SelectedValue);
                    sheets[2].Cells["B1"].PutValue("Project: " + this.ddlProject.SelectedItem.Text);
                    for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                    {
                        var userList = this.userService.GetAllByRoleId(listWorkgroupInPermission[i].DepartmentId.GetValueOrDefault());
                        var rangeUserList = tempSheet.Cells.CreateRange(tempSheet.Cells[0, 0 + i].Name,
                            Regex.Replace(tempSheet.Cells[0, 0 + i].Name, @"\d+", string.Empty) + (userList.Count == 0 ? 1 : userList.Count));
                        rangeUserList.Name = "UserList" + i;
                        string nameRange = "UserList" + i;
                        for (int j = 0; j < userList.Count; j++)
                        {
                            rangeUserList[j, 0].PutValue(userList[j].UserNameWithFullName);
                        }
                        sheets.AddCopy("Sheet3");
                        //var wpRevisionName = !string.IsNullOrEmpty(listWorkgroupInPermission[i].RevisionName)
                        //    ? "(" + listWorkgroupInPermission[i].RevisionName + ")-"
                        //    : string.Empty;

                        sheets[i + 3].Name = Utility.RemoveSpecialCharacter(listWorkgroupInPermission[i].FullNameRev, "-");
                        sheets[i + 3].Cells["A1"].PutValue(listWorkgroupInPermission[i].ID);
                        //sheets[i + 3].Cells["A2"].PutValue(ConfigurationManager.AppSettings.Get("CurrentMasterFileVersion"));

                        var validations = sheets[i + 3].Validations;
                        this.CreateValidation(rangeRevisionList.Name, validations, 1, 1000, 3, 3);
                        this.CreateValidation(nameRange, validations, 1, 1000, 15, 15);
                        this.CreateValidation(nameRange, validations, 1, 1000, 16, 16);
                        this.CreateValidation(rangeDocTypeList.Name, validations, 1, 1000, 13, 13);
                        //this.CreateValidation(rangDepartmentList.Name, validations, 1, 1000, 11, 11);
                        if (projectObj.IsIDC.GetValueOrDefault())
                        {
                            sheets[i + 3].Cells.Columns[7].IsHidden = false;
                            sheets[i + 3].Cells.Columns[9].IsHidden = false;
                        }
                        else
                        {
                            sheets[i + 3].Cells.Columns[7].IsHidden = true;
                            sheets[i + 3].Cells.Columns[9].IsHidden = true;
                        }
                    }

                    workbook.Worksheets[0].IsVisible = false;
                    workbook.Worksheets.RemoveAt(2);
                    //workbook.Worksheets.Add("Temp");
                    //workbook.Worksheets["Temp"].IsVisible = false;
                    workbook.Worksheets.ActiveSheetIndex = 1;

                    var filename = Utilities.Utility.RemoveAllSpecialCharacter(projectObj.Name) + "$" + "MasterListTemplate.xls";
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument == "ExportEMDRReport")
            {
                var filePath = Server.MapPath("Exports") + @"\";
                var workbook = new Workbook();
                var docList = new List<DocumentPackage>();

                var dtFull = new DataTable();
                dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("Department", typeof(String)),
                        new DataColumn("Start", typeof(String)),
                        new DataColumn("Planned", typeof(String)),
                        new DataColumn("RevName", typeof(String)),
                        new DataColumn("RevPlanned", typeof(String)),
                        new DataColumn("RevActual", typeof(String)),
                        new DataColumn("RevCommentCode", typeof(String)),
                        new DataColumn("Complete", typeof(Double)),
                        new DataColumn("Weight", typeof(Double)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("IncomingNo", typeof(String)),
                        new DataColumn("IncomingDate", typeof(String)),
                        new DataColumn("ICANo", typeof(String)),
                        new DataColumn("ICADate", typeof(String)),
                        new DataColumn("ICAReviewCode", typeof(String)),
                        new DataColumn("Notes", typeof(String)),
                        new DataColumn("IsEMDR", typeof(String)),
                        new DataColumn("HasAttachFile", typeof(String)),

                    });

                var dtWorkgroup = new DataTable();
                dtWorkgroup.Columns.AddRange(new[]
                    {
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("Start", typeof(DateTime)),
                        new DataColumn("RevName", typeof(String)),
                        new DataColumn("RevPlanned", typeof(DateTime)),
                        new DataColumn("RevActual", typeof(DateTime)),
                        new DataColumn("Complete", typeof(Double)),
                        new DataColumn("Weight", typeof(Double)),
                        new DataColumn("Department", typeof(String)),
                        new DataColumn("Notes", typeof(String)),
                        new DataColumn("IsEMDR", typeof(String)),
                        new DataColumn("HasAttachFile", typeof(String)),
                    });

                if (this.rtvWorkgroup.SelectedNode != null)
                {

                    var templateManagement = this.templateManagementService.GetSpecial(1,
                        Convert.ToInt32(this.ddlProject.SelectedValue));
                    if (templateManagement != null)
                    {
                        workbook.Open(Server.MapPath(templateManagement.FilePath));

                        var sheets = workbook.Worksheets;

                        var workgroupId = Convert.ToInt32(this.rtvWorkgroup.SelectedNode.Value);
                        var workgroup = this.workGroupService.GetById(workgroupId);
                        if (workgroup != null)
                        {
                            sheets[0].Name = workgroup.Name;
                            sheets[0].Cells["C7"].PutValue(workgroup.Name);
                            sheets[0].Cells["B7"].PutValue(this.ddlProject.SelectedValue + "," + workgroupId);
                            sheets[0].Cells["M4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            docList =
                                this.documentPackageService.GetAllEMDRByWorkgroup(workgroupId, false)
                                    .OrderBy(t => t.DocNo)
                                    .ToList();

                            var count = 1;

                            var listDocumentTypeId =
                                docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();
                            foreach (var documentTypeId in listDocumentTypeId)
                            {
                                var documentType = this.documentTypeService.GetById(documentTypeId.GetValueOrDefault());

                                var dataRow = dtWorkgroup.NewRow();
                                dataRow["DocId"] = -1;
                                dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                dtWorkgroup.Rows.Add(dataRow);

                                var listDocByDocType = docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                foreach (var document in listDocByDocType)
                                {
                                    dataRow = dtWorkgroup.NewRow();
                                    dataRow["DocId"] = document.ID;
                                    dataRow["NoIndex"] = count;
                                    dataRow["DocNo"] = document.DocNo;
                                    dataRow["DocTitle"] = document.DocTitle;
                                    dataRow["Start"] = (object)document.StartDate ?? DBNull.Value;
                                    dataRow["RevName"] = document.RevisionName;
                                    dataRow["RevPlanned"] = (object)document.RevisionPlanedDate ?? DBNull.Value;
                                    dataRow["RevActual"] = (object)document.RevisionActualDate ?? DBNull.Value;
                                    dataRow["Complete"] = document.Complete / 100;
                                    dataRow["Weight"] = document.Weight / 100;
                                    dataRow["Department"] = document.DeparmentName;
                                    dataRow["Notes"] = document.Notes;
                                    dataRow["IsEMDR"] = document.IsEMDR ? "x" : string.Empty;
                                    dataRow["HasAttachFile"] = document.HasAttachFile.GetValueOrDefault() ? "x" : string.Empty;

                                    count += 1;
                                    dtWorkgroup.Rows.Add(dataRow);
                                }
                            }

                            sheets[0].Cells["A7"].PutValue(dtWorkgroup.Rows.Count);

                            sheets[0].Cells.ImportDataTable(dtWorkgroup, false, 7, 1, dtWorkgroup.Rows.Count, 14, true);
                            sheets[1].Cells.ImportDataTable(dtWorkgroup, false, 7, 1, dtWorkgroup.Rows.Count, 14, true);

                            sheets[0].Cells[7 + dtWorkgroup.Rows.Count, 2].PutValue("Total");

                            var txtWorkgroupComplete = this.CustomerMenu.Items[3].FindControl("txtWorkgroupComplete") as RadNumericTextBox;
                            var txtWorkgroupWeight = this.CustomerMenu.Items[3].FindControl("txtWorkgroupWeight") as RadNumericTextBox;
                            if (txtWorkgroupComplete != null)
                            {
                                sheets[0].Cells[7 + dtWorkgroup.Rows.Count, 9].PutValue(txtWorkgroupComplete.Value / 100);
                            }

                            if (txtWorkgroupWeight != null)
                            {
                                sheets[0].Cells[7 + dtWorkgroup.Rows.Count, 10].PutValue(txtWorkgroupWeight.Value / 100);
                            }
                            sheets[0].AutoFitRows();
                            sheets[1].IsVisible = false;

                            var filename = this.ddlProject.SelectedItem.Text + " - " + workgroup.Name + " EMDR Report " +
                                           DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                            workbook.Save(filePath + filename);
                            this.DownloadByWriteByte(filePath + filename, filename, true);

                        }
                    }
                }
                else
                {
                    var listWorkgroupInPermission = UserSession.Current.User.Id == 1
                        ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue))
                            .OrderBy(t => t.ID)
                            .ToList()
                        : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id,
                            !string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                                ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                : 0)
                            .OrderBy(t => t.ID).ToList();

                    if (listWorkgroupInPermission.Count > 0)
                    {
                        var templateManagement = this.templateManagementService.GetSpecial(2,
                            Convert.ToInt32(this.ddlProject.SelectedValue));
                        if (templateManagement != null)
                        {
                            workbook.Open(Server.MapPath(templateManagement.FilePath));

                            var totalDoc = 0;
                            var totalDocIssues = 0;
                            var totalDocRev0Issues = 0;
                            var totalDocRev1Issues = 0;
                            var totalDocRev2Issues = 0;
                            var totalDocRev3Issues = 0;
                            var totalDocRev4Issues = 0;
                            var totalDocRev5Issues = 0;
                            var totalDocRevIssues = 0;

                            var totalDocDontIssues = 0;

                            var sheets = workbook.Worksheets;
                            var wsSummary = sheets[0];
                            wsSummary.Cells.InsertRows(7, listWorkgroupInPermission.Count - 1);

                            for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                            {
                                dtFull.Rows.Clear();

                                sheets.AddCopy(1);

                                sheets[i + 2].Name = listWorkgroupInPermission[i].Name;
                                sheets[i + 2].Cells["V4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                                sheets[i + 2].Cells["C7"].PutValue(listWorkgroupInPermission[i].Name);

                                // Add hyperlink
                                var linkName = listWorkgroupInPermission[i].Name;
                                wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                                wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1, "'" + listWorkgroupInPermission[i].Name + "'" + "!D7");

                                docList = this.documentPackageService.GetAllEMDRByWorkgroup(listWorkgroupInPermission[i].ID, false).OrderBy(t => t.DocNo).ToList();
                                var docListHasAttachFile = docList.Where(t => t.HasAttachFile.GetValueOrDefault());
                                var wgDoc = docList.Count;
                                var wgDocIssues = docListHasAttachFile.Count();
                                var wgDocRev0Issues = docListHasAttachFile.Count(t => t.RevisionName == "0");
                                var wgDocRev1Issues = docListHasAttachFile.Count(t => t.RevisionName == "1");
                                var wgDocRev2Issues = docListHasAttachFile.Count(t => t.RevisionName == "2");
                                var wgDocRev3Issues = docListHasAttachFile.Count(t => t.RevisionName == "3");
                                var wgDocRev4Issues = docListHasAttachFile.Count(t => t.RevisionName == "4");
                                var wgDocRev5Issues = docListHasAttachFile.Count(t => t.RevisionName == "5");
                                var wgTotalDocRev = wgDocRev0Issues + wgDocRev1Issues + wgDocRev2Issues +
                                                    wgDocRev3Issues + wgDocRev4Issues + wgDocRev5Issues;
                                var wgDocDontIssues = wgDoc - wgDocIssues;

                                totalDoc += wgDoc;
                                totalDocIssues += wgDocIssues;
                                totalDocRev0Issues += wgDocRev0Issues;
                                totalDocRev1Issues += wgDocRev1Issues;
                                totalDocRev2Issues += wgDocRev2Issues;
                                totalDocRev3Issues += wgDocRev3Issues;
                                totalDocRev4Issues += wgDocRev4Issues;
                                totalDocRev5Issues += wgDocRev5Issues;
                                totalDocRevIssues += wgTotalDocRev;
                                totalDocDontIssues = totalDoc - totalDocIssues;

                                wsSummary.Cells["C" + (7 + i)].PutValue(wgDoc);
                                wsSummary.Cells["D" + (7 + i)].PutValue(wgDocDontIssues);
                                wsSummary.Cells["E" + (7 + i)].PutValue(wgDocRev0Issues);
                                wsSummary.Cells["F" + (7 + i)].PutValue(wgDocRev1Issues);
                                wsSummary.Cells["G" + (7 + i)].PutValue(wgDocRev2Issues);
                                wsSummary.Cells["H" + (7 + i)].PutValue(wgDocRev3Issues);
                                wsSummary.Cells["I" + (7 + i)].PutValue(wgDocRev4Issues);
                                wsSummary.Cells["J" + (7 + i)].PutValue(wgDocRev5Issues);
                                wsSummary.Cells["K" + (7 + i)].PutValue(wgTotalDocRev);
                                wsSummary.Cells["L" + (7 + i)].PutValue(wgTotalDocRev);


                                var count = 1;

                                var listDocumentTypeId =
                                    docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                                double complete = 0;
                                double weight = 0;

                                foreach (var documentTypeId in listDocumentTypeId)
                                {
                                    var documentType = this.documentTypeService.GetById(documentTypeId.GetValueOrDefault());

                                    var dataRow = dtFull.NewRow();
                                    dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                    dtFull.Rows.Add(dataRow);

                                    var listDocByDocType =
                                        docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                    foreach (var document in listDocByDocType)
                                    {
                                        dataRow = dtFull.NewRow();
                                        dataRow["DocId"] = document.ID;
                                        dataRow["NoIndex"] = count;
                                        dataRow["DocNo"] = document.DocNo;
                                        dataRow["DocTitle"] = document.DocTitle;
                                        dataRow["Department"] = document.DeparmentName;
                                        dataRow["Start"] = document.StartDate != null
                                            ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["Planned"] = document.PlanedDate != null
                                            ? document.PlanedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevName"] = document.RevisionName;
                                        dataRow["RevPlanned"] = document.RevisionPlanedDate != null
                                            ? document.RevisionPlanedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevActual"] = document.RevisionActualDate != null
                                            ? document.RevisionActualDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevCommentCode"] = document.RevisionCommentCode;
                                        dataRow["Complete"] = document.Complete / 100;
                                        dataRow["Weight"] = document.Weight / 100;
                                        dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                        dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                            ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["IncomingNo"] = document.IncomingTransNo;
                                        dataRow["IncomingDate"] = document.IncomingTransDate != null
                                            ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["ICANo"] = document.ICAReviewOutTransNo;
                                        dataRow["ICADate"] = document.ICAReviewOutDate != null
                                            ? document.ICAReviewOutDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["ICAReviewCode"] = document.ICAReviewCode;
                                        dataRow["Notes"] = document.Notes;
                                        dataRow["IsEMDR"] = document.IsEMDR ? "x" : string.Empty;
                                        dataRow["HasAttachFile"] = document.HasAttachFile.GetValueOrDefault() ? "x" : string.Empty;

                                        count += 1;
                                        dtFull.Rows.Add(dataRow);

                                        complete += (document.Complete.GetValueOrDefault() / 100) * (document.Weight.GetValueOrDefault() / 100);
                                        weight += document.Weight.GetValueOrDefault() / 100;
                                    }
                                }

                                sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                                sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, 23, true);

                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");

                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 12].PutValue(complete);
                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 13].PutValue(weight);

                            }

                            wsSummary.Cells["H4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));

                            wsSummary.Cells["C" + (7 + listWorkgroupInPermission.Count)].PutValue(totalDoc);
                            wsSummary.Cells["D" + (7 + listWorkgroupInPermission.Count)].PutValue(totalDocDontIssues);
                            wsSummary.Cells["E" + (7 + listWorkgroupInPermission.Count)].PutValue(totalDocRev0Issues);
                            wsSummary.Cells["F" + (7 + listWorkgroupInPermission.Count)].PutValue(totalDocRev1Issues);
                            wsSummary.Cells["G" + (7 + listWorkgroupInPermission.Count)].PutValue(totalDocRev2Issues);
                            wsSummary.Cells["H" + (7 + listWorkgroupInPermission.Count)].PutValue(totalDocRev3Issues);
                            wsSummary.Cells["I" + (7 + listWorkgroupInPermission.Count)].PutValue(totalDocRev4Issues);
                            wsSummary.Cells["J" + (7 + listWorkgroupInPermission.Count)].PutValue(totalDocRev5Issues);
                            wsSummary.Cells["K" + (7 + listWorkgroupInPermission.Count)].PutValue(totalDocRevIssues);
                            wsSummary.Cells["L" + (7 + listWorkgroupInPermission.Count)].PutValue(totalDocRevIssues);

                            sheets[1].IsVisible = false;

                            var filename = this.ddlProject.SelectedItem.Text + " - " + "EMDR Report " +
                                           DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                            workbook.Save(filePath + filename);
                            this.DownloadByWriteByte(filePath + filename, filename, true);
                        }
                    }
                }
            }
            else if (e.Argument == "UpdatePackageStatus")
            {
                var txtPackageComplete = this.CustomerMenu.Items[2].FindControl("txtPackageComplete") as RadNumericTextBox;
                var txtPackageWeight = this.CustomerMenu.Items[2].FindControl("txtPackageWeight") as RadNumericTextBox;

                var packageobj = this.packageService.GetById(Convert.ToInt32(this.rtvWorkgroup.SelectedNode.Value));
                if (packageobj != null)
                {
                    if (txtPackageComplete != null)
                    {
                        packageobj.Complete = txtPackageComplete.Value.GetValueOrDefault();
                    }

                    if (txtPackageWeight != null)
                    {
                        packageobj.Weight = txtPackageWeight.Value.GetValueOrDefault();
                    }

                    this.packageService.Update(packageobj);
                }
            }
            else if (e.Argument.Contains("DeleteRev"))
            {
                string st = e.Argument.ToString();
                int docId = Convert.ToInt32(st.Replace("DeleteRev_", string.Empty));

                var docObj = this.documentPackageService.GetById(docId);
                var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                if (docObj != null && listRelateDoc.Count > 1)
                {

                    docObj.IsDelete = true;
                    docObj.IsLeaf = false;
                    this.documentPackageService.Update(docObj);
                    docId = 0;
                    listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                    if (listRelateDoc != null)
                    {
                        foreach (var objDoc in listRelateDoc)
                        {
                            if (docId < objDoc.ID) { docId = objDoc.ID; docObj = objDoc; }
                        }
                    }
                    if (docId != 0)
                    {
                        docObj.IsLeaf = true;
                        docObj.UpdatedBy = UserSession.Current.User.Id;
                        docObj.UpdatedDate = DateTime.Now.Date;
                        this.documentPackageService.Update(docObj);

                        this.grdDocument.Rebind();
                    }
                }
                else
                {
                    Response.Write("<script>window.alert('Can not be reduced, because this document is only one version.')</script>");
                }
            }
            else if (e.Argument == "DownloadMulti")
            {
                //var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_DocPack.rar");
                //var docPack = ZipPackage.CreateFile(serverTotalDocPackPath);

                //foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                //{
                //    var cboxSelected = (CheckBox)item["IsSelected"].FindControl("IsSelected");
                //    if (cboxSelected.Checked)
                //    {
                //        var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                //        var name = (Label)item["Index1"].FindControl("lblName");
                //        var serverDocPackPath = Server.MapPath("~/Exports/DocPack/" + name.Text + "_" + DateTime.Now.ToString("ddMMyyyhhmmss") + ".rar");

                //        var attachFiles = this.attachFileService.GetAllByDocId(docId);

                //        var temp = ZipPackage.CreateFile(serverDocPackPath);

                //        foreach (var attachFile in attachFiles)
                //        {
                //            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                //            {
                //                temp.Add(Server.MapPath(attachFile.FilePath));
                //            }
                //        }

                //        docPack.Add(serverDocPackPath);
                //    }
                //}

                //this.DownloadByWriteByte(serverTotalDocPackPath, "DocumentPackage.rar", true);

            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "SendNotification")
            {
                var listDisciplineId = new List<int>();
                var listSelectedDoc = new List<Document>();
                var count = 0;
                foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                {
                    var cboxSelected = (CheckBox)item["IsSelected"].FindControl("IsSelected");
                    if (cboxSelected.Checked)
                    {
                        count += 1;
                        var docItem = new Document();
                        var disciplineId = item["DisciplineID"].Text != @"&nbsp;"
                                                     ? item["DisciplineID"].Text
                                                     : string.Empty;
                        if (!string.IsNullOrEmpty(disciplineId) && disciplineId != "0")
                        {
                            listDisciplineId.Add(Convert.ToInt32(disciplineId));

                            docItem.ID = count;
                            docItem.DocumentNumber = item["DocumentNumber"].Text != @"&nbsp;"
                                                     ? item["DocumentNumber"].Text
                                                     : string.Empty;
                            docItem.Title = item["Title"].Text != @"&nbsp;"
                                                         ? item["Title"].Text
                                                         : string.Empty;
                            docItem.RevisionName = item["Revision"].Text != @"&nbsp;"
                                                         ? item["Revision"].Text
                                                         : string.Empty;
                            docItem.FilePath = item["FilePath"].Text != @"&nbsp;"
                                                         ? item["FilePath"].Text
                                                         : string.Empty;
                            docItem.DisciplineID = Convert.ToInt32(disciplineId);
                            listSelectedDoc.Add(docItem);
                        }
                    }
                }

                listDisciplineId = listDisciplineId.Distinct().ToList();

                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials = new NetworkCredential(UserSession.Current.User.Email, "")
                };

                foreach (var disciplineId in listDisciplineId)
                {
                    var notificationRule = this.notificationRuleService.GetAllByDiscipline(disciplineId);

                    if (notificationRule != null)
                    {
                        var message = new MailMessage();
                        message.From = new MailAddress(UserSession.Current.User.Email, UserSession.Current.User.FullName);
                        message.Subject = "Test send notification from EDMs";
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = @"******<br/>
                                        Dear users,<br/><br/>

                                        Please be informed that the following documents are now available on the BDPOC Document Library System for your information.<br/><br/>

                                        <table border='1' cellspacing='0'>
	                                        <tr>
		                                        <th style='text-align:center; width:40px'>No.</th>
		                                        <th style='text-align:center; width:350px'>Document number</th>
		                                        <th style='text-align:center; width:350px'>Document title</th>
		                                        <th style='text-align:center; width:60px'>Revision</th>
	                                        </tr>";

                        if (!string.IsNullOrEmpty(notificationRule.ReceiverListId))
                        {
                            var listUserId = notificationRule.ReceiverListId.Split(';').Select(t => Convert.ToInt32(t)).ToList();
                            foreach (var userId in listUserId)
                            {
                                var user = this.userService.GetByID(userId);
                                if (user != null)
                                {
                                    message.To.Add(new MailAddress(user.Email));
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(notificationRule.ReceiveGroupId) && string.IsNullOrEmpty(notificationRule.ReceiverListId))
                        {
                            var listGroupId = notificationRule.ReceiveGroupId.Split(';').Select(t => Convert.ToInt32(t)).ToList();
                            var listUser = this.userService.GetSpecialListUser(listGroupId);
                            foreach (var user in listUser)
                            {
                                message.To.Add(new MailAddress(user.Email));
                            }
                        }

                        var subBody = string.Empty;
                        foreach (var document in listSelectedDoc)
                        {
                            var port = ConfigurationSettings.AppSettings.Get("DocLibPort");
                            if (document.DisciplineID == disciplineId)
                            {
                                subBody += @"<tr>
                                <td>" + document.ID + @"</td>
                                <td><a href='http://" + Server.MachineName + (!string.IsNullOrEmpty(port) ? ":" + port : string.Empty)
                                           + document.FilePath + "' download='" + document.DocumentNumber + "'>"
                                           + document.DocumentNumber + @"</a></td>
                                <td>"
                                           + document.Title + @"</td>
                                <td>"
                                           + document.RevisionName + @"</td>";
                            }
                        }


                        message.Body += subBody + @"</table>
                                        <br/><br/>
                                        Thanks and regards,<br/>
                                        ******";

                        smtpClient.Send(message);
                    }
                }
            }
            else if (e.Argument == "GetManhours")
            {
                if (this.ddlProject.SelectedItem != null)
                {
                    var projectobj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
                    var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectobj.ID);
                    var doclist = this.documentPackageService.GetAllDocProject(projectobj.ID);

                    foreach (var wp in wpList)
                    {
                        if (wp.StartDate != null && wp.Deadline != null)
                        {
                            var manhourplaned = new ProcessManhourPlaned()
                            {
                                ProjectId = projectobj.ID,
                                StartDate = wp.StartDate,
                                EndDate = wp.Deadline,
                                DepartmentId = wp.DepartmentId,
                                WorkgroupId = wp.ID,
                                Planed = ""
                            };

                            var manhouractual = new ProcessManhourActual()
                            {
                                ProjectId = projectobj.ID,
                                StartDate = wp.StartDate,
                                EndDate = wp.Deadline,
                                DepartmentId = wp.DepartmentId,
                                WorkgroupId = wp.ID,
                                Actual = ""
                            };

                            var wpdoclist = doclist.Where(t => t.WorkgroupId == wp.ID);
                            var actual = "";
                            var planed = "";
                            var differenceMonth = ((wp.Deadline.GetValueOrDefault().Year - wp.StartDate.GetValueOrDefault().Year) * 12) + wp.Deadline.GetValueOrDefault().Month - wp.StartDate.GetValueOrDefault().Month + 1;
                            for (int i = 0; i < differenceMonth; i++)
                            {
                                actual += "0.0$";
                                planed += "0.0$";

                            }
                            actual = actual.Substring(0, actual.Length - 1);
                            manhouractual.Actual = actual;
                            planed = planed.Substring(0, planed.Length - 1);
                            var planeslistarry = planed.Split('$');
                            foreach (var doc in wpdoclist)
                            {
                                if (doc.ManHourPlan != null && doc.StartDate != null && doc.Deadline != null && doc.ManHourPlan.Value != 0)
                                {
                                    var WokingDay = this.GetBusinessDays(doc.StartDate.GetValueOrDefault(), doc.Deadline.GetValueOrDefault());
                                    var valuesmanhour = doc.ManHourPlan.Value / (double)WokingDay.Count();

                                    //  var currentmonth = doc.StartDate.Value.Month;
                                    var listyear = WokingDay.GroupBy(t => t.Year).ToList();
                                    foreach (var currentyear in listyear)
                                    {
                                        var listmonth = currentyear.GroupBy(t => t.Month).ToList();
                                        foreach (var currentmonth in listmonth)
                                        {
                                            var valuescurrent = 0.0;
                                            valuescurrent = currentmonth.Count() * valuesmanhour;
                                            var day = wp.StartDate.GetValueOrDefault();
                                            for (int i = 0; i < differenceMonth; i++)
                                            {
                                                if (day.Month == currentmonth.Key && day.Year == currentyear.Key)
                                                {
                                                    valuescurrent += double.Parse(planeslistarry[i]);
                                                    planeslistarry[i] = Math.Round(valuescurrent, 2).ToString();
                                                    break;
                                                }
                                                day = day.AddMonths(1);
                                            }

                                        }
                                    }
                                }

                            }

                            var newActual = string.Empty;
                            newActual = planeslistarry.Aggregate(newActual, (current, t) => current + t + "$");

                            newActual = newActual.Substring(0, newActual.Length - 1);
                            manhourplaned.Planed = newActual;
                            var process = this.processmanhourplanedsevice.GetByProjectAndWorkgroup(projectobj.ID, wp.ID);
                            if (process != null)
                            {
                                process.StartDate = wp.StartDate;
                                process.EndDate = wp.Deadline;
                                process.Planed = newActual;
                                this.processmanhourplanedsevice.Update(process);
                                var processActul = this.processmanhouractualservice.GetByProjectAndWorkgroup(projectobj.ID, wp.ID);
                                if (processActul.Actual.Split(',').Count() < differenceMonth)
                                {
                                    processActul.StartDate = wp.StartDate;
                                    processActul.EndDate = wp.Deadline;
                                    for (int k = 0; k < (differenceMonth - processActul.Actual.Split(',').Count()); k++)
                                    {
                                        processActul.Actual = processActul.Actual + "$0.0";
                                    }
                                    this.processmanhouractualservice.Update(processActul);
                                }


                            }
                            else
                            {
                                this.processmanhourplanedsevice.Insert(manhourplaned);
                                this.processmanhouractualservice.Insert(manhouractual);
                            }

                        }

                    }
                }

            }
            else if (e.Argument.Contains("ExportCMDRFilter"))
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                var dtFull = new DataTable();
                //var projectID = Convert.ToInt32(this.lblProjectId.Value);
                dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("RevName", typeof(String)),
                        new DataColumn("Start", typeof(String)),
                        new DataColumn("IDCDeadline", typeof(String)),
                        new DataColumn("IFRDeadline", typeof(String)),
                        new DataColumn("DeadLine", typeof(String)),
                        new DataColumn("End", typeof(String)),
                        new DataColumn("Complete", typeof(Double)),
                        new DataColumn("Weight",typeof(Double)),
                        new DataColumn("DocumentTypeName", typeof(String)),
                        new DataColumn("ManhourPlan", typeof(String)),
                        new DataColumn("ManhourActual", typeof(String)),
                        new DataColumn("Department", typeof(String)),
                        new DataColumn("Engineer", typeof(String)),
                        new DataColumn("Leader", typeof(String)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("OutgoingXDCBNo", typeof(String)),
                        new DataColumn("OutgoingXDCBDate", typeof(String)),
                        new DataColumn("IncomingNo", typeof(String)),
                        new DataColumn("IncomingDate", typeof(String)),
                        new DataColumn("ICANo", typeof(String)),
                        new DataColumn("ICADate", typeof(String)),
                        new DataColumn("ICAReviewCode", typeof(String)),
                        new DataColumn("Markup",typeof(String)),
                        new DataColumn("hardcopy", typeof(String)),
                        new DataColumn("Notes", typeof(String)),
                    });
                var selectedProject = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
                var projectName = selectedProject.Name;
                var projectId = selectedProject.ID;
                List<int> ListId = new List<int>();

                this.grdDocument.AllowPaging = false;
                this.grdDocument.Rebind();
                foreach (GridDataItem row in this.grdDocument.Items) // loops through each rows in RadGrid
                {
                    ListId.Add(Convert.ToInt32(row.GetDataKeyValue("ID").ToString()));
                }
                this.grdDocument.AllowPaging = true;
                this.grdDocument.Rebind();
                var docListFilter = this.documentPackageService.GetAllDocList(ListId);

                if (docListFilter.Count > 0)
                {
                    var listwp = docListFilter.Select(t => (int)t.WorkgroupId).Distinct().ToList();
                    var currentDepartment = UserSession.Current.User.RoleId;
                    var workPackageList = this.workGroupService.GetAllWorkGroupOfProject(projectId).Where(t => listwp.Contains(t.ID)).ToList();
                    if (workPackageList.Count > 0)
                    {
                        workbook.Open(Server.MapPath("~/" + @"Exports\Template\CMDRReport.xls"));

                        var sheets = workbook.Worksheets;
                        var wsSummary = sheets[0];
                        wsSummary.Cells.InsertRows(7, workPackageList.Count - 1);
                        for (int i = 0; i < workPackageList.Count; i++)
                        {
                            var totalDoc = 0;
                            var totalDocIssues = 0;
                            var totalDocRev5Issues = 0;
                            var totalDocRevIssues = 0;
                            var totalDocDontIssues = 0;
                            var docListFilterwp = docListFilter.Where(t => t.WorkgroupId == workPackageList[i].ID).OrderBy(t => t.DocNo).ToList();
                            dtFull.Rows.Clear();
                            sheets.AddCopy(1);
                            string st = workPackageList[i].Name.Replace("/", " ").Replace(@"\", " ");
                            if (st.Length > 30)
                            {
                                st = st.Substring(0, 25) + "_(" + i + ")";
                            }
                            sheets[i + 2].Name = st;
                            sheets[i + 2].Cells["Z4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            sheets[i + 2].Cells["C7"].PutValue(workPackageList[i].Name);
                            sheets[i + 2].Cells["E1"].PutValue("DETAIL ENGINEERING SERVICE FOR PROJECT " + projectName);
                            // add hyperlink
                            var linkName = workPackageList[i].Name;
                            wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                            wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1, "'" + st + "'!D7");
                            if (workPackageList[i].DisciplineId != null)
                            {
                                var disciplineObj = this.disciplineService.GetById(workPackageList[i].DisciplineId.GetValueOrDefault());
                                wsSummary.Cells["C" + (7 + i)].PutValue(disciplineObj != null ? disciplineObj.Description : string.Empty);
                            }
                            wsSummary.Cells["C1"].PutValue("DETAIL  ENGINEERING SERVICE FOR " + projectName + " PROJECT");
                            wsSummary.Cells["C3"].PutValue(projectName + " PROJECT - SUMMARY REPORT");
                            wsSummary.Cells["O4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            wsSummary.Cells["O3"].PutValue(workPackageList.Count.ToString());

                            var docListHasAttachFile = docListFilterwp.Where(t => t.HasAttachFile.GetValueOrDefault() && t.Complete == 100);
                            var wgDoc = docListFilterwp.Count;
                            var wgDocIssues = docListHasAttachFile.Count();
                            var wgDocHardCopy = docListHasAttachFile.Count(t => !string.IsNullOrEmpty(t.OutgoingTransNo));
                            var wgTotalDocRev = wgDocIssues;
                            var wgDocDontIssues = wgDoc - wgDocIssues;
                            var wgDocOverdue = docListFilterwp.Where(t => !t.HasAttachFile.GetValueOrDefault() && t.Complete != 100 && t.Deadline.GetValueOrDefault() < DateTime.Now).ToList();
                            var total = docListHasAttachFile.Count(t => !string.IsNullOrEmpty(t.OutgoingLeterNo));

                            totalDoc += wgDoc;
                            totalDocIssues += wgDocIssues;
                            totalDocRev5Issues += wgDocHardCopy;
                            totalDocRevIssues += total;
                            totalDocDontIssues = totalDoc - totalDocIssues;

                            wsSummary.Cells["D" + (7 + i)].PutValue(wgDoc);
                            wsSummary.Cells["E" + (7 + i)].PutValue(wgDocDontIssues);
                            wsSummary.Cells["K" + (7 + i)].PutValue(wgTotalDocRev);
                            wsSummary.Cells["L" + (7 + i)].PutValue(wgDocOverdue.Count);
                            wsSummary.Cells["M" + (7 + i)].PutValue(totalDocRev5Issues);
                            wsSummary.Cells["N" + (7 + i)].PutValue(totalDocRevIssues);

                            var count = 1;

                            var listDocumentTypeId =
                                docListFilterwp.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                            double complete = 0;
                            double weight = 0;

                            foreach (var documentTypeId in listDocumentTypeId)
                            {
                                var documentType = this.documentTypeService.GetById((int)documentTypeId);

                                var dataRow = dtFull.NewRow();
                                dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                dtFull.Rows.Add(dataRow);

                                var listDocByDocType =
                                    docListFilterwp.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                foreach (var document in listDocByDocType)
                                {
                                    dataRow = dtFull.NewRow();
                                    dataRow["DocId"] = document.ID;
                                    dataRow["NoIndex"] = count;
                                    dataRow["DocNo"] = document.DocNo;
                                    dataRow["DocTitle"] = document.DocTitle;

                                    dataRow["RevName"] = document.RevisionName;
                                    dataRow["Start"] = document.StartDate != null
                                      ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;
                                    dataRow["IDCDeadline"] = document.IDCDeadline != null
                                ? document.IDCDeadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                                    dataRow["IFRDeadline"] = document.IFRDeadline != null
                                        ? document.IFRDeadline.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["DeadLine"] = document.Deadline != null
                                      ? document.Deadline.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;
                                    dataRow["End"] = document.EndDate != null
                                      ? document.EndDate.Value.ToString("dd/MM/yyyy")
                                      : string.Empty;
                                    dataRow["Complete"] = document.Complete != null ? document.Complete / 100 : 0;
                                    dataRow["Weight"] = document.Weight != null ? document.Weight / 100 : 0;

                                    dataRow["DocumentTypeName"] = document.DocumentTypeName;
                                    dataRow["ManhourPlan"] = document.ManHourPlan != null ? document.ManHourPlan.GetValueOrDefault() : 0;
                                    dataRow["ManhourActual"] = document.ManHours != null ? document.ManHours.GetValueOrDefault() : 0;
                                    dataRow["Department"] = document.DeparmentName;
                                    dataRow["Engineer"] = document.EngineerName;
                                    dataRow["Leader"] = document.LeaderName;
                                    dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                    dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                        ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["OutgoingXDCBNo"] = document.OutgoingLeterNo;
                                    dataRow["OutgoingXDCBDate"] = document.OutgoingLeterDate != null
                                        ? document.OutgoingLeterDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["IncomingNo"] = document.IncomingTransNo;
                                    dataRow["IncomingDate"] = document.IncomingTransDate != null
                                        ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["ICANo"] = document.ICAReviewOutTransNo;
                                    dataRow["ICADate"] = document.ICAReviewOutDate != null
                                        ? document.ICAReviewOutDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    dataRow["ICAReviewCode"] = document.ICAReviewCode;
                                    dataRow["Markup"] = document.Markup;
                                    dataRow["hardcopy"] = string.Empty;
                                    dataRow["Notes"] = document.Notes;
                                    count += 1;
                                    dtFull.Rows.Add(dataRow);
                                    complete += (document.Complete != null && document.Weight != null) ? (double)((document.Complete / 100) * (document.Weight / 100)) : 0;
                                    weight += document.Weight != null ? (double)document.Weight / 100 : 0;
                                }
                            }

                            sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                            sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                            sheets[i + 2].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");
                            sheets[i + 2].Cells[7 + dtFull.Rows.Count, 11].PutValue(complete);
                            sheets[i + 2].Cells[7 + dtFull.Rows.Count, 12].PutValue(weight);
                            if (selectedProject.IsIDC.GetValueOrDefault() == true)
                            {
                                sheets[i + 2].Cells.Columns[7].IsHidden = false;
                                sheets[i + 2].Cells.Columns[8].IsHidden = false;
                            }
                            else
                            {
                                sheets[i + 2].Cells.Columns[7].IsHidden = true;
                                sheets[i + 2].Cells.Columns[8].IsHidden = true;
                            }
                        }
                        sheets[1].IsVisible = false;
                        //sssss  sheets[0].IsVisible = false;

                        var filename = projectName.Replace("/", " ").Replace(@"\", " ") + " - " + "EMDR Filter Report " +
                                       DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                        filename = filename.Replace("/", " ").Replace(@"\", " ");
                        workbook.Save(filePath + filename);
                        this.DownloadByWriteByte(filePath + filename, filename, true);
                    }

                }
            }

        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(false, isListAll);
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentPackageService.GetById(docId);
            if (docObj != null)
            {
                if (docObj.ParentId == null || docObj.ParentId == 0)
                {
                    docObj.IsLeaf = false;
                    docObj.IsDelete = true;
                    docObj.UpdatedBy = UserSession.Current.User.Id;
                    docObj.UpdatedDate = DateTime.Now.Date;
                    this.documentPackageService.Update(docObj);
                }
                else
                {
                    var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                    if (listRelateDoc != null)
                    {
                        foreach (var objDoc in listRelateDoc)
                        {
                            objDoc.IsLeaf = false;
                            objDoc.IsDelete = true;
                            objDoc.UpdatedBy = UserSession.Current.User.Id;
                            objDoc.UpdatedDate = DateTime.Now.Date;
                            this.documentPackageService.Update(objDoc);
                        }
                    }
                }

                var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());
                if (wpObj != null)
                {
                    var docList = this.documentPackageService.GetAllEMDRByWorkgroup(wpObj.ID);
                    var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                    double complete = 0;
                    Milestone milestoneObj = new Milestone();
                    ProcessActual processActualObj = new ProcessActual();
                    List<WorkgroupManhourMonth> wmmList = new List<WorkgroupManhourMonth>();
                    WorkgroupManhourMonth wmmObj = new WorkgroupManhourMonth();

                    //update weight cho document
                    UpdateWeightDocument(projectObj, ref docList);
                    var wpUpdateList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                    //update data workpackage
                    UpdateDataWorkpackage(projectObj, docList, ref wpObj, ref wpUpdateList, ref complete);
                    if (wpObj.IsAutoCalculate.GetValueOrDefault())
                    {
                        if ((complete < 100 && docList.Where(t => t.Complete != 100).Any()))
                        {
                            //update milestone
                            UpdateMilestonActual(wpObj, UserSession.Current.User, milestoneObj);
                        }
                    }
                    //Update actual progress
                    UpdateProgressActual(wpObj, processActualObj);

                    // update data Project
                    UpdateDataProject(wpUpdateList, projectObj);

                    //Update all wpWeight trong thang workgroup manhour month cua project
                    UpdateWeightWorkgroupManhourMonth(wpObj, wmmList);

                    UpdateDataWorkgroupManhourMonth(wpObj, UserSession.Current.User, wpUpdateList, wmmObj);
                }

                if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                {
                    this.NotificationDeleteDoc(docObj);
                }
            }
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid")
            {

            }
            if (e.CommandName == RadGrid.RebindGridCommandName)
            {
                this.CustomerMenu.Items[3].Visible = false;
                this.rtvWorkgroup.UnselectAllNodes();
                this.grdDocument.Rebind();
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {

            }
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                var btnDelete = item["DeleteColumn"].Controls[0] as ImageButton;
                int projectID = Convert.ToInt32(item["ProjectId"].Text);
                var projectObj = this.scopeProjectService.GetById(projectID);
                if (item["HasAttachFile"].Text == "True")
                {
                    if (projectObj != null)
                    {
                        if (projectObj.IsIDC.GetValueOrDefault())
                        {
                            btnDelete.ImageUrl = "~/Images/deleteDisable.png";
                            btnDelete.Enabled = false;
                            if (item["StatusID"].Text == "8")
                            {
                                item.BackColor = Color.Aqua;
                                item.BorderColor = Color.Aqua;
                            }
                            else if (item["StatusID"].Text == "7")
                            {
                                item.BackColor = Color.LightGreen;
                                item.BorderColor = Color.LightGreen;


                            }
                            else if (item["StatusID"].Text == "6")
                            {
                                item.BackColor = Color.LightBlue;
                                item.BorderColor = Color.LightBlue;
                            }
                        }
                        else
                        {
                            item.BackColor = Color.Aqua;
                            item.BorderColor = Color.Aqua;
                            btnDelete.ImageUrl = "~/Images/deleteDisable.png";
                            btnDelete.Enabled = false;
                        }

                    }
                }
                else
                {
                    btnDelete.ImageUrl = "~/Images/delete.png";
                    btnDelete.Enabled = true;
                }
                Label lblWeight = (Label)item["Weight"].FindControl("lblWeight");
                if (lblWeight.Text.Contains("%"))
                {
                    double weight = Convert.ToDouble(lblWeight.Text.Replace("%", string.Empty));
                    weight = Math.Round(weight, 2);
                    lblWeight.Text = weight.ToString() + "%";
                }
                Label lblComplete = (Label)item["Complete"].FindControl("lblComplete");
                if (lblComplete.Text.Contains("%"))
                {
                    double complete = Convert.ToDouble(lblComplete.Text.Replace("%", string.Empty));
                    complete = Math.Round(complete, 2);
                    lblComplete.Text = complete.ToString() + "%";
                }
            }
            ////if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            ////{
            ////    var item = e.Item as GridEditableItem;
            ////    var lblWorkgroupName = item.FindControl("lblWorkgroupName") as Label;
            ////    var lbldocNo = item.FindControl("lbldocNo") as Label;
            ////    var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
            ////    var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
            ////    var txtManHours = item.FindControl("txtManHours") as RadNumericTextBox;
            ////    var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
            ////    var txtDeadline = item.FindControl("txtDeadline") as RadDatePicker;
            ////    var txtEndDate = item.FindControl("txtEndDate") as RadDatePicker;
            ////    var txtIsoReviseDate = item.FindControl("txtIsoReviseDate") as RadDatePicker;
            ////    var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
            ////    var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;
            ////    var listRevision = this.revisionService.GetAll();
            ////    listRevision.Insert(0, new Revision() {ID = 0});
            ////    if (ddlRevision != null)
            ////    {
            ////        ddlRevision.DataSource = listRevision;
            ////        ddlRevision.DataTextField = "Name";
            ////        ddlRevision.DataValueField = "ID";
            ////        ddlRevision.DataBind();
            ////    }

            ////    if (txtStartDate != null)
            ////    {
            ////        txtStartDate.DatePopupButton.Visible = false;
            ////    }

            ////    if (txtDeadline != null)
            ////    {
            ////        txtDeadline.DatePopupButton.Visible = false;
            ////    }

            ////    if (txtEndDate != null)
            ////    {
            ////        txtEndDate.DatePopupButton.Visible = false;
            ////    }

            ////    if (txtIsoReviseDate != null)
            ////    {
            ////        txtIsoReviseDate.DatePopupButton.Visible = false;
            ////    }

            ////    var WorkgroupName = (item.FindControl("WorkgroupName") as HiddenField).Value;
            ////    var DocNo = (item.FindControl("DocNo") as HiddenField).Value;
            ////    var DocTitle = (item.FindControl("DocTitle") as HiddenField).Value;
            ////    var RevisionId = (item.FindControl("RevisionId") as HiddenField).Value;
            ////    var ManHours = (item.FindControl("ManHours") as HiddenField).Value;
            ////    var StartDate = (item.FindControl("StartDate") as HiddenField).Value;
            ////    var Deadline = (item.FindControl("Deadline") as HiddenField).Value;
            ////    var EndDate = (item.FindControl("EndDate") as HiddenField).Value;
            ////    var IsoReviseDate = (item.FindControl("IsoReviseDate") as HiddenField).Value;
            ////    var complete = (item.FindControl("Complete") as HiddenField).Value;
            ////    var weight = (item.FindControl("Weight") as HiddenField).Value;

            ////    if (!string.IsNullOrEmpty(StartDate))
            ////    {
            ////        txtStartDate.SelectedDate = Convert.ToDateTime(StartDate);
            ////    }

            ////    if (!string.IsNullOrEmpty(Deadline))
            ////    {
            ////        txtDeadline.SelectedDate = Convert.ToDateTime(Deadline);
            ////    }

            ////    if (!string.IsNullOrEmpty(EndDate))
            ////    {
            ////        txtEndDate.SelectedDate = Convert.ToDateTime(EndDate);
            ////    }

            ////    if (!string.IsNullOrEmpty(IsoReviseDate))
            ////    {
            ////        txtIsoReviseDate.SelectedDate = Convert.ToDateTime(IsoReviseDate);
            ////    }

            ////    lblWorkgroupName.Text = WorkgroupName;
            ////    lbldocNo.Text = DocNo;
            ////    txtDocTitle.Text = DocTitle;


            ////    ddlRevision.SelectedValue = RevisionId;
            ////    txtComplete.Value = Convert.ToDouble(complete);
            ////    txtWeight.Value = Convert.ToDouble(weight);
            ////    txtManHours.Value = Convert.ToDouble(ManHours);
            ////}
        }

        protected void grdDocument_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var txtManHours = item.FindControl("txtManHours") as RadNumericTextBox;
                var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
                var txtDeadline = item.FindControl("txtDeadline") as RadDatePicker;
                var txtEndDate = item.FindControl("txtEndDate") as RadDatePicker;
                var txtIsoReviseDate = item.FindControl("txtIsoReviseDate") as RadDatePicker;
                var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
                var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;

                var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                var objDoc = this.documentPackageService.GetById(docId);

                var currentRevision = objDoc.RevisionId;
                var newRevision = Convert.ToInt32(ddlRevision.SelectedValue);
                if (currentRevision != 0 && newRevision != currentRevision)
                {
                    var docObjNew = new DocumentPackage
                    {
                        ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                        ProjectName = this.ddlProject.SelectedItem.Text,
                        WorkgroupId = objDoc.WorkgroupId,
                        WorkgroupName = objDoc.WorkgroupName,
                        DocNo = objDoc.DocNo,
                        DocTitle = txtDocTitle.Text.Trim(),
                        StartDate = txtStartDate.SelectedDate,
                        Deadline = txtDeadline.SelectedDate,
                        EndDate = txtEndDate.SelectedDate,
                        IsoReviseDate = txtIsoReviseDate.SelectedDate,
                        RevisionId = newRevision,
                        RevisionName = ddlRevision.SelectedItem.Text,
                        Complete = txtComplete.Value,
                        Weight = txtWeight.Value,
                        ManHours = txtManHours.Value,
                        DocumentTypeId = objDoc.DocumentTypeId,
                        DocumentTypeName = objDoc.DocumentTypeName,
                        IsLeaf = true,
                        IsEMDR = true,
                        ParentId = objDoc.ParentId ?? objDoc.ID,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };

                    this.documentPackageService.Insert(docObjNew);

                    objDoc.IsLeaf = false;
                }
                else
                {
                    objDoc.DocTitle = txtDocTitle.Text.Trim();
                    objDoc.StartDate = txtStartDate.SelectedDate;
                    objDoc.Deadline = txtDeadline.SelectedDate;
                    objDoc.EndDate = txtEndDate.SelectedDate;
                    objDoc.IsoReviseDate = txtIsoReviseDate.SelectedDate;
                    objDoc.ManHours = txtManHours.Value;
                    objDoc.RevisionId = newRevision;
                    objDoc.RevisionName = ddlRevision.SelectedItem.Text;
                    objDoc.Complete = txtComplete.Value.GetValueOrDefault();
                    objDoc.Weight = txtWeight.Value.GetValueOrDefault();
                }

                objDoc.UpdatedBy = UserSession.Current.User.Id;
                objDoc.UpdatedDate = DateTime.Now;

                this.documentPackageService.Update(objDoc);
            }
        }

        protected void ckbEnableFilter_OnCheckedChanged(object sender, EventArgs e)
        {
            this.grdDocument.AllowFilteringByColumn = ((CheckBox)sender).Checked;
            this.grdDocument.Rebind();
        }

        protected void radTreeFolder_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "Images/folderdir16.png";
        }

        private List<DateTime> GetBusinessDays(DateTime start, DateTime end)
        {
            List<DateTime> listdate = new List<DateTime>();
            for (var i = start; i <= end; i = i.AddDays(1))
            {
                if (i.DayOfWeek != DayOfWeek.Saturday && i.DayOfWeek != DayOfWeek.Sunday) { listdate.Add(i); }
            }
            return listdate;
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                ////this.radPbList.DataSource = permissions;
                ////this.radPbList.DataFieldParentID = "ParentId";
                ////this.radPbList.DataFieldID = "Id";
                ////this.radPbList.DataValueField = "Id";
                ////this.radPbList.DataTextField = "MenuName";
                ////this.radPbList.DataBind();
                ////this.radPbList.Items[0].Expanded = true;

                ////foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                ////{
                ////    item.ImageUrl = @"Images/listmenu.png";
                ////    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                ////}
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                ////this.radPbSystem.DataSource = permissions;
                ////this.radPbSystem.DataFieldParentID = "ParentId";
                ////this.radPbSystem.DataFieldID = "Id";
                ////this.radPbSystem.DataValueField = "Id";
                ////this.radPbSystem.DataTextField = "MenuName";
                ////this.radPbSystem.DataBind();
                ////this.radPbSystem.Items[0].Expanded = true;

                ////foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                ////{
                ////    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                ////    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                ////}
            }
        }

        private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = Aspose.Cells.ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;
            validation.ShowInput = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";
            validation.InputTitle = "Warning";

            // Set the error message.
            validation.ErrorMessage = "Please select item from the list";
            validation.InputMessage = "Please select item from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void LoadProject()
        {
            try
            {
                var wpList = new List<WorkGroup>();
                //if (UserSession.Current.IsEngineer)
                //{
                //    //edit 19/12
                //    //var wpInWorkOfEng =
                //    //    this.documentPackageService.GetAllByEngineer(UserSession.Current.User.Id)
                //    //        .Select(t => t.WorkgroupId)
                //    //        .Distinct().ToList();
                //    //wpList = this.WorkpackageList.Where(t => wpInWorkOfEng.Contains(t.ID)).ToList();
                //    wpList = this.WorkpackageList;
                //}
                //else
                //{
                //    wpList = this.WorkpackageList;
                //}
                wpList = this.WorkpackageList;
                var projectInPermission = this.scopeProjectService.GetAll(wpList.Select(t => t.ProjectId.GetValueOrDefault()).Distinct().ToList());

                //if (UserSession.Current.IsGip)
                //{
                //    projectInPermission = projectInPermission.Where(t => t.ProjectManagerId == UserSession.Current.User.Id).ToList();
                //}
                //if (!UserSession.Current.User.Role.IsWorking.GetValueOrDefault())
                //{
                //    var folderIdsInPermission = this.userDataPermissionService.GetByUserId(UserSession.Current.User.Id)
                //                                        .Select(t => t.FolderId.GetValueOrDefault()).Distinct().ToList();
                //    var projectPermiss = this.folderService.GetSpecificFolderStatic(folderIdsInPermission).Select(t=> t.ProjectId.GetValueOrDefault()).Distinct().ToList();
                //    projectInPermission = this.scopeProjectService.GetAll(projectPermiss);
                //}

                var nodefirst = "";
                var firstvalue = 0;
                var rtvProject = (RadTreeView)this.ddlProject.Items[0].FindControl("rtvProject");
                //var platform = projectInPermission.Select(t => new { t.PlatformId, t.PlatformName }).Distinct().OrderByDescending(t => t.PlatformName).ToList();
                var blockList = this.blockService.GetAll().OrderBy(t => t.Name).ToList();
                foreach (var itemBlock in blockList)
                {
                    var nodeBlock = new RadTreeNode();
                    nodeBlock.Text = itemBlock.Name;
                    nodeBlock.Value = itemBlock.ID.ToString();
                    nodeBlock.Target = "Block";
                    nodeBlock.ImageUrl = @"Images/block.png";
                    nodeBlock.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                    rtvProject.Nodes.Add(nodeBlock);
                    var fieldList = this.fieldService.GetByBlock(itemBlock.ID).OrderBy(t => t.Name).ToList();
                    if (fieldList.Any())
                    {
                        foreach (var itemField in fieldList)
                        {
                            var nodeField = new RadTreeNode();
                            nodeField.Text = itemField.Name;
                            nodeField.Value = itemField.ID.ToString();
                            nodeField.ImageUrl = "~/Images/field.png";
                            nodeField.Target = "Field";
                            nodeField.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                            nodeBlock.Nodes.Add(nodeField);
                            var platformList = this.platformService.GetByField(itemField.ID).OrderBy(t => t.Name).ToList();
                            if (platformList.Any())
                            {
                                foreach (var itemPlatform in platformList)
                                {
                                    var nodePlatform = new RadTreeNode();
                                    nodePlatform.Text = itemPlatform.Name;
                                    nodePlatform.Value = itemPlatform.ID.ToString();
                                    nodePlatform.ImageUrl = "~/Images/platform.png";
                                    nodePlatform.Target = "Platform";
                                    nodePlatform.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                                    nodeField.Nodes.Add(nodePlatform);
                                    if (projectInPermission.Any())
                                    {
                                        foreach (var itemProject in projectInPermission.Where(t => t.PlatformId == itemPlatform.ID).OrderBy(t => t.FullName).ToList())
                                        {
                                            var nodeProject = new RadTreeNode();
                                            nodeProject.Text = itemProject.FullName;
                                            nodeProject.Value = itemProject.ID.ToString();
                                            nodeProject.Target = "Project";
                                            nodeProject.ImageUrl = @"Images/project.png";
                                            nodePlatform.Nodes.Add(nodeProject);
                                            if (nodefirst == "")
                                            {
                                                nodefirst = itemProject.FullName;
                                                firstvalue = itemProject.ID;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    rtvProject.Nodes.Add(nodeBlock);
                }

                rtvProject.DataBind();

                //foreach (var nodeplatfoem in platform)
                //{
                //    var nodepl = new RadTreeNode();
                //    if (nodeplatfoem.PlatformId == 0)
                //    {
                //        nodepl.Text = "Other";
                //        nodepl.Value = nodeplatfoem.PlatformId.ToString();
                //        nodepl.Target = "Platform";
                //        nodepl.ImageUrl = @"Images/platform.png";
                //    }
                //    else
                //    {
                //        nodepl.Text = nodeplatfoem.PlatformName;
                //        nodepl.Value = nodeplatfoem.PlatformId.ToString();
                //        nodepl.Target = "Platform";
                //        nodepl.ImageUrl = @"Images/platform.png";
                //    }
                //    foreach (var nodeproject in projectInPermission.Where(t => t.PlatformId == nodeplatfoem.PlatformId))
                //    {
                //        var node = new RadTreeNode();
                //        node.Text = nodeproject.FullName;
                //        node.Value = nodeproject.ID.ToString();
                //        node.Target = "Project";
                //        node.ImageUrl = @"Images/project.png";
                //        nodepl.Nodes.Add(node);
                //        if (nodefirst == "")
                //        {
                //            nodefirst = nodeproject.FullName;
                //            firstvalue = nodeproject.ID;
                //            nodepl.Expanded = true;
                //        }
                //    }
                //    rtvProject.Nodes.Add(nodepl);
                //}
                //rtvProject.DataBind();

                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    ddlProject.Enabled = false;
                    var objScopeProject = this.scopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objScopeProject != null)
                    {
                        nodefirst = objScopeProject.FullName;
                        firstvalue = objScopeProject.ID;
                    }
                }
                //var nodeselected = rtvProject.FindNodeByText(nodefirst);
                //if (nodeselected != null)
                //{
                //    nodeselected.Selected = true;
                //    ExpandParentNode(nodeselected);
                //}
                //this.ddlProject.DataSource = projectInPermission;
                this.ddlProject.Items[0].Text = nodefirst;
                this.ddlProject.Items[0].Value = firstvalue.ToString();
                this.ddlProject.DataBind();
                this.ddlProject.SelectedIndex = 0;

                //if (projectInPermission.Any())
                //{
                //    //edit 19/12
                //    //wpList = !UserSession.Current.IsEngineer ? this.workGroupService.GetAllWorkGroupOfProject(this.ddlProject.SelectedValue != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0) : wpList;

                //}


                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var projectObj = this.scopeProjectService.GetById(projectId);
                this.ProjectFolderPath.Value = projectObj.PathLatestRevision;
            }
            catch { }
        }

        private void LoadWP()
        {
            var rtvProject = (RadTreeView)this.ddlProject.Items[0].FindControl("rtvProject");
            if (!string.IsNullOrEmpty(this.Request.QueryString["WpID"]))
            {
                ddlProject.Enabled = false;
                var wpId = Convert.ToInt32(this.Request.QueryString["WpID"]);
                if (wpId != 0)
                {
                    var wpcurrent = this.workGroupService.GetById(wpId);
                    if (wpcurrent != null)
                    {
                        var wpselected = rtvProject.FindNodeByValue(wpcurrent.ProjectId.ToString());
                        if (wpselected != null)
                        {
                            wpselected.Selected = true;
                            wpselected.ParentNode.Expanded = true;
                            ExpandParentNode(wpselected);
                        }

                        this.ddlProject.Items[0].Text = wpcurrent.ProjectName;
                        this.ddlProject.Items[0].Value = wpcurrent.ProjectId.ToString();

                        var wpList = new List<WorkGroup>();
                        List<int> roleList = ConfigurationManager.AppSettings.Get("GroupNotInVien").Split(',').Select(Int32.Parse).ToList();
                        roleList.Remove(108);

                        wpList = this.workGroupService.GetAllWorkGroupOfProject(this.ddlProject.SelectedValue != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
                        this.rtvWorkgroup.DataSource = wpList;
                        this.rtvWorkgroup.DataTextField = "FullNameRev";
                        this.rtvWorkgroup.DataValueField = "ID";
                        this.rtvWorkgroup.DataFieldID = "ID";
                        this.rtvWorkgroup.DataBind();
                    }
                }
            }
            else
            {
                var wpList = this.workGroupService.GetAllWorkGroupOfProject(this.ddlProject.SelectedValue != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);

                this.rtvWorkgroup.DataSource = wpList;
                this.rtvWorkgroup.DataTextField = "FullNameRev";
                this.rtvWorkgroup.DataValueField = "ID";
                this.rtvWorkgroup.DataFieldID = "ID";
                this.rtvWorkgroup.DataBind();

                this.rtvmove.DataSource = wpList.Where(
                                                t =>
                                                    t.ProjectId ==
                                                    (this.ddlProject.SelectedItem != null
                                                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                                        : 0));
                this.rtvmove.DataTextField = "FullNameRev";
                this.rtvmove.DataValueField = "ID";
                this.rtvmove.DataFieldID = "ID";
                this.rtvmove.DataBind();
            }
        }

        private void ExpandParentNode(RadTreeNode node)
        {
            if (node.ParentNode != null)
            {
                node.ParentNode.Expanded = true;
                ExpandParentNode(node.ParentNode);
            }
        }

        /// <summary>
        /// The repair list.
        /// </summary>
        /// <param name="listOptionalTypeDetail">
        /// The list optional type detail.
        /// </param>
        private void RepairList(ref List<OptionalTypeDetail> listOptionalTypeDetail)
        {
            var temp = listOptionalTypeDetail.Where(t => t.ParentId != null).Select(t => t.ParentId).Distinct().ToList();
            var temp2 = listOptionalTypeDetail.Select(t => t.ID).ToList();
            var tempList = new List<OptionalTypeDetail>();
            foreach (var x in temp)
            {
                if (!temp2.Contains(x.Value))
                {
                    tempList.AddRange(listOptionalTypeDetail.Where(t => t.ParentId == x.Value).ToList());
                }
            }

            var listOptionalType = tempList.Where(t => t.OptionalTypeId != null).Select(t => t.OptionalTypeId).Distinct().ToList();

            foreach (var optionalTypeId in listOptionalType)
            {
                var optionalType = this.optionalTypeService.GetById(optionalTypeId.Value);
                var tempOptTypeDetail = new OptionalTypeDetail() { ID = optionalType.ID * 9898, Name = optionalType.Name + "s" };
                listOptionalTypeDetail.Add(tempOptTypeDetail);
                ////tempList.Add(tempOptTypeDetail);
                OptionalType type = optionalType;
                foreach (var optionalTypeDetail in tempList.Where(t => t.OptionalTypeId == type.ID).ToList())
                {
                    optionalTypeDetail.ParentId = tempOptTypeDetail.ID;
                }
            }
        }

        protected void ddlProject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //var rtvProject = (RadTreeView)this.ddlProject.Items[0].FindControl("rtvProject");
            //if (rtvProject != null && rtvProject.SelectedNode != null && rtvProject.SelectedNode.Target == "Project")
            //{
            //    this.CustomerMenu.Items[3].Visible = false;

            //    var wpList = new List<WorkGroup>();
            //    wpList = this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(rtvProject.SelectedNode.Value));

            //    var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
            //    var projectObj = this.scopeProjectService.GetById(projectId);
            //    this.ProjectFolderPath.Value = projectObj.PathLatestRevision;

            //    //if (!UserSession.Current.User.Role.IsWorking.GetValueOrDefault())
            //    //{
            //    //    wpList = this.workGroupService.GetAllWorkGroupOfProject(this.ddlProject.SelectedItem != null
            //    //                                          ? Convert.ToInt32(this.ddlProject.SelectedValue)
            //    //                                          : 0);
            //    //}

            //    this.rtvWorkgroup.DataSource = wpList;
            //    this.rtvWorkgroup.DataTextField = "FullNameRev";
            //    this.rtvWorkgroup.DataValueField = "ID";
            //    this.rtvWorkgroup.DataFieldID = "ID";
            //    this.rtvWorkgroup.DataBind();

            //    this.rtvmove.DataSource = wpList;
            //    this.rtvmove.DataTextField = "FullNameRev";
            //    this.rtvmove.DataValueField = "ID";
            //    this.rtvmove.DataFieldID = "ID";
            //    this.rtvmove.DataBind();

            //    this.grdDocument.Rebind();
            //}

        }

        protected void grdDocument_Init(object sender, EventArgs e)
        {
        }

        protected void grdDocument_DataBound(object sender, EventArgs e)
        {
        }

        protected void rtvWorkgroup_NodeClick(object sender, RadTreeNodeEventArgs e)
        {

            this.CustomerMenu.Items[3].Visible = true;

            var txtWorkgroupComplete = this.CustomerMenu.Items[3].FindControl("txtWorkgroupComplete") as RadNumericTextBox;
            var txtWorkgroupWeight = this.CustomerMenu.Items[3].FindControl("txtWorkgroupWeight") as RadNumericTextBox;

            var workgroup = this.workGroupService.GetById(Convert.ToInt32(e.Node.Value));
            if (workgroup != null)
            {
                if (txtWorkgroupComplete != null)
                {
                    txtWorkgroupComplete.Value = workgroup.Complete;
                }

                if (txtWorkgroupWeight != null)
                {
                    txtWorkgroupWeight.Value = workgroup.Weight;
                }



                //if (!UserSession.Current.User.Role.IsWorking.GetValueOrDefault())
                //{
                //    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                //    this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
                //    this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                //    this.CustomerMenu.Visible = false;
                //    this.radMenu.Visible = false;
                //}
            }

            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The btn download_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnDownload_Click(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).Parent.Parent as GridDataItem;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentNewService.GetById(docId);
            var docPackName = string.Empty;
            if (docObj != null)
            {
                //docPackName = docObj.Name;
                //var serverDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_" + docObj.Name + "_Pack.rar");

                //var attachFiles = this.attachFileService.GetAllByDocId(docId);

                //var temp = ZipPackage.CreateFile(serverDocPackPath);

                //foreach (var attachFile in attachFiles)
                //{
                //    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                //    {
                //        temp.Add(Server.MapPath(attachFile.FilePath));
                //    }
                //}

                //this.DownloadByWriteByte(serverDocPackPath, docPackName + ".rar", true);
            }
        }

        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {

            //if (e.Item is GridFilteringItem)
            //{
            //    var filterItem = (GridFilteringItem)e.Item;
            //    var selectedProperty = new List<string>();

            //    var ddlFilterRev = (RadComboBox)filterItem.FindControl("ddlFilterRev");
            //}
        }

        protected DateTime? SetPublishDate(GridItem item)
        {
            if (item.OwnerTableView.GetColumn("Index27").CurrentFilterValue == string.Empty)
            {
                return new DateTime?();
            }
            else
            {
                return DateTime.Parse(item.OwnerTableView.GetColumn("Index27").CurrentFilterValue);
            }
        }

        /// <summary>
        /// The bind tree view combobox.
        /// </summary>
        /// <param name="optionalType">
        /// The optional type.
        /// </param>
        /// <param name="ddlObj">
        /// The ddl obj.
        /// </param>
        /// <param name="rtvName">
        /// The rtv name.
        /// </param>
        /// <param name="listOptionalTypeDetailFull">
        /// The list optional type detail full.
        /// </param>
        private void BindTreeViewCombobox(int optionalType, RadComboBox ddlObj, string rtvName, IEnumerable<OptionalTypeDetail> listOptionalTypeDetailFull)
        {
            var rtvobj = (RadTreeView)ddlObj.Items[0].FindControl(rtvName);
            if (rtvobj != null)
            {
                var listOptionalTypeDetail = listOptionalTypeDetailFull.Where(t => t.OptionalTypeId == optionalType).ToList();
                this.RepairList(ref listOptionalTypeDetail);

                rtvobj.DataSource = listOptionalTypeDetail;
                rtvobj.DataFieldParentID = "ParentId";
                rtvobj.DataTextField = "Name";
                rtvobj.DataValueField = "ID";
                rtvobj.DataFieldID = "ID";
                rtvobj.DataBind();
            }
        }

        protected void rtvWorkgroup_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/package.png";
        }

        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"Images/project.png";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void NotificationDeleteDoc(DocumentPackage docObj)
        {
            if (docObj.EngineerId != "0")
            {
                var engineer = new List<User>();
                if (docObj.EngineerId.Contains(","))
                {
                    var listEng = docObj.EngineerId.Split(',');
                    foreach (var item in listEng)
                    {
                        engineer.Add(this.userService.GetByID(item));
                    }
                }
                var wpObj = this.workGroupService.GetById(docObj.WorkgroupId.GetValueOrDefault());

                var emailList = this.userService.GetAllByRoleId(wpObj != null ? wpObj.DepartmentId.GetValueOrDefault() : 0)
                    .Where(t => t.IsChief.GetValueOrDefault())
                    .Select(t => t.Email)
                    .Where(t => !string.IsNullOrEmpty(t)).ToList();

                if (engineer != null && engineer.Count > 0)
                {
                    foreach (var item in engineer)
                    {
                        if (!string.IsNullOrEmpty(item.Email))
                        {
                            emailList.Add(item.Email);
                        }
                    }
                    var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.DeleteDoc);
                    if (notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault() && engineer[0].Role.Notify.GetValueOrDefault())
                    {
                        var smtpClient = new SmtpClient
                        {
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                            EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                            Host = ConfigurationManager.AppSettings["Host"],
                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                            Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                        };
                        var deletedUser = this.userService.GetByID(UserSession.Current.User.Id);
                        var subject = notificationTemplate.Subject.Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#DeletedUser#", deletedUser != null ? deletedUser.FullName : string.Empty);
                        string strEnginner = string.Join(" - ", engineer.Select(t => t.FullName));
                        var message = new MailMessage();
                        message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                        message.Subject = subject;
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = notificationTemplate.Contents
                            .Replace("#DeletedUser#", deletedUser != null ? deletedUser.FullName : string.Empty)
                            .Replace("#WPNumber#", wpObj != null ? wpObj.Name : string.Empty)
                            .Replace("#DocNumber#", docObj.DocNo)
                            .Replace("#DocTitle#", docObj.DocTitle)
                            .Replace("#DocType#", docObj.DocumentTypeName)
                            .Replace("#DocRev#", docObj.RevisionName)
                            .Replace("#DocEng#", strEnginner)
                            .Replace("#DocStartDate#", docObj.StartDate != null ? docObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocDeadline#", docObj.Deadline != null ? docObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocFinishDate#", docObj.EndDate != null ? docObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocISODate#", docObj.IsoReviseDate != null ? docObj.IsoReviseDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                            .Replace("#DocWeight#", docObj.Weight != null ? docObj.Weight.Value.ToString() : string.Empty)
                            .Replace("#DocComplete#", docObj.Complete != null ? docObj.Complete.Value.ToString() : string.Empty);


                        foreach (var to in emailList)
                        {
                            message.To.Add(new MailAddress(to));
                        }
                        smtpClient.Send(message);
                    }
                }
            }
        }

        protected void rtvmove_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/package.png";
        }

        protected void btSave_Click(object sender, EventArgs e)
        {

            if (rtvmove.SelectedNode != null)
            {
                var wp = this.workGroupService.GetById(Convert.ToInt32(this.rtvmove.SelectedNode.Value));
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        if (docObj.ParentId == null)
                        {
                            docObj.WorkgroupId = wp.ID;
                            docObj.WorkgroupName = wp.Name;
                            docObj.WorkgroupFullName = wp.FullName;
                            docObj.WorkgroupRevName = wp.FullNameRev;

                            docObj.DeparmentId = wp.DepartmentId;
                            docObj.DeparmentName = wp.DepartmentName;

                            docObj.UpdatedBy = UserSession.Current.User.Id;
                            docObj.UpdatedDate = DateTime.Now;

                            this.documentPackageService.Update(docObj);
                        }
                        else
                        {
                            var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                            if (listRelateDoc != null)
                            {
                                foreach (var objDoc in listRelateDoc)
                                {
                                    docObj.WorkgroupId = wp.ID;
                                    docObj.WorkgroupName = wp.Name;
                                    docObj.WorkgroupFullName = wp.FullName;
                                    docObj.WorkgroupRevName = wp.FullNameRev;

                                    docObj.DeparmentId = wp.DepartmentId;
                                    docObj.DeparmentName = wp.DepartmentName;

                                    docObj.UpdatedBy = UserSession.Current.User.Id;
                                    docObj.UpdatedDate = DateTime.Now;
                                    this.documentPackageService.Update(objDoc);
                                }
                            }
                        }
                    }
                }
                this.grdDocument.Rebind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "<script>alert('Please select workpackage.') </script>", false);
            }
        }

        protected void rtvProject_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            var rtvProject = (RadTreeView)this.ddlProject.Items[0].FindControl("rtvProject");
            if (rtvProject != null && rtvProject.SelectedNode != null && e.Node.Level == 3)
            {
                this.CustomerMenu.Items[3].Visible = false;

                var wpList = new List<WorkGroup>();
                wpList = this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(rtvProject.SelectedNode.Value));

                var projectId = Convert.ToInt32(rtvProject.SelectedNode.Value);
                var projectObj = this.scopeProjectService.GetById(projectId);

                if (UserSession.Current.IsGip)
                {
                    if (projectObj.ProjectManagerId != UserSession.Current.User.Id)
                    {
                        this.CustomerMenu.Items[0].Visible = false;
                        this.CustomerMenu.Items[1].Visible = false;
                        this.CustomerMenu.Items[2].Controls[4].Visible = false;
                    }
                }
                this.ProjectFolderPath.Value = projectObj.PathLatestRevision;

                this.rtvWorkgroup.DataSource = wpList;
                this.rtvWorkgroup.DataTextField = "FullNameRev";
                this.rtvWorkgroup.DataValueField = "ID";
                this.rtvWorkgroup.DataFieldID = "ID";
                this.rtvWorkgroup.DataBind();

                this.rtvmove.DataSource = wpList;
                this.rtvmove.DataTextField = "FullNameRev";
                this.rtvmove.DataValueField = "ID";
                this.rtvmove.DataFieldID = "ID";
                this.rtvmove.DataBind();

                this.grdDocument.Rebind();
            }
        }

        protected void rtvProject_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            var rtvProject = (RadTreeView)this.ddlProject.Items[0].FindControl("rtvProject");
            var ID = Convert.ToInt32(e.Node.Value);
            var type = e.Node.Level;
            if (type == 0)
            {
                var fieldList = this.fieldService.GetByBlock(ID);
                if (fieldList.Any())
                {
                    foreach (var itemField in fieldList.OrderBy(t => t.Name))
                    {
                        var nodeField = new RadTreeNode();
                        nodeField.Text = itemField.Name;
                        nodeField.Value = itemField.ID.ToString();
                        nodeField.ImageUrl = "~/Images/field.png";
                        nodeField.Target = "Field";
                        nodeField.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                        e.Node.Nodes.Add(nodeField);
                    }

                    e.Node.Expanded = true;
                }
            }
            else if (type == 1)
            {
                var platformList = this.platformService.GetByField(ID);
                if (platformList.Any())
                {
                    foreach (var itemPlatform in platformList.OrderBy(t => t.Name))
                    {
                        var nodePlatform = new RadTreeNode();
                        nodePlatform.Text = itemPlatform.Name;
                        nodePlatform.Value = itemPlatform.ID.ToString();
                        nodePlatform.ImageUrl = "~/Images/platform.png";
                        nodePlatform.Target = "Platform";
                        nodePlatform.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                        e.Node.Nodes.Add(nodePlatform);
                    }

                    e.Node.Expanded = true;
                }
            }
            else if (type == 2)
            {
                var projectInPermission = this.scopeProjectService.GetAll(WorkpackageList.Select(t => t.ProjectId.GetValueOrDefault()).Distinct().ToList());

                if (UserSession.Current.IsGip)
                {
                    projectInPermission = projectInPermission.Where(t => t.ProjectManagerId == UserSession.Current.User.Id).ToList();
                }
                if (projectInPermission.Any())
                {
                    foreach (var itemProject in projectInPermission.Where(t => t.PlatformId == ID).OrderBy(t => t.FullName))
                    {
                        var nodeProject = new RadTreeNode();
                        nodeProject.Text = itemProject.FullName;
                        nodeProject.Value = itemProject.ID.ToString();
                        nodeProject.Target = "Project";
                        nodeProject.ImageUrl = @"Images/project.png";
                        e.Node.Nodes.Add(nodeProject);
                    }
                }
            }
            rtvProject.DataBind();
        }

        #region UpdateData
        private void UpdateWeightDocument(ScopeProject projectObj, ref List<DocumentPackage> docList)
        {
            try
            {
                var totalPlanManhour = 0.0;
                totalPlanManhour = docList.Aggregate(totalPlanManhour, (current, t) => current + t.ManHourPlan.GetValueOrDefault());

                foreach (var doc in docList)
                {
                    doc.Weight = (doc.ManHourPlan.GetValueOrDefault() / totalPlanManhour) * 100;
                    this.documentPackageService.Update(doc);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkpackage(ScopeProject projectObj, List<DocumentPackage> docList, ref WorkGroup wpObj, ref List<WorkGroup> wpList, ref double complete)
        {
            try
            {
                if (docList.Count > 0)
                {
                    //update complete workpackage
                    complete = 0.0;
                    complete = docList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                    complete = Math.Round(complete, 5);
                    if (complete == 100 || !docList.Where(t => t.Complete < 100).Any())
                    {
                        wpObj.EndDate = DateTime.Now;
                        wpObj.Complete = 100;
                    }
                    else
                    {
                        wpObj.Complete = complete;
                    }
                }

                // Update total document weight for workpackage
                double totalWeight = 0.0;
                totalWeight = docList.Sum(t => t.Weight.GetValueOrDefault());
                totalWeight = Math.Round(totalWeight, 2);
                totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                wpObj.TotalDocWeight = totalWeight;

                // Update total man hours plan & actual for workpackage
                double totalManhourPlan = 0.0;
                totalManhourPlan = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                wpObj.TotalManHours = totalManhourPlan;

                double totalManHourActual = 0.0;
                totalManHourActual = docList.Sum(t => t.ManHours.GetValueOrDefault());
                wpObj.UsedManHours = totalManHourActual;

                // Update can delete for workpackage
                wpObj.CanDelete = !docList.Any();
                this.workGroupService.Update(wpObj);

                //update weight all WP of PJ
                if (projectObj.AutoCalculateWeightWorkGroup == true)
                {
                    var sumTotalManHours = 0.0;
                    sumTotalManHours = wpList.Sum(t => t.TotalManHours.GetValueOrDefault());
                    foreach (var items in wpList)
                    {
                        var calWeight = ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) > 0 ? ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) : 0;
                        items.Weight = calWeight >= 100 ? 100 : calWeight;
                        this.workGroupService.Update(items);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateMilestonActual(WorkGroup wpObj, User userObj, Milestone milestoneObj)
        {
            try
            {
                var milestoneList = this.milestoneService.GetAllByWorkpackage(wpObj.ID).OrderByDescending(t => t.MilestoneDate).ToList();
                if (milestoneList.Count > 0)
                {
                    var presentMilestoneObj = milestoneList.FirstOrDefault(t => t.MilestoneDate != null
                                                                         && t.MilestoneDate.Value.Month == DateTime.Now.Month
                                                                         && t.MilestoneDate.Value.Year == DateTime.Now.Year);
                    if (presentMilestoneObj != null)
                    {
                        milestoneObj = presentMilestoneObj;
                        var prevIndex = milestoneList.IndexOf(milestoneObj) + 1;
                        var prevRealTotal = 0.0;
                        var prevPlanTotal = 0.0;
                        if (prevIndex < (milestoneList.Count - 1))
                        {
                            var prevMilestoneObj = milestoneList[prevIndex];
                            if (prevMilestoneObj != null)
                            {
                                prevRealTotal = prevMilestoneObj.RealTotal.GetValueOrDefault();
                                prevPlanTotal = prevMilestoneObj.PlanTotal.GetValueOrDefault();
                            }
                            else
                            {
                                prevRealTotal = 0;
                                prevPlanTotal = 0;
                            }
                        }
                        else
                        {
                            prevRealTotal = 0;
                            prevPlanTotal = 0;
                        }

                        // calculator real
                        double real = Math.Round(wpObj.Complete.GetValueOrDefault() - prevRealTotal, 2);
                        milestoneObj.Real = real;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PlanTotal = (prevPlanTotal + milestoneObj.PlanPercent) > 100 ? 100 : (prevPlanTotal + milestoneObj.PlanPercent);
                        milestoneObj.UpdatedBy = userObj.Id;
                        milestoneObj.UpdatedDate = DateTime.Now;
                        this.milestoneService.Update(milestoneObj);
                    }
                    else
                    {
                        var lastedMilestoneObj = milestoneList[0];
                        milestoneObj = new Milestone();
                        milestoneObj.MilestoneDate = DateTime.Now;
                        milestoneObj.PlanPercent = 0;
                        milestoneObj.PlanTotal = lastedMilestoneObj.PlanTotal;
                        double preReal = Math.Round(wpObj.Complete.GetValueOrDefault() - lastedMilestoneObj.RealTotal.GetValueOrDefault(), 2);
                        milestoneObj.Real = preReal;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PerformingUser = lastedMilestoneObj.PerformingUser;
                        milestoneObj.Note = lastedMilestoneObj.Note;
                        milestoneObj.WorkpackageId = wpObj.ID;
                        milestoneObj.WorkpackageName = wpObj.Name;
                        if (DateTime.Now.Year > lastedMilestoneObj.MilestoneDate.Value.Year && (DateTime.Now.Year - lastedMilestoneObj.MilestoneDate.Value.Year) > 0)
                        {
                            milestoneObj.PlanOfYear = 0;
                        }
                        else
                        {
                            milestoneObj.PlanOfYear = lastedMilestoneObj.PlanOfYear;
                        }
                        milestoneObj.CreatedBy = userObj.Id;
                        milestoneObj.CreatedDate = DateTime.Now;
                        this.milestoneService.Insert(milestoneObj);
                    }
                }
                else
                {
                    //milestoneObj = new Milestone();
                    milestoneObj.MilestoneDate = DateTime.Now;
                    milestoneObj.PlanPercent = 0;
                    milestoneObj.PlanTotal = 0;
                    milestoneObj.RealTotal = wpObj.Complete;
                    milestoneObj.Real = wpObj.Complete;
                    milestoneObj.PerformingUser = string.Empty;
                    milestoneObj.Note = string.Empty;
                    milestoneObj.WorkpackageId = wpObj.ID;
                    milestoneObj.WorkpackageName = wpObj.Name;
                    milestoneObj.PlanOfYear = 0;
                    milestoneObj.CreatedBy = userObj.Id;
                    milestoneObj.CreatedDate = DateTime.Now;
                    this.milestoneService.Insert(milestoneObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateProgressActual(WorkGroup wpObj, ProcessActual processActualObj)
        {
            try
            {
                var projectId = wpObj.ProjectId.GetValueOrDefault();
                var projectObj = this.scopeProjectService.GetById(projectId);
                var tempProcessActualObj = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, wpObj.ID);
                if (tempProcessActualObj != null)
                {
                    processActualObj = tempProcessActualObj;
                    if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null)
                    {
                        if (string.IsNullOrEmpty(processActualObj.Actual))
                        {
                            processActualObj.Actual = "0";
                        }
                        if (string.IsNullOrEmpty(processActualObj.ActualMonth))
                        {
                            processActualObj.ActualMonth = "0";
                        }
                        var progressActualWeekList = processActualObj.Actual.Split('$').ToList();
                        var progressActualMonthList = processActualObj.ActualMonth.Split('$').ToList();
                        var countWeek = 0;
                        var countMonth = 0;
                        for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualWeekList.Count() > countWeek)
                            {
                                if (DateTime.Now > j.AddDays(7))
                                {
                                    countWeek += 1;
                                }
                            }
                        }
                        var currentMonth = 0;
                        for (var j = projectObj.StartDate.GetValueOrDefault();
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualMonthList.Count() > countMonth)
                            {
                                if (DateTime.Now.Month > j.AddDays(7).Month && DateTime.Now > j.AddDays(7) && currentMonth != j.AddDays(7).Month)
                                {
                                    currentMonth = j.Month;
                                    countMonth++;
                                }
                            }
                        }
                        if (progressActualWeekList.Count() >= countWeek && progressActualWeekList.Count() > 0 && countWeek > 0)
                        {
                            progressActualWeekList = progressActualWeekList.Take(countWeek).ToList();
                            progressActualWeekList[countWeek - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.Actual = string.Join("$", progressActualWeekList);
                            this.processActualService.Update(processActualObj);
                        }
                        if (progressActualMonthList.Count() >= countMonth && progressActualMonthList.Count() > 0 && countMonth > 0)
                        {
                            progressActualMonthList = progressActualMonthList.Take(countMonth).ToList();
                            progressActualMonthList[countMonth - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.ActualMonth = string.Join("$", progressActualMonthList);
                            this.processActualService.Update(processActualObj);
                        }
                    }
                }
                else
                {
                    processActualObj = new ProcessActual();
                    processActualObj.ProjectId = wpObj.ProjectId;
                    processActualObj.WorkgroupId = wpObj.ID;
                    processActualObj.Actual = wpObj.Complete.ToString();
                    processActualObj.ActualMonth = wpObj.Complete.ToString();
                    this.processActualService.Insert(processActualObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private DateTime GetMondayOfWeek(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        private DateTime GetFridayOfWeek(DateTime date)
        {
            return GetMondayOfWeek(date).AddDays(4);
        }

        private void UpdateDataProject(List<WorkGroup> wpList, ScopeProject projectObj)
        {
            try
            {
                if (projectObj != null)
                {
                    projectObj.CanDelete = false;
                    if (projectObj.IsAutoCalculate.GetValueOrDefault())
                    {
                        double complete = 0.0;
                        complete = wpList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                        projectObj.Complete = Math.Round(complete, 5);
                        double totalWeight = 0.0;
                        totalWeight = wpList.Sum(t => t.Weight.GetValueOrDefault());
                        totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                        projectObj.TotalWorkpackageWeight = Math.Round(totalWeight, 2);
                    }
                    this.scopeProjectService.Update(projectObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateWeightWorkgroupManhourMonth(WorkGroup wpObj, List<WorkgroupManhourMonth> wmmList)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                wmmList = this.workgroupManhourMonthService.GetByPJ(wpObj.ProjectId.GetValueOrDefault(), month, year);
                foreach (var wmmItem in wmmList)
                {
                    var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                    if (wpItem != null)
                    {
                        wmmItem.WorkgroupName = wpItem.Name;
                        wmmItem.WorkgroupWeight = wpItem.Weight;
                        this.workgroupManhourMonthService.Update(wmmItem);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkgroupManhourMonth(WorkGroup wpObj, User userObj, List<WorkGroup> wpList, WorkgroupManhourMonth wmmObj)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                var docList = this.documentPackageService.GetAllByWorkgroup(wpObj.ID);
                var manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                var presentWMMObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), month, year);
                var prevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpObj.ID, month, year);
                if (presentWMMObj != null)
                {
                    wmmObj = presentWMMObj;
                    wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                    wmmObj.ManhourPlanTotal = manhourPlanTotal;
                    wmmObj.ManhourPlanMonth = (wmmObj.ManhourPlanTotal.GetValueOrDefault() * wmmObj.CompletePlanMonth.GetValueOrDefault()) / 100;
                    wmmObj.CompleteActualTotal = wpObj.Complete.GetValueOrDefault();
                    wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                    //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                    if (prevWMMObj != null)
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                    }
                    else
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                    }

                    wmmObj.UpdateBy = userObj.Id;
                    wmmObj.UpdateDate = DateTime.Now;
                    this.workgroupManhourMonthService.Update(wmmObj);
                }
                else
                {
                    foreach (var wpItem in wpList)
                    {
                        docList = this.documentPackageService.GetAllByWorkgroup(wpItem.ID);
                        manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                        var tempObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpItem.ID, wpItem.ProjectId.GetValueOrDefault(), month, year);
                        var tempPrevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpItem.ID, month, year);
                        if (tempObj == null)
                        {
                            wmmObj = new WorkgroupManhourMonth();
                            wmmObj.Month = month;
                            wmmObj.Year = year;
                            wmmObj.WorkgroupID = wpItem.ID;
                            wmmObj.WorkgroupName = wpItem.Name;
                            wmmObj.DeparmentID = wpItem.DepartmentId;
                            wmmObj.ProjectID = wpItem.ProjectId.GetValueOrDefault();
                            wmmObj.WorkgroupWeight = wpItem.Weight.GetValueOrDefault();
                            wmmObj.ManhourPlanTotal = manhourPlanTotal;
                            wmmObj.CompleteActualTotal = wpItem.Complete.GetValueOrDefault();
                            wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                            //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                            if (tempPrevWMMObj != null)
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - tempPrevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - tempPrevWMMObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            else
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            wmmObj.CreateBy = userObj.Id;
                            wmmObj.CreateDate = DateTime.Now;
                            this.workgroupManhourMonthService.Insert(wmmObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourWeek(DocumentPackage documentObj, User userObj, UserManhour userManhourObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;

                CultureInfo myCI = CultureInfo.CurrentCulture;
                System.Globalization.Calendar myCal = myCI.Calendar;
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var listUserManhour = this.userManhourService.GetByDocIDAndUserID(documentObj.ID, user.Id);
                            listUserManhour = listUserManhour.OrderByDescending(t => t.ID).ToList();

                            var currentUserManhourWeek = listUserManhour.FirstOrDefault(t => myCal.GetWeekOfYear(DateTime.Now.Date, myCWR, myFirstDOW) == myCal.GetWeekOfYear(t.Date.GetValueOrDefault(), myCWR, myFirstDOW));
                            int index = listUserManhour.IndexOf(currentUserManhourWeek);
                            var prevUserManhourWeek = new UserManhour();
                            if (index != -1 && (index + 1) < listUserManhour.Count)
                            {
                                prevUserManhourWeek = listUserManhour[(index + 1)];
                            }
                            if (currentUserManhourWeek != null)
                            {
                                userManhourObj = currentUserManhourWeek;
                                if (prevUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - prevUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UpdateBy = userObj.Id;
                                userManhourObj.UpdateDate = DateTime.Now;
                                this.userManhourService.Update(userManhourObj);
                            }
                            else
                            {
                                var lastedUserManhourWeek = new UserManhour();
                                if (listUserManhour.Count > 0)
                                {
                                    lastedUserManhourWeek = listUserManhour[0];
                                }
                                userManhourObj = new UserManhour();
                                userManhourObj.DocumentID = documentObj.ID;
                                userManhourObj.Date = DateTime.Now.Date;
                                userManhourObj.DeparmentID = user.RoleId;
                                userManhourObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourObj.ProjectID = documentObj.ProjectId;
                                if (lastedUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - lastedUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - lastedUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UserID = user.Id;
                                userManhourObj.UserName = user.FullName;
                                userManhourObj.CreateBy = userObj.Id;
                                userManhourObj.CreateDate = DateTime.Now;
                                this.userManhourService.Insert(userManhourObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourMonth(DocumentPackage documentObj, User userObj, UserManhourMonth userManhourMonthObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;
                var date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var currentUserManhourMonth = this.userManhourMonthService.GetByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            var prevUserManhourMonth = this.userManhourMonthService.GetLastedByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            if (currentUserManhourMonth != null)
                            {
                                userManhourMonthObj = currentUserManhourMonth;
                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UpdateBy = userObj.Id;
                                userManhourMonthObj.UpdateDate = DateTime.Now;
                                this.userManhourMonthService.Update(userManhourMonthObj);
                            }
                            else
                            {
                                userManhourMonthObj = new UserManhourMonth();
                                userManhourMonthObj.DocumentID = documentObj.ID;
                                userManhourMonthObj.Month = DateTime.Now.Month;
                                userManhourMonthObj.Year = DateTime.Now.Year;
                                userManhourMonthObj.DeparmentID = user.RoleId;
                                userManhourMonthObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourMonthObj.ProjectID = documentObj.ProjectId;

                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UserID = user.Id;
                                userManhourMonthObj.UserName = user.FullName;
                                userManhourMonthObj.CreateBy = userObj.Id;
                                userManhourMonthObj.CreateDate = DateTime.Now;
                                this.userManhourMonthService.Insert(userManhourMonthObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}
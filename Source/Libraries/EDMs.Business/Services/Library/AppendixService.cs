﻿namespace EDMs.Business.Services.Library
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Library;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class AppendixService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly AppendixDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppendixService"/> class.
        /// </summary>
        public AppendixService()
        {
            this.repo = new AppendixDAO();
        }

        #region Get (Advances)
        public List<Appendix> GetByWorkWithoutActive(int workID)
        {
            return this.repo.GetByWorkWithoutActive(workID);
        }
        public List<Appendix> GetByWork(int workID)
        {
            return this.repo.GetByWork(workID);
        }
        public List<Appendix> GetByParent(int parentID)
        {
            return this.repo.GetByParent(parentID);
        }
        public List<Appendix> GetByWorkWithoutID(int workID,int year, int id)
        {
            return this.repo.GetByWorkWithoutID(workID, year, id);
        }

        public Appendix GetByCodeType(string code, List<int> workID)
        {
            return this.repo.GetByCodeType(code, workID);
        }

        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Appendix> GetAll()
        {
            return this.repo.GetAll().ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Appendix GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Appendix bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Appendix bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Appendix bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Data.DAO.Scope;
using EDMs.Data.Entities;

namespace EDMs.Business.Services.Scope
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class MilestoneService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly MilestoneDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MilestoneService"/> class.
        /// </summary>
        public MilestoneService()
        {
            this.repo = new MilestoneDAO();
        }

        #region Get (Advances)
        public List<Milestone> GetAllByWorkpackage(int workpackageId)
        {
            return this.repo.GetAll().Where(t => t.WorkpackageId == workpackageId).ToList();
        }

        public List<Milestone> GetAllByWorkpackage(int workpackageId, int month, int year)
        {
            return this.repo.GetAll().Where(t => t.WorkpackageId == workpackageId
                && (t.MilestoneDate != null && t.MilestoneDate.Value.Month == month && t.MilestoneDate.Value.Year == year)).ToList();
        }

        public List<Milestone> GetAllByWorkpackage(List<int> workpackageIds, int month, int year)
        {
            return this.repo.GetAll().Where(t => workpackageIds.Contains(t.WorkpackageId.GetValueOrDefault())
                && (t.MilestoneDate != null && t.MilestoneDate.Value.Month == month && t.MilestoneDate.Value.Year == year)).ToList();
        }

        public List<Milestone> GetAllByWorkpackage(List<int> workpackageIds, DateTime fromDate, DateTime toDate)
        {
            return this.repo.GetAll().Where(t => workpackageIds.Contains(t.WorkpackageId.GetValueOrDefault())
                && t.MilestoneDate != null 
                && t.MilestoneDate.Value >= fromDate 
                && t.MilestoneDate.Value < toDate).ToList();
        }

        public List<Milestone> GetAllByWorkpackage(List<int> workpackageIds)
        {
            return this.repo.GetAll().Where(t => workpackageIds.Contains(t.WorkpackageId.GetValueOrDefault())).ToList();
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Milestone> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        public List<Milestone> GetAllByMonth(int month, int year)
        {
            return this.repo.GetAll().Where(t => t.MilestoneDate != null 
                && t.MilestoneDate.Value.Month == month
                && t.MilestoneDate.Value.Year == year).ToList();
        }

        public List<Milestone> GetAllByMonth(int month, int year, int department)
        {
            return this.repo.GetAll().Where(t => t.MilestoneDate != null && t.Workpackage != null
                && t.MilestoneDate.Value.Month == month
                && t.MilestoneDate.Value.Year == year
                && t.Workpackage.DepartmentId == department).ToList();
        }

        public List<Milestone> GetAllByWorkpackage(DateTime fromDate, DateTime toDate, int wpID)
        {
            return this.repo.GetAll().Where(t => t.MilestoneDate != null
                && t.MilestoneDate >= fromDate
                && t.MilestoneDate < toDate
                && t.WorkpackageId == wpID).ToList();
        }

        public Milestone GetByLasted(int wpID)
        {
            return this.repo.GetByLasted(wpID);
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Milestone GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Milestone bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Milestone bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Milestone bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

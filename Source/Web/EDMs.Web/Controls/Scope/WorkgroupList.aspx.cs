﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Web;
using System.Data;
using System.Reflection;
using System.Drawing;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using OfficeHelper.Utilities.Data;
using Telerik.Web.UI;
using CheckBox = System.Web.UI.WebControls.CheckBox;
using Image = System.Web.UI.WebControls.Image;
using RadioButton = System.Web.UI.WebControls.RadioButton;
using EDMs.Business.Services.Function;
using EDMs.Web.Function;
using System.Globalization;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// Class customer
    /// </summary>
    public partial class WorkgroupList : Page
    {
        private readonly PermissionService permissionService = new PermissionService();

        private readonly WorkGroupService workGroupService = new WorkGroupService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly RoleService roleService = new RoleService();

        private readonly AreaService areaService = new AreaService();
        private readonly DisciplineService disciplineService = new DisciplineService();
        private readonly WorkpackageContentService workpackageContentService = new WorkpackageContentService();
        private readonly UserService userService = new UserService();
        private readonly WPRevisionService wpRevisionService = new WPRevisionService();

        private readonly EmailNotificationTemplateService emailNotificationTemplateService = new EmailNotificationTemplateService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();
        private readonly RevisionService revisionService = new RevisionService();
        private readonly RoleService roleservice = new RoleService();
        private readonly AttachFilesPackageService attachFileService = new AttachFilesPackageService();

        private readonly ProjectAppendixService projectAppendixService = new ProjectAppendixService();
        private readonly FolderService folderService = new FolderService();
        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();
        private readonly MilestoneService milestoneService;
        private readonly ProcessActualService processActualService;
        private readonly ProcessPlanedService processPlanedService;
        private readonly UserManhourService userManhourService = new UserManhourService();
        private readonly UserManhourMonthService userManhourMonthService = new UserManhourMonthService();
        private readonly WorkgroupManhourMonthService workgroupManhourMonthService = new WorkgroupManhourMonthService();

        private int idProject = 0;
        /// <summary>
        /// 
        /// The unread pattern.
        /// </summary>
        //protected const string unreadPattern = @"\(\d+\)";

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            RadPane temp = (RadPane)this.Master.FindControl("leftPane");
            temp.Collapsed = true;
            if (!this.Page.IsPostBack)
            {
                //chinh sua cho eng xem tat ca du an
                if (UserSession.Current.IsEngineer)
                {
                    this.IsEngineer.Value = "true";
                    PermissionOnlyView();

                    for (int i = 0; i < this.radMenu.Items.Count; i++)
                    {
                        if (i != 4)
                        {
                            this.radMenu.Items[i].Visible = false;
                        }
                    }
                }
                else if (UserSession.Current.IsSuperViewer)
                {
                    PermissionOnlyExport();
                    var list = new List<int>() { 3, 4, 7 };
                    for (int i = 0; i < this.radMenu.Items.Count; i++)
                    {
                        if (!list.Contains(i))
                        {
                            this.radMenu.Items[i].Visible = false;
                        }
                    }
                }
                else if (UserSession.Current.IsLeader)
                {
                    var list = new List<int>() { 3, 4, 5, 7 };
                    for (int i = 0; i < this.radMenu.Items.Count; i++)
                    {
                        if (!list.Contains(i))
                        {
                            this.radMenu.Items[i].Visible = false;
                        }
                    }
                }
                else if (UserSession.Current.IsCheif)
                {
                    var list = new List<int>() { 1, 3, 4, 5, 7, 8 };
                    for (int i = 0; i < this.radMenu.Items.Count; i++)
                    {
                        if (!list.Contains(i))
                        {
                            this.radMenu.Items[i].Visible = false;
                        }
                    }
                }

                this.IsGip.Value = UserSession.Current.IsGip ? "true" : "false";
            }
            else
            {
                idProject = 0;
            }
            //if (UserSession.Current.IsLeader || UserSession.Current.IsCheif)
            //{
            //    this.radPbList.Visible = false;

            //    var DownRaseRev = this.radMenu.Items.FindItemByValue("ImportCMDR");
            //    if (DownRaseRev != null)
            //    {
            //        DownRaseRev.Visible = false;
            //    }
            //    DownRaseRev = this.radMenu.Items.FindItemByValue("ImportCMDREN");
            //    if (DownRaseRev != null)
            //    {
            //        DownRaseRev.Visible = true;
            //    }
            //}

        }

        private void PermissionOnlyExport()
        {
            this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
            this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
            this.CustomerMenu.Items[0].Visible = false;
            this.CustomerMenu.Items[1].Visible = false;
            this.RadPane3.Visible = true;
        }

        private void PermissionOnlyView()
        {
            PermissionOnlyExport();
            this.CustomerMenu.Items[2].Visible = false;
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected List<WorkGroup> LoadDocuments()
        {
            var PJID = !string.IsNullOrEmpty(this.Request.QueryString["PJID"]) ? Convert.ToInt32(this.Request.QueryString["PJID"]) : 0;
            if (PJID != 0)
            {
                var pjcurrent = this.scopeProjectService.GetById(PJID);
                GridColumn rowStateColumn = grdDocument.MasterTableView.GetColumnSafe("ProjectName");
                rowStateColumn.CurrentFilterValue = pjcurrent.Name;
                idProject = PJID;
                lblProjectId.Value = PJID.ToString();
            }
            var ddlShow = (DropDownList)this.CustomerMenu.Items[4].FindControl("ddlShow");
            //user engineer see all
            List<int> roleList = ConfigurationManager.AppSettings.Get("GroupNotInVien").Split(',').Select(Int32.Parse).ToList();
            roleList.Remove(108);
            //var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsSuperViewer || UserSession.Current.IsEngineer && !roleList.Contains(UserSession.Current.RoleId);
            var currentDepartment = UserSession.Current.User.RoleId;
            var workPackageList = new List<WorkGroup>();
            List<int> userSpecial = ConfigurationManager.AppSettings.Get("ChiefandGIP").Split(',').Select(Int32.Parse).ToList();
            if (!roleList.Contains(UserSession.Current.RoleId))
            {
                workPackageList = this.workGroupService.GetAll();
                //user gip and chief
                if (userSpecial.Contains(UserSession.Current.User.Id) || (UserSession.Current.IsGip))
                {
                    var prjectInPermission =
                        this.scopeProjectService.GetAllByProjectManager(UserSession.Current.User.Id).Select(t => t.ID).ToList();
                    workPackageList = this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault(), prjectInPermission);
                }
                else if (UserSession.Current.IsEngineer || UserSession.Current.IsLeader || UserSession.Current.IsCheif)
                {
                    var projectDepartment = this.workGroupService.GetAllByDepartment(UserSession.Current.RoleId).Select(t => t.ProjectId.GetValueOrDefault()).Distinct().ToList();
                    workPackageList = workPackageList.Where(t => projectDepartment.Contains(t.ProjectId.GetValueOrDefault())).ToList();
                }

                if (ddlShow.SelectedValue == "ShowUncomplete")
                {
                    workPackageList = workPackageList.Where(t => t.Complete.GetValueOrDefault() < 100 && !t.IsStop).ToList();

                    // Special bussiness, user trongvv.rd need only see project of user Han(87)
                    if (UserSession.Current.User.Username == "trongvv.rd")
                    {
                        workPackageList = workPackageList.Where(t => t.ProjectManagerID == 87).ToList();
                    }
                }
                else if (ddlShow.SelectedValue == "ShowOverDue")
                {
                    workPackageList = workPackageList.Where(t => t.IsLater && !t.IsStop).ToList();
                }
                else if (ddlShow.SelectedValue == "ShowStop")
                {
                    workPackageList = workPackageList.Where(t => t.IsStop).ToList();
                }
                else
                {
                    workPackageList = workPackageList.OrderByDescending(t => t.CreatedDate).ToList();
                }
                if (idProject != 0)
                {
                    workPackageList = workPackageList.Where(t => t.ProjectId.GetValueOrDefault() == idProject).ToList();
                }
            }
            return workPackageList;
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// 

        private void ExportUncomplete()
        {
            try
            {
                var ddlShow = (DropDownList)this.CustomerMenu.Items[4].FindControl("ddlShow");
                if (ddlShow.SelectedValue == "ShowUncomplete")
                {
                    var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Exports") + @"\";
                    //var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\WorkpackageUncomplete.xls");

                    var dataSheet = workbook.Worksheets[0];
                    string filterValue = grdDocument.MasterTableView.GetColumn("DepartmentName").CurrentFilterValue;
                    var wpList = this.LoadDocuments().Where(t => !string.IsNullOrEmpty(t.ProjectName) && t.DepartmentName.ToLower() == filterValue.ToLower()).OrderBy(t => t.ProjectName).ToList();
                    int firstrow = 5;
                    int lastrow = firstrow;
                    for (int i = 0; i < wpList.Count; i++)
                    {
                        dataSheet.Cells.InsertRow(lastrow);
                        dataSheet.Cells["B" + lastrow].PutValue(wpList[i].Name);
                        dataSheet.Cells["C" + lastrow].PutValue(wpList[i].RevisionName);
                        dataSheet.Cells["D" + lastrow].PutValue(wpList[i].Description);
                        dataSheet.Cells["E" + lastrow].PutValue(wpList[i].DepartmentName);
                        dataSheet.Cells["F" + lastrow].PutValue(wpList[i].ProjectName);
                        dataSheet.Cells["G" + lastrow].PutValue(wpList[i].ProjectManagerFullName);
                        dataSheet.Cells["H" + lastrow].PutValue(wpList[i].AreaName);
                        dataSheet.Cells["I" + lastrow].PutValue(wpList[i].DisciplineName);
                        dataSheet.Cells["J" + lastrow].PutValue(wpList[i].TypeName);
                        dataSheet.Cells["K" + lastrow].PutValue(wpList[i].TotalDocWeight);
                        dataSheet.Cells["L" + lastrow].PutValue(wpList[i].Weight);
                        dataSheet.Cells["M" + lastrow].PutValue(wpList[i].Complete);
                        dataSheet.Cells["N" + lastrow].PutValue(wpList[i].UsedManHours);
                        dataSheet.Cells["O" + lastrow].PutValue(wpList[i].TotalManHours);
                        dataSheet.Cells["P" + lastrow].PutValue(wpList[i].StartDate);
                        dataSheet.Cells["Q" + lastrow].PutValue(wpList[i].Deadline);
                        dataSheet.Cells["R" + lastrow].PutValue(wpList[i].EndDate);
                        dataSheet.Cells["S" + lastrow].PutValue(wpList[i].IncomingNo);
                        dataSheet.Cells["T" + lastrow].PutValue(wpList[i].OutgoingNo);
                        if (wpList[i].IsAutoCalculate.GetValueOrDefault())
                        {
                            dataSheet.Cells["U" + lastrow].PutValue("Yes");
                        }
                        else
                        {
                            dataSheet.Cells["U" + lastrow].PutValue("No");
                        }
                        lastrow++;
                    }
                    dataSheet.Cells["B1"].PutValue("WORKPACKAGE UNCOMPLETE");
                    var filename = "WorkpackageUncomplete_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    workbook.Save(filePath + filename);
                    this.Download_File(filePath + filename);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void ExportOverdue()
        {
            try
            {
                var ddlShow = (DropDownList)this.CustomerMenu.Items[4].FindControl("ddlShow");
                if (ddlShow.SelectedValue == "ShowOverDue")
                {
                    var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Exports") + @"\";
                    //var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\WorkpackageUncomplete.xls");

                    var dataSheet = workbook.Worksheets[0];
                    string filterValue = grdDocument.MasterTableView.GetColumn("DepartmentName").CurrentFilterValue;
                    var wpList = this.LoadDocuments().Where(t => !string.IsNullOrEmpty(t.ProjectName) && t.DepartmentName.ToLower() == filterValue.ToLower()).OrderBy(t => t.ProjectName).ToList();
                    int firstrow = 5;
                    int lastrow = firstrow;
                    for (int i = 0; i < wpList.Count; i++)
                    {
                        dataSheet.Cells.InsertRow(lastrow);
                        dataSheet.Cells["B" + lastrow].PutValue(wpList[i].Name);
                        dataSheet.Cells["C" + lastrow].PutValue(wpList[i].RevisionName);
                        dataSheet.Cells["D" + lastrow].PutValue(wpList[i].Description);
                        dataSheet.Cells["E" + lastrow].PutValue(wpList[i].DepartmentName);
                        dataSheet.Cells["F" + lastrow].PutValue(wpList[i].ProjectName);
                        dataSheet.Cells["G" + lastrow].PutValue(wpList[i].ProjectManagerFullName);
                        dataSheet.Cells["H" + lastrow].PutValue(wpList[i].AreaName);
                        dataSheet.Cells["I" + lastrow].PutValue(wpList[i].DisciplineName);
                        dataSheet.Cells["J" + lastrow].PutValue(wpList[i].TypeName);
                        dataSheet.Cells["K" + lastrow].PutValue(wpList[i].TotalDocWeight);
                        dataSheet.Cells["L" + lastrow].PutValue(wpList[i].Weight);
                        dataSheet.Cells["M" + lastrow].PutValue(wpList[i].Complete);
                        dataSheet.Cells["N" + lastrow].PutValue(wpList[i].UsedManHours);
                        dataSheet.Cells["O" + lastrow].PutValue(wpList[i].TotalManHours);
                        dataSheet.Cells["P" + lastrow].PutValue(wpList[i].StartDate);
                        dataSheet.Cells["Q" + lastrow].PutValue(wpList[i].Deadline);
                        dataSheet.Cells["R" + lastrow].PutValue(wpList[i].EndDate);
                        dataSheet.Cells["S" + lastrow].PutValue(wpList[i].IncomingNo);
                        dataSheet.Cells["T" + lastrow].PutValue(wpList[i].OutgoingNo);
                        if (wpList[i].IsAutoCalculate.GetValueOrDefault())
                        {
                            dataSheet.Cells["U" + lastrow].PutValue("Yes");
                        }
                        else
                        {
                            dataSheet.Cells["U" + lastrow].PutValue("No");
                        }
                        lastrow++;
                    }
                    dataSheet.Cells["B1"].PutValue("WORKPACKAGE OVERDUE");
                    var filename = "WorkpackageOverdue_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    workbook.Save(filePath + filename);
                    this.Download_File(filePath + filename);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void Download_File(string FilePath)
        {
            Response.Clear();
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }

            else if (e.Argument == "ExportOverdue")
            { ExportOverdue(); }

            else if (e.Argument == "ExportUncomplete")
            { ExportUncomplete(); }

            else if (e.Argument == "ExportDocList")
            {
                var ddlShow = (DropDownList)this.CustomerMenu.Items[4].FindControl("ddlShow");
                var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC ||
                                UserSession.Current.IsSuperViewer;
                var currentDepartment = UserSession.Current.User.RoleId;
                var workPackageList = new List<WorkGroup>();

                if (isSeeFull)
                {
                    workPackageList = this.workGroupService.GetAll();
                }
                else if (UserSession.Current.IsGip)
                {
                    var prjectInPermission =
                        this.scopeProjectService.GetAll()
                            .Where(t => t.ProjectManagerId.GetValueOrDefault() == UserSession.Current.User.Id)
                            .Select(t => t.ID)
                            .ToList();

                    workPackageList =
                        this.workGroupService.GetAll()
                            .Where(t => prjectInPermission.Contains(t.ProjectId.GetValueOrDefault()))
                            .ToList();
                }
                else if (UserSession.Current.IsCheif)
                {
                    // Get all wp in department or have project in permission
                    var prjectInPermission =
                        this.scopeProjectService.GetAllByProjectManager(UserSession.Current.User.Id)
                            .Select(t => t.ID)
                            .ToList();
                    workPackageList = this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault(),
                        prjectInPermission);
                }

                workPackageList = ddlShow.SelectedValue == "ShowOverDue"
                        ? workPackageList.Where(t => t.IsLater).OrderBy(t => t.Name).ToList()
                        : workPackageList.Where(t => t.Complete == null || t.Complete < 100).OrderBy(t => t.Name).ToList();

                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\UpdateDocFromWP.xls");
                var sheets = workbook.Worksheets;

                var dtFull = new DataTable();
                dtFull.Columns.AddRange(new[]
                    {
                    new DataColumn("DocId", typeof (String)),
                    new DataColumn("NoIndex", typeof (String)),
                    new DataColumn("DocNo", typeof (String)),
                    new DataColumn("DocTitle", typeof (String)),
                    new DataColumn("Rev", typeof (String)),
                    new DataColumn("ManHour", typeof (String)),
                    new DataColumn("Start", typeof (String)),
                    new DataColumn("Deadline", typeof (String)),
                    new DataColumn("Fisnish", typeof (String)),
                    new DataColumn("IsoReviseDate", typeof (String)),
                    new DataColumn("Weight", typeof (Double)),
                    new DataColumn("Complete", typeof (Double)),
                    });

                var countWP = 0;
                foreach (var workGroup in workPackageList)
                {
                    var count = 1;
                    var docList = this.documentPackageService.GetAllByWorkgroup(workGroup.ID);
                    if (docList.Count > 0)
                    {
                        dtFull.Rows.Clear();
                        sheets.AddCopy(0);
                        sheets[countWP + 1].Name = Utility.RemoveSpecialCharacter(workGroup.Name, "-");
                        var projectObj = this.scopeProjectService.GetById(workGroup.ProjectId.GetValueOrDefault());
                        sheets[countWP + 1].Cells["B5"].PutValue(workGroup.ID);
                        sheets[countWP + 1].Cells["E1"].PutValue(workGroup.ProjectName +
                                                                 (projectObj != null &&
                                                                  !string.IsNullOrEmpty(projectObj.Description)
                                ? "\n(" + projectObj.Description + ")"
                                : string.Empty));
                        sheets[countWP + 1].Cells["E3"].PutValue(workGroup.Name +
                                (!string.IsNullOrEmpty(workGroup.Description)
                                ? "\n(" + workGroup.Description + ")"
                                : string.Empty));


                        foreach (var documentPackage in docList)
                        {
                            var dataRow = dtFull.NewRow();
                            dataRow["DocId"] = documentPackage.ID;
                            dataRow["NoIndex"] = count;
                            dataRow["DocNo"] = documentPackage.DocNo;
                            dataRow["DocTitle"] = documentPackage.DocTitle;
                            dataRow["Rev"] = documentPackage.RevisionName;
                            dataRow["ManHour"] = documentPackage.ManHours;
                            dataRow["Start"] = documentPackage.StartDate != null
                                ? documentPackage.StartDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Deadline"] = documentPackage.Deadline != null
                                ? documentPackage.Deadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Fisnish"] = documentPackage.EndDate != null
                                ? documentPackage.EndDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["IsoReviseDate"] = documentPackage.IsoReviseDate != null
                                ? documentPackage.IsoReviseDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Complete"] = documentPackage.Complete / 100;
                            dataRow["Weight"] = documentPackage.Weight / 100;

                            count += 1;
                            dtFull.Rows.Add(dataRow);
                        }

                        sheets[countWP + 1].Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, 12, true);
                        sheets[countWP + 1].AutoFitRows();
                        countWP += 1;
                    }
                }

                if (countWP > 0)
                {
                    sheets.RemoveAt(0);
                }

                var filename = "DocumentList " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, filename, true);
            }

            else if (e.Argument == "ExportMasterList")
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\WorkpackageMasterListTemplate.xls");
                var sheets = workbook.Worksheets;
                var readMeSheet = sheets[0];
                var dataSheet = sheets[1];

                var wpRevisionList = this.wpRevisionService.GetAll();
                var projectList = this.scopeProjectService.GetAll();
                var departmentList = this.roleService.GetAll(UserSession.Current.User.Id == 1);
                var areaList = this.areaService.GetAll();
                var disciplineList = this.disciplineService.GetAll();

                for (var i = 0; i < projectList.Count; i++)
                {
                    readMeSheet.Cells["B" + (7 + i)].PutValue(projectList[i].Name);
                    readMeSheet.Cells["C" + (7 + i)].PutValue(projectList[i].Description);
                }

                for (var i = 0; i < departmentList.Count; i++)
                {
                    readMeSheet.Cells["E" + (7 + i)].PutValue(departmentList[i].Name);
                    readMeSheet.Cells["F" + (7 + i)].PutValue(departmentList[i].Description);
                }

                for (var i = 0; i < areaList.Count; i++)
                {
                    readMeSheet.Cells["H" + (7 + i)].PutValue(areaList[i].Name);
                    readMeSheet.Cells["I" + (7 + i)].PutValue(areaList[i].Description);
                }

                for (var i = 0; i < disciplineList.Count; i++)
                {
                    readMeSheet.Cells["K" + (7 + i)].PutValue(disciplineList[i].Name);
                    readMeSheet.Cells["L" + (7 + i)].PutValue(disciplineList[i].Description);
                }

                for (var i = 0; i < wpRevisionList.Count; i++)
                {
                    readMeSheet.Cells["Q" + (7 + i)].PutValue(wpRevisionList[i].Name);
                    readMeSheet.Cells["R" + (7 + i)].PutValue(wpRevisionList[i].Description);
                }

                var rangeProjectList = readMeSheet.Cells.CreateRange("B7",
                    "B" + (7 + (projectList.Count == 0 ? 1 : projectList.Count)));
                rangeProjectList.Name = "ProjectList";

                var rangeDepartmentList = readMeSheet.Cells.CreateRange("E7",
                    "E" + (7 + (departmentList.Count == 0 ? 1 : departmentList.Count)));
                rangeDepartmentList.Name = "DepartmentList";

                var rangeAreaList = readMeSheet.Cells.CreateRange("H7",
                    "H" + (7 + (areaList.Count == 0 ? 1 : areaList.Count)));
                rangeAreaList.Name = "AreaList";

                var rangeDisciplineList = readMeSheet.Cells.CreateRange("K7",
                    "K" + (7 + (disciplineList.Count == 0 ? 1 : disciplineList.Count)));
                rangeDisciplineList.Name = "DisciplineList";

                var rangeTypeList = readMeSheet.Cells.CreateRange("N7", "N8");
                rangeTypeList.Name = "TypeList";

                var rangeWPRevisionList = readMeSheet.Cells.CreateRange("Q7",
                    "Q" + (7 + (wpRevisionList.Count == 0 ? 1 : wpRevisionList.Count)));
                rangeWPRevisionList.Name = "WPRevision";

                var validations = dataSheet.Validations;
                this.CreateValidation(rangeProjectList.Name, validations, 2, 1000, 1, 1);
                this.CreateValidation(rangeWPRevisionList.Name, validations, 2, 1000, 4, 4);

                this.CreateValidation(rangeDepartmentList.Name, validations, 2, 1000, 5, 5);
                this.CreateValidation(rangeAreaList.Name, validations, 2, 1000, 6, 6);
                this.CreateValidation(rangeDisciplineList.Name, validations, 2, 1000, 7, 7);
                this.CreateValidation(rangeTypeList.Name, validations, 2, 1000, 7, 8);

                readMeSheet.AutoFitRows();

                var filename = "WorkpackageInfoMasterList_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, filename, true);
            }
            else if (e.Argument.Contains("FTK02"))
            {
                var selectedWP =
                    this.workGroupService.GetById(!string.IsNullOrEmpty(this.lblWorkpackageId.Value)
                        ? Convert.ToInt32(this.lblWorkpackageId.Value)
                        : 0);
                if (selectedWP != null)
                {
                    var contentCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                    {
                        new DataColumn("Index", Type.GetType("System.String")),
                        new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                        new DataColumn("DeadLine", Type.GetType("System.String")),
                        new DataColumn("Note", Type.GetType("System.String"))
                    };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                    {
                        new DataColumn("DepartmentName", Type.GetType("System.String")),
                        new DataColumn("DepartmentDescription", Type.GetType("System.String")),
                        new DataColumn("ProjectDescription", Type.GetType("System.String")),
                        new DataColumn("ProjectName", Type.GetType("System.String")),
                        new DataColumn("WorkpackageName", Type.GetType("System.String")),
                        new DataColumn("WorkpackageDescription", Type.GetType("System.String")),
                        new DataColumn("WPStartDate", Type.GetType("System.String")),
                        new DataColumn("ProjectManager", Type.GetType("System.String")),
                        new DataColumn("InputDocument", Type.GetType("System.String")),
                        new DataColumn("InfoExchange", Type.GetType("System.String")),
                        new DataColumn("ReportMode", Type.GetType("System.String")),
                        new DataColumn("DateExport", Type.GetType("System.String")),
                        new DataColumn("PlanName", Type.GetType("System.String")),
                        new DataColumn("TypeOfWork", Type.GetType("System.String")),
                    };
                    reportInfo.Columns.AddRange(listColumn1);

                    var infoItem = reportInfo.NewRow();
                    var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                    var projectAppendixObj = this.projectAppendixService.GetByProject(selectedWP.ProjectId.GetValueOrDefault());
                    var projectManager =
                        this.userService.GetByID(projectObj != null
                            ? projectObj.ProjectManagerId.GetValueOrDefault()
                            : 0);
                    var deparment = this.roleService.GetByID(selectedWP.DepartmentId.GetValueOrDefault());

                    infoItem["DepartmentName"] = deparment != null
                        ? (e.Argument == "FTK02V" ? deparment.Name : deparment.RussiaName)
                        : string.Empty;
                    infoItem["DepartmentDescription"] = deparment != null
                        ? (e.Argument == "FTK02V" ? deparment.Description : string.Empty)
                        : string.Empty;
                    infoItem["ProjectDescription"] = projectObj != null ? projectObj.Description : string.Empty;
                    infoItem["ProjectName"] = projectObj != null ? projectObj.Name : string.Empty;
                    infoItem["WorkpackageName"] = selectedWP.Name;
                    infoItem["WorkpackageDescription"] = selectedWP.Description;
                    infoItem["WPStartDate"] = e.Argument == "FTK02V"
                            ? "Ngày " + DateTime.Now.Day + " Tháng " +
                              DateTime.Now.Month + " Năm " +
                              DateTime.Now.Year
                            : DateTime.Now.ToString("dd/MM/yyyy");

                    infoItem["ProjectManager"] = projectManager != null ? projectManager.FullName : string.Empty;
                    infoItem["InputDocument"] = selectedWP.InputDocument;
                    infoItem["InfoExchange"] = selectedWP.InformationExchange;
                    infoItem["ReportMode"] = selectedWP.ReportMode;
                    infoItem["DateExport"] = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                    ///infoItem["PlanNo"] = projectObj.PlanNo;
                    if (projectAppendixObj != null)
                    {
                        infoItem["PlanName"] = projectAppendixObj.OrderCode.Replace("<br/>", "\n");
                        infoItem["TypeOfWork"] = projectAppendixObj.Type.GetValueOrDefault() == 1 ? (e.Argument == "FTK02V" ? "trong kế hoạch" : "планового") : (e.Argument == "FTK02V" ? "ngoài kế hoạch" : "внепланового");
                    }
                    else
                    {
                        infoItem["PlanName"] = projectObj.planName;
                        infoItem["TypeOfWork"] = projectObj.ProjectTypeId.GetValueOrDefault() == 1 ? (e.Argument == "FTK02V" ? "trong kế hoạch" : "планового") : (e.Argument == "FTK02V" ? "ngoài kế hoạch" : "внепланового");
                    }

                    reportInfo.Rows.Add(infoItem);

                    // Add wpinfo row
                    //  var wpInfoRow = dataTable.NewRow();
                    //   wpInfoRow["WorkPackageContent"] = selectedWP.Description;
                    //  wpInfoRow["DeadLine"] = selectedWP.Deadline != null
                    //      ? selectedWP.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy")
                    //       : string.Empty;
                    //  dataTable.Rows.Add(wpInfoRow);
                    var content = this.workpackageContentService.GetAllByWorkpackage(selectedWP.ID).OrderBy(t => t.Ordinal).ToList();
                    if (content.Count > 0)
                    {
                        foreach (
                                var workpackageContent in content
                            )
                        {
                            var contentItem = dataTable.NewRow();
                            contentCount += 1;
                            contentItem["Index"] = contentCount;
                            contentItem["WorkPackageContent"] = workpackageContent.ContentInfo;
                            contentItem["DeadLine"] = workpackageContent.Deadline != null
                                ? workpackageContent.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy")
                                : string.Empty;
                            contentItem["Note"] = workpackageContent.Note;
                            dataTable.Rows.Add(contentItem);
                        }

                        // Add some empty info row
                        //for (int i = 0; i < 3; i++)
                        //{
                        //    var emptyRow = dataTable.NewRow();
                        //    dataTable.Rows.Add(emptyRow);
                        //}

                        ds.Tables.Add(dataTable);
                        ds.Tables[0].TableName = "Table";

                        var rootPath = Server.MapPath("../../Exports") + @"\";
                        const string WordPath = @"Template\";
                        const string WordPathExport = @"Generated\";
                        var StrTemplateFileName = e.Argument == "FTK02V"
                            ? "F-TK-02-V_Template.doc"
                            : "F-TK-02-R_Template.doc";
                        var strOutputFileName = "F-TK-02_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                        var isSuccess = OfficeCommon.ExportToWordWithRegion(
                            rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                        if (isSuccess)
                        {
                            this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                                "F-TK-02_" + Utility.RemoveSpecialCharacter(selectedWP.Name, "-") + ".doc", true);
                        }
                    }
                    else
                    {
                        var st = "Content is empty! Please enter contents of Workpackage.";
                        ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + st + "');</script>");
                    }
                }
            }
            else if (e.Argument == "FTKA")
            {
                //var selectedWP =
                //    this.workGroupService.GetById(!string.IsNullOrEmpty(this.lblWorkpackageId.Value)
                //        ? Convert.ToInt32(this.lblWorkpackageId.Value)
                //        : 0);
                List<int> list = new List<int>() { 29
,30
,13
,14
,15
,16
,17
,18
,19 };
                var projectList = this.projectAppendixService.GetByWork(list, 2019).Select(t => t.ProjectID.Value).ToList();
                var wplist = this.workGroupService.GetAllWorkGroupOfProject(projectList);
                foreach (var selectedWP in wplist)
                {

                    if (selectedWP != null)
                    {
                        var contentCount = 0;
                        var dataTable = new DataTable();
                        var reportInfo = new DataTable();

                        var ds = new DataSet();
                        var listColumn = new[]
                        {
                        new DataColumn("Index", Type.GetType("System.String")),
                        new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                        new DataColumn("DeadLine", Type.GetType("System.String")),
                        new DataColumn("Note", Type.GetType("System.String"))
                    };
                        dataTable.Columns.AddRange(listColumn);

                        var listColumn1 = new[]
                        {
                        new DataColumn("DepartmentName", Type.GetType("System.String")),
                        new DataColumn("DepartmentDescription", Type.GetType("System.String")),
                        new DataColumn("ProjectDescription", Type.GetType("System.String")),
                        new DataColumn("ProjectName", Type.GetType("System.String")),
                        new DataColumn("WorkpackageName", Type.GetType("System.String")),
                        new DataColumn("WorkpackageDescription", Type.GetType("System.String")),
                        new DataColumn("WPStartDate", Type.GetType("System.String")),
                        new DataColumn("ProjectManager", Type.GetType("System.String")),
                        new DataColumn("InputDocument", Type.GetType("System.String")),
                        new DataColumn("InfoExchange", Type.GetType("System.String")),
                        new DataColumn("ReportMode", Type.GetType("System.String")),
                        new DataColumn("DateExport", Type.GetType("System.String")),
                        new DataColumn("PlanName", Type.GetType("System.String")),
                        new DataColumn("TypeOfWork", Type.GetType("System.String")),
                    };
                        reportInfo.Columns.AddRange(listColumn1);

                        var infoItem = reportInfo.NewRow();
                        var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                        var projectAppendixObj = this.projectAppendixService.GetByProject(selectedWP.ProjectId.GetValueOrDefault());
                        var projectManager =
                            this.userService.GetByID(projectObj != null
                                ? projectObj.ProjectManagerId.GetValueOrDefault()
                                : 0);
                        var deparment = this.roleService.GetByID(selectedWP.DepartmentId.GetValueOrDefault());

                        infoItem["DepartmentName"] = deparment != null
                            ? deparment.RussiaName
                            : string.Empty;
                        infoItem["DepartmentDescription"] = deparment != null
                            ? string.Empty
                            : string.Empty;
                        infoItem["ProjectDescription"] = projectObj != null ? projectObj.Description : string.Empty;
                        infoItem["ProjectName"] = projectObj != null ? projectObj.Name : string.Empty;
                        infoItem["WorkpackageName"] = selectedWP.Name;
                        infoItem["WorkpackageDescription"] = selectedWP.Description;
                        infoItem["WPStartDate"] = e.Argument == "FTK02V"
                                ? "Ngày " + DateTime.Now.Day + " Tháng " +
                                  DateTime.Now.Month + " Năm " +
                                  DateTime.Now.Year
                                : DateTime.Now.ToString("dd/MM/yyyy");

                        infoItem["ProjectManager"] = projectManager != null ? projectManager.FullName : string.Empty;
                        infoItem["InputDocument"] = selectedWP.InputDocument;
                        infoItem["InfoExchange"] = selectedWP.InformationExchange;
                        infoItem["ReportMode"] = selectedWP.ReportMode;
                        infoItem["DateExport"] = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                        ///infoItem["PlanNo"] = projectObj.PlanNo;
                        if (projectAppendixObj != null)
                        {
                            infoItem["PlanName"] = projectAppendixObj.OrderCode.Replace("<br/>", "\n");
                            infoItem["TypeOfWork"] = projectAppendixObj.Type.GetValueOrDefault() == 1 ? (e.Argument == "FTK02V" ? "trong kế hoạch" : "планового") : (e.Argument == "FTK02V" ? "ngoài kế hoạch" : "внепланового");
                        }
                        else
                        {
                            infoItem["PlanName"] = projectObj.planName;
                            infoItem["TypeOfWork"] = projectObj.ProjectTypeId.GetValueOrDefault() == 1 ? (e.Argument == "FTK02V" ? "trong kế hoạch" : "планового") : (e.Argument == "FTK02V" ? "ngoài kế hoạch" : "внепланового");
                        }

                        reportInfo.Rows.Add(infoItem);

                        var content = this.workpackageContentService.GetAllByWorkpackage(selectedWP.ID).OrderBy(t => t.Ordinal).ToList();
                        if (content.Count > 0)
                        {
                            foreach (
                                    var workpackageContent in content
                                )
                            {
                                var contentItem = dataTable.NewRow();
                                contentCount += 1;
                                contentItem["Index"] = contentCount;
                                contentItem["WorkPackageContent"] = workpackageContent.ContentInfo;
                                contentItem["DeadLine"] = workpackageContent.Deadline != null
                                    ? workpackageContent.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy")
                                    : string.Empty;
                                contentItem["Note"] = workpackageContent.Note;
                                dataTable.Rows.Add(contentItem);
                            }
                            ds.Tables.Add(dataTable);
                            ds.Tables[0].TableName = "Table";

                            var rootPath = Server.MapPath("../../Exports") + @"\";
                            const string WordPath = @"Template\";
                            const string WordPathExport = @"Generated\";
                            var StrTemplateFileName = "F-TK-02-R_Template.doc";
                            var strOutputFileName = "F-TK-02_" + selectedWP.Name + ".doc";
                            var isSuccess = OfficeCommon.ExportToWordWithRegion(
                                rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                            if (isSuccess)
                            {
                                //this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                                //    "F-TK-02_" + Utility.RemoveSpecialCharacter(selectedWP.Name, "-") + ".doc", true);
                            }
                        }
                    }
                }
            }
            else if (e.Argument.Contains("ExportCMDR"))
            {
                var selectedWP =
                    this.workGroupService.GetById(!string.IsNullOrEmpty(this.lblWorkpackageId.Value)
                        ? Convert.ToInt32(this.lblWorkpackageId.Value)
                        : 0);

                if (selectedWP != null)
                {
                    var dtFull = new DataTable();
                    dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("Rev", typeof(String)),
                        new DataColumn("Start", typeof(String)),
                        new DataColumn("IDCDeadline", typeof(String)),
                        new DataColumn("IFRDeadline", typeof(String)),
                        new DataColumn("Deadline", typeof(String)),
                        new DataColumn("Fisnish", typeof(String)),
                        new DataColumn("Weight", typeof(Double)),
                        new DataColumn("Complete", typeof(Double)),
                        new DataColumn("DocType", typeof(String)),
                        new DataColumn("ManHourPlan", typeof(Double)),
                        new DataColumn("ManHour", typeof(Double)),
                        new DataColumn("Department", typeof(String)),
                        new DataColumn("Leader", typeof(String)),
                        new DataColumn("Engineer", typeof(String)),
                        new DataColumn("PersonsInvolved", typeof(String)),
                        new DataColumn("OutgoingLeterNo", typeof(String)),
                        new DataColumn("OutgoingLeterDate", typeof(String)),
                        new DataColumn("OutgoingNo", typeof(String)),
                        new DataColumn("OutgoingDate", typeof(String)),
                        new DataColumn("ICATrans",typeof(String)),
                        new DataColumn("ICADate",typeof(String)),
                        new DataColumn("ICACode",typeof(String)),
                        new DataColumn("Markup",typeof(String)),
                        new DataColumn("Note",typeof(String)),
                    });
                    var filePath = Server.MapPath("~/Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\CMDRReportTemplate_Report.xls");
                    var sheets = workbook.Worksheets;
                    var readMeSheet = sheets[2];
                    sheets[0].Cells["E1"].PutValue("DETAIL ENGINEERING SERVICE " + selectedWP.Name + " WORK PACKAGE");
                    sheets[0].Name = Utilities.Utility.RemoveAllSpecialCharacter(selectedWP.Name);
                    sheets[0].Cells["C7"].PutValue(selectedWP.Name);
                    sheets[0].Cells["B7"].PutValue(selectedWP.ProjectId + "," + selectedWP.ID);
                    sheets[0].Cells[3, 28].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                    var revisionList = this.revisionService.GetAll();
                    var documenttypeList = this.documentTypeService.GetAll();
                    var departmentlist = this.roleservice.GetAll(false);
                    var userlist = this.userService.GetAllByRoleId(selectedWP.DepartmentId.GetValueOrDefault());
                    var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                    if (projectObj.IsIDC.GetValueOrDefault() == true)
                    {
                        sheets[0].Cells.Columns[7].IsHidden = false;
                        sheets[0].Cells.Columns[8].IsHidden = false;
                    }
                    else
                    {
                        sheets[0].Cells.Columns[7].IsHidden = true;
                        sheets[0].Cells.Columns[8].IsHidden = true;
                    }
                    for (int i = 0; i < revisionList.Count; i++)
                    {
                        readMeSheet.Cells["C" + (7 + i)].PutValue(revisionList[i].Name);
                        readMeSheet.Cells["D" + (7 + i)].PutValue(revisionList[i].Description);
                    }
                    for (int i = 0; i < documenttypeList.Count; i++)
                    {
                        readMeSheet.Cells["E" + (7 + i)].PutValue(documenttypeList[i].Name);
                        readMeSheet.Cells["F" + (7 + i)].PutValue(documenttypeList[i].Description);
                    }
                    for (int i = 0; i < departmentlist.Count; i++)
                    {
                        readMeSheet.Cells["H" + (7 + i)].PutValue(departmentlist[i].Name);
                        readMeSheet.Cells["I" + (7 + i)].PutValue(departmentlist[i].Description);
                    }
                    var rangeRevisionList = readMeSheet.Cells.CreateRange("C7", "C" + (7 + (revisionList.Count == 0 ? 1 : revisionList.Count)));
                    rangeRevisionList.Name = "RevisionList";
                    var rangeDocTypeList = readMeSheet.Cells.CreateRange("E7", "E" + (7 + (documenttypeList.Count == 0 ? 1 : documenttypeList.Count)));
                    rangeDocTypeList.Name = "DocumentTypeList";
                    var rangDepartmentList = readMeSheet.Cells.CreateRange("H7", "H" + (7 + (departmentlist.Count == 0 ? 1 : departmentlist.Count)));
                    rangDepartmentList.Name = "DepartmentList";
                    var rangeUserList = readMeSheet.Cells.CreateRange(readMeSheet.Cells[0, 0].Name,
                          Regex.Replace(readMeSheet.Cells[0, 0].Name, @"\d+", string.Empty) + (userlist.Count == 0 ? 1 : userlist.Count));
                    rangeUserList.Name = "UserList";
                    for (int j = 0; j < userlist.Count; j++)
                    {
                        rangeUserList[j, 0].PutValue(userlist[j].UserNameWithFullName);
                    }
                    var validations = sheets[0].Validations;
                    this.CreateValidation(rangeRevisionList.Name, validations, 1, 1000, 5, 5);
                    this.CreateValidation(rangeUserList.Name, validations, 1, 1000, 17, 18);
                    this.CreateValidation(rangeUserList.Name, validations, 1, 1000, 18, 18);
                    this.CreateValidation(rangeDocTypeList.Name, validations, 1, 1000, 13, 13);
                    this.CreateValidation(rangDepartmentList.Name, validations, 1, 1000, 16, 16);
                    var count = 1;
                    var docList = this.documentPackageService.GetAllByWorkgroup(selectedWP.ID).OrderBy(t => t.DocNo).ToList();
                    var listDocumentTypeId = docList.Select(t => Convert.ToInt32(t.DocumentTypeId)).Distinct().OrderBy(t => t).ToList();
                    var complete = selectedWP.Complete;
                    var weight = selectedWP.Weight;
                    complete = 0;
                    weight = 0;
                    foreach (var documentTypeId in listDocumentTypeId)
                    {
                        var documentType = this.documentTypeService.GetById(documentTypeId);
                        var dataRow = dtFull.NewRow();
                        dataRow["DocId"] = -1;
                        dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                        dtFull.Rows.Add(dataRow);
                        var listDocByDocType = docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                        foreach (var documentPackage in listDocByDocType)
                        {
                            dataRow = dtFull.NewRow();
                            dataRow["DocId"] = documentPackage.ID;
                            dataRow["NoIndex"] = count;
                            dataRow["DocNo"] = documentPackage.DocNo;
                            dataRow["DocTitle"] = documentPackage.DocTitle;
                            dataRow["Rev"] = documentPackage.RevisionName;
                            dataRow["Start"] = documentPackage.StartDate != null
                                ? documentPackage.StartDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["IDCDeadline"] = documentPackage.IDCDeadline != null
                                ? documentPackage.IDCDeadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["IFRDeadline"] = documentPackage.IFRDeadline != null
                                ? documentPackage.IFRDeadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Deadline"] = documentPackage.Deadline != null
                                ? documentPackage.Deadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Fisnish"] = documentPackage.EndDate != null
                                ? documentPackage.EndDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Weight"] = documentPackage.Weight != null ? documentPackage.Weight / 100 : 0;
                            dataRow["Complete"] = documentPackage.Complete != null ? documentPackage.Complete / 100 : 0;
                            dataRow["DocType"] = documentPackage.DocumentTypeName;
                            dataRow["ManHourPlan"] = documentPackage.ManHourPlan != null ? documentPackage.ManHourPlan.GetValueOrDefault() : 0;
                            dataRow["ManHour"] = documentPackage.ManHours != null ? documentPackage.ManHours.GetValueOrDefault() : 0;
                            dataRow["Department"] = documentPackage.DeparmentName;
                            dataRow["Leader"] = documentPackage.LeaderName;
                            dataRow["Engineer"] = documentPackage.EngineerName;
                            dataRow["PersonsInvolved"] = documentPackage.PersonsInvolved;
                            dataRow["OutgoingLeterNo"] = documentPackage.OutgoingLeterNo;
                            dataRow["OutgoingLeterDate"] = documentPackage.OutgoingLeterDate != null
                                ? documentPackage.OutgoingLeterDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["OutgoingNo"] = documentPackage.OutgoingTransNo;
                            dataRow["OutgoingDate"] = documentPackage.OutgoingTransDate != null
                                ? documentPackage.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["ICATrans"] = documentPackage.ICAReviewOutTransNo;
                            dataRow["ICADate"] = documentPackage.ICAReviewOutDate != null
                                ? documentPackage.ICAReviewOutDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["ICACode"] = documentPackage.ICAReviewCode;
                            dataRow["Markup"] = documentPackage.Markup;
                            dataRow["Note"] = documentPackage.Notes;

                            count += 1;
                            dtFull.Rows.Add(dataRow);
                        }
                    }
                    if (count > 1)
                    {
                        sheets[0].Cells["A7"].PutValue(dtFull.Rows.Count);
                        sheets[0].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, 28, true);
                        sheets[1].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, 28, true);
                        sheets[0].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");
                        sheets[0].Cells[7 + dtFull.Rows.Count, 10].Formula = "=SUMPRODUCT(J9:J" + (7 + dtFull.Rows.Count) + ",K9:K" + (7 + dtFull.Rows.Count) + ")";
                        sheets[0].Cells[7 + dtFull.Rows.Count, 9].Formula = "=SUM(J9:J" + (7 + dtFull.Rows.Count) + ")";
                        sheets[0].Name = "CMDR Report";
                        sheets[0].AutoFitRows();
                        sheets[1].IsVisible = false;
                        sheets[2].IsVisible = false;
                        var filename = selectedWP.ProjectName + " - " + selectedWP.Name + " EMDR Report " +
                                              DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                        filename = filename.Replace(@"\", " ").Replace("/", " ");
                        workbook.Save(filePath + filename);
                        this.DownloadByWriteByte(filePath + filename, filename, true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "<script>alert(' Workpackage not have document.');</script>",
                                      false);
                    }
                }
            }
            else if (e.Argument.Contains("CreateFolder"))
            {
                var WPObj = this.workGroupService.GetById(Convert.ToInt32(this.lblWorkpackageId.Value));
                var folderparent = this.folderService.GetAllByName(WPObj.ProjectName);
                if (folderparent != null)
                {
                    var folderwp = creterfolder(WPObj.Name, folderparent, WPObj);
                    //PDF
                    var pdf = creterfolder("PDF", folderwp, WPObj);
                    //Native
                    var native = creterfolder("Native", folderwp, WPObj);
                }
            }
            else if (e.Argument.Contains("GipChiefNative"))
            {
                var folderNative = this.folderService.GetByContainDirName("Native");
                var user = this.userService.GetByID(99);
                foreach (var folder in folderNative)
                {
                    var exist = this.userDataPermissionService.GetByUserAndFolder(user.Id, folder.ID);
                    if (exist == null)
                    {
                        var newPermission = new UserDataPermission();

                        newPermission.RoleId = user.RoleId;
                        newPermission.FolderId = folder.ID;
                        newPermission.UserId = user.Id;
                        newPermission.IsFullPermission = false;
                        newPermission.CreatedDate = DateTime.Now;
                        newPermission.CreatedBy = UserSession.Current.User.Id;
                        newPermission.OnlyView = false;
                        this.userDataPermissionService.Insert(newPermission);
                    }
                    System.Threading.Thread.Sleep(100);
                }
            }
            else if (e.Argument.Contains("recalculatorProject"))
            {
                DateTime d = new DateTime(2018, 1, 1);
                var projectList = this.scopeProjectService.GetAll().Where(t => t.Complete < 100).Select(t => Convert.ToInt32(t.ID)).ToList();
                var wpList = this.workGroupService.GetAll().Where(t => t.CreatedDate > d && projectList.Contains(t.ProjectId.GetValueOrDefault())).ToList();
                foreach (var item in wpList)
                {
                    var wpObj = item;
                    var docList = this.documentPackageService.GetAllEMDRByWorkgroup(wpObj.ID);
                    var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                    double complete = 0;
                    Milestone milestoneObj = new Milestone();
                    ProcessActual processActualObj = new ProcessActual();
                    List<WorkgroupManhourMonth> wmmList = new List<WorkgroupManhourMonth>();
                    WorkgroupManhourMonth wmmObj = new WorkgroupManhourMonth();

                    //update weight cho document
                    UpdateWeightDocument(projectObj, ref docList);
                    var wpUpdateList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                    //update data workpackage
                    UpdateDataWorkpackage(projectObj, docList, ref wpObj, ref wpUpdateList, ref complete);
                    if (wpObj.IsAutoCalculate.GetValueOrDefault())
                    {
                        if ((complete < 100 && docList.Where(t => t.Complete != 100).Any()))
                        {
                            //update milestone
                            UpdateMilestonActual(wpObj, UserSession.Current.User, milestoneObj);
                        }
                    }
                    //Update actual progress
                    UpdateProgressActual(wpObj, processActualObj);

                    // update data Project
                    UpdateDataProject(wpUpdateList, projectObj);

                    //Update all wpWeight trong thang workgroup manhour month cua project
                    UpdateWeightWorkgroupManhourMonth(wpObj, wmmList);

                    UpdateDataWorkgroupManhourMonth(wpObj, UserSession.Current.User, wpUpdateList, wmmObj);
                }
            }
            else if (e.Argument.Contains("recalculatorMilestone"))
            {
                DateTime d = new DateTime(2019, 6, 1);
                var milestoneList = this.milestoneService.GetAll().Where(t => t.MilestoneDate >= d).ToList();
                //milestoneList = milestoneList.Where(t => t.WorkpackageId == 1857).ToList();
                foreach (var item in milestoneList.Distinct())
                {
                    var wpObj = this.workGroupService.GetById(item.WorkpackageId.GetValueOrDefault());
                    if (wpObj != null)
                    {
                        var lastedMilestone = this.milestoneService.GetByLasted(wpObj.ID);
                        var list = this.milestoneService.GetAllByWorkpackage(wpObj.ID).Where(t => t.ID != lastedMilestone.ID).OrderByDescending(t => t.MilestoneDate).ToList();
                        if (list.Count > 0)
                        {
                            if (lastedMilestone.RealTotal != wpObj.Complete)
                            {
                                lastedMilestone.RealTotal = wpObj.Complete;
                                lastedMilestone.Real = lastedMilestone.RealTotal - (list[0].RealTotal != null ? list[0].RealTotal : 0);
                                this.milestoneService.Update(lastedMilestone);
                            }
                        }
                        else
                        {
                            lastedMilestone.RealTotal = wpObj.Complete;
                            lastedMilestone.Real = lastedMilestone.RealTotal;
                            this.milestoneService.Update(lastedMilestone);
                        }
                    }
                }
            }
        }

        private Folder creterfolder(string name, Folder folder, WorkGroup wp)
        {

            var trans = new Folder()
            {
                Name = name,
                Description = name,
                ParentID = folder.ID,
                DirName = folder.DirName + "/" + Regex.Replace(name, @"[^0-9a-zA-Z]+", string.Empty),
                ProjectId = wp.ProjectId,
                WorkgroupID = wp.ID,
                CreatedBy = UserSession.Current.User.Id,
                CreatedDate = DateTime.Now
            };
            
            int? folderId;
            var folderTemp = this.folderService.GetByDirName(trans.DirName);
            if (folderTemp == null)
            {
                folderId = this.folderService.Insert(trans);
                Directory.CreateDirectory(Server.MapPath(trans.DirName));
            }
            else
            {
                folderId = folderTemp.ID;
            }

            var AllUser = this.userService.GetAll(false).Where(t => !t.IsDC.GetValueOrDefault());
            var obj = this.scopeProjectService.GetById(wp.ProjectId.GetValueOrDefault());

            var addingPermission = AllUser.Where(t => t.RoleId == wp.DepartmentId.GetValueOrDefault() || t.Id == obj.ProjectManagerId || t.Id == obj.SupervisorId).Select(t => new UserDataPermission()
            {
                ProjectId = wp.ProjectId,
                RoleId = t.RoleId,
                FolderId = folderId,
                UserId = t.Id,
                IsFullPermission = true,
                CreatedDate = DateTime.Now,
                CreatedBy = UserSession.Current.User.Id,
                OnlyView = false
            });
            this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            if (name != "Native")
            {
                var listgroup = this.roleservice.GetAllKhoiThietKe().Select(t => t.Id).ToList();
                var listuserview = AllUser.Where(t => listgroup.Contains(t.RoleId.GetValueOrDefault()));

                addingPermission = listuserview.Where(t => t.RoleId != wp.DepartmentId.GetValueOrDefault() && t.Id != obj.ProjectManagerId && t.Id != obj.SupervisorId).Select(t => new UserDataPermission()
                {
                    ProjectId = wp.ProjectId,
                    RoleId = t.RoleId,
                    FolderId = folderId,
                    UserId = t.Id,
                    IsFullPermission = false,
                    CreatedDate = DateTime.Now,
                    CreatedBy = UserSession.Current.User.Id,
                    OnlyView = true
                });
                this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            }
            else if (name == "Native")
            {
                var listGipAndChief = AllUser.Where(t => t.RoleId != wp.DepartmentId.GetValueOrDefault() && (t.IsGip.GetValueOrDefault() || t.IsChief.GetValueOrDefault()));
                addingPermission = listGipAndChief.Select(t => new UserDataPermission()
                {
                    ProjectId = wp.ProjectId,
                    RoleId = t.RoleId,
                    FolderId = folderId,
                    UserId = t.Id,
                    IsFullPermission = false,
                    CreatedDate = DateTime.Now,
                    CreatedBy = UserSession.Current.User.Id,
                    OnlyView = false
                });
                this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
            }
            return trans;
        }

        /// <summary>
        /// Conver List<t> to datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public DataTable ToDataTables<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            // PropertyInfo and BindingFlags using library System.Reflection;
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    if (i == 10 || i == 11 || i == 12)
                    {
                        if (Props[i].GetValue(item, null) != null)
                        {
                            try
                            {
                                DateTime d = DateTime.Parse(Props[i].GetValue(item, null).ToString());

                                values[i] = d.ToString("dd/MM/yyyy");
                            }
                            catch { }
                        }
                        else
                        {
                            values[i] = Props[i].GetValue(item, null);
                        }
                    }
                    else
                    {
                        values[i] = Props[i].GetValue(item, null);
                    }

                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }


        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            this.grdDocument.DataSource = this.LoadDocuments().Where(t => !string.IsNullOrEmpty(t.ProjectName)).OrderBy(t => t.Name);
        }

        /// <summary>
        /// Grid KhacHang item created
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem)
            {
                var item = e.Item as GridEditableItem;
                var btnDelete = item["DeleteColumn"].Controls[0] as ImageButton;
                if (btnDelete != null)
                {
                    if (item["CanDelete"].Text.ToLower() == "true")
                    {
                        btnDelete.ImageUrl = "~/Images/delete.png";
                        btnDelete.Enabled = true;
                    }
                    else
                    {
                        btnDelete.ImageUrl = "~/Images/deleteDisable.png";
                        //DC co xoa uu tien
                        if (UserSession.Current.IsDC)
                        {
                            //btnDelete.ImageUrl = "~/Images/delete.png";
                            btnDelete.Enabled = true;
                        }
                        else
                        {
                            btnDelete.Enabled = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var wpId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var wpObj = this.workGroupService.GetById(wpId);
            var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());

            //Xoa het trong workgroup manhour month
            var wmmList = this.workgroupManhourMonthService.GetByWPAndPJ(wpId, projectObj.ID);
            foreach (var wmmItem in wmmList)
            {
                this.workgroupManhourMonthService.Delete(wmmItem);
            }
            //DC co xoa uu tien
            if (UserSession.Current.IsDC)
            {
                var docList = this.documentPackageService.GetAllByWorkgroup(wpId).ToList();
                foreach (var docObj in docList)
                {
                    var attachDocList = this.attachFileService.GetAllDocId(docObj.ID).ToList();
                    foreach (var attachDocObj in attachDocList)
                    {
                        this.attachFileService.Delete(attachDocObj);
                    }
                    this.documentPackageService.Delete(docObj);
                }
                this.workGroupService.Delete(wpId);
            }
            else
            {
                this.workGroupService.Delete(wpId);
            }

            // Update total workpackage weight for project after delete wp.
            if (projectObj != null)
            {
                projectObj.TotalWorkpackageWeight -= wpObj.Weight;

                var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                if (wpList.Count == 0)
                {
                    projectObj.CanDelete = true;
                }
                this.scopeProjectService.Update(projectObj);
                if (projectObj.AutoCalculateWeightWorkGroup == true)
                {
                    var sumTotalManHours = 0.0;
                    sumTotalManHours = wpList.Aggregate(sumTotalManHours, (current, t) => current + t.TotalManHours.GetValueOrDefault());
                    foreach (var it in wpList)
                    {
                        var calWeight = ((it.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) > 0 ? ((it.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) : 0;
                        it.Weight = calWeight > 100 ? 100 : calWeight;
                        this.workGroupService.Update(it);
                    }
                }
            }

            // Send notification
            if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false" && UserSession.Current.User.Role.Notify.GetValueOrDefault())
            {
                this.NotificationDeleteWp(wpObj);
            }
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }


        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var ddlShow = (DropDownList)this.CustomerMenu.Items[4].FindControl("ddlShow");
                var item = e.Item as GridDataItem;
                if (ddlShow.SelectedValue != "ShowOverDue")
                {
                    if (item["IsLater"].Text.ToLower() == "true")
                    {
                        if (ddlShow.SelectedValue != "ShowStop")
                        {
                            item.BackColor = Color.Crimson;
                            item.BorderColor = Color.Crimson;
                            //item.ForeColor = Color.White;
                        }
                    }
                }
                if (ddlShow.SelectedValue != "ShowStop")
                {
                    if (item["IsStop"].Text.ToLower() == "true")
                    {
                        item.BackColor = Color.Orange;
                        item.BorderColor = Color.Orange;
                        //item.ForeColor = Color.White;
                    }
                }
                Label lblWeight = (Label)item["Weight"].FindControl("lblWeight");
                if (lblWeight.Text.Contains("%"))
                {
                    double weight = Convert.ToDouble(lblWeight.Text.Replace("%", string.Empty));
                    weight = Math.Round(weight, 2);
                    lblWeight.Text = weight.ToString() + "%";
                }
                Label lblComplete = (Label)item["Complete"].FindControl("lblComplete");
                if (lblComplete.Text.Contains("%"))
                {
                    double complete = Convert.ToDouble(lblComplete.Text.Replace("%", string.Empty));
                    complete = Math.Round(complete, 2);
                    lblComplete.Text = complete.ToString() + "%";
                }
            }
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"~/Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    //if (item.Text == "Originator")
                    //{
                    //    item.Selected = true;
                    //}
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = Aspose.Cells.ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;
            validation.ShowInput = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";
            validation.InputTitle = "Warning";

            // Set the error message.
            validation.ErrorMessage = "Please select item from the list";
            validation.InputMessage = "Please select item from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                var fs = new FileStream(strFileName, FileMode.Open);
                var streamLength = Convert.ToInt32(fs.Length);
                var data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        protected void ddlShow_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        private void NotificationDeleteWp(WorkGroup wpObj)
        {
            if (wpObj.DepartmentId != 0)
            {
                var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                var gipUser = this.userService.GetByID(projectObj != null ? projectObj.ProjectManagerId.GetValueOrDefault() : 0);
                var emailList = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault())
                    .Where(t => t.IsChief.GetValueOrDefault())
                    .Select(t => t.Email)
                    .Where(t => !string.IsNullOrEmpty(t)).ToList();
                var leader = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault())
                                          .Where(t => t.IsLeader.GetValueOrDefault() && wpObj.DisciplineId == t.DisciplineID)
                                          .Select(t => t.Email)
                                          .Where(t => !string.IsNullOrEmpty(t)).ToList();
                if (gipUser != null && !string.IsNullOrEmpty(gipUser.Email))
                {
                    emailList.Add(gipUser.Email);
                }

                var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.DeleteWP);
                if (emailList.Count > 0 && notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                {
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };

                    var wpDeletedUser = this.userService.GetByID(UserSession.Current.User.Id);


                    var subject = notificationTemplate.Subject.Replace("#WPName#", wpObj.Name)
                        .Replace("#WPDeletedUser#", wpDeletedUser != null ? wpDeletedUser.FullName : string.Empty);

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    message.Body = notificationTemplate.Contents
                        .Replace("#WPNumber#", wpObj.Name)
                        .Replace("#WPDeletedUser#", wpDeletedUser != null ? wpDeletedUser.FullName : string.Empty)
                        .Replace("#WPProjectNumber#", projectObj != null ? projectObj.Name : string.Empty)
                        .Replace("#WPProjectName#", projectObj != null ? projectObj.Description : string.Empty)
                        .Replace("#WPName#", wpObj.Description)
                        .Replace("#WPStartDate#", wpObj.StartDate != null ? wpObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPDeadline#", wpObj.Deadline != null ? wpObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPFinishDate#", wpObj.EndDate != null ? wpObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPIncomingNo#", wpObj.IncomingNo)
                        .Replace("#WPOutgoingNo#", wpObj.OutgoingNo)
                        .Replace("#WPWeight#", wpObj.Weight != null ? wpObj.Weight.Value.ToString() : string.Empty)
                        .Replace("#WPComplete#", wpObj.Complete != null ? wpObj.Complete.Value.ToString() : string.Empty);


                    foreach (var to in emailList)
                    {
                        message.To.Add(new MailAddress(to));
                    }

                    foreach (var to in leader)
                    {
                        message.CC.Add(new MailAddress(to));
                    }


                    smtpClient.Send(message);
                }
            }
        }

        #region UpdateData
        private void UpdateWeightDocument(ScopeProject projectObj, ref List<DocumentPackage> docList)
        {
            try
            {
                var totalPlanManhour = 0.0;
                totalPlanManhour = docList.Aggregate(totalPlanManhour, (current, t) => current + t.ManHourPlan.GetValueOrDefault());

                foreach (var doc in docList)
                {
                    doc.Weight = (doc.ManHourPlan.GetValueOrDefault() / totalPlanManhour) * 100;
                    this.documentPackageService.Update(doc);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkpackage(ScopeProject projectObj, List<DocumentPackage> docList, ref WorkGroup wpObj, ref List<WorkGroup> wpList, ref double complete)
        {
            try
            {
                if (docList.Count > 0)
                {
                    //update complete workpackage
                    complete = 0.0;
                    complete = docList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                    complete = Math.Round(complete, 5);
                    if (complete == 100 || !docList.Where(t => t.Complete < 100).Any())
                    {
                        wpObj.EndDate = DateTime.Now;
                        wpObj.Complete = 100;
                    }
                    else
                    {
                        wpObj.Complete = complete;
                    }
                }

                // Update total document weight for workpackage
                double totalWeight = 0.0;
                totalWeight = docList.Sum(t => t.Weight.GetValueOrDefault());
                totalWeight = Math.Round(totalWeight, 2);
                totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                wpObj.TotalDocWeight = totalWeight;

                // Update total man hours plan & actual for workpackage
                double totalManhourPlan = 0.0;
                totalManhourPlan = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                wpObj.TotalManHours = totalManhourPlan;

                double totalManHourActual = 0.0;
                totalManHourActual = docList.Sum(t => t.ManHours.GetValueOrDefault());
                wpObj.UsedManHours = totalManHourActual;

                // Update can delete for workpackage
                wpObj.CanDelete = !docList.Any();
                this.workGroupService.Update(wpObj);

                //update weight all WP of PJ
                if (projectObj.AutoCalculateWeightWorkGroup == true)
                {
                    var sumTotalManHours = 0.0;
                    sumTotalManHours = wpList.Sum(t => t.TotalManHours.GetValueOrDefault());
                    foreach (var items in wpList)
                    {
                        var calWeight = ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) > 0 ? ((items.TotalManHours.GetValueOrDefault() / sumTotalManHours) * 100) : 0;
                        items.Weight = calWeight >= 100 ? 100 : calWeight;
                        this.workGroupService.Update(items);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateMilestonActual(WorkGroup wpObj, User userObj, Milestone milestoneObj)
        {
            try
            {
                var milestoneList = this.milestoneService.GetAllByWorkpackage(wpObj.ID).OrderByDescending(t => t.MilestoneDate).ToList();
                if (milestoneList.Count > 0)
                {
                    var presentMilestoneObj = milestoneList.FirstOrDefault(t => t.MilestoneDate != null
                                                                         && t.MilestoneDate.Value.Month == DateTime.Now.Month
                                                                         && t.MilestoneDate.Value.Year == DateTime.Now.Year);
                    if (presentMilestoneObj != null)
                    {
                        milestoneObj = presentMilestoneObj;
                        var prevIndex = milestoneList.IndexOf(milestoneObj) + 1;
                        var prevRealTotal = 0.0;
                        var prevPlanTotal = 0.0;
                        if (prevIndex < (milestoneList.Count - 1))
                        {
                            var prevMilestoneObj = milestoneList[prevIndex];
                            if (prevMilestoneObj != null)
                            {
                                prevRealTotal = prevMilestoneObj.RealTotal.GetValueOrDefault();
                                prevPlanTotal = prevMilestoneObj.PlanTotal.GetValueOrDefault();
                            }
                            else
                            {
                                prevRealTotal = 0;
                                prevPlanTotal = 0;
                            }
                        }
                        else
                        {
                            prevRealTotal = 0;
                            prevPlanTotal = 0;
                        }

                        // calculator real
                        double real = Math.Round(wpObj.Complete.GetValueOrDefault() - prevRealTotal, 2);
                        milestoneObj.Real = real;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PlanTotal = (prevPlanTotal + milestoneObj.PlanPercent) > 100 ? 100 : (prevPlanTotal + milestoneObj.PlanPercent);
                        milestoneObj.UpdatedBy = userObj.Id;
                        milestoneObj.UpdatedDate = DateTime.Now;
                        this.milestoneService.Update(milestoneObj);
                    }
                    else
                    {
                        var lastedMilestoneObj = milestoneList[0];
                        milestoneObj = new Milestone();
                        milestoneObj.MilestoneDate = DateTime.Now;
                        milestoneObj.PlanPercent = 0;
                        milestoneObj.PlanTotal = lastedMilestoneObj.PlanTotal;
                        double preReal = Math.Round(wpObj.Complete.GetValueOrDefault() - lastedMilestoneObj.RealTotal.GetValueOrDefault(), 2);
                        milestoneObj.Real = preReal;
                        milestoneObj.RealTotal = wpObj.Complete;
                        milestoneObj.PerformingUser = lastedMilestoneObj.PerformingUser;
                        milestoneObj.Note = lastedMilestoneObj.Note;
                        milestoneObj.WorkpackageId = wpObj.ID;
                        milestoneObj.WorkpackageName = wpObj.Name;
                        if (DateTime.Now.Year > lastedMilestoneObj.MilestoneDate.Value.Year && (DateTime.Now.Year - lastedMilestoneObj.MilestoneDate.Value.Year) > 0)
                        {
                            milestoneObj.PlanOfYear = 0;
                        }
                        else
                        {
                            milestoneObj.PlanOfYear = lastedMilestoneObj.PlanOfYear;
                        }
                        milestoneObj.CreatedBy = userObj.Id;
                        milestoneObj.CreatedDate = DateTime.Now;
                        this.milestoneService.Insert(milestoneObj);
                    }
                }
                else
                {
                    //milestoneObj = new Milestone();
                    milestoneObj.MilestoneDate = DateTime.Now;
                    milestoneObj.PlanPercent = 0;
                    milestoneObj.PlanTotal = 0;
                    milestoneObj.RealTotal = wpObj.Complete;
                    milestoneObj.Real = wpObj.Complete;
                    milestoneObj.PerformingUser = string.Empty;
                    milestoneObj.Note = string.Empty;
                    milestoneObj.WorkpackageId = wpObj.ID;
                    milestoneObj.WorkpackageName = wpObj.Name;
                    milestoneObj.PlanOfYear = 0;
                    milestoneObj.CreatedBy = userObj.Id;
                    milestoneObj.CreatedDate = DateTime.Now;
                    this.milestoneService.Insert(milestoneObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateProgressActual(WorkGroup wpObj, ProcessActual processActualObj)
        {
            try
            {
                var projectId = wpObj.ProjectId.GetValueOrDefault();
                var projectObj = this.scopeProjectService.GetById(projectId);
                var tempProcessActualObj = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, wpObj.ID);
                if (tempProcessActualObj != null)
                {
                    processActualObj = tempProcessActualObj;
                    if (projectObj != null && projectObj.StartDate != null && projectObj.Deadline != null)
                    {
                        if (string.IsNullOrEmpty(processActualObj.Actual))
                        {
                            processActualObj.Actual = "0";
                        }
                        if (string.IsNullOrEmpty(processActualObj.ActualMonth))
                        {
                            processActualObj.ActualMonth = "0";
                        }
                        var progressActualWeekList = processActualObj.Actual.Split('$').ToList();
                        var progressActualMonthList = processActualObj.ActualMonth.Split('$').ToList();
                        var countWeek = 0;
                        var countMonth = 0;
                        for (var j = GetFridayOfWeek(projectObj.StartDate.GetValueOrDefault());
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualWeekList.Count() > countWeek)
                            {
                                if (DateTime.Now > j.AddDays(7))
                                {
                                    countWeek += 1;
                                }
                            }
                        }
                        var currentMonth = 0;
                        for (var j = projectObj.StartDate.GetValueOrDefault();
                            j < projectObj.Deadline.GetValueOrDefault();
                            j = j.AddDays(7))
                        {
                            if (progressActualMonthList.Count() > countMonth)
                            {
                                if (DateTime.Now.Month > j.AddDays(7).Month && DateTime.Now > j.AddDays(7) && currentMonth != j.AddDays(7).Month)
                                {
                                    currentMonth = j.Month;
                                    countMonth++;
                                }
                            }
                        }
                        if (progressActualWeekList.Count() >= countWeek && progressActualWeekList.Count() > 0 && countWeek > 0)
                        {
                            progressActualWeekList = progressActualWeekList.Take(countWeek).ToList();
                            progressActualWeekList[countWeek - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.Actual = string.Join("$", progressActualWeekList);
                            this.processActualService.Update(processActualObj);
                        }
                        if (progressActualMonthList.Count() >= countMonth && progressActualMonthList.Count() > 0 && countMonth > 0)
                        {
                            progressActualMonthList = progressActualMonthList.Take(countMonth).ToList();
                            progressActualMonthList[countMonth - 1] = Math.Round(wpObj.Complete.GetValueOrDefault(), 2).ToString();
                            processActualObj.ActualMonth = string.Join("$", progressActualMonthList);
                            this.processActualService.Update(processActualObj);
                        }
                    }
                }
                else
                {
                    processActualObj = new ProcessActual();
                    processActualObj.ProjectId = wpObj.ProjectId;
                    processActualObj.WorkgroupId = wpObj.ID;
                    processActualObj.Actual = wpObj.Complete.ToString();
                    processActualObj.ActualMonth = wpObj.Complete.ToString();
                    this.processActualService.Insert(processActualObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private DateTime GetMondayOfWeek(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        private DateTime GetFridayOfWeek(DateTime date)
        {
            return GetMondayOfWeek(date).AddDays(4);
        }

        private void UpdateDataProject(List<WorkGroup> wpList, ScopeProject projectObj)
        {
            try
            {
                if (projectObj != null)
                {
                    projectObj.CanDelete = false;
                    if (projectObj.IsAutoCalculate.GetValueOrDefault())
                    {
                        double complete = 0.0;
                        complete = wpList.Sum(t => (t.Weight.GetValueOrDefault() * t.Complete.GetValueOrDefault()) / 100);
                        projectObj.Complete = Math.Round(complete, 5);
                        double totalWeight = 0.0;
                        totalWeight = wpList.Sum(t => t.Weight.GetValueOrDefault());
                        totalWeight = totalWeight >= 100 ? 100 : totalWeight;
                        projectObj.TotalWorkpackageWeight = Math.Round(totalWeight, 2);
                    }
                    this.scopeProjectService.Update(projectObj);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateWeightWorkgroupManhourMonth(WorkGroup wpObj, List<WorkgroupManhourMonth> wmmList)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                wmmList = this.workgroupManhourMonthService.GetByPJ(wpObj.ProjectId.GetValueOrDefault(), month, year);
                foreach (var wmmItem in wmmList)
                {
                    var wpItem = this.workGroupService.GetById(wmmItem.WorkgroupID.GetValueOrDefault());
                    if (wpItem != null)
                    {
                        wmmItem.WorkgroupName = wpItem.Name;
                        wmmItem.WorkgroupWeight = wpItem.Weight;
                        this.workgroupManhourMonthService.Update(wmmItem);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateDataWorkgroupManhourMonth(WorkGroup wpObj, User userObj, List<WorkGroup> wpList, WorkgroupManhourMonth wmmObj)
        {
            try
            {
                DateTime date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                var docList = this.documentPackageService.GetAllByWorkgroup(wpObj.ID);
                var manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                var presentWMMObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpObj.ID, wpObj.ProjectId.GetValueOrDefault(), month, year);
                var prevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpObj.ID, month, year);
                if (presentWMMObj != null)
                {
                    wmmObj = presentWMMObj;
                    wmmObj.WorkgroupWeight = wpObj.Weight.GetValueOrDefault();
                    wmmObj.ManhourPlanTotal = manhourPlanTotal;
                    wmmObj.ManhourPlanMonth = (wmmObj.ManhourPlanTotal.GetValueOrDefault() * wmmObj.CompletePlanMonth.GetValueOrDefault()) / 100;
                    wmmObj.CompleteActualTotal = wpObj.Complete.GetValueOrDefault();
                    wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                    //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                    if (prevWMMObj != null)
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - prevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - prevWMMObj.CompleteActualTotal.GetValueOrDefault();
                    }
                    else
                    {
                        wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                        wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                    }

                    wmmObj.UpdateBy = userObj.Id;
                    wmmObj.UpdateDate = DateTime.Now;
                    this.workgroupManhourMonthService.Update(wmmObj);
                }
                else
                {
                    foreach (var wpItem in wpList)
                    {
                        docList = this.documentPackageService.GetAllByWorkgroup(wpItem.ID);
                        manhourPlanTotal = docList.Sum(t => t.ManHourPlan.GetValueOrDefault());
                        var tempObj = this.workgroupManhourMonthService.GetByWPAndPJ(wpItem.ID, wpItem.ProjectId.GetValueOrDefault(), month, year);
                        var tempPrevWMMObj = this.workgroupManhourMonthService.GetLastedByPJAndWP(wpObj.ProjectId.GetValueOrDefault(), wpItem.ID, month, year);
                        if (tempObj == null)
                        {
                            wmmObj = new WorkgroupManhourMonth();
                            wmmObj.Month = month;
                            wmmObj.Year = year;
                            wmmObj.WorkgroupID = wpItem.ID;
                            wmmObj.WorkgroupName = wpItem.Name;
                            wmmObj.DeparmentID = wpItem.DepartmentId;
                            wmmObj.ProjectID = wpItem.ProjectId.GetValueOrDefault();
                            wmmObj.WorkgroupWeight = wpItem.Weight.GetValueOrDefault();
                            wmmObj.ManhourPlanTotal = manhourPlanTotal;
                            wmmObj.CompleteActualTotal = wpItem.Complete.GetValueOrDefault();
                            wmmObj.ManhourActualAccumulation = (wmmObj.CompleteActualTotal.GetValueOrDefault() * manhourPlanTotal) / 100;
                            //wmmObj.ManhourActualMonth = wmmObj.ManhourActualMonth.GetValueOrDefault() + currentManhourActual;
                            if (tempPrevWMMObj != null)
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault() - tempPrevWMMObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault() - tempPrevWMMObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            else
                            {
                                wmmObj.ManhourActualMonth = wmmObj.ManhourActualAccumulation.GetValueOrDefault();
                                wmmObj.CompleteActualMonth = wmmObj.CompleteActualTotal.GetValueOrDefault();
                            }
                            wmmObj.CreateBy = userObj.Id;
                            wmmObj.CreateDate = DateTime.Now;
                            this.workgroupManhourMonthService.Insert(wmmObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourWeek(DocumentPackage documentObj, User userObj, UserManhour userManhourObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;

                CultureInfo myCI = CultureInfo.CurrentCulture;
                System.Globalization.Calendar myCal = myCI.Calendar;
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var listUserManhour = this.userManhourService.GetByDocIDAndUserID(documentObj.ID, user.Id);
                            listUserManhour = listUserManhour.OrderByDescending(t => t.ID).ToList();

                            var currentUserManhourWeek = listUserManhour.FirstOrDefault(t => myCal.GetWeekOfYear(DateTime.Now.Date, myCWR, myFirstDOW) == myCal.GetWeekOfYear(t.Date.GetValueOrDefault(), myCWR, myFirstDOW));
                            int index = listUserManhour.IndexOf(currentUserManhourWeek);
                            var prevUserManhourWeek = new UserManhour();
                            if (index != -1 && (index + 1) < listUserManhour.Count)
                            {
                                prevUserManhourWeek = listUserManhour[(index + 1)];
                            }
                            if (currentUserManhourWeek != null)
                            {
                                userManhourObj = currentUserManhourWeek;
                                if (prevUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - prevUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UpdateBy = userObj.Id;
                                userManhourObj.UpdateDate = DateTime.Now;
                                this.userManhourService.Update(userManhourObj);
                            }
                            else
                            {
                                var lastedUserManhourWeek = new UserManhour();
                                if (listUserManhour.Count > 0)
                                {
                                    lastedUserManhourWeek = listUserManhour[0];
                                }
                                userManhourObj = new UserManhour();
                                userManhourObj.DocumentID = documentObj.ID;
                                userManhourObj.Date = DateTime.Now.Date;
                                userManhourObj.DeparmentID = user.RoleId;
                                userManhourObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourObj.ProjectID = documentObj.ProjectId;
                                if (lastedUserManhourWeek != null)
                                {
                                    userManhourObj.CompleteWeek = (documentObj.Complete.GetValueOrDefault() - lastedUserManhourWeek.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourObj.ManhourActualWeek = (documentObj.ManHours.GetValueOrDefault() - lastedUserManhourWeek.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourObj.CompleteWeek = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourObj.ManhourActualWeek = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourObj.DocumentName = documentObj.DocNo;
                                userManhourObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourObj.UserID = user.Id;
                                userManhourObj.UserName = user.FullName;
                                userManhourObj.CreateBy = userObj.Id;
                                userManhourObj.CreateDate = DateTime.Now;
                                this.userManhourService.Insert(userManhourObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpdateUserManhourMonth(DocumentPackage documentObj, User userObj, UserManhourMonth userManhourMonthObj)
        {
            try
            {
                var engList = documentObj.EngineerId.Split(',').ToList();
                var diffMonth = ((documentObj.Deadline.GetValueOrDefault().Year - documentObj.StartDate.GetValueOrDefault().Year) * 12) + documentObj.Deadline.GetValueOrDefault().Month - documentObj.StartDate.GetValueOrDefault().Month;
                int countEng = engList.Count;
                var date = DateTime.Now;
                int month = date.Month;
                int year = date.Year;
                int prevMonth = date.AddMonths(-1).Month;
                int prevYear = date.AddMonths(-1).Year;
                if (countEng > 0)
                {
                    foreach (var item in engList)
                    {
                        var user = this.userService.GetByID(Convert.ToInt32(item));
                        if (user != null)
                        {
                            var currentUserManhourMonth = this.userManhourMonthService.GetByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            var prevUserManhourMonth = this.userManhourMonthService.GetLastedByDocIDAndUserID(documentObj.ID, user.Id, month, year);
                            if (currentUserManhourMonth != null)
                            {
                                userManhourMonthObj = currentUserManhourMonth;
                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UpdateBy = userObj.Id;
                                userManhourMonthObj.UpdateDate = DateTime.Now;
                                this.userManhourMonthService.Update(userManhourMonthObj);
                            }
                            else
                            {
                                userManhourMonthObj = new UserManhourMonth();
                                userManhourMonthObj.DocumentID = documentObj.ID;
                                userManhourMonthObj.Month = DateTime.Now.Month;
                                userManhourMonthObj.Year = DateTime.Now.Year;
                                userManhourMonthObj.DeparmentID = user.RoleId;
                                userManhourMonthObj.WorkgroupID = documentObj.WorkgroupId;
                                userManhourMonthObj.ProjectID = documentObj.ProjectId;

                                if (prevUserManhourMonth != null)
                                {
                                    userManhourMonthObj.CompleteMonth = (documentObj.Complete.GetValueOrDefault() - prevUserManhourMonth.CompleteTotal.GetValueOrDefault()) / countEng;
                                    userManhourMonthObj.ManhourActualMonth = (documentObj.ManHours.GetValueOrDefault() - prevUserManhourMonth.ManhourActualTotal.GetValueOrDefault()) / countEng;
                                }
                                else
                                {
                                    userManhourMonthObj.CompleteMonth = documentObj.Complete.GetValueOrDefault() / countEng;
                                    userManhourMonthObj.ManhourActualMonth = documentObj.ManHours.GetValueOrDefault() / countEng;
                                }
                                userManhourMonthObj.DocumentName = documentObj.DocNo;
                                userManhourMonthObj.CompleteTotal = documentObj.Complete.GetValueOrDefault();
                                userManhourMonthObj.ManhourActualTotal = documentObj.ManHours.GetValueOrDefault();
                                userManhourMonthObj.UserID = user.Id;
                                userManhourMonthObj.UserName = user.FullName;
                                userManhourMonthObj.CreateBy = userObj.Id;
                                userManhourMonthObj.CreateDate = DateTime.Now;
                                this.userManhourMonthService.Insert(userManhourMonthObj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}


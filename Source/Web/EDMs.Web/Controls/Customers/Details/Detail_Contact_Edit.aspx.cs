﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Data.Entities;

namespace EDMs.Web.Controls.Customers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Business.Services;
    using EDMs.Web.Utilities.Sessions;
    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class Detail_Contact_Edit : Page
    {
        /// <summary>
        /// The patient status service
        /// </summary>
        private Patient_ContactService _patientContactService;
        private ServiceTypeService _serviceTypeService;

        /// <summary>
        /// The patient service
        /// </summary>
        private PatientService _patientService;

        public Detail_Contact_Edit()
        {
            _serviceTypeService = new ServiceTypeService();
            _patientContactService = new Patient_ContactService();
            _patientService =new PatientService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["cID"]))
            {
                var cID = Request.QueryString["cID"];
                if (cID != null)
                {
                    int tempcID;
                    if (int.TryParse(cID, out tempcID))
                    {
                       var customer = _patientService.GetByID(tempcID);
                       lblTenKhachHang.Text = customer.FullName;
                    }
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                var ID = Request.QueryString["ID"];
                if (ID != null)
                {
                    int tempID;
                    if (int.TryParse(ID, out tempID))
                    {
                        Contact contact = _patientContactService.GetByID(tempID);
                        txtContent.Text = contact.Content;
                        txtDescription.Text = contact.Description;
                        txtDateTime.SelectedDate = contact.CreatedDate;
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                var ID = Request.QueryString["ID"];
                if (ID != null)
                {
                    int tempID;
                    if (int.TryParse(ID, out tempID))
                    {
                        //Update
                        UpdateContact(tempID);
                        ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
                    }
                }
            }
            else
            {
                CreateContact();
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }
        private void UpdateContact(int ID)
        {
            //Create
            Contact cs = _patientContactService.GetByID(ID);
            cs.CreatedDate = txtDateTime.SelectedDate;
            cs.UpdatedBy = UserSession.Current.User.Id;
            cs.UpdatedDate = DateTime.Now;
            //cs.UserID = UserSession.Current.User.Id;
            cs.Content = txtContent.Text.Trim();
            cs.Description = txtDescription.Text.Trim();
            
            _patientContactService.Update(cs);
        }
        private void CreateContact()
        {
            //Create
            Contact cs = new Contact();
            cs.UpdatedBy = UserSession.Current.User.Id;
            cs.UpdatedDate = DateTime.Now;
            cs.CreatedDate = txtDateTime.SelectedDate;
            cs.UserID = UserSession.Current.User.Id;
            cs.CustomerID = Request.QueryString["cID"] == null ? 0 : int.Parse(Request.QueryString["cID"].ToString());
            cs.Content = txtContent.Text.Trim();
            cs.Description = txtDescription.Text.Trim();

            _patientContactService.Insert(cs);
        }
        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}
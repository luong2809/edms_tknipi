﻿namespace EDMs.Business.Services.Library
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Library;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class WorkService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly WorkDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkService"/> class.
        /// </summary>
        public WorkService()
        {
            this.repo = new WorkDAO();
        }

        #region Get (Advances)
        public List<Work> GetByTypeAndYearAndBlock(int type, int year, int block)
        {
            return this.repo.GetByTypeAndYearAndBlock(type, year, block);
        }

        public List<Work> GetByTypeAndYear(int type, int year)
        {
            return this.repo.GetByTypeAndYear(type, year);
        }

        public List<Work> GetByYear(int year)
        {
            return this.repo.GetByYear(year);
        }

        public List<Work> GetByParent(int parentID)
        {
            return this.repo.GetByParent(parentID);
        }

        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Work> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        public List<Work> GetAllParent(int year)
        {
            return this.repo.GetAllParent(year);
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Work GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Work bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Work bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Work bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Scope;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using OfficeHelper.Utilities.Data;

    using Telerik.Web.UI;
    using System.Net.Mail;
    using System.Text;
    using System.Net;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ICATransmittalAttachDocument : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly ICATransmittalService iCATransmittalService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The group data permission service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly DocumentPackageService documentPackageService;

        private readonly WorkGroupService workGroupService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly AttachDocToICATransmittalService attachDocToICATransmittalService;

        private readonly FolderService folderService;

        private readonly AttachFilesPackageService attachFilesPackageService;

        private readonly TemplateManagementService templateManagementService;

        //private readonly ToDoListService todolistService;
        private readonly AttachResponseFileService attachresponseFileService;
        private readonly ProjectAppendixService projectAppendixService;
        private readonly RoleService roleService = new RoleService();
        private readonly AttachCommentFileService attachCommentFileService = new AttachCommentFileService();
        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        private readonly int TransmittalFolderId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TransFolderId"));

        /// <summary>
        /// Initializes a new instance of the <see cref="TransmittalAttachDocument"/> class.
        /// </summary>
        public ICATransmittalAttachDocument()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.receivedFromService = new ReceivedFromService();
            this.iCATransmittalService = new ICATransmittalService();
            this.categoryService = new CategoryService();
            this.userService = new UserService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.documentPackageService = new DocumentPackageService();
            this.workGroupService = new WorkGroupService();
            this.scopeProjectService = new ScopeProjectService();
            this.attachDocToICATransmittalService = new AttachDocToICATransmittalService();
            this.folderService = new FolderService();
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.templateManagementService = new TemplateManagementService();
            this.userDataPermissionService = new UserDataPermissionService();
            ///this.todolistService = new ToDoListService();
            this.attachresponseFileService = new AttachResponseFileService();
            this.projectAppendixService = new ProjectAppendixService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                this.LoadComboData();
                if (Request.QueryString["type"] == "1")
                {
                    divIn.Visible = true;
                    divOut.Visible = false;
                }
                else if (Request.QueryString["type"] == "2")
                {
                    divIn.Visible = false;
                    divOut.Visible = true;
                    this.ddlStepDefinition.SelectedValue = "IFR";
                    this.ddlType.SelectedValue = "1";
                }
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
            else if (e.Argument.Contains("delete"))
            {
                var id = e.Argument.Split('_').ToList()[1];
                var docId = Convert.ToInt32(id);
                var temp = this.attachDocToICATransmittalService.GetById(docId);
                if (temp != null)
                {
                    this.attachDocToICATransmittalService.Delete(temp);
                    this.rgdTransDocumenlist.Rebind();
                }
            }
            else if (e.Argument == "Closing")
            {
                #region Hiden
                //if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                //{
                //    var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                //    var objTran = this.iCATransmittalService.GetById(tranId);
                //    var listSelectedDocId = new List<int>();
                //    var listAttachFilePackage = new List<AttachFilesPackage>();

                //    if (objTran != null)
                //    {
                //        var count = 1;
                //        var filePath = Server.MapPath("../../Transmittals") + @"\";
                //        var workbook = new Workbook();

                //        var templateManagement = this.templateManagementService.GetSpecial(4, 8);
                //        if (templateManagement != null)
                //        {
                //            if (Request.QueryString["type"] == "1")
                //            {
                //                foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                //                {
                //                    var cboxSelected =
                //                        (System.Web.UI.WebControls.CheckBox)
                //                            item["IsSelected"].FindControl("cboxSelectDocTransmittal");
                //                    if (cboxSelected.Checked)
                //                    {
                //                        var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));
                //                        listSelectedDocId.Add(docId);

                //                        var attachDoc = new AttachDocToTransmittal()
                //                        {
                //                            TransmittalId = tranId,
                //                            DocumentId = docId
                //                        };

                //                        if (!this.attachDocToICATransmittalService.IsExist(tranId, docId))
                //                        {
                //                            this.attachDocToICATransmittalService.Insert(attachDoc);
                //                        }

                //                        cboxSelected.Checked = false;
                //                    }
                //                }
                //                var attachDocList = this.attachDocToICATransmittalService.GetAllByTransId(tranId);
                //                foreach (var item in attachDocList)
                //                {
                //                    var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                //                    if (docObj != null)
                //                    {


                //                        docObj.IncomingTransNo = objTran.TransmittalNumber;
                //                        docObj.IncomingTransDate = objTran.IssuseDate;

                //                        this.documentPackageService.Update(docObj);
                //                    }


                //                }
                //            }
                //            else if (Request.QueryString["type"] == "2")
                //            {
                //                var listdocTrans = this.attachDocToICATransmittalService.GetAllByTransId(objTran.ID);
                //                if (listdocTrans.Count>0)
                //                {
                //                    foreach (var item in listdocTrans) {

                //                        var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                //                        if (docObj != null)
                //                        {
                //                            var selectitemstep = this.ddlStepDefinition.SelectedItem;
                //                            var duration = this.txtDuration.Value != null ? Convert.ToInt32(this.txtDuration.Value) : 0;
                //                            if(selectitemstep != null){
                //                                if (selectitemstep.Text == "Response")
                //                            {
                //                                docObj.OutgoingTransNo = objTran.TransmittalNumber;
                //                                docObj.OutgoingTransDate = objTran.IssuseDate;
                //                                docObj.DealineComment = objTran.IssuseDate.GetValueOrDefault().AddDays(Convert.ToInt32(duration));
                //                                docObj.StatusName = selectitemstep.Text;
                //                            }
                //                            else 
                //                            {
                //                                docObj.ResponseTransNo = objTran.TransmittalNumber;
                //                                docObj.ResponseDate = objTran.IssuseDate;
                //                                docObj.ResponseDeadline = objTran.IssuseDate.GetValueOrDefault().AddDays(Convert.ToInt32(duration));
                //                            }}
                //                            else
                //                            {
                //                                docObj.OutgoingTransNo = !string.IsNullOrEmpty(docObj.OutgoingTransNo) ? docObj.OutgoingTransNo : objTran.TransmittalNumber;
                //                                docObj.OutgoingTransDate = !string.IsNullOrEmpty(docObj.OutgoingTransNo) ? docObj.OutgoingTransDate : objTran.IssuseDate;
                //                            }
                //                            this.documentPackageService.Update(docObj);
                //                        }
                //                    }


                //                    var transInfo = new DataTable();
                //                    var docDt = new DataTable();
                //                    var ds = new DataSet();
                //                    var listColumn = new DataColumn[]
                //            {
                //                new DataColumn("Index", Type.GetType("System.String")),
                //                new DataColumn("DocumentNumber", Type.GetType("System.String")),
                //                new DataColumn("RevisionName", Type.GetType("System.String")),
                //                new DataColumn("Description", Type.GetType("System.String")),
                //                new DataColumn("Remark", Type.GetType("System.String"))
                //            };
                //                    var listColumn1 = new[]
                //                {
                //                    new DataColumn("IssuseDate", Type.GetType("System.String")),
                //                    new DataColumn("FromList", Type.GetType("System.String")),
                //                    new DataColumn("ContractorName", Type.GetType("System.String")),
                //                    new DataColumn("ToList", Type.GetType("System.String")),
                //                    new DataColumn("ProjectName", Type.GetType("System.String")),
                //                    new DataColumn("TransmittalNumber", Type.GetType("System.String")),
                //                    new DataColumn("DcName", Type.GetType("System.String")),
                //                    new DataColumn("DcEmail", Type.GetType("System.String")),
                //                };
                //                    docDt.Columns.AddRange(listColumn);
                //                    transInfo.Columns.AddRange(listColumn1);

                //                    var from = this.userService.GetByID(objTran.FromId.GetValueOrDefault());
                //                    var to = this.userService.GetByID(objTran.ToId.GetValueOrDefault());

                //                    var infoItem = transInfo.NewRow();
                //                    infoItem["IssuseDate"] = objTran.IssuseDate != null ? objTran.IssuseDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                //                    infoItem["FromList"] = from.FullName;
                //                    infoItem["ContractorName"] = objTran.ContractorName;
                //                    infoItem["ToList"] = to.FullName;
                //                    infoItem["ProjectName"] = objTran.ProjectName;
                //                    infoItem["TransmittalNumber"] = objTran.TransmittalNumber;
                //                    infoItem["DcName"] = from.FullName;
                //                    infoItem["DcEmail"] = from.Email;
                //                    transInfo.Rows.Add(infoItem);

                //                    count = 0;


                //                    var attachDocList = this.attachDocToICATransmittalService.GetAllByTransId(tranId);
                //                    foreach (var item in attachDocList)
                //                    {
                //                        var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                //                        if (docObj != null)
                //                        {
                //                            var dataItem = docDt.NewRow();
                //                            count += 1;
                //                            dataItem["Index"] = count;
                //                            dataItem["DocumentNumber"] = docObj.DocNo;
                //                            dataItem["RevisionName"] = docObj.RevisionName;
                //                            dataItem["Description"] = docObj.DocTitle;

                //                            dataItem["Remark"] = "1 Bộ";

                //                            docDt.Rows.Add(dataItem);


                //                        }
                //                    }

                //                    ds.Tables.Add(docDt);
                //                    ds.Tables[0].TableName = "Table";

                //                    var rootPath = Server.MapPath("../../Transmittals/");
                //                    const string WordPath = @"Template\";
                //                    const string WordPathExport = @"Generated\";
                //                    var StrTemplateFileName = templateManagement.FilePath;
                //                    var strOutputFileName = Utility.RemoveSpecialCharacter(objTran.TransmittalNumber) + "_" +
                //                                            DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                //                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                //                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName,
                //                        transInfo, ds);

                //                    if (isSuccess)
                //                    {

                //                        if (File.Exists(Server.MapPath(objTran.GeneratePath)))
                //                        {
                //                            File.Delete(Server.MapPath(objTran.GeneratePath));
                //                        }

                //                        var serverPath = "../../Transmittals/Generated/";
                //                        objTran.GeneratePath = serverPath + strOutputFileName;
                //                        objTran.IsGenerate = true;

                //                        this.iCATransmittalService.Update(objTran);
                //                    }
                //                }

                //                // Auto create transmittal folder on Document library and share document files
                //                int type = Request.QueryString["type"] == "2" ? 7 : 6;
                //                var templateManagement1 = this.templateManagementService.GetSpecial(type, (int)objTran.ProjectId);

                //                var mainTransFolder = this.folderService.GetById(templateManagement1.TransFolderId.GetValueOrDefault());

                //                try
                //                {
                //                    var usersInPermissionOfParent = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(mainTransFolder.ID));

                //                    var transFolderId = 0;
                //                    var pdfFolderId = 0;
                //                    var nativeFilesFolderId = 0;

                //                    // Add new transmittal folder
                //                    var transfolder =
                //                        this.folderService.GetByDirName(mainTransFolder.DirName + "/" +
                //                                    Utility.RemoveSpecialCharacter(objTran.TransmittalNumber));
                //                    var pdfFolder = new Folder();
                //                    var nativeFilesFolder = new Folder();
                //                    if (transfolder == null)
                //                    {
                //                        transfolder = new Folder()
                //                        {
                //                            Name = objTran.TransmittalNumber,
                //                            ParentID = mainTransFolder.ID,
                //                            ProjectId = mainTransFolder.ProjectId,
                //                            DirName =
                //                                mainTransFolder.DirName + "/" +
                //                                Utility.RemoveSpecialCharacter(objTran.TransmittalNumber),
                //                            CreatedBy = UserSession.Current.User.Id,
                //                            CreatedDate = DateTime.Now
                //                        };

                //                        Directory.CreateDirectory(Server.MapPath(transfolder.DirName));
                //                        transFolderId = this.folderService.Insert(transfolder).GetValueOrDefault();

                //                        // Inherit permission from Parent folder
                //                        foreach (var parentPermission in usersInPermissionOfParent)
                //                        {
                //                            var childPermission = new UserDataPermission()
                //                            {
                //                                CategoryId = parentPermission.CategoryId,
                //                                RoleId = parentPermission.RoleId,
                //                                FolderId = transFolderId,
                //                                UserId = parentPermission.UserId,
                //                                IsFullPermission = parentPermission.IsFullPermission,
                //                                CreatedDate = DateTime.Now,
                //                                CreatedBy = UserSession.Current.User.Id
                //                            };

                //                            this.userDataPermissionService.Insert(childPermission);
                //                        }

                //                    }
                //                    else
                //                    {
                //                        transFolderId = transfolder.ID;
                //                    }

                //                    pdfFolder = this.folderService.GetByDirName(transfolder.DirName + "/PDF");
                //                    nativeFilesFolder = this.folderService.GetByDirName(transfolder.DirName + "/Native Files");

                //                    if (pdfFolder == null)
                //                    {
                //                        pdfFolder = new Folder()
                //                        {
                //                            Name = "PDF",
                //                            ParentID = transFolderId,
                //                            ProjectId = transfolder.ProjectId,
                //                            DirName = transfolder.DirName + "/PDF",
                //                            CreatedBy = UserSession.Current.User.Id,
                //                            CreatedDate = DateTime.Now
                //                        };
                //                        Directory.CreateDirectory(Server.MapPath(pdfFolder.DirName));
                //                        pdfFolderId = this.folderService.Insert(pdfFolder).GetValueOrDefault();

                //                        // Inherit permission from Parent folder
                //                        foreach (var parentPermission in usersInPermissionOfParent)
                //                        {
                //                            var childPermission = new UserDataPermission()
                //                            {
                //                                CategoryId = parentPermission.CategoryId,
                //                                RoleId = parentPermission.RoleId,
                //                                FolderId = pdfFolderId,
                //                                UserId = parentPermission.UserId,
                //                                IsFullPermission = parentPermission.IsFullPermission,
                //                                CreatedDate = DateTime.Now,
                //                                CreatedBy = UserSession.Current.User.Id
                //                            };

                //                            this.userDataPermissionService.Insert(childPermission);
                //                        }
                //                    }
                //                    else
                //                    {
                //                        pdfFolderId = pdfFolder.ID;


                //                    }

                //                    if (nativeFilesFolder == null)
                //                    {
                //                        nativeFilesFolder = new Folder()
                //                        {
                //                            Name = "Native Files",
                //                            ParentID = transFolderId,
                //                            ProjectId = transfolder.ProjectId,
                //                            DirName = transfolder.DirName + "/Native Files",
                //                            CreatedBy = UserSession.Current.User.Id,
                //                            CreatedDate = DateTime.Now
                //                        };
                //                        Directory.CreateDirectory(Server.MapPath(nativeFilesFolder.DirName));
                //                        nativeFilesFolderId = this.folderService.Insert(nativeFilesFolder).GetValueOrDefault();
                //                        // Inherit permission from Parent folder
                //                        foreach (var parentPermission in usersInPermissionOfParent)
                //                        {
                //                            var childPermission = new UserDataPermission()
                //                            {
                //                                CategoryId = parentPermission.CategoryId,
                //                                RoleId = parentPermission.RoleId,
                //                                FolderId = nativeFilesFolderId,
                //                                UserId = parentPermission.UserId,
                //                                IsFullPermission = parentPermission.IsFullPermission,
                //                                CreatedDate = DateTime.Now,
                //                                CreatedBy = UserSession.Current.User.Id
                //                            };

                //                            this.userDataPermissionService.Insert(childPermission);
                //                        }
                //                    }
                //                    else
                //                    {
                //                        nativeFilesFolderId = nativeFilesFolder.ID;

                //                    }


                //                    foreach (var docId in listSelectedDocId)
                //                    {
                //                        var todoitem = this.documentPackageService.GetById(docId);
                //                        var docintrans = this.attachDocToICATransmittalService.GetByDoc(objTran.ID, docId);
                //                        if (todoitem != null)
                //                        {
                //                            if (docintrans != null && docintrans.StepStatus == "Response")
                //                            {
                //                                var reponsefile = this.attachresponseFileService.GetAllByDocId(docId).FirstOrDefault(t => t.IsDefault.GetValueOrDefault());
                //                                if(reponsefile != null){ // Path file to save on server disc
                //                                    var saveFilePath = reponsefile.Extension.ToLower() == "pdf" ? Path.Combine(Server.MapPath(pdfFolder.DirName), reponsefile.Filename) : Path.Combine(Server.MapPath(nativeFilesFolder.DirName),
                //                                reponsefile.Filename);
                //                                    // Path file to download from server
                //                                    var serverFilePath = reponsefile.Extension.ToLower() == "pdf" ? pdfFolder.DirName + "/" + reponsefile.Filename : nativeFilesFolder.DirName + "/" + reponsefile.Filename;
                //                                    var fileExt = reponsefile.Extension;
                //                                    if (File.Exists(Server.MapPath(reponsefile.FilePath)))
                //                                    {
                //                                        File.Copy(Server.MapPath(reponsefile.FilePath), saveFilePath, true);
                //                                    }
                //                                }
                //                            }
                //                            else
                //                            {
                //                                listAttachFilePackage.AddRange(
                //                               this.attachFilesPackageService.GetAllDocumentFileByDocId(docId));
                //                            }
                //                        }


                //                    }

                //                    var pdfFiles = listAttachFilePackage.Where(t => t.Extension.ToLower() == "pdf").ToList();
                //                    var nativeFiles = listAttachFilePackage.Where(t => t.Extension.ToLower() != "pdf").ToList();

                //                    foreach (var pdfFile in pdfFiles)
                //                    {
                //                        // Path file to save on server disc
                //                        var saveFilePath = Path.Combine(Server.MapPath(pdfFolder.DirName), pdfFile.FileName);
                //                        // Path file to download from server
                //                        var serverFilePath = pdfFolder.DirName + "/" + pdfFile.FileName;
                //                        var fileExt = pdfFile.Extension;

                //                        if (!File.Exists(saveFilePath))
                //                        {
                //                            var document = new Document()
                //                            {
                //                                Name = pdfFile.FileName,
                //                                FileExtension = fileExt,
                //                                FileExtensionIcon = pdfFile.ExtensionIcon,
                //                                FilePath = serverFilePath,
                //                                FolderID = pdfFolderId,
                //                                IsLeaf = true,
                //                                IsDelete = false,
                //                                CreatedBy = UserSession.Current.User.Id,
                //                                CreatedDate = DateTime.Now
                //                            };
                //                            this.documentService.Insert(document);
                //                        }

                //                        if (File.Exists(Server.MapPath(pdfFile.FilePath)))
                //                        {
                //                            File.Copy(Server.MapPath(pdfFile.FilePath), saveFilePath, true);
                //                        }


                //                    }

                //                    foreach (var nativeFile in nativeFiles)
                //                    {
                //                        // Path file to save on server disc
                //                        var saveFilePath = Path.Combine(Server.MapPath(nativeFilesFolder.DirName),
                //                            nativeFile.FileName);
                //                        // Path file to download from server
                //                        var serverFilePath = nativeFilesFolder.DirName + "/" + nativeFile.FileName;
                //                        var fileExt = nativeFile.Extension;

                //                        if (!File.Exists(saveFilePath))
                //                        {
                //                            var document = new Document()
                //                            {
                //                                Name = nativeFile.FileName,
                //                                FileExtension = fileExt,
                //                                FileExtensionIcon = nativeFile.ExtensionIcon,
                //                                FilePath = serverFilePath,
                //                                FolderID = nativeFilesFolderId,
                //                                IsLeaf = true,
                //                                IsDelete = false,
                //                                CreatedBy = UserSession.Current.User.Id,
                //                                CreatedDate = DateTime.Now
                //                            };
                //                            this.documentService.Insert(document);
                //                        }

                //                        if (File.Exists(Server.MapPath(nativeFile.FilePath)))
                //                        {
                //                            File.Copy(Server.MapPath(nativeFile.FilePath), saveFilePath, true);
                //                        }
                //                    }

                //                }
                //                catch (Exception)
                //                {

                //                }
                //            }
                //        }
                //    }

                //   // this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                //}
                #endregion
            }
        }
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var transobj = this.iCATransmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                var projectId = transobj.ProjectId.GetValueOrDefault();
                var workgroupId = !string.IsNullOrEmpty(this.ddlWorkgroup.SelectedValue) ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue) : 0;
                var docNo = this.txtDocNo.Text.Trim();
                var docTitle = this.txtDocTitle.Text.Trim();

                var listDoc = this.documentPackageService.SearchDocument(
                    projectId,
                    workgroupId,
                    docNo,
                    docTitle);
                this.grdDocument.DataSource = listDoc;
            }
        }

        /// <summary>
        /// The rad menu_ item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        //protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        //{
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        private DateTime GetBusinessDays(DateTime start, int duration)
        {
            DateTime listdate = start;
            int i = 1;
            while (i <= duration)
            {
                listdate = listdate.AddDays(1);
                if (listdate.DayOfWeek != DayOfWeek.Saturday && listdate.DayOfWeek != DayOfWeek.Sunday) { i++; }

            }
            return listdate;
        }


        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var transobj = this.iCATransmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                var projectId = transobj.ProjectId.GetValueOrDefault();
                var listWorkgroup = this.workGroupService.GetAllWorkGroupOfProject(projectId).OrderBy(t => t.ID).ToList();
                this.ddlWorkgroup.DataSource = listWorkgroup;
                this.ddlWorkgroup.DataTextField = "Name";
                this.ddlWorkgroup.DataValueField = "ID";
                this.ddlWorkgroup.DataBind();
            }
        }

        /// <summary>
        /// The btn save_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Session["EditStep"] != null)
            {
                var id = Convert.ToInt32(Session["EditStep"].ToString());
                var docintrans = this.attachDocToICATransmittalService.GetById(id);
                if (docintrans != null)
                {
                    docintrans.StepStatus = this.ddlStepDefinition.SelectedItem != null ? this.ddlStepDefinition.SelectedValue : "";
                    docintrans.TypeID = Convert.ToInt32(this.ddlType.SelectedValue);
                    if (Request.QueryString["type"] == "1")
                    {
                        docintrans.ReviewCode = this.ddlReviewCode.SelectedValue;
                        docintrans.DNVMarkup = this.txtDNV.Text;
                        docintrans.MWSMarkup = this.txtMWS.Text;
                        docintrans.VRMarkup = this.txtVR.Text;
                    }
                    else if (Request.QueryString["type"] == "2")
                    {
                        docintrans.StepStatus = this.ddlStepDefinition.SelectedItem != null ? this.ddlStepDefinition.SelectedValue : "";
                        docintrans.TypeID = Convert.ToInt32(this.ddlType.SelectedValue);
                    }
                    this.attachDocToICATransmittalService.Update(docintrans);
                }
                Session.Remove("EditStep");
                // this.ddlStepDefinition.SelectedValue="0";
                //  this.txtDuration.Value = null;

            }
            else
            {
                var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                var objTran = this.iCATransmittalService.GetById(tranId);
                if (objTran != null)
                {
                    //var docId = Convert.ToInt32(this.lblDocId.Value);
                    //get id multi row selected
                    foreach (GridDataItem item in grdDocument.SelectedItems)
                    {
                        var docId = Convert.ToInt32(item.GetDataKeyValue("ID")); // Works if you set the DataKeyValue as ID 
                        if (docId != 0)
                        {
                            var docintrans = new AttachDocToICATransmittal();
                            docintrans.ICATransmittalId = tranId;
                            docintrans.DocumentId = docId;
                            if (Request.QueryString["type"] == "1")
                            {
                                docintrans.ReviewCode = this.ddlReviewCode.SelectedValue;
                                docintrans.DNVMarkup = this.txtDNV.Text;
                                docintrans.MWSMarkup = this.txtMWS.Text;
                                docintrans.VRMarkup = this.txtVR.Text;
                            }
                            else if (Request.QueryString["type"] == "2")
                            {
                                docintrans.StepStatus = this.ddlStepDefinition.SelectedItem != null ? this.ddlStepDefinition.SelectedValue : "";
                                docintrans.TypeID = Convert.ToInt32(this.ddlType.SelectedValue);
                            }

                            if (!this.attachDocToICATransmittalService.IsExist(tranId, docId))
                            {
                                this.attachDocToICATransmittalService.Insert(docintrans);
                            }
                        }
                    }
                }
            }

            this.ddlStepDefinition.SelectedIndex = 0;
            this.ddlStepDefinition.SelectedItem.Text = "";
            this.ddlType.SelectedIndex = 0;
            this.rgdTransDocumenlist.Rebind();
        }

        protected void grdDocument_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["HasAttachFile"].Text == "True")
                {
                    item.BackColor = Color.Aqua;
                    item.BorderColor = Color.Aqua;
                }
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            this.ddlStepDefinition.SelectedItem.Text = "";
        }

        protected void rgdTransDocumenlist_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (Request.QueryString["tranId"] != null)
            {
                var tranId = Convert.ToInt32(Request.QueryString["tranId"]);
                var listDocument = new List<DocumentPackage>();

                var attachDocList = this.attachDocToICATransmittalService.GetAllByTransId(tranId);
                foreach (var item in attachDocList)
                {
                    var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                    if (docObj != null)
                    {
                        listDocument.Add(docObj);
                    }
                }

                listDocument = listDocument.OrderBy(t => t.DocNo).ToList();
                var newtable = (from A in listDocument
                                join B in attachDocList on A.ID equals B.DocumentId
                                select new
                                {
                                    ID = B.ID,
                                    DocumentId = B.DocumentId,
                                    TransId = B.ICATransmittalId,
                                    StepStatus = B.StepStatus,
                                    DocNo = A.DocNo,
                                    DocTitle = A.DocTitle,
                                    RevisionName = A.RevisionName,
                                    StartDate = A.StartDate,
                                    Deadline = A.Deadline,
                                });
                this.rgdTransDocumenlist.DataSource = newtable;
            }
        }

        protected void rgdTransDocumenlist_ItemCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            if (e.CommandName == "EditCmd")
            {
                var Id = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
                var docintrans = this.attachDocToICATransmittalService.GetById(Convert.ToInt32(Id));
                if (docintrans != null)
                {

                    var DropdownList = docintrans.StepStatus;
                    if (DropdownList == null)
                    {
                        this.ddlStepDefinition.SelectedIndex = 0;
                    }
                    else if (DropdownList == "")
                    {
                        this.ddlStepDefinition.SelectedIndex = 0;
                    }
                    else if (DropdownList == "INF-Information")
                    {
                        this.ddlStepDefinition.SelectedIndex = 1;
                    }
                    else if (DropdownList == "IFR-Review")
                    {
                        this.ddlStepDefinition.SelectedIndex = 2;
                    }
                    else if (DropdownList == "Response")
                    {
                        this.ddlStepDefinition.SelectedIndex = 3;
                    }
                    else if (DropdownList == "IFA-Aproved")
                    {
                        this.ddlStepDefinition.SelectedIndex = 4;
                    }
                    else if (DropdownList == "IFC-Construction")
                    {
                        this.ddlStepDefinition.SelectedIndex = 5;
                    }
                    this.ddlType.SelectedValue = docintrans.TypeID != null ? docintrans.TypeID.ToString() : "1";
                    Session.Add("EditStep", Id);
                }
            }

        }
        protected void rgdTransDocumenlist_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var temp = this.attachDocToICATransmittalService.GetById(docId);
            if (temp != null)
            {
                this.attachDocToICATransmittalService.Delete(temp);
            }
        }
        protected void btFinish_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                var objTran = this.iCATransmittalService.GetById(tranId);
                var projectObj = this.scopeProjectService.GetById(objTran.ProjectId.GetValueOrDefault());
                var listSelectedDocId = new List<int>();
                var listAttachFilePackage = new List<AttachFilesPackage>();

                if (objTran != null)
                {
                    var templateManagement = this.templateManagementService.GetSpecial(4, 8);
                    if (templateManagement != null)
                    {
                        if (Request.QueryString["type"] == "1")
                        {
                            foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                            {
                                var cboxSelected =
                                    (System.Web.UI.WebControls.CheckBox)
                                        item["IsSelected"].FindControl("cboxSelectDocTransmittal");
                                if (cboxSelected.Checked)
                                {
                                    var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));
                                    listSelectedDocId.Add(docId);

                                    var attachDoc = new AttachDocToICATransmittal()
                                    {
                                        ICATransmittalId = tranId,
                                        DocumentId = docId
                                    };

                                    if (!this.attachDocToICATransmittalService.IsExist(tranId, docId))
                                    {
                                        this.attachDocToICATransmittalService.Insert(attachDoc);
                                    }

                                    cboxSelected.Checked = false;
                                }
                            }
                            var attachDocList = this.attachDocToICATransmittalService.GetAllByTransId(tranId);
                            foreach (var item in attachDocList)
                            {
                                var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                if (docObj != null)
                                {
                                    docObj.ICAReviewInTransNo = objTran.TransmittalNumber;
                                    docObj.ICAReviewInDate = objTran.IssuseDate;
                                    docObj.ICAReviewCode = item.ReviewCode;
                                    docObj.DNVMarkup = item.DNVMarkup;
                                    docObj.VRMarkup = item.VRMarkup;
                                    docObj.MWSMarkup = item.MWSMarkup;
                                    docObj.ICAStatus = "";

                                    this.documentPackageService.Update(docObj);

                                    if (objTran.IsGenerate.GetValueOrDefault())
                                    {
                                        var fileIcon = new Dictionary<string, string>()
                                        {
                                            { "doc", "~/images/wordfile.png" },
                                            { "docx", "~/images/wordfile.png" },
                                            { "dotx", "~/images/wordfile.png" },
                                            { "xls", "~/images/excelfile.png" },
                                            { "xlsx", "~/images/excelfile.png" },
                                            { "pdf", "~/images/pdffile.png" },
                                            { "7z", "~/images/7z.png" },
                                            { "dwg", "~/images/dwg.png" },
                                            { "dxf", "~/images/dxf.png" },
                                            { "rar", "~/images/rar.png" },
                                            { "zip", "~/images/zip.png" },
                                            { "txt", "~/images/txt.png" },
                                            { "xml", "~/images/xml.png" },
                                            { "xlsm", "~/images/excelfile.png" },
                                            { "bmp", "~/images/bmp.png" },
                                        };
                                        var fileExt = objTran.GeneratePath.Substring(objTran.GeneratePath.LastIndexOf(".") + 1, objTran.GeneratePath.Length - objTran.GeneratePath.LastIndexOf(".") - 1);
                                        var attachFile = new AttachCommentFile()
                                        {
                                            DocumentId = docObj.ID,
                                            Filename = objTran.TransmittalNumber + " comment",
                                            Extension = fileExt,
                                            FilePath = objTran.GeneratePath,
                                            ExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                                            CreatedBy = UserSession.Current.User.Id,
                                            CreatedByName = UserSession.Current.User.FullName,
                                            CreatedDate = DateTime.Now
                                        };

                                        this.attachCommentFileService.Insert(attachFile);
                                    }
                                }
                            }
                        }
                        else if (Request.QueryString["type"] == "2")
                        {
                            var listdocTrans = this.attachDocToICATransmittalService.GetAllByTransId(objTran.ID);
                            if (listdocTrans.Count > 0)
                            {
                                foreach (var item in listdocTrans)
                                {
                                    var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                    if (docObj != null)
                                    {
                                        listSelectedDocId.Add(docObj.ID);

                                        var selectitemstep = item.StepStatus;
                                        docObj.ICAReviewOutTransNo = objTran.TransmittalNumber;
                                        docObj.ICAReviewOutDate = objTran.IssuseDate;
                                        docObj.ICAStatus = "send";
                                        this.documentPackageService.Update(docObj);
                                    }
                                }
                            }
                        }
                    }
                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                }
            }
        }

        private List<int> GetAllChildren(int parent, List<Folder> folderList, ref List<int> childNodes)
        {
            var childFolderList = this.folderService.GetSpecificFolderChild(parent);
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList.Select(t => t.ID));
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, folderList, ref childNodes);
                }
            }

            childNodes.Add(parent);

            return childNodes.Distinct().ToList();
        }

        private List<int> GetAllParentID(int folderId, List<int> listFolderId)
        {
            var folder = this.folderService.GetById(folderId);
            if (folder.ParentID != null)
            {
                listFolderId.Add(folder.ParentID.Value);
                this.GetAllParentID(folder.ParentID.Value, listFolderId);
            }

            return listFolderId;
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttachDocToICATransmittalDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class AttachDocToICATransmittalDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AttachDocToICATransmittalDAO"/> class.
        /// </summary>
        public AttachDocToICATransmittalDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<AttachDocToICATransmittal> GetIQueryable()
        {
            return this.EDMsDataContext.AttachDocToICATransmittals;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AttachDocToICATransmittal> GetAll()
        {
            return this.EDMsDataContext.AttachDocToICATransmittals.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public AttachDocToICATransmittal GetById(int id)
        {
            return this.EDMsDataContext.AttachDocToICATransmittals.FirstOrDefault(ob => ob.ID == id);
        }

        public List<AttachDocToICATransmittal> GetAllByTransId(int tranid)
        {
            return this.EDMsDataContext.AttachDocToICATransmittals.Where(ob => ob.ICATransmittalId == tranid).ToList();
        }

        public AttachDocToICATransmittal GetByDoc(int transId, int docId)
        {
            return this.EDMsDataContext.AttachDocToICATransmittals.FirstOrDefault(t => t.ICATransmittalId == transId && t.DocumentId == docId);
        }

        public bool IsExist(int transId, int docId)
        {
            return this.EDMsDataContext.AttachDocToICATransmittals.Any(t => t.ICATransmittalId == transId && t.DocumentId == docId);
        }

        public List<AttachDocToICATransmittal> GetAllByDocId(int docId)
        {
            return this.EDMsDataContext.AttachDocToICATransmittals.Where(t => t.DocumentId == docId).ToList();
        }

        #endregion

        #region GET ADVANCE

        public AttachDocToICATransmittal GetByDocument(int id)
        {
            return this.EDMsDataContext.AttachDocToICATransmittals.OrderByDescending(ob => ob.ID).FirstOrDefault(ob => ob.DocumentId == id);
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see>
        ///       <cref>int?</cref>
        ///     </see> .
        /// </returns>
        public int? Insert(AttachDocToICATransmittal ob)
        {
            try
            {
                this.EDMsDataContext.AddToAttachDocToICATransmittals(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(AttachDocToICATransmittal src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(AttachDocToICATransmittal src)
        {
            try
            {
                AttachDocToICATransmittal des = (from rs in this.EDMsDataContext.AttachDocToICATransmittals
                                          where rs.ID == src.ID
                                          select rs).First();

                des.ICATransmittalId = src.ICATransmittalId;
                des.DocumentId = src.DocumentId;
                des.StepStatus = src.StepStatus;
                des.TypeID = src.TypeID;
                des.Status = src.Status;
                des.ReviewCode = src.ReviewCode;
                des.DNVMarkup = src.DNVMarkup;
                des.MWSMarkup = src.MWSMarkup;
                des.VRMarkup = src.VRMarkup;
                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

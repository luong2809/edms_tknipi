﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.DAO;
using EDMs.Data.Entities;

namespace EDMs.Business.Services
{
    public class PatientStatusService
    {       
       private readonly PatientStatusDAO repo;

        public PatientStatusService()
        {
            repo = new PatientStatusDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All PatientStatus
        /// </summary>
        /// <returns></returns>
        public List<PatientStatus> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get PatientStatus By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public PatientStatus GetByID(int ID)
        {
            return repo.GetByID( ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert PatientStatus
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(PatientStatus bo)
        {
            try
            {
                return repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update PatientStatus
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(PatientStatus bo)
        {
            try
            {
                return repo.Update(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete PatientStatus
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(PatientStatus bo)
        {
            try
            {
                return repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete PatientStatus By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

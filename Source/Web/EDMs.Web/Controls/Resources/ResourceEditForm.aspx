﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResourceEditForm.aspx.cs" Inherits="EDMs.Web.Controls.Resources.ResourceEditForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }
    </script>

    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>

        <div style="width: 100%">
            <ul style="list-style-type: none">
                <li style="width: 500px; padding-bottom: 3px;">
                    <div style="border-style: solid; border-color: #E44B25;"></div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <span style="color: #E44B25; font-size: 16px;">THÔNG TIN BÁC SĨ</span>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Họ và tên lót
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:TextBox ID="txtHoTenLot" runat="server" Style="width: 350px;"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Tên
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:TextBox ID="txtTen" runat="server" Style="width: 350px;"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Giới tính
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:DropDownList ID="ddlGioiTinh" runat="server" Width="100px">
                                <asp:ListItem Value="1">Nam</asp:ListItem>
                                <asp:ListItem Value="0">Nữ</asp:ListItem>

                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Ngày sinh
                            </span>
                        </label>
                        <div style="float: left;">
                            <telerik:RadDatePicker ID="txtNgaySinh" Skin="Metro" runat="server">
                                <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Tình trạng hôn nhân
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:DropDownList ID="ddlTinhTrangHonNhan" runat="server" Width="100px">
                                <asp:ListItem Value="1">Độc thân</asp:ListItem>
                                <asp:ListItem Value="2">Có gia đình</asp:ListItem>
                                <asp:ListItem Value="3">Ly dị</asp:ListItem>
                                <asp:ListItem Value="4">Góa</asp:ListItem>

                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Số CMND
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:TextBox ID="txtCMND" runat="server" Style="width: 150px;"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Địa chỉ
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:TextBox ID="txtDiaChi" runat="server" Style="width: 350px;"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Số điện thoại
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:TextBox ID="txtSoDienThoai" runat="server" Style="width: 150px;"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>



                <li style="width: 500px; padding-top: 10px; padding-bottom: 3px;">
                    <div style="border-style: solid; border-color: #E44B25;"></div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <span style="color: #E44B25; font-size: 16px;">THÔNG TIN KHÁC</span>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Mã bác sĩ
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:TextBox ID="txtMaKhachHang" runat="server" Style="width: 100px;"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Nghề nghiệp
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:TextBox ID="txtNgheNghiep" runat="server" Style="width: 350px;"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">E-mail
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:TextBox ID="txtEmail" runat="server" Style="width: 350px;"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; font-size: 16px;">Nhóm
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:DropDownList ID="ddlTinhTrang" runat="server" Width="200px">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px; padding-top: 10px; padding-bottom: 3px; text-align: center">
                    <telerik:RadButton ID="btnCapNhat" runat="server" Text="Cập nhật" OnClick="btnCapNhat_Click">
                        <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btncancel" runat="server" Text="Bỏ qua"
                        OnClick="btncancel_Click">
                        <Icon PrimaryIconUrl="../../Images/Cancel.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>

                </li>
            </ul>
        </div>


    </form>
</body>
</html>

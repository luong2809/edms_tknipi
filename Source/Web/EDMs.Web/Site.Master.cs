﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Site.Master.cs" company="">
//   
// </copyright>
// <summary>
//   The site master.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Web.Security;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using EDMs.Data.Entities;
    using System.Web.UI.WebControls;
    using EDMs.Business.Services.Security;
    using EDMs.Web.Utilities.Enums;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The site master.
    /// </summary>
    public partial class SiteMaster : MasterPage
    {
        #region Fields

        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        private PermissionService _permissionService;
        private MenuService _menuService;
        private UsersLoginHistoryService userloginservice;
        private readonly RoleService roleService = new RoleService();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the role id on session.
        /// </summary>
        /// <value>
        /// The role id.
        /// </value>
        public int RoleId
        {
            get { return UserSession.Current.User.RoleId != null ? UserSession.Current.User.RoleId.Value : -1; }
        }

        #endregion

        #region Methods

        public SiteMaster()
        {
            _permissionService = new PermissionService();
            _menuService = new MenuService();
            userloginservice = new UsersLoginHistoryService();
        }

        #endregion

        #region Events

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!UserSession.Current.IsAvailable)
            {
                if (Request.RawUrl != "/Controls/System/Login.aspx")
                {
                    Session.Add("ReturnURL", Request.RawUrl);
                }
                Response.Redirect("~/Controls/System/Login.aspx");
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        /// <summary>
        /// Handles the PreLoad event of the master_Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.InvalidOperationException">Validation of Anti-XSRF token failed.</exception>
        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? string.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? string.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (UserSession.Current.IsAvailable)
                {
                    var lblFullName = this.menu.Items[0].FindControl("lblFullName") as Label;
                    var lblCurrentUserOnline = this.menu.Items[0].FindControl("lblCurrentUserOnline") as Label;

                    if (lblFullName != null)
                    {
                        lblFullName.Text = "Welcome, " + UserSession.Current.User.FullName + ".";
                    }

                    if (lblCurrentUserOnline != null)
                    {
                        lblCurrentUserOnline.Text = "Current license online: " + Application.Get("userLoginCount");
                    }

                    LoadLeftMenu();
                    LoadMainMenu();
                }
            }
        }

        /// <summary>
        /// Handles the OnItemClick event of the menu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadMenuEventArgs"/> instance containing the event data.</param>
        protected void menu_OnItemClick(object sender, RadMenuEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "LogoutCommand":
                    UserLogOut(UserSession.Current.LogId);
                    Session.Clear();
                    Session.Abandon();
                    FormsAuthentication.SignOut();

                    UserSession.DestroySession();
                    Response.Redirect("~/Controls/System/Login.aspx");
                    break;
            }
        }
        private void UserLogOut(int logId)
        {
            var userlogin = userloginservice.GetByID(logId);
            if (userlogin != null)
            {
                var curenttime = DateTime.Now;
                var duretime = curenttime.TimeOfDay - userlogin.ServerTime.Value.TimeOfDay;
                userlogin.LogoutLocalTime = userlogin.LocalTime.Value.Add(duretime);
                userlogin.DurationTimeLogin = duretime.ToString();
                userlogin.IsOn = false;
                userloginservice.Update(userlogin);
            }
        }
        #endregion

        #region Helpers

        /// <summary>
        /// Loads the main menu.
        /// </summary>
        private void LoadMainMenu()
        {
            var roleId = 0;
            if (this.roleService.IsNipi(UserSession.Current.RoleId))
            {
                if (UserSession.Current.IsAdmin)
                {
                    roleId = 1;
                }
                else if (UserSession.Current.IsDC)
                {
                    roleId = 2;
                }
                else if (UserSession.Current.IsSuperViewer)
                {
                    roleId = 3;
                }
                else if (UserSession.Current.IsGip)
                {
                    roleId = 4;
                }
                else if (UserSession.Current.IsCheif)
                {
                    roleId = 5;
                }
                else if (UserSession.Current.IsEngineer)
                {
                    roleId = 6;
                }
                else if (UserSession.Current.IsLeader)
                {
                    roleId = 7;
                }
            }
            else
            {
                roleId = 8;
            }

            var menus = this._menuService.GetAllRelatedPermittedMenuItems(roleId, (int)MenuType.TopMenu).Where(t => t.Active == true).OrderBy(t => t.Priority).ToList();

            //list user role gip and chief
            List<int> userSpecial = System.Configuration.ConfigurationManager.AppSettings.Get("ChiefandGIP").Split(',').Select(Int32.Parse).ToList();

            //5 user 2in1 role chief and GIP
            if (userSpecial.Contains(UserSession.Current.User.Id))
            {
                var mn = new EDMs.Data.Entities.Menu();
                roleId = 4;
                mn.Id = 104;
                mn.ParentId = 95;
                mn.Description = "Perform";
                mn.Url = "ManhourPercentReport.aspx";
                mn.Icon = "~/Images/report3.png";
                menus = this._menuService.GetAllRelatedPermittedMenuItems(roleId, (int)MenuType.TopMenu).Where(t => t.Active == true).OrderBy(t => t.Priority).ToList();
                menus.Add(mn);
            }

            this.MainMenu.DataSource = menus;
            this.MainMenu.DataTextField = "Description";
            this.MainMenu.DataNavigateUrlField = "Url";
            this.MainMenu.DataFieldID = "Id";
            this.MainMenu.DataValueField = "Id";
            this.MainMenu.DataFieldParentID = "ParentId";

            this.MainMenu.DataBind();

            this.SetIcon(this.MainMenu.Items, menus);

            ////foreach (RadMenuItem menuItem in MainMenu.Items)
            ////{
            ////    if (!string.IsNullOrEmpty(menuItem.Value))
            ////    {
            ////        var tempMenu = menus.FirstOrDefault(t => t.Id == Convert.ToInt32(menuItem.Value));
            ////        if (tempMenu != null)
            ////        {
            ////            menuItem.ImageUrl = tempMenu.Icon;
            ////        }
            ////    }
            ////}
        }

        private void SetIcon(RadMenuItemCollection menu, List<EDMs.Data.Entities.Menu> menuData)
        {
            foreach (RadMenuItem menuItem in menu)
            {
                if (!string.IsNullOrEmpty(menuItem.Value))
                {
                    var tempMenu = menuData.FirstOrDefault(t => t.Id == Convert.ToInt32(menuItem.Value));
                    if (tempMenu != null)
                    {
                        menuItem.ImageUrl = tempMenu.Icon;
                    }
                }

                if (menuItem.Items.Count > 0)
                {
                    this.SetIcon(menuItem.Items, menuData);
                }
            }
        }

        /// <summary>
        /// Loads the left menu.
        /// </summary>
        private void LoadLeftMenu()
        {
            var menus = _menuService.GetAllRelatedPermittedMenuItems(RoleId, (int)MenuType.LeftMenu);
            if (menus == null)
            {
                //bottomLeftPane.Collapsed = true; 
                return;
            }

            //Gets root parents only
            menus = menus.Where(x => x.ParentId == null).ToList();

            if (menus.Count == 0)
            {
                //bottomLeftPane.Collapsed = true;
                return;
            }
            //LeftMenu.DataSource = menus;
            //LeftMenu.DataTextField = "Description";
            //LeftMenu.DataNavigateUrlField = "Url";
            //LeftMenu.DataFieldID = "Id";
            //LeftMenu.DataBind();
        }

        #endregion
    }
}

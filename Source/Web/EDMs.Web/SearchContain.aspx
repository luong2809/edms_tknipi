﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchContain.aspx.cs" Inherits="EDMs.Web.SearchContain" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Avanced search</title>
    <link href="Scripts/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery.min.js"></script>
    <script src="Scripts/ElasticSearch/elasticsearch.jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <input id='txtKeyword' class="form-control" />
                </div>
                <div class="col-sm-2 col-md-2 col-xs-6">
                    <button id='btnSearch' type="button" class="btn btn-primary">Tìm kiếm</button>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                    <div id="lbResult1"></div>
                    <span id="lbResult"></span>
                </div>
            </div>

            <script>
                var input = document.getElementById("txtKeyword");

                // Thêm sự kiện cho đối tượng
                input.addEventListener('keyup', function () {
                    // Gán giá trị vào div
                    document.getElementById('lbResult1').innerHTML = input.value;
                    if (input.value.length > 5) {
                        alert("Bạn đã nhập nhiều hơn 5 ký tự");
                    }
                });

                $(document).ready(function () {
                    $('#btnSearch').click(function () {
                        var json = { "query": { "match": { "content": $('#txtKeyword').val() } } };
                        //$.ajax({
                        //    url: 'http://localhost:9200/edms_tknipi/_search',
                        //    type: 'POST',
                        //    data: JSON.stringify(json),
                        //    contentType: 'application/json; charset=utf-8',
                        //    dataType: 'json',
                        //    async: true,
                        //    success: function (msg) {
                        //        // msg chứa chuỗi json
                        //        //  $('#lbResult').html($.parseJSON(msg));
                        //        console.log(msg);
                        //        var list = msg.hits.hits;
                        //        if (list.length > 0) {
                        //            $('#lbResult').html('');
                        //            for (i = 0; i < list.length; i++) {
                        //                var source = list[i]._source;
                        //                var file = '<a href="SearchContain.aspx?path=' + source.path.virtual + '&type=' + source.file.content_type + '" >' + source.file.filename + '</a>';
                        //                $('#lbResult').html($('#lbResult').html() + file + '<br/>');
                        //            }
                        //        }
                        //        else {
                        //            $('#lbResult').html('Không tìm thấy kết quả');
                        //        }
                        //        var i;

                        //        // $('#lbResult').text(files);
                        //    }
                        //});
                        var dt = { "val": 1 };
                        $.ajax({
                            url: 'http://localhost:8003/SearchContain.aspx',
                            type: 'POST',
                            data: dt,
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            async: false,
                            success: function (msg) {
                            }
                        });
                        //var d = { "id": 1 };
                        //$.ajax({
                        //    url: 'http://localhost:8003/SearchContain.aspx',
                        //    type: 'POST',
                        //    data: JSON.stringify(d),
                        //    contentType: 'application/json; charset=utf-8',
                        //    dataType: 'json',
                        //    async: true,
                        //    success: function (msg) {
                        //        alert(msg + "1");
                        //    }
                        //});
                    });
                });
            </script>
        </div>
    </form>
</body>
</html>

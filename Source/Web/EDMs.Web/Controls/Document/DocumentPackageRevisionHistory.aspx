﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentPackageRevisionHistory.aspx.cs" Inherits="EDMs.Web.Controls.Document.DocumentPackageRevisionHistory" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript">

        function CloseAndRefreshGrid() {
            var oWin = GetRadWindow();
            var parentWindow = oWin.BrowserWindow;
            $(oWin).ready(function () {
                oWin.close();
            });
            parentWindow.refreshGrid();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including classic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }


    </script>
    <style type="text/css">
        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 1px !important;
        }

        /*Hide change page size control*/
        div.RadGrid .rgPager .rgAdvPart {
            display: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content" style="height: 600px !important;">
            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
            <telerik:RadGrid AllowCustomPaging="False" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" GridLines="None"
                ID="grdDocument" Skin="Windows7" Height="550px"
                OnNeedDataSource="grdDocument_OnNeedDataSource"
                OnDetailTableDataBind="grdDocument_DetailTableDataBind"
                PageSize="100" runat="server" Width="100%">
                <SortingSettings SortedBackColor="#FFF6D6"></SortingSettings>
                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                <MasterTableView AllowMultiColumnSorting="false"
                    ClientDataKeyNames="ID" DataKeyNames="ID" Font-Size="8pt">

                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ColumnGroups>
                        <telerik:GridColumnGroup HeaderText="REVISION DETAILS" Name="RevisionDetails"
                            HeaderStyle-HorizontalAlign="Center" />
                        <telerik:GridColumnGroup HeaderText="OUTGOING TRANSMITTAL" Name="OutgoingTrans"
                            HeaderStyle-HorizontalAlign="Center" />
                        <telerik:GridColumnGroup HeaderText="TRANSMITTAL HARD COPY" Name="OutgoingTransXDCB"
                            HeaderStyle-HorizontalAlign="Center" />
                        <telerik:GridColumnGroup HeaderText="INCOMING TRANSMITTAL" Name="IncomingTrans"
                            HeaderStyle-HorizontalAlign="Center" />
                        <telerik:GridColumnGroup HeaderText="ICA REVIEW DETAILS" Name="ICAReviews"
                            HeaderStyle-HorizontalAlign="Center" />
                    </ColumnGroups>

                    <Columns>
                        <telerik:GridBoundColumn DataField="HasAttachFile" UniqueName="HasAttachFile" Display="False" />
                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                        <telerik:GridTemplateColumn HeaderText="No." Groupable="False">
                            <HeaderStyle HorizontalAlign="Center" Width="2%" VerticalAlign="Middle"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="2%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblSoTT" runat="server" Text='<%# grdDocument.CurrentPageIndex * grdDocument.PageSize + grdDocument.Items.Count+1 %>'>
                                </asp:Label>

                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="EditColumn">
                            <HeaderStyle Width="30" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <a href='javascript:ShowEditForm(<%# DataBinder.Eval(Container.DataItem, "ID")%>,<%#DataBinder.Eval(Container.DataItem, "ProjectId")%>,<%#
                                                        DataBinder.Eval(Container.DataItem, "WorkgroupId")%>)'
                                    style="text-decoration: none; color: blue">
                                    <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer;" AlternateText="Edit properties" />
                                    <a />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Workpackage" UniqueName="WorkgroupName"
                            ShowFilterIcon="False" FilterControlWidth="97%" DataField="WorkgroupName"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                            <HeaderStyle HorizontalAlign="Center" Width="120" />
                            <ItemStyle HorizontalAlign="Center" Width="120" />
                            <ItemTemplate>
                                <%# Eval("WorkgroupName") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="WorkgroupName" runat="server"
                                    Value='<%# Eval("WorkgroupName") %>' />
                                <asp:Label runat="server" ID="lblWorkgroupName"></asp:Label>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="DOC. No." UniqueName="DocNo"
                            DataField="DocNo" ShowFilterIcon="False" FilterControlWidth="97%"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                            <HeaderStyle HorizontalAlign="Center" Width="150" />
                            <ItemStyle HorizontalAlign="Left" Width="150" />
                            <ItemTemplate>
                                <%# Eval("DocNo") %>
                                <%--<telerik:RadToolTip Skin="Simple" runat="server" ID="dirNameToolTip" RelativeTo="Element" 
                                            AutoCloseDelay="10000" ShowDelay="0" Position="BottomRight"
                                            Width="400px" Height="150px" HideEvent="LeaveTargetAndToolTip"  TargetControlID="lblDocNo" IsClientID="False"
                                            Animation="Fade" Text='<%# "<b >Transmittals Information </b> <br/>" 
                                                                    + "Incoming trans no.: " + Eval("IncomingTransNo") + "<br/>"
                                                                    + "Date: " + Eval("IncomingTransDate","{0:dd/MM/yyyy}") + "<hr/>" + "<br/>"
                                                                    + "Outgoing trans no.: " + Eval("OutgoingTransNo") + "<br/>"
                                                                    + "Date: " + Eval("OutgoingTransDate","{0:dd/MM/yyyy}") + "<hr/>" + "<br/>"
                                                                    + "ICA review in/out trans no.: " + Eval("ICAReviewOutTransNo") + "<br/>"
                                                                    + "Date: " + Eval("ICAReviewOutDate","{0:dd/MM/yyyy}") + "<br/>"
                                                                    + "Review code: " + Eval("ICAReviewCode","{0:dd/MM/yyyy}") + "<br/>"%>'>
                                         </telerik:RadToolTip>--%>
                            </ItemTemplate>

                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="DOC. Title" UniqueName="DocTitle"
                            DataField="DocTitle" ShowFilterIcon="False" FilterControlWidth="97%"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                            <HeaderStyle HorizontalAlign="Center" Width="220" />
                            <ItemStyle HorizontalAlign="Left" Width="220" />
                            <ItemTemplate>
                                <%# Eval("DocTitle") %>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Rev." UniqueName="Rev" AllowFiltering="false">
                            <HeaderStyle HorizontalAlign="Center" Width="50" />
                            <ItemStyle HorizontalAlign="Center" Width="50" />
                            <ItemTemplate>
                                <%# Eval("RevisionName") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="RevisionId" runat="server"
                                    Value='<%# Eval("RevisionId") %>' />
                                <telerik:RadComboBox ID="ddlRevision" runat="server"
                                    DropDownWidth="70px" MaxHeight="300px" Width="98%" />
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Man-Hour Planed" UniqueName="ManHourPlan" AllowFiltering="false">
                            <HeaderStyle HorizontalAlign="Center" Width="65" />
                            <ItemStyle HorizontalAlign="Center" Width="65" />
                            <ItemTemplate>
                                <%# Eval("ManHourPlan") != null && Eval("ManHourPlan").ToString() != "0" ? Eval("ManHourPlan") : string.Empty%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Total Real Man-Hour" UniqueName="ManHours" AllowFiltering="false">
                            <HeaderStyle HorizontalAlign="Center" Width="65" />
                            <ItemStyle HorizontalAlign="Center" Width="65" />
                            <ItemTemplate>
                                <%# Eval("ManHours") != null && Eval("ManHours").ToString() != "0" ? Eval("ManHours") : string.Empty%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Start" UniqueName="StartDate" AllowFiltering="false">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Center" Width="80" />
                            <ItemTemplate>
                                <%# Eval("StartDate","{0:dd/MM/yyyy}") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="StartDate" runat="server" Value='<%# Eval("StartDate") %>' />
                                <telerik:RadDatePicker ID="txtStartDate" Width="98%"
                                    runat="server" Skin="Windows7" ShowPopupOnFocus="True"
                                    PopupDirection="BottomRight">
                                    <DateInput ID="txtStartDateInput" runat="server"
                                        DateFormat="dd/MM/yyyy" ShowButton="False" />
                                </telerik:RadDatePicker>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Deadline" UniqueName="Deadline" AllowFiltering="false">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Center" Width="80" />
                            <ItemTemplate>
                                <%# Eval("Deadline","{0:dd/MM/yyyy}") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="Deadline" runat="server" Value='<%# Eval("Deadline") %>' />
                                <telerik:RadDatePicker ID="txtDeadline" Width="98%"
                                    runat="server" Skin="Windows7" ShowPopupOnFocus="True"
                                    PopupDirection="BottomRight">
                                    <DateInput ID="txtDeadlineInput" runat="server"
                                        DateFormat="dd/MM/yyyy" ShowButton="False" />
                                </telerik:RadDatePicker>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Finish" UniqueName="EndDate" AllowFiltering="false">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Center" Width="80" />
                            <ItemTemplate>
                                <%# Eval("EndDate","{0:dd/MM/yyyy}") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="EndDate" runat="server" Value='<%# Eval("EndDate") %>' />
                                <telerik:RadDatePicker ID="txtEndDate" Width="98%"
                                    runat="server" Skin="Windows7" ShowPopupOnFocus="True"
                                    PopupDirection="BottomRight">
                                    <DateInput ID="txtEndDateInput" runat="server"
                                        DateFormat="dd/MM/yyyy" ShowButton="False" />
                                </telerik:RadDatePicker>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Iso Revise Date" UniqueName="IsoReviseDate" AllowFiltering="false">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Center" Width="80" />
                            <ItemTemplate>
                                <%# Eval("IsoReviseDate","{0:dd/MM/yyyy}") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="IsoReviseDate" runat="server" Value='<%# Eval("IsoReviseDate") %>' Visible="false" />
                                <telerik:RadDatePicker ID="txtIsoReviseDate" Width="98%"
                                    runat="server" Skin="Windows7" ShowPopupOnFocus="True"
                                    PopupDirection="BottomRight">
                                    <DateInput ID="txtIsoReviseDateInput" runat="server"
                                        DateFormat="dd/MM/yyyy" ShowButton="False" />
                                </telerik:RadDatePicker>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Weight" UniqueName="Weight" AllowFiltering="false">
                            <HeaderStyle HorizontalAlign="Center" Width="65" />
                            <ItemStyle HorizontalAlign="Center" Width="65" />
                            <ItemTemplate>
                                <%# Eval("Weight") + "%"%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="Weight" runat="server" Value='<%# Eval("Weight") %>' />
                                <telerik:RadNumericTextBox Type="Percent" ID="txtWeight"
                                    runat="server" Width="98%">
                                    <NumberFormat DecimalDigits="2"></NumberFormat>
                                </telerik:RadNumericTextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Complete" UniqueName="Complete" AllowFiltering="false" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" Width="65" />
                            <ItemStyle HorizontalAlign="Center" Width="65" />
                            <ItemTemplate>
                                <%# Eval("Complete") + "%"%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="Complete" runat="server" Value='<%# Eval("Complete") %>' />
                                <telerik:RadNumericTextBox Type="Percent" ID="txtComplete"
                                    runat="server" Width="98%">
                                    <NumberFormat DecimalDigits="2"></NumberFormat>
                                </telerik:RadNumericTextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="OutgoingTransNo" HeaderText="No." UniqueName="OutgoingTransNo" ShowFilterIcon="false" ColumnGroupName="OutgoingTrans" Display="True" FilterControlWidth="97%"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                            <HeaderStyle HorizontalAlign="Center" Width="100" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="OutgoingTransDate" HeaderText="Date" UniqueName="OutgoingTransDate"
                            ColumnGroupName="OutgoingTrans" AllowFiltering="false" Display="True" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="OutgoingLeterNo" HeaderText="No." UniqueName="OutgoingLeterNo" ShowFilterIcon="false" ColumnGroupName="OutgoingTransXDCB" Display="True" FilterControlWidth="97%"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                            <HeaderStyle HorizontalAlign="Center" Width="100" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="OutgoingLeterDate" UniqueName="OutgoingLeterDate" HeaderText="Date"
                            ColumnGroupName="OutgoingTransXDCB" AllowFiltering="false" Display="True" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <DetailTables>
                        <telerik:GridTableView DataKeyNames="ID" Name="DocDetail" Width="100%"
                            AllowPaging="True" PageSize="10">
                            <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                            <Columns>
                                <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                    <HeaderStyle Width="3%" />
                                    <ItemStyle HorizontalAlign="Center" Width="3%" />
                                    <ItemTemplate>
                                        <a href='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>'
                                            download='<%# DataBinder.Eval(Container.DataItem, "FileName") %>' target="_blank">
                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ExtensionIcon") %>'
                                                Style="cursor: pointer;" ToolTip="Download document" />
                                        </a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="FileName" HeaderText="File name" UniqueName="FileName">
                                    <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="Description" HeaderText="Notes" UniqueName="Description" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="CmtResFrom" HeaderText="From" UniqueName="CmtResFrom" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="CmtResTo" HeaderText="To" UniqueName="CmtResTo" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="AttachTypeName" HeaderText="Type" UniqueName="AttachTypeName" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" Width="9%" />
                                    <ItemStyle HorizontalAlign="Left" Width="9%" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="CreatedByUser" HeaderText="Upload by" UniqueName="CreatedByUser">
                                    <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Upload time" UniqueName="CreatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm tt}">
                                    <HeaderStyle HorizontalAlign="Center" Width="16%" />
                                    <ItemStyle HorizontalAlign="Left" Width="16%" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </telerik:GridTableView>
                    </DetailTables>
                </MasterTableView>
                <ClientSettings Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="true" AllowColumnHide="True">
                    <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True" ClipCellContentOnResize="false"></Resizing>
                    <Selecting AllowRowSelect="true" />
                    <ClientEvents OnGridCreated="GetGridObject" />
                    <ClientEvents OnRowContextMenu="RowContextMenu" OnRowClick="RowClick"></ClientEvents>
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                </ClientSettings>
            </telerik:RadGrid>
        </div>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <span style="display: none">

            <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
                <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>

        </span>
        <telerik:RadContextMenu ID="radMenu" runat="server"
            EnableRoundedCorners="true" EnableShadows="true" OnClientItemClicking="gridMenuClicking">
            <Items>
                <telerik:RadMenuItem Text="" Value="AttachDocument" />

                <telerik:RadMenuItem Text="Attach document" ImageUrl="~/Images/attach.png" Value="AttachDocument" Visible="false" />
                <telerik:RadMenuItem Text="Attach comment/response" ImageUrl="~/Images/comment.png" Value="AttachComment" />

                <telerik:RadMenuItem IsSeparator="True" />
                <telerik:RadMenuItem Text="" Value="AttachDocument" />
                <telerik:RadMenuItem Text="Transmittals" ImageUrl="~/Images/transmittal.png" Value="Transmittals" Visible="False" />
            </Items>
        </telerik:RadContextMenu>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
            <Windows>
                <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Document Information"
                    VisibleStatusbar="false" Height="540" Width="650"
                    Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
                </telerik:RadWindow>
                <telerik:RadWindow ID="AttachDoc" runat="server" Title="Attach document files"
                    VisibleStatusbar="false" Height="500" Width="500" MinHeight="500" MinWidth="500" MaxHeight="500" MaxWidth="500"
                    Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
                </telerik:RadWindow>

                <telerik:RadWindow ID="AttachComment" runat="server" Title="Attach Comment/Response"
                    VisibleStatusbar="false" Height="500" Width="900" MinHeight="500" MinWidth="900" MaxHeight="500" MaxWidth="900"
                    Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
        <asp:HiddenField runat="server" ID="lblDocId" />
        <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
        <telerik:RadCodeBlock runat="server">
            <script type="text/javascript">
                var radDocuments;
                function RowDblClick(sender, eventArgs) {
                    sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
                }

                var ajaxManager;

                function pageLoad() {
                    ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
                }

                function onRequestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("btnDownloadPackage") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                function ShowEditForm(id, projectId, packageId) {

                    var owd = $find("<%=CustomerDialog.ClientID %>");
                    owd.Show();
                    owd.setUrl("../../Controls/Document/DocumentPackageInfoEditForm.aspx?docId=" + id + "&projectId=" + projectId + "&wgId=" + packageId + "&EditRev=True", "CustomerDialog");
                }
                function ShowUploadForm(id) {
                    var owd = $find("<%=AttachDoc.ClientID %>");
                    owd.Show();
                    owd.setUrl("UploadDragDrop.aspx?docId=" + id, "AttachDoc");
                    // window.parent.radopen("Controls/Document/DocumentInfoEditForm.aspx?docId=" + id, "DocDialog");
                    // return false;
                }

                function ShowInsertForm() {
                    window.radopen("Controls/Customers/CustomerEditForm.aspx", "DocDialog");
                    return false;
                }

                function refreshGrid(arg) {
                    //alert(arg);
                    if (!arg) {
                        ajaxManager.ajaxRequest("Rebind");
                    }
                    else {
                        ajaxManager.ajaxRequest("RebindAndNavigate");
                    }
                }
                function GetGridObject(sender, eventArgs) {
                    radDocuments = sender;
                }
                function gridMenuClicking(sender, args) {
                    var itemValue = args.get_item().get_value();
                    var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                    switch (itemValue) {
                        case "AttachComment":
                            var owd = $find("<%=AttachComment.ClientID %>");
                            owd.Show();
                            owd.setUrl("../../Controls/Document/AttachComment.aspx?docId=" + docId, "AttachComment");
                            break;
                    }
                }

                function RowClick(sender, eventArgs) {
                    var Id = eventArgs.getDataKeyValue("ID");
                    document.getElementById("<%= lblDocId.ClientID %>").value = Id;
                }
            </script>
            <script type="text/javascript">
                function RowContextMenu(sender, eventArgs) {
                    var menu = $find("<%=radMenu.ClientID %>");
                    var evt = eventArgs.get_domEvent();

                    if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                        return;
                    }
                    var index = eventArgs.get_itemIndexHierarchical();
                    document.getElementById("radGridClickedRowIndex").value = index;
                    var Id = eventArgs.getDataKeyValue("ID");
                    document.getElementById("<%= lblDocId.ClientID %>").value = Id;

                    sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);

                    menu.show(evt);

                    evt.cancelBubble = true;
                    evt.returnValue = false;

                    if (evt.stopPropagation) {
                        evt.stopPropagation();
                        evt.preventDefault();
                    }
                }

            </script>
        </telerik:RadCodeBlock>
    </form>
</body>
</html>

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Library
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class AppendixEditForm : Page
    {
        private readonly AppendixService appendixService = new AppendixService();
        private readonly UserService userService = new UserService();
        private readonly ProjectAppendixService projectAppendixService = new ProjectAppendixService();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public AppendixEditForm()
        {
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["id"]))
                {
                    int id = Convert.ToInt32(this.Request.QueryString["id"]);
                    this.CreatedInfo.Visible = true;
                    var obj = this.appendixService.GetById(id);
                    if (obj != null)
                    {
                        this.ddlYear.SelectedValue = obj.Year.ToString();
                        int work = Convert.ToInt32(this.Request.QueryString["work"]);
                        var appendixList = this.appendixService.GetByWorkWithoutID(work, Convert.ToInt32(ddlYear.SelectedItem.Text), id);
                        appendixList.Insert(0, new Appendix() { ID = 0, Name = string.Empty });
                        this.ddlAppendix.DataSource = appendixList;
                        this.ddlAppendix.DataTextField = "FullName";
                        this.ddlAppendix.DataValueField = "ID";
                        this.ddlAppendix.DataBind();
                        this.ddlAppendix.SelectedIndex = 0;
                        var child = this.appendixService.GetByParent(obj.ID);
                        this.txtCode.Text = obj.Code;
                        this.txtAlias.Text = obj.AliasCode;
                        this.txtName.Text = obj.Name;
                        
                        this.ddlAppendix.SelectedValue = obj.ParentID.GetValueOrDefault().ToString();
                        this.cbActive.Checked = obj.Active.GetValueOrDefault();
                        this.cbMultipleProject.Checked = obj.MultipleProject.GetValueOrDefault();
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);
                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    int work = Convert.ToInt32(this.Request.QueryString["work"]);
                    var appendixList = this.appendixService.GetByWorkWithoutActive(work);
                    appendixList.Insert(0, new Appendix() { ID = 0, Name = string.Empty });
                    this.ddlAppendix.DataSource = appendixList;
                    this.ddlAppendix.DataTextField = "FullName";
                    this.ddlAppendix.DataValueField = "ID";
                    this.ddlAppendix.DataBind();
                    this.ddlAppendix.SelectedIndex = 0;
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        private void ExpandParentNode(RadTreeNode node)
        {
            if (node.ParentNode != null)
            {
                node.ParentNode.Expanded = true;
                this.ExpandParentNode(node.ParentNode);
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["work"]))
                {
                    var workID = Convert.ToInt32(this.Request.QueryString["work"]);
                    if (!string.IsNullOrEmpty(this.Request.QueryString["id"]))
                    {
                        var id = Convert.ToInt32(this.Request.QueryString["id"]);
                        var obj = this.appendixService.GetById(id);
                        if (obj != null)
                        {
                            obj.WorkID = workID;
                            obj.Code = this.txtCode.Text.Trim();
                            obj.AliasCode = this.txtAlias.Text.Trim();
                            obj.Name = this.txtName.Text.Trim();
                            obj.Year = Convert.ToInt32(this.ddlYear.SelectedValue);
                            if (this.ddlAppendix.SelectedValue != "0")
                            {
                                obj.ParentID = Convert.ToInt32(this.ddlAppendix.SelectedValue);
                            }
                            else
                            {
                                obj.ParentID = null;
                            }
                            obj.Active = cbActive.Checked;
                            obj.MultipleProject = cbMultipleProject.Checked;
                            obj.LastUpdatedBy = UserSession.Current.User.Id;
                            obj.LastUpdatedDate = DateTime.Now;
                            this.appendixService.Update(obj);
                        }
                    }
                    else
                    {
                        Appendix obj = new Appendix();
                        obj.WorkID = workID;
                        obj.Code = this.txtCode.Text.Trim();
                        obj.AliasCode = this.txtAlias.Text.Trim();
                        obj.Name = this.txtName.Text.Trim();
                        obj.Year = Convert.ToInt32(this.ddlYear.SelectedValue);
                        if (this.ddlAppendix.SelectedValue != "0")
                        {
                            obj.ParentID = Convert.ToInt32(this.ddlAppendix.SelectedValue);
                        }
                        else
                        {
                            obj.ParentID = null;
                        }
                        obj.Active = cbActive.Checked;
                        obj.MultipleProject = cbMultipleProject.Checked;
                        obj.CreatedBy = UserSession.Current.User.Id;
                        obj.CreatedDate = DateTime.Now;
                        this.appendixService.Insert(obj);
                    }
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }


        private void LoadComboData()
        {
            for (var i = DateTime.Now.Year + 5; i >= 2017; i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }

        protected void vldAlias_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtAlias.Text.Trim().Length == 0)
            {
                this.vldAlias.ErrorMessage = "Please enter Alias Code.";
                this.divAlias.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        protected void vldCode_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtCode.Text.Trim().Length == 0)
            {
                this.vldCode.ErrorMessage = "Please enter Code.";
                this.divCode.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        protected void grdAppendix_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["id"]))
            {
                var id = Convert.ToInt32(this.Request.QueryString["id"]);
                var projectAppendixList = projectAppendixService.GetAllByAppendix(id);
                grdAppendix.DataSource = projectAppendixList;
            }
        }

        protected void grdAppendix_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var disId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.projectAppendixService.Delete(disId);

            this.grdAppendix.Rebind();
        }
    }
}
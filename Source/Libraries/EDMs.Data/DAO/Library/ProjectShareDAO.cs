﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProjectShareDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Library
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class ProjectShareDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectShareDAO"/> class.
        /// </summary>
        public ProjectShareDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<ProjectShare> GetIQueryable()
        {
            return this.EDMsDataContext.ProjectShares;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ProjectShare> GetAll()
        {
            return this.EDMsDataContext.ProjectShares.Where(t => t.Active == true).ToList();
        }

        public List<ProjectShare> GetAllView()
        {
            return this.EDMsDataContext.ProjectShares.ToList();
        }

        public List<ProjectShare> GetByGroup(int groupID)
        {
            return this.EDMsDataContext.ProjectShares.Where(t => t.GroupID == groupID && t.Active == true).ToList();
        }

        public bool IsExist(int workID, int groupID)
        {
            return this.EDMsDataContext.ProjectShares.Any(ob => ob.WorkID == workID && ob.GroupID == groupID);
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public ProjectShare GetById(int id)
        {
            return this.EDMsDataContext.ProjectShares.FirstOrDefault(ob => ob.ID == id && ob.Active == true);
        }

        #endregion

        #region GET ADVANCE
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(ProjectShare ob)
        {
            try
            {
                this.EDMsDataContext.AddToProjectShares(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(ProjectShare src)
        {
            try
            {
                var des = (from rs in this.EDMsDataContext.ProjectShares
                           where rs.ID == src.ID
                           select rs).First();

                des.WorkID = src.WorkID;
                des.WorkName = src.WorkName;
                des.GroupID = src.GroupID;
                des.GroupName = src.GroupName;
                des.Active = src.Active;

                des.LastUpdatedBy = src.LastUpdatedBy;
                des.LastUpdatedDate = src.LastUpdatedDate;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(ProjectShare src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

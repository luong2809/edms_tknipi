﻿
namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Scope;

    /// <summary>
    /// The package.
    /// </summary>
    public partial class Milestone
    {
        /// <summary>
        /// Gets the project obj.
        /// </summary>
        public WorkGroup Workpackage
        {
            get
            {
                var wpDAO = new WorkGroupDAO();
                return wpDAO.GetById(this.WorkpackageId.GetValueOrDefault());
            }
        }

        public string WorkpackageType
        {
            get
            {
                var result = string.Empty;
                switch (this.Workpackage.TypeId)
                {
                    case 0:
                        result = "Others";
                        break;
                    case 1:
                        result = "Planned workpackages";
                        break;
                    case 2:
                        result = "Unplanned workpackages";
                        break;
                }
                return result;
            }
        }
    }
}

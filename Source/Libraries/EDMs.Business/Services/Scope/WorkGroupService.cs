﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Business.Services.Library;
using EDMs.Data.DAO.Scope;
using EDMs.Data.Entities;

namespace EDMs.Business.Services.Scope
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class WorkGroupService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly WorkGroupDAO repo;

        private readonly PermissionWorkgroupService permissionWorkGroupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkGroupService"/> class.
        /// </summary>
        public WorkGroupService()
        {
            this.repo = new WorkGroupDAO();
            this.permissionWorkGroupService = new PermissionWorkgroupService();
        }

        #region Get (Advances)

        public bool IsExist(string workpackageName, int workpackageId)
        {
            return this.repo.IsExist(workpackageName, workpackageId);
        }

        /// <summary>
        /// The get all WorkGroup in permission.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="projectId">
        /// The project Id.
        /// </param>
        /// <returns>
        /// The list <see cref="WorkGroup"/>.
        /// </returns>
        public List<WorkGroup> GetAllWorkGroupInPermission(int userId, int projectId)
        {
            var WorkGroupIdInPermission = this.permissionWorkGroupService.GetPackageInPermission(userId);
            return this.repo.GetAllWorkGroupInPermission(userId, projectId, WorkGroupIdInPermission);
        }

        /// <summary>
        /// The get all WorkGroup of project.
        /// </summary>
        /// <param name="projectId">
        /// The project id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<WorkGroup> GetAllWorkGroupOfProject(int projectId)
        {
            return this.repo.GetAllWorkGroupOfProject(projectId);
        }

        public List<WorkGroup> GetAllWorkGroupOfProject(List<int> projectId)
        {
            return this.repo.GetAllWorkGroupOfProject(projectId);
        }

        public WorkGroup GetWorkGroupCostOfProject(int projectId)
        {
            var list = new List<int>() { 83, 102, 110, 134 };
            return this.repo.GetWorkGroupCostOfProject(projectId, list);
        }

        public WorkGroup GetByName(string name)
        {
            return this.repo.GetByName(name);
        }
        public List<WorkGroup> GetAllByName(string name)
        {
            return this.repo.GetAllByName(name);
        }
        public List<WorkGroup> GetAllByTodolist(List<int> wpList)
        {
            return this.repo.GetAllByTodolist(wpList).Where(t => t.IsStop == false).ToList();
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<WorkGroup> GetAll()
        {
            return this.repo.GetAll();
        }

        public List<WorkGroup> GetAllByDepartment(int deparmentId, List<int> prjectInPermission)
        {
            return this.repo.GetAllByDepartment(deparmentId, prjectInPermission);
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public WorkGroup GetById(int id)
        {
            return this.repo.GetById(id);
        }

        public List<WorkGroup> GetAllByDepartment(int departmentId)
        {
            return this.repo.GetAllByDepartment(departmentId);
        }

        public List<WorkGroup> GetAllByDepartment(int departmentId, int projectId)
        {
            return this.repo.GetAllByDepartment(departmentId, projectId);
        }

        public List<WorkGroup> GetAllByDepartment(List<int> departmentId)
        {
            return this.repo.GetAllByDepartment(departmentId).Where(t => t.IsStop == false).ToList();
        }

        public DateTime? GetMaxDeadline(int pjId)
        {
            return this.repo.GetMaxDeadline(pjId);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(WorkGroup bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(WorkGroup bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(WorkGroup bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ScopeProjectList.aspx.cs" Inherits="EDMs.Web.Controls.Scope.ScopeProjectList" EnableViewState="true" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .RadAjaxPanel {
            height: 100% !important;
        }

        .RadGrid .rgSelectedRow {
            background-image: none !important;
            background-color: coral !important;
        }

        a.tooltip {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong {
                line-height: 30px;
            }

            a.tooltip:hover {
                text-decoration: none;
            }

            a.tooltip span {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5 {
            height: 100% !important;
        }

        #divContainerLeft {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper {
            width: 100%;
            height: 100%;
            /*background: transparent url(~/Images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow {
            z-index: 8000 !important;
        }

        .TemplateMenu {
            z-index: 10;
        }

        .rtbItem.rtbTemplate {
            display: inline-flex !important;
        }

        /*.rlbItem
        {
            float:left !important;
        }
        .rlbGroup, .RadListBox
        {
            width:auto !important;
        }*/
    </style>

    <telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="SCOPE" runat="server" Expanded="True" Width="100%">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Projects" ImageUrl="~/Images/project.png" NavigateUrl="~/Controls/Scope/ScopeProjectList.aspx" Selected="True" />
                    <telerik:RadPanelItem runat="server" Text="Workpackages" ImageUrl="~/Images/package.png" NavigateUrl="~/Controls/Scope/WorkGroupList.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>

    <telerik:RadPanelBar ID="radPbList" runat="server" Width="100%" />
    <telerik:RadPanelBar ID="radPbSystem" runat="server" Width="100%" />

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
        <telerik:RadPane ID="RadPane3" runat="server" Height="30px" Scrollable="false" Scrolling="None">
            <telerik:RadToolBar ID="CustomerMenu" runat="server" Width="100%" OnClientButtonClicking="OnClientButtonClicking">
                <Items>
                    <telerik:RadToolBarDropDown runat="server" Text="New" ImageUrl="~/Images/addNew.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Project" Value="1" ImageUrl="~/Images/project.png" />
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                    <telerik:RadToolBarDropDown runat="server" Text="Action" ImageUrl="~/Images/action.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Export master list template" Value="Adminfunc" ImageUrl="~/Images/export.png" Visible="False" />
                            <telerik:RadToolBarButton runat="server" Text="Import master list" Value="Adminfunc" ImageUrl="~/Images/import.png" Visible="False" />
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Value="Adminfunc" Visible="False" />
                            <telerik:RadToolBarButton runat="server" Text="Export edit data file" Value="7" ImageUrl="~/Images/emdrreport.png" Visible="False" />
                            <telerik:RadToolBarButton runat="server" Text="Import edit data file" Value="Adminfunc" ImageUrl="~/Images/upload.png" Visible="False" />
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Value="Adminfunc" Visible="False" />
                            <telerik:RadToolBarButton runat="server" Text="Export F-TK-01 (Vietnamese)" Value="FTK01V" ImageUrl="~/Images/report3.png" />
                            <telerik:RadToolBarButton runat="server" Text="Export F-TK-01 (Russian)" Value="FTK01R" ImageUrl="~/Images/report3.png" />
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                    <telerik:RadToolBarButton runat="server" Value="ShowAll">
                        <ItemTemplate>
                            Filter query: 
                            <asp:DropDownList ID="ddlShow" runat="server" Width="200px"
                                OnSelectedIndexChanged="ddlShow_OnSelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem Text="Show my uncompleted projects" Value="ShowMyUncomplete" style="background-color: gold" />
                                <asp:ListItem Text="Show all uncompleted projects" Value="ShowUncomplete" style="background-color: chartreuse" />
                                <asp:ListItem Text="Show my overdue projects" Value="ShowMyOverDue" style="background-color: gold" />
                                <asp:ListItem Text="Show all overdue projects" Value="ShowOverDue" style="background-color: chartreuse" />
                                <asp:ListItem Text="Show my stopped projects" Value="ShowMyStop" style="background-color: gold" />
                                <asp:ListItem Text="Show all stopped projects" Value="ShowStop" style="background-color: chartreuse" />
                                <asp:ListItem Text="Show all projects" Value="ShowAll" Selected="True" />
                            </asp:DropDownList>
                            <%-- <asp:RadioButtonList runat="server" RepeatLayout="Table" RepeatDirection="Horizontal" ID="cblShow" OnSelectedIndexChanged="cblShow_OnSelectedIndexChanged" AutoPostBack="True" >
                                <asp:ListItem Text="Show overdue project | " Value="ShowOverDue"/>
                                <asp:ListItem Text="Show uncomplete project | " Value="ShowUncomplete"/>
                                <asp:ListItem Text="Show all project" Value="ShowAll"/>
                            </asp:RadioButtonList>--%>
                            <%--<asp:CheckBox ID="ckbShowLater" runat="server" Text="Show overdue project | " AutoPostBack="True" OnCheckedChanged="ckbShowLater_CheckedChange" ForeColor="Crimson"/>
                            <asp:CheckBox ID="ckbShowUncomplete" runat="server" Text="Show uncomplete project | " AutoPostBack="True" OnCheckedChanged="ckbShowUncomplete_CheckedChange" Checked="True"/>
                            <asp:CheckBox ID="ckbShowAll" runat="server" Text="Show all project" AutoPostBack="True" OnCheckedChanged="ckbShowAll_CheckedChange"/>--%>
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton Value="toolbarWork">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="min25Percent" Width="150px" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true" />
                            <telerik:RadComboBox ID="rcbWorkList" runat="server" Skin="Windows7" Width="400px"
                                OnClientDropDownOpened="ddlWorkHandler" AutoPostBack="true">
                                <ItemTemplate>
                                    <div id="div4">
                                        <telerik:RadTreeView runat="server" ID="rtvWorkList" Skin="Windows7"
                                            OnClientNodeClicking="rtvWorkNodeClicking" OnNodeClick="rtvWorkList_NodeClick">
                                            <DataBindings>
                                                <telerik:RadTreeNodeBinding Expanded="true"></telerik:RadTreeNodeBinding>
                                            </DataBindings>
                                        </telerik:RadTreeView>
                                    </div>
                                </ItemTemplate>
                                <Items>
                                    <telerik:RadComboBoxItem Text="" />
                                </Items>
                            </telerik:RadComboBox>
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                </Items>
            </telerik:RadToolBar>

        </telerik:RadPane>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter3" runat="server" Orientation="Horizontal">
                <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None">

                    <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical">
                        <telerik:RadPane ID="Radpane6" runat="server" Scrolling="None">
                            <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True"
                                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                                GridLines="None" Height="100%" AllowFilteringByColumn="True" AllowSorting="True"
                                OnItemDataBound="grdDocument_ItemDataBound"
                                OnDeleteCommand="grdDocument_DeleteCommand"
                                OnItemCreated="grdDocument_ItemCreated"
                                OnNeedDataSource="grdDocument_OnNeedDataSource"
                                PageSize="100" Style="outline: none">
                                <SortingSettings SortedBackColor="#FFF6D6"></SortingSettings>
                                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                                <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%" TableLayout="Auto" CssClass="rgMasterTable" CommandItemDisplay="Top">
                                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; items." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                    <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Refresh Data" ShowExportToExcelButton="false" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                        <telerik:GridBoundColumn DataField="IsLater" UniqueName="IsLater" Display="False" />
                                        <telerik:GridBoundColumn DataField="IsStop" UniqueName="IsStop" Display="False" />
                                        <telerik:GridBoundColumn DataField="IsFullPermission" UniqueName="IsFullPermission" Display="False" />

                                        <%--<telerik:GridBoundColumn DataField="IsUsing" UniqueName="IsUsing" Display="False" />--%>
                                        <telerik:GridBoundColumn DataField="CanDelete" UniqueName="CanDelete" Display="False" />

                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="EditColumn">
                                            <HeaderStyle Width="30" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div runat="server" id="EditImage">
                                                    <a href='javascript:ShowEditForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer; border: none" AlternateText="Edit properties" />
                                                        <a />
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                                            <HeaderStyle Width="30" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridButtonColumn>

                                        <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn" HeaderText="Proj. File">
                                            <HeaderStyle Width="35" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div runat="server" visible='<%# DataBinder.Eval(Container.DataItem, "DefaultFilePath") != null && !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "DefaultFilePath").ToString()) %>'>
                                                    <a href='<%# DataBinder.Eval(Container.DataItem, "DefaultFilePath") %>' target="_blank">
                                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtentionIcon") %>'
                                                            Style="cursor: pointer;" ToolTip="Download default project file." />
                                                    </a>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="ViewMDR">
                                            <HeaderStyle Width="30" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div runat="server" id="showemdr">
                                                    <a href='javascript:ShowEMDR(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Image ID="viewmdr" runat="server" ImageUrl="~/Images/emdr.png" Style="cursor: pointer; border: none" AlternateText="View EMDR" ToolTip="View EMDR" />
                                                        <a />
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="ViewSummary">
                                            <HeaderStyle Width="30" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div runat="server" id="showsummary">
                                                    <a href='javascript:ShowSummary(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Image ID="viewsum" runat="server" ImageUrl="~/Images/report.png" Style="cursor: pointer; border: none" AlternateText="View Sum" ToolTip="View Summary" />
                                                        <a />
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="FieldName" HeaderText="Field" UniqueName="FieldName"
                                            ShowFilterIcon="False" FilterControlWidth="65"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="PlatformName" HeaderText="Platform" UniqueName="PlatformName"
                                            ShowFilterIcon="False" FilterControlWidth="65"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="Name" HeaderText="Project Number" UniqueName="Name"
                                            ShowFilterIcon="False" FilterControlWidth="105"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="120" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="Description" HeaderText="Project Name" UniqueName="Description"
                                            ShowFilterIcon="False" FilterControlWidth="235"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="250" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="ScopeOfWork" HeaderText="Scope Of Work" UniqueName="ScopeOfWork"
                                            ShowFilterIcon="False" FilterControlWidth="155"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" Display="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="170" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="ProjectTypeName" HeaderText="Project Type" UniqueName="ProjectTypeName"
                                            ShowFilterIcon="False" FilterControlWidth="65" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="Year" HeaderText="Year" UniqueName="Year"
                                            ShowFilterIcon="False" FilterControlWidth="65" AutoPostBackOnFilter="true" CurrentFilterFunction="EqualTo">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="ProjectManagerFullName" HeaderText="Engineering Manager" UniqueName="ProjectManagerFullName"
                                            ShowFilterIcon="False" FilterControlWidth="185"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="SupervisorFullName" HeaderText="Project Manager" UniqueName="SupervisorFullName"
                                            ShowFilterIcon="False" FilterControlWidth="135"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="150" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="planName" HeaderText="Order Code" UniqueName="planName"
                                            ShowFilterIcon="False" FilterControlWidth="185"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="TransferDocOfProject" HeaderText="Transfer Doc Of Project" UniqueName="TransferDocOfProject"
                                            ShowFilterIcon="False" FilterControlWidth="185"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>

                                        <telerik:GridDateTimeColumn DataField="StartDate" HeaderText="Start Date" UniqueName="StartDate" DataFormatString="{0:dd/MM/yyyy}" SortExpression="StartDate" PickerType="DatePicker" AutoPostBackOnFilter="true"
                                            EnableRangeFiltering="true" FilterControlWidth="95" ShowFilterIcon="true" CurrentFilterFunction="Between">
                                            <HeaderStyle HorizontalAlign="Center" Width="280" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridDateTimeColumn>

                                        <telerik:GridDateTimeColumn DataField="Deadline" HeaderText="Deadline" UniqueName="Deadline" DataFormatString="{0:dd/MM/yyyy}" SortExpression="StartDate" PickerType="DatePicker" AutoPostBackOnFilter="true"
                                            EnableRangeFiltering="true" FilterControlWidth="95" ShowFilterIcon="true" CurrentFilterFunction="Between">
                                            <HeaderStyle HorizontalAlign="Center" Width="280" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridDateTimeColumn>

                                        <telerik:GridTemplateColumn HeaderText="Complete" UniqueName="Complete"
                                            DataField="Complete" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="50"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="LessThanOrEqualTo"
                                            SortExpression="Complete">
                                            <HeaderStyle HorizontalAlign="Center" Width="90" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblComplete" runat="server" ForeColor='<%# Convert.ToDouble(Eval("Complete")) == 0 ? Color.Red : Color.Black %>'
                                                    Text='<%# Eval("Complete") != null ? Eval("Complete") + "%" : string.Empty%>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn HeaderText="Total Workpackage Weight" UniqueName="TotalWorkpackageWeight"
                                            DataField="TotalWorkpackageWeight" AllowFiltering="false" ShowFilterIcon="true" FilterControlWidth="50"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="LessThanOrEqualTo"
                                            SortExpression="TotalWorkpackageWeight">
                                            <HeaderStyle HorizontalAlign="Center" Width="90" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblWeight" runat="server" ForeColor='<%# Convert.ToDouble(Eval("TotalWorkpackageWeight")) != 100 && Convert.ToBoolean(Eval("IsAutoCalculate"))? Color.Red : Color.Black %>'
                                                    Text='<%# Eval("TotalWorkpackageWeight") != null ? Eval("TotalWorkpackageWeight") + "%" : string.Empty%>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="Notes" HeaderText="Notes" UniqueName="Notes" AllowFiltering="false">
                                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">

                                    <Selecting AllowRowSelect="true" />
                                    <ClientEvents OnRowSelecting="OnRowSelecting" OnGridCreated="GetGridObject" OnRowContextMenu="RowContextMenu" OnRowDblClick="RowDblClick" />
                                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </telerik:RadPane>

                    </telerik:RadSplitter>

                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>

    <telerik:RadContextMenu ID="radMenu" runat="server"
        EnableRoundedCorners="true" EnableShadows="true" OnClientItemClicking="gridMenuClicking">
        <Items>
            <telerik:RadMenuItem Text="Attach project files" ImageUrl="~/Images/attach.png" Value="AttachDocument" />
            <telerik:RadMenuItem Text="Add New Workpackage" ImageUrl="~/Images/addNew.png" Value="AddNewWorkpackage" />
            <telerik:RadMenuItem Text="Workpackage list" ImageUrl="~/Images/package.png" Value="WorkpackageList" />
            <telerik:RadMenuItem Text="Start/Stop Project" ImageUrl="~/Images/addNew.png" Value="StartStopProject" />
            <telerik:RadMenuItem IsSeparator="True" />
            <telerik:RadMenuItem Text="Export Workpackage List" ImageUrl="~/Images/export.png" Value="ExportWorkpackage" />
            <telerik:RadMenuItem Text="Import workpackage For Update" ImageUrl="~/Images/import.png" Value="UpdateWPMaster" />
            <telerik:RadMenuItem IsSeparator="True" />
            <telerik:RadMenuItem Text="Export EMDR Of Project" ImageUrl="~/Images/export.png" Value="ExportCMDR" />
            <telerik:RadMenuItem Text="Export EMDR Tổng hợp" ImageUrl="~/Images/export.png" Value="ExportMergeCMDR" />
            <telerik:RadMenuItem Text="Export EMDR Will Issue Next Week" ImageUrl="~/Images/export.png" Value="ExportCMDRNextWeek" Visible="false" />
            <telerik:RadMenuItem Text="Export EMDR Of Project For Contractor" ImageUrl="~/Images/export.png" Value="ExportCMDR_C" />
            <telerik:RadMenuItem Text="Export EMDR Of Project For Comment" ImageUrl="~/Images/export.png" Value="ExportComment" />
            <telerik:RadMenuItem Text="Export EMDR Of Project For Wait Comment" ImageUrl="~/Images/export.png" Value="ExportWaitComment" Visible="false" />
            <telerik:RadMenuItem IsSeparator="True" Visible="false" />
            <telerik:RadMenuItem Text="Export F-TK-01 (Vietnamese)" ImageUrl="~/Images/report3.png" Value="FTK01V" Visible="false" />
            <telerik:RadMenuItem Text="Export F-TK-01 (Russian)" ImageUrl="~/Images/report3.png" Value="FTK01R" Visible="false" />
            <telerik:RadMenuItem IsSeparator="True" />
            <telerik:RadMenuItem Text="Export workpackage master template" ImageUrl="~/Images/export.png" Value="ExportWPMaster" />
            <telerik:RadMenuItem Text="Import workpackage master" ImageUrl="~/Images/import.png" Value="ImportWPMaster" />
            <telerik:RadMenuItem IsSeparator="True" Visible="false" />
            <telerik:RadMenuItem Text="View Progress Report" ImageUrl="~/Images/progressReport.png" Value="ProgressReport" Visible="false" />
            <telerik:RadMenuItem Text="Print Progress Report" ImageUrl="~/Images/report.png" Value="PrintProgressReport" Visible="false" />
            <telerik:RadMenuItem IsSeparator="True" />
            <telerik:RadMenuItem Text="Export Form Transmittal" ImageUrl="~/Images/export.png" Value="FormTrans" Visible="false" />
            <telerik:RadMenuItem Text="Export IDC Matrix" ImageUrl="~/Images/export.png" Value="IDCMatrix" />
            <telerik:RadMenuItem Text="Import IDC Matrix" ImageUrl="~/Images/export.png" Value="ImportIDCMatrix" />
            <telerik:RadMenuItem Text="Create Folder Trans" ImageUrl="~/Images/export.png" Value="CreateFolderTrans" Visible="false" />
        </Items>
    </telerik:RadContextMenu>

    <span style="display: none">


        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>

                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <%--<telerik:AjaxSetting AjaxControlID="radMenu">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>

                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="rtvWorkList">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="ddlYear">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rtvWorkList" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <%--                <telerik:AjaxSetting AjaxControlID="ckbShowUncomplete">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ckbShowAll" />
                        <telerik:AjaxUpdatedControl ControlID="ckbShowLater" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="ckbShowAll">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ckbShowUncomplete" />
                        <telerik:AjaxUpdatedControl ControlID="ckbShowLater" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="ckbShowLater">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ckbShowUncomplete" />
                        <telerik:AjaxUpdatedControl ControlID="ckbShowAll" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="cblShow">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>

                <telerik:AjaxSetting AjaxControlID="ddlShow">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>

            <telerik:RadWindow ID="AddNewWP" runat="server" Title="Workpackage Information"
                VisibleStatusbar="false" Height="650" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ViewSummary" runat="server" Title="Summary"
                VisibleStatusbar="false" Height="460" Width="630"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Project Information"
                VisibleStatusbar="false" Height="600" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="AttachDoc" runat="server" Title="Attach project files" OnClientClose="refreshGrid"
                VisibleStatusbar="false" Height="550" Width="700"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="StartStopProject" runat="server" Title="Start/Stop Project" OnClientClose="refreshGrid"
                VisibleStatusbar="false" Height="450" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="ImportData" runat="server" Title="Import Project Info"
                VisibleStatusbar="false" Height="400" Width="520"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="WP" runat="server" Title="Workpackage List"
                VisibleStatusbar="false" Height="400" Width="1100" MinHeight="400" MinWidth="1100" MaxHeight="400" MaxWidth="1100"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="ImportWP" runat="server" Title="Import Workpackage Info"
                VisibleStatusbar="false" Height="400" Width="520"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ExportForm" runat="server" Title="Export Form"
                VisibleStatusbar="false" Height="500" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ProgressReport" runat="server" Title="ProgressReport"
                VisibleStatusbar="false" Height="560" Width="950"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <asp:HiddenField runat="server" ID="FolderContextMenuAction" />
    <asp:HiddenField runat="server" ID="lblFolderId" />
    <asp:HiddenField runat="server" ID="lblProjectId" />
    <asp:HiddenField runat="server" ID="lblCategoryId" />
    <asp:HiddenField runat="server" ID="IsEngineer" />

    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
        <script type="text/javascript">

            var radDocuments;

            function refreshGrid() {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }

            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

            function onColumnHidden(sender) {

                var masterTableView = sender.get_masterTableView().get_element();
                masterTableView.style.tableLayout = "auto";
                //window.setTimeout(function () { masterTableView.style.tableLayout = "auto"; }, 0);
            }

            // Undocked and Docked event slide bar Tree folder
            function OnClientUndocked(sender, args) {
                radDocuments.get_masterTableView().showColumn(7);
                radDocuments.get_masterTableView().showColumn(8);
                radDocuments.get_masterTableView().showColumn(9);

                radDocuments.get_masterTableView().showColumn(11);
                radDocuments.get_masterTableView().showColumn(12);
            }

            function OnClientDocked(sender, args) {
                radDocuments.get_masterTableView().hideColumn(7);
                radDocuments.get_masterTableView().hideColumn(8);
                radDocuments.get_masterTableView().hideColumn(9);

                radDocuments.get_masterTableView().hideColumn(11);
                radDocuments.get_masterTableView().hideColumn(12);
            }

            function RowClick(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblProjectId.ClientID %>").value = Id;
            }


            function Set_Cookie(name, value, expires, path, domain, secure) {
                // set time, it's in milliseconds
                var today = new Date();
                today.setTime(today.getTime());

                /*
                if the expires variable is set, make the correct 
                expires time, the current script below will set 
                it for x number of days, to make it for hours, 
                delete * 24, for minutes, delete * 60 * 24
                */
                if (expires) {
                    expires = expires * 1000 * 60 * 60 * 24;
                }
                var expires_date = new Date(today.getTime() + (expires));

                document.cookie = name + "=" + escape(value) +
                    ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
                    ((path) ? ";path=" + path : "") +
                    ((domain) ? ";domain=" + domain : "") +
                    ((secure) ? ";secure" : "");
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                var loadingPanel = $find('<%= RadAjaxLoadingPanel2.ClientID %>');
                loadingPanel.set_modal(true);
                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");

                searchButton = toolbar.findButtonByCommandName("doSearch");

                $telerik.$(".searchtextbox")
                    .bind("keypress", function (e) {
                        searchButton.set_imageUrl("~/Images/search.gif");
                        searchButton.set_value("search");
                    });
            }

            function ShowEditForm(id) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.setSize(700, document.documentElement.offsetHeight - 100);
                owd.Show();
                owd.setUrl("ScopeProjectEditForm.aspx?disId=" + id, "CustomerDialog");
            }
            function ShowEMDR(id) {
                <%--var owd = $find("<%=ViewEMDR.ClientID %>");
                owd.Show();
                owd.maximize(true);
                owd.setUrl("../../ViewEMDR.aspx?disId=" + id, "ViewEMDR");--%>
                window.open("../../EMDR.aspx?disId=" + id, "_blank");
            }
            function ShowSummary(id) {
                var owd = $find("<%=ViewSummary.ClientID %>");
                //owd.setSize(730, document.documentElement.offsetHeight);
                owd.Show();
                owd.setUrl("CompactSummary.aspx?projectId=" + id, "SummaryDialog");
            }
            function refreshGrid(arg) {
                //alert(arg);
                if (!arg) {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                    masterTable.rebind();
                }
                else {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                    masterTable.rebind();
                }
            }

            function refreshTab(arg) {
                $('.EDMsRadPageView' + arg + ' iframe').attr('src', $('.EDMsRadPageView' + arg + ' iframe').attr('src'));
            }

            function RowDblClick(sender, eventArgs) {
          <%--    var owd = $find("<%=CustomerDialog.ClientID %>");--%>
                //  owd.Show();
                //  owd.setUrl("Controls/Customers/ViewCustomerDetails.aspx?docId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
                // window.radopen("Controls/Customers/ViewCustomerDetails.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
                var win = window.open("WorkGroupList.aspx?PjID=" + eventArgs.getDataKeyValue("ID"), "_blank");
                //if (win) {
                win.focus();
                // }
                // else {

                //  }
            }



            function radPbCategories_OnClientItemClicking(sender, args) {
                var item = args.get_item();
                var categoryId = item.get_value();
                document.getElementById("<%= lblCategoryId.ClientID %>").value = categoryId;

            }

            function OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var strValue = button.get_value();

                var grid = $find("<%= grdDocument.ClientID %>");
                var customerId = null;
                var customerName = "";

                //if (grid.get_masterTableView().get_selectedItems().length > 0) {
                //    var selectedRow = grid.get_masterTableView().get_selectedItems()[0];
                //    customerId = selectedRow.getDataKeyValue("Id");
                //    //customerName = selectedRow.Items["FullName"]; 
                //    //customerName = grid.get_masterTableView().getCellByColumnUniqueName(selectedRow, "FullName").innerHTML;
                //}

                if (strText.toLowerCase() == "project") {
                    var owd = $find("<%=CustomerDialog.ClientID %>");
                    owd.setSize(700, document.documentElement.offsetHeight - 100);
                    owd.Show();
                    owd.setUrl("ScopeProjectEditForm.aspx", "CustomerDialog");
                }

                if (strText.toLowerCase() == "export master list template") {
                    ajaxManager.ajaxRequest("ExportMasterList");
                }

                if (strText.toLowerCase() == "import master list") {
                    var owd = $find("<%=ImportData.ClientID %>");
                    owd.Show();
                    owd.setUrl("ImportProjectInfo.aspx?type=addnew", "ImportData");
                }

                if (strText.toLowerCase() == "import temp") {
                    var owd = $find("<%=ImportData.ClientID %>");
                    owd.Show();
                    owd.setUrl("ImportTemp.aspx", "ImportData");
                }

                if (strValue == "FTK01V") {
                    var selectedProject = document.getElementById("<%= lblProjectId.ClientID %>").value;
                    if (selectedProject == "") {
                        alert("Please choice one Project to export F-TK-01 report");
                        return false;
                    }
                    ajaxManager.ajaxRequest("FTK01V");
                }

                if (strValue == "FTK01R") {
                    var selectedProject = document.getElementById("<%= lblProjectId.ClientID %>").value;
                    if (selectedProject == "") {
                        alert("Please choice one Project to export F-TK-01 report");
                        return false;
                    }
                    ajaxManager.ajaxRequest("FTK01R");
                }
            }

            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("~/Images/clear.gif");
                    searchButton.set_value("clear");
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value());
            }
            function onTabSelecting(sender, args) {
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }
            }

            function gridMenuClicking(sender, args) {
                var itemValue = args.get_item().get_value();
                var projId = document.getElementById("<%= lblProjectId.ClientID %>").value;
                switch (itemValue) {
                    case "AttachDocument":
                        var owd = $find("<%=AttachDoc.ClientID %>");
                        owd.Show();
                        owd.setUrl("ScopeProjectAttachFiles.aspx?docId=" + projId, "AttachDoc");
                        break;
                    case "AddNewWorkpackage":
                        var owd = $find("<%=AddNewWP.ClientID %>");
                        owd.Show();
                        owd.setUrl("WorkGroupEditForm.aspx?projectId=" + projId, "AddNewWP");
                        break;
                    case "StartStopProject":
                        var owd = $find("<%=StartStopProject.ClientID %>");
                        owd.Show();
                        owd.setUrl("StartStopProject.aspx?projectId=" + projId, "StartStopProject");
                        break;
                    case "WorkpackageList":
                        var owd = $find("<%=WP.ClientID %>");
                        owd.Show();
                        owd.setUrl("WorkpackageListByProject.aspx?docId=" + projId, "WP");
                        break;
                    case "FormTrans":
                        var owd = $find("<%=ExportForm.ClientID %>");
                        owd.Show();
                        owd.setUrl("ExporForm.aspx?disId=" + projId, "ExportForm");
                        break;
                    case "FTK01V":
                        ajaxManager.ajaxRequest("FTK01V");
                        break;
                    case "FTK01R":
                        ajaxManager.ajaxRequest("FTK01R");
                        break;
                    case "ExportWPMaster":
                        ajaxManager.ajaxRequest("ExportWPMaster");
                        break;
                    case "ExportWorkpackage":
                        ajaxManager.ajaxRequest("ExportWorkpackage");
                        break;
                    case "ExportCMDR":
                        ajaxManager.ajaxRequest("ExportCMDR");
                        break;
                    case "ExportMergeCMDR":
                        ajaxManager.ajaxRequest("ExportMergeCMDR");
                        break;
                    case "ExportCMDR_C":
                        ajaxManager.ajaxRequest("ExportCMDR_C");
                        break;
                    case "ExportCMDRNextWeek":
                        ajaxManager.ajaxRequest("IssueNextWeek");
                        break;
                    case "ExportComment":
                        ajaxManager.ajaxRequest("ExportComment");
                        break;
                    case "ExportWaitComment":
                        ajaxManager.ajaxRequest("ExportWaitComment");
                        break;
                    case "ImportWPMaster":
                        var owd = $find("<%=ImportWP.ClientID %>");
                        owd.Show();
                        owd.setUrl("ImportWorkpackageInfo.aspx?type=addnew&projId=" + projId, "ImportWP");
                        break;
                    case "UpdateWPMaster":
                        var owd = $find("<%=ImportWP.ClientID %>");
                        owd.Show();
                        owd.setUrl("ImportWorkpackageInfo.aspx?type=update&projId=" + projId, "ImportWP");
                        break;
                    case "ProgressReport":
                        var owd = $find("<%=ProgressReport.ClientID %>");
                        owd.Show();
                        owd.setUrl("ProgressReport.aspx?projId=" + projId, "ProgressReport");
                        break;
                    case "PrintProgressReport":
                        ajaxManager.ajaxRequest("PrintProgressReport");
                        break;
                    case "IDCMatrix":
                        ajaxManager.ajaxRequest("IDCMatrix");
                        break;
                    case "ImportIDCMatrix":
                        var owd = $find("<%=ImportWP.ClientID %>");
                        owd.Show();
                        owd.setUrl("ImportDistributionMatrixDetail.aspx", "ImportMatrix");
                        break;
                    case "CreateFolderTrans":
                        var selectedProject = document.getElementById("<%= lblProjectId.ClientID %>").value;
                        ajaxManager.ajaxRequest("CreateFolderTrans_" + selectedProject);
                        break;
                }
            }

            function RowContextMenu(sender, eventArgs) {
                var menu = $find("<%=radMenu.ClientID %>");
                var evt = eventArgs.get_domEvent();

                if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                    return;
                }

                var index = eventArgs.get_itemIndexHierarchical();
                document.getElementById("radGridClickedRowIndex").value = index;

                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblProjectId.ClientID %>").value = Id;

                sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);

                menu.show(evt);

                evt.cancelBubble = true;
                evt.returnValue = false;

                if (evt.stopPropagation) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }

            function OnRowSelecting(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblProjectId.ClientID %>").value = Id;
            }

            function onRequestStart(sender, args) {
                ////alert(args.get_eventTarget());
                if (args.get_eventTarget().indexOf("ExportTo") >= 0 || args.get_eventTarget().indexOf("btnDownloadPackage") >= 0 ||
                    args.get_eventTarget().indexOf("ajaxCustomer") >= 0) {
                    args.set_enableAjax(false);
                }
            }


            function gridContextMenuShowing(menu, args) {
                var isEngineer = document.getElementById("<%= IsEngineer.ClientID %>").value;

                if (isEngineer == "true") {
                    menu.get_allItems()[3].hide();
                    menu.get_allItems()[4].hide();
                    menu.get_allItems()[5].hide();

                    menu.get_allItems()[6].hide();
                    menu.get_allItems()[7].hide();
                    menu.get_allItems()[8].hide();

                } else {
                    menu.get_allItems()[3].show();
                    menu.get_allItems()[4].show();
                    menu.get_allItems()[5].show();

                    menu.get_allItems()[6].show();
                    menu.get_allItems()[7].show();
                    menu.get_allItems()[8].show();
                }
            }
            function ddlWorkHandler(sender, eventArgs) {
                var tree = sender.get_items().getItem(0).findControl("rtvWorkList");
                var selectedNode = tree.get_selectedNode();
                if (selectedNode) {
                    selectedNode.scrollIntoView();
                }
            }
            function rtvWorkNodeClicking(sender, args) {
                var menu = $find("<%= CustomerMenu.ClientID %>");
                var comboBox = menu.get_items().getItem(5).findControl("rcbWorkList");

                var node = args.get_node();
                comboBox.set_text(node.get_text());
                comboBox.trackChanges();
                comboBox.get_items().getItem(0).set_text(node.get_text());
                comboBox.get_items().getItem(0).set_value(node.get_value());
                comboBox.commitChanges();
                comboBox.hideDropDown();
            }
            /* ]]> */
        </script>
    </telerik:RadCodeBlock>
</asp:Content>

<%--Tan.Le Remove here--%>
<%--<uc1:List runat="server" ID="CustomerList"/>--%>
<%-- <div id="EDMsCustomers" runat="server" />--%>

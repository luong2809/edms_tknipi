﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.DAO;
using EDMs.Data.Entities;

namespace EDMs.Business.Services
{
    public class CheckInHistoryService
    {       
         private readonly CheckInHistoryDAO repo;

        public CheckInHistoryService()
        {
            repo = new CheckInHistoryDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All CheckInHistory
        /// </summary>
        /// <returns></returns>
        public List<CheckInHistory> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get CheckInHistory By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public CheckInHistory GetByID(int ID)
        {
            return repo.GetByID( ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert CheckInHistory
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(CheckInHistory bo)
        {
            try
            {
                return repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update CheckInHistory
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(CheckInHistory bo)
        {
            try
            {
                return repo.Update(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete CheckInHistory
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(CheckInHistory bo)
        {
            try
            {
                return repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete CheckInHistory By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentNew.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the DocumentNew type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Data.DAO.Document;
using EDMs.Data.DAO.Scope;
using System;

namespace EDMs.Data.Entities
{
    /// <summary>
    /// The document new.
    /// </summary>
    public partial class DocumentPackage
    {
        private readonly ScopeProjectDAO scopeProjectDAO = new ScopeProjectDAO();
        //public string DepartmentShortName
        //{
        //    get
        //    {
        //        var roleDAO = new RoleDAO();
        //        var role = roleDAO.GetByID(this.DeparmentId.GetValueOrDefault());
        //        return role != null ? role.Name : string.Empty;
        //    }
        //}

        ////public bool HasAttachFile
        ////{
        ////    get
        ////    {
        ////        var temp = new AttachFilesPackageDAO();
        ////        return temp.GetAll().Any(t => t.DocumentPackageID == this.ID);
        ////    }
        ////}

        public bool HaveWpAutoCalculate { get; set; }

        public WorkGroup WorkGroup { get; set; }

        public DateTime? MilestoneDate { get; set; }

        public string NumberHardCopy
        {
            get
            {
                var obj = new AttachDocToTransmittalDAO();
                var temp = obj.GetByDocument(this.ID);
                string result = "";
                if (temp != null && (!string.IsNullOrEmpty(temp.OriginalNumber.ToString()) || !string.IsNullOrEmpty(temp.CopyNumber.ToString())))
                {
                    if (!string.IsNullOrEmpty(temp.OriginalNumber.ToString()))
                    {
                        result += temp.OriginalNumber + " ORIGINAL ";
                    }
                    if (!string.IsNullOrEmpty(temp.CopyNumber.ToString()))
                    {
                        result += temp.CopyNumber + " COPY ";
                    }
                }
                else
                {
                    result = this.CopyNumber;
                }
                return result;
            }
        }
        public bool IsStop
        {
            get
            {
                var obj = scopeProjectDAO.GetById(this.ProjectId.GetValueOrDefault());
                return obj.IsStop.GetValueOrDefault();
            }
        }
    }
}

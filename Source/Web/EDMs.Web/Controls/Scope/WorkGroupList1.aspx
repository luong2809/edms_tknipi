﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WorkGroupList1.aspx.cs" Inherits="EDMs.Web.Controls.Scope.WorkGroupList1" EnableViewState="true" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .RadAjaxPanel {
            height: 100% !important;
        }
        

        .RadGrid .rgSelectedRow
        {
            background-image : none !important;
            background-color: coral !important;
        }
        .rgExpandCol {
            width: 1% !important;
        }

        .rgGroupCol {
            width: 1% !important;
        }
        .RadAjaxPanel {
            height: 100% !important;
        }
        a.tooltip
        {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong
            {
                line-height: 30px;
            }

            a.tooltip:hover
            {
                text-decoration: none;
            }

            a.tooltip span
            {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span
            {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout
        {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span
        {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel
        {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5
        {
            height: 100% !important;
        }

        #divContainerLeft
        {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight
        {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted
        {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper
        {
            width: 100%;
            height: 100%;
            /*background: transparent url(~/Images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip
        {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage
        {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow
        {
            z-index: 8000 !important;
        }

        .TemplateMenu
        {
            z-index: 10;
        }

        .rtbItem.rtbTemplate {
            display: inline-flex !important;
        }
    </style>
    
     <telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="SCOPE" runat="server" Expanded="True" Width="100%">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Projects" ImageUrl="~/Images/project.png" NavigateUrl="~/Controls/Scope/ScopeProjectList.aspx"/>
                    <telerik:RadPanelItem runat="server" Text="Workpackages" ImageUrl="~/Images/package.png" NavigateUrl="~/Controls/Scope/WorkGroupList.aspx" Selected="True"/>
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>
    
    <telerik:RadPanelBar ID="radPbList" runat="server" Width="100%"/>
    <telerik:RadPanelBar ID="radPbSystem" runat="server" Width="100%"/>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
        <telerik:RadPane ID="RadPane3" runat="server" Height="30px" Scrollable="false" Scrolling="None">
            <telerik:RadToolBar ID="CustomerMenu" runat="server" Width="100%" OnClientButtonClicking="OnClientButtonClicking">
                <Items>
                    <telerik:RadToolBarDropDown runat="server" Text="New" ImageUrl="~/Images/addNew.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Workpackage" Value="1" ImageUrl="~/Images/package.png"/>
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    <telerik:RadToolBarButton runat="server" IsSeparator="true"/>
                    <telerik:RadToolBarDropDown runat="server" Text="Action" ImageUrl="~/Images/action.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Export master list template" Value="Adminfunc" ImageUrl="~/Images/export.png"/>
                            <telerik:RadToolBarButton runat="server" Text="Import master list" Value="Adminfunc" ImageUrl="~/Images/import.png" />
                            
                            <telerik:RadToolBarButton runat="server" IsSeparator="true"  Value="Adminfunc" Visible="False"/>
                            <telerik:RadToolBarButton runat="server" Text="Export edit data file" Value="7" ImageUrl="~/Images/emdrreport.png" Visible="False"/>
                            <telerik:RadToolBarButton runat="server" Text="Import edit data file" Value="Adminfunc" ImageUrl="~/Images/upload.png" Visible="False"/>
                            
                            <telerik:RadToolBarButton runat="server" IsSeparator="true"  Value="Adminfunc"/>
                            <telerik:RadToolBarButton runat="server" Text="Export F-TK-02 (Vietnamese)" Value="FTK02V" ImageUrl="~/Images/report3.png"/>
                            <telerik:RadToolBarButton runat="server" Text="Export F-TK-02 (Russian)" Value="FTK02R" ImageUrl="~/Images/report3.png"/>
                            <telerik:RadToolBarButton runat="server" IsSeparator="true"/>
                            <telerik:RadToolBarButton runat="server" Text="Export Document list for Update" Value="ExportDocList" ImageUrl="~/Images/emdrreport.png"/>
                            <telerik:RadToolBarButton runat="server" Text="Import Document list for Update" Value="ImportDocList" ImageUrl="~/Images/upload.png"/>
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    
                    <telerik:RadToolBarButton runat="server" IsSeparator="true"/>
                    <telerik:RadToolBarButton runat="server" Value="ShowAll">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlShow" runat="server" Width="200px"
                                OnSelectedIndexChanged="ddlShow_OnSelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem Text="Show overdue workpackages" Value="ShowOverDue" style="background-color: Crimson"/>
                                <asp:ListItem Text="Show uncompleted workpackages" Value="ShowUncomplete" Selected="True"/>
                                <asp:ListItem Text="Show all workpackages" Value="ShowAll"/>
                            </asp:DropDownList>
                            <%--<asp:CheckBox ID="ckbShowLater" runat="server" Text="Show overdue workpackages | " AutoPostBack="True" OnCheckedChanged="ckbShowLater_CheckedChange" ForeColor="Crimson"/>
                            <asp:CheckBox ID="ckbShowUncomplete" runat="server" Text="Show uncomplete workpackages | " AutoPostBack="True" OnCheckedChanged="ckbShowUncomplete_CheckedChange" Checked="True"/>
                            <asp:CheckBox ID="ckbShowAll" runat="server" Text="Show all workpackages" AutoPostBack="True" OnCheckedChanged="ckbShowAll_CheckedChange"/>--%>
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                </Items>
            </telerik:RadToolBar>

        </telerik:RadPane>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter3" runat="server" Orientation="Horizontal">
                <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None" >
                    
                     <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical">
                         <telerik:RadPane ID="Radpane6" runat="server" Scrolling="None">
                                <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True"
                                    AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                                    GridLines="None" Height="100%" AllowFilteringByColumn="True" AllowSorting="True"
                                    OnItemDataBound="grdDocument_ItemDataBound"
                                    OnDeleteCommand="grdDocument_DeleteCommand" 
                                    OnItemCreated="grdDocument_ItemCreated"
                                    OnNeedDataSource="grdDocument_OnNeedDataSource" 
                                    PageSize="100" Style="outline: none">
                                    <SortingSettings SortedBackColor="#FFF6D6"></SortingSettings>
                                    <GroupingSettings CaseSensitive="False"></GroupingSettings>

                                    <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%" TableLayout="Auto" CssClass="rgMasterTable"
                                        CommandItemDisplay="Top" >
                                        <GroupByExpressions>
                                            <telerik:GridGroupByExpression>
                                                <SelectFields>
                                                    <telerik:GridGroupByField FieldAlias="Project" FieldName="ProjectName" FormatString="{0:D}"
                                                        HeaderValueSeparator=": "></telerik:GridGroupByField>
                                                </SelectFields>
                                                <GroupByFields>
                                                    <telerik:GridGroupByField FieldName="ProjectName" SortOrder="Ascending" ></telerik:GridGroupByField>
                                                </GroupByFields>
                                            </telerik:GridGroupByExpression>
                                        </GroupByExpressions>
                                        <CommandItemSettings  ShowAddNewRecordButton="false" RefreshText="Refresh Data" ShowExportToExcelButton="false"/>
                                        <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; items." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ColumnGroups>
                                            <telerik:GridColumnGroup HeaderText="Man Hours" Name="ManHours" HeaderStyle-HorizontalAlign="Center"/>
                                        </ColumnGroups>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                            <telerik:GridBoundColumn DataField="IsLater" UniqueName="IsLater" Display="False" />
                                            <telerik:GridBoundColumn DataField="ProjectId" UniqueName="ProjectId" Display="False" />
                                            <%--<telerik:GridBoundColumn DataField="IsUsing" UniqueName="IsUsing" Display="False" />--%>
                                            <telerik:GridBoundColumn DataField="CanDelete" UniqueName="CanDelete" Display="False" />

                                            <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="EditColumn">
                                                <HeaderStyle Width="30"  />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <a href='javascript:ShowEditForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color:blue">
                                                    <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer;border: none" AlternateText="Edit properties" />
                                                        <a/>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                                                <HeaderStyle Width="30" />
                                                 <ItemStyle HorizontalAlign="Center"/>
                                            </telerik:GridButtonColumn>
                                            
                                            <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn" HeaderText="Wp. File">
                                                <HeaderStyle Width="35" HorizontalAlign="Center"/>
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <div runat="server" Visible='<%# DataBinder.Eval(Container.DataItem, "DefaultFilePath") != null && !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "DefaultFilePath").ToString()) %>'>
                                                        <a href='<%# DataBinder.Eval(Container.DataItem, "DefaultFilePath") %>' target="_blank">
                                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtentionIcon") %>' 
                                                                Style="cursor: pointer;" ToolTip="Download default workpackage file." /> 
                                                        </a>
                                                    </div>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridBoundColumn DataField="Name" HeaderText="Workpackage Number" UniqueName="Name"
                                                 ShowFilterIcon="False" FilterControlWidth="185" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="200" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridBoundColumn DataField="RevisionName" HeaderText="Revision" UniqueName="RevisionName"
                                                 ShowFilterIcon="False" FilterControlWidth="45" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="60" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridBoundColumn DataField="Description" HeaderText="Workpackage Name" UniqueName="Description"
                                                 ShowFilterIcon="False" FilterControlWidth="335" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="350" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridBoundColumn DataField="DepartmentName" HeaderText="Department" UniqueName="DepartmentName"
                                                 ShowFilterIcon="False" FilterControlWidth="65" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="80" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridBoundColumn DataField="ProjectName" HeaderText="Project" UniqueName="ProjectName"
                                                 ShowFilterIcon="False" FilterControlWidth="135" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="150" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>

                                              <telerik:GridBoundColumn DataField="ProjectManagerFullName" HeaderText="Engineering Manager" UniqueName="ProjectManagerFullName"
                                                 ShowFilterIcon="False" FilterControlWidth="185" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="200" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>

                                            <telerik:GridBoundColumn DataField="AreaName" HeaderText="Area" UniqueName="AreaName"
                                                 ShowFilterIcon="False" FilterControlWidth="45" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="60" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridBoundColumn DataField="DisciplineName" HeaderText="Discipline" UniqueName="DisciplineName"
                                                 ShowFilterIcon="False" FilterControlWidth="55" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="70" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridBoundColumn DataField="TypeName" HeaderText="Type" UniqueName="TypeName"
                                                 ShowFilterIcon="False" FilterControlWidth="45" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="60" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Total Document Weight" UniqueName="TotalDocWeight"
                                                DataField="TotalDocWeight" ShowFilterIcon="true" FilterControlWidth="50" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="LessThanOrEqualTo"
                                                SortExpression="TotalDocWeight">
                                                <HeaderStyle HorizontalAlign="Center" Width="90" />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ForeColor='<%# Convert.ToDouble(Eval("TotalDocWeight")) != 100 && Convert.ToBoolean(Eval("IsAutoCalculate"))? Color.Red : Color.Black %>'
                                                        Text='<%# Eval("TotalDocWeight") != null ? Eval("TotalDocWeight") + "%" : string.Empty%>'/>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Weight" UniqueName="Weight" DataField="Weight" ShowFilterIcon="true" FilterControlWidth="50" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="LessThanOrEqualTo" SortExpression="Weight">
                                                <HeaderStyle HorizontalAlign="Center" Width="90" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ForeColor='<%# Convert.ToDouble(Eval("Weight")) == 0 ? Color.Red : Color.Black %>'
                                                        Text='<%# Eval("Weight") != null ? Eval("Weight") + "%" : string.Empty%>'/>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn HeaderText="Complete" UniqueName="Complete" 
                                                DataField="Complete" ShowFilterIcon="True" FilterControlWidth="50" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="LessThanOrEqualTo">
                                                <HeaderStyle HorizontalAlign="Center" Width="90" />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <%# Eval("Complete") != null ? Eval("Complete") + "%" : string.Empty%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Used man hours" UniqueName="UsedManHours" ColumnGroupName="ManHours"
                                                DataField="UsedManHours" ShowFilterIcon="True" FilterControlWidth="50" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="LessThanOrEqualTo">
                                                <HeaderStyle HorizontalAlign="Center" Width="90" />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <%# Eval("UsedManHours") ?? string.Empty%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Total man hours" UniqueName="TotalManHours"  ColumnGroupName="ManHours"
                                                DataField="TotalManHours" ShowFilterIcon="True" FilterControlWidth="50" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="LessThanOrEqualTo">
                                                <HeaderStyle HorizontalAlign="Center" Width="90" />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <%# Eval("TotalManHours") ?? string.Empty%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridDateTimeColumn DataField="StartDate" HeaderText="Start Date" UniqueName="StartDate" DataFormatString="{0:dd/MM/yyyy}" SortExpression="StartDate"  PickerType="DatePicker" AutoPostBackOnFilter="true" 
                                                EnableRangeFiltering="true" FilterControlWidth="95" ShowFilterIcon="true" CurrentFilterFunction="Between">
                                                <HeaderStyle HorizontalAlign="Center" Width="280" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridDateTimeColumn>
                                            
                                            <telerik:GridDateTimeColumn DataField="Deadline" HeaderText="Deadline" UniqueName="Deadline" DataFormatString="{0:dd/MM/yyyy}" SortExpression="StartDate"  PickerType="DatePicker" AutoPostBackOnFilter="true" 
                                                EnableRangeFiltering="true" FilterControlWidth="95" ShowFilterIcon="true" CurrentFilterFunction="Between">
                                                <HeaderStyle HorizontalAlign="Center" Width="280" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridDateTimeColumn>
                                            
                                            <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="Finish Date" UniqueName="EndDate" DataFormatString="{0:dd/MM/yyyy}" SortExpression="StartDate"  PickerType="DatePicker" AutoPostBackOnFilter="true" 
                                                EnableRangeFiltering="true" FilterControlWidth="95" ShowFilterIcon="true" CurrentFilterFunction="Between">
                                                <HeaderStyle HorizontalAlign="Center" Width="280" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridDateTimeColumn>
                                            
                                            <telerik:GridBoundColumn Display="False" DataField="InputDataSupplier" HeaderText="Input Data Supplier" UniqueName="InputDataSupplier"
                                                 ShowFilterIcon="False" FilterControlWidth="135" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="150" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                             <telerik:GridBoundColumn Display="False" DataField="InputDocument" HeaderText="Input Document" UniqueName="InputDocument"
                                                 ShowFilterIcon="False" FilterControlWidth="135" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="150" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                             <telerik:GridBoundColumn Display="False" DataField="InformationExchange" HeaderText="Information Exchange" UniqueName="InformationExchange"
                                                 ShowFilterIcon="False" FilterControlWidth="235" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="250" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                             <telerik:GridBoundColumn Display="False" DataField="ReportMode" HeaderText="Report Mode" UniqueName="ReportMode"
                                                 ShowFilterIcon="False" FilterControlWidth="135" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="150" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                             <telerik:GridBoundColumn DataField="IncomingNo" HeaderText="Incoming No." UniqueName="IncomingNo"
                                                 ShowFilterIcon="False" FilterControlWidth="135" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="150" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                             <telerik:GridBoundColumn DataField="OutgoingNo" HeaderText="Outgoing No." UniqueName="OutgoingNo"
                                                 ShowFilterIcon="False" FilterControlWidth="135" 
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="150" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Auto Calculate" UniqueName="IsAutoCalculate" AllowFiltering="False">
                                                <HeaderStyle Width="70" HorizontalAlign="Center"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/ok.png"
                                                        Visible='<%# DataBinder.Eval(Container.DataItem, "IsAutoCalculate")%>'/>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                                        <Selecting AllowRowSelect="true" />
                                        <ClientEvents OnRowSelecting="OnRowSelecting"  OnGridCreated="GetGridObject" OnRowContextMenu="RowContextMenu"/>
                                        <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                                    </ClientSettings>
                            </telerik:RadGrid>
                        </telerik:RadPane>

                     </telerik:RadSplitter>       

                </telerik:RadPane>
                
               
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <telerik:RadContextMenu ID="radMenu" runat="server"
        EnableRoundedCorners="true" EnableShadows="true" OnClientItemClicking="gridMenuClicking"
        OnClientShowing="gridContextMenuShowing">
        <Items>
            <telerik:RadMenuItem Text="Revise" ImageUrl="~/Images/revise.png" Value="Revise"/>
            <telerik:RadMenuItem  IsSeparator="True"/>
            <telerik:RadMenuItem Text="Add New Document" ImageUrl="~/Images/addNew.png" Value="AddDocument"/>
            <telerik:RadMenuItem Text="Attach workpackage files" ImageUrl="~/Images/attach.png" Value="AttachDocument"/>
            <telerik:RadMenuItem Text="Document list" ImageUrl="~/Images/document1.png" Value="DocumentList"/>
            <telerik:RadMenuItem Text="Export EMDR " ImageUrl="~/Images/export.png" Value="ExportCMDR" />
            <telerik:RadMenuItem Text="Import EMDR  For Update" ImageUrl="~/Images/upload.png" Value="ImportCMDR" />
            <telerik:RadMenuItem Text="Contents" ImageUrl="~/Images/content.png" Value="Content"/>
            <telerik:RadMenuItem Text="Milestones" ImageUrl="~/Images/milestone.png" Value="Milestone"/>
            <telerik:RadMenuItem  IsSeparator="True"/>
            <telerik:RadMenuItem Text="Export F-TK-02 (Vietnamese)" ImageUrl="~/Images/report3.png" Value="FTK02V"/>
            <telerik:RadMenuItem Text="Export F-TK-02 (Russian)" ImageUrl="~/Images/report3.png" Value="FTK02R"/>

        </Items>
    </telerik:RadContextMenu>
    <span style="display: none">
        

        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>
                
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radMenu">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument"  LoadingPanelID="RadAjaxLoadingPanel2"  ></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContainer" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                    
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="ckbShowUncomplete">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="ckbShowAll"/>
                        <telerik:AjaxUpdatedControl ControlID="ckbShowLater"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="ckbShowAll">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="ckbShowUncomplete"/>
                        <telerik:AjaxUpdatedControl ControlID="ckbShowLater"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="ckbShowLater">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="ckbShowUncomplete"/>
                        <telerik:AjaxUpdatedControl ControlID="ckbShowAll"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlShow">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" >
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Workpackage Information"
                VisibleStatusbar="false" Height="650" Width="650" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="AddDocument" runat="server" Title="Document Information" 
                VisibleStatusbar="false" Height="600" Width="650" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="AttachDoc" runat="server" Title="Attach workpackage files" OnClientClose="refreshGrid"
                VisibleStatusbar="false" Height="550" Width="700"  
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="Content" runat="server" Title="Workpackage contents"
                VisibleStatusbar="false" Height="600" Width="800" MinHeight="600" MinWidth="800" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="Milestone" runat="server" Title="Milestones Info"
                VisibleStatusbar="false" Height="640" Width="800" MinHeight="640" MinWidth="800" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="ImportData" runat="server" Title="Import Workpackage Info"
                VisibleStatusbar="false" Height="400" Width="520" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="ImportDocumentData" runat="server" Title="Update Document Info"
                VisibleStatusbar="false" Height="400" Width="520" OnClientClose="refreshGrid"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="DocList" runat="server" Title="Document List"
                VisibleStatusbar="false" Height="610" Width="1100" MinHeight="610" MinWidth="1100" MaxHeight="610" MaxWidth="1100" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <asp:HiddenField runat="server" ID="FolderContextMenuAction"/>
    <asp:HiddenField runat="server" ID="lblFolderId"/>
    <asp:HiddenField runat="server" ID="lblProjectId"/>
    <asp:HiddenField runat="server" ID="lblWorkpackageId"/>
    <asp:HiddenField runat="server" ID="lblCategoryId"/>
    <asp:HiddenField runat="server" ID="IsEngineer"/>
    <asp:HiddenField runat="server" ID="IsGip"/>

    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex"/>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="../../Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">
            var radDocuments;

            function refreshGrid() {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }


            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

            function onColumnHidden(sender) {
                
                var masterTableView = sender.get_masterTableView().get_element();
                masterTableView.style.tableLayout = "auto";
                //window.setTimeout(function () { masterTableView.style.tableLayout = "auto"; }, 0);
            }

            // Undocked and Docked event slide bar Tree folder
            function OnClientUndocked(sender, args) {
                radDocuments.get_masterTableView().showColumn(7);
                radDocuments.get_masterTableView().showColumn(8);
                radDocuments.get_masterTableView().showColumn(9);
                
                radDocuments.get_masterTableView().showColumn(11);
                radDocuments.get_masterTableView().showColumn(12);
            }
            
            function OnClientDocked(sender, args) {
                radDocuments.get_masterTableView().hideColumn(7);
                radDocuments.get_masterTableView().hideColumn(8);
                radDocuments.get_masterTableView().hideColumn(9);
                
                radDocuments.get_masterTableView().hideColumn(11);
                radDocuments.get_masterTableView().hideColumn(12);
            }

            function RowClick(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblWorkpackageId.ClientID %>").value = Id;
            }
            
            
            function Set_Cookie(name, value, expires, path, domain, secure) {
                // set time, it's in milliseconds
                var today = new Date();
                today.setTime(today.getTime());

                /*
                if the expires variable is set, make the correct 
                expires time, the current script below will set 
                it for x number of days, to make it for hours, 
                delete * 24, for minutes, delete * 60 * 24
                */
                if (expires) {
                    expires = expires * 1000 * 60 * 60 * 24;
                }
                var expires_date = new Date(today.getTime() + (expires));

                document.cookie = name + "=" + escape(value) +
                    ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
                    ((path) ? ";path=" + path : "") +
                    ((domain) ? ";domain=" + domain : "") +
                    ((secure) ? ";secure" : "");
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents());
                });

                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function ShowEditForm(id) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("WorkGroupEditForm.aspx?disId=" + id, "CustomerDialog");
            }
            
            function refreshGrid(arg) {
                //alert(arg);
                if (!arg) {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                    masterTable.rebind();
                }
                else {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                    masterTable.rebind();
                }
            }
            
            function refreshTab(arg) {
                $('.EDMsRadPageView' + arg + ' iframe').attr('src', $('.EDMsRadPageView' + arg + ' iframe').attr('src'));
            }

            function RowDblClick(sender, eventArgs) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/ViewCustomerDetails.aspx?docId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
                // window.radopen("Controls/Customers/ViewCustomerDetails.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
            }
            
            
            
            function radPbCategories_OnClientItemClicking(sender, args)
            {
                var item = args.get_item();
                var categoryId = item.get_value();
                document.getElementById("<%= lblCategoryId.ClientID %>").value = categoryId;
                
            }
            
            function OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var strValue = button.get_value();

                 
                if (strText.toLowerCase() == "workpackage") {
                    var owd = $find("<%=CustomerDialog.ClientID %>");
                    owd.Show();
                    owd.setUrl("WorkGroupEditForm.aspx", "CustomerDialog");
                }

                if (strText.toLowerCase() == "export master list template") {
                    ajaxManager.ajaxRequest("ExportMasterList");
                }
                
                if (strText.toLowerCase() == "import master list") {
                    var owd = $find("<%=ImportData.ClientID %>");
                    owd.Show();
                    owd.setUrl("ImportWorkpackageInfo.aspx?type=addnew", "ImportData");
                }

                if (strValue == "FTK02V") {
                    var selectedWorkpackage = document.getElementById("<%= lblWorkpackageId.ClientID %>").value;
                    if (selectedWorkpackage == "") {
                        alert("Please choice one workpackage to export F-TK-02 report");
                        return false;
                    }
                    ajaxManager.ajaxRequest("FTK02V");
                }

                if (strValue == "FTK02R") {
                    var selectedWorkpackage = document.getElementById("<%= lblWorkpackageId.ClientID %>").value;
                    if (selectedWorkpackage == "") {
                        alert("Please choice one workpackage to export F-TK-02 report");
                        return false;
                    }
                    ajaxManager.ajaxRequest("FTK02R");
                }

                if (strValue == "ExportDocList") {
                    ajaxManager.ajaxRequest("ExportDocList");
                }

                
                if (strValue == "ImportDocList") {
                    var owd = $find("<%=ImportDocumentData.ClientID %>");
                    owd.Show();
                    owd.setUrl("ImportDocData.aspx", "ImportDocumentData");
                }
            }
            
            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("~/Images/clear.gif");
                    searchButton.set_value("clear");
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value());
            }
            function onTabSelecting(sender, args) {
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }
            }
            
            function gridMenuClicking(sender, args) {
                var itemValue = args.get_item().get_value();
                var workpackageId = document.getElementById("<%= lblWorkpackageId.ClientID %>").value;
                var projectId = document.getElementById("<%= lblProjectId.ClientID %>").value;
                switch (itemValue) {
                    case "Revise":
                        var owd = $find("<%=CustomerDialog.ClientID %>");
                        owd.Show();
                        owd.setUrl("WorkGroupEditForm.aspx?disId=" + workpackageId + "&type=revise", "CustomerDialog");
                        break;

                    case "AddDocument":
                        var owd = $find("<%=AddDocument.ClientID %>");
                        owd.Show();
                        owd.setUrl("../../Controls/Document/DocumentPackageInfoEditForm.aspx?wpId=" + workpackageId + "&projectId=" + projectId, "AddDocument");
                        break;
                    case "AttachDocument":
                        var owd = $find("<%=AttachDoc.ClientID %>");
                            owd.Show();
                            owd.setUrl("WorkpackageAttachFiles.aspx?docId=" + workpackageId, "AttachDoc");
                            break;
                    case "DocumentList":
                        var owd = $find("<%=DocList.ClientID %>");
                            owd.Show();
                            owd.setUrl("DocumentListByWorkpackage.aspx?docId=" + workpackageId, "DocList");
                            break;
                    case "Content":
                        var owd = $find("<%=Content.ClientID %>");
                            owd.Show();
                            owd.setUrl("WorkpackageContents.aspx?docId=" + workpackageId, "Content");
                            break;
                    case "Milestone":
                        var owd = $find("<%=Milestone.ClientID %>");
                            owd.Show();
                            owd.setUrl("WorkpackageMilestone.aspx?docId=" + workpackageId, "Milestone");
                            break;
                    case "FTK02V":
                        ajaxManager.ajaxRequest("FTK02V");
                        break;
                    case "FTK02R":
                        ajaxManager.ajaxRequest("FTK02R");
                        break;
                    case  "ExportCMDR":
                        ajaxManager.ajaxRequest("ExportCMDR");
                        break;
                    case "ImportCMDR":
                        var owd = $find("<%=ImportDocumentData.ClientID %>");
                        owd.Show();
                        owd.setUrl("../../Controls/Document/ImportEMDRUpdate.aspx", "ImportDocumentData");
                        break;
                            
                }
            }

            function RowContextMenu(sender, eventArgs) {
                var menu = $find("<%=radMenu.ClientID %>");
                var evt = eventArgs.get_domEvent();
                var grid = sender;
                var masterTable = grid.get_masterTableView();
                var row = masterTable.get_dataItems()[eventArgs.get_itemIndexHierarchical()];

                if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                    return;
                }

                var index = eventArgs.get_itemIndexHierarchical();
                document.getElementById("radGridClickedRowIndex").value = index;

                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblWorkpackageId.ClientID %>").value = Id;

                var cellProjectId = masterTable.getCellByColumnUniqueName(row, "ProjectId");
                var projectId = cellProjectId.innerHTML == "&nbsp;" ? false : cellProjectId.innerHTML;
                document.getElementById("<%= lblProjectId.ClientID %>").value = projectId;

                sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);

                menu.show(evt);

                evt.cancelBubble = true;
                evt.returnValue = false;

                if (evt.stopPropagation) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }

            function onRequestStart(sender, args) {
                ////alert(args.get_eventTarget());
                if (args.get_eventTarget().indexOf("ExportTo") >= 0 || args.get_eventTarget().indexOf("btnDownloadPackage") >= 0 ||
                    args.get_eventTarget().indexOf("ajaxCustomer") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function OnRowSelecting(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblWorkpackageId.ClientID %>").value = Id;
            }

            function gridContextMenuShowing(menu, args) {
                var isEngineer = document.getElementById("<%= IsEngineer.ClientID %>").value;
                var isGip = document.getElementById("<%= IsGip.ClientID %>").value;
                //alert(isGip);

                if (isEngineer == "true") {
                    menu.get_allItems()[7].hide();
                    menu.get_allItems()[8].hide();
                    menu.get_allItems()[9].hide();

                } else {
                    menu.get_allItems()[7].show();
                    menu.get_allItems()[8].show();
                    menu.get_allItems()[9].show();
                }

                if (isGip == "true") {
                    menu.get_allItems()[2].hide();
                } else {
                    menu.get_allItems()[2].show();
                }
            }
        /* ]]> */
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
﻿

namespace EDMs.Data.DAO.Document
{  using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
   public class ManhourDAO : BaseDAO
    {

       public ManhourDAO() : base() { }

       public IQueryable<Manhour> GetIQueryable()
       {
           return this.EDMsDataContext.Manhours;
       }
       public List<Manhour> GetAll()
       {
           return this.EDMsDataContext.Manhours.OrderByDescending(t => t.ID).ToList();
       }

       public Manhour GetById(int id)
       {
           return this.EDMsDataContext.Manhours.FirstOrDefault(ob => ob.ID == id);
       }



       public int? Insert(Manhour ob)
       {
           try
           {
               this.EDMsDataContext.AddToManhours(ob);
               this.EDMsDataContext.SaveChanges();
               return ob.ID;
           }
           catch
           {
               return null;
           }
       }


       public bool Update(Manhour src)
       {
           try
           {
               Manhour des = (from rs in this.EDMsDataContext.Manhours
                                where rs.ID == src.ID
                                select rs).First();
            
               des.DocumentID=src.DocumentID;
               des.DocumentName=src.DocumentName;
               des.Total=src.Total;
               des.Engineer=src.Engineer;
               des.EngineerName=src.EngineerName;
               des.UpdateBy=src.UpdateBy;
               des.UpdateDate=src.UpdateDate;
               des.ManhourDate = src.ManhourDate;
               des.Note = src.Note;
               des.PercentTotal = src.PercentTotal;
               this.EDMsDataContext.SaveChanges();
               return true;
           }
           catch
           {
               return false;
           }
       }



       public bool Delete(Manhour src)
       {
           try
           {
               var des = this.GetById(src.ID);
               if (des != null)
               {
                   this.EDMsDataContext.DeleteObject(des);
                   this.EDMsDataContext.SaveChanges();
                   return true;
               }

               return false;
           }
           catch
           {
               return false;
           }
       }


       public bool Delete(int ID)
       {
           try
           {
               var des = this.GetById(ID);
               if (des != null)
               {
                   this.EDMsDataContext.DeleteObject(des);
                   this.EDMsDataContext.SaveChanges();
                   return true;
               }

               return false;
           }
           catch
           {
               return false;
           }
       }

    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentListByWorkpackage : Page
    {
        private readonly DocumentNewService documentNewService;

        private readonly AttachFilesPackageService attachFileService;

        private readonly DocumentPackageService documentPackageService;

        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly WorkGroupService workGroupService;
        private readonly AttachFilesWorkpackageService attachFilesWorkpackageService;


        /// <summary>
        /// Initializes a new instance of the <see cref="RevisionHistory"/> class.
        /// </summary>
        public DocumentListByWorkpackage()
        {
            this.documentNewService = new DocumentNewService();
            this.attachFileService = new AttachFilesPackageService();
            this.documentPackageService = new DocumentPackageService();
            this.workGroupService = new WorkGroupService();
            this.attachFilesWorkpackageService = new AttachFilesWorkpackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if(Request.QueryString["docId"] != null)
            {
                if (!UserSession.Current.IsEngineer)
                {
                    this.grdDocument.DataSource = this.documentPackageService.GetAllByWorkgroup(Convert.ToInt32(Request.QueryString["docId"]))
                        .OrderBy(t => t.DocNo)
                        .ToList();
                }
                else
                {
                    this.grdDocument.DataSource = this.documentPackageService.GetAllByWorkgroupForEng(Convert.ToInt32(Request.QueryString["docId"]), UserSession.Current.User.Id.ToString())
                        .OrderBy(t => t.DocNo)
                        .ToList();
                }
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                grdDocument.Rebind();
            }
        }

        
        protected void grdDocument_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            switch (e.DetailTableView.Name)
            {
                case "DocDetail":
                    {
                        var docId = Convert.ToInt32(dataItem.GetDataKeyValue("ID").ToString());
                        e.DetailTableView.DataSource = this.attachFileService.GetAllDocumentFileByDocId(docId); ;
                        break;
                    }
            }
        }
    }
}
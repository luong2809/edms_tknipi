﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCustomerDetails.aspx.cs" Inherits="EDMs.Web.Controls.Customers.ViewCustomerDetails" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }
    </script>
    <style type="text/css">
        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5
        {
            height: 100% !important;
        }

        #divContainerRight
        {
            width: 72%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted
        {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper
        {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip
        {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage
        {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow
        {
            z-index: 8000 !important;
        }
        .TemplateMenu
        {
            z-index: 10;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer1" >
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadTabStrip1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadMultiPage1" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadTabStrip1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadTabStrip1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadMultiPage1" LoadingPanelID="RadAjaxLoadingPanel2">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadMultiPage1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadMultiPage1" LoadingPanelID="RadAjaxLoadingPanel2">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />

    </span>
        <div style="width: 100%">
            <ul style="list-style-type: none">
                <li style="width: 500px; padding-bottom: 3px;">
                    <div style="border-style: solid; border-color: #E44B25;"></div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <span style="color: #E44B25;font-size:14px; ">THÔNG TIN CÁ NHÂN</span>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Mã KH:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblMaKH" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Ngày sinh:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblNgaySinh" runat="server" />&nbsp&nbsp&nbsp
                            <a href="#"  class="tooltip"><img alt="Sinh nhật" runat="server" id="imgBirthDay" visible="false" src="~/Images/birthday_16.png" />
                                <asp:Label ID="lblBirthday" runat="server" />
                            </a>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Chuyên ngành:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblNgheNghiep" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Địa chỉ:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblDiaChi" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Điện thoại:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblDienThoai" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Email:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblEmail" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">CMND:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblCMND" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Nơi cấp:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblNoiCap" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Ngày cấp:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblNgayCap" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Tình trạng:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblTinhTrang" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Ghi chú:
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:Label ID="lblGhiChu" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 800px; padding-top: 10px; padding-bottom: 3px;">
                    <div style="border-style: solid; border-color: #E44B25;"></div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 800px;">
                    <span style="color: #E44B25; font-size:14px;">THÔNG TIN DỊCH VỤ</span>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 1000px;">
                    <div class="exampleWrapper">
                    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" 
                        CssClass="multiPage">
                        <telerik:RadPageView ID="RadPageView1" runat="server" CssClass="EDMsRadPageView1">
                            <b>adjaksj dajslk lk jdkajsk djdjasj dlkajs </b>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server" CssClass="EDMsRadPageView2">
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView3" runat="server" CssClass="EDMsRadPageView3">
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView4" runat="server" CssClass="EDMsRadPageView4">
                        </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView5" runat="server" CssClass="EDMsRadPageView5">
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                    <telerik:RadTabStrip ID="RadTabStrip1" SelectedIndex="0" Width="100%"
                        CssClass="tabStrip" runat="server" MultiPageID="RadMultiPage1"
                        OnTabClick="RadTabStrip1_TabClick">
                        <Tabs>
                            <telerik:RadTab Text="Liên hệ">
                            </telerik:RadTab>
                            <telerik:RadTab Text="Tư vấn">
                            </telerik:RadTab>
                            <telerik:RadTab Text="Sử dụng dịch vụ">
                            </telerik:RadTab>
                            <telerik:RadTab Text="Thẻ thành viên">
                            </telerik:RadTab>
                            <telerik:RadTab Text="Ghi chú">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip></div>
                </li>

            </ul>
        </div>


    </form>
</body>
</html>

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Document;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using OfficeHelper.Utilities.Data;
using Telerik.Web.UI;
using CheckBox = System.Web.UI.WebControls.CheckBox;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// Class customer
    /// </summary>
    public partial class WorkGroupList1 : Page
    {
        private readonly PermissionService permissionService = new PermissionService();

        private readonly WorkGroupService workGroupService = new WorkGroupService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly RoleService  roleService = new RoleService();
        
        private readonly AreaService areaService = new AreaService();
        private readonly DisciplineService disciplineService = new DisciplineService();
        private readonly WorkpackageContentService workpackageContentService = new WorkpackageContentService();
        private readonly UserService userService = new UserService();
        private readonly WPRevisionService wpRevisionService = new WPRevisionService();

        private readonly EmailNotificationTemplateService emailNotificationTemplateService = new EmailNotificationTemplateService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        /// <summary>
        /// The unread pattern.
        /// </summary>
        //protected const string unreadPattern = @"\(\d+\)";

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!this.Page.IsPostBack)
            {
                if (UserSession.Current.IsEngineer)
                {
                    this.IsEngineer.Value = "true";
                    this.RadPane3.Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                }

                this.IsGip.Value = UserSession.Current.IsGip ? "true" : "false";
                this.LoadListPanel();
                this.LoadSystemPanel();
            }
             Response.Buffer = false;
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments()
        {
            var ddlShow = (DropDownList)this.CustomerMenu.Items[4].FindControl("ddlShow");

            var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC || UserSession.Current.IsSuperViewer;
            var currentDepartment = UserSession.Current.User.RoleId;
            var workPackageList = new List<WorkGroup>();

            if (isSeeFull)
            {
                workPackageList = this.workGroupService.GetAll();
            }
            else if (UserSession.Current.IsGip)
            {
                var prjectInPermission =
                    this.scopeProjectService.GetAll()
                        .Where(t => t.ProjectManagerId.GetValueOrDefault() == UserSession.Current.User.Id)
                        .Select(t => t.ID)
                        .ToList();

                workPackageList =
                    this.workGroupService.GetAll()
                        .Where(t => prjectInPermission.Contains(t.ProjectId.GetValueOrDefault()))
                        .ToList();
            }
            else if (UserSession.Current.IsCheif)
            {
                // Get all wp in department or have project in permission
                var prjectInPermission =
                    this.scopeProjectService.GetAllByProjectManager(UserSession.Current.User.Id).Select(t => t.ID).ToList();
                workPackageList = this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault(), prjectInPermission);
            }

            if (ddlShow.SelectedValue == "ShowUncomplete")
            {
                workPackageList = workPackageList.Where(t => t.Complete == null || t.Complete < 100).OrderByDescending(t => t.CreatedDate).ToList();

                // Special bussiness, user trongvv.rd need only see project of user Han(87)
                if (UserSession.Current.User.Username == "trongvv.rd")
                {
                    workPackageList = workPackageList.Where(t => t.ProjectManagerID == 87).ToList();
                }
            }
            else if (ddlShow.SelectedValue == "ShowOverDue")
            {
                workPackageList = workPackageList.Where(t => t.IsLater).OrderByDescending(t => t.CreatedDate).ToList();
            }
            else
            {
                workPackageList = workPackageList.OrderByDescending(t => t.CreatedDate).ToList();
            }

            this.grdDocument.DataSource = workPackageList;
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }

            else if (e.Argument == "ExportDocList")
            {
                var ddlShow = (DropDownList) this.CustomerMenu.Items[4].FindControl("ddlShow");
                var isSeeFull = UserSession.Current.IsAdmin || UserSession.Current.IsDC ||
                                UserSession.Current.IsSuperViewer;
                var currentDepartment = UserSession.Current.User.RoleId;
                var workPackageList = new List<WorkGroup>();

                if (isSeeFull)
                {
                    workPackageList = this.workGroupService.GetAll();
                }
                else if (UserSession.Current.IsGip)
                {
                    var prjectInPermission =
                        this.scopeProjectService.GetAll()
                            .Where(t => t.ProjectManagerId.GetValueOrDefault() == UserSession.Current.User.Id)
                            .Select(t => t.ID)
                            .ToList();

                    workPackageList =
                        this.workGroupService.GetAll()
                            .Where(t => prjectInPermission.Contains(t.ProjectId.GetValueOrDefault()))
                            .ToList();
                }
                else if (UserSession.Current.IsCheif)
                {
                    // Get all wp in department or have project in permission
                    var prjectInPermission =
                        this.scopeProjectService.GetAllByProjectManager(UserSession.Current.User.Id)
                            .Select(t => t.ID)
                            .ToList();
                    workPackageList = this.workGroupService.GetAllByDepartment(currentDepartment.GetValueOrDefault(),
                        prjectInPermission);
                }

                workPackageList = ddlShow.SelectedValue == "ShowOverDue"
                    ? workPackageList.Where(t => t.IsLater).OrderBy(t => t.Name).ToList()
                    : workPackageList.Where(t => t.Complete == null || t.Complete < 100).OrderBy(t => t.Name).ToList();

                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\UpdateDocFromWP.xls");
                var sheets = workbook.Worksheets;

                var dtFull = new DataTable();
                dtFull.Columns.AddRange(new[]
                {
                    new DataColumn("DocId", typeof (String)),
                    new DataColumn("NoIndex", typeof (String)),
                    new DataColumn("DocNo", typeof (String)),
                    new DataColumn("DocTitle", typeof (String)),
                    new DataColumn("Rev", typeof (String)),
                    new DataColumn("ManHour", typeof (String)),
                    new DataColumn("Start", typeof (String)),
                    new DataColumn("Deadline", typeof (String)),
                    new DataColumn("Fisnish", typeof (String)),
                    new DataColumn("IsoReviseDate", typeof (String)),
                    new DataColumn("Weight", typeof (Double)),
                    new DataColumn("Complete", typeof (Double)),
                });

                var countWP = 0;
                foreach (var workGroup in workPackageList)
                {
                    var count = 1;
                    var docList = this.documentPackageService.GetAllByWorkgroup(workGroup.ID);
                    if (docList.Count > 0)
                    {
                        dtFull.Rows.Clear();
                        sheets.AddCopy(0);
                        sheets[countWP + 1].Name = Utility.RemoveSpecialCharacter(workGroup.Name, "-");
                        var projectObj = this.scopeProjectService.GetById(workGroup.ProjectId.GetValueOrDefault());
                        sheets[countWP + 1].Cells["B5"].PutValue(workGroup.ID);
                        sheets[countWP + 1].Cells["E1"].PutValue(workGroup.ProjectName +
                                                                 (projectObj != null &&
                                                                  !string.IsNullOrEmpty(projectObj.Description)
                                                                     ? "\n(" + projectObj.Description + ")"
                                                                     : string.Empty));
                        sheets[countWP + 1].Cells["E3"].PutValue(workGroup.Name +
                                                                 (!string.IsNullOrEmpty(workGroup.Description)
                                                                     ? "\n(" + workGroup.Description + ")"
                                                                     : string.Empty));


                        foreach (var documentPackage in docList)
                        {
                            var dataRow = dtFull.NewRow();
                            dataRow["DocId"] = documentPackage.ID;
                            dataRow["NoIndex"] = count;
                            dataRow["DocNo"] = documentPackage.DocNo;
                            dataRow["DocTitle"] = documentPackage.DocTitle;
                            dataRow["Rev"] = documentPackage.RevisionName;
                            dataRow["ManHour"] = documentPackage.ManHours;
                            dataRow["Start"] = documentPackage.StartDate != null
                                ? documentPackage.StartDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Deadline"] = documentPackage.Deadline != null
                                ? documentPackage.Deadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Fisnish"] = documentPackage.EndDate != null
                                ? documentPackage.EndDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["IsoReviseDate"] = documentPackage.IsoReviseDate != null
                                ? documentPackage.IsoReviseDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Complete"] = documentPackage.Complete/100;
                            dataRow["Weight"] = documentPackage.Weight/100;

                            count += 1;
                            dtFull.Rows.Add(dataRow);
                        }

                        sheets[countWP + 1].Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, 12, true);
                        sheets[countWP + 1].AutoFitRows();
                        countWP += 1;
                    }
                }

                if (countWP > 0)
                {
                    sheets.RemoveAt(0);
                }

                var filename = "DocumentList " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, filename, true);
            }

            else if (e.Argument == "ExportMasterList")
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\WorkpackageMasterListTemplate.xls");
                var sheets = workbook.Worksheets;
                var readMeSheet = sheets[0];
                var dataSheet = sheets[1];

                var wpRevisionList = this.wpRevisionService.GetAll();
                var projectList = this.scopeProjectService.GetAll();
                var departmentList = this.roleService.GetAll(UserSession.Current.User.Id == 1);
                var areaList = this.areaService.GetAll();
                var disciplineList = this.disciplineService.GetAll();

                for (var i = 0; i < projectList.Count; i++)
                {
                    readMeSheet.Cells["B" + (7 + i)].PutValue(projectList[i].Name);
                    readMeSheet.Cells["C" + (7 + i)].PutValue(projectList[i].Description);
                }

                for (var i = 0; i < departmentList.Count; i++)
                {
                    readMeSheet.Cells["E" + (7 + i)].PutValue(departmentList[i].Name);
                    readMeSheet.Cells["F" + (7 + i)].PutValue(departmentList[i].Description);
                }

                for (var i = 0; i < areaList.Count; i++)
                {
                    readMeSheet.Cells["H" + (7 + i)].PutValue(areaList[i].Name);
                    readMeSheet.Cells["I" + (7 + i)].PutValue(areaList[i].Description);
                }

                for (var i = 0; i < disciplineList.Count; i++)
                {
                    readMeSheet.Cells["K" + (7 + i)].PutValue(disciplineList[i].Name);
                    readMeSheet.Cells["L" + (7 + i)].PutValue(disciplineList[i].Description);
                }

                for (var i = 0; i < wpRevisionList.Count; i++)
                {
                    readMeSheet.Cells["Q" + (7 + i)].PutValue(wpRevisionList[i].Name);
                    readMeSheet.Cells["R" + (7 + i)].PutValue(wpRevisionList[i].Description);
                }

                var rangeProjectList = readMeSheet.Cells.CreateRange("B7",
                    "B" + (7 + (projectList.Count == 0 ? 1 : projectList.Count)));
                rangeProjectList.Name = "ProjectList";

                var rangeDepartmentList = readMeSheet.Cells.CreateRange("E7",
                    "E" + (7 + (departmentList.Count == 0 ? 1 : departmentList.Count)));
                rangeDepartmentList.Name = "DepartmentList";

                var rangeAreaList = readMeSheet.Cells.CreateRange("H7",
                    "H" + (7 + (areaList.Count == 0 ? 1 : areaList.Count)));
                rangeAreaList.Name = "AreaList";

                var rangeDisciplineList = readMeSheet.Cells.CreateRange("K7",
                    "K" + (7 + (disciplineList.Count == 0 ? 1 : disciplineList.Count)));
                rangeDisciplineList.Name = "DisciplineList";

                var rangeTypeList = readMeSheet.Cells.CreateRange("N7", "N8");
                rangeTypeList.Name = "TypeList";

                var rangeWPRevisionList = readMeSheet.Cells.CreateRange("Q7",
                    "Q" + (7 + (wpRevisionList.Count == 0 ? 1 : wpRevisionList.Count)));
                rangeWPRevisionList.Name = "WPRevision";

                var validations = dataSheet.Validations;
                this.CreateValidation(rangeProjectList.Name, validations, 2, 1000, 1, 1);
                this.CreateValidation(rangeWPRevisionList.Name, validations, 2, 1000, 4, 4);

                this.CreateValidation(rangeDepartmentList.Name, validations, 2, 1000, 5, 5);
                this.CreateValidation(rangeAreaList.Name, validations, 2, 1000, 6, 6);
                this.CreateValidation(rangeDisciplineList.Name, validations, 2, 1000, 7, 7);
                this.CreateValidation(rangeTypeList.Name, validations, 2, 1000, 7, 8);

                readMeSheet.AutoFitRows();

                var filename = "WorkpackageInfoMasterList_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, filename, true);
            }
            else if (e.Argument.Contains("FTK02"))
            {
                var selectedWP =
                    this.workGroupService.GetById(!string.IsNullOrEmpty(this.lblWorkpackageId.Value)
                        ? Convert.ToInt32(this.lblWorkpackageId.Value)
                        : 0);
                if (selectedWP != null)
                {
                    var contentCount = 0;
                    var dataTable = new DataTable();
                    var reportInfo = new DataTable();

                    var ds = new DataSet();
                    var listColumn = new[]
                    {
                        new DataColumn("Index", Type.GetType("System.String")),
                        new DataColumn("WorkPackageContent", Type.GetType("System.String")),
                        new DataColumn("DeadLine", Type.GetType("System.String")),
                        new DataColumn("Note", Type.GetType("System.String"))
                    };
                    dataTable.Columns.AddRange(listColumn);

                    var listColumn1 = new[]
                    {
                        new DataColumn("DepartmentName", Type.GetType("System.String")),
                        new DataColumn("DepartmentDescription", Type.GetType("System.String")),
                        new DataColumn("ProjectDescription", Type.GetType("System.String")),
                        new DataColumn("ProjectName", Type.GetType("System.String")),
                        new DataColumn("WorkpackageName", Type.GetType("System.String")),
                        new DataColumn("WPStartDate", Type.GetType("System.String")),
                        new DataColumn("ProjectManager", Type.GetType("System.String")),
                        new DataColumn("InputDocument", Type.GetType("System.String")),
                        new DataColumn("InfoExchange", Type.GetType("System.String")),
                        new DataColumn("ReportMode", Type.GetType("System.String")),
                    };
                    reportInfo.Columns.AddRange(listColumn1);

                    var infoItem = reportInfo.NewRow();
                    var projectObj = this.scopeProjectService.GetById(selectedWP.ProjectId.GetValueOrDefault());
                    var projectManager =
                        this.userService.GetByID(projectObj != null
                            ? projectObj.ProjectManagerId.GetValueOrDefault()
                            : 0);
                    var deparment = this.roleService.GetByID(selectedWP.DepartmentId.GetValueOrDefault());

                    infoItem["DepartmentName"] = deparment != null
                        ? (e.Argument == "FTK02V" ? deparment.Name : deparment.RussiaName)
                        : string.Empty;
                    infoItem["DepartmentDescription"] = deparment != null
                        ? (e.Argument == "FTK02V" ? deparment.Description : string.Empty)
                        : string.Empty;
                    infoItem["ProjectDescription"] = projectObj != null ? projectObj.Description : string.Empty;
                    infoItem["ProjectName"] = projectObj != null ? projectObj.Name : string.Empty;
                    infoItem["WorkpackageName"] = selectedWP.Name;
                    infoItem["WPStartDate"] = selectedWP.StartDate != null
                        ? (e.Argument == "FTK02V"
                            ? "Ngày " + selectedWP.StartDate.GetValueOrDefault().Day + " Tháng " +
                              selectedWP.StartDate.GetValueOrDefault().Month + " Năm " +
                              selectedWP.StartDate.GetValueOrDefault().Year
                            : selectedWP.StartDate.GetValueOrDefault().ToString("dd/MM/yyyy"))
                        : string.Empty;

                    infoItem["ProjectManager"] = projectManager != null ? projectManager.FullName : string.Empty;
                    infoItem["InputDocument"] = selectedWP.InputDocument;
                    infoItem["InfoExchange"] = selectedWP.InformationExchange;
                    infoItem["ReportMode"] = selectedWP.ReportMode;
                    reportInfo.Rows.Add(infoItem);

                    // Add wpinfo row
                    var wpInfoRow = dataTable.NewRow();
                    wpInfoRow["WorkPackageContent"] = selectedWP.Description;
                    wpInfoRow["DeadLine"] = selectedWP.Deadline != null
                        ? selectedWP.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy")
                        : string.Empty;
                    dataTable.Rows.Add(wpInfoRow);

                    foreach (
                        var workpackageContent in this.workpackageContentService.GetAllByWorkpackage(selectedWP.ID)
                        )
                    {
                        var contentItem = dataTable.NewRow();
                        contentCount += 1;
                        contentItem["Index"] = contentCount;
                        contentItem["WorkPackageContent"] = workpackageContent.ContentInfo;
                        contentItem["DeadLine"] = workpackageContent.Deadline != null
                            ? workpackageContent.Deadline.GetValueOrDefault().ToString("dd/MM/yyyy")
                            : string.Empty;
                        contentItem["Note"] = workpackageContent.Note;
                        dataTable.Rows.Add(contentItem);
                    }

                    // Add some empty info row
                    for (int i = 0; i < 3; i++)
                    {
                        var emptyRow = dataTable.NewRow();
                        dataTable.Rows.Add(emptyRow);
                    }

                    ds.Tables.Add(dataTable);
                    ds.Tables[0].TableName = "Table";

                    var rootPath = Server.MapPath("../../Exports") + @"\";
                    const string WordPath = @"Template\";
                    const string WordPathExport = @"Generated\";
                    var StrTemplateFileName = e.Argument == "FTK02V"
                        ? "F-TK-02-V_Template.doc"
                        : "F-TK-02-R_Template.doc";
                    var strOutputFileName = "F-TK-02_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".doc";
                    var isSuccess = OfficeCommon.ExportToWordWithRegion(
                        rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, reportInfo, ds);
                    if (isSuccess)
                    {
                        this.DownloadByWriteByte(rootPath + WordPathExport + strOutputFileName,
                            "F-TK-02_" + Utility.RemoveSpecialCharacter(selectedWP.Name, "-") + ".doc", true);
                    }
                }
            }
            else if (e.Argument.Contains("ExportCMDR"))
            {
                var selectedWP =
                    this.workGroupService.GetById(!string.IsNullOrEmpty(this.lblWorkpackageId.Value)
                        ? Convert.ToInt32(this.lblWorkpackageId.Value)
                        : 0);
                if (selectedWP != null)
                {

                    var dtFull = new DataTable();
                    dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocId", typeof (String)),
                        new DataColumn("NoIndex", typeof (String)),
                        new DataColumn("DocNo", typeof (String)),
                        new DataColumn("DocTitle", typeof (String)),
                        new DataColumn("Rev", typeof (String)),
                        new DataColumn("ManHour", typeof (String)),
                        new DataColumn("Start", typeof (String)),
                        new DataColumn("Deadline", typeof (String)),
                        new DataColumn("Fisnish", typeof (String)),
                        new DataColumn("IsoReviseDate", typeof (String)),
                        new DataColumn("Weight", typeof (Double)),
                        new DataColumn("Complete", typeof (Double)),
                        new DataColumn("OutgoingLeterNo", typeof (String)),
                        new DataColumn("OutgoingLeterDate", typeof (String)),
                    });
                    var filePath = Server.MapPath("~/Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\CMDRReportTemplate_Report.xls");
                    var sheets = workbook.Worksheets;

                    sheets[0].Cells["E1"].PutValue("DETAIL ENGINEERING SERVICE " + selectedWP.Name + " WORK PACKAGE");
                    sheets[0].Name = selectedWP.Name;
                    sheets[0].Cells["C7"].PutValue(selectedWP.Name);
                    sheets[0].Cells["B7"].PutValue(selectedWP.ProjectId + "," + selectedWP.ID);
                    sheets[0].Cells["O4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));

                    var count = 1;
                    var docList = this.documentPackageService.GetAllByWorkgroup(selectedWP.ID)
                        .OrderBy(t => t.DocNo)
                        .ToList();

                    var listDocumentTypeId =
                        docList.Select(t => Convert.ToInt32(t.DocumentTypeId)).Distinct().OrderBy(t => t).ToList();
                    var complete = selectedWP.Complete;
                    var weight = selectedWP.Weight;
                    complete = 0;
                    weight = 0;
                    foreach (var documentTypeId in listDocumentTypeId)
                    {
                        var documentType = this.documentTypeService.GetById(documentTypeId);

                        var dataRow = dtFull.NewRow();
                        dataRow["DocId"] = -1;
                        dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                        dtFull.Rows.Add(dataRow);

                        var listDocByDocType = docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                        foreach (var documentPackage in listDocByDocType)
                        {
                            dataRow = dtFull.NewRow();
                            dataRow["DocId"] = documentPackage.ID;
                            dataRow["NoIndex"] = count;
                            dataRow["DocNo"] = documentPackage.DocNo;
                            dataRow["DocTitle"] = documentPackage.DocTitle;
                            dataRow["Rev"] = documentPackage.RevisionName;
                            dataRow["ManHour"] = documentPackage.ManHours;
                            dataRow["Start"] = documentPackage.StartDate != null
                                ? documentPackage.StartDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Deadline"] = documentPackage.Deadline != null
                                ? documentPackage.Deadline.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Fisnish"] = documentPackage.EndDate != null
                                ? documentPackage.EndDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["IsoReviseDate"] = documentPackage.IsoReviseDate != null
                                ? documentPackage.IsoReviseDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;
                            dataRow["Weight"] = documentPackage.Weight != null ? documentPackage.Weight/100 : 0/100;
                            dataRow["Complete"] = documentPackage.Complete != null
                                ? documentPackage.Complete/100
                                : 0/100;
                            dataRow["OutgoingLeterNo"] = documentPackage.OutgoingLeterNo;
                            dataRow["OutgoingLeterDate"] = documentPackage.OutgoingLeterDate != null
                                ? documentPackage.OutgoingLeterDate.Value.ToString("dd/MM/yyyy")
                                : string.Empty;

                            count += 1;
                            dtFull.Rows.Add(dataRow);
                            if (documentPackage.Complete != null && documentPackage.Weight != null)
                            {
                                complete += (documentPackage.Complete/100)*(documentPackage.Weight/100);
                                weight += documentPackage.Weight/100;
                            }

                        }
                    }
                    sheets[0].Cells["A7"].PutValue(dtFull.Rows.Count);
                    sheets[0].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, 15, true);
                    sheets[1].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, 15, true);
                    sheets[0].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");



                    sheets[0].Cells[7 + dtFull.Rows.Count, 12].PutValue(complete);

                    sheets[0].Cells[7 + dtFull.Rows.Count, 11].PutValue(weight);

                    sheets[0].AutoFitRows();
                    sheets[1].IsVisible = false;


                    var filename = selectedWP.ProjectName + " - " + selectedWP.Name + " EMDR Report " +
                                   DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    filename = filename.Replace(@"\", " ").Replace("/", " ");
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            this.LoadDocuments();
        }

        /// <summary>
        /// Grid KhacHang item created
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem)
            {
                var item = e.Item as GridEditableItem;
                var btnDelete = item["DeleteColumn"].Controls[0] as ImageButton;
                if (btnDelete != null)
                {
                    if (item["CanDelete"].Text.ToLower() == "true")
                    {
                        btnDelete.ImageUrl = "~/Images/delete.png";
                        btnDelete.Enabled = true;
                    }
                    else
                    {
                        btnDelete.ImageUrl = "~/Images/deleteDisable.png";
                        btnDelete.Enabled = false;
                    }
                }
            }
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var wpId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var wpObj = this.workGroupService.GetById(wpId);
            var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
            this.workGroupService.Delete(wpId);

            // Update total workpackage weight for project after delete wp.
            if (projectObj != null)
            {
                projectObj.TotalWorkpackageWeight -= wpObj.Weight;

                var wpList = this.workGroupService.GetAllWorkGroupOfProject(projectObj.ID);
                if (wpList.Count == 0)
                {
                    projectObj.CanDelete = true;
                }

                this.scopeProjectService.Update(projectObj);
            }

            // Send notification
            if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
            {
                this.NotificationDeleteWp(wpObj);
            }


            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["IsLater"].Text.ToLower() == "true")
                {
                    item.BackColor = Color.Crimson;
                    item.BorderColor = Color.Crimson;
                    //item.ForeColor = Color.White;
                }
            }
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(),
                listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() {Id = -1, MenuName = "LIST"});

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"~/Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    //if (item.Text == "Originator")
                    //{
                    //    item.Selected = true;
                    //}
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void CreateValidation(string formular, Validations objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = Aspose.Cells.ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;
            validation.ShowInput = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";
            validation.InputTitle = "Warning";

            // Set the error message.
            validation.ErrorMessage = "Please select item from the list";
            validation.InputMessage = "Please select item from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                var fs = new FileStream(strFileName, FileMode.Open);
                var streamLength = Convert.ToInt32(fs.Length);
                var data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        //protected void ckbShowUncomplete_CheckedChange(object sender, EventArgs e)
        //{
        //    var ckbShowLater = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowLater");
        //    var cbShowAll = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowAll");
        //    var cbShowUncomplete = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowUncomplete");
        //    cbShowAll.Checked = !cbShowUncomplete.Checked;
        //    ckbShowLater.Checked = !cbShowUncomplete.Checked;

        //    this.grdDocument.Rebind();
        //}

        //protected void ckbShowAll_CheckedChange(object sender, EventArgs e)
        //{
        //    var ckbShowLater = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowLater");
        //    var cbShowAll = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowAll");
        //    var cbShowUncomplete = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowUncomplete");
        //    cbShowUncomplete.Checked = !cbShowAll.Checked;
        //    ckbShowLater.Checked = !cbShowAll.Checked;

        //    this.grdDocument.Rebind();
        //}

        //protected void ckbShowLater_CheckedChange(object sender, EventArgs e)
        //{
        //    var ckbShowLater = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowLater");
        //    var cbShowAll = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowAll");
        //    var cbShowUncomplete = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowUncomplete");
        //    cbShowUncomplete.Checked = !ckbShowLater.Checked;
        //    cbShowAll.Checked = !ckbShowLater.Checked;

        //    this.grdDocument.Rebind();
        //}

        protected void ddlShow_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        private void NotificationDeleteWp(WorkGroup wpObj)
        {
            if (wpObj.DepartmentId != 0)
            {
                var projectObj = this.scopeProjectService.GetById(wpObj.ProjectId.GetValueOrDefault());
                var gipUser = this.userService.GetByID(projectObj != null ? projectObj.ProjectManagerId.GetValueOrDefault() : 0);
                var emailList = this.userService.GetAllByRoleId(wpObj.DepartmentId.GetValueOrDefault())
                    .Where(t => t.IsChief.GetValueOrDefault())
                    .Select(t => t.Email)
                    .Where(t => !string.IsNullOrEmpty(t)).ToList();

                if (gipUser != null && !string.IsNullOrEmpty(gipUser.Email))
                {
                    emailList.Add(gipUser.Email);
                }

                var notificationTemplate = this.emailNotificationTemplateService.GetByType(Utility.DeleteWP);
                if (emailList.Count > 0 && notificationTemplate != null && notificationTemplate.IsActive.GetValueOrDefault())
                {
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };

                    var wpDeletedUser = this.userService.GetByID(UserSession.Current.User.Id);


                    var subject = notificationTemplate.Subject.Replace("#WPName#", wpObj.Name)
                        .Replace("#WPDeletedUser#", wpDeletedUser != null ? wpDeletedUser.FullName : string.Empty);

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    message.Body = notificationTemplate.Contents
                        .Replace("#WPNumber#", wpObj.Name)
                        .Replace("#WPDeletedUser#", wpDeletedUser != null ? wpDeletedUser.FullName : string.Empty)
                        .Replace("#WPProjectNumber#", projectObj != null ? projectObj.Name : string.Empty)
                        .Replace("#WPProjectName#", projectObj != null ? projectObj.Description : string.Empty)
                        .Replace("#WPName#", wpObj.Description)
                        .Replace("#WPStartDate#", wpObj.StartDate != null ? wpObj.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPDeadline#", wpObj.Deadline != null ? wpObj.Deadline.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPFinishDate#", wpObj.EndDate != null ? wpObj.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                        .Replace("#WPIncomingNo#", wpObj.IncomingNo)
                        .Replace("#WPOutgoingNo#", wpObj.OutgoingNo)
                        .Replace("#WPWeight#", wpObj.Weight != null ? wpObj.Weight.Value.ToString() : string.Empty)
                        .Replace("#WPComplete#", wpObj.Complete != null ? wpObj.Complete.Value.ToString() : string.Empty);

                    foreach (var to in emailList)
                    {
                        message.To.Add(new MailAddress(to));
                    }

                    smtpClient.Send(message);
                }
            }
        }
    }
}


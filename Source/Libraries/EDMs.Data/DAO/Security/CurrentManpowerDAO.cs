﻿
namespace EDMs.Data.DAO.Security
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

  public   class CurrentManpowerDAO :BaseDAO
    {
      public CurrentManpowerDAO() : base() { }

        #region GET (Basic)
        public IQueryable<CurrentManpower> GetIQueryable()
        {
            return this.EDMsDataContext.CurrentManpowers;
        }
        
        public List<CurrentManpower> GetAll()
        {
            return this.EDMsDataContext.CurrentManpowers.ToList();
        }

        public CurrentManpower GetDepartment(int departmentid, int year)
        {
            return this.EDMsDataContext.CurrentManpowers.FirstOrDefault(ob=> ob.DepartmentId==departmentid && ob.Year==year);
        }
      
        public CurrentManpower GetByID(int ID)
        {
            return this.EDMsDataContext.CurrentManpowers.FirstOrDefault(ob => ob.ID == ID);
        }
       
        #endregion

        #region Get (Advances)

        #endregion

        #region Insert, Update, Delete
        public bool Insert(CurrentManpower ob)
        {
            try
            {
                this.EDMsDataContext.AddToCurrentManpowers(ob);
                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(CurrentManpower src)
        {
            try
            {
                CurrentManpower des;

                des = (from rs in this.EDMsDataContext.CurrentManpowers
                       where rs.ID == src.ID
                       select rs).First();

                des.Year = src.Year;
                des.Values = src.Values;
                des.DepartmentId = src.DepartmentId;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(CurrentManpower ob)
        {
            try
            {
                CurrentManpower _ob = this.GetByID(ob.ID);
                if (_ob != null)
                {
                    this.EDMsDataContext.DeleteObject(_ob);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                CurrentManpower _ob = this.GetByID(ID);
                if (_ob != null)
                {
                    this.EDMsDataContext.DeleteObject(_ob);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttachDocToTransmittalDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class AttachDocToTransmittalDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AttachDocToTransmittalDAO"/> class.
        /// </summary>
        public AttachDocToTransmittalDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<AttachDocToTransmittal> GetIQueryable()
        {
            return this.EDMsDataContext.AttachDocToTransmittals;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AttachDocToTransmittal> GetAll()
        {
            return this.EDMsDataContext.AttachDocToTransmittals.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public AttachDocToTransmittal GetById(int id)
        {
            return this.EDMsDataContext.AttachDocToTransmittals.FirstOrDefault(ob => ob.ID == id);
        }

        public List<AttachDocToTransmittal> GetAllByTransId(int tranid)
        {
            return this.EDMsDataContext.AttachDocToTransmittals.Where(ob => ob.TransmittalId == tranid).ToList();
        }

        public AttachDocToTransmittal GetByDoc(int transId, int docId)
        {
            return this.EDMsDataContext.AttachDocToTransmittals.FirstOrDefault(t => t.TransmittalId == transId && t.DocumentId == docId);
        }

        public bool IsExist(int transId, int docId)
        {
            return this.EDMsDataContext.AttachDocToTransmittals.Any(t => t.TransmittalId == transId && t.DocumentId == docId);
        }

        public List<AttachDocToTransmittal> GetAllByDocId(int docId)
        {
            return this.EDMsDataContext.AttachDocToTransmittals.Where(t => t.DocumentId == docId).ToList();
        }

        #endregion

        #region GET ADVANCE

        public AttachDocToTransmittal GetByDocument(int id)
        {
            return this.EDMsDataContext.AttachDocToTransmittals.OrderByDescending(ob => ob.ID).FirstOrDefault(ob => ob.DocumentId == id);
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see>
        ///       <cref>int?</cref>
        ///     </see> .
        /// </returns>
        public int? Insert(AttachDocToTransmittal ob)
        {
            try
            {
                this.EDMsDataContext.AddToAttachDocToTransmittals(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(AttachDocToTransmittal src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(AttachDocToTransmittal src)
        {
            try
            {
                AttachDocToTransmittal des = (from rs in this.EDMsDataContext.AttachDocToTransmittals
                                          where rs.ID == src.ID
                                          select rs).First();

                des.TransmittalId = src.TransmittalId;
                des.DocumentId = src.DocumentId;
                des.StepStatus = src.StepStatus;
                des.DurationStep = src.DurationStep;
                des.CopyNumber = src.CopyNumber;
                des.OriginalNumber = src.OriginalNumber;
                des.TypeID = src.TypeID;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentPackageInfoEditForm.aspx.cs" Inherits="EDMs.Web.Controls.Document.DocumentPackageInfoEditForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, form {
            overflow: visible;
        }

        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
            margin: 0 0px;
        }

        /*.RadComboBox .rcbInputCell .rcbInput,   .RadComboBox table td.rcbInputCell {
            border-left-color: #FF0000 !important;
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 283px;
        }*/

        .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
            padding-left: 0px !important;
        }

        div.rgEditForm label {
            float: right;
            text-align: right;
            width: 72px;
        }

        .rgEditForm {
            text-align: right;
        }

        .RadComboBox {
            border-bottom: none !important;
        }

        .RadUpload .ruFileWrap {
            overflow: visible !important;
        }

        .accordion dt a {
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .accordion dt span {
            color: #085B8F;
            border-bottom: 1px solid #46A3D3;
            font-size: 10pt;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .MyModalPanel {
            position: center;
            z-index: 3;
            top: 0;
            left: 0;
            width: 200px;
            height: 200px;
            margin: 0;
            padding: 0;
            background: #ffa url(loading.gif) center center no-repeat;
        }
    </style>

    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

    <script type="text/javascript">

        function GetRadWindow() {
            var oWindow = null;

            if (window.radWindow)
                oWindow = window.radWindow;
            else if (window.frameElement.radWindow)
                oWindow = window.frameElement.radWindow;

            return oWindow;
        }

        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }
    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" Skin="Silk" />
        <div style="width: 100%">
            <ul style="list-style-type: none">
                <div class="qlcbFormItem">
                    <div class="dnnFormMessage dnnFormInfo">
                        <div class="dnnFormItem dnnFormHelp dnnClear">
                            <p class="dnnFormRequired" style="float: left;">
                                <span style="text-decoration: underline;">Notes</span>: All fields marked with a red are required.
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
            </ul>
        </div>

        <div style="width: 100%; height: 100%" runat="server" id="divContent">
            <ul style="list-style-type: none">
                <dl class="accordion">
                    <dt style="width: 100%;">
                        <span>General Details</span>
                    </dt>
                </dl>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Project
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <%--  <asp:TextBox ID="txtProject" runat="server" Style="width: 300px;" CssClass="min25Percent" ReadOnly="True"/>--%>
                            <telerik:RadComboBox ID="ddlProject" runat="server"
                                Skin="Windows7" Width="300px" AutoPostBack="True" AllowCustomText="True" MarkFirstMatch="True" CssClass="min25Percent"
                                OnItemDataBound="ddlProject_ItemDataBound"
                                OnSelectedIndexChanged="ddlProject_SelectedIndexChanged" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Workpackage
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlWorkgroup" runat="server" CssClass="min25Percent" Width="316px"
                                OnSelectedIndexChanged="ddlWorkgroup_OnSelectedIndexChanged" AutoPostBack="True" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Document Number
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divDocNo">
                            <asp:TextBox ID="txtDocNumber" runat="server" Style="width: 300px;" CssClass="min25Percent qlcbFormRequired" />
                            <asp:CustomValidator runat="server" ID="fileNameValidator" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModuleEdit1"
                                OnServerValidate="ServerValidationFileNameIsExist" Display="Dynamic" ControlToValidate="txtDocNumber" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Document Title
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtDocumentTitle" runat="server" Style="width: 300px;" CssClass="min25Percent"
                                TextMode="MultiLine" Rows="5" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>


                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Document Type
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlDocType" runat="server" CssClass="min25Percent" Width="316px" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Revision
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divDocNo1">
                            <asp:DropDownList ID="ddlRevision" runat="server" CssClass="min25Percent qlcbFormRequired" Width="316px" AutoPostBack="true" OnSelectedIndexChanged="ddlRevision_SelectedIndexChanged" />
                            <asp:CustomValidator runat="server" ID="Rivvalidate" CssClass="dnnFormMessage dnnFormErrorModuleEdit1"
                                OnServerValidate="Rivvalidate_ServerValidate" Display="Dynamic" ControlToValidate="ddlRevision" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <asp:Panel ID="pnStatus" runat="server">
                    <li style="width: 500px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Status
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="min25Percent qlcbFormRequired" Width="316px" Enabled="false" />
                                <%--<asp:TextBox ID="txtStatus" runat="server" Style="width: 300px;" CssClass="min25Percent" />--%>
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>
                </asp:Panel>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Engineers
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divDocNo2">
                            <telerik:RadComboBox ID="ddlEngineer" runat="server" InputCssClass="min25Percent qlcbFormRequired" Width="315px" Skin="Metro" CheckBoxes="true"></telerik:RadComboBox>
                            <%--<asp:CustomValidator runat="server" ID="EngineeringValidate" CssClass="dnnFormMessage dnnFormErrorModuleEdit1"
                                    OnServerValidate="EngineeringValidate_ServerValidate" Display="Dynamic" ControlToValidate="ddlEngineer" />--%>
                            <asp:RequiredFieldValidator ID="engvalidate" runat="server" ControlToValidate="ddlEngineer" Display="Dynamic" CssClass="" ErrorMessage="Please choose an Employee's."></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Leader
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divChecker">
                            <asp:DropDownList ID="ddlChecker" CssClass="min25Percent qlcbFormRequired" runat="server" Width="316px"></asp:DropDownList>
                            <asp:CustomValidator runat="server" ID="CheckerValidate" CssClass="dnnFormMessage dnnFormErrorModuleEdit1"
                                OnServerValidate="CheckerValidate_ServerValidate" Display="Dynamic" ControlToValidate="ddlChecker" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Persons Involved
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtPersonsInvolved" runat="server" Style="width: 300px;" CssClass="min25Percent"
                                TextMode="MultiLine" Rows="2" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Weight
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadNumericTextBox Type="Percent" ID="txtWeight" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="316px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" NegativePattern="n %"></NumberFormat>
                            </telerik:RadNumericTextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Man hour Plan
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="Divmahour">
                            <telerik:RadNumericTextBox Type="Number" ID="txtManHourplan" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="316px" CssClass="min25Percent qlcbFormRequired">
                                <NumberFormat DecimalDigits="2"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <asp:CustomValidator runat="server" ID="manhoursplanvalidate" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModuleEdit1"
                                OnServerValidate="manhoursplanvalidate_ServerValidate" Display="Dynamic" ControlToValidate="txtManHourplan" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Man hour Actual
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadNumericTextBox Type="Number" ID="txtManHour" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="316px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2"></NumberFormat>
                            </telerik:RadNumericTextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Level Priority
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="level" runat="server" CssClass="min25Percent" Width="316px" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Note
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtNote" runat="server" Style="width: 300px;" CssClass="min25Percent"
                                TextMode="MultiLine" Rows="2"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <dl class="accordion">
                    <dt style="width: 100%;">
                        <span>Progress Details</span>
                    </dt>
                </dl>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Start date
                            </span>
                        </label>
                        <div style="float: left; padding-bottom: 2px; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divDocNo3">
                            <telerik:RadDatePicker ID="txtStartDate" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormRequired" />
                            </telerik:RadDatePicker>
                            <asp:CustomValidator runat="server" ID="startdatevalidate" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModuleEdit1"
                                OnServerValidate="startdatevalidate_ServerValidate" Display="Dynamic" ControlToValidate="txtStartDate" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 550px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Deadline
                            </span>
                        </label>
                        <div style="float: left; padding-bottom: 2px; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divDocNo4">
                            <telerik:RadDatePicker ID="txtDeadline" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormRequired" />
                            </telerik:RadDatePicker>
                            <asp:CustomValidator runat="server" ID="DeadlineValidate" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModuleEdit1"
                                OnServerValidate="DeadlineValidate_ServerValidate" Display="Dynamic" ControlToValidate="txtDeadline" />
                        </div>
                    </div>
                    <div>
                        <label style="float: left; padding-left: 20px; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Finish date
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtEndDate" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Complete-%
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="Divcomplete">
                            <telerik:RadNumericTextBox Type="Percent" ID="txtComplete" runat="server" Style="min-width: 0px !important; border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;" Width="135px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" NegativePattern="n %"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <asp:CustomValidator runat="server" ID="Validatorcomplete" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModuleEdit1"
                                OnServerValidate="ServerValidatecomplete" Display="Dynamic" ControlToValidate="txtComplete" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <%--<li style="width: 500px;">
                        
                        <div style="clear: both; font-size: 0;"></div>
                    </li>--%>

                <li style="width: 500px; display: none">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">ISO Revise date
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtISODate" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <asp:Panel ID="pnIDC" runat="server" Visible="false">
                    <li style="width: 550px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">IDC Deadline
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divIDCDeadline">
                                <telerik:RadDatePicker ID="txtIDCDeadline" runat="server"
                                    ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                    <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormRequired" />
                                </telerik:RadDatePicker>
                                <asp:CustomValidator runat="server" ID="IDCDeadlineValiddator" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModuleEdit1"
                                    OnServerValidate="IDCDeadlineValiddator_ServerValidate" Display="Dynamic" ControlToValidate="txtIDCDeadline" />
                            </div>
                        </div>
                        <div>
                            <label style="float: left; padding-left: 20px; padding-top: 3px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Finish date
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <telerik:RadDatePicker ID="txtIDCEndDate" runat="server"
                                    ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                    <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                </telerik:RadDatePicker>
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>
                    <%--<li style="width: 500px;">
                            
                            <div style="clear: both; font-size: 0;"></div>
                        </li>--%>
                    <li style="width: 550px;">
                        <div>
                            <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">IFR Deadline
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divIFRDeadline">
                                <telerik:RadDatePicker ID="txtIFRDeadline" runat="server"
                                    ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                    <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormRequired" />
                                </telerik:RadDatePicker>
                                <%--<asp:CustomValidator runat="server" ID="IFRDeadlineValidator" ValidateEmptyText="True" CssClass="dnnFormMessage dnnFormErrorModuleEdit1"
                                        OnServerValidate="IFRDeadlineValidator_ServerValidate" Display="Dynamic" ControlToValidate="txtIFRDeadline" />--%>
                            </div>
                        </div>
                        <div>
                            <label style="float: left; padding-left: 20px; padding-top: 3px; padding-right: 10px; text-align: right;">
                                <span style="color: #2E5689; text-align: right;">Finish date
                                </span>
                            </label>
                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                <telerik:RadDatePicker ID="txtIFREndDate" runat="server"
                                    ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                    <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                                </telerik:RadDatePicker>
                            </div>
                        </div>
                        <div style="clear: both; font-size: 0;"></div>
                    </li>
                    <%--<li style="width: 500px;">
                            
                            <div style="clear: both; font-size: 0;"></div>
                        </li>--%>
                </asp:Panel>
                <dl class="accordion">
                    <dt style="width: 100%;">
                        <span>OUTGOING TRANSMITTAL</span>
                    </dt>
                </dl>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">No.
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtOutgoingTransNo" runat="server" Style="width: 300px;" CssClass="min25Percent" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Date
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtOutgoingTransDate" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <dl class="accordion">
                    <dt style="width: 100%;">
                        <span>OUTGOING LETER TO XDCB</span>
                    </dt>
                </dl>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">No.
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtOutgoingLeterNo" runat="server" Style="width: 300px;" CssClass="min25Percent" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Date 
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtOutgoingLeterDate" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Copy No.
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtCopyNumber" runat="server" Style="width: 300px;" CssClass="min25Percent" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <dl class="accordion" style="display: none">
                    <dt style="width: 100%;">
                        <span>INCOMING TRANSMITTAL</span>
                    </dt>
                </dl>

                <li style="width: 500px; display: none">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">No.
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtIncomingTransNo" runat="server" Style="width: 300px;" CssClass="min25Percent" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px; display: none">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Date
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtIncomingTransDate" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px; display: none">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Code
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtFinalcode" runat="server" Style="width: 100px;" CssClass="min25Percent" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <dl class="accordion">
                    <dt style="width: 100%;">
                        <span>ICA REVIEW DETAILS</span>
                    </dt>
                </dl>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">No.
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txticatransno" runat="server" Style="width: 300px;" CssClass="min25Percent" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Date
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txticadate" runat="server"
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" CssClass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Review Code
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtreviewcode" runat="server" Style="width: 300px;" CssClass="min25Percent" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Markup
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:TextBox ID="txtmarkup" runat="server" Style="width: 300px;" CssClass="min25Percent" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;"></span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:CheckBox ID="cbIsEMDR" Checked="true" runat="server" Text="Is EMDR" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <div class="qlcbFormItem" runat="server" id="CreatedInfo" visible="False">
                    <div class="dnnFormMessage dnnFormInfo">
                        <div class="dnnFormItem dnnFormHelp dnnClear">
                            <p class="dnnFormRequired" style="float: left;">
                                <asp:Label ID="lblCreated" runat="server"></asp:Label>
                                <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
            </ul>
        </div>
        <div runat="server" id="divExecuted">
            <ul style="list-style-type: none">
                <li style="width: 500px; padding-top: 10px; padding-bottom: 3px; text-align: center">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" Width="70px" Style="text-align: center">
                        <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                </li>
                <li style="width: 400px;" runat="server" id="blockError" visible="False">
                    <div>
                        <label style="width: 60px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: red; text-align: right;">Notification:
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:Label runat="server" ID="lblError" Width="300px"></asp:Label>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
            </ul>
        </div>
        <asp:HiddenField runat="server" ID="docUploadedIsExist" />
        <asp:HiddenField runat="server" ID="docIdUpdateUnIsLeaf" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Width="100%" Height="100px">
            <asp:Image ID="Image1" runat="server" Width="160px" Height="20px" ImageUrl="~/Images/loading1.gif"></asp:Image>
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument" DefaultLoadingPanelID="RadAjaxLoadingPanel2">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" />
                        <telerik:AjaxUpdatedControl ControlID="divExecuted" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlWorkgroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ddlEngineer" />
                        <telerik:AjaxUpdatedControl ControlID="txtDocNumber" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;
                function adjustLoadingPanelHeight() {
                    // the following Javascript code takes care of expanding the RadAjaxLoadingPanel
                    // to the full height of the page, if it is more than the browser window viewport

                    var loadingPanel = $get("<%= RadAjaxLoadingPanel2.ClientID %>");
                    var pageHeight = document.documentElement.scrollHeight;
                    var viewportHeight = document.documentElement.clientHeight;

                    if (pageHeight > viewportHeight) {
                        loadingPanel.style.height = pageHeight + "px";
                    }

                    var pageWidth = document.documentElement.scrollWidth;
                    var viewportWidth = document.documentElement.clientWidth;

                    if (pageWidth > viewportWidth) {
                        loadingPanel.style.width = pageWidth + "px";
                    }

                    // the following Javascript code takes care of centering the RadAjaxLoadingPanel
                    // background image, taking into consideration the scroll offset of the page content

                    if ($telerik.isSafari) {
                        var scrollTopOffset = document.body.scrollTop;
                        var scrollLeftOffset = document.body.scrollLeft;
                    }
                    else {
                        var scrollTopOffset = document.documentElement.scrollTop;
                        var scrollLeftOffset = document.documentElement.scrollLeft;
                    }

                    var loadingImageWidth = 55;
                    var loadingImageHeight = 55;

                    loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
                }

                function pageLoad() {
                    ajaxManager = $find("<%=ajaxDocument.ClientID %>");
                }

                function StopPropagation(e) {
                    if (!e) {
                        e = window.event;
                    }

                    e.cancelBubble = true;
                }
            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>

﻿
/////


//////
namespace EDMs.Business.Services.Document
{

    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;
    using System.Globalization;


    public class WorkgroupManhourMonthService
    {
        private readonly WorkgroupManhourMonthDAO repo;

        public WorkgroupManhourMonthService()
        {
            this.repo = new WorkgroupManhourMonthDAO();
        }

        public List<WorkgroupManhourMonth> GetAll()
        {
            return this.repo.GetAll().ToList();

        }

        public WorkgroupManhourMonth GetById(int id)
        {
            return this.repo.GetById(id);
        }

        public WorkgroupManhourMonth GetByWP(int wpID)
        {
            return this.repo.GetByWP(wpID);
        }

        public List<WorkgroupManhourMonth> GetByMonthAndYear(int month, int year)
        {
            return this.repo.GetByMonthAndYear(month, year);
        }

        public WorkgroupManhourMonth GetByWP(int wpID, int month, int year)
        {
            return this.repo.GetByWP(wpID, month, year);
        }

        public List<WorkgroupManhourMonth> GetByPJ(int pjID, int month, int year)
        {
            return this.repo.GetByPJ(pjID, month, year);
        }

        public List<WorkgroupManhourMonth> GetByPJ(int pjID, int departmentID, int month, int year)
        {
            return this.repo.GetByPJ(pjID, departmentID, month, year);
        }

        public List<WorkgroupManhourMonth> GetLastedByPJ(int pjID)
        {
            return this.repo.GetLastedByPJ(pjID);
        }
        public WorkgroupManhourMonth GetLastedByPJAndWP(int pjID, int wpID, int month, int year)
        {
            DateTime date = new DateTime(year, month, 1);
            var temp = this.repo.GetLastedByPJAndWP(pjID, wpID);
            return temp.FirstOrDefault(t => new DateTime(t.Year.GetValueOrDefault(), t.Month.GetValueOrDefault(), 1) < date);
        }

        public List<WorkgroupManhourMonth> GetByPJ(List<int> pjID, int year)
        {
            return this.repo.GetByPJ(pjID, year);
        }

        public List<WorkgroupManhourMonth> GetByWPAndPJ(int wpID, int pjID)
        {
            return this.repo.GetByWPAndPJ(wpID, pjID);
        }

        public WorkgroupManhourMonth GetByWPAndPJ(int wpID, int pjID, int month, int year)
        {
            return this.repo.GetByWPAndPJ(wpID, pjID, month, year);
        }

        public List<WorkgroupManhourMonth> GetByDP(int dpID, int month, int year)
        {
            return this.repo.GetByDP(dpID, month, year);
        }

        public List<WorkgroupManhourMonth> GetByDP(int dpID, int year)
        {
            return this.repo.GetByDP(dpID, year);
        }

        public int? Insert(WorkgroupManhourMonth bo)
        {
            return this.repo.Insert(bo);
        }


        public bool Update(WorkgroupManhourMonth bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(WorkgroupManhourMonth bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

﻿using EDMs.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EDMs.Data.DAO
{
    public class Patient_MemberCardDAO:BaseDAO
    {
        public Patient_MemberCardDAO() : base() { }

        #region GET (Basic)
        public IQueryable<MemberCard> GetIQueryable()
        {
            return EDMsDataContext.MemberCards;
        }
        
        public List<MemberCard> GetAll()
        {
            return EDMsDataContext.MemberCards.ToList<MemberCard>();
        }

        public MemberCard GetByID(int ID)
        {
            return EDMsDataContext.MemberCards.Where(ob => ob.ID == ID).FirstOrDefault<MemberCard>();
        }

       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// Find By Any expression
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<MemberCard> Find(Expression<Func<MemberCard, bool>> where)
        {
            return EDMsDataContext.MemberCards.Where(where).ToList<MemberCard>();
        }
        #endregion

        #region Insert, Update, Delete
        public bool Insert(MemberCard ob)
        {
            try
            {
                EDMsDataContext.AddToMemberCards(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(MemberCard ob)
        {
            try
            {
                MemberCard _ob;

                _ob = (from rs in EDMsDataContext.MemberCards
                       where rs.ID == ob.ID
                       select rs).First();

                _ob.UserID = ob.UserID;
                _ob.CustomerID = ob.CustomerID;
                _ob.Name = ob.Name;
                _ob.Description = ob.Description;
                _ob.UpdatedDate = ob.UpdatedDate;
                _ob.UpdatedBy = ob.UpdatedBy;
                _ob.StartDate = ob.StartDate;
                _ob.EndDate = ob.EndDate;
                _ob.DiscountRate = _ob.DiscountRate;
                _ob.IsActive = ob.IsActive;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(MemberCard ob)
        {
            try
            {
                MemberCard _ob = GetByID(ob.ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                MemberCard _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}

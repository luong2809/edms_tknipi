﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Scope;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using System.Linq;

    using EDMs.Web.Utilities.Sessions;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ICAOutgoingTransmittalEditForm : Page
    {
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;
        /// <summary>
        /// The transmittal service.
        /// </summary>
        private readonly ICATransmittalService iCATransmittalService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly ContractorService contractorService;

        private readonly RoleService rolesevice;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ICAOutgoingTransmittalEditForm()
        {
            this.userService = new UserService();
            this.rolesevice = new RoleService();
            this.iCATransmittalService = new ICATransmittalService();
            this.scopeProjectService = new ScopeProjectService();
            this.contractorService = new ContractorService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {
                    this.CreatedInfo.Visible = true;

                    var objTran = this.iCATransmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                    if (objTran != null)
                    {
                        this.ddlProject.SelectedValue = objTran.ProjectId.ToString();
                        this.txtTransmittalNumber.Text = objTran.TransmittalNumber;
                        this.ddlReason.SelectedValue = !string.IsNullOrEmpty(objTran.ReasonForIssue)
                            ? objTran.ReasonForIssue
                            : "0";
                        this.ddlDivision.SelectedValue = objTran.CADivision.ToString();
                        this.txtDate.SelectedDate = objTran.IssuseDate;

                        this.UploadControl.Visible = !objTran.IsGenerate.GetValueOrDefault();

                        var createdUser = this.userService.GetByID(objTran.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objTran.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objTran.LastUpdatedBy != null && objTran.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objTran.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objTran.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                var objTran = new ICATransmittal();
                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {
                    var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                    objTran = this.iCATransmittalService.GetById(tranId);
                    if (objTran != null)
                    {
                        objTran.TransmittalNumber = this.txtTransmittalNumber.Text.Trim();
                        objTran.ProjectName = this.ddlProject.SelectedItem.Text;
                        objTran.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                        objTran.ReasonForIssue = this.ddlReason.SelectedItem.Text;
                        objTran.CADivision = Convert.ToInt32(this.ddlDivision.SelectedValue);
                        objTran.IssuseDate = this.txtDate.SelectedDate;
                        objTran.TypeTran = 2;
                        objTran.LastUpdatedBy = UserSession.Current.User.Id;
                        objTran.LastUpdatedDate = DateTime.Now;

                        this.iCATransmittalService.Update(objTran);
                    }
                }
                else
                {
                    objTran = new ICATransmittal()
                    {
                        TransmittalNumber = this.txtTransmittalNumber.Text.Trim(),
                        ProjectName = this.ddlProject.SelectedItem.Text,
                        ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                        ReasonForIssue = this.ddlReason.SelectedItem.Text,
                        CADivision = Convert.ToInt32(this.ddlDivision.SelectedValue),
                        IssuseDate = this.txtDate.SelectedDate,
                        TypeTran = 2,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                    };

                    var transId = this.iCATransmittalService.Insert(objTran);
                }
                if (objTran != null)
                {
                    foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                    {
                        var filename = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        var oldfilePath = Server.MapPath(objTran.GeneratePath);
                        var newServerPath = "../../Transmittals/Generated/" + filename;
                        var newFilePath = Server.MapPath("../../Transmittals/Generated/") + filename;
                        docFile.SaveAs(newFilePath);
                        objTran.IsGenerate = true;
                        objTran.GeneratePath = newServerPath;
                        if (File.Exists(oldfilePath))
                        {
                            File.Delete(oldfilePath);
                        }
                        this.iCATransmittalService.Update(objTran);
                    }
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if (this.txtTransmittalNumber.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter transmittal Record No.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var projectInPermission = UserSession.Current.IsGip || UserSession.Current.IsCheif
                    ? this.scopeProjectService.GetAll().Where(t => t.ProjectManagerId == UserSession.Current.User.Id)
                    : this.scopeProjectService.GetAll();
            projectInPermission = projectInPermission.OrderBy(t => t.Name).ToList();
            this.ddlProject.DataSource = projectInPermission;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();
        }
    }
}
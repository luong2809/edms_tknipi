﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.DAO;
using EDMs.Data.Entities;

namespace EDMs.Business.Services
{
    public class AppointmentStatusService
    {  
        private readonly AppointmentStatusDAO repo;

        public AppointmentStatusService()
        {
            repo = new AppointmentStatusDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All AppointmentStatus
        /// </summary>
        /// <returns></returns>
        public List<AppointmentStatus> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get AppointmentStatus By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public AppointmentStatus GetByID(int ID)
        {
            return repo.GetByID( ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert AppointmentStatus
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(AppointmentStatus bo)
        {
            try
            {
                return repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update AppointmentStatus
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(AppointmentStatus bo)
        {
            try
            {
                return repo.Update(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete AppointmentStatus
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(AppointmentStatus bo)
        {
            try
            {
                return repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete AppointmentStatus By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

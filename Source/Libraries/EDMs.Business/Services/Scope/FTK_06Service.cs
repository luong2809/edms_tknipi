﻿

namespace EDMs.Business.Services.Scope
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using EDMs.Data.DAO.Scope;
    using EDMs.Data.Entities;
  public  class FTK_06Service
    {

      /// <summary>
        /// The repo.
        /// </summary>
        private readonly FTK_06DAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FTK_06Service"/> class.
        /// </summary>
        public FTK_06Service()
        {
            this.repo = new FTK_06DAO();
        }

        #region Get (Advances)
        public List<FTK_06> GetAllByWorkpackage(int workpackageId)
        {
            return this.repo.GetAll().Where(t => t.WorkpackageId == workpackageId).ToList();
        }

        public List<FTK_06> GetAllByWorkpackage(List<int> workpackageIds)
        {
            return this.repo.GetAll().Where(t => workpackageIds.Contains(t.WorkpackageId.GetValueOrDefault())).ToList();
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<FTK_06> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public FTK_06 GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(FTK_06 bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(FTK_06 bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(FTK_06 bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
